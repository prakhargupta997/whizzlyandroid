package com.whizzly.search_module.search_people_videos;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.search.search_video.VideoSearchResponseBean;
import com.whizzly.models.search.users.UserSearchResponseBean;
import com.whizzly.utils.AppConstants;


public class SearchPeopleAndVideosViewModel extends ViewModel {
    private SearchPeopleAndVideosModel mSearchModel;
    private RichMediatorLiveData<UserSearchResponseBean> mUserSearchResponseBeanLiveData;
    private RichMediatorLiveData<VideoSearchResponseBean> mVideoListResponseBeanLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private OnClickSendListener onClickSendListener;

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureResponseObserver;
        initLiveData();
    }


    private void initLiveData() {
        if (mUserSearchResponseBeanLiveData == null) {
            mUserSearchResponseBeanLiveData = new RichMediatorLiveData<UserSearchResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
        if (mVideoListResponseBeanLiveData == null) {
            mVideoListResponseBeanLiveData = new RichMediatorLiveData<VideoSearchResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
    }

    public RichMediatorLiveData<VideoSearchResponseBean> getVideosSearchliveData() {
        return mVideoListResponseBeanLiveData;
    }

    public RichMediatorLiveData<UserSearchResponseBean> getUserSearchLiveData() {
        return mUserSearchResponseBeanLiveData;
    }

    public void hitVideoSearchApi(String seachText) {
        mSearchModel.hitSearchApi(seachText, mVideoListResponseBeanLiveData);

    }

    public void setModel(SearchPeopleAndVideosModel searchModel) {
        this.mSearchModel = searchModel;
    }

    public void hitUserSearchApi(String search) {
        mSearchModel.hitUserSearchApi(search, mUserSearchResponseBeanLiveData);
    }

    public void onCrossViewClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.CROSS_VIEW_CLICKED);
    }
}

