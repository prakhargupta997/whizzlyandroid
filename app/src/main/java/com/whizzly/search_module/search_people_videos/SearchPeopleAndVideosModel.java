package com.whizzly.search_module.search_people_videos;

import android.content.Context;
import androidx.databinding.BaseObservable;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.search.search_video.VideoSearchResponseBean;
import com.whizzly.models.search.users.UserSearchResponseBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import java.util.HashMap;


public class SearchPeopleAndVideosModel extends BaseObservable {
    private Context context;
   public SearchPeopleAndVideosModel(Context context) {
        this.context=context;
    }

    public void hitSearchApi(String seachText, RichMediatorLiveData<VideoSearchResponseBean> mVideoListResponseBeanLiveData) {
        if(AppUtils.isNetworkAvailable(context)) {

            DataManager.getInstance().hitVideoSearchApi(getSearchData(seachText)).enqueue(new NetworkCallback<VideoSearchResponseBean>() {
                @Override
                public void onSuccess(VideoSearchResponseBean videoListResponseBean) {
                    mVideoListResponseBeanLiveData.setValue(videoListResponseBean);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    mVideoListResponseBeanLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    mVideoListResponseBeanLiveData.setError(t);
                }
            });

        }else {
            Toast.makeText(context, context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    private HashMap<String, String>  getSearchData(String seachText) {
       HashMap<String,String> hashMap=new HashMap();
       hashMap.put(AppConstants.NetworkConstants.USER_ID,String.valueOf(DataManager.getInstance().getUserId()));
       hashMap.put(AppConstants.NetworkConstants.SEARCH,seachText);
       return hashMap;
   }


    public void hitUserSearchApi(String search, RichMediatorLiveData<UserSearchResponseBean> mUserSearchResponseBeanLiveData) {
        if(AppUtils.isNetworkAvailable(context)) {

            DataManager.getInstance().hitUserSearchApi(getSearchData(search)).enqueue(new NetworkCallback<UserSearchResponseBean>() {
                @Override
                public void onSuccess(UserSearchResponseBean userSearchResponseBean) {
                    mUserSearchResponseBeanLiveData.setValue(userSearchResponseBean);
                    userSearchResponseBean.getResult().setSearchValue(search);

                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    mUserSearchResponseBeanLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    mUserSearchResponseBeanLiveData.setError(t);
                }
            });

        }else {
            Toast.makeText(context, context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }
}
