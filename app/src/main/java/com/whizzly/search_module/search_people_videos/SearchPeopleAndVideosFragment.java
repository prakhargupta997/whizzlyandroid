package com.whizzly.search_module.search_people_videos;


import android.app.Activity;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.FragmentSearchPeopleVideosBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.home_screen_module.home_screen_fragments.user.UserListingFragment;
import com.whizzly.home_screen_module.home_screen_fragments.videos.VideoListingFragment;
import com.whizzly.interfaces.OnBackPressedListener;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.search.search_video.VideoSearchResponseBean;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;
import com.whizzly.utils.GenericFragmentPagerAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class SearchPeopleAndVideosFragment extends Fragment implements TextWatcher, OnClickSendListener, OnBackPressedListener {
    private FragmentSearchPeopleVideosBinding mFragmentSearchPeopleVideosBinding;
    private SearchPeopleAndVideosViewModel mSearchViewModel;
    private SearchPeopleAndVideosModel mSearchModel;
    private Activity mActivity;
    private GenericFragmentPagerAdapter pagerAdapter;
    private List<Fragment> mList;

    public SearchPeopleAndVideosFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mActivity = getActivity();
        mFragmentSearchPeopleVideosBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_people_videos, container, false);
        mSearchModel = new SearchPeopleAndVideosModel(mActivity);
        mSearchViewModel = ViewModelProviders.of(this).get(SearchPeopleAndVideosViewModel.class);
        mSearchViewModel.setGenericListeners(((HomeActivity) Objects.requireNonNull(getActivity())).getErrorObserver(), ((HomeActivity) Objects.requireNonNull(getActivity())).getFailureResponseObserver());
        mFragmentSearchPeopleVideosBinding.setViewModel(mSearchViewModel);
        mSearchViewModel.setOnClickSendListener(this);
        mSearchViewModel.setModel(mSearchModel);
        if(DataManager.getInstance().isNotchDevice()){
            mFragmentSearchPeopleVideosBinding.view.setVisibility(View.VISIBLE);
        }else{
            mFragmentSearchPeopleVideosBinding.view.setVisibility(View.GONE);
        }
        ((HomeActivity) getActivity()).setOnBackPressedListener(this);
        initViewsAndVariables();
        observeLiveData();
        setSwipeListener();

        mFragmentSearchPeopleVideosBinding.vpPeopleVideos.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
                mFragmentSearchPeopleVideosBinding.etSearch.setText("");
                if (pagerAdapter != null && pagerAdapter.getItem(mFragmentSearchPeopleVideosBinding.vpPeopleVideos.getCurrentItem()) instanceof VideoListingFragment) {
                    mFragmentSearchPeopleVideosBinding.tvHash.setVisibility(View.VISIBLE);
                    mFragmentSearchPeopleVideosBinding.etSearch.setSelection(mFragmentSearchPeopleVideosBinding.etSearch.getText().toString().length());
                } else
                    mFragmentSearchPeopleVideosBinding.tvHash.setVisibility(View.GONE);
            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        return mFragmentSearchPeopleVideosBinding.getRoot();
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void initViewsAndVariables() {
        mFragmentSearchPeopleVideosBinding.etSearch.addTextChangedListener(this);
    }

    private void observeLiveData() {
        mSearchViewModel.getUserSearchLiveData().observe(this, response -> {
            if (response != null) {
                if (response.getCode() == 200) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (mFragmentSearchPeopleVideosBinding.srvSearch.isRefreshing()) {
                                mFragmentSearchPeopleVideosBinding.etSearch.setText("");
                                mFragmentSearchPeopleVideosBinding.srvSearch.setRefreshing(false);
                            }
                            if ((mList != null && mList.size() > 0))
                                ((UserListingFragment) mList.get(0)).setUserData(response.getResult(), response.getNextCount());
                        }
                    }, 200);

                } else if (response.getCode() == 301 || response.getCode() == 302) {
                    Toast.makeText(mActivity, response.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) Objects.requireNonNull(getActivity())).autoLogOut();
                }
            }

        });


        mSearchViewModel.getVideosSearchliveData().observe(this, (VideoSearchResponseBean response) -> {
            if (response != null) {
                if (response.getCode() == 200) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (mFragmentSearchPeopleVideosBinding.srvSearch.isRefreshing()) {
                                mFragmentSearchPeopleVideosBinding.etSearch.setText("");
                                mFragmentSearchPeopleVideosBinding.srvSearch.setRefreshing(false);
                            }

                            if ((mList != null && mList.size() > 1))
                                ((VideoListingFragment) mList.get(1)).setUserData(response.getResult());
                        }
                    }, 200);


                } else if (response.getCode() == 301 || response.getCode() == 302) {
                    Toast.makeText(mActivity, response.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) Objects.requireNonNull(getActivity())).autoLogOut();
                }
            }


        });
    }

    private void setSwipeListener() {
        mFragmentSearchPeopleVideosBinding.srvSearch.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getUserOrVideosListing("", true);
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpViewPager();
//        getUserOrVideosListing("", true);

    }

    private void setUpViewPager() {
        mList = new ArrayList<>();

        VideoListingFragment videoListingFragment = new VideoListingFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.ClassConstants.SEARCH_TYPE, AppConstants.SEARCH_TYPE_PEOPLE);
        Bundle bundle1 = new Bundle();
        bundle1.putInt(AppConstants.ClassConstants.SEARCH_TYPE, AppConstants.SEARCH_TYPE_VIDEOS);
        UserListingFragment userListingFragment = new UserListingFragment();

        mList.add(userListingFragment);
        mList.add(videoListingFragment);

        List<String> titles = new ArrayList<>();

        titles.add(getString(R.string.s_people));
        titles.add(getString(R.string.s_videos));

        pagerAdapter = new GenericFragmentPagerAdapter(getChildFragmentManager(), mList, titles);
        mFragmentSearchPeopleVideosBinding.vpPeopleVideos.setOffscreenPageLimit(0);
        mFragmentSearchPeopleVideosBinding.vpPeopleVideos.setAdapter(pagerAdapter);
        mFragmentSearchPeopleVideosBinding.tbPeopleVideos.setupWithViewPager(mFragmentSearchPeopleVideosBinding.vpPeopleVideos);

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable seachText) {
        String search = seachText.toString().trim();

        mFragmentSearchPeopleVideosBinding.ivCross.setVisibility(View.VISIBLE);
        if (mFragmentSearchPeopleVideosBinding.etSearch.getText().toString().contains("#"))
            search = mFragmentSearchPeopleVideosBinding.etSearch.getText().toString().trim().replace("#", "");

        if (search.length() > 0) {
            getUserOrVideosListing(search, false);
            mFragmentSearchPeopleVideosBinding.ivCross.setVisibility(View.VISIBLE);
        }else {
            getUserOrVideosListing("", false);
            mFragmentSearchPeopleVideosBinding.ivCross.setVisibility(View.GONE);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    private void getUserOrVideosListing(String seachText, Boolean refresh) {
        Fragment fragment = pagerAdapter.getItem(mFragmentSearchPeopleVideosBinding.vpPeopleVideos.getCurrentItem());
        if (fragment != null) {
            if (refresh) {
                mSearchViewModel.hitVideoSearchApi(String.valueOf(seachText));
                mSearchViewModel.hitUserSearchApi(String.valueOf(seachText));

            } else {
                if (fragment instanceof VideoListingFragment)
                    mSearchViewModel.hitVideoSearchApi(String.valueOf(seachText));
                else if (fragment instanceof UserListingFragment)
                    mSearchViewModel.hitUserSearchApi(String.valueOf(seachText));

            }
        }
    }


    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.CROSS_VIEW_CLICKED:
                mFragmentSearchPeopleVideosBinding.etSearch.setText("");
//                getUserOrVideosListing("", true);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        ((HomeActivity) getActivity()).onClickSend(AppConstants.ClassConstants.ON_HOME_CLICKED);
    }
}
