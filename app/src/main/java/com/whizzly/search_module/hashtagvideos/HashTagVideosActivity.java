package com.whizzly.search_module.hashtagvideos;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.ActivityHastagVideosBinding;
import com.whizzly.models.search.videos.VideoList;
import com.whizzly.models.suggested_videos_info.Result;
import com.whizzly.notification.NotificationActivity;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.EndlessRecyclerOnScrollListener;
import com.whizzly.utils.SpacesItemDecorationGrid;

import java.util.ArrayList;
import java.util.Objects;


public class HashTagVideosActivity extends BaseActivity implements OnClickAdapterListener {
    private HashtagVideosViewModel mHashtagVideosViewModel;
    private ActivityHastagVideosBinding mActivityHashTagVideosBinding;
    private ArrayList<Object> mHashTagVideoList;
    private HashTagVideosAdapter mHashTagVideosAdapter;
    private Integer mPage = 1;
    private String mSearch = "";
    private int isFrom;
    private int mVideoId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityHashTagVideosBinding = DataBindingUtil.setContentView(this, R.layout.activity_hastag_videos);
        getData();
        mActivityHashTagVideosBinding.toolbar.ivBack.setOnClickListener(v -> onBackPressed());
        mHashtagVideosViewModel = ViewModelProviders.of(this).get(HashtagVideosViewModel.class);
        mActivityHashTagVideosBinding.setViewModel(mHashtagVideosViewModel);
        mHashtagVideosViewModel.setGenericListeners(getFailureResponseObserver(), getErrorObserver());
        observeLiveData();
        mHashTagVideoList = new ArrayList<>();
        setRecyclerView();
        if (isFrom == AppConstants.ClassConstants.IS_FROM_VIDEO_LISTING)
            getHashTagVideosList();
        else if (isFrom == AppConstants.ClassConstants.IS_FROM_FEEDS)
            getSuggestedVideoList();

    }

    private void getSuggestedVideoList() {
        if (AppUtils.isNetworkAvailable(this))
            mHashtagVideosViewModel.getSuggestedVideos(mPage, mVideoId);
        else {
            mActivityHashTagVideosBinding.bar.setVisibility(View.GONE);
            Toast.makeText(this, getString(R.string.txt_no_internet_connection), Toast.LENGTH_SHORT).show();
        }
    }


    private void getHashTagVideosList() {
        if (AppUtils.isNetworkAvailable(this))
            mHashtagVideosViewModel.getHashTagVideos(mPage, mSearch);
        else {
            mActivityHashTagVideosBinding.bar.setVisibility(View.GONE);
            Toast.makeText(this, getString(R.string.txt_no_internet_connection), Toast.LENGTH_SHORT).show();
        }
    }

    private void getData() {
        if (getIntent() != null && getIntent().hasExtra(AppConstants.ClassConstants.IS_FROM)) {
            isFrom = getIntent().getIntExtra(AppConstants.ClassConstants.IS_FROM, 0);
            if (isFrom == AppConstants.ClassConstants.IS_FROM_VIDEO_LISTING && getIntent().hasExtra(AppConstants.ClassConstants.VIDEO_DETAILS)) {
                mSearch = getIntent().getStringExtra(AppConstants.ClassConstants.VIDEO_DETAILS);
                mActivityHashTagVideosBinding.toolbar.tvToolbarText.setText("#" + mSearch.trim());
            } else if (isFrom == AppConstants.ClassConstants.IS_FROM_FEEDS) {
                mVideoId = getIntent().getIntExtra(AppConstants.NetworkConstants.VIDEO_ID, 0);
                mActivityHashTagVideosBinding.toolbar.tvToolbarText.setText(getString(R.string.s_related_videos));

            }
            mActivityHashTagVideosBinding.toolbar.ivBack.setVisibility(View.VISIBLE);

        }
    }

    private void setRecyclerView() {
        GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
        mActivityHashTagVideosBinding.rvVideos.setLayoutManager(layoutManager);
        mHashTagVideosAdapter = new HashTagVideosAdapter(mHashTagVideoList, this);
        mActivityHashTagVideosBinding.rvVideos.addItemDecoration(new SpacesItemDecorationGrid(this, 1, 3));
        mActivityHashTagVideosBinding.rvVideos.setAdapter(mHashTagVideosAdapter);
        mActivityHashTagVideosBinding.rvVideos.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                if (mPage != -1)
                    getHashTagVideosList();
            }
        });
    }


    private void observeLiveData() {
        mHashtagVideosViewModel.getHashTagVideosLiveData().observe(this, hashTagVideosResponse -> {
            mActivityHashTagVideosBinding.bar.setVisibility(View.GONE);
            if (hashTagVideosResponse != null) {
                if (hashTagVideosResponse.getCode() == 200) {
                    if (hashTagVideosResponse.getResult().getVideoList().size() > 0) {
                        mHashTagVideoList.clear();
                        mActivityHashTagVideosBinding.tvNoData.setVisibility(View.GONE);
                        mActivityHashTagVideosBinding.rvVideos.setVisibility(View.VISIBLE);
                        mHashTagVideoList.addAll(hashTagVideosResponse.getResult().getVideoList());
                        mPage = hashTagVideosResponse.getNextCount();
                        mHashTagVideosAdapter.notifyDataSetChanged();
                    } else {
                        mActivityHashTagVideosBinding.tvNoData.setVisibility(View.VISIBLE);
                        mActivityHashTagVideosBinding.rvVideos.setVisibility(View.GONE);
                        mActivityHashTagVideosBinding.tvNoData.setText(HashTagVideosActivity.this.getResources().getString(R.string.txt_no_videos_available_for_hashtag));
                    }


                } else if (hashTagVideosResponse.getCode() == 301 || hashTagVideosResponse.getCode() == 302) {
                    Toast.makeText(HashTagVideosActivity.this, hashTagVideosResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    Objects.requireNonNull(HashTagVideosActivity.this).autoLogOut();
                } else {
                    Toast.makeText(HashTagVideosActivity.this, hashTagVideosResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

        });
        mHashtagVideosViewModel.getSuggestedVideosLiveData().observe(this, suggestedVideoInfoResponse -> {
            mActivityHashTagVideosBinding.bar.setVisibility(View.GONE);
            if (suggestedVideoInfoResponse != null) {
                if (suggestedVideoInfoResponse.getCode() == 200) {
                    if (suggestedVideoInfoResponse.getResult().size() > 0) {
                        mHashTagVideoList.clear();
                        mActivityHashTagVideosBinding.tvNoData.setVisibility(View.GONE);
                        mActivityHashTagVideosBinding.rvVideos.setVisibility(View.VISIBLE);
                        mHashTagVideoList.addAll(suggestedVideoInfoResponse.getResult());
                        mPage = suggestedVideoInfoResponse.getNextCount();
                        mHashTagVideosAdapter.notifyDataSetChanged();
                    } else {
                        mActivityHashTagVideosBinding.tvNoData.setVisibility(View.VISIBLE);
                        mActivityHashTagVideosBinding.rvVideos.setVisibility(View.GONE);
                        mActivityHashTagVideosBinding.tvNoData.setText(HashTagVideosActivity.this.getResources().getString(R.string.txt_no_related_videos_available));
                    }

                } else if (suggestedVideoInfoResponse.getCode() == 301 || suggestedVideoInfoResponse.getCode() == 302) {
                    Toast.makeText(HashTagVideosActivity.this, suggestedVideoInfoResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    HashTagVideosActivity.this.autoLogOut();
                } else {
                    Toast.makeText(HashTagVideosActivity.this, suggestedVideoInfoResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    @Override
    public void onAdapterClicked(int code, Object obj, int position) {
        if (obj != null) {
            if (obj instanceof VideoList) {
                VideoList videoData = (VideoList) obj;
                Intent intent = new Intent(this, NotificationActivity.class);
                intent.putExtra(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.FROM_HASHTAG_VIDEOS);
                intent.putExtra(AppConstants.ClassConstants.NOTIFICATION_DATA, videoData);
                startActivity(intent);
            } else if (obj instanceof Result) {
                Result result = (Result) obj;
                Intent intent = new Intent(this, NotificationActivity.class);
                intent.putExtra(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_FEEDS);
                intent.putExtra(AppConstants.ClassConstants.NOTIFICATION_DATA, result);
                startActivity(intent);

            }
        }
    }
}