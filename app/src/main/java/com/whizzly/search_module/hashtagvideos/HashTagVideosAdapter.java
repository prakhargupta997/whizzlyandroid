package com.whizzly.search_module.hashtagvideos;


import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.ItemRowSuggestedVideoBinding;
import com.whizzly.models.profileVideo.VideoInfo;
import com.whizzly.models.search.videos.VideoList;
import com.whizzly.models.suggested_videos_info.Result;
import com.whizzly.utils.AppConstants;

import java.util.ArrayList;

public class HashTagVideosAdapter extends RecyclerView.Adapter<HashTagVideosAdapter.ViewHolder> {

    private ItemRowSuggestedVideoBinding itemRowSuggestedVideoBinding;
    private OnClickAdapterListener onClickAdapterListener;
    private ArrayList<Object> mHashTagVideoList;
    private int width = 0;
    private ViewGroup viewGroup;

    public HashTagVideosAdapter(ArrayList<Object> videoListBeans, OnClickAdapterListener onClickAdapterListener) {
        this.mHashTagVideoList = videoListBeans;
        this.onClickAdapterListener = onClickAdapterListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        itemRowSuggestedVideoBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_row_suggested_video, viewGroup, false);
        width = viewGroup.getWidth() / 3;
        return new ViewHolder(itemRowSuggestedVideoBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if (mHashTagVideoList.get(i) instanceof VideoInfo) {
            VideoInfo videoInfo= (VideoInfo) mHashTagVideoList.get(i);
            RequestOptions requestOptions = new RequestOptions();
            viewHolder.itemRowSuggestedVideoBinding.ivVideo.setMinimumHeight(width);
            viewHolder.itemRowSuggestedVideoBinding.ivVideo.setMinimumWidth(width);
            requestOptions.placeholder(R.drawable.ic_choose_image_gallery);
            Glide.with(itemRowSuggestedVideoBinding.getRoot()).load(videoInfo.getThumbnailUrl()).apply(requestOptions).into(itemRowSuggestedVideoBinding.ivVideo);
            viewHolder.itemRowSuggestedVideoBinding.ivVideo.setOnClickListener(view -> onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_SUGGESTION_VIDEO_CLICKED, videoInfo, i));
        }
        else if (mHashTagVideoList.get(i) instanceof Result){
            Result result= (Result) mHashTagVideoList.get(i);
            RequestOptions requestOptions = new RequestOptions();
            viewHolder.itemRowSuggestedVideoBinding.ivVideo.setMinimumHeight(width);
            viewHolder.itemRowSuggestedVideoBinding.ivVideo.setMinimumWidth(width);
            requestOptions.placeholder(R.drawable.ic_choose_image_gallery);
            Glide.with(itemRowSuggestedVideoBinding.getRoot()).load(result.getThumbnailUrl()).apply(requestOptions).into(itemRowSuggestedVideoBinding.ivVideo);
            viewHolder.itemRowSuggestedVideoBinding.ivVideo.setOnClickListener(view -> onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_SUGGESTION_VIDEO_CLICKED, result, i));

        }else if(mHashTagVideoList.get(i) instanceof VideoList){
            VideoList result= (VideoList) mHashTagVideoList.get(i);
            RequestOptions requestOptions = new RequestOptions();
            viewHolder.itemRowSuggestedVideoBinding.ivVideo.setMinimumHeight(width);
            viewHolder.itemRowSuggestedVideoBinding.ivVideo.setMinimumWidth(width);
            requestOptions.placeholder(R.drawable.ic_choose_image_gallery);
            Glide.with(itemRowSuggestedVideoBinding.getRoot()).load(result.getThumbnailUrl()).apply(requestOptions).into(itemRowSuggestedVideoBinding.ivVideo);
            viewHolder.itemRowSuggestedVideoBinding.ivVideo.setOnClickListener(view -> onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_SUGGESTION_VIDEO_CLICKED, result, i));

        }
    }
    @Override
    public int getItemCount() {
        return mHashTagVideoList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ItemRowSuggestedVideoBinding itemRowSuggestedVideoBinding;

        public ViewHolder(@NonNull ItemRowSuggestedVideoBinding itemRowSuggestedVideoBinding) {
            super(itemRowSuggestedVideoBinding.getRoot());
            this.itemRowSuggestedVideoBinding = itemRowSuggestedVideoBinding;
        }

        public void bind(VideoList result) {
            itemRowSuggestedVideoBinding.setVariable(BR.model, result);
            itemRowSuggestedVideoBinding.executePendingBindings();
        }
    }
}