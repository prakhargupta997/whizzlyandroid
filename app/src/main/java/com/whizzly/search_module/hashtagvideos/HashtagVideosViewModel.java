package com.whizzly.search_module.hashtagvideos;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.search.videos.VideoSearchResponseBean;
import com.whizzly.models.suggested_videos_info.SuggestedVideoInfoResponse;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class HashtagVideosViewModel extends ViewModel {
    private RichMediatorLiveData<VideoSearchResponseBean> mHashtagVideosLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private RichMediatorLiveData<SuggestedVideoInfoResponse> mSuggestedVideosLiveData;

    public void setGenericListeners(Observer<FailureResponse> failureResponseObserver, Observer<Throwable> errorObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureResponseObserver;
        initializeLiveData();

    }

    private void initializeLiveData() {
        if (mHashtagVideosLiveData == null) {
            mHashtagVideosLiveData = new RichMediatorLiveData<VideoSearchResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
        if (mSuggestedVideosLiveData==null){
            mSuggestedVideosLiveData=new RichMediatorLiveData<SuggestedVideoInfoResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

    }

    public RichMediatorLiveData<VideoSearchResponseBean> getHashTagVideosLiveData() {
        return mHashtagVideosLiveData;
    }
    public RichMediatorLiveData<SuggestedVideoInfoResponse> getSuggestedVideosLiveData(){
        return mSuggestedVideosLiveData;
    }



    private HashMap<String, String> getData(Integer mPage, String mSearch) {
        HashMap<String,String> hashMap=new HashMap();
        hashMap.put(AppConstants.NetworkConstants.USER_ID,String.valueOf(DataManager.getInstance().getUserId()));
        hashMap.put(AppConstants.NetworkConstants.PAGE,String.valueOf(mPage));
        hashMap.put(AppConstants.NetworkConstants.SEARCH,mSearch);
        return hashMap;
    }

    public void getHashTagVideos(Integer mPage, String mSearch) {
        DataManager.getInstance().hitExactSearchApi(getData(mPage,mSearch)).enqueue(new NetworkCallback<VideoSearchResponseBean>() {
            @Override
            public void onSuccess(VideoSearchResponseBean videoSearchResponseBean) {
                mHashtagVideosLiveData.setValue(videoSearchResponseBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mHashtagVideosLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mHashtagVideosLiveData.setError(t);
            }
        });
    }

    public void getSuggestedVideos(Integer mPage, int mVideoId) {
        DataManager.getInstance().getSuggestedVideos(getSuggestedVideoData(mPage,mVideoId)).enqueue(new NetworkCallback<SuggestedVideoInfoResponse>() {
            @Override
            public void onSuccess(SuggestedVideoInfoResponse suggestedVideoInfoResponse) {
                mSuggestedVideosLiveData.setValue(suggestedVideoInfoResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mSuggestedVideosLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mSuggestedVideosLiveData.setError(t);
            }
        });
    }

    private HashMap<String,String> getSuggestedVideoData(Integer mPage, int mVideoId) {
        HashMap<String,String> hashMap=new HashMap();
        hashMap.put(AppConstants.NetworkConstants.USER_ID,String.valueOf(DataManager.getInstance().getUserId()));
        hashMap.put(AppConstants.NetworkConstants.PAGE,String.valueOf(mPage));
        hashMap.put(AppConstants.NetworkConstants.VIDEO_ID, String.valueOf(mVideoId));
        return hashMap;
    }
}