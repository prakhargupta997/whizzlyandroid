package com.whizzly.home_screen_module.home_screen_activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.ActivityInnerFragmentsHostBinding;
import com.whizzly.home_screen_module.home_screen_fragments.profile.edit_post.EditPostFragment;
import com.whizzly.home_screen_module.home_screen_fragments.profile.edit_profile.EditProfileFragment;
import com.whizzly.home_screen_module.home_screen_fragments.profile.settings.SettingsFragment;
import com.whizzly.home_screen_module.home_screen_models.InnerFragmentsHostModel;
import com.whizzly.home_screen_module.home_screen_view_model.InnerFragmentsHostViewModel;
import com.whizzly.models.feeds_response.Result;
import com.whizzly.models.profile.ProfileInfo;
import com.whizzly.utils.AppConstants;

public class InnerFragmentsHostActivity extends BaseActivity {

    private InnerFragmentsHostViewModel mInnerFragmentHostViewModel;
    private InnerFragmentsHostModel mInnerFragmentHostModel;
    private ActivityInnerFragmentsHostBinding activityInnerFragmentsHostBinding;
    private EditProfileFragment editProfileFragment;
    private Result mResult;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityInnerFragmentsHostBinding = DataBindingUtil.setContentView(this, R.layout.activity_inner_fragments_host);
        mInnerFragmentHostModel = new InnerFragmentsHostModel();
        getData();
        mInnerFragmentHostViewModel = new InnerFragmentsHostViewModel();
        activityInnerFragmentsHostBinding.setModel(mInnerFragmentHostModel);
        activityInnerFragmentsHostBinding.setViewModel(mInnerFragmentHostViewModel);
//        AppUtils.setStatusBarGradiant(this);

    }

    private void getData() {

        if (getIntent() != null && getIntent().hasExtra(AppConstants.ClassConstants.FRAGMENT_TYPE)) {
            if (getIntent().hasExtra(AppConstants.ClassConstants.FEED_DATA)) {
                mResult = getIntent().getParcelableExtra(AppConstants.ClassConstants.FEED_DATA);
            }
            int fragmentType = getIntent().getIntExtra(AppConstants.ClassConstants.FRAGMENT_TYPE, AppConstants.ClassConstants.FRAGMENT_TYPE_SETTINGS);
            addCurrentFragment(fragmentType);

        }

    }

    private void addCurrentFragment(int fragmentType) {
        switch (fragmentType) {
            case AppConstants.ClassConstants.FRAGMENT_TYPE_SETTINGS:

                SettingsFragment settingsFragment = new SettingsFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(AppConstants.ClassConstants.RATING_COUNT, getIntent().getIntExtra(AppConstants.ClassConstants.RATING_COUNT, 0));
                settingsFragment.setArguments(bundle);
                addFragment(R.id.fl_container_inner, settingsFragment, SettingsFragment.class.getCanonicalName());
                break;

            case AppConstants.ClassConstants.FRAGMENT_TYPE_EDIT_PROFILE:
                ProfileInfo profileInfo = getIntent().getParcelableExtra(AppConstants.ClassConstants.USER_INFO);
                editProfileFragment = new EditProfileFragment();
                Bundle bundle1 = new Bundle();
                bundle1.putParcelable(AppConstants.ClassConstants.USER_INFO, profileInfo);
                editProfileFragment.setArguments(bundle1);
                addFragment(R.id.fl_container_inner, editProfileFragment, EditProfileFragment.class.getCanonicalName());
                break;
            case AppConstants.ClassConstants.FRAGMENT_TYPE_EDIT_POST:
                EditPostFragment editPostFragment = new EditPostFragment();
                Bundle postBundle=new Bundle();
                postBundle.putParcelable(AppConstants.ClassConstants.FEED_DATA,mResult);
                editPostFragment.setArguments(postBundle);
                addFragment(R.id.fl_container_inner, editPostFragment, EditPostFragment.class.getCanonicalName());
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment fragment = currentVisibleFragment();

        if (fragment != null && fragment.isAdded() && fragment.isVisible()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private boolean hasPermission(int[] grantResults) {
        boolean hasPermissions = false;
        int permissionCount = 0;
        for (int grantResult : grantResults) {
            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                hasPermissions = true;
                permissionCount++;
            }
        }
        return hasPermissions && permissionCount == grantResults.length;
    }


    private Fragment currentVisibleFragment() {
        return getSupportFragmentManager()
                .findFragmentById(R.id.fl_container_inner);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (permissions.length == grantResults.length && hasPermission(grantResults)) {
            if (editProfileFragment != null) {
                editProfileFragment.onPermissionGranted(requestCode);
            }
        }

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
