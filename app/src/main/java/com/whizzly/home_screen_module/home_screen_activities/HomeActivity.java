package com.whizzly.home_screen_module.home_screen_activities;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.DisplayCutout;
import android.view.View;
import android.view.Window;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dnitinverma.amazons3library.AmazonS3;
import com.dnitinverma.amazons3library.AmazonS3Constants;
import com.dnitinverma.amazons3library.interfaces.AmazonCallback;
import com.dnitinverma.amazons3library.model.MediaBean;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.whizzly.R;
import com.whizzly.arguments.models.ArgumentDataBean;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.activity.VideoRecordActivity;
import com.whizzly.collab_module.beans.CollabBarBean;
import com.whizzly.collab_module.collab_model.CollabPostModel;
import com.whizzly.collab_module.interfaces.OnSuccessRecordMerge;
import com.whizzly.colorBarChanges.activity.NewTemplateSelectionActivity;
import com.whizzly.databinding.ActivityHomeBinding;
import com.whizzly.home_screen_module.home_screen_fragments.home_feeds.HomeFragment;
import com.whizzly.home_screen_module.home_screen_fragments.notification.personal.PersonalNotificationsFragment;
import com.whizzly.home_screen_module.home_screen_fragments.profile.ProfileFragment;
import com.whizzly.home_screen_module.home_screen_models.HomeScreenModel;
import com.whizzly.home_screen_module.home_screen_view_model.HomeScreenViewModel;
import com.whizzly.interfaces.OnBackPressedListener;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.interfaces.OnOpenUserProfile;
import com.whizzly.interfaces.PermissionListener;
import com.whizzly.models.change_password.ChangePasswordModel;
import com.whizzly.models.notifications.PushNotificationResponse;
import com.whizzly.models.post.VideoPostResponse;
import com.whizzly.models.post_video.AdditionalInfo;
import com.whizzly.models.post_video.Detail;
import com.whizzly.models.privacy_status.PrivacyStatusResponseBean;
import com.whizzly.models.video_list_response.AdditinalInfo;
import com.whizzly.search_module.search_people_videos.SearchPeopleAndVideosFragment;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;
import com.whizzly.utils.FFMpegCommandListener;
import com.whizzly.utils.FFMpegCommands;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class HomeActivity extends BaseActivity implements OnClickSendListener, AmazonCallback, OnSuccessRecordMerge, OnOpenUserProfile {

    public ActivityHomeBinding mActivityHomeBinding;
    private HomeScreenViewModel mHomeScreenViewModel;
    private HomeScreenModel mHomeScreenModel;
    private AmazonS3 amazonS3;
    private String audioUrl, videoUrl;
    private ArrayList<AdditionalInfo> mAdditionalInfos = new ArrayList<>();
    private FFMpegCommands mFFMpegCommands;
    private String mergedFinalFile = "";
    private String mFinalVideo = "";
    private OnSuccessRecordMerge onSuccessRecordMerge;
    private boolean isFromCollab = false;
    private int count = 0;
    private ArgumentDataBean argumentDataBean;
    private String argumentVideo = "", argumentThumbnail = "";
    private Observer<ChangePasswordModel> mDeleteObserver;
    private Observer<PrivacyStatusResponseBean> mSavedVideoObserver;
    private OnBackPressedListener onBackPressedListener;
    private String mWaterMarkVideo;
    private String waterMarkUrl = "";
    private PermissionListener mPermissionListener;
    private String scaledSingleVideo = "";
    private String compressFinalFile = "";
    private CollabPostModel mCollabPostModel;
    private int isShareable = 0;
    private BroadcastReceiver mViewHideReciever;
    private int topPadding = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        mActivityHomeBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        mActivityHomeBinding.getRoot().setOnApplyWindowInsetsListener((view, windowInsets) -> {
            DisplayCutout displayCutout = windowInsets.getDisplayCutout();
            if (displayCutout != null) {
                DataManager.getInstance().setIsNotchDevice(true);
            }else{
                DataManager.getInstance().setIsNotchDevice(false);
            }
            return windowInsets;
        });
        mHomeScreenViewModel = ViewModelProviders.of(this).get(HomeScreenViewModel.class);
        mActivityHomeBinding.setViewModel(mHomeScreenViewModel);
        mHomeScreenModel = new HomeScreenModel();
        mActivityHomeBinding.setModel(mHomeScreenModel);
        mHomeScreenViewModel.setGenericListeners(getFailureResponseObserver(), getErrorObserver());
        onSuccessRecordMerge = this;
        mFFMpegCommands = FFMpegCommands.getInstance(this);
        try {
            mFFMpegCommands.initializeFFmpeg();
        } catch (FFmpegNotSupportedException e) {
            e.printStackTrace();
        }
        initializeAmazonS3();
        mHomeScreenViewModel.setOnClickSendListener(this);
        onClickSend(AppConstants.ClassConstants.ON_HOME_CLICKED);
        getData();
        observeDeleteLiveData();
        observeArgumentVideo();
        observeSavedVideoLiveData();
        setBroadcastReciever();
        mHomeScreenViewModel.getVideoPostResponseRichMediatorLiveData().observe(HomeActivity.this, this::onChanged);
    }

    private void setBroadcastReciever() {
        mViewHideReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (Objects.requireNonNull(intent.getAction())){
                    case AppConstants.ClassConstants.ACTION_SHOW_VIEWS:
                        if(mActivityHomeBinding.llBottom.getVisibility() == View.GONE) {
                            showTopView();
                            mActivityHomeBinding.llBottom.setVisibility(View.VISIBLE);
                        }
                        break;
                    case AppConstants.ClassConstants.ACTION_HIDE_VIEWS:
                        if(mActivityHomeBinding.llBottom.getVisibility() == View.VISIBLE) {
                            hideTopView();
                            mActivityHomeBinding.llBottom.setVisibility(View.GONE);
                        }
                        break;
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AppConstants.ClassConstants.ACTION_SHOW_VIEWS);
        intentFilter.addAction(AppConstants.ClassConstants.ACTION_HIDE_VIEWS);
        LocalBroadcastManager.getInstance(this).registerReceiver(mViewHideReciever, intentFilter);
    }

    private void observeSavedVideoLiveData() {
        mSavedVideoObserver = privacyStatusResponseBean -> {
            if (privacyStatusResponseBean != null) {
                if (privacyStatusResponseBean.getCode() == 200) {
                    Toast.makeText(HomeActivity.this, privacyStatusResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (privacyStatusResponseBean.getCode() == 301 || privacyStatusResponseBean.getCode() == 302) {
                    Toast.makeText(HomeActivity.this, privacyStatusResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                    HomeActivity.this.autoLogOut();
                } else
                    Toast.makeText(HomeActivity.this, HomeActivity.this.getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();

            }

        };
    }

    private boolean isPackageInstalled(String packageName) {
        boolean found = true;
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            found = false;
        }
        return found;
    }

    public void shareURL(Context context, String mUrl, String mPackageName) {
        if (isPackageInstalled(mPackageName)) {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.setPackage(mPackageName);
            i.putExtra(Intent.EXTRA_SUBJECT, "com.whizzly");
            i.putExtra(Intent.EXTRA_TEXT, mUrl);
            context.startActivity(Intent.createChooser(i, "Share URL"));
        } else {
            Toast.makeText(context, getResources().getText(R.string.txt_app_not_installed), Toast.LENGTH_SHORT).show();
        }
    }

    private void observeDeleteLiveData() {
        mDeleteObserver = deleteResponseModel -> {
            if (deleteResponseModel != null) {
                if (deleteResponseModel.getCode().equals("200")) {
                    Toast.makeText(HomeActivity.this, deleteResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    HomeActivity.this.onClickSend(AppConstants.ClassConstants.ON_HOME_CLICKED);
                } else if (deleteResponseModel.getCode().equals("301") || deleteResponseModel.getCode().equals("302")) {
                    Toast.makeText(HomeActivity.this, deleteResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    HomeActivity.this.autoLogOut();
                } else
                    Toast.makeText(HomeActivity.this, HomeActivity.this.getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();

            }

        };
    }

    private void observeArgumentVideo() {
        mHomeScreenViewModel.getArgumentPostResponseRichMediatorLiveData().observe(this, new Observer<VideoPostResponse>() {
            @Override
            public void onChanged(@Nullable VideoPostResponse videoPostResponse) {
                if (videoPostResponse != null) {
                    if (videoPostResponse.getCode() == 200) {
                        if (argumentDataBean.getClickedSocailMedia() == AppConstants.ClassConstants.SHARE_ON_FACEBOOK) {
                            shareURL(HomeActivity.this, videoPostResponse.getResult().getUrl(), "com.facebook.katana");
                        } else if (argumentDataBean.getClickedSocailMedia() == AppConstants.ClassConstants.SHARE_ON_INSTAGRAM) {
                            shareURL(HomeActivity.this, videoPostResponse.getResult().getUrl(), "com.instagram.android");
                        }
                    } else if (videoPostResponse.getCode() == 301 || videoPostResponse.getCode() == 302) {
                        Toast.makeText(HomeActivity.this, videoPostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        HomeActivity.this.autoLogOut();
                    }
                }
            }
        });
    }

    private void getData() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra(AppConstants.ClassConstants.VIDEO_DATA) || intent.hasExtra(AppConstants.ClassConstants.ARGUMENT_DATA)) {
                argumentDataBean = (ArgumentDataBean) intent.getSerializableExtra(AppConstants.ClassConstants.ARGUMENT_DATA);
                String jsonString = intent.getStringExtra(AppConstants.ClassConstants.VIDEO_DATA);
                if (!TextUtils.isEmpty(jsonString)) {
                    isShareable = intent.getIntExtra(AppConstants.ClassConstants.IS_SHAREABLE, 0);
                    mCollabPostModel = new Gson().fromJson(jsonString, CollabPostModel.class);
                    if (mCollabPostModel != null) {
                        if (mCollabPostModel.getCollabUrl() != null && !mCollabPostModel.getCollabUrl().isEmpty() && AppUtils.isNetworkAvailable(this)) {
                            mActivityHomeBinding.videoUpload.clUpload.setVisibility(View.VISIBLE);
                            File file = new File(mCollabPostModel.getCollabThumbnailUrl());
                            Uri imageUri = Uri.fromFile(file);
                            isFromCollab = true;
                            Glide.with(this).load(imageUri).apply(new RequestOptions().override(100, 100)).into(mActivityHomeBinding.videoUpload.ivVideoFrame);
                            mActivityHomeBinding.videoUpload.pbUploadProgress.setVisibility(View.VISIBLE);
                            mActivityHomeBinding.videoUpload.tvContent.setText("Preparing Video");
//                            setAdditionalInfo();
                            compressVideo(mCollabPostModel.getCollabUrl());
                        }
                    }
                } else if (argumentDataBean != null) {
                    mActivityHomeBinding.videoUpload.clUpload.setVisibility(View.VISIBLE);
                    File file = new File(argumentDataBean.getThumbnailPath());
                    Uri imageUri = Uri.fromFile(file);
                    isFromCollab = false;
                    Glide.with(this).load(imageUri).apply(new RequestOptions().override(100, 100)).into(mActivityHomeBinding.videoUpload.ivVideoFrame);
                    mActivityHomeBinding.videoUpload.pbUploadProgress.setVisibility(View.VISIBLE);
                    mActivityHomeBinding.videoUpload.tvContent.setText("Preparing Video");
                    compressVideo(argumentDataBean.getVideoPath());
                }
            } else if (intent.hasExtra(AppConstants.ClassConstants.NOTIFICATION_DATA) && intent.getParcelableExtra(AppConstants.ClassConstants.NOTIFICATION_DATA) != null) {
                PushNotificationResponse pushNotificationResponse = getIntent().getParcelableExtra(AppConstants.ClassConstants.NOTIFICATION_DATA);
                openOtherUserProfile(pushNotificationResponse.getSenderUserId());
            }
        }
    }

    public void openOtherUserProfile(Integer senderUserId) {
        ProfileFragment profileFragment = new ProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.OTHER_USER_PROFILE);
        bundle.putString(AppConstants.ClassConstants.USER_ID, String.valueOf(senderUserId));
        bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_NOTIFICATION_FRAGMENT);
        profileFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, profileFragment).addToBackStack(ProfileFragment.class.getCanonicalName()).commit();

    }

    private String getCompressFile() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "compress_audioAndVideo_" + System.currentTimeMillis() + ".mp4";
    }

    private String getMergedFile() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "merge_audioAndVideo_" + System.currentTimeMillis() + ".mp4";
    }

    private String getWaterMarkFile() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "waterMark_" + System.currentTimeMillis() + ".mp4";
    }

    private void addWaterMarkUrl() {
        mWaterMarkVideo = getWaterMarkFile();

        try {
            mFFMpegCommands.addWaterMark(getFileFromBitmap(), mergedFinalFile, mWaterMarkVideo, new FFMpegCommandListener() {
                @Override
                public void onSuccess(String message) {
                    if (isFromCollab) {
                        mActivityHomeBinding.videoUpload.tvContent.setText("Uploading Video");
                        uploadVideosToS3();
                    } else {
                        mActivityHomeBinding.videoUpload.tvContent.setText("Uploading Video");
                        uploadFilesToS3();
                    }
                }

                @Override
                public void onProgress(String message) {

                }

                @Override
                public void onFailure(String message) {

                }

                @Override
                public void onStart() {

                }

                @Override
                public void onFinish() {

                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private String getPngStringPath() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "watermark_" + System.currentTimeMillis() + ".png";
    }

    private String getFileFromBitmap() throws FileNotFoundException {
        String image = getPngStringPath();
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.watermark);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(new File(image)));
        return image;
    }

    private void compressVideo(String path) {
        mActivityHomeBinding.videoUpload.tvContent.setText(getResources().getString(R.string.txt_preparing_video));
        mFinalVideo = getMergedFile();
        mFFMpegCommands.compressVideo(path, mFinalVideo, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                onSuccessRecordMerge.onMergeSuccess();
            }

            @Override
            public void onProgress(String message) {

            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onStart() {

            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void moveAtomFFmpeg() {
        mergedFinalFile = getMergedFile();
        try {
            mFFMpegCommands.moveAtomWithVideos(compressFinalFile, mergedFinalFile, new FFMpegCommandListener() {
                @Override
                public void onSuccess(String message) {
                    addWaterMarkUrl();

                }

                @Override
                public void onProgress(String message) {

                }

                @Override
                public void onFailure(String message) {

                }

                @Override
                public void onStart() {

                }

                @Override
                public void onFinish() {

                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    private void uploadFilesToS3() {
        mActivityHomeBinding.videoUpload.tvContent.setText(getResources().getString(R.string.txt_uploading_video));
        MediaBean uploadThumbnail = new MediaBean();
        uploadThumbnail.setMediaPath(argumentDataBean.getThumbnailPath());
        amazonS3.upload(uploadThumbnail);
        MediaBean uploadVideo = new MediaBean();
        uploadVideo.setMediaPath(argumentDataBean.getVideoPath());
        amazonS3.upload(uploadVideo);

        MediaBean uploadWaterMarkVideo = new MediaBean();
        uploadWaterMarkVideo.setMediaPath(mWaterMarkVideo);
        amazonS3.upload(uploadWaterMarkVideo);

        waterMarkUrl = AmazonS3Constants.AMAZON_SERVER_URL + getFileName(Uri.parse(mWaterMarkVideo));

        File thumbFile = new File(argumentDataBean.getThumbnailPath());
        argumentThumbnail = AmazonS3Constants.AMAZON_SERVER_URL + getFileName(Uri.fromFile(thumbFile));

        File videoFile = new File(argumentDataBean.getVideoPath());
        argumentVideo = AmazonS3Constants.AMAZON_SERVER_URL + getFileName(Uri.fromFile(videoFile));

        if (!TextUtils.isEmpty(argumentThumbnail) && !TextUtils.isEmpty(argumentVideo)) {
            postArgument();
        }
    }

    private void postArgument() {
        HashMap<String, String> argumentData = new HashMap<>();
        argumentData.put(AppConstants.NetworkConstants.USER_ID, argumentDataBean.getUserId());
        argumentData.put(AppConstants.NetworkConstants.ARGUMENT_VIDEO_URL, argumentVideo);
        argumentData.put(AppConstants.NetworkConstants.DURATION, argumentDataBean.getDuration());
        argumentData.put(AppConstants.NetworkConstants.ARGUMENT_VIDEO_THUMBNAIL, argumentThumbnail);
        argumentData.put(AppConstants.NetworkConstants.ARGUMENT_VIDEO_TITLE, argumentDataBean.getTitle());
        argumentData.put(AppConstants.NetworkConstants.ARGUMENT_WATERMARK_URL, waterMarkUrl);
        String hashtagString = argumentDataBean.getHashtag();
        String[] hashtag = hashtagString.split(" ");
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < hashtag.length; i++) {
            if (i == 0) {
                stringBuilder.append(hashtag[i].replace("#", ""));
            } else {
                stringBuilder.append(hashtag[i].replace("#", ","));
            }
        }
        argumentData.put(AppConstants.NetworkConstants.ARGUMENT_HASHTAGS, stringBuilder.toString());
        mHomeScreenModel.hitPostArgumentApi(argumentData, mHomeScreenViewModel.getArgumentPostResponseRichMediatorLiveData());
    }

    private void initializeAmazonS3() {
        amazonS3 = new AmazonS3();
        amazonS3.setActivity(this);
        amazonS3.setCallback(this);
    }

    private void uploadVideosToS3() {


        for (int i = 0; i < mCollabPostModel.getCollabVideoArray().size(); i++) {
            MediaBean uploadVideoWithoutAudio = new MediaBean();
            uploadVideoWithoutAudio.setMediaPath(mCollabPostModel.getCollabVideoArray().get(i).getVideoUrl());
            amazonS3.upload(uploadVideoWithoutAudio);
            mCollabPostModel.getCollabVideoArray().get(i).setVideoUrl(AmazonS3Constants.AMAZON_SERVER_URL + getFileName(Uri.parse(mCollabPostModel.getCollabVideoArray().get(i).getVideoUrl())));
        }


        MediaBean uploadAudio = new MediaBean();
        uploadAudio.setMediaPath(mCollabPostModel.getCollabMusicUrl());
        amazonS3.upload(uploadAudio);
        mCollabPostModel.setCollabMusicUrl(AmazonS3Constants.AMAZON_SERVER_URL + getFileName(Uri.parse(mCollabPostModel.getCollabMusicUrl())));

        MediaBean uploadVideoWithAudio = new MediaBean();
        uploadVideoWithAudio.setMediaPath(mergedFinalFile);
        amazonS3.upload(uploadVideoWithAudio);
        mCollabPostModel.setCollabUrl(AmazonS3Constants.AMAZON_SERVER_URL + getFileName(Uri.parse(mergedFinalFile)));

        if (mCollabPostModel.getCollabThumbnailUrl() != null) {
            MediaBean uploadThumbnail = new MediaBean();
            uploadThumbnail.setMediaPath(mCollabPostModel.getCollabThumbnailUrl());
            amazonS3.upload(uploadThumbnail);
            mCollabPostModel.setCollabThumbnailUrl(AmazonS3Constants.AMAZON_SERVER_URL + getFileName(Uri.parse(mCollabPostModel.getCollabThumbnailUrl())));
        }

        for (int i = 0; i < mCollabPostModel.getCollabVideoArray().size(); i++) {
            if (mCollabPostModel.getCollabVideoArray().get(i).getVideoThumbnailUrl() != null) {
                MediaBean uploadVideoThumbnail = new MediaBean();
                uploadVideoThumbnail.setMediaPath(mCollabPostModel.getCollabVideoArray().get(i).getVideoThumbnailUrl());
                amazonS3.upload(uploadVideoThumbnail);
                mCollabPostModel.getCollabVideoArray().get(i).setVideoThumbnailUrl(AmazonS3Constants.AMAZON_SERVER_URL + getFileName(Uri.parse(mCollabPostModel.getCollabVideoArray().get(i).getVideoThumbnailUrl())));
            }
        }

        MediaBean uploadWaterMarkVideo = new MediaBean();
        uploadWaterMarkVideo.setMediaPath(mWaterMarkVideo);
        amazonS3.upload(uploadWaterMarkVideo);
        mCollabPostModel.setCollabWatermarkUrl(AmazonS3Constants.AMAZON_SERVER_URL + getFileName(Uri.parse(mWaterMarkVideo)));

        videoUrl = AmazonS3Constants.AMAZON_SERVER_URL + getFileName(Uri.parse(scaledSingleVideo));
    }

    //scale each video to 640*480 using @ffmpeg from original one
    private void scaleVideo() {
        BlockingQueue<Runnable> mWorkQueue = new LinkedBlockingDeque<>(7);
        MyProcessingClass mThreadPool =
                new MyProcessingClass(Runtime.getRuntime().availableProcessors(), Runtime.getRuntime().availableProcessors(),
                        3, TimeUnit.SECONDS, mWorkQueue);
        final int[] count = {0};
        for (int i = 0; i < mCollabPostModel.getCollabVideoArray().size(); i++) {
            int finalI = i;
            mThreadPool.execute(() -> {
                scaledSingleVideo = getScaledSingleVideo();
                mFFMpegCommands.scaleVideo(mCollabPostModel.getCollabVideoArray().get(finalI).getVideoUrl(), scaledSingleVideo, new FFMpegCommandListener() {
                    @Override
                    public void onSuccess(String message) {
                        Log.e("onSuccess: ", message);
                        count[0]++;
                        if (count[0] == mCollabPostModel.getCollabVideoArray().size()) {
                            mThreadPool.afterExecute(null, null);
                        }
                    }

                    @Override
                    public void onProgress(String message) {
                        Log.e("onProgress: ", message);
                    }

                    @Override
                    public void onFailure(String message) {
                        Log.e("onFailure: ", message);
                    }

                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onFinish() {

                    }
                });
                mCollabPostModel.getCollabVideoArray().get(finalI).setVideoUrl(scaledSingleVideo);
            });
        }

        mThreadPool.prestartAllCoreThreads();

    }

    private String getScaledSingleVideo() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "scaled_single_video_" + System.currentTimeMillis() + ".mp4";
    }

    public String getFileName(Uri uri) {
        String result = null;
        result = uri.getPath();
        int cut = Objects.requireNonNull(result).lastIndexOf('/');
        if (cut != -1) {
            result = result.substring(cut + 1);
        }
        return result;
    }

    public void clearStatusBar() {
        Window w = getWindow();
        w.getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    @Override
    public void onClickSend(int code) {
        Fragment fragment = getFragment(R.id.fl_container);
        switch (code) {
            case AppConstants.ClassConstants.ON_HOME_CLICKED:
                selectBottomNav(true, false, false, false, true);
                addFragment(R.id.fl_container, new HomeFragment(), HomeFragment.class.getCanonicalName());
                break;
            case AppConstants.ClassConstants.ON_SEARCH_CLICKED:
                if (!(fragment instanceof SearchPeopleAndVideosFragment)) {
                    selectBottomNav(false, true, false, false, false);
                    addFragment(R.id.fl_container, new SearchPeopleAndVideosFragment(), SearchPeopleAndVideosFragment.class.getCanonicalName());
                }
                break;
            case AppConstants.ClassConstants.ON_ADD_COLLAB_CLICKED:
                Intent intent = new Intent(this, NewTemplateSelectionActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case AppConstants.ClassConstants.ON_NOTIFICATION_CLICKED:
                selectBottomNav(false, false, true, false, false);
                addFragment(R.id.fl_container, new PersonalNotificationsFragment(), PersonalNotificationsFragment.class.getCanonicalName());
                break;
            case AppConstants.ClassConstants.ON_PROFILE_CLICKED:
                if (fragment != null && !(fragment instanceof ProfileFragment)) {
                    selectBottomNav(false, false, false, true, false);
                    ProfileFragment profileFragment = new ProfileFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_HOME_ACTIVITY);
                    bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.MY_PROFILE);
                    profileFragment.setArguments(bundle);
                    addFragment(R.id.fl_container, profileFragment, ProfileFragment.class.getCanonicalName());
                } else {
                    if (fragment != null && ((ProfileFragment) fragment).getAccountType() == AppConstants.OTHER_USER_PROFILE) {
                        selectBottomNav(false, false, false, true, false);
                        ProfileFragment profileFragment = new ProfileFragment();
                        Bundle bundle = new Bundle();
                        bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_HOME_ACTIVITY);
                        bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.MY_PROFILE);
                        profileFragment.setArguments(bundle);
                        addFragment(R.id.fl_container, profileFragment, ProfileFragment.class.getCanonicalName());
                    }
                }
                break;
        }
    }

    public void selectBottomNav(boolean onHomeSelected, boolean onSearchSelected, boolean onNotificationSelected, boolean onProfileSelected, boolean isOtherSelected) {
        mHomeScreenModel.setIsHomeSelected(onHomeSelected);
        mHomeScreenModel.setIsSearchSelected(onSearchSelected);
        mHomeScreenModel.setIsNotificationSelected(onNotificationSelected);
        mHomeScreenModel.setIsProfileSelected(onProfileSelected);
        mHomeScreenModel.setIsOtherSelected(!isOtherSelected);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void uploadSuccess(MediaBean bean) {
        if (isFromCollab) {
            count++;
            if (count == 5) {
                if (AppUtils.isNetworkAvailable(this))
                    postVideoToServer();
                else
                    Toast.makeText(this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                mActivityHomeBinding.videoUpload.clUpload.setVisibility(View.GONE);
                deleteDirectory();
            }
        } else {
            count++;
            if (count == 2) {
                if (AppUtils.isNetworkAvailable(this))
                    postVideoToServer();
                else
                    Toast.makeText(this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                mActivityHomeBinding.videoUpload.clUpload.setVisibility(View.GONE);
                deleteDirectory();
            }
        }
    }

    private void postVideoToServer() {
        CollabPostModel collabPostModel = mCollabPostModel;
        if (TextUtils.isEmpty(collabPostModel.getCollabAdditionalInfo()) || collabPostModel.getCollabAdditionalInfo().startsWith("{")) {
            collabPostModel.setCollabAdditionalInfo(new Gson().toJson(mAdditionalInfos));
        }
        String hashtagString = mCollabPostModel.getCollabHashtags();
        String[] hashtag = hashtagString.split(" ");
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < hashtag.length; i++) {
            if (i == 0) {
                stringBuilder.append(hashtag[i].replace("#", ""));
            } else {
                stringBuilder.append(hashtag[i].replace("#", ","));
            }
        }
        collabPostModel.setCollabHashtags(stringBuilder.toString());
        collabPostModel.setUserId(DataManager.getInstance().getUserId());
        JSONObject collabObject = new JSONObject();
        try {
            collabObject.put("collab_id", mCollabPostModel.getCollabId());
            collabObject.put("collab_watermark_url", mCollabPostModel.getCollabWatermarkUrl());
            collabObject.put("collab_total_frames", mCollabPostModel.getCollabTotalFrames());
            collabObject.put("collab_title", mCollabPostModel.getCollabTitle());
            collabObject.put("collab_thumbnail_url", mCollabPostModel.getCollabThumbnailUrl());
            collabObject.put("collab_privacy_type_id", mCollabPostModel.getCollabPrivacyTypeId());
            collabObject.put("collab_music_url", mCollabPostModel.getCollabMusicUrl());
            collabObject.put("collab_music_id", mCollabPostModel.getCollabMusicId());
            collabObject.put("collab_duration", mCollabPostModel.getCollabDuration());
            collabObject.put("collab_url", mCollabPostModel.getCollabUrl());
            collabObject.put("collab_hashtags", stringBuilder.toString());
            if(!TextUtils.isEmpty(collabPostModel.getCollabAdditionalInfo())  && !collabPostModel.getCollabAdditionalInfo().startsWith("\"[")) {
                collabObject.put("collab_additional_info", mCollabPostModel.getCollabAdditionalInfo());
                ArrayList<CollabBarBean> additionalInfos = new Gson().fromJson(mCollabPostModel.getCollabAdditionalInfo(), new TypeToken<ArrayList<CollabBarBean>>() {
                }.getType());
                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < additionalInfos.size(); i++) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("start_time", additionalInfos.get(i).getStartTime());
                    jsonObject.put("end_time", additionalInfos.get(i).getEndTime());
                    jsonObject.put("code", additionalInfos.get(i).getCode());
                    jsonObject.put("duration_in_millies", additionalInfos.get(i).getDurationInMillies());
                    jsonObject.put("color_code", additionalInfos.get(i).getColorCode());
                    jsonObject.put("description", additionalInfos.get(i).getDescription());
                    jsonArray.put(jsonObject);
                }
                collabObject.put("collab_additional_info", jsonArray);
            }else{
                collabObject.put("collab_additional_info", new JSONArray(mCollabPostModel.getCollabAdditionalInfo().substring(1, mCollabPostModel.getCollabAdditionalInfo().length()-1).replace("\\", "")));
            }
            JSONArray jsonArray1 = new JSONArray();
            for (int i = 0; i < mCollabPostModel.getCollabVideoArray().size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("video_title", mCollabPostModel.getCollabVideoArray().get(i).getVideoTitle());
                jsonObject.put("video_url", mCollabPostModel.getCollabVideoArray().get(i).getVideoUrl());
                jsonObject.put("video_thumbnail_url", mCollabPostModel.getCollabVideoArray().get(i).getVideoThumbnailUrl());
                jsonObject.put("video_duration", mCollabPostModel.getCollabVideoArray().get(i).getVideoDuration());
                jsonObject.put("video_frame_no", mCollabPostModel.getCollabVideoArray().get(i).getVideoFrameNo());
                jsonObject.put("video_is_mandatory", mCollabPostModel.getCollabVideoArray().get(i).getVideoIsMandatory());
                jsonArray1.put(jsonObject);
            }
            collabObject.put("collab_video_array", jsonArray1);
            collabObject.put("user_id", DataManager.getInstance().getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("JSON_OBJECT", collabObject.toString());
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"), collabObject.toString());
        if (AppUtils.isNetworkAvailable(this))
            mHomeScreenViewModel.hitPostApi(requestBody, String.valueOf(DataManager.getInstance().getUserId()));
        else
            Toast.makeText(this, getResources().getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void uploadFailed(MediaBean bean) {
//        uploadBean(bean);
    }

    @Override
    public void uploadProgress(MediaBean bean) {

    }

    private void deleteDirectory() {
        try {
            File dir = getExternalFilesDir(null);
            FileUtils.deleteDirectory(dir);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void uploadError(Exception e, MediaBean bean) {
        uploadBean(bean);
    }

    private void uploadBean(MediaBean mediaBean) {
        amazonS3.upload(mediaBean);
    }

    @Override
    public void onMergeSuccess() {
        compressFinalFile = mFinalVideo;
        moveAtomFFmpeg();
    }

    private void compress(String input) {
        compressFinalFile = getCompressFile();
        mFFMpegCommands.compressCommand(input, compressFinalFile, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {

            }

            @Override
            public void onProgress(String message) {

            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onStart() {

            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else if (onBackPressedListener != null) {
            onBackPressedListener.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    public void hideTopView() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_container);
        if (fragment instanceof HomeFragment) {
            ((HomeFragment) fragment).hideVisibility();
        }
    }

    public void showTopView() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_container);
        if (fragment instanceof HomeFragment) {
            ((HomeFragment) fragment).showVisibility();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        onBackPressedListener = null;
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mViewHideReciever);
    }

    @Override
    public void onUserProfile(String userId) {
        if (TextUtils.equals(userId, String.valueOf(DataManager.getInstance().getUserId()))) {
            ProfileFragment profileFragment = new ProfileFragment();
            Bundle bundle = new Bundle();
            bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.MY_PROFILE);
            bundle.putString(AppConstants.ClassConstants.USER_ID, userId);
            bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_HOME_ACTIVITY);
            profileFragment.setArguments(bundle);
            selectBottomNav(false, false, false, true, false);
            addFragmentWithBackstack(R.id.fl_container, profileFragment, ProfileFragment.class.getCanonicalName());
        } else {
            ProfileFragment profileFragment = new ProfileFragment();
            Bundle bundle = new Bundle();
            bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.OTHER_USER_PROFILE);
            bundle.putString(AppConstants.ClassConstants.USER_ID, userId);
            bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_HOME_ACTIVITY);
            profileFragment.setArguments(bundle);
            addFragmentWithBackstack(R.id.fl_container, profileFragment, ProfileFragment.class.getCanonicalName());
        }
    }

    public Observer<ChangePasswordModel> getDeleteObserver() {
        return mDeleteObserver;
    }

    public Observer<PrivacyStatusResponseBean> geSavedFeedsObserver() {
        return mSavedVideoObserver;
    }

    private void onChanged(VideoPostResponse videoPostResponse) {
        if (videoPostResponse != null) {
            if (videoPostResponse.getCode() == 200) {
                Toast.makeText(HomeActivity.this, videoPostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                if (isShareable == AppConstants.ClassConstants.SHARE_ON_FACEBOOK) {
                    shareURL(this, videoPostResponse.getResult().getUrl(), "com.facebook.katana");
                } else if (isShareable == AppConstants.ClassConstants.SHARE_ON_INSTAGRAM) {
                    shareURL(this, videoPostResponse.getResult().getUrl(), "com.instagram.android");
                }
            } else if (videoPostResponse.getCode() == 301 || videoPostResponse.getCode() == 302) {
                Toast.makeText(HomeActivity.this, videoPostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                HomeActivity.this.autoLogOut();
            }
        }
    }

    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }

    public void updateProfile() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_container);
        if (fragment != null && fragment instanceof ProfileFragment) {
            ((ProfileFragment) fragment).updateProfile();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mPermissionListener.onPermissionGiven(requestCode, permissions, grantResults);
    }

    public void setOnPermissionListener(PermissionListener permissionListener) {
        this.mPermissionListener = permissionListener;
    }

    private class MyProcessingClass extends ThreadPoolExecutor {

        MyProcessingClass(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
        }

        @Override
        protected void afterExecute(Runnable r, Throwable t) {
            super.afterExecute(r, t);
            runOnUiThread(() -> {
                if (r == null) {

                }
            });
        }
    }
}
