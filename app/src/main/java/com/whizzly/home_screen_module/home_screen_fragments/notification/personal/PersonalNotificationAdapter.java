package com.whizzly.home_screen_module.home_screen_fragments.notification.personal;

import android.content.Context;

import androidx.databinding.DataBindingUtil;
import android.graphics.Color;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.whizzly.R;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.ItemRowNotificationFollowBinding;
import com.whizzly.models.notifications.Result;
import com.whizzly.utils.AppConstants;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class PersonalNotificationAdapter extends RecyclerView.Adapter<PersonalNotificationAdapter.ViewHolder> {
    private OnClickAdapterListener onClickAdapterListener;
    private ViewGroup viewGroup;
    private ItemRowNotificationFollowBinding itemRowNotificationBinding;
    private ArrayList<Result> mNotificationList;
    private Context context;

    public PersonalNotificationAdapter(OnClickAdapterListener onClickAdapterListener, ArrayList<Result> notificationsList, Context context) {
        this.onClickAdapterListener = onClickAdapterListener;
        this.mNotificationList = notificationsList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        itemRowNotificationBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_row_notification_follow, viewGroup, false);
        this.viewGroup = viewGroup;
        return new PersonalNotificationAdapter.ViewHolder(itemRowNotificationBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.itemRowNotificationFollowBinding.tvFollow.setVisibility(View.INVISIBLE);
        viewHolder.itemRowNotificationFollowBinding.ivVideoThumbnail.setVisibility(View.VISIBLE);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_profile_setup_user_placeholder);
        requestOptions.error(R.drawable.ic_profile_setup_user_placeholder);
        Glide.with(viewHolder.itemRowNotificationFollowBinding.ivProfilePic.getContext()).load(mNotificationList.get(i).getSenderProfileImage()).apply(requestOptions.circleCrop()).into(viewHolder.itemRowNotificationFollowBinding.ivProfilePic);
        if (mNotificationList.get(i).getTime() != null) {
            long now = new Date().getTime();
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(mNotificationList.get(i).getTime()*1000);
            CharSequence dateTime = DateUtils.getRelativeTimeSpanString(calendar.getTimeInMillis(), Calendar.getInstance().getTimeInMillis(), DateUtils.SECOND_IN_MILLIS);
            viewHolder.itemRowNotificationFollowBinding.tvTime.setText(dateTime);
        }
        if (mNotificationList.get(i).getEventType().equalsIgnoreCase("follow")) {
            viewHolder.itemRowNotificationFollowBinding.ivVideoThumbnail.setVisibility(View.GONE);
            viewHolder.itemRowNotificationFollowBinding.tvFollow.setVisibility(View.VISIBLE);

            if (mNotificationList.get(i).getIsFollowed() == 1)
                viewHolder.itemRowNotificationFollowBinding.tvFollow.setText(context.getString(R.string.txt_unfollow));
            else
                viewHolder.itemRowNotificationFollowBinding.tvFollow.setText(context.getString(R.string.txt_follow));
            viewHolder.itemRowNotificationFollowBinding.tvNotificationMsg.setText(mNotificationList.get(i).getUser());
            viewHolder.itemRowNotificationFollowBinding.tvNotificationMsg.append(" " + context.getString(R.string.s_started_following_you));

        } else if (mNotificationList.get(i).getEventType().equalsIgnoreCase("collab_like") || mNotificationList.get(i).getEventType().equalsIgnoreCase("video_like")) {
            viewHolder.itemRowNotificationFollowBinding.tvNotificationMsg.setText(mNotificationList.get(i).getUser());
            viewHolder.itemRowNotificationFollowBinding.tvNotificationMsg.append(" " + context.getString(R.string.s_liked_your_video));
            if (mNotificationList.get(i).getLikedCollabThumbnail() != null)
                Glide.with(viewHolder.itemRowNotificationFollowBinding.ivVideoThumbnail.getContext()).load(mNotificationList.get(i).getLikedCollabThumbnail()).into(itemRowNotificationBinding.ivVideoThumbnail);
            if(mNotificationList.get(i).getLikedVideoThumbnail()!=null)
                Glide.with(viewHolder.itemRowNotificationFollowBinding.ivVideoThumbnail.getContext()).load(mNotificationList.get(i).getLikedVideoThumbnail()).into(itemRowNotificationBinding.ivVideoThumbnail);

        } else if (mNotificationList.get(i).getEventType().equalsIgnoreCase("video_comment")||mNotificationList.get(i).getEventType().equalsIgnoreCase("collab_comment")) {
            viewHolder.itemRowNotificationFollowBinding.tvNotificationMsg.setText(mNotificationList.get(i).getUser());
            viewHolder.itemRowNotificationFollowBinding.tvNotificationMsg.append(" " + context.getString(R.string.s_comment_on_your_video));
            if (mNotificationList.get(i).getCommentedCollabThumbnail() != null)
                Glide.with(viewHolder.itemRowNotificationFollowBinding.ivVideoThumbnail.getContext()).load(mNotificationList.get(i).getCommentedCollabThumbnail()).into(itemRowNotificationBinding.ivVideoThumbnail);

        } else if (mNotificationList.get(i).getEventType().equalsIgnoreCase("video_reuse")||mNotificationList.get(i).getEventType().equalsIgnoreCase("collab_reuse")) {
            viewHolder.itemRowNotificationFollowBinding.tvNotificationMsg.setText(mNotificationList.get(i).getUser());
            viewHolder.itemRowNotificationFollowBinding.tvNotificationMsg.append(" " + context.getString(R.string.s_reused_your_video));
            if (mNotificationList.get(i).getLikedCollabThumbnail() != null)
                Glide.with(viewHolder.itemRowNotificationFollowBinding.ivVideoThumbnail.getContext()).load(mNotificationList.get(i).getReusedCollabThumbnail()).into(itemRowNotificationBinding.ivVideoThumbnail);

        }
        setSpanableString(viewHolder.itemRowNotificationFollowBinding.tvNotificationMsg,mNotificationList.get(i),i);

        viewHolder.itemRowNotificationFollowBinding.tvFollow.setOnClickListener(v -> onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.FOLLOW_BUTTON_PRESSED, mNotificationList.get(i), i));
        viewHolder.itemRowNotificationFollowBinding.getRoot().setOnClickListener(v -> {
            onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_NOTIFICATION_CLICKED, mNotificationList.get(i), i);
        });
        viewHolder.itemRowNotificationFollowBinding.ivProfilePic.setOnClickListener(v -> onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.USER_NAME_VIEW_CLICKED,mNotificationList.get(i),i));
    }


    public static final long AVERAGE_MONTH_IN_MILLIS = DateUtils.DAY_IN_MILLIS * 30;

    private String getRelationTime(long time) {
        final long now = new Date().getTime();
        final long delta = now - time;
        long resolution;
        if (delta <= DateUtils.MINUTE_IN_MILLIS) {
            resolution = DateUtils.SECOND_IN_MILLIS;
        } else if (delta <= DateUtils.HOUR_IN_MILLIS) {
            resolution = DateUtils.MINUTE_IN_MILLIS;
        } else if (delta <= DateUtils.DAY_IN_MILLIS) {
            resolution = DateUtils.HOUR_IN_MILLIS;
        } else if (delta <= DateUtils.WEEK_IN_MILLIS) {
            resolution = DateUtils.DAY_IN_MILLIS;
        } else if (delta <= AVERAGE_MONTH_IN_MILLIS) {
            return Integer.toString((int) (delta / DateUtils.WEEK_IN_MILLIS)) + " weeks(s) ago";
        } else if (delta <= DateUtils.YEAR_IN_MILLIS) {
            return Integer.toString((int) (delta / AVERAGE_MONTH_IN_MILLIS)) + " month(s) ago";
        } else {
            return Integer.toString((int) (delta / DateUtils.YEAR_IN_MILLIS)) + " year(s) ago";
        }
        return DateUtils.getRelativeTimeSpanString(time, now, resolution).toString();
    }

    private void setSpanableString(TextView tvNotificationMsg, Result result, int i) {
        String senderName = result.getUser();
        String notificationMessage = tvNotificationMsg.getText().toString();
        SpannableString spannableString = new SpannableString(notificationMessage);

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.USER_NAME_VIEW_CLICKED, result, i);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.bgColor = Color.TRANSPARENT;
                ds.linkColor = Color.TRANSPARENT;
            }
        };

        if (senderName != null) {
            spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.text_color)), notificationMessage.indexOf(senderName), notificationMessage.indexOf(senderName) + senderName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), notificationMessage.indexOf(senderName), notificationMessage.indexOf(senderName) + senderName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(clickableSpan, notificationMessage.indexOf(senderName), notificationMessage.indexOf(senderName) + senderName.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            tvNotificationMsg.setText(spannableString);
            tvNotificationMsg.setMovementMethod(LinkMovementMethod.getInstance());
            tvNotificationMsg.setHighlightColor(Color.TRANSPARENT);
        }
    }


    @Override
    public int getItemCount() {
        return mNotificationList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        ItemRowNotificationFollowBinding itemRowNotificationFollowBinding;

        public ViewHolder(ItemRowNotificationFollowBinding itemRowNotificationBinding) {
            super(itemRowNotificationBinding.getRoot());
            this.itemRowNotificationFollowBinding = itemRowNotificationBinding;
        }

    }
}
