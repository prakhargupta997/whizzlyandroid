package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;


import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.ItemRowSuggestedVideoBinding;
import com.whizzly.models.collab.collab_videos.allvideos.Result;
import com.whizzly.utils.AppConstants;

import java.util.ArrayList;

public class CategoryAllVideosAdapter extends RecyclerView.Adapter<CategoryAllVideosAdapter.ViewHolder> {

    private ItemRowSuggestedVideoBinding itemRowSuggestedVideoBinding;
    private OnClickAdapterListener onClickAdapterListener;
    private ArrayList<Result> mVideoList;
    private int width = 0;
    private ViewGroup viewGroup;


    public CategoryAllVideosAdapter(ArrayList<Result> videoList, OnClickAdapterListener onClickAdapterListener) {
        this.mVideoList = videoList;
        this.onClickAdapterListener = onClickAdapterListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        itemRowSuggestedVideoBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_row_suggested_video, viewGroup, false);
        width = viewGroup.getWidth() / 3;
        return new ViewHolder(itemRowSuggestedVideoBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(mVideoList.get(i));
        RequestOptions requestOptions = new RequestOptions();
        viewHolder.itemRowSuggestedVideoBinding.ivVideo.setMinimumHeight(width);
        viewHolder.itemRowSuggestedVideoBinding.ivVideo.setMinimumWidth(width);
        requestOptions.placeholder(R.drawable.ic_choose_image_gallery);
        Glide.with(itemRowSuggestedVideoBinding.getRoot()).load(mVideoList.get(i).getThumbnailUrl()).apply(requestOptions).into(itemRowSuggestedVideoBinding.ivVideo);
        viewHolder.itemRowSuggestedVideoBinding.ivVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_CATEGORY_VIDEO_CLICKED, mVideoList.get(i), i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mVideoList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ItemRowSuggestedVideoBinding itemRowSuggestedVideoBinding;

        public ViewHolder(@NonNull ItemRowSuggestedVideoBinding itemRowSuggestedVideoBinding) {
            super(itemRowSuggestedVideoBinding.getRoot());
            this.itemRowSuggestedVideoBinding = itemRowSuggestedVideoBinding;
        }

        public void bind(Result result) {
            itemRowSuggestedVideoBinding.setVariable(BR.model, result);
            itemRowSuggestedVideoBinding.executePendingBindings();
        }
    }
}
