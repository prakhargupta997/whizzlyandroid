package com.whizzly.home_screen_module.home_screen_fragments.profile.followers;


import android.app.Activity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.FragmentFollowersBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.interfaces.OnOpenUserProfile;
import com.whizzly.models.followers_following_response.FollowersResponseBean;
import com.whizzly.models.followers_following_response.Result;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;
import com.whizzly.utils.EndlessRecyclerOnScrollListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class BottomSheetFollowersListFragment extends BottomSheetDialogFragment implements OnClickSendListener , OnOpenUserProfile, OnClickAdapterListener {
    private FragmentFollowersBinding mFragmentFollowersBinding;
    private FollowersViewModel mFollowersViewModel;
    private FollowersRecyclerViewAdapter mFollowersAdapter;
    private int page = 1, offset = 20;
    private boolean isForFollowersListing;
    private FollowersModel mModel;
    private Activity mActivity;
    private ArrayList<Result> mFollowersOrFollowingListing;
    private int mCount;
    private String userId;
    private int accountType;
    private int mPosition=0;
    private String listType;


    public BottomSheetFollowersListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFragmentFollowersBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_followers, container, false);
        mActivity = getActivity();
        initVariables();
        getArgumentsDate();
        setRecyclerView();
        mFollowersViewModel = ViewModelProviders.of(this).get(FollowersViewModel.class);
        mFragmentFollowersBinding.setViewModel(mFollowersViewModel);
        mModel = new FollowersModel(mActivity);
        mFollowersViewModel.setModel(mModel);
        mFollowersViewModel.setOnClickSendListener(this);
        mFollowersViewModel.setGenericListeners(((HomeActivity) Objects.requireNonNull(getActivity())).getErrorObserver(), ((HomeActivity) Objects.requireNonNull(getActivity())).getFailureResponseObserver());
        observeliveData();
        return mFragmentFollowersBinding.getRoot();
    }

    private void initVariables() {
        mFollowersOrFollowingListing = new ArrayList<>();
    }


    private void getArgumentsDate() {
        if (getArguments().containsKey(AppConstants.ClassConstants.FOLLOWER_FOLLOWING_RESPONSE) && getArguments().getParcelable(AppConstants.ClassConstants.FOLLOWER_FOLLOWING_RESPONSE) != null) {
            FollowersResponseBean followersResponseBean = getArguments().getParcelable(AppConstants.ClassConstants.FOLLOWER_FOLLOWING_RESPONSE);
            mFollowersOrFollowingListing.addAll(followersResponseBean.getResult());
            //mFollowersAdapter.notifyDataSetChanged();
            page = followersResponseBean.getNextCount();
            userId=followersResponseBean.getOtherUserId();
            accountType=followersResponseBean.getAccountType();
            mCount = followersResponseBean.getCount();
            listType=followersResponseBean.getType();

            if (listType.equalsIgnoreCase(AppConstants.ClassConstants.FOLLOWERS)) {
                isForFollowersListing = true;
                mFragmentFollowersBinding.ivType.setImageResource(R.drawable.ic_profile_followers);
                if (mCount>1)
                mFragmentFollowersBinding.tvCount.setText(String.format("%d %s", mCount,getString(R.string.txt_followers)));
                else
                    mFragmentFollowersBinding.tvCount.setText(String.format("%d %s", mCount,getString(R.string.txt_follower)));


            } else {
                isForFollowersListing = false;
                mFragmentFollowersBinding.ivType.setImageResource(R.drawable.ic_profile_following);
                mFragmentFollowersBinding.tvCount.setText(String.format("%d %s", mCount,getString(R.string.txt_following)));

            }

        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void observeliveData() {
        mFollowersViewModel.getFollowersListliveData().observe(this, new Observer<FollowersResponseBean>() {
            @Override
            public void onChanged(@Nullable FollowersResponseBean response) {
                if (response != null ){
                    if (response.getCode() == 200) {
                        mFollowersOrFollowingListing.clear();
                        page = response.getNextCount();
                        for (int i=0;i<response.getResult().size();i++){
                            if (response.getResult().get(i).getUserStatus()==0)
                                mFollowersOrFollowingListing.add(response.getResult().get(i));
                        }
                        mFollowersAdapter.notifyDataSetChanged();

                    }
                    else if (response.getCode()==301 ||response.getCode()==302){
                        Toast.makeText(mActivity, response.getMessage(), Toast.LENGTH_SHORT).show();
                        ((BaseActivity) Objects.requireNonNull(getActivity())).autoLogOut();
                    }
                    else {
                        Toast.makeText(mActivity, response.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        mFollowersViewModel.getFollowUnfollowLiveData().observe(this, followUnfollowResponseBean -> {
            if (followUnfollowResponseBean != null && followUnfollowResponseBean.getCode() == 200) {
                mFollowersOrFollowingListing.remove(mPosition);
                mFollowersAdapter.notifyItemRemoved(mPosition);
                mFragmentFollowersBinding.tvCount.setText(String.valueOf(mFollowersOrFollowingListing.size()));
                ((HomeActivity)getActivity()).updateProfile();
                if (mFollowersOrFollowingListing.size()==0){
                    dismiss();
                }

            }
            else if (followUnfollowResponseBean.getCode()==301 ||followUnfollowResponseBean.getCode()==302){
                Toast.makeText(mActivity, followUnfollowResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                ((BaseActivity) Objects.requireNonNull(getActivity())).autoLogOut();
            }
            else
                Toast.makeText(mActivity, followUnfollowResponseBean.getMessage(), Toast.LENGTH_SHORT).show();

        });
    }

    private void setRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mFragmentFollowersBinding.rvFollowers.setLayoutManager(linearLayoutManager);
        mFollowersAdapter = new FollowersRecyclerViewAdapter(getActivity(), mFollowersOrFollowingListing,(HomeActivity)getActivity(), this,this,accountType,listType);
        mFragmentFollowersBinding.rvFollowers.setAdapter(mFollowersAdapter);
        mFragmentFollowersBinding.rvFollowers.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                if (page != -1)
                    getFollowerOrFollowingList(page, offset);
            }
        });
    }

    private void getFollowerOrFollowingList(int page, int offset) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        if (accountType==AppConstants.MY_PROFILE)
        hashMap.put(AppConstants.NetworkConstants.PARAM_OTHER_USER_ID, String.valueOf(mFollowersViewModel.getUserId()));
        else
            hashMap.put(AppConstants.NetworkConstants.PARAM_OTHER_USER_ID,userId);

        hashMap.put(AppConstants.NetworkConstants.PAGE, String.valueOf(page));
        hashMap.put(AppConstants.NetworkConstants.LIMIT, String.valueOf(offset));
        if (isForFollowersListing)
            mFollowersViewModel.getFollowersListing(hashMap);
        else
            mFollowersViewModel.getFollowingListing(hashMap);
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_DROP_DOWN_CLICKED:
                dismiss();
                break;
        }
    }

    @Override
    public void onUserProfile(String userId) {
   dismiss();
    }

    @Override
    public void onAdapterClicked(int code, Object obj, int position) {
        if (obj != null && obj instanceof Result) {
            mPosition=position;
            Result  result= (Result) obj;
            mFollowersViewModel.OnUnFollowButtonViewClicked(result.getUserId());
        }
    }
}
