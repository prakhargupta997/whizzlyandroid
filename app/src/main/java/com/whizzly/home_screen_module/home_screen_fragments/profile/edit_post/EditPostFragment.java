package com.whizzly.home_screen_module.home_screen_fragments.profile.edit_post;


import android.app.Activity;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.FragmentEditPostBinding;
import com.whizzly.home_screen_module.home_screen_activities.InnerFragmentsHostActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.feeds_response.Result;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditPostFragment extends Fragment implements OnClickSendListener {
    private FragmentEditPostBinding mFragmentEditPostBinding;
    private EditPostViewModel mEditPostViewModel;
    private Result mResult;
    private EditPostModel mEditPostModel;
    private Activity mActivity;
    private ExoPlayer mExoPlayer;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getArgumnetsData();
    }

    private void getArgumnetsData() {
        if (getArguments() != null && getArguments().getParcelable(AppConstants.ClassConstants.FEED_DATA) != null) {

            mResult = getArguments().getParcelable(AppConstants.ClassConstants.FEED_DATA);
        }

    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mActivity = getActivity();
        mFragmentEditPostBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_post, container, false);
        mEditPostViewModel = ViewModelProviders.of(this).get(EditPostViewModel.class);
        mFragmentEditPostBinding.setViewModel(mEditPostViewModel);
        mEditPostViewModel.setClickListener(this);
        mEditPostModel = new EditPostModel(mActivity);
        mEditPostViewModel.setGenericListeners(((InnerFragmentsHostActivity) mActivity).getErrorObserver(),
                ((InnerFragmentsHostActivity) mActivity).getFailureResponseObserver());
        mEditPostViewModel.setModel(mEditPostModel);
        mFragmentEditPostBinding.setModel(mEditPostModel);
        observeLiveData();
        mFragmentEditPostBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                releasePlayer();
                mActivity.onBackPressed();
            }
        });
        setViews();
        if(DataManager.getInstance().isNotchDevice()){
            mFragmentEditPostBinding.toolbar.view.setVisibility(View.VISIBLE);
        }else{
            mFragmentEditPostBinding.toolbar.view.setVisibility(View.GONE);
        }
        setListeners();
        return mFragmentEditPostBinding.getRoot();
    }

    private void setListeners() {
        mFragmentEditPostBinding.etHashtags.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b)
                    hideKeyboard(view);
                else {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(mFragmentEditPostBinding.etHashtags, InputMethodManager.SHOW_IMPLICIT);
                    String text = mFragmentEditPostBinding.etHashtags.getText().toString();
                    if (text.trim().length() != 0) {
                        char last = text.trim().charAt(text.length() - 1);
                        if (last != '#')
                            mFragmentEditPostBinding.etHashtags.setText(text + " #");
                    } else
                        mFragmentEditPostBinding.etHashtags.setText("#");

                    mFragmentEditPostBinding.etHashtags.setSelection(mFragmentEditPostBinding.etHashtags.getText().toString().length());
                }
            }
        });

        mFragmentEditPostBinding.etWriteCaption.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b)
                    hideKeyboard(view);
            }
        });

        mFragmentEditPostBinding.etHashtags.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                char last = s.charAt(s.length() - 1);
                if (last == ' ' || last == '\n') {
                    mFragmentEditPostBinding.etHashtags.setText(String.format("%s#", s));
                    mFragmentEditPostBinding.etHashtags.setSelection(mFragmentEditPostBinding.etHashtags.getText().toString().length());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }



    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    private void observeLiveData() {
        mEditPostViewModel.getEditFeedsLiveData().observe(this, editPostResponse -> {
            if (editPostResponse != null) {
                if (editPostResponse.getCode().equals("301") || editPostResponse.getCode().equals("302")) {
                    Toast.makeText(mActivity, editPostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) Objects.requireNonNull(mActivity)).autoLogOut();

                }else if (editPostResponse.getCode().equalsIgnoreCase("200")) {
                    mFragmentEditPostBinding.pbMain.setVisibility(View.GONE);
                    releasePlayer();
                    Toast.makeText(mActivity, getString(R.string.s_feeds_has_been_changed_successfully), Toast.LENGTH_SHORT).show();
                    mActivity.onBackPressed();
                }
            }

        });
    }

    private void setViews() {
        mFragmentEditPostBinding.toolbar.tvToolbarText.setText(getResources().getString(R.string.txt_edit_post));
        mFragmentEditPostBinding.toolbar.ivBack.setVisibility(View.VISIBLE);
        mEditPostModel.setVideoTitle(mResult.getVideoDescription());
        String hashtag = mResult.getVideoHastags();
        String[] hashtags = hashtag.split(",");
        StringBuilder hashtagBuillder = new StringBuilder();
        for (int i = 0; i < hashtags.length; i++) {
            if (i == 0) {
                hashtagBuillder.append("#"+hashtags[i]);
            }else {
                hashtagBuillder.append(" #"+hashtags[i]);
            }
        }
        mEditPostModel.setVideoHashTag(hashtagBuillder.toString());
        if (mResult.getMusicDescription() != null &&mResult.getMusicTitle()!=null&&!mResult.getMusicDescription().equals("")) {
            mFragmentEditPostBinding.tvSong.setText(String.format("%s - %s", mResult.getMusicTitle(), mResult.getMusicDescription()));;
        } else {
            mFragmentEditPostBinding.viewSong.setVisibility(View.GONE);
            mFragmentEditPostBinding.tvSong.setVisibility(View.GONE);
            mFragmentEditPostBinding.tvLabelSong.setVisibility(View.GONE);
        }
        setExoplayer(true);
        setThumbnailUrl(mResult.getThumbnailUrl());
    }

    private void setThumbnailUrl(String thumbnailUrl) {
        mFragmentEditPostBinding.ivVideoThumbnail.setVisibility(View.VISIBLE);
        RequestOptions requestOptions1 = new RequestOptions();
        requestOptions1.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions1.skipMemoryCache(true);
        Glide.with(this).
                load(thumbnailUrl)
                .apply(requestOptions1).into(mFragmentEditPostBinding.ivVideoThumbnail);

    }


    private void setExoplayer(boolean playWhenReady) {
        if (mExoPlayer == null) {
            TrackSelection.Factory adaptiveTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(new DefaultBandwidthMeter());

            mExoPlayer = ExoPlayerFactory.newSimpleInstance(getActivity(),
                    new DefaultRenderersFactory(getActivity()),
                    new DefaultTrackSelector(adaptiveTrackSelectionFactory),
                    new DefaultLoadControl());

            mExoPlayer.setPlayWhenReady(playWhenReady);
            mExoPlayer.setRepeatMode(Player.REPEAT_MODE_ONE);
            MediaSource mediaSource = buildMediaSource(Uri.parse(mResult.getUrl()));
            mExoPlayer.addListener(new Player.DefaultEventListener() {
                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    super.onPlayerStateChanged(playWhenReady, playbackState);
                    switch (playbackState) {
                        case ExoPlayer.STATE_BUFFERING:
                            mFragmentEditPostBinding.pbMain.setVisibility(View.VISIBLE);
                            break;
                        case ExoPlayer.STATE_READY:
                            mFragmentEditPostBinding.pbMain.setVisibility(View.GONE);
                            mFragmentEditPostBinding.ivVideoThumbnail.setVisibility(View.GONE);
                            break;
                    }
                }
            });
            mExoPlayer.prepare(mediaSource, true, false);
            mFragmentEditPostBinding.pvVideo.setPlayer(mExoPlayer);
        }
    }

    private MediaSource buildMediaSource(Uri uri) {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getActivity(),
                Util.getUserAgent(getActivity(), getString(R.string.app_name)), bandwidthMeter);
        return new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri);
    }

    @Override
    public void onResume() {
        super.onResume();
        // setExoplayer(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23 && mExoPlayer == null && mResult.getUrl() != null) {
            setExoplayer(true);
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }

    private void releasePlayer() {
        if (mExoPlayer != null) {
            mExoPlayer.setPlayWhenReady(false);
            mExoPlayer.stop();
            mExoPlayer.release();
            mExoPlayer = null;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }

    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_UPDATE_VIEW_CLICKED:
                if (AppUtils.isNetworkAvailable(mActivity)) {
                    mFragmentEditPostBinding.pbMain.setVisibility(View.VISIBLE);
                    mEditPostViewModel.hitEditPostApi(mResult.getId(), mResult.getType());
                    // mFragmentEditPostBinding.flProgressBar.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(mActivity, getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
                }
                /*if (mEditPostModel.isValidTitle() == null && mEditPostModel.isValidHashtag() == null) {

                } else {
                    if (mEditPostModel.isValidTitle() != null) {
                        showToast(mEditPostModel.isValidTitle());
                    } else if (mEditPostModel.isValidHashtag() != null) {
                        showToast(mEditPostModel.isValidHashtag());
                    }
                }*/
                break;
        }

    }

    private void showToast(String message) {
        Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
    }

}
