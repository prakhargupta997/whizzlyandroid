package com.whizzly.home_screen_module.home_screen_fragments.videos;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.search.search_video.VideoSearchResponseBean;


public class VideosViewModel extends ViewModel{
    private RichMediatorLiveData<VideoSearchResponseBean> mVideosLivedData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private VideosModel mVideosModel;


    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver)
    {
        this.errorObserver=errorObserver;
        this.failureResponseObserver=failureResponseObserver;
        initLiveData();
    }
    public void setModel(VideosModel videosModel){
        this.mVideosModel=videosModel;

    }


    private void initLiveData() {
        if (mVideosLivedData == null) {
            mVideosLivedData = new RichMediatorLiveData<VideoSearchResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
}
public RichMediatorLiveData<VideoSearchResponseBean> getVideoListingLiveData()
{
  return mVideosLivedData;
}

    public void getVideoListing(String mSearch, int mPage) {
      mVideosModel.hitVideoListingApi(mSearch,mPage,mVideosLivedData);
    }
}
