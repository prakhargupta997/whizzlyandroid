package com.whizzly.home_screen_module.home_screen_fragments.profile.followers;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.followUnfollow.FollowUnfollowResponseBean;
import com.whizzly.models.followers_following_response.FollowersResponseBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

/**
 * Created by Rahul Mishra on 24/4/19.
 */
public class FollowersViewModel extends ViewModel {
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private RichMediatorLiveData<FollowersResponseBean> mFollowersLisrResponseLiveData;
    private FollowersModel mFollowersModel;
    private OnClickSendListener onClickSendListener;
    private RichMediatorLiveData<FollowUnfollowResponseBean> mFollowUnfollowLiveData;

    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureObserver;
        initializeLiveData();
    }

    private void initializeLiveData() {
        if (mFollowersLisrResponseLiveData == null) {
            mFollowersLisrResponseLiveData = new RichMediatorLiveData<FollowersResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
        if (mFollowUnfollowLiveData==null){
            mFollowUnfollowLiveData=new RichMediatorLiveData<FollowUnfollowResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
    }

    public int getUserId() {
        return DataManager.getInstance().getUserId();

    }

    public void getFollowersListing(HashMap<String, String> hashMap) {
        mFollowersModel.getFollowersListing(hashMap, mFollowersLisrResponseLiveData);

    }

    public RichMediatorLiveData<FollowersResponseBean> getFollowersListliveData() {
        return mFollowersLisrResponseLiveData;
    }

    public void getFollowingListing(HashMap<String, String> hashMap) {
        mFollowersModel.getFollowingListing(hashMap, mFollowersLisrResponseLiveData);
    }

    public void setModel(FollowersModel mModel) {
        this.mFollowersModel = mModel;
    }

    public void onDropDownClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_DROP_DOWN_CLICKED);

    }

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void OnUnFollowButtonViewClicked(Integer affectedUserId) {
        DataManager.getInstance().hitFollowUnfollowApi(getFollowUnfollowData(String.valueOf(affectedUserId))).enqueue(new NetworkCallback<FollowUnfollowResponseBean>() {
            @Override
            public void onSuccess(FollowUnfollowResponseBean followUnfollowResponseBean) {
                mFollowUnfollowLiveData.setValue(followUnfollowResponseBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mFollowUnfollowLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mFollowUnfollowLiveData.setError(t);
            }
        });
    }

    private HashMap<String, String> getFollowUnfollowData(String affectedUserId) {
        HashMap<String, String> params = new HashMap<>();
        String userId = String.valueOf(DataManager.getInstance().getUserId());
        params.put(AppConstants.NetworkConstants.PARAM_USER_ID, userId);
        params.put(AppConstants.NetworkConstants.PARAM_FOLLOWING_ID, affectedUserId);
            params.put(AppConstants.NetworkConstants.PARAM_STATUS, String.valueOf(0));

        return params;
    }

    public RichMediatorLiveData<FollowUnfollowResponseBean> getFollowUnfollowLiveData() {
        return mFollowUnfollowLiveData;
    }
}
