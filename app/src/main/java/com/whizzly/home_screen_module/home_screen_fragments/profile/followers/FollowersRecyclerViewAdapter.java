package com.whizzly.home_screen_module.home_screen_fragments.profile.followers;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.ItemRowLikeReuseListItemBinding;
import com.whizzly.interfaces.OnOpenUserProfile;
import com.whizzly.models.followers_following_response.Result;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.ArrayList;


public class FollowersRecyclerViewAdapter extends RecyclerView.Adapter<FollowersRecyclerViewAdapter.ViewHolder> {

    private ItemRowLikeReuseListItemBinding itemRowLikeReuseListItemBinding;
    private Context mContext;
    private ArrayList<Result> mList;
    private OnOpenUserProfile onOpenUserProfile;
    private OnOpenUserProfile openUserProfile;
    private OnClickAdapterListener mOnClickAdapterListener;
    private int mAccountType;
    private final String mListType;




    public FollowersRecyclerViewAdapter(Context mContext, ArrayList<Result> mFollowersOrFollowingListing, OnOpenUserProfile onOpenUserProfile, OnOpenUserProfile openUserProfile, OnClickAdapterListener onClickAdapterListener, int accountType, String listType) {
        this.mContext = mContext;
        this.mList=mFollowersOrFollowingListing;
        this.onOpenUserProfile=onOpenUserProfile;
        this.openUserProfile=openUserProfile;
        this.mOnClickAdapterListener=onClickAdapterListener;
        this.mAccountType=accountType;
        this.mListType=listType;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        itemRowLikeReuseListItemBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.item_row_like_reuse_list_item, viewGroup, false);
        return new ViewHolder(itemRowLikeReuseListItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(mList.get(i));
        if (mAccountType==AppConstants.MY_PROFILE && mListType.equalsIgnoreCase(AppConstants.ClassConstants.FOLLOWING))
            viewHolder.itemRowLikeReuseListItemBinding.tvUnfollow.setVisibility(View.VISIBLE);
        else
            viewHolder.itemRowLikeReuseListItemBinding.tvUnfollow.setVisibility(View.GONE);

        if (mList.get(i).getUserStatus() == 0) {
            viewHolder.itemRowLikeReuseListItemBinding.tvUserFirstName.setText(mList.get(i).getFirstName());
            viewHolder.itemRowLikeReuseListItemBinding.tvUserName.setText(mList.get(i).getUserName());
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.circleCrop();
            Glide.with(mContext).load(mList.get(i).getProfileImage()).apply(requestOptions).apply(requestOptions.placeholder(R.drawable.ic_feed_user_pic_placeholder)).into(viewHolder.itemRowLikeReuseListItemBinding.ivUserProfileImage);
            viewHolder.itemRowLikeReuseListItemBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mList.get(i).getUserId()!= DataManager.getInstance().getUserId()) {
                        onOpenUserProfile.onUserProfile(String.valueOf(mList.get(i).getUserId()));
                        openUserProfile.onUserProfile(String.valueOf(mList.get(i).getUserId()));
                    }
                }
            });
            viewHolder.itemRowLikeReuseListItemBinding.tvUnfollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.UNFOLLOW_BUTTON_PRESSED, mList.get(i), i);
                }
            });
        }
        else {
            viewHolder.itemRowLikeReuseListItemBinding.tvUserFirstName.setText(mContext.getString(R.string.s_unknown));
            viewHolder.itemRowLikeReuseListItemBinding.tvUserName.setText(mContext.getString(R.string.s_unknown));
            viewHolder.itemRowLikeReuseListItemBinding.ivUserProfileImage.setImageResource(R.drawable.ic_profile_setup_user_placeholder);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ItemRowLikeReuseListItemBinding itemRowLikeReuseListItemBinding;
        ViewHolder(@NonNull ItemRowLikeReuseListItemBinding itemRowLikeReuseListItemBinding) {
            super(itemRowLikeReuseListItemBinding.getRoot());
            this.itemRowLikeReuseListItemBinding = itemRowLikeReuseListItemBinding;
        }

        public void bind(Result result){
            itemRowLikeReuseListItemBinding.setVariable(BR.model, result);
            itemRowLikeReuseListItemBinding.executePendingBindings();
        }
    }
}

