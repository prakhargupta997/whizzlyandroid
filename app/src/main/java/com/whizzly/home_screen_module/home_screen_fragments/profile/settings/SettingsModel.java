package com.whizzly.home_screen_module.home_screen_fragments.profile.settings;

import android.content.Context;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableField;
import android.widget.Toast;


import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.forgot_password.ResetPasswordBean;
import com.whizzly.models.submit_profile.SubmitProfileBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class SettingsModel extends BaseObservable {

    private Context context;
    public ObservableField<Boolean> isNotificationOn = new ObservableField<>();

    public void setIsNotificationOn(boolean isNotificationOn) {
        this.isNotificationOn.set(isNotificationOn);
        notifyPropertyChanged(BR.isNotificationOn);
    }

    @Bindable
    boolean getIsNotificationOn()
    {
        return this.isNotificationOn.get();
    }

    public SettingsModel(Context context) {
        this.context = context;
    }

    public HashMap<String, String> getNotificationStatusData(String userId) {

        HashMap<String, String> notificationStatus = new HashMap<>();

        notificationStatus.put(AppConstants.NetworkConstants.PARAM_IS_NOTIFICATION_ON, getIsNotificationOn() ? "1" : "0");
        notificationStatus.put(AppConstants.NetworkConstants.PARAM_USER_ID, userId);

        return notificationStatus;
    }


    public void hitApiToChangeNotificationOnStatus(final RichMediatorLiveData<SubmitProfileBean> submitProfileBeanRichMediatorLiveData, HashMap<String, String> submitProfile, boolean isChecked) {
        if (AppUtils.isNetworkAvailable(context)) {

            DataManager.getInstance().hitSubmitProfile(submitProfile).enqueue(new NetworkCallback<SubmitProfileBean>() {
                @Override
                public void onSuccess(SubmitProfileBean submitProfileBean) {
                    if (isChecked)
                    DataManager.getInstance().setNotificationStatus(AppConstants.ClassConstants.NOTIFICATION_ENABLE);
                    else
                        DataManager.getInstance().setNotificationStatus(AppConstants.ClassConstants.NOTIFICATION_DISABLE);

                    submitProfileBeanRichMediatorLiveData.setValue(submitProfileBean);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    submitProfileBeanRichMediatorLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    submitProfileBeanRichMediatorLiveData.setError(t);
                }
            });

        } else {
            Toast.makeText(context, context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    public void hitLogOutApi(RichMediatorLiveData<ResetPasswordBean> logOutMediatorLiveData,String userId) {
        if (AppUtils.isNetworkAvailable(context)) {
            DataManager.getInstance().hitLogOutApi(userId, DataManager.getInstance().getLanguage()).enqueue(new NetworkCallback<ResetPasswordBean>() {
                @Override
                public void onSuccess(ResetPasswordBean submitProfileBean) {
                    logOutMediatorLiveData.setValue(submitProfileBean);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    logOutMediatorLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    logOutMediatorLiveData.setError(t);
                }
            });

        } else {
            Toast.makeText(context, context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }
}
