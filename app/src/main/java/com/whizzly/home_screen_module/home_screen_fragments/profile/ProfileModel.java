package com.whizzly.home_screen_module.home_screen_fragments.profile;

import android.content.Context;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;

import com.whizzly.BR;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.blockedUsers.BlockedUsersResponseBean;
import com.whizzly.models.followUnfollow.FollowUnfollowResponseBean;
import com.whizzly.models.followers_following_response.FollowersResponseBean;
import com.whizzly.models.profile.ProfileResponseBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class ProfileModel extends BaseObservable {

    private final Context context;
    public ObservableField<ProfileResponseBean> profileData = new ObservableField<>();
    public ObservableField<Integer> accountType = new ObservableField<>();

    ProfileModel(Context context) {
        this.context = context;
    }

    @Bindable
    public ProfileResponseBean getProfileData() {
        return profileData.get();
    }

    public void setProfileData(ProfileResponseBean profileData) {
        this.profileData.set(profileData);
        notifyPropertyChanged(BR.profileData);
    }

    @Bindable
    public int getAccountType() {
        return accountType.get();
    }

    public void setAccountType(int accountType) {
        this.accountType.set(accountType);
    }

    public void hitApiToFetchOtherUserProfile(String userId, RichMediatorLiveData<ProfileResponseBean> mProfileResponseBeanRichMediatorLiveData) {

        //TODO: hit api and fetch data
        DataManager.getInstance().getProfile(userId, DataManager.getInstance().getLanguage()).enqueue(new NetworkCallback<ProfileResponseBean>() {
            @Override
            public void onSuccess(ProfileResponseBean profileResponseBean) {
                mProfileResponseBeanRichMediatorLiveData.setValue(profileResponseBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {

                mProfileResponseBeanRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {

                mProfileResponseBeanRichMediatorLiveData.setError(t);

            }
        });

    }

    public void hitApiToFetchMyProfile(RichMediatorLiveData<ProfileResponseBean> mProfileResponseBeanRichMediatorLiveData) {

        //TODO: hit api and fetch data
        DataManager.getInstance().getProfile("", DataManager.getInstance().getLanguage()).enqueue(new NetworkCallback<ProfileResponseBean>() {
            @Override
            public void onSuccess(ProfileResponseBean profileResponseBean) {
                mProfileResponseBeanRichMediatorLiveData.setValue(profileResponseBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {

                mProfileResponseBeanRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {

                mProfileResponseBeanRichMediatorLiveData.setError(t);

            }
        });
    }

    public void hitApiToBlockUnblockUser(RichMediatorLiveData<BlockedUsersResponseBean> blockedUsersRichMediatorLiveData, String affectedUserId, String blockAction) {

        if (AppUtils.isNetworkAvailable(context)) {

            DataManager.getInstance().hitBlockUnblockUserApi(getBlockUnblockData(affectedUserId, blockAction)).enqueue(new NetworkCallback<BlockedUsersResponseBean>() {
                @Override
                public void onSuccess(BlockedUsersResponseBean blockedUsersBean) {
                    blockedUsersRichMediatorLiveData.setValue(blockedUsersBean);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    blockedUsersRichMediatorLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    blockedUsersRichMediatorLiveData.setError(t);
                }
            });

        }
    }

    public void hitApiToFollowUnfollow(RichMediatorLiveData<FollowUnfollowResponseBean> mFollowUnfollowResponseBeanRichMediatorLiveData, String affectedUserId) {
        if (AppUtils.isNetworkAvailable(context)) {

            DataManager.getInstance().hitFollowUnfollowApi(getFollowUnfollowData(affectedUserId)).enqueue(new NetworkCallback<FollowUnfollowResponseBean>() {
                @Override
                public void onSuccess(FollowUnfollowResponseBean blockedUsersBean) {

                    mFollowUnfollowResponseBeanRichMediatorLiveData.setValue(blockedUsersBean);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    mFollowUnfollowResponseBeanRichMediatorLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    mFollowUnfollowResponseBeanRichMediatorLiveData.setError(t);
                }
            });

        }
    }

    public void toggleFollowStatus() {

        ProfileResponseBean profileInfo = profileData.get();

        int currentFollowStatus = profileInfo.getResult().getIsFollowing();

        if (currentFollowStatus == 1) {
            currentFollowStatus = 0;
        } else {
            currentFollowStatus = 1;
        }

        profileInfo.getResult().setIsFollowing(currentFollowStatus);

        setProfileData(profileInfo);
    }

    private HashMap<String, String> getFollowUnfollowData(String affectedUserId) {
        HashMap<String, String> params = new HashMap<>();
        String userId = String.valueOf(DataManager.getInstance().getUserId());
        int status = 0;
        if (profileData.get().getResult().getIsFollowing() == 0) {
            status = 1;
        }
        params.put(AppConstants.NetworkConstants.PARAM_USER_ID, userId);
        params.put(AppConstants.NetworkConstants.PARAM_FOLLOWING_ID, affectedUserId);
        params.put(AppConstants.NetworkConstants.PARAM_STATUS, String.valueOf(status));

        return params;
    }

    private HashMap<String, String> getBlockUnblockData(String affectedUserId, String blockAction) {

        HashMap<String, String> params = new HashMap<>();

        String userId = String.valueOf(DataManager.getInstance().getUserId());

        params.put(AppConstants.NetworkConstants.PARAM_USER_ID, userId);
        params.put(AppConstants.NetworkConstants.PARAM_AFFECTED_USER_ID, affectedUserId);
        params.put(AppConstants.NetworkConstants.PARAM_BLOCK_ACTION, blockAction);
        params.put(AppConstants.NetworkConstants.PARAM_REASON, "demo reason");


        return params;
    }

    public void getFollowingListing(HashMap<String, String> hashMap, RichMediatorLiveData<FollowersResponseBean> mFollowingListResponseLiveData) {
        DataManager.getInstance().getFollowingListing(hashMap).enqueue(new NetworkCallback<FollowersResponseBean>() {
            @Override
            public void onSuccess(FollowersResponseBean followersResponseBean) {
                mFollowingListResponseLiveData.setValue(followersResponseBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {

                mFollowingListResponseLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {

                mFollowingListResponseLiveData.setError(t);

            }
        });
    }

    public void getFollowersListing(HashMap<String, String> hashMap, RichMediatorLiveData<FollowersResponseBean> mFollowersListResponseLiveData) {
        DataManager.getInstance().getFollowersListing(hashMap).enqueue(new NetworkCallback<FollowersResponseBean>() {
            @Override
            public void onSuccess(FollowersResponseBean followersResponseBean) {
                mFollowersListResponseLiveData.setValue(followersResponseBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {

                mFollowersListResponseLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {

                mFollowersListResponseLiveData.setError(t);

            }
        });
    }
}
