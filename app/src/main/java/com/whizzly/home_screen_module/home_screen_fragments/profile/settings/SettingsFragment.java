package com.whizzly.home_screen_module.home_screen_fragments.profile.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.jaychang.sa.facebook.SimpleAuth;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.FragmentSettingsBinding;
import com.whizzly.home_screen_module.home_screen_activities.InnerFragmentsHostActivity;
import com.whizzly.home_screen_module.home_screen_fragments.profile.settings.blocked_users.BlockedUserFragment;
import com.whizzly.home_screen_module.home_screen_fragments.profile.settings.change_language.ChangeLanguageFragment;
import com.whizzly.home_screen_module.home_screen_fragments.profile.settings.change_password.ChangePasswordFragment;
import com.whizzly.home_screen_module.home_screen_fragments.profile.settings.suppot_center.SupportCenterDialogFragment;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.login_signup_module.login_screen.LoginActivity;
import com.whizzly.models.forgot_password.ResetPasswordBean;
import com.whizzly.models.submit_profile.SubmitProfileBean;
import com.whizzly.staticpages.StaticPagesActivity;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class SettingsFragment extends Fragment implements OnClickSendListener, CompoundButton.OnCheckedChangeListener {

    private Activity mActivity;
    private FragmentSettingsBinding mFragmentSettingsBinding;
    private SettingsViewModel mSettingsViewModel;
    private SettingsModel mSettingsModel;
    private Bundle bundle;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mActivity = getActivity();
        mFragmentSettingsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);
        mSettingsViewModel = ViewModelProviders.of(this).get(SettingsViewModel.class);
        mFragmentSettingsBinding.setViewModel(mSettingsViewModel);
        mSettingsModel = new SettingsModel(mActivity);
        mSettingsModel.setIsNotificationOn(true);
        mFragmentSettingsBinding.setModel(mSettingsModel);
        mSettingsViewModel.setOnClickSendListener(this);
        mSettingsViewModel.setModel(mSettingsModel);
        if(DataManager.getInstance().isNotchDevice()){
            mFragmentSettingsBinding.toolbar.view.setVisibility(View.VISIBLE);
        }else{
            mFragmentSettingsBinding.toolbar.view.setVisibility(View.GONE);
        }
        mFragmentSettingsBinding.cbNotification.setOnCheckedChangeListener(this);
        mSettingsViewModel.setGenericListeners(((InnerFragmentsHostActivity) mActivity).getErrorObserver(), ((InnerFragmentsHostActivity) mActivity).getFailureResponseObserver());
        setViews();
        getVersionNumber();
        mSettingsViewModel.getNotificationStatusMediatorLiveData().observe(this, new Observer<SubmitProfileBean>() {
            @Override
            public void onChanged(@Nullable SubmitProfileBean submitProfileBean) {
                if (submitProfileBean != null && submitProfileBean.getCode() == 200) {

                }
            }
        });
        observeLogOutLiveData();


        return mFragmentSettingsBinding.getRoot();
    }

    private void getVersionNumber() {
        try {
            PackageManager manager = getActivity().getPackageManager();
            PackageInfo info = manager.getPackageInfo(getActivity().getPackageName(), PackageManager.GET_ACTIVITIES);
            mFragmentSettingsBinding.tvAppVersion.setText("App Version: " + info.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void setViews() {
        mFragmentSettingsBinding.toolbar.tvToolbarText.setText(getString(R.string.txt_settings));
        mFragmentSettingsBinding.toolbar.ivBack.setVisibility(View.VISIBLE);
        if (DataManager.getInstance().getNotificationStatus() != null && DataManager.getInstance().getNotificationStatus().equalsIgnoreCase(AppConstants.ClassConstants.NOTIFICATION_ENABLE))
            mFragmentSettingsBinding.cbNotification.setChecked(true);
        else
            mFragmentSettingsBinding.cbNotification.setChecked(false);


        mFragmentSettingsBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });

    }

    private void observeLogOutLiveData() {
        mSettingsViewModel.getLogOutMediatorLiveData().observe(this, new Observer<ResetPasswordBean>() {
            @Override
            public void onChanged(@Nullable ResetPasswordBean resetPasswordBean) {
                if (resetPasswordBean != null) {
                    if (resetPasswordBean != null && resetPasswordBean.getCode() == 200) {
                        Objects.requireNonNull(getActivity()).finishAffinity();
                        DataManager.getInstance().clearPrefernces();
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.putExtra(AppConstants.ClassConstants.ON_LOGOUT_CLICKED, "YES");
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else if (resetPasswordBean.getCode() == 301 || resetPasswordBean.getCode() == 302) {
                        Toast.makeText(mActivity, resetPasswordBean.getMessage(), Toast.LENGTH_SHORT).show();
                        ((BaseActivity) Objects.requireNonNull(mActivity)).autoLogOut();
                    }
                }

            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onClickSend(int code) {
        String userId = String.valueOf(mSettingsViewModel.getUserId());
        switch (code) {
            case AppConstants.ClassConstants.SUGGESTED_BACK_PRESSED:
                mActivity.onBackPressed();
                break;
            case AppConstants.ClassConstants.CHANGE_PASSWORD_CLICKED:
                ChangePasswordFragment changePasswordFragment = new ChangePasswordFragment();

                ((InnerFragmentsHostActivity) mActivity).addFragmentWithBackstack(R.id.fl_container_inner, changePasswordFragment, ChangePasswordFragment.class.getSimpleName());
                break;
            case AppConstants.ClassConstants.BLOCKED_USERS_CLICKED:
                BlockedUserFragment blockedUserFragment = new BlockedUserFragment();

                ((InnerFragmentsHostActivity) mActivity).addFragmentWithBackstack(R.id.fl_container_inner, blockedUserFragment, ChangePasswordFragment.class.getSimpleName());
                break;
            case AppConstants.ClassConstants.CHANGE_LANGUAGE_CLICKED:
                ChangeLanguageFragment changeLanguageFragment = new ChangeLanguageFragment();
                ((InnerFragmentsHostActivity) mActivity).addFragmentWithBackstack(R.id.fl_container_inner, changeLanguageFragment, ChangeLanguageFragment.class.getSimpleName());

                break;
            case AppConstants.ClassConstants.SUPPORT_CENTER_CLICKED:
                DialogFragment supportDialogFragment = new SupportCenterDialogFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.SUPPORT_CENTER_CLICKED);
                supportDialogFragment.setArguments(bundle);
                supportDialogFragment.show(((InnerFragmentsHostActivity) mActivity).getSupportFragmentManager(), SupportCenterDialogFragment.class.getCanonicalName());
                break;
            case AppConstants.ClassConstants.SUGGESTION_CLICKED:
                DialogFragment suggestionDialogFragment = new SupportCenterDialogFragment();
                Bundle suggestionBundle = new Bundle();
                suggestionBundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.SUGGESTION_CLICKED);
                suggestionDialogFragment.setArguments(suggestionBundle);
                suggestionDialogFragment.show(((InnerFragmentsHostActivity) mActivity).getSupportFragmentManager(), SupportCenterDialogFragment.class.getCanonicalName());
                break;
            case AppConstants.ClassConstants.LOG_OUT_CLICKED:
                logout();
                break;
        /*    case AppConstants.ClassConstants.NOTIFICATION_TOGGLED:
                mFragmentSettingsBinding.cbNotification.setEnabled(false);
                mSettingsModel.hitApiToChangeNotificationOnStatus(mSettingsViewModel.getNotificationStatusMediatorLiveData(), mSettingsModel.getNotificationStatusData(userId));
                break;*/
            case AppConstants.ClassConstants.ON_TERMS_AND_CONDITION_VIEW_CLCIKED:
                Intent intent = new Intent(getActivity(), StaticPagesActivity.class);
                intent.putExtra(AppConstants.ClassConstants.IS_FOR, AppConstants.ClassConstants.TERMS_AND_CONDITION);
                startActivity(intent);
                break;
            case AppConstants.ClassConstants.ON_PRIVACY_POLICY_VIEW_CLICKED:
                Intent pricyPolicyIntent = new Intent(getActivity(), StaticPagesActivity.class);
                pricyPolicyIntent.putExtra(AppConstants.ClassConstants.IS_FOR, AppConstants.ClassConstants.PRIVACY_POLICY);
                startActivity(pricyPolicyIntent);
                break;

            case AppConstants.ClassConstants.ON_RANKING_CLICKED:
                RankingFragment rankingFragment = new RankingFragment();
                Bundle  bundle1 = new Bundle();
                bundle1.putInt(AppConstants.ClassConstants.RATING_COUNT, getArguments().getInt(AppConstants.ClassConstants.RATING_COUNT));
                rankingFragment.setArguments(bundle1);
                ((InnerFragmentsHostActivity) mActivity).addFragmentWithBackstack(R.id.fl_container_inner, rankingFragment, ChangeLanguageFragment.class.getSimpleName());
                break;
        }
    }

    private void logout() {
        new AlertDialog.Builder(getActivity())
                .setMessage(R.string.txt_logout_dialog)
                .setPositiveButton(R.string.txt_log_out, (dialogInterface, i) -> {

                    logoutApiHit(dialogInterface);
                })
                .setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss())
                .create().show();
    }

    private void logoutApiHit(DialogInterface dialogInterface) {
        switch (DataManager.getInstance().getLoginOrSignUpTyp()) {
            case 2:
                SimpleAuth.disconnectFacebook();
                break;
            case 3:
                GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .build();
                GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
                mGoogleSignInClient.signOut();
                break;
            case 4:
                com.jaychang.sa.instagram.SimpleAuth.disconnectInstagram();
                break;
        }
        mSettingsModel.hitLogOutApi(mSettingsViewModel.getLogOutMediatorLiveData(), String.valueOf(mSettingsViewModel.getUserId()));
        dialogInterface.dismiss();
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        String userId = String.valueOf(mSettingsViewModel.getUserId());
        mSettingsModel.hitApiToChangeNotificationOnStatus(mSettingsViewModel.getNotificationStatusMediatorLiveData(), mSettingsModel.getNotificationStatusData(userId), isChecked);

    }
}
