package com.whizzly.home_screen_module.home_screen_fragments.profile.posted;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.change_password.ChangePasswordModel;
import com.whizzly.models.privacy_status.PrivacyStatusResponseBean;
import com.whizzly.models.profileVideo.ProfileVideosResponseBean;
import com.whizzly.utils.DataManager;

public class ProfileVideosViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;
    private ProfileVideosModel mPostedModel;
    private RichMediatorLiveData<ProfileVideosResponseBean> mProfileVideosRichMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<FailureResponse> validateLiveData;
    private RichMediatorLiveData<ChangePasswordModel>  mDeleteResponseLiveData;
    private RichMediatorLiveData<PrivacyStatusResponseBean>  mPrivacyStatusLiveData;


    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void setModel(ProfileVideosModel postedModel) {
        this.mPostedModel = postedModel;
    }

    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (mProfileVideosRichMediatorLiveData == null) {
            mProfileVideosRichMediatorLiveData = new RichMediatorLiveData<ProfileVideosResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
        if (mDeleteResponseLiveData == null) {
            mDeleteResponseLiveData = new RichMediatorLiveData<ChangePasswordModel>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
        if (mPrivacyStatusLiveData == null) {
            mPrivacyStatusLiveData = new RichMediatorLiveData<PrivacyStatusResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

        if (validateLiveData == null)
            validateLiveData = new MutableLiveData<>();
    }

    public void hitApiToFetchPostedVideos(int userId, int next, int pagecount) {

        mPostedModel.hitApiToFetchPostedVideos(mProfileVideosRichMediatorLiveData, userId, next, pagecount);
    }

    public RichMediatorLiveData<ProfileVideosResponseBean> getProfileVideosRichMediatorLiveData()
    {
        return mProfileVideosRichMediatorLiveData;
    }
    public RichMediatorLiveData<PrivacyStatusResponseBean> getVideoPrivacyStatusLiveData()
    {
        return mPrivacyStatusLiveData;
    }

    public int getUserIdFromPrefs()
    {
        return DataManager.getInstance().getUserId();
    }

    public void hitDeleteApi(int videoId, int videoType, int isMyVideo) {
        mPostedModel.hitDeleteApi(mDeleteResponseLiveData,videoId,videoType, isMyVideo);

    }

    public RichMediatorLiveData<ChangePasswordModel> getDeleteVideoLiveData() {
        return mDeleteResponseLiveData;
    }

    public void hitVideoPrivacyTypeApi( int videoId, int videoType, int privacyTypeId,int isMyVideo) {
      mPostedModel.hitPrivacyStatusApi(mPrivacyStatusLiveData,videoId,videoType,privacyTypeId,isMyVideo);
    }


}
