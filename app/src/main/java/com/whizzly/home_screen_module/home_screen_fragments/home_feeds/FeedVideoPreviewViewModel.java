package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.change_password.ChangePasswordModel;
import com.whizzly.models.comment_list.CommentListingBean;
import com.whizzly.models.like_dislike_response.LikeDislikeResponse;
import com.whizzly.models.video_details.VideoDetailsResponse;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class FeedVideoPreviewViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;
    private RichMediatorLiveData<ChangePasswordModel> mDeleteResponseLiveData;
    private RichMediatorLiveData<LikeDislikeResponse> likeDislikeResponseRichMediatorLiveData;
    private RichMediatorLiveData<CommentListingBean> commentListingBeanRichMediatorLiveData;
    private RichMediatorLiveData<VideoDetailsResponse> mVideoDetailsRichMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;

    public RichMediatorLiveData<VideoDetailsResponse> getmVideoDetailsRichMediatorLiveData() {
        return mVideoDetailsRichMediatorLiveData;
    }

    public RichMediatorLiveData<CommentListingBean> getCommentListingBeanRichMediatorLiveData() {
        return commentListingBeanRichMediatorLiveData;
    }

    public RichMediatorLiveData<LikeDislikeResponse> getLikeDislikeResponseRichMediatorLiveData() {
        return likeDislikeResponseRichMediatorLiveData;
    }

    public void setGenericListeners(Observer<FailureResponse> failureResponseObserver, Observer<Throwable> errorObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        setVideoDetailsRichMediatorLiveData();
    }

    private void setVideoDetailsRichMediatorLiveData() {
        if (mVideoDetailsRichMediatorLiveData == null) {
            mVideoDetailsRichMediatorLiveData = new RichMediatorLiveData<VideoDetailsResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
        if (likeDislikeResponseRichMediatorLiveData == null) {
            likeDislikeResponseRichMediatorLiveData = new RichMediatorLiveData<LikeDislikeResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

        if (commentListingBeanRichMediatorLiveData == null) {
            commentListingBeanRichMediatorLiveData = new RichMediatorLiveData<CommentListingBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

        if (mDeleteResponseLiveData == null) {
            mDeleteResponseLiveData = new RichMediatorLiveData<ChangePasswordModel>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
    }

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void onCommentClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_VIDEO_COMMENT_CLICKED);
    }

    public void onReuseCountClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_VIDEO_REUSE_COUNT_CLICKED);
    }

    public void onReuseClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_VIDEO_REUSE_CLICKED);
    }

    public void onLikesCountClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_VIDEO_LIKE_COUNT_CLICKED);
    }

    public void onUserProfileIconClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_USER_PROFILE_CLICKED);
    }

    public void onLikeClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_VIDEO_LIKE_CLICKED);
    }

    public void onMusicIconClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_MUSIC_ICON_CLICKED);
    }

    public void hitVideoDetailsApi(HashMap<String, String> videoQuery) {
        DataManager.getInstance().hitVideoDetailsApi(videoQuery).enqueue(new NetworkCallback<VideoDetailsResponse>() {
            @Override
            public void onSuccess(VideoDetailsResponse videoDetailsResponse) {
                mVideoDetailsRichMediatorLiveData.setValue(videoDetailsResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mVideoDetailsRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mVideoDetailsRichMediatorLiveData.setError(t);
            }
        });
    }


    public void hitLikeDislikeApi(HashMap<String, String> queryMap) {
        DataManager.getInstance().hitLikeDislikeApi(queryMap).enqueue(new NetworkCallback<LikeDislikeResponse>() {
            @Override
            public void onSuccess(LikeDislikeResponse likeDislikeResponse) {
                likeDislikeResponseRichMediatorLiveData.setValue(likeDislikeResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                likeDislikeResponseRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                likeDislikeResponseRichMediatorLiveData.setError(t);
            }
        });
    }

    public void hitCommentListingApi(HashMap<String, String> commentQuery) {
        DataManager.getInstance().getCommentListApi(commentQuery).enqueue(new NetworkCallback<CommentListingBean>() {
            @Override
            public void onSuccess(CommentListingBean commentListingBean) {
                commentListingBeanRichMediatorLiveData.setValue(commentListingBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                commentListingBeanRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                commentListingBeanRichMediatorLiveData.setError(t);
            }
        });
    }

    public void onFeedOptionClicked(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_FEED_OPTION_CLICKED);
    }

    public RichMediatorLiveData<ChangePasswordModel> getDeleteLiveData() {
        return mDeleteResponseLiveData;
    }
}

