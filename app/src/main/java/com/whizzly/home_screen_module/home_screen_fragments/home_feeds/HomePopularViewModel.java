package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.feeds_response.FeedsResponse;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class HomePopularViewModel extends ViewModel {
    private RichMediatorLiveData<FeedsResponse> mFeedsResponseRichMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;

    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureObserver;
        initializeLiveData();
    }

    private void initializeLiveData(){
        if(mFeedsResponseRichMediatorLiveData == null){
            mFeedsResponseRichMediatorLiveData = new RichMediatorLiveData<FeedsResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
    }

    public void hitFeedsApi(int page, int offset) {
        int userId = DataManager.getInstance().getUserId();
        HashMap<String,String> friendsFeeds = new HashMap<>();
        friendsFeeds.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(userId));
        friendsFeeds.put(AppConstants.NetworkConstants.FEED_TYPE, String.valueOf(2));
        friendsFeeds.put(AppConstants.NetworkConstants.PAGE, String.valueOf(page));
        friendsFeeds.put(AppConstants.NetworkConstants.LIMIT, String.valueOf(offset));
        DataManager.getInstance().hitFeedsApi(friendsFeeds).enqueue(new NetworkCallback<FeedsResponse>() {
            @Override
            public void onSuccess(FeedsResponse feedsResponse) {
                mFeedsResponseRichMediatorLiveData.setValue(feedsResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mFeedsResponseRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mFeedsResponseRichMediatorLiveData.setError(t);
            }
        });
    }

    public RichMediatorLiveData<FeedsResponse> getmFeedsResponseRichMediatorLiveData() {
        return mFeedsResponseRichMediatorLiveData;
    }
}
