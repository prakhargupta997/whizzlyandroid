package com.whizzly.home_screen_module.home_screen_fragments.profile.settings;


import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.whizzly.R;
import com.whizzly.databinding.FragmentRankingBinding;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class RankingFragment extends BottomSheetDialogFragment {

    private FragmentRankingBinding fragmentRankingBinding;
    private final int[] mColors = new int[] {
            0xFF1E9DB6,
            0xFF3A76CA,
            0xFF2197D4,
            0xFF212AD4
    };

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();

        if (dialog != null) {
            View bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
            bottomSheet.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
        }
        View view = getView();
        view.post(() -> {
            View parent = (View) view.getParent();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) (parent).getLayoutParams();
            CoordinatorLayout.Behavior behavior = params.getBehavior();
            BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) behavior;
            bottomSheetBehavior.setPeekHeight(view.getMeasuredHeight());
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentRankingBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_ranking, container, false);
        fragmentRankingBinding.flContainer.setBackgroundColor(mColors[0]);
        fragmentRankingBinding.toolbar.ivBack.setVisibility(View.VISIBLE);
        fragmentRankingBinding.toolbar.tvToolbarText.setVisibility(View.VISIBLE);
        fragmentRankingBinding.toolbar.tvToolbarText.setText("Ranking");
        if(DataManager.getInstance().isNotchDevice()){
            fragmentRankingBinding.toolbar.view.setVisibility(View.GONE);
        }else{
            fragmentRankingBinding.toolbar.view.setVisibility(View.GONE);
        }
        fragmentRankingBinding.toolbar.ivBack.setOnClickListener(view -> getActivity().onBackPressed());
        setSesameView(getArguments().getInt(AppConstants.ClassConstants.RATING_COUNT));
        return fragmentRankingBinding.getRoot();
    }

    private void setSesameView(int anInt){
        fragmentRankingBinding.sesameView.setSesameValues(anInt);
        startColorChangeAnim();
    }


    public void startColorChangeAnim() {
        ObjectAnimator animator = ObjectAnimator.ofInt(fragmentRankingBinding.flContainer, "backgroundColor", mColors);
        animator.setDuration(3000);
        animator.setEvaluator(new ArgbEvaluator());
        animator.start();
    }
}
