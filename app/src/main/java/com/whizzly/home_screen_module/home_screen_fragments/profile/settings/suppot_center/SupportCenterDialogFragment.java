package com.whizzly.home_screen_module.home_screen_fragments.profile.settings.suppot_center;


import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.chat.model.Message;
import com.whizzly.chat.model.Room;
import com.whizzly.databinding.DialogSupportCenteredBinding;
import com.whizzly.home_screen_module.home_screen_activities.InnerFragmentsHostActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.submit_profile.SubmitProfileBean;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import java.util.Objects;


public class SupportCenterDialogFragment extends DialogFragment implements OnClickSendListener {

    private Activity mActivity;
    private DialogSupportCenteredBinding mDialogSupportCenteredBinding;
    private SupportCenterViewModel mSupportCenterViewModel;
    private SupportCenterModel mSupportCenterModel;
    private int isFrom = 0;
    private DatabaseReference mDatabaseReference;

    public SupportCenterDialogFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mActivity = getActivity();
        mDialogSupportCenteredBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_support_centered, container, false);
        mSupportCenterViewModel = ViewModelProviders.of(this).get(SupportCenterViewModel.class);
        mDialogSupportCenteredBinding.setViewModel(mSupportCenterViewModel);
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mSupportCenterModel = new SupportCenterModel(mActivity);
        mDialogSupportCenteredBinding.setModel(mSupportCenterModel);
        mSupportCenterViewModel.setOnClickSendListener(this);
        setCancelable(false);
        mSupportCenterViewModel.setModel(mSupportCenterModel);
        Bundle bundle = getArguments();
        assert bundle != null;
        isFrom = bundle.getInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.SUPPORT_CENTER_CLICKED);
        mSupportCenterViewModel.setGenericListeners(((InnerFragmentsHostActivity) mActivity).getErrorObserver(), ((InnerFragmentsHostActivity) mActivity).getFailureResponseObserver());
        observeLiveData();
        initViews();
        mDialogSupportCenteredBinding.etEmailId.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        mDialogSupportCenteredBinding.etSubject.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        mDialogSupportCenteredBinding.etMessage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        return mDialogSupportCenteredBinding.getRoot();
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void initViews() {
        if (isFrom == AppConstants.ClassConstants.SUGGESTION_CLICKED) {
            mDialogSupportCenteredBinding.llMessage.setVisibility(View.VISIBLE);
        } else {
            mDialogSupportCenteredBinding.llSupport.setVisibility(View.VISIBLE);
        }
        String email = DataManager.getInstance().getEmail();
        if (email != null && !email.equals("")) {
            mSupportCenterModel.setEmail(email);
            mDialogSupportCenteredBinding.etEmailId.setEnabled(false);
        }
    }

    private void observeLiveData() {
        mSupportCenterViewModel.getSupportCenterLiveData().observe(this, new Observer<SubmitProfileBean>() {
            @Override
            public void onChanged(@Nullable SubmitProfileBean submitProfileBean) {
                if (submitProfileBean != null) {
                    if (submitProfileBean.getCode() == 301 || submitProfileBean.getCode() == 302) {
                        Toast.makeText(mActivity, submitProfileBean.getMessage(), Toast.LENGTH_SHORT).show();
                        ((BaseActivity) Objects.requireNonNull(mActivity)).autoLogOut();
                    } else {
                        Toast.makeText(mActivity, submitProfileBean.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    dismiss();
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
            dialog.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_CLICK_CANCEL:
                dismiss();
                break;
            case AppConstants.ClassConstants.SUBMIT_CLICKED:

                if (mSupportCenterModel.isFieldsValid()) {
                    mSupportCenterModel.hitSupportCenterApi(mSupportCenterViewModel.getSupportCenterLiveData());
                } else {
                    mSupportCenterModel.showErrorMessages();
                }
            case AppConstants.ClassConstants.MESSAGE_CLICKED:
                if (mDialogSupportCenteredBinding.etMessage1.getText().toString().length() > 0) {
                    String roomId = "";
                    if (DataManager.getInstance().getUserId() > 4) {
                        roomId = DataManager.getInstance().getUserId() + "_1";
                    } else {
                        roomId = "1_" + DataManager.getInstance().getUserId();
                    }
                    String finalRoomId1 = roomId;
                    String finalRoomId2 = roomId;
                    mDatabaseReference.child(AppConstants.FirebaseConstants.INBOX).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (!dataSnapshot.hasChild(finalRoomId1)) {
                                mDatabaseReference.child(AppConstants.FirebaseConstants.INBOX).child(String.valueOf(DataManager.getInstance().getUserId())).child("1").setValue(finalRoomId1);
                                mDatabaseReference.child(AppConstants.FirebaseConstants.INBOX).child("1").child(String.valueOf(DataManager.getInstance().getUserId())).setValue(finalRoomId1);
                                mDatabaseReference.child(AppConstants.FirebaseConstants.ROOM).child(finalRoomId2).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (dataSnapshot != null && dataSnapshot.child(AppConstants.FirebaseConstants.CREATED_AT).exists()) {
                                            mDatabaseReference.child(AppConstants.FirebaseConstants.ROOM).child(finalRoomId2).child(AppConstants.FirebaseConstants.LAST_ACTIVE).setValue(System.currentTimeMillis());
                                        } else {
                                            mDatabaseReference.child(AppConstants.FirebaseConstants.ROOM).child(finalRoomId2).setValue(new Room(System.currentTimeMillis(), System.currentTimeMillis(), "Direct", finalRoomId2, "0", "0", ""));
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    String id = mDatabaseReference.child(AppConstants.FirebaseConstants.MESSAGES).child(roomId).push().getKey();
                    Message message = new Message();
                    message.setIdReceiver("1");
                    message.setFirebaseKey(id);
                    message.setIdSender(String.valueOf(DataManager.getInstance().getUserId()));
                    message.setMessageType(AppConstants.FirebaseConstants.MESSAGE_TYPE);
                    message.setText(mDialogSupportCenteredBinding.etMessage1.getText().toString().trim());
                    message.setTimestamp(System.currentTimeMillis());
                    String finalRoomId = roomId;
                    mDatabaseReference.child(AppConstants.FirebaseConstants.MESSAGES).child(roomId).child(id).setValue(message).addOnSuccessListener(aVoid -> {
                        if (AppUtils.isNetworkAvailable(Objects.requireNonNull(getActivity())))
                            mSupportCenterViewModel.hitChatNotificationApi(message, finalRoomId);
                        dismiss();
                    });

                }
                break;
        }
    }
}
