package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.generated.callback.OnClickListener;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.collab.collab_videos.CollabVideosResponseBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

public class HomeCollabViewModel extends ViewModel {

    private RichMediatorLiveData<CollabVideosResponseBean> mCollabLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private OnClickSendListener onClickSendListener;

    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureObserver;
        initializeLiveData();
    }

    private void initializeLiveData() {
        if (mCollabLiveData == null) {
            mCollabLiveData = new RichMediatorLiveData<CollabVideosResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
    }
    public void onChartsViewClicked(){
  onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CHART_VIEW_CLICKED);
    }

    public RichMediatorLiveData<CollabVideosResponseBean> getCollabVideosLiveData() {
        return mCollabLiveData;
    }

    public void getCollabVideoListing() {
        DataManager.getInstance().hitCategoryRelatedVideosApi(String.valueOf(DataManager.getInstance().getUserId()), DataManager.getInstance().getLanguage()).enqueue(new NetworkCallback<CollabVideosResponseBean>() {
            @Override
            public void onSuccess(CollabVideosResponseBean collabVideosResponseBean) {
                mCollabLiveData.setValue(collabVideosResponseBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mCollabLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mCollabLiveData.setError(t);
            }
        });
    }

    public void setClickListener(OnClickSendListener onClickListener) {
        onClickSendListener=onClickListener;
    }
}
