package com.whizzly.home_screen_module.home_screen_fragments.user;


import android.app.Activity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.whizzly.R;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.FragmentUserSearchBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.home_screen_module.home_screen_fragments.profile.ProfileFragment;
import com.whizzly.models.search.users.Result;
import com.whizzly.models.search.users.UserList;
import com.whizzly.utils.AppConstants;

import java.util.ArrayList;
import java.util.Objects;


public class UserListingFragment extends Fragment implements OnClickAdapterListener {
    private FragmentUserSearchBinding mFragmentUserSearchBinding;
    private UserViewModel mUserViewModel;
    private UserModel mUserModel;
    private Activity mActivity;
    private UserListingAdapter mUserListingAdapter;
    private ArrayList<UserList> mUserList;
    private int mPage = 1;
    private String mSearch = "";

    public UserListingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mFragmentUserSearchBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_search, container, false);
        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        mUserViewModel.setGenericListeners(((HomeActivity) Objects.requireNonNull(getActivity())).getErrorObserver(), ((HomeActivity) Objects.requireNonNull(getActivity())).getFailureResponseObserver());
        mFragmentUserSearchBinding.setViewModel(mUserViewModel);
        mUserModel = new UserModel(mActivity);
        mUserViewModel.setModel(mUserModel);
        mActivity = getActivity();
        initViewsAndVariables();
        return mFragmentUserSearchBinding.getRoot();
    }

    private void initViewsAndVariables() {
        mUserList = new ArrayList<>();
        setRecyclerView();

    }

    private void setRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mFragmentUserSearchBinding.rvUsers.setLayoutManager(linearLayoutManager);
        mUserListingAdapter = new UserListingAdapter(this, mUserList, getActivity());
        mFragmentUserSearchBinding.rvUsers.setAdapter(mUserListingAdapter);
    }

    @Override
    public void onAdapterClicked(int code, Object obj, int position) {
        if (obj != null && (obj instanceof UserList)) {
            UserList userList = (UserList) obj;
            ProfileFragment profileFragment = new ProfileFragment();
            Bundle bundle = new Bundle();
            if (userList.getUserId().equals(mUserViewModel.getUserId())) {
                bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_USER_LISTING_MY_PROFILE_FRAGMENT);
                bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.MY_PROFILE);
                ((HomeActivity) getActivity()).selectBottomNav(false, false, false, true, false);
            } else {
                bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.OTHER_USER_PROFILE);
                bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_USER_LISTING_FRAGMENT);
            }
            bundle.putString(AppConstants.ClassConstants.USER_ID, String.valueOf(userList.getUserId()));
            profileFragment.setArguments(bundle);
            Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction().add(R.id.fl_container, profileFragment).addToBackStack(ProfileFragment.class.getCanonicalName()).commit();
        }
    }

    public void setUserData(Result result, Integer nextCount) {
        try {
            mUserList.clear();
            mSearch = result.getSearchValue();
            if (result.getUserList() != null && result.getUserList().size() > 0)
                mUserList.addAll(result.getUserList());
            if (mUserList.size() > 0)
                mFragmentUserSearchBinding.tvError.setVisibility(View.GONE);
            else
                mFragmentUserSearchBinding.tvError.setVisibility(View.VISIBLE);
            mUserListingAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
