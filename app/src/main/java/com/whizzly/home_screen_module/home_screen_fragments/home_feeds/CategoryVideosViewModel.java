package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.collab.collab_videos.allvideos.CategoryVideosResponse;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class CategoryVideosViewModel extends ViewModel {
    private RichMediatorLiveData<CategoryVideosResponse> mCategoryVideosLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureResponseObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (mCategoryVideosLiveData == null) {
            mCategoryVideosLiveData = new RichMediatorLiveData<CategoryVideosResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

    }

    public RichMediatorLiveData<CategoryVideosResponse> getCategoryVideosLiveData() {
        return mCategoryVideosLiveData;
    }

    public void getCategoryVideos(Integer mPage, Integer mOffset, Integer categoryId) {
        DataManager.getInstance().hitViewMoreVideosApi(getData(mPage, mOffset, categoryId)).enqueue(new NetworkCallback<CategoryVideosResponse>() {
            @Override
            public void onSuccess(CategoryVideosResponse categoryVideosResponse) {
                mCategoryVideosLiveData.setValue(categoryVideosResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mCategoryVideosLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mCategoryVideosLiveData.setError(t);
            }
        });
    }

    private HashMap<String, String> getData(Integer mPage, Integer mOffset, Integer categoryId) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(AppConstants.NetworkConstants.PAGE, String.valueOf(mPage));
        hashMap.put(AppConstants.NetworkConstants.LIMIT, String.valueOf(mOffset));
        hashMap.put(AppConstants.NetworkConstants.CATEGORY_ID, String.valueOf(categoryId));
        hashMap.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        return hashMap;
    }
}

