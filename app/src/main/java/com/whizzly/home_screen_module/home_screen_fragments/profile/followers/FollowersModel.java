package com.whizzly.home_screen_module.home_screen_fragments.profile.followers;

import android.content.Context;
import androidx.databinding.BaseObservable;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.followers_following_response.FollowersResponseBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

/**
 * Created by Rahul Mishra on 24/4/19.
 */
public class FollowersModel extends BaseObservable {
    private final Context context;

    FollowersModel(Context context) {
        this.context = context;
    }
    public void getFollowersListing(HashMap<String, String> hashMap, RichMediatorLiveData<FollowersResponseBean> mFollowersLisrResponseLiveData) {
        DataManager.getInstance().getFollowersListing(hashMap).enqueue(new NetworkCallback<FollowersResponseBean>() {
            @Override
            public void onSuccess(FollowersResponseBean followersResponseBean) {
                mFollowersLisrResponseLiveData.setValue(followersResponseBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {

                mFollowersLisrResponseLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {

                mFollowersLisrResponseLiveData.setError(t);

            }
        });
    }

    public void getFollowingListing(HashMap<String, String> hashMap, RichMediatorLiveData<FollowersResponseBean> mFollowersLisrResponseLiveData) {
        DataManager.getInstance().getFollowingListing(hashMap).enqueue(new NetworkCallback<FollowersResponseBean>() {
            @Override
            public void onSuccess(FollowersResponseBean followersResponseBean) {
                mFollowersLisrResponseLiveData.setValue(followersResponseBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {

                mFollowersLisrResponseLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {

                mFollowersLisrResponseLiveData.setError(t);

            }
        });
    }
}
