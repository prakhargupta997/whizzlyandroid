package com.whizzly.home_screen_module.home_screen_fragments.profile.settings.change_language;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.submit_profile.SubmitProfileBean;
import com.whizzly.utils.AppConstants;

public class ChangeLanguageViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;
    private ChangeLanguageModel mChangeLanguageModel;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<FailureResponse> validateLiveData;

    private RichMediatorLiveData<SubmitProfileBean> mChangeLanguageRichMediatorLiveData;


    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureObserver;
        initLiveData();
    }


    public ChangeLanguageModel getmChangeLanguageModel() {
        return mChangeLanguageModel;
    }

    private void initLiveData() {
        if (mChangeLanguageRichMediatorLiveData == null) {
            mChangeLanguageRichMediatorLiveData = new RichMediatorLiveData<SubmitProfileBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }


        if (validateLiveData == null)
            validateLiveData = new MutableLiveData<>();
    }

    public void onBackClick() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_BACK_PRESSED);
    }

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void setModel(ChangeLanguageModel mChangeLanguageModel) {
        this.mChangeLanguageModel = mChangeLanguageModel;
    }

    public void onEnglishSelected()
    {
        mChangeLanguageModel.setIsLanguageEnglish(true);
        mChangeLanguageModel.hitChangeLanguageApi(mChangeLanguageRichMediatorLiveData);
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_ENGLISH_SELECTED);
    }

    public void onItalianSelected()
    {
        mChangeLanguageModel.setIsLanguageEnglish(false);
        mChangeLanguageModel.hitChangeLanguageApi(mChangeLanguageRichMediatorLiveData);
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_ITALIAN_SELECTED);
    }


}
