package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;

import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.HomeFriendsFragmentBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.interfaces.OnActionDetectListener;
import com.whizzly.interfaces.OnLikeDislikeUpdate;
import com.whizzly.models.feeds_response.FeedsResponse;
import com.whizzly.models.feeds_response.Result;
import com.whizzly.models.privacy_status.PrivacyStatusResponseBean;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.VideoPlayerRecyclerAdapter;
import com.whizzly.utils.VideoPlayerViewHolder;

import java.util.ArrayList;
import java.util.Objects;

public class HomeFriendsFragment extends Fragment implements OnLikeDislikeUpdate, OnActionDetectListener {

    private HomeFriendsViewModel mViewModel;
    private HomeFriendsFragmentBinding homeFriendsFragmentBinding;
    private ArrayList<Result> results = new ArrayList<>();
    private VideoPlayerRecyclerAdapter videosViewPagerAdapter;
    private int page = 1;
    private int mNextPos = 1;
    private int mCurrentPos = 0;
    private int mCurrentSelectedScreen;
    private Observer<PrivacyStatusResponseBean> mSavedVideoObserver;
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (isAdded() && isVisible())
                homeFriendsFragmentBinding.rvHomeFeeds.playVideo(false);
        }
    };
    private int next = 1;
    private boolean isFromRefresh = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        homeFriendsFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.home_friends_fragment, container, false);
        mViewModel = ViewModelProviders.of(this).get(HomeFriendsViewModel.class);
        homeFriendsFragmentBinding.setViewModel(mViewModel);
        mViewModel.setGenericListeners(((HomeActivity) Objects.requireNonNull(getActivity())).getErrorObserver(), ((HomeActivity) Objects.requireNonNull(getActivity())).getFailureResponseObserver());
        mViewModel.getmFeedsResponseRichMediatorLiveData().observe(this, new Observer<FeedsResponse>() {
            @Override
            public void onChanged(@Nullable FeedsResponse feedsResponse) {
                if (feedsResponse != null) {
                    if (isAdded() && isVisible()) {
                        homeFriendsFragmentBinding.pbLoading.setVisibility(View.GONE);
                        if (homeFriendsFragmentBinding.srvFeeds.isRefreshing()) {
                            homeFriendsFragmentBinding.srvFeeds.setRefreshing(false);
                        }
                        page = feedsResponse.getNextCount();
                        if (isFromRefresh) {
                            results.clear();
                            getDataFromApi(false);
                        }
                        results.addAll(feedsResponse.getResult());
                        if (results.size() == 0) {
                            homeFriendsFragmentBinding.ivNoData.setVisibility(View.VISIBLE);
                        } else {
                            homeFriendsFragmentBinding.ivNoData.setVisibility(View.GONE);
                        }
                        homeFriendsFragmentBinding.rvHomeFeeds.setMediaObjects(results);
                        videosViewPagerAdapter.notifyDataSetChanged();
                        handler.postDelayed(runnable, 1000);
                    }
                }
            }
        });
        homeFriendsFragmentBinding.noInternetConnection.btnRetryFeeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataFromApi(false);
            }
        });
        getDataFromApi(false);
        setViewPager();
        observeSavedVideoLiveData();
        return homeFriendsFragmentBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        homeFriendsFragmentBinding.rvHomeFeeds.resumePlayer();
    }

    private void setViewPager() {
        homeFriendsFragmentBinding.srvFeeds.setColorSchemeResources(R.color.colorAccent);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        homeFriendsFragmentBinding.rvHomeFeeds.setLayoutManager(linearLayoutManager);
        homeFriendsFragmentBinding.rvHomeFeeds.setMediaObjects(results);
        homeFriendsFragmentBinding.rvHomeFeeds.setNestedScrollingEnabled(false);
        videosViewPagerAdapter = new VideoPlayerRecyclerAdapter(results, getActivity(), getChildFragmentManager(), this);
        homeFriendsFragmentBinding.rvHomeFeeds.setAdapter(videosViewPagerAdapter);
        PagerSnapHelper pagerHelper = new PagerSnapHelper();
        pagerHelper.attachToRecyclerView(homeFriendsFragmentBinding.rvHomeFeeds);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                if (direction == ItemTouchHelper.LEFT) {
                    ((VideoPlayerViewHolder) viewHolder).onSwipeLeft();
                }
            }

            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };

        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(homeFriendsFragmentBinding.rvHomeFeeds);

        homeFriendsFragmentBinding.srvFeeds.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDataFromApi(true);
            }
        });

        homeFriendsFragmentBinding.rvHomeFeeds.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                videosViewPagerAdapter.notifyDataSetChanged();
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    getDataFromApi(false);
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                /*((HomeActivity) getActivity()).showTopView();
                ((HomeActivity) getActivity()).mActivityHomeBinding.llBottom.setVisibility(View.VISIBLE);*/
            }
        });
    }

    private void getDataFromApi(boolean isFromRefresh) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            if (AppUtils.isNetworkAvailable(getActivity())) {
                this.isFromRefresh = isFromRefresh;
                if (page > 0) {
                    homeFriendsFragmentBinding.noInternetConnection.rlMainLayout.setVisibility(View.GONE);
                    mViewModel.hitFeedsApi(page, 30);
                    homeFriendsFragmentBinding.pbLoading.setVisibility(View.VISIBLE);
                }
            } else {
                homeFriendsFragmentBinding.noInternetConnection.rlMainLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        homeFriendsFragmentBinding.rvHomeFeeds.pausePlayer();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
        homeFriendsFragmentBinding.rvHomeFeeds.releasePlayer();
    }

    @Override
    public void onLikeDislikeUpdate(int position, int status) {
        Result result = results.get(position);
        result.setIsLiked(status);
        if (status == 0)
            result.setTotalLikes(result.getTotalLikes() - 1);
        else
            result.setTotalLikes(result.getTotalLikes() + 1);
        results.set(position, result);
    }

    public Observer<PrivacyStatusResponseBean> geSavedFeedsObserver() {
        return mSavedVideoObserver;
    }

    private void observeSavedVideoLiveData() {
        mSavedVideoObserver = privacyStatusResponseBean -> {
            if (privacyStatusResponseBean != null) {
                if (privacyStatusResponseBean.getCode() == 200) {
                    if (privacyStatusResponseBean.getResult().getIsLocked() != null && privacyStatusResponseBean.getResult().getVideoId() != null) {
                        for (int i = 0; i < results.size(); i++) {
                            if (results.get(i).getId() == privacyStatusResponseBean.getResult().getVideoId()) {
                                results.get(i).setSavedVideoPrivacy(privacyStatusResponseBean.getResult().getIsLocked());
                                break;
                            }
                        }
                    }
                    Toast.makeText(getActivity(), privacyStatusResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (privacyStatusResponseBean.getCode() == 301 || privacyStatusResponseBean.getCode() == 302) {
                    Toast.makeText(getActivity(), privacyStatusResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) getActivity()).autoLogOut();
                } else
                    Toast.makeText(getActivity(), getActivity().getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();

            }

        };
    }

    @Override
    public void onActionDone(Boolean action) {
        if (action) {
            homeFriendsFragmentBinding.rvHomeFeeds.pausePlayer();
        } else {
            homeFriendsFragmentBinding.rvHomeFeeds.resumePlayer();
        }
    }
}