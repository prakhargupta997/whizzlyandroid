package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;


import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.ItemRowCategoriesBinding;
import com.whizzly.models.collab.collab_videos.VideoDatum;
import com.whizzly.utils.AppConstants;

import java.util.ArrayList;

public class HomeCollabAdapter   extends RecyclerView.Adapter<HomeCollabAdapter.CollabViewHolder> {

    private ItemRowCategoriesBinding mItemRowCollabFeedsBinding;
    private Context mContext;
    private ArrayList<VideoDatum> mList;
    private OnClickAdapterListener onClickAdapterListener;

    public HomeCollabAdapter(Context mContext, ArrayList<VideoDatum> mList, OnClickAdapterListener onClickAdapterListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.onClickAdapterListener=onClickAdapterListener;
    }

    @NonNull
    @Override
    public CollabViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mItemRowCollabFeedsBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.item_row_categories, viewGroup, false);
        return new CollabViewHolder(mItemRowCollabFeedsBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeCollabAdapter.CollabViewHolder viewHolder, int i) {
        viewHolder.bind(mList.get(i));
        mItemRowCollabFeedsBinding.tvCategory.setText(mList.get(i).getCategoryName());
       if (mList.get(i).getCategoryVideo().size()>10)
            mItemRowCollabFeedsBinding.tvLabelViewMore.setVisibility(View.VISIBLE);
        setRecyclerView(i);
      }

    private void setRecyclerView(int i) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false);
        mItemRowCollabFeedsBinding.rvVideos.setLayoutManager(layoutManager);
        CategoriesVideosAdapter categoriesVideosAdapter = new CategoriesVideosAdapter(mContext,mList.get(i).getCategoryVideo(),onClickAdapterListener);
        mItemRowCollabFeedsBinding.rvVideos.setAdapter(categoriesVideosAdapter);
        mItemRowCollabFeedsBinding.tvLabelViewMore.setOnClickListener(v -> onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_VIEW_MORE_VIEW_CLICKED, mList.get(i), i));


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class CollabViewHolder extends RecyclerView.ViewHolder {
        private ItemRowCategoriesBinding mItemRowCollabFeedsBinding;
        CollabViewHolder(@NonNull ItemRowCategoriesBinding itemRowCollabFeedsBinding) {
            super(itemRowCollabFeedsBinding.getRoot());
            this.mItemRowCollabFeedsBinding = itemRowCollabFeedsBinding;
        }

        public void bind(VideoDatum result){
            mItemRowCollabFeedsBinding.setVariable(BR.model, result);
            mItemRowCollabFeedsBinding.executePendingBindings();
        }
    }
}