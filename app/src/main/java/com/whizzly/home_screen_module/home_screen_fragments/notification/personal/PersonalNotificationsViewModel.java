package com.whizzly.home_screen_module.home_screen_fragments.notification.personal;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.followUnfollow.FollowUnfollowResponseBean;
import com.whizzly.models.notifications.NotificationResponse;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class PersonalNotificationsViewModel extends ViewModel {
    private RichMediatorLiveData<NotificationResponse> mNotificationLiveData;
    private RichMediatorLiveData<FollowUnfollowResponseBean> mFollowUnfollowLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;


    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureResponseObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (mNotificationLiveData == null) {
            mNotificationLiveData = new RichMediatorLiveData<NotificationResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
        if (mFollowUnfollowLiveData == null) {
            mFollowUnfollowLiveData = new RichMediatorLiveData<FollowUnfollowResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
    }

    public RichMediatorLiveData<NotificationResponse> getNotificationLiveData() {
        return mNotificationLiveData;
    }

    public void getNotificationListing() {
        DataManager.getInstance().hitNotificationListingApi(String.valueOf(DataManager.getInstance().getUserId()), DataManager.getInstance().getLanguage()).enqueue(new NetworkCallback<NotificationResponse>() {
            @Override
            public void onSuccess(NotificationResponse notificationResponse) {
                mNotificationLiveData.setValue(notificationResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mNotificationLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mNotificationLiveData.setError(t);
            }
        });
    }

    public void OnFollowButtonViewClicked(Integer affectedUserId, Integer followedStatus) {
        DataManager.getInstance().hitFollowUnfollowApi(getFollowUnfollowData(String.valueOf(affectedUserId), followedStatus)).enqueue(new NetworkCallback<FollowUnfollowResponseBean>() {
            @Override
            public void onSuccess(FollowUnfollowResponseBean followUnfollowResponseBean) {
                mFollowUnfollowLiveData.setValue(followUnfollowResponseBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mFollowUnfollowLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mFollowUnfollowLiveData.setError(t);
            }
        });
    }

    private HashMap<String, String> getFollowUnfollowData(String affectedUserId, Integer followedStatus) {
        HashMap<String, String> params = new HashMap<>();
        String userId = String.valueOf(DataManager.getInstance().getUserId());
        params.put(AppConstants.NetworkConstants.PARAM_USER_ID, userId);
        params.put(AppConstants.NetworkConstants.PARAM_FOLLOWING_ID, affectedUserId);
        if (followedStatus==0)
        params.put(AppConstants.NetworkConstants.PARAM_STATUS, String.valueOf(1));
        else
            params.put(AppConstants.NetworkConstants.PARAM_STATUS, String.valueOf(0));

        return params;
    }

    public RichMediatorLiveData<FollowUnfollowResponseBean> getFollowUnfollowLiveData() {
        return mFollowUnfollowLiveData;
    }
}
