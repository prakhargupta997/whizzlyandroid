package com.whizzly.home_screen_module.home_screen_fragments.profile.posted;

import android.content.Context;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableField;
import android.widget.Toast;

import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.change_password.ChangePasswordModel;
import com.whizzly.models.privacy_status.PrivacyStatusResponseBean;
import com.whizzly.models.profileVideo.ProfileVideosResponseBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class ProfileVideosModel extends BaseObservable {

    ObservableField<Integer> videoType = new ObservableField<>();


    public void setVideoType(int videoType) {
        this.videoType.set(videoType);
        notifyPropertyChanged(BR.videoType);
    }

    @Bindable
    public int getVideoType()
    {
        return videoType.get();
    }

    private Context context;

    ProfileVideosModel(Context context)
    {
        this.context = context;
    }

    public HashMap<String, String> getData(String userId, int pageCount) {

        HashMap<String, String> postedVideo = new HashMap<>();
        postedVideo.put(AppConstants.NetworkConstants.PARAM_USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        postedVideo.put(AppConstants.NetworkConstants.PARAM_OTHER_USER_ID, userId);
        postedVideo.put(AppConstants.NetworkConstants.TYPE, String.valueOf(getVideoType()));
        postedVideo.put(AppConstants.NetworkConstants.LIMIT, "10");
        postedVideo.put("page", String.valueOf(pageCount));


        return postedVideo;
    }

    public void hitApiToFetchPostedVideos(RichMediatorLiveData<ProfileVideosResponseBean> mProfileVideosRichMediatorLiveData, int userId, int next, int pageCount)
    {
        if(AppUtils.isNetworkAvailable(context)) {

                DataManager.getInstance().getProfileVideos(getData(String.valueOf(userId), pageCount)).enqueue(new NetworkCallback<ProfileVideosResponseBean>() {
                    @Override
                    public void onSuccess(ProfileVideosResponseBean profileVideoBean) {
                        mProfileVideosRichMediatorLiveData.setValue(profileVideoBean);
                    }

                    @Override
                    public void onFailure(FailureResponse failureResponse) {
                        mProfileVideosRichMediatorLiveData.setFailure(failureResponse);
                    }

                    @Override
                    public void onError(Throwable t) {
                        mProfileVideosRichMediatorLiveData.setError(t);
                    }
                });

        }else {
            Toast.makeText(context, context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    public void hitDeleteApi(RichMediatorLiveData<ChangePasswordModel> mDeleteResponseLiveData, int videoId, int videoType, int isMyVideo) {
        if(AppUtils.isNetworkAvailable(context)) {
         DataManager.getInstance().hitDeletePostApi(getVideoData(videoId,videoType, isMyVideo)).enqueue(new NetworkCallback<ChangePasswordModel>() {
             @Override
             public void onSuccess(ChangePasswordModel deleteVideoResponse) {
                 mDeleteResponseLiveData.setValue(deleteVideoResponse);
             }

             @Override
             public void onFailure(FailureResponse failureResponse) {
              mDeleteResponseLiveData.setFailure(failureResponse);
             }

             @Override
             public void onError(Throwable t) {
            mDeleteResponseLiveData.setError(t);
             }
         });
        }
        else {
            Toast.makeText(context, context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    private HashMap<String, String> getVideoData(int videoId, int videoType, int isMyVideo) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        hashMap.put(AppConstants.NetworkConstants.VIDEO_ID, String.valueOf(videoId));
        hashMap.put(AppConstants.NetworkConstants.VIDEO_TYPE, String.valueOf(videoType));
        hashMap.put(AppConstants.NetworkConstants.IS_MY_VIDEO, String.valueOf(isMyVideo));

        return hashMap;
    }

    public void hitPrivacyStatusApi(RichMediatorLiveData<PrivacyStatusResponseBean> mPrivacyStatusLiveData, int videoId, int videoType, int privacyTypeId,int isMyVideo) {
     DataManager.getInstance().hitPrivacyStatusApi(getDataForStatus(videoId,videoType,privacyTypeId,isMyVideo)).enqueue(new NetworkCallback<PrivacyStatusResponseBean>() {
         @Override
         public void onSuccess(PrivacyStatusResponseBean response) {
             mPrivacyStatusLiveData.setValue(response);
         }

         @Override
         public void onFailure(FailureResponse failureResponse) {
             mPrivacyStatusLiveData.setFailure(failureResponse);
         }

         @Override
         public void onError(Throwable t) {
         mPrivacyStatusLiveData.setError(t);
         }
     });
    }

        private HashMap<String,String> getDataForStatus(int videoId, int videoType, int privacyTypeId,int isMyVideo) {
            HashMap<String,String> hashMap=new HashMap<>();
            hashMap.put(AppConstants.NetworkConstants.USER_ID,String.valueOf(DataManager.getInstance().getUserId()));
            hashMap.put(AppConstants.NetworkConstants.VIDEO_ID,String.valueOf(videoId));
            hashMap.put(AppConstants.NetworkConstants.VIDEO_TYPE,String.valueOf(videoType));
            hashMap.put(AppConstants.NetworkConstants.IS_MY_VIDEO,String.valueOf(isMyVideo));
            if (privacyTypeId==1)
            hashMap.put(AppConstants.NetworkConstants.PRIVACY_TYPE_ID,String.valueOf(2));
            else if (privacyTypeId==2)
                hashMap.put(AppConstants.NetworkConstants.PRIVACY_TYPE_ID,String.valueOf(1));

            return hashMap;

    }

}
