package com.whizzly.home_screen_module.home_screen_fragments.profile.settings;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.forgot_password.ResetPasswordBean;
import com.whizzly.models.submit_profile.SubmitProfileBean;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

public class SettingsViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;
    private SettingsModel mSettingsModel;
    private RichMediatorLiveData<SubmitProfileBean> mNotificationStatusMediatorLiveData;
    private RichMediatorLiveData<ResetPasswordBean> mLogOutMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<FailureResponse> validateLiveData;

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public RichMediatorLiveData<SubmitProfileBean> getNotificationStatusMediatorLiveData()
    {
        return mNotificationStatusMediatorLiveData;
    }
    public RichMediatorLiveData<ResetPasswordBean> getLogOutMediatorLiveData()
    {
        return mLogOutMediatorLiveData;
    }


    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (mNotificationStatusMediatorLiveData == null) {
            mNotificationStatusMediatorLiveData = new RichMediatorLiveData<SubmitProfileBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

        if (mLogOutMediatorLiveData == null) {
            mLogOutMediatorLiveData = new RichMediatorLiveData<ResetPasswordBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }


        if (validateLiveData == null)
            validateLiveData = new MutableLiveData<>();
    }

    public void setModel(SettingsModel mSettingModel) {
        this.mSettingsModel = mSettingModel;
    }


    public void onChangePasswordClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.CHANGE_PASSWORD_CLICKED);
    }

    public void onRankingClicked(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_RANKING_CLICKED);
    }

    public void onBlockedUsersClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.BLOCKED_USERS_CLICKED);
    }

    public void onChangeLanguageClicked() {

        onClickSendListener.onClickSend(AppConstants.ClassConstants.CHANGE_LANGUAGE_CLICKED);
    }
    public void onLogOutClicked() {

        onClickSendListener.onClickSend(AppConstants.ClassConstants.LOG_OUT_CLICKED);
    }
    public void onPrivacyPolicyViewClicked(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_PRIVACY_POLICY_VIEW_CLICKED);
    }
    public void onTermsAndConditionViewClicked(){
       onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_TERMS_AND_CONDITION_VIEW_CLCIKED);
    }


    public int getUserId()
    {
       return  DataManager.getInstance().getUserId();
    }

   /* public void onNotificationStatusChanged(boolean value)
    {
        mSettingsModel.setIsNotificationOn(value);
        onClickSendListener.onClickSend(AppConstants.ClassConstants.NOTIFICATION_TOGGLED);
    }*/
    public  void onSupportCenterViewClicked(){
      onClickSendListener.onClickSend(AppConstants.ClassConstants.SUPPORT_CENTER_CLICKED);
    }

    public  void onSuggestionClicked(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.SUGGESTION_CLICKED);
    }



}
