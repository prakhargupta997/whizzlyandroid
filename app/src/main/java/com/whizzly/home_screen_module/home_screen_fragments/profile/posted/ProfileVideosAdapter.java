package com.whizzly.home_screen_module.home_screen_fragments.profile.posted;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import androidx.databinding.DataBindingUtil;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.ItemRowSuggestedVideoBinding;
import com.whizzly.models.profileVideo.VideoInfo;
import com.whizzly.utils.AppConstants;

import java.util.ArrayList;

public class ProfileVideosAdapter extends RecyclerView.Adapter<ProfileVideosAdapter.ViewHolder> {

    private ItemRowSuggestedVideoBinding itemRowSuggestedVideoBinding;
    private OnClickAdapterListener onClickAdapterListener;
    private ArrayList<VideoInfo> videoListBeans;
    private int width = 0;
    private int mAccountType;
    private int mVideoType;
    private Context context;

    public ProfileVideosAdapter(Context context, ArrayList<VideoInfo> videoListBeans, OnClickAdapterListener onClickAdapterListener, int videoType, int mAccountType) {
        this.videoListBeans = videoListBeans;
        this.onClickAdapterListener = onClickAdapterListener;
        this.mVideoType = videoType;
        mHandler = new Handler();
        this.context = context;
        this.mAccountType = mAccountType;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        itemRowSuggestedVideoBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_row_suggested_video, viewGroup, false);
        width = viewGroup.getWidth() / 3;
        return new ViewHolder(itemRowSuggestedVideoBinding);
    }

    private Handler mHandler;
    private Runnable mRunnable;

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        mRunnable = () -> {
            notifyDataSetChanged();
        };
        viewHolder.bind(videoListBeans.get(i));
        RequestOptions requestOptions = new RequestOptions();
        viewHolder.itemRowSuggestedVideoBinding.ivVideo.setMinimumHeight(width);
        viewHolder.itemRowSuggestedVideoBinding.ivVideo.setMinimumWidth(width);
        requestOptions.placeholder(R.drawable.ic_choose_image_gallery);
        Glide.with(viewHolder.itemRowSuggestedVideoBinding.getRoot()).load(videoListBeans.get(i).getThumbnailUrl()).apply(requestOptions).into(viewHolder.itemRowSuggestedVideoBinding.ivVideo);
        if (mVideoType == AppConstants.VIDEO_TYPE_SAVED) {
            TransitionManager.beginDelayedTransition(viewHolder.itemRowSuggestedVideoBinding.rlContainer);
            if ( videoListBeans.get(i).getPrivacyTypeId() == 1) {
                TransitionManager.beginDelayedTransition(viewHolder.itemRowSuggestedVideoBinding.rlContainer);
                viewHolder.itemRowSuggestedVideoBinding.ivLockClosed.setVisibility(View.VISIBLE);
                viewHolder.itemRowSuggestedVideoBinding.ivLockOpen.setVisibility(View.GONE);
                viewHolder.itemRowSuggestedVideoBinding.ivDelete.setVisibility(View.GONE);
            } else {
                TransitionManager.beginDelayedTransition(viewHolder.itemRowSuggestedVideoBinding.rlContainer);
                viewHolder.itemRowSuggestedVideoBinding.ivLockClosed.setVisibility(View.GONE);
                viewHolder.itemRowSuggestedVideoBinding.ivLockOpen.setVisibility(View.GONE);
                viewHolder.itemRowSuggestedVideoBinding.ivDelete.setVisibility(View.GONE);
            }

            viewHolder.itemRowSuggestedVideoBinding.ivVideo.setOnLongClickListener(v -> {
                if(mAccountType != AppConstants.OTHER_USER_PROFILE) {
                    if (videoListBeans.get(i).getPrivacyTypeId() == 1) {
                        TransitionManager.beginDelayedTransition(viewHolder.itemRowSuggestedVideoBinding.rlContainer);
                        viewHolder.itemRowSuggestedVideoBinding.ivLockOpen.setVisibility(View.VISIBLE);
                        viewHolder.itemRowSuggestedVideoBinding.ivLockClosed.setVisibility(View.GONE);
                        viewHolder.itemRowSuggestedVideoBinding.ivDelete.setVisibility(View.VISIBLE);
                    } else {
                        TransitionManager.beginDelayedTransition(viewHolder.itemRowSuggestedVideoBinding.rlContainer);
                        viewHolder.itemRowSuggestedVideoBinding.ivLockOpen.setVisibility(View.GONE);
                        viewHolder.itemRowSuggestedVideoBinding.ivLockClosed.setVisibility(View.VISIBLE);
                        viewHolder.itemRowSuggestedVideoBinding.ivDelete.setVisibility(View.VISIBLE);

                    }
                    mHandler.postDelayed(mRunnable, 3000);
                }
                return true;
            });
        }
        else {
            viewHolder.itemRowSuggestedVideoBinding.ivLockClosed.setVisibility(View.GONE);
            viewHolder.itemRowSuggestedVideoBinding.ivLockOpen.setVisibility(View.GONE);
            viewHolder.itemRowSuggestedVideoBinding.ivDelete.setVisibility(View.GONE);
        }

        viewHolder.itemRowSuggestedVideoBinding.ivVideo.setOnClickListener(view ->
                onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_SUGGESTION_VIDEO_CLICKED, videoListBeans.get(i), i));

        viewHolder.itemRowSuggestedVideoBinding.ivDelete.setOnClickListener(v -> {
            mHandler.removeCallbacks(mRunnable);
            new AlertDialog.Builder(context)
                    .setMessage(R.string.txt_delete_post_dialog)
                    .setPositiveButton(R.string.txt_yes, (dialogInterface, pos) ->
                            hitDeleteApi(dialogInterface, videoListBeans.get(i), i))
                    .setNegativeButton(R.string.txt_no, (dialogInterface, pos) -> {
                        setPrivacyStatusView(i, viewHolder);
                        dialogInterface.dismiss();
                    })
                    .create().show();
        });
        viewHolder.itemRowSuggestedVideoBinding.ivLockOpen.setOnClickListener(v -> {
            mHandler.removeCallbacks(mRunnable);

            onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_UNLOCK_VIEW_CLICKED, videoListBeans.get(i), i);
        });
        viewHolder.itemRowSuggestedVideoBinding.ivLockClosed.setOnClickListener(v -> onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_LOCK_VIEW_CLICKED, videoListBeans.get(i), i));

    }

    private void hitDeleteApi(DialogInterface dialogInterface, Object obj, int i) {
        onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_DELETE_VIEW_CLICKED, obj, i);
        dialogInterface.dismiss();
    }

    @Override
    public int getItemCount() {
        return videoListBeans.size();
    }

    public void setPrivacyStatusView(int position, ViewHolder viewHolder) {
        TransitionManager.beginDelayedTransition(viewHolder.itemRowSuggestedVideoBinding.rlContainer);
        viewHolder.itemRowSuggestedVideoBinding.ivDelete.setVisibility(View.GONE);
        viewHolder.itemRowSuggestedVideoBinding.ivLockOpen.setVisibility(View.GONE);
        //private
        if (videoListBeans.get(position).getPrivacyTypeId() == 1) {
            viewHolder.itemRowSuggestedVideoBinding.ivLockClosed.setVisibility(View.VISIBLE);
        } else {
            viewHolder.itemRowSuggestedVideoBinding.ivLockClosed.setVisibility(View.GONE);
        }

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ItemRowSuggestedVideoBinding itemRowSuggestedVideoBinding;

        public ViewHolder(@NonNull ItemRowSuggestedVideoBinding itemRowSuggestedVideoBinding) {
            super(itemRowSuggestedVideoBinding.getRoot());
            this.itemRowSuggestedVideoBinding = itemRowSuggestedVideoBinding;
        }

        public void bind(VideoInfo result) {
            itemRowSuggestedVideoBinding.setVariable(BR.model, result);
            itemRowSuggestedVideoBinding.executePendingBindings();
        }
    }
}

