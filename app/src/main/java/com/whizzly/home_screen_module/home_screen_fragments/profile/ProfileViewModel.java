package com.whizzly.home_screen_module.home_screen_fragments.profile;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.blockedUsers.BlockedUsersResponseBean;
import com.whizzly.models.followUnfollow.FollowUnfollowResponseBean;
import com.whizzly.models.followers_following_response.FollowersResponseBean;
import com.whizzly.models.profile.ProfileResponseBean;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.HashMap;


public class ProfileViewModel extends ViewModel {

    private RichMediatorLiveData<ProfileResponseBean> mProfileResponseBeanRichMediatorLiveData;
    private RichMediatorLiveData<FollowUnfollowResponseBean> mFollowUnfollowResponseBeanRichMediatorLiveData;
    private RichMediatorLiveData<BlockedUsersResponseBean> mBlockUnblockBeanRichMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<FailureResponse> validateLiveData;
    private OnClickSendListener onClickSendListener;
    private ProfileModel mProfileModel;
    private RichMediatorLiveData<FollowersResponseBean> mFollowersListResponseLiveData;
    private RichMediatorLiveData<FollowersResponseBean> mFollowingListResponseLiveData;



    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureObserver;
        initLiveData();
    }

    public RichMediatorLiveData<BlockedUsersResponseBean> getBlockUnblockBeanRichMediatorLiveData() {
        return mBlockUnblockBeanRichMediatorLiveData;
    }

    public RichMediatorLiveData<FollowUnfollowResponseBean> getmFollowUnfollowResponseBeanRichMediatorLiveData() {
        return mFollowUnfollowResponseBeanRichMediatorLiveData;
    }

    public RichMediatorLiveData<ProfileResponseBean> getmProfileResponseBeanRichMediatorLiveData() {
        return mProfileResponseBeanRichMediatorLiveData;
    }

    private void initLiveData() {
        if (mProfileResponseBeanRichMediatorLiveData == null) {
            mProfileResponseBeanRichMediatorLiveData = new RichMediatorLiveData<ProfileResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

        if (mFollowUnfollowResponseBeanRichMediatorLiveData == null) {
            mFollowUnfollowResponseBeanRichMediatorLiveData = new RichMediatorLiveData<FollowUnfollowResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

        if (mBlockUnblockBeanRichMediatorLiveData == null) {
            mBlockUnblockBeanRichMediatorLiveData = new RichMediatorLiveData<BlockedUsersResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
        if (mFollowersListResponseLiveData == null) {
            mFollowersListResponseLiveData = new RichMediatorLiveData<FollowersResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
        if (mFollowingListResponseLiveData == null) {
            mFollowingListResponseLiveData = new RichMediatorLiveData<FollowersResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

        if (validateLiveData == null)
            validateLiveData = new MutableLiveData<>();
    }

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }
    public RichMediatorLiveData<FollowersResponseBean> getFollowersListliveData() {
        return mFollowersListResponseLiveData;
    }


    public void onSettingsClicked()
    {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.PROFILE_SETTINGS_PRESSED);
    }

    public void onBackClick() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.SUGGESTED_BACK_PRESSED);
    }

    public void onFollowClicked()
    {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.FOLLOW_BUTTON_PRESSED);
    }

    public void setModel(ProfileModel mLoginModel) {
        this.mProfileModel = mLoginModel;
    }

    public void hitApiToFetchMyProfileData() {

        mProfileModel.hitApiToFetchMyProfile(mProfileResponseBeanRichMediatorLiveData);
    }

    public void hitApiToFetchOtherUserProfile(String userId) {
        mProfileModel.hitApiToFetchOtherUserProfile(userId, mProfileResponseBeanRichMediatorLiveData);
    }

    public void onMessageClicked(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_MESSAGE_CLICKED);
    }

    public void onEditProfileClicked()
    {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.EDIT_PROFILE_CLICKED);
    }
    public void onFollowerViewClicked(){
  onClickSendListener.onClickSend(AppConstants.ClassConstants.FOLLOWERS_VIEW_CLICKED);
    }
    public  void onFollowingViewClicked(){
    onClickSendListener.onClickSend(AppConstants.ClassConstants.FOLLOWING_VIEW_CLICKED);
    }

    public void onRankingProfileClicked(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_RANKING_CLICKED);
    }

    public String getUserId() {
        return String.valueOf(DataManager.getInstance().getUserId());
    }

    public void getFollowersListing(HashMap<String, String> hashMap) {
        mProfileModel.getFollowersListing(hashMap,mFollowersListResponseLiveData);

    }

    public void getFollowingListing(HashMap<String, String> hashMap) {
        mProfileModel.getFollowingListing(hashMap,mFollowingListResponseLiveData);

    }

    public RichMediatorLiveData<FollowersResponseBean> getFollowingListLiveData() {
        return mFollowingListResponseLiveData;
    }
    public void onUnBlockViewClicked(){
    onClickSendListener.onClickSend(AppConstants.ClassConstants.UNBLOCKED_VIEW_CLICKED);
    }
}

