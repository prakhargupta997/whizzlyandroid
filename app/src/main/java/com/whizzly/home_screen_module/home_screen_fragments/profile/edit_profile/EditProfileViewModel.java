package com.whizzly.home_screen_module.home_screen_fragments.profile.edit_profile;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.submit_profile.SubmitProfileBean;
import com.whizzly.utils.AppConstants;

public class EditProfileViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;
    private EditProfileModel mEditProfileModel;
    private RichMediatorLiveData<SubmitProfileBean> mSubmitResponseBeanRichMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<FailureResponse> validateLiveData;


    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureObserver;
        initLiveData();
    }


    private void initLiveData() {
        if (mSubmitResponseBeanRichMediatorLiveData == null) {
            mSubmitResponseBeanRichMediatorLiveData = new RichMediatorLiveData<SubmitProfileBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }


        if (validateLiveData == null)
            validateLiveData = new MutableLiveData<>();
    }

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void setModel(EditProfileModel mEditProfileModel) {
        this.mEditProfileModel = mEditProfileModel;
    }

    public void onBackClick() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.SUGGESTED_BACK_PRESSED);
    }

    public void onChangeProfileClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.CHANGE_PROFILE_CLICKED);
    }

    public void onSaveButtonClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_EDIT_PROFILE_SAVE);
    }

    public RichMediatorLiveData<SubmitProfileBean> getEditProfileResponseLiveData() {
        return mSubmitResponseBeanRichMediatorLiveData;
    }


}
