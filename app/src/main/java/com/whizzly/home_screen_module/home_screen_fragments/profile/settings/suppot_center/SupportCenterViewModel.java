package com.whizzly.home_screen_module.home_screen_fragments.profile.settings.suppot_center;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.chat.model.Message;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.change_password.ChangePasswordModel;
import com.whizzly.models.submit_profile.SubmitProfileBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class SupportCenterViewModel extends ViewModel {
    private RichMediatorLiveData<SubmitProfileBean> mSuppotCenterLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<FailureResponse> validateLiveData;
    private OnClickSendListener onClickSendListener;
    private SupportCenterModel mSupportCenterModel;

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }
    public void onCancelViewClicked(){
      onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CLICK_CANCEL);
    }
    public void onSubmitViewClicked(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.SUBMIT_CLICKED);
    }

    public void onMessageClicked(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.MESSAGE_CLICKED);
    }

    public void setModel(SupportCenterModel mSupportCenterModel) {
        this.mSupportCenterModel=mSupportCenterModel;
    }

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureResponseObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (mSuppotCenterLiveData == null) {
            mSuppotCenterLiveData = new RichMediatorLiveData<SubmitProfileBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }


        if (validateLiveData == null)
            validateLiveData = new MutableLiveData<>();
    }

    public RichMediatorLiveData<SubmitProfileBean> getSupportCenterLiveData() {
        return mSuppotCenterLiveData;
    }

    public void hitChatNotificationApi(Message message, String roomId) {
        DataManager.getInstance().hitChatMessageNotificationApi(getChatData(message, roomId)).enqueue(new NetworkCallback<ChangePasswordModel>() {
            @Override
            public void onSuccess(ChangePasswordModel notificationResponse) {

            }

            @Override
            public void onFailure(FailureResponse failureResponse) {

            }

            @Override
            public void onError(Throwable t) {

            }
        });
    }

    public HashMap<String, String> getChatData(Message message, String roomId) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(AppConstants.ClassConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        hashMap.put(AppConstants.NetworkConstants.SENDER_USER_ID, message.idSender);
        hashMap.put(AppConstants.NetworkConstants.RECIEVER_USER_ID, message.idReceiver);
        hashMap.put(AppConstants.NetworkConstants.CHAT_USER_NAME, DataManager.getInstance().getUserName());
        hashMap.put(AppConstants.NetworkConstants.MESSAGE, message.text);
        hashMap.put(AppConstants.NetworkConstants.TIME, String.valueOf(message.timestamp));
        hashMap.put(AppConstants.NetworkConstants.ROOM_ID, roomId);
        hashMap.put(AppConstants.NetworkConstants.TEXT_TYPE, message.messageType);
        hashMap.put(AppConstants.NetworkConstants.PROFILE_PIC, DataManager.getInstance().getProfilePic());
        return hashMap;
    }
}
