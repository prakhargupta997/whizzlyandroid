package com.whizzly.home_screen_module.home_screen_fragments.notification.personal;


import android.app.Activity;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.chat.InboxListActivity;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.FragmentPersonalNotificationsBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.interfaces.OnBackPressedListener;
import com.whizzly.models.notifications.Result;
import com.whizzly.notification.NotificationActivity;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

public class PersonalNotificationsFragment extends Fragment implements OnClickAdapterListener, OnBackPressedListener {
    private PersonalNotificationsViewModel mPersonalNotificationsViewModel;
    private PersonalNotificationAdapter mAdapter;
    private FragmentPersonalNotificationsBinding mFragmentPersonalNotificationsBinding;
    private Activity mActivity;
    private ArrayList<Result> mNotificationsList;
    private int mPosition;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mActivity = getActivity();
        mFragmentPersonalNotificationsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_personal_notifications, container, false);
        mPersonalNotificationsViewModel = ViewModelProviders.of(this).get(PersonalNotificationsViewModel.class);
        mFragmentPersonalNotificationsBinding.setViewModel(mPersonalNotificationsViewModel);
        mPersonalNotificationsViewModel.setGenericListeners(((HomeActivity) mActivity).getErrorObserver(), ((HomeActivity) mActivity).getFailureResponseObserver());
        observeLiveData();
        mNotificationsList = new ArrayList<>();
        setUpAdapter();

        setViews();
        configureSwipeRefresh();
        ((HomeActivity)getActivity()).setOnBackPressedListener(this);
        return mFragmentPersonalNotificationsBinding.getRoot();
    }

    private void configureSwipeRefresh(){
        mFragmentPersonalNotificationsBinding.srlNotification.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPersonalNotificationsViewModel.getNotificationListing();
            }
        });
    }

    private void setViews() {
        mFragmentPersonalNotificationsBinding.toolbar.ivNotificationOptions.setVisibility(View.VISIBLE);
        mFragmentPersonalNotificationsBinding.toolbar.tvToolbarText.setText(getString(R.string.txt_notifications));
        mFragmentPersonalNotificationsBinding.toolbar.ivNotificationOptions.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), InboxListActivity.class);
            startActivity(intent);
        });

        if(DataManager.getInstance().isNotchDevice()){
            mFragmentPersonalNotificationsBinding.toolbar.view.setVisibility(View.VISIBLE);
        }else{
            mFragmentPersonalNotificationsBinding.toolbar.view.setVisibility(View.GONE);
        }
    }

    private void observeLiveData() {
        mPersonalNotificationsViewModel.getNotificationLiveData().observe(this, notificationResponse -> {
            mFragmentPersonalNotificationsBinding.flProgressBar.setVisibility(View.GONE);
            if (notificationResponse != null) {
                if(mFragmentPersonalNotificationsBinding.srlNotification.isRefreshing()){
                    mFragmentPersonalNotificationsBinding.srlNotification.setRefreshing(false);
                }
                if (notificationResponse.getCode() == 301 || notificationResponse.getCode() == 302) {
                    Toast.makeText(mActivity, notificationResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) Objects.requireNonNull(getActivity())).autoLogOut();
                } else if (notificationResponse.getCode() == 200) {
                    if (notificationResponse.getResult()!=null && notificationResponse.getResult().size()>0) {
                        mNotificationsList.clear();
                        mFragmentPersonalNotificationsBinding.ivNoData.setVisibility(View.GONE);
                        mNotificationsList.addAll(notificationResponse.getResult());
                        mAdapter.notifyDataSetChanged();
                    }
                    else {
                        mFragmentPersonalNotificationsBinding.ivNoData.setVisibility(View.VISIBLE);
                    }
                } else {
                    Toast.makeText(getActivity(), notificationResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

        });
        mPersonalNotificationsViewModel.getFollowUnfollowLiveData().observe(this, followUnfollowResponseBean -> {
            if (followUnfollowResponseBean != null && followUnfollowResponseBean.getCode() == 200) {
                if (mNotificationsList.get(mPosition).getIsFollowed() == 1)
                    mNotificationsList.get(mPosition).setIsFollowed(0);
                else
                    mNotificationsList.get(mPosition).setIsFollowed(1);
                mAdapter.notifyItemChanged(mPosition);

            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPersonalNotificationsViewModel.getNotificationListing();

    }

    private void setUpAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        mFragmentPersonalNotificationsBinding.rvNotifications.setLayoutManager(layoutManager);
        mAdapter = new PersonalNotificationAdapter(this, mNotificationsList, mActivity);
        mFragmentPersonalNotificationsBinding.rvNotifications.setAdapter(mAdapter);
    }

    @Override
    public void onAdapterClicked(int code, Object obj, int position) {
        switch (code) {

            case AppConstants.ClassConstants.FOLLOW_BUTTON_PRESSED:
                mPosition = position;
                if (obj != null && obj instanceof Result) {
                    Result result = (Result) obj;
                    mPersonalNotificationsViewModel.OnFollowButtonViewClicked(result.getUserId(),   result.getIsFollowed());
                }
                break;
            case AppConstants.ClassConstants.USER_NAME_VIEW_CLICKED:
                if (obj != null && obj instanceof Result) {
                    Result result = (Result) obj;
                    ((HomeActivity) getActivity()).openOtherUserProfile(result.getUserId());
                }
                break;
            case AppConstants.ClassConstants.ON_NOTIFICATION_CLICKED:
                if (obj != null && obj instanceof Result) {
                    Result result = (Result) obj;
                    if (result.getEventType().equalsIgnoreCase(getString(R.string.txt_follow))) {
                        ((HomeActivity) getActivity()).openOtherUserProfile(result.getUserId());
                    } else {
                        Intent intent = new Intent(getActivity(), NotificationActivity.class);
                        intent.putExtra(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.FROM_NOTIFICATION);
                        intent.putExtra(AppConstants.ClassConstants.NOTIFICATION_DATA, result);
                        startActivity(intent);
                        break;
                    }
                }
                break;

        }

    }

    @Override
    public void onBackPressed() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((HomeActivity)getActivity()).onClickSend(AppConstants.ClassConstants.ON_HOME_CLICKED);
            }
        });
    }
}
