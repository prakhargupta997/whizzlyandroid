package com.whizzly.home_screen_module.home_screen_fragments.user;

import android.content.Context;
import androidx.databinding.BaseObservable;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.search.users.UserSearchResponseBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import java.util.HashMap;


public class UserModel extends BaseObservable {
    private Context context;
    public UserModel(Context context) {
        this.context=context;
    }

    public void hitUserListingApi(String search, int mPage, RichMediatorLiveData<UserSearchResponseBean> mUserSearchResponseBeanLiveData) {
        if(AppUtils.isNetworkAvailable(context)) {

            DataManager.getInstance().hitUserSearchApi(getSearchData(search,mPage)).enqueue(new NetworkCallback<UserSearchResponseBean>() {
                @Override
                public void onSuccess(UserSearchResponseBean userSearchResponseBean) {
                    mUserSearchResponseBeanLiveData.setValue(userSearchResponseBean);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    mUserSearchResponseBeanLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    mUserSearchResponseBeanLiveData.setError(t);
                }
            });

        }else {
            Toast.makeText(context, context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    private HashMap<String, String> getSearchData(String seachText, int mPage) {
        HashMap<String,String> hashMap=new HashMap();
        hashMap.put(AppConstants.NetworkConstants.USER_ID,String.valueOf(DataManager.getInstance().getUserId()));
        hashMap.put(AppConstants.NetworkConstants.PAGE,String.valueOf(mPage));
        hashMap.put(AppConstants.NetworkConstants.SEARCH,seachText);
        return hashMap;

}
}
