package com.whizzly.home_screen_module.home_screen_fragments.profile.settings.blocked_users;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.ItemRowBlockedUserBinding;
import com.whizzly.models.blockedUsers.Result;
import com.whizzly.utils.AppConstants;

import java.util.ArrayList;

public class BlockedUsersAdapter extends RecyclerView.Adapter<BlockedUsersAdapter.ViewHolder> {

    private ItemRowBlockedUserBinding itemRowBlockedUserBinding;
    private ViewGroup viewGroup;
    private ArrayList<Result> users;
    private OnClickAdapterListener onClickAdapterListener;

    public BlockedUsersAdapter(ArrayList<Result> users, OnClickAdapterListener onClickAdapterListener) {
        if (this.users != null)
            this.users.clear();
        this.users = users;
        this.onClickAdapterListener = onClickAdapterListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        itemRowBlockedUserBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_row_blocked_user, viewGroup, false);
        this.viewGroup = viewGroup;
        return new BlockedUsersAdapter.ViewHolder(itemRowBlockedUserBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(users.get(i));
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.circleCrop();
        requestOptions.placeholder(R.drawable.ic_profile_setup_user_placeholder);
        requestOptions.error(R.drawable.ic_profile_setup_user_placeholder);
        Glide.with(itemRowBlockedUserBinding.getRoot()).load(users.get(i).getProfileImage()).apply(requestOptions).into(itemRowBlockedUserBinding.ivProfile);
        itemRowBlockedUserBinding.tvName.setText(users.get(i).getFirstName());
        itemRowBlockedUserBinding.tvUsername.setText(users.get(i).getUserName());

        viewHolder.itemRowBlockedUserBinding.tvRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_REMOVE_BLOCKED_USER_CLICKED, users.get(i), i);

            }
        });
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends  RecyclerView.ViewHolder {
        ItemRowBlockedUserBinding itemRowBlockedUserBinding;

        public ViewHolder(@NonNull ItemRowBlockedUserBinding itemRowBlockedUserBinding) {
            super(itemRowBlockedUserBinding.getRoot());
            this.itemRowBlockedUserBinding = itemRowBlockedUserBinding;
        }

        public void bind(Result result) {
            itemRowBlockedUserBinding.setVariable(BR.model, result);
            itemRowBlockedUserBinding.executePendingBindings();
        }
    }
}
