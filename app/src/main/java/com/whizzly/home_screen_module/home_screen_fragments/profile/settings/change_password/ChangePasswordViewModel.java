package com.whizzly.home_screen_module.home_screen_fragments.profile.settings.change_password;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.utils.AppConstants;

public class ChangePasswordViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;
    private ChangePasswordModel mChangePasswordModel;
    private RichMediatorLiveData<com.whizzly.models.change_password.ChangePasswordModel> mChangePasswordMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<FailureResponse> validateLiveData;

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public RichMediatorLiveData<com.whizzly.models.change_password.ChangePasswordModel> getChangePasswordMediatorLiveData()
    {
        return mChangePasswordMediatorLiveData;
    }

    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (mChangePasswordMediatorLiveData == null) {
            mChangePasswordMediatorLiveData = new RichMediatorLiveData<com.whizzly.models.change_password.ChangePasswordModel>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }


        if (validateLiveData == null)
            validateLiveData = new MutableLiveData<>();
    }

    public void setModel(ChangePasswordModel mSettingModel) {
        this.mChangePasswordModel = mSettingModel;
    }

    public void onSaveClicked()
    {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.CHANGE_PASSWORD_SAVE_CLICKED);
    }

    public void onBackClick() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.SUGGESTED_BACK_PRESSED);
    }
}

