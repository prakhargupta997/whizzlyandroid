package com.whizzly.home_screen_module.home_screen_fragments.videos;


import android.app.Activity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.FragmentVideosBinding;
import com.whizzly.models.search.search_video.Result;
import com.whizzly.models.search.search_video.VideoList;
import com.whizzly.search_module.hashtagvideos.HashTagVideosActivity;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoListingFragment extends Fragment implements OnClickAdapterListener {

    private FragmentVideosBinding mFragmentVideosBinding;
    private VideosViewModel mVideosViewModel;
    private VideosModel mVideosModel;
    private Activity mActivity;
    private VideoListingAdapter mVideosAdapter;
    private int mPage = 1;
    private ArrayList<VideoList> mVideoList;
    private String mSearch = "";

    public VideoListingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        mFragmentVideosBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_videos, container, false);
        mActivity = getActivity();
        mVideosViewModel = ViewModelProviders.of(this).get(VideosViewModel.class);
        mFragmentVideosBinding.setViewModel(mVideosViewModel);
        mVideosModel = new VideosModel(mActivity);
        mFragmentVideosBinding.setModel(mVideosModel);
        mVideosViewModel.setGenericListeners(((BaseActivity) getActivity()).getErrorObserver(), ((BaseActivity) getActivity()).getFailureResponseObserver());
        mVideoList = new ArrayList<>();
        setRecylerView();
        return mFragmentVideosBinding.getRoot();
    }


    private void setRecylerView() {
        mFragmentVideosBinding.rvVideos.setLayoutManager(new LinearLayoutManager(getActivity()));
        mVideosAdapter = new VideoListingAdapter(mVideoList, this, mActivity);
        mFragmentVideosBinding.rvVideos.setAdapter(mVideosAdapter);
    }


    @Override
    public void onAdapterClicked(int code, Object obj, int position) {
        if (obj != null && (obj instanceof VideoList)) {
            VideoList videoList = (VideoList) obj;
            Intent intent = new Intent(mActivity, HashTagVideosActivity.class);
            intent.putExtra(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_VIDEO_LISTING);
            intent.putExtra(AppConstants.ClassConstants.VIDEO_DETAILS, videoList.getKey());
            startActivity(intent);
        }
    }

    public void setUserData(Result result) {
        try {
            mVideoList.clear();
                if (result.getVideoList() != null && result.getVideoList().size() > 0)
                    mVideoList.addAll(result.getVideoList());
                if (mVideoList.size() > 0)
                    mFragmentVideosBinding.tvError.setVisibility(View.GONE);
                else
                    mFragmentVideosBinding.tvError.setVisibility(View.VISIBLE);
                mVideosAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

