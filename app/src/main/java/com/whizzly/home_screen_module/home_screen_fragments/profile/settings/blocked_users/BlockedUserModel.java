package com.whizzly.home_screen_module.home_screen_fragments.profile.settings.blocked_users;

import android.content.Context;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableField;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.blockedUsers.BlockedUsersResponseBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class BlockedUserModel extends BaseObservable {

    private final Context context;

    BlockedUserModel(Context context)
    {
        this.context = context;
    }


    public void hitApiToFetchBlockedList(RichMediatorLiveData<BlockedUsersResponseBean> mBlockedUsersRichMediatorLiveData)
    {

        if (AppUtils.isNetworkAvailable(context)) {

            DataManager.getInstance().getBlockedUsersList(getData()).enqueue(new NetworkCallback<BlockedUsersResponseBean>() {
                @Override
                public void onSuccess(BlockedUsersResponseBean blockedUsersBean) {
                    mBlockedUsersRichMediatorLiveData.setValue(blockedUsersBean);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    mBlockedUsersRichMediatorLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    mBlockedUsersRichMediatorLiveData.setError(t);
                }
            });

        }
        else {
            Toast.makeText(context, context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    private HashMap<String, String> getData() {

        HashMap<String, String> params = new HashMap<>();

        String userId = String.valueOf(DataManager.getInstance().getUserId());

        params.put(AppConstants.NetworkConstants.PARAM_USER_ID, userId);


        return params;
    }

    public void hitApiToUnblockUser(RichMediatorLiveData<BlockedUsersResponseBean> blockedUsersRichMediatorLiveData, String affectedUserId) {

        if (AppUtils.isNetworkAvailable(context)) {

            DataManager.getInstance().hitBlockUnblockUserApi(getUnblockData(affectedUserId)).enqueue(new NetworkCallback<BlockedUsersResponseBean>() {
                @Override
                public void onSuccess(BlockedUsersResponseBean blockedUsersBean) {
                    blockedUsersRichMediatorLiveData.setValue(blockedUsersBean);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    blockedUsersRichMediatorLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    blockedUsersRichMediatorLiveData.setError(t);
                }
            });

        }
        else {
            Toast.makeText(context, context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    private HashMap<String, String> getUnblockData(String affectedUserId) {

        HashMap<String, String> params = new HashMap<>();

        String userId = String.valueOf(DataManager.getInstance().getUserId());

        params.put(AppConstants.NetworkConstants.PARAM_USER_ID, userId);
        params.put(AppConstants.NetworkConstants.PARAM_AFFECTED_USER_ID, affectedUserId);
        params.put(AppConstants.NetworkConstants.PARAM_BLOCK_ACTION, context.getString(R.string.txt_unblock));
        params.put(AppConstants.NetworkConstants.PARAM_REASON, "demo reason");


        return params;
    }
}
