package com.whizzly.home_screen_module.home_screen_fragments.profile.posted;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.FragmentProfileVideosBinding;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.privacy_status.PrivacyStatusResponseBean;
import com.whizzly.models.profileVideo.VideoInfo;
import com.whizzly.notification.NotificationActivity;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.EndlessRecyclerOnScrollListener;
import com.whizzly.utils.SpacesItemDecorationGrid;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

public class ProfileVideosFragment extends Fragment implements OnClickSendListener, OnClickAdapterListener {

    private ProfileVideosModel mPostedModel;
    private ProfileVideosViewModel mPostedViewModel;
    private Activity mActivity;
    private FragmentProfileVideosBinding mFragmentProfileVideosBinding;
    private ProfileVideosAdapter mPostedAdapter;
    private ArrayList<VideoInfo> videoListResponse;
    private int accountType;
    private int userId;
    private int type;
    private int mPosition;
    private int next = 1;
    private boolean isFromRefresh = false;

    public ProfileVideosFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void getArgumentData() {
        if (getArguments() != null && getArguments().containsKey(AppConstants.ClassConstants.ACCOUNT_TYPE)) {
            accountType = getArguments().getInt(AppConstants.ClassConstants.ACCOUNT_TYPE);
            type = getArguments().getInt(AppConstants.ClassConstants.VIDEO_TYPE);
            mPostedModel.setVideoType(type);
        }

        if (accountType == AppConstants.OTHER_USER_PROFILE) {
            userId = Integer.parseInt(getArguments().getString(AppConstants.ClassConstants.USER_ID));
        } else {
            userId = mPostedViewModel.getUserIdFromPrefs();
        }
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mActivity = getActivity();
        mFragmentProfileVideosBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile_videos, container, false);
        mPostedViewModel = ViewModelProviders.of(this).get(ProfileVideosViewModel.class);
        mFragmentProfileVideosBinding.setViewModel(mPostedViewModel);
        mPostedModel = new ProfileVideosModel(mActivity);
        mFragmentProfileVideosBinding.setModel(mPostedModel);
        mPostedViewModel.setOnClickSendListener(this);
        mPostedViewModel.setModel(mPostedModel);
        mPostedViewModel.setGenericListeners(((BaseActivity) mActivity).getErrorObserver(), ((BaseActivity) mActivity).getFailureResponseObserver());
        videoListResponse = new ArrayList<>();
        getArgumentData();
        setRecyclerView();
        observeLiveData();
        return mFragmentProfileVideosBinding.getRoot();
    }

    private void observeLiveData() {

        mPostedViewModel.getProfileVideosRichMediatorLiveData().observe(this, profileVideosResponseBean -> {
            if (profileVideosResponseBean != null) {
                if (profileVideosResponseBean.getCode() == 200) {
                    if (profileVideosResponseBean.getResult().getVideoInfo() != null && profileVideosResponseBean.getResult().getVideoInfo().size() > 0) {
                        mFragmentProfileVideosBinding.tvError.setVisibility(View.GONE);
                        next = profileVideosResponseBean.getResult().getNext();
                        if (videoListResponse != null && isFromRefresh) {
                            videoListResponse.clear();
                        }
                        Collections.reverse(profileVideosResponseBean.getResult().getVideoInfo());
                        videoListResponse.addAll(profileVideosResponseBean.getResult().getVideoInfo());
                        mPostedAdapter.notifyDataSetChanged();
                    } else {
                        videoListResponse.clear();
                        mFragmentProfileVideosBinding.tvError.setVisibility(View.VISIBLE);
                        if (type == AppConstants.VIDEO_TYPE_SAVED)
                            mFragmentProfileVideosBinding.tvError.setText(R.string.s_no_saved_videos);
                        else
                            mFragmentProfileVideosBinding.tvError.setText(R.string.s_no_posted_videos);
                    }
                    mPostedAdapter.notifyDataSetChanged();
                } else if (profileVideosResponseBean.getCode() == 301 || profileVideosResponseBean.getCode() == 302) {
                    Toast.makeText(mActivity, profileVideosResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) Objects.requireNonNull(getActivity())).autoLogOut();
                } else {
                    Toast.makeText(mActivity, profileVideosResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

        });

        mPostedViewModel.getDeleteVideoLiveData().observe(this, deleteResponse -> {
            if (deleteResponse != null) {
                if (deleteResponse.getCode().equals("301") || deleteResponse.getCode().equals("302")) {
                    Toast.makeText(mActivity, deleteResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) Objects.requireNonNull(mActivity)).autoLogOut();
                } else if (deleteResponse.getCode().equals("200")) {
                    videoListResponse.remove(mPosition);
                    mPostedAdapter.notifyItemRemoved(mPosition);
                    Toast.makeText(mActivity, deleteResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    if (videoListResponse.size() == 0) {
                        mFragmentProfileVideosBinding.tvError.setVisibility(View.VISIBLE);
                        mFragmentProfileVideosBinding.tvError.setText(R.string.s_no_saved_videos);
                    }

                }
            }


        });
        mPostedViewModel.getVideoPrivacyStatusLiveData().observe(this, new Observer<PrivacyStatusResponseBean>() {
            @Override
            public void onChanged(@Nullable PrivacyStatusResponseBean privacyStatusResponse) {
                if (privacyStatusResponse != null) {
                    if (privacyStatusResponse.getCode() == 301 || privacyStatusResponse.getCode() == 302) {
                        Toast.makeText(mActivity, privacyStatusResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        ((BaseActivity) Objects.requireNonNull(mActivity)).autoLogOut();
                    } else if (privacyStatusResponse.getCode() == 200) {
                        if (privacyStatusResponse.getResult().getIsLocked() == 2) {
                            if (videoListResponse.get(mPosition).getIsMyVideo() == 1) {
                                videoListResponse.remove(mPosition);
                                mPostedAdapter.notifyItemRemoved(mPosition);
                                if (next > 0)
                                    hitVideoApi(next, next, false);
                                else hitVideoApi(1, 10, false);
                            } else
                                videoListResponse.get(mPosition).setPrivacyTypeId(2);
                        } else if (privacyStatusResponse.getResult().getIsLocked() == 1)
                            videoListResponse.get(mPosition).setPrivacyTypeId(1);

                        mPostedAdapter.notifyItemChanged(mPosition);
                        Toast.makeText(mActivity, privacyStatusResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    } else
                        Toast.makeText(mActivity, privacyStatusResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void setRecyclerView() {
        GridLayoutManager layoutManager = new GridLayoutManager(mActivity, 3);
        mFragmentProfileVideosBinding.rvVid.setLayoutManager(layoutManager);
        mPostedAdapter = new ProfileVideosAdapter(getActivity(), videoListResponse, this, type, accountType);
        mFragmentProfileVideosBinding.rvVid.addItemDecoration(new SpacesItemDecorationGrid(mActivity, 1, 3));
        mFragmentProfileVideosBinding.rvVid.setAdapter(mPostedAdapter);
        mFragmentProfileVideosBinding.rvVid.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                if (next > 0)
                    hitVideoApi(next, next, false);
            }
        });

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (AppUtils.isNetworkAvailable(getActivity())) {
            if (next > 0)
                hitVideoApi(next, next, true);
            else hitVideoApi(1, 10, true);
        }
    }

    @Override
    public void onClickSend(int code) {

    }

    public void hitVideoApi(int next, int pageCount, boolean isFromRefresh) {
        mPostedViewModel.hitApiToFetchPostedVideos(userId, next, pageCount);
        this.isFromRefresh = isFromRefresh;
    }

    public ArrayList<VideoInfo> getVideoListResponse() {
        return videoListResponse;
    }

    @Override
    public void onAdapterClicked(int code, Object obj, int position) {
        switch (code) {
            case AppConstants.ClassConstants.ON_SUGGESTION_VIDEO_CLICKED:
                if (accountType == AppConstants.MY_PROFILE) {
                    if (obj instanceof VideoInfo) {
                        VideoInfo videoData = (VideoInfo) obj;
                        Intent intent = new Intent(getActivity(), NotificationActivity.class);
                        intent.putExtra(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.FROM_PROFILE_VIDEOS);
                        intent.putExtra(AppConstants.ClassConstants.USER_ID, userId);
                        intent.putExtra(AppConstants.ClassConstants.NOTIFICATION_DATA, videoData);
                        startActivity(intent);
                        /*FeedVideoPreviewFragment feedVideoPreviewFragment = new FeedVideoPreviewFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(AppConstants.ClassConstants.VIDEO_DETAILS, (VideoInfo) obj);
                        bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_PROFILE);
                        bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.MY_PROFILE);
                        feedVideoPreviewFragment.setArguments(bundle);
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, feedVideoPreviewFragment).commit();*/
                    }
                } else if (accountType == AppConstants.OTHER_USER_PROFILE) {
                    if (obj instanceof VideoInfo) {
                        VideoInfo videoData = (VideoInfo) obj;
                        Intent intent = new Intent(getActivity(), NotificationActivity.class);
                        intent.putExtra(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.FROM_PROFILE_VIDEOS);
                        intent.putExtra(AppConstants.ClassConstants.USER_ID, userId);
                        intent.putExtra(AppConstants.ClassConstants.NOTIFICATION_DATA, videoData);
                        startActivity(intent);
                        /*
                        FeedVideoPreviewFragment feedVideoPreviewFragment = new FeedVideoPreviewFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(AppConstants.ClassConstants.VIDEO_DETAILS, (VideoInfo) obj);
                        bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_PROFILE);
                        bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.OTHER_USER_PROFILE);
                        feedVideoPreviewFragment.setArguments(bundle);
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, feedVideoPreviewFragment).commit();*/
                    }
                }
                break;
            case AppConstants.ClassConstants.ON_DELETE_VIEW_CLICKED:
                mPosition = position;
                if (obj != null && obj instanceof VideoInfo) {
                    VideoInfo videoInfo = (VideoInfo) obj;
                    int videoType;
                    if (videoInfo.getType().equalsIgnoreCase(getString(R.string.txt_collab)))
                        videoType = 2;
                    else
                        videoType = 1;
                    mPostedViewModel.hitDeleteApi(videoInfo.getId(), videoType, videoInfo.getIsMyVideo());
                }

                break;
            case AppConstants.ClassConstants.ON_UNLOCK_VIEW_CLICKED:
                if (obj != null && obj instanceof VideoInfo) {
                    VideoInfo videoInfo = (VideoInfo) obj;
                    int videoType;
                    mPosition = position;
                    if (videoInfo.getType().equalsIgnoreCase(getString(R.string.txt_collab)))
                        videoType = 2;
                    else
                        videoType = 1;
                    mPostedViewModel.hitVideoPrivacyTypeApi(videoInfo.getId(), videoType, videoInfo.getPrivacyTypeId(), videoInfo.getIsMyVideo());
                }
                break;
            case AppConstants.ClassConstants.ON_LOCK_VIEW_CLICKED:
                if (obj != null && obj instanceof VideoInfo) {
                    VideoInfo videoInfo = (VideoInfo) obj;
                    int videoType;
                    mPosition = position;
                    if (videoInfo.getType().equalsIgnoreCase(getString(R.string.txt_collab)))
                        videoType = 2;
                    else
                        videoType = 1;
                    mPostedViewModel.hitVideoPrivacyTypeApi(videoInfo.getId(), videoType, videoInfo.getPrivacyTypeId(), videoInfo.getIsMyVideo());
                }
                break;
        }

    }


    private void hitDeleteApi(DialogInterface dialogInterface, Object obj, int position) {
        if (obj != null && obj instanceof VideoInfo) {
            VideoInfo videoInfo = (VideoInfo) obj;
            int videoType;
            mPosition = position;
            if (videoInfo.getType().equalsIgnoreCase(getString(R.string.txt_collab)))
                videoType = 2;
            else
                videoType = 1;
            mPostedViewModel.hitDeleteApi(videoInfo.getId(), videoType, videoInfo.getIsMyVideo());
            dialogInterface.dismiss();

        }
    }
}

