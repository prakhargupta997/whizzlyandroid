package com.whizzly.home_screen_module.home_screen_fragments.profile.settings.blocked_users;

import android.app.Activity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.FragmentBlockedUsersBinding;
import com.whizzly.home_screen_module.home_screen_activities.InnerFragmentsHostActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.blockedUsers.BlockedUsersResponseBean;
import com.whizzly.models.blockedUsers.Result;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

public class BlockedUserFragment extends Fragment implements OnClickSendListener, OnClickAdapterListener {

    private Activity mActivity;
    private FragmentBlockedUsersBinding mFragmentBlockedUserBinding;
    private BlockedUserModel mBlockedUserModel;
    private BlockedUserViewModel mBlockedUserViewModel;
    private BlockedUsersAdapter blockedUsersAdapter;
    private ArrayList<Result> blockedUsers;
    private int mPosition;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mActivity = getActivity();
        mFragmentBlockedUserBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_blocked_users, container, false);
        mBlockedUserViewModel = ViewModelProviders.of(this).get(BlockedUserViewModel.class);
        mBlockedUserViewModel.setGenericListeners(((InnerFragmentsHostActivity) getActivity()).getErrorObserver(), ((InnerFragmentsHostActivity) getActivity()).getFailureResponseObserver());
        mFragmentBlockedUserBinding.setViewModel(mBlockedUserViewModel);
        mBlockedUserModel = new BlockedUserModel(mActivity);
        mFragmentBlockedUserBinding.setModel(mBlockedUserModel);
        mBlockedUserViewModel.setOnClickSendListener(this);
        mBlockedUserViewModel.setModel(mBlockedUserModel);
        blockedUsers = new ArrayList<>();

        observerBlockedUsersResponse();
        observerUnblockUserResponse();

        if(DataManager.getInstance().isNotchDevice()){
            mFragmentBlockedUserBinding.view.setVisibility(View.VISIBLE);
        }else{
            mFragmentBlockedUserBinding.view.setVisibility(View.GONE);
        }

        setRecyclerView();
        return mFragmentBlockedUserBinding.getRoot();
    }

    private void observerUnblockUserResponse() {

        mBlockedUserViewModel.getmUnblockedUserRichMediatorLiveData().observe(this, new Observer<BlockedUsersResponseBean>() {
            @Override
            public void onChanged(@Nullable BlockedUsersResponseBean blockedUsersResponseBean) {
                mFragmentBlockedUserBinding.bar.setVisibility(View.GONE);
                if (blockedUsersResponseBean != null) {
                    if (blockedUsersResponseBean.getCode() == 200) {
                        blockedUsers.remove(mPosition);
                        blockedUsersAdapter.notifyItemRemoved(mPosition);
                        Toast.makeText(mActivity, blockedUsersResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                        if (blockedUsers.size() > 0)
                            mFragmentBlockedUserBinding.tvNoData.setVisibility(View.GONE);
                        else
                            mFragmentBlockedUserBinding.tvNoData.setVisibility(View.VISIBLE);

                    } else if (blockedUsersResponseBean.getCode() == 301 || blockedUsersResponseBean.getCode() == 302) {
                        Toast.makeText(mActivity, blockedUsersResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                        ((BaseActivity) Objects.requireNonNull(getActivity())).autoLogOut();

                    } else
                        Toast.makeText(mActivity, blockedUsersResponseBean.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    private void observerBlockedUsersResponse() {

        mBlockedUserViewModel.getBlockedUsersRichMediatorLiveData().observe(this, new Observer<BlockedUsersResponseBean>() {
            @Override
            public void onChanged(@Nullable BlockedUsersResponseBean blockedUsersResponseBean) {
                mFragmentBlockedUserBinding.bar.setVisibility(View.GONE);
                if (blockedUsersResponseBean != null) {
                    if (blockedUsersResponseBean.getCode() == 200) {
                        if (blockedUsersResponseBean.getResult() != null && blockedUsersResponseBean.getResult().size() > 0) {
                            blockedUsers.clear();
                            blockedUsers.addAll(blockedUsersResponseBean.getResult());
                            blockedUsersAdapter.notifyDataSetChanged();
                        } else {
                            mFragmentBlockedUserBinding.tvNoData.setVisibility(View.VISIBLE);
                        }
                    } else if (blockedUsersResponseBean.getCode() == 301 || blockedUsersResponseBean.getCode() == 302) {
                        Toast.makeText(mActivity, blockedUsersResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                        ((BaseActivity) Objects.requireNonNull(getActivity())).autoLogOut();
                    }
                }
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (AppUtils.isNetworkAvailable(getActivity())) {
            mFragmentBlockedUserBinding.bar.setVisibility(View.VISIBLE);
            mBlockedUserModel.hitApiToFetchBlockedList(mBlockedUserViewModel.getBlockedUsersRichMediatorLiveData());
        } else
            Toast.makeText(mActivity, mActivity.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClickSend(int code) {

        switch (code) {
            case AppConstants.ClassConstants.ON_BLOCKED_BACK_PRESSED:
                mActivity.onBackPressed();
                break;
        }
    }

    private void setRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mActivity);
        mFragmentBlockedUserBinding.rvMain.setLayoutManager(layoutManager);
        blockedUsersAdapter = new BlockedUsersAdapter(blockedUsers, this);
        mFragmentBlockedUserBinding.rvMain.setAdapter(blockedUsersAdapter);
    }

    @Override
    public void onAdapterClicked(int code, Object obj, int position) {

        mPosition = position;

        Result blockedUser = (Result) obj;

        mFragmentBlockedUserBinding.bar.setVisibility(View.VISIBLE);
        mBlockedUserModel.hitApiToUnblockUser(mBlockedUserViewModel.getmUnblockedUserRichMediatorLiveData(), blockedUser.getUserId());


    }
}
