package com.whizzly.home_screen_module.home_screen_fragments.user;



import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.whizzly.R;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.ItemRowBlockedUserBinding;
import com.whizzly.models.search.users.UserList;
import com.whizzly.utils.AppConstants;

import java.util.ArrayList;

public class UserListingAdapter extends RecyclerView.Adapter<UserListingAdapter.ViewHolder> {

    private ItemRowBlockedUserBinding itemRowUserListingBinding;
    private ArrayList<UserList> mUserList;
    private OnClickAdapterListener mOnClickAdapterListener;
    private Context mContext;


    public UserListingAdapter(OnClickAdapterListener onClickAdapterListener, ArrayList<UserList> userList, Context mContext) {
        this.mOnClickAdapterListener = onClickAdapterListener;
        this.mUserList=userList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        itemRowUserListingBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.item_row_blocked_user, viewGroup, false);
        return new ViewHolder(itemRowUserListingBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.itemRowUserListingBinding.tvRemove.setVisibility(View.GONE);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_profile_setup_user_placeholder);
        requestOptions.error(R.drawable.ic_profile_setup_user_placeholder);
        Glide.with(viewHolder.itemRowUserListingBinding.ivProfile.getContext()).load(mUserList.get(i).getProfileImage()).apply(requestOptions.circleCrop()).into(viewHolder.itemRowUserListingBinding.ivProfile);
        viewHolder.itemRowUserListingBinding.tvName.setText(mUserList.get(i).getFirstName());
        viewHolder.itemRowUserListingBinding.tvUsername.setText(mUserList.get(i).getUserName());

        viewHolder.itemRowUserListingBinding.getRoot().setOnClickListener(view -> {
            mOnClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_USER_ROW_CLICKED, mUserList.get(i), i);
        });
    }

    @Override
    public int getItemCount() {
        return mUserList.size();
    }

    public class ViewHolder extends  RecyclerView.ViewHolder {
        ItemRowBlockedUserBinding itemRowUserListingBinding;

        public ViewHolder(@NonNull ItemRowBlockedUserBinding itemRowUserListingBinding) {
            super(itemRowUserListingBinding.getRoot());
            this.itemRowUserListingBinding = itemRowUserListingBinding;
        }
    }
}

