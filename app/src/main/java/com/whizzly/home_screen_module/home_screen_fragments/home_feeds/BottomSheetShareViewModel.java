package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.appcompat.widget.AppCompatTextView;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.change_password.ChangePasswordModel;
import com.whizzly.models.privacy_status.PrivacyStatusResponseBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class BottomSheetShareViewModel extends ViewModel {
    private RichMediatorLiveData<ChangePasswordModel> mDeleteResponseLiveData;
    private RichMediatorLiveData<PrivacyStatusResponseBean> mSaveVideoLiveData;
    private Observer<Throwable> errorObserver;
    private Observer<FailureResponse> failureResponseObserver;
    private OnClickSendListener mOnClickSendListener;


    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureResponseObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (mDeleteResponseLiveData == null) {
            mDeleteResponseLiveData = new RichMediatorLiveData<ChangePasswordModel>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
        if (mSaveVideoLiveData==null){
            mSaveVideoLiveData=new RichMediatorLiveData<PrivacyStatusResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
    }

    public RichMediatorLiveData<ChangePasswordModel> getDeleteResponseLiveData() {
        return mDeleteResponseLiveData;
    }

    public void onCancelViewClicked() {
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CANCEL_VIEW_CLICKED);
    }

    public void setOnClickListener(OnClickSendListener onClickListener) {
        this.mOnClickSendListener = onClickListener;
    }

    public void onEditViewClicked() {
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_EDIT_VIEW_CLICKED);
    }

    public void onReportViewClicked() {
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_REPORT_VIEW_CLICKED);
    }

    public void onDeleteViewClicked() {
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_DELETE_VIEW_CLICKED);
    }

    public void onMessageViewClicked() {
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_MESSAGE_CLICKED);
    }

    public void onWhatsAppViewClicked() {
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_WHATSAPP_VIEW_CLICKED);
    }

    public void onInstagramViewClicked() {
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_INSTAGRAM_VIEW_CLICKED);

    }

    public void onFaceBookViewClicked() {
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_FACEBOOK_VIEW_CLICKED);

    }

    public void onGmailViewClicked() {
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_GMAIL_VIEW_CLICKED);

    }

    public void onCopyLinkViewClicked() {
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_COPY_LINK_CLICKED);
    }

    public void onSaveViewClicked() {
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_SAVE_LINK_CLICKED);

    }
    public void onDownloadViewClicked(){
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_DOWNLOAD_VIEW_CLICKED);
    }

    public String getUserId() {
        return String.valueOf(DataManager.getInstance().getUserId());
    }

    public void hitDeletePostApi(Integer videoId, int videoType, int isMyVideo) {
        DataManager.getInstance().hitDeletePostApi(getData(videoId, videoType, isMyVideo)).enqueue(new NetworkCallback<ChangePasswordModel>() {
            @Override
            public void onSuccess(ChangePasswordModel deletePostResponse) {
                mDeleteResponseLiveData.setValue(deletePostResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mDeleteResponseLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mDeleteResponseLiveData.setError(t);
            }
        });
    }

    private HashMap<String, String> getData(Integer videoId, int videoType, int isMyVideo) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        hashMap.put(AppConstants.NetworkConstants.VIDEO_ID, String.valueOf(videoId));
        hashMap.put(AppConstants.NetworkConstants.VIDEO_TYPE, String.valueOf(videoType));
        hashMap.put(AppConstants.NetworkConstants.IS_MY_VIDEO, String.valueOf(isMyVideo));
        return hashMap;
    }

    public void hitSaveVideoApi(Integer id, int videoType,int videoPrivacyStatus) {
        DataManager.getInstance().hitSaveFeedApi(getSaveFeedData(id,videoType,videoPrivacyStatus)).enqueue(new NetworkCallback<PrivacyStatusResponseBean>() {
            @Override
            public void onSuccess(PrivacyStatusResponseBean privacyStatusResponseBean) {
                mSaveVideoLiveData.setValue(privacyStatusResponseBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
          mSaveVideoLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
           mSaveVideoLiveData.setError(t);
            }
        });
    }

    private HashMap<String, String> getSaveFeedData(Integer videoId, int videoType, int videoPrivacyStatus) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        hashMap.put(AppConstants.NetworkConstants.VIDEO_ID, String.valueOf(videoId));
        hashMap.put(AppConstants.NetworkConstants.VIDEO_TYPE, String.valueOf(videoType));
        if (videoPrivacyStatus==1)
        hashMap.put(AppConstants.NetworkConstants.PRIVACY_TYPE_ID,String.valueOf(2));
        else
            hashMap.put(AppConstants.NetworkConstants.PRIVACY_TYPE_ID,String.valueOf(1));

        return hashMap;
    }
    public RichMediatorLiveData<PrivacyStatusResponseBean> getSaveVideoLiveData(){
        return mSaveVideoLiveData;
    }
}
