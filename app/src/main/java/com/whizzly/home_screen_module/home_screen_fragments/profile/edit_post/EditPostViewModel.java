package com.whizzly.home_screen_module.home_screen_fragments.profile.edit_post;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.change_password.ChangePasswordModel;
import com.whizzly.utils.AppConstants;

public class EditPostViewModel extends ViewModel {
    private RichMediatorLiveData<ChangePasswordModel> mEditFeedsLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorResponseObserver;
    private EditPostModel mEditPostModel;
    private OnClickSendListener mOnClickSendListener;


    public void setClickListener(OnClickSendListener onClickSendListener) {
        this.mOnClickSendListener = onClickSendListener;
    }

    public void onUpdateViewClicked() {
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_UPDATE_VIEW_CLICKED);
    }

    public void hitEditPostApi(Integer videoId, String type) {
        mEditPostModel.hitEditPostApi(videoId, type, mEditFeedsLiveData);
    }


    public void setModel(EditPostModel mEditPostModel) {
        this.mEditPostModel = mEditPostModel;
    }

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver) {
        this.errorResponseObserver = errorObserver;
        this.failureResponseObserver = failureResponseObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (mEditFeedsLiveData == null)
            mEditFeedsLiveData = new RichMediatorLiveData<ChangePasswordModel>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorResponseObserver;
                }
            };
    }

    public RichMediatorLiveData<ChangePasswordModel> getEditFeedsLiveData() {
        return mEditFeedsLiveData;
    }
}
