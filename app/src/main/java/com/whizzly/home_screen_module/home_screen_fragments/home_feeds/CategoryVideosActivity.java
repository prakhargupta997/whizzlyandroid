package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;


import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.ActivityCategoriesVideosBinding;
import com.whizzly.models.collab.collab_videos.VideoDatum;
import com.whizzly.models.collab.collab_videos.allvideos.Result;
import com.whizzly.notification.NotificationActivity;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.EndlessRecyclerOnScrollListener;
import com.whizzly.utils.SpacesItemDecorationGrid;

import java.util.ArrayList;
import java.util.Objects;


public class CategoryVideosActivity extends BaseActivity implements OnClickAdapterListener {
    private ActivityCategoriesVideosBinding mActivityCategoryVideosBinding;
    private CategoryAllVideosAdapter mVideosAdapter;
    private ArrayList<Result> mVideoList;
    private CategoryVideosViewModel mcategoryVideosViewModel;
    private Integer mPage = 1, mOffset = 10;
    private ExoPlayer mExoPlayer;
    private String mUrl;
    private VideoDatum videoDatum;

    public CategoryVideosActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityCategoryVideosBinding = DataBindingUtil.setContentView(this, R.layout.activity_categories_videos);
        geData();
        mcategoryVideosViewModel = ViewModelProviders.of(this).get(CategoryVideosViewModel.class);
        mcategoryVideosViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver());
        mActivityCategoryVideosBinding.setViewModel(mcategoryVideosViewModel);
        observeLiveData();
        mVideoList = new ArrayList<>();
        setRecyclerView();
        getVideosListing();
        mActivityCategoryVideosBinding.ivBack.setOnClickListener(v -> onBackPressed());
    }


    private void geData() {
        if (getIntent() != null && getIntent().hasExtra(AppConstants.ClassConstants.VIDEO_DETAILS) && getIntent().hasExtra(AppConstants.ClassConstants.VIDEO_FILE_URI) &&
                getIntent().hasExtra(AppConstants.ClassConstants.VIDEO_THUMBNAIL_URL)) {
            videoDatum = getIntent().getParcelableExtra(AppConstants.ClassConstants.VIDEO_DETAILS);
            mUrl = getIntent().getStringExtra(AppConstants.ClassConstants.VIDEO_FILE_URI);
            setExoplayer(true);
            setViews();
//            setThumbnailUrl(getArguments().getString(AppConstants.ClassConstants.VIDEO_THUMBNAIL_URL));
        }
    }

    private void setViews() {
        mActivityCategoryVideosBinding.tvCategory.setText(videoDatum.getCategoryName());
        mActivityCategoryVideosBinding.ivBack.setVisibility(View.VISIBLE);
        mActivityCategoryVideosBinding.tvLabelVideosPosted.append(" " + videoDatum.getCategoryName());
    }

    private void setRecyclerView() {
        GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
        mActivityCategoryVideosBinding.rvVideos.setLayoutManager(layoutManager);
        mVideosAdapter = new CategoryAllVideosAdapter(mVideoList, this);
        mActivityCategoryVideosBinding.rvVideos.addItemDecoration(new SpacesItemDecorationGrid(this, 1, 3));
        mActivityCategoryVideosBinding.rvVideos.setAdapter(mVideosAdapter);
        mActivityCategoryVideosBinding.rvVideos.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                if (mPage != -1) {
                    getVideosListing();
                }
            }
        });
    }

    private void getVideosListing() {
        if (AppUtils.isNetworkAvailable(this))
            mcategoryVideosViewModel.getCategoryVideos(mPage, mOffset, videoDatum.getCategoryId());
        else
            Toast.makeText(this, getString(R.string.txt_no_internet_connection), Toast.LENGTH_SHORT).show();
    }

    private void observeLiveData() {
        mcategoryVideosViewModel.getCategoryVideosLiveData().observe(this, categoryVideosResponse -> {
            if (categoryVideosResponse != null) {
                if (categoryVideosResponse.getCode() == 301 || categoryVideosResponse.getCode() == 302) {
                    Toast.makeText(CategoryVideosActivity.this, categoryVideosResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    Objects.requireNonNull(this).autoLogOut();

                } else if (categoryVideosResponse.getCode() == 200) {
                    mPage = categoryVideosResponse.getNextCount();
                    mVideoList.addAll(categoryVideosResponse.getResult());
                    mVideosAdapter.notifyDataSetChanged();
                }
            }

        });
    }

    /*private void setThumbnailUrl(String thumbnailUrl) {
        mActivityCategoryVideosBinding.ivVideoThumb.setVisibility(View.VISIBLE);
        RequestOptions requestOptions1 = new RequestOptions();
        Glide.with(this).
                load(thumbnailUrl)
                .apply(requestOptions1).into(mActivityCategoryVideosBinding.ivVideoThumb);

    }*/


    private void setExoplayer(boolean playWhenReady) {
        if (mExoPlayer == null) {
            TrackSelection.Factory adaptiveTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(new DefaultBandwidthMeter());

            mExoPlayer = ExoPlayerFactory.newSimpleInstance(this,
                    new DefaultRenderersFactory(this),
                    new DefaultTrackSelector(adaptiveTrackSelectionFactory),
                    new DefaultLoadControl());

            mExoPlayer.setPlayWhenReady(playWhenReady);
            mExoPlayer.setRepeatMode(Player.REPEAT_MODE_ONE);
            MediaSource mediaSource = buildMediaSource(Uri.parse(mUrl));
            mExoPlayer.addListener(new Player.DefaultEventListener() {
                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    super.onPlayerStateChanged(playWhenReady, playbackState);
                    switch (playbackState) {
                        case Player.STATE_READY:
                            mActivityCategoryVideosBinding.pbMain.setVisibility(View.GONE);
//                            mActivityCategoryVideosBinding.ivVideoThumb.setVisibility(View.GONE);
                            break;
                    }
                }
            });
            mExoPlayer.prepare(mediaSource, true, false);
            mActivityCategoryVideosBinding.pvVideo.setPlayer(mExoPlayer);
        }
    }

    private MediaSource buildMediaSource(Uri uri) {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, getString(R.string.app_name)), bandwidthMeter);
        return new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23 && mExoPlayer == null && mUrl != null) {
            setExoplayer(true);
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }

    private void releasePlayer() {
        if (mExoPlayer != null) {
            mExoPlayer.setPlayWhenReady(false);
            mExoPlayer.stop();
            mExoPlayer.release();
            mExoPlayer = null;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }

    }

    @Override
    public void onAdapterClicked(int code, Object obj, int position) {
        switch (code) {
            case AppConstants.ClassConstants.ON_CATEGORY_VIDEO_CLICKED:
                if (obj != null && obj instanceof Result) {
                    Result result = (Result) obj;
                    Intent intent = new Intent(this, NotificationActivity.class);
                    intent.putExtra(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.FROM_CATEGORY_VIDEOS);
                    intent.putExtra(AppConstants.ClassConstants.NOTIFICATION_DATA, result);
                    startActivity(intent);
                }
                break;
        }
    }
}
