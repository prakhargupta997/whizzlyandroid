package com.whizzly.home_screen_module.home_screen_fragments.notification.following;


import android.app.Activity;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.whizzly.R;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.FragmentFollowingNotificationsBinding;
import com.whizzly.utils.DataManager;

import org.jetbrains.annotations.NotNull;

public class FollowingNotificationsFragment extends Fragment implements OnClickAdapterListener {
    private FollowingNotificationsViewModel mFollowingNotificationsViewModel;
    private FragmentFollowingNotificationsBinding mFragmentFollowingNotificationsBinding;
    private FollowingNotificationAdapter mAdapter;
    private Activity mActivity;

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mActivity=getActivity();
        mFragmentFollowingNotificationsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_following_notifications, container, false);
        mFollowingNotificationsViewModel= ViewModelProviders.of(this).get(FollowingNotificationsViewModel.class);
        mFragmentFollowingNotificationsBinding.setViewModel(mFollowingNotificationsViewModel);
        setUpAdapter();
        return mFragmentFollowingNotificationsBinding.getRoot();
    }


    private void setUpAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        mFragmentFollowingNotificationsBinding.rvNotifications.setLayoutManager(layoutManager);
        mAdapter = new FollowingNotificationAdapter(this);
      //  mFragmentFollowingNotificationsBinding.rvNotifications.setAdapter(mAdapter);

    }

    @Override
    public void onAdapterClicked(int code, Object obj, int position) {

    }
}
