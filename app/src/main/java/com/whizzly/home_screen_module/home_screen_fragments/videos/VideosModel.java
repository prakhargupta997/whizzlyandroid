package com.whizzly.home_screen_module.home_screen_fragments.videos;

import android.content.Context;
import androidx.databinding.BaseObservable;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.search.search_video.VideoSearchResponseBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class VideosModel extends BaseObservable {
    private Context context;
    public VideosModel(Context context) {
        this.context=context;
    }


    public void hitVideoListingApi(String seachText, int mPage, RichMediatorLiveData<VideoSearchResponseBean> mVideosLivedData) {
        if(AppUtils.isNetworkAvailable(context)) {

            DataManager.getInstance().hitVideoSearchApi(getSearchData(seachText, mPage)).enqueue(new NetworkCallback<VideoSearchResponseBean>() {
                @Override
                public void onSuccess(VideoSearchResponseBean videoListResponseBean) {
                    mVideosLivedData.setValue(videoListResponseBean);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    mVideosLivedData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    mVideosLivedData.setError(t);
                }
            });

        }else {
            Toast.makeText(context, context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }
    private HashMap<String, String> getSearchData(String seachText, int mPage) {
        HashMap<String,String> hashMap=new HashMap();
        hashMap.put(AppConstants.NetworkConstants.USER_ID,String.valueOf(DataManager.getInstance().getUserId()));
        hashMap.put(AppConstants.NetworkConstants.PAGE,String.valueOf(mPage));
        hashMap.put(AppConstants.NetworkConstants.SEARCH,seachText);
        return hashMap;

    }

}
