package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.databinding.ItemRowLikeReuseListItemBinding;
import com.whizzly.interfaces.OnOpenUserProfile;
import com.whizzly.models.reuse_response.Result;

import java.util.ArrayList;

class ReuseRecyclerViewAdapter extends RecyclerView.Adapter<ReuseRecyclerViewAdapter.ViewHolder> {

    private ItemRowLikeReuseListItemBinding itemRowLikeReuseListItemBinding;
    private Context mContext;
    private ArrayList<Result> mList;
    private OnOpenUserProfile onOpenUserProfile, openUserProfile;

    public ReuseRecyclerViewAdapter(Context mContext, ArrayList<Result> mList, OnOpenUserProfile onOpenUserProfile, OnOpenUserProfile openUserProfile) {
        this.mContext = mContext;
        this.mList = mList;
        this.onOpenUserProfile = onOpenUserProfile;
        this.openUserProfile = openUserProfile;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        itemRowLikeReuseListItemBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.item_row_like_reuse_list_item, viewGroup, false);
        return new ViewHolder(itemRowLikeReuseListItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(mList.get(i));
        if (mList.get(i).getUserStatus() == 0) {
            viewHolder.itemRowLikeReuseListItemBinding.tvUserFirstName.setText(mList.get(i).getFirstName());
            viewHolder.itemRowLikeReuseListItemBinding.tvUserName.setText(mList.get(i).getUserName());
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.ic_profile_setup_user_placeholder);
            requestOptions.error(R.drawable.ic_profile_setup_user_placeholder);
            requestOptions.circleCrop();
            viewHolder.itemRowLikeReuseListItemBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openUserProfile.onUserProfile(String.valueOf(mList.get(i).getId()));
                    onOpenUserProfile.onUserProfile(String.valueOf(mList.get(i).getId()));
                }
            });
            Glide.with(mContext).load(mList.get(i).getProfileImage()).apply(requestOptions).into(viewHolder.itemRowLikeReuseListItemBinding.ivUserProfileImage);
        }
        else {
            viewHolder.itemRowLikeReuseListItemBinding.tvUserFirstName.setText(mContext.getString(R.string.s_unknown));
            viewHolder.itemRowLikeReuseListItemBinding.tvUserName.setText(mContext.getString(R.string.s_unknown));
            viewHolder.itemRowLikeReuseListItemBinding.ivUserProfileImage.setImageResource(R.drawable.ic_profile_setup_user_placeholder);

        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ItemRowLikeReuseListItemBinding itemRowLikeReuseListItemBinding;
        ViewHolder(@NonNull ItemRowLikeReuseListItemBinding itemRowLikeReuseListItemBinding) {
            super(itemRowLikeReuseListItemBinding.getRoot());
            this.itemRowLikeReuseListItemBinding = itemRowLikeReuseListItemBinding;
        }

        public void bind(Result result){
            itemRowLikeReuseListItemBinding.setVariable(BR.model, result);
            itemRowLikeReuseListItemBinding.executePendingBindings();
        }
    }
}
