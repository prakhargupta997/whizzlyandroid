package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;


import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.ItemRowCategoriesVideosBinding;
import com.whizzly.models.collab.collab_videos.CategoryVideo;
import com.whizzly.utils.AppConstants;

import java.util.List;

public class CategoriesVideosAdapter extends RecyclerView.Adapter<CategoriesVideosAdapter.CategoriesVideosViewHolder> {

    private ItemRowCategoriesVideosBinding mItemRowVideosBinding;
    private Context mContext;
    private List<CategoryVideo> mList;
    private OnClickAdapterListener mOnClickAdapterListener;

    public CategoriesVideosAdapter(Context context, List<CategoryVideo> categoryVideo, OnClickAdapterListener onClickAdapterListener) {
        this.mContext=context;
        this.mList = categoryVideo;
       this.mOnClickAdapterListener=onClickAdapterListener;
    }



    @NonNull
    @Override
    public CategoriesVideosViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mItemRowVideosBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.item_row_categories_videos, viewGroup, false);
        return new CategoriesVideosViewHolder(mItemRowVideosBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoriesVideosAdapter.CategoriesVideosViewHolder viewHolder, int i) {
        viewHolder.bind(mList.get(i));
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_choose_image_gallery);
        requestOptions.error(R.drawable.ic_choose_image_gallery);
        Glide.with(mContext).load(mList.get(i).getThumbnailUrl()).apply(requestOptions).into(viewHolder.mItemRowVideosBinding.ivVideo);
         mItemRowVideosBinding.ivVideo.setOnClickListener(v -> mOnClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_CATEGORY_VIDEO_CLICKED,mList.get(i),i));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class CategoriesVideosViewHolder extends RecyclerView.ViewHolder {
        private ItemRowCategoriesVideosBinding mItemRowVideosBinding;
        CategoriesVideosViewHolder(@NonNull ItemRowCategoriesVideosBinding itemRowCollabFeedsBinding) {
            super(itemRowCollabFeedsBinding.getRoot());
            this.mItemRowVideosBinding = itemRowCollabFeedsBinding;
        }

        public void bind(CategoryVideo result){
            mItemRowVideosBinding.setVariable(BR.model, result);
            mItemRowVideosBinding.executePendingBindings();
        }
    }
}
