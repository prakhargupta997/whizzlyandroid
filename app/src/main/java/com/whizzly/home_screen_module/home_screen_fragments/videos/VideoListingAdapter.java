package com.whizzly.home_screen_module.home_screen_fragments.videos;


import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.whizzly.R;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.ItemRowBlockedUserBinding;
import com.whizzly.models.search.search_video.VideoList;
import com.whizzly.utils.AppConstants;

import java.util.ArrayList;

public class VideoListingAdapter extends RecyclerView.Adapter<VideoListingAdapter.ViewHolder> {

    private ItemRowBlockedUserBinding itemRowVideoListingBinding;
    private ArrayList<VideoList> mVideosList;
    private OnClickAdapterListener onClickAdapterListener;
    private Context mContext;

    public VideoListingAdapter(ArrayList<VideoList> videoList, OnClickAdapterListener onClickAdapterListener, Context context) {
        this.onClickAdapterListener = onClickAdapterListener;
        this.mVideosList = videoList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        itemRowVideoListingBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.item_row_blocked_user, viewGroup, false);
        return new ViewHolder(itemRowVideoListingBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.itemRowVideoListingBinding.tvRemove.setVisibility(View.INVISIBLE);
        viewHolder.itemRowVideoListingBinding.tvUsername.setText("#" + mVideosList.get(i).getKey());
        viewHolder.itemRowVideoListingBinding.tvName.setText(String.valueOf(mVideosList.get(i).getDocCount()));
        if (mVideosList.get(i).getDocCount() != null && mVideosList.get(i).getDocCount() > 1)
            viewHolder.itemRowVideoListingBinding.tvName.append(" " + mContext.getString(R.string.videos));
        else
            viewHolder.itemRowVideoListingBinding.tvName.append(" " + mContext.getString(R.string.s_video));

        viewHolder.itemRowVideoListingBinding.getRoot().setOnClickListener(view -> {
            onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_VIDEO_ROW_VIEW_CLICKED, mVideosList.get(i), i);
        });
        viewHolder.itemRowVideoListingBinding.ivProfile.setImageDrawable(mContext.getDrawable(R.drawable.ic_hashtag));
    }

    @Override
    public int getItemCount() {
        return mVideosList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ItemRowBlockedUserBinding itemRowVideoListingBinding;

        public ViewHolder(@NonNull ItemRowBlockedUserBinding itemRowVideoListingBinding) {
            super(itemRowVideoListingBinding.getRoot());
            this.itemRowVideoListingBinding = itemRowVideoListingBinding;
        }
    }
}
