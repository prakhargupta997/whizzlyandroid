package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;

import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.FragmentBottomSheetPeopleLikeListBinding;

import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.interfaces.OnOpenUserProfile;
import com.whizzly.models.likers_list_response.LikersListResponse;
import com.whizzly.models.likers_list_response.Result;
import com.whizzly.notification.NotificationActivity;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;
import com.whizzly.utils.EndlessRecyclerOnScrollListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class BottomSheetPeopleLikeListFragment extends BottomSheetDialogFragment implements OnClickSendListener, OnOpenUserProfile {

    private FragmentBottomSheetPeopleLikeListBinding mFragmentBottomSheetPeopleListBinding;
    private BottomSheetPeopleLikeListViewModel mBottomSheetPeopleLikeListViewModel;
    private String mVideoType, mVideoId, mUserId, mTotalLikes;
    private int page = 1, offset = 20;
    private ArrayList<Result> likersResponse;
    private LikersListRecyclerViewAdapter mLikersListRecyclerViewAdapter;

    private void getData() {
        Bundle videoData = getArguments();
        if (videoData != null) {
            mVideoId = videoData.getString(AppConstants.ClassConstants.FEED_VIDEO_ID);
            mVideoType = videoData.getString(AppConstants.ClassConstants.VIDEO_TYPE);
            mTotalLikes = videoData.getString(AppConstants.ClassConstants.TOTAL_LIKES);
            mUserId = String.valueOf(DataManager.getInstance().getUserId());
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getData();
        likersResponse = new ArrayList<>();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentBottomSheetPeopleListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_bottom_sheet_people_like_list, container, false);
        mBottomSheetPeopleLikeListViewModel = ViewModelProviders.of(this).get(BottomSheetPeopleLikeListViewModel.class);
        mFragmentBottomSheetPeopleListBinding.setViewModel(mBottomSheetPeopleLikeListViewModel);
        mBottomSheetPeopleLikeListViewModel.setGenericListeners(((BaseActivity) Objects.requireNonNull(getActivity())).getErrorObserver(), ((BaseActivity) Objects.requireNonNull(getActivity())).getFailureResponseObserver());
        mBottomSheetPeopleLikeListViewModel.setOnClickSendListener(this);
        setRecyclerView();
        mBottomSheetPeopleLikeListViewModel.getLikersListResponseRichMediatorLiveData().observe(this, this::onChanged);
        getLikersList(page, offset);
        if (Integer.parseInt(mTotalLikes)>1)
        mFragmentBottomSheetPeopleListBinding.tvLikersCount.setText(String.format("%s  %s", mTotalLikes,getString(R.string.txt_likes)));
        else
            mFragmentBottomSheetPeopleListBinding.tvLikersCount.setText(String.format("%s  %s", mTotalLikes,getString(R.string.txt_like)));

            return mFragmentBottomSheetPeopleListBinding.getRoot();
    }

    private void getLikersList(int page, int offset) {
        HashMap<String, String> likersQuery = new HashMap<>();
        likersQuery.put(AppConstants.NetworkConstants.VIDEO_ID, mVideoId);
        likersQuery.put(AppConstants.NetworkConstants.VIDEO_TYPE, mVideoType);
        likersQuery.put(AppConstants.NetworkConstants.USER_ID, mUserId);
        likersQuery.put(AppConstants.NetworkConstants.PAGE, String.valueOf(page));
        likersQuery.put(AppConstants.NetworkConstants.LIMIT, String.valueOf(offset));
        mBottomSheetPeopleLikeListViewModel.getLikersList(likersQuery);
    }

    private void setRecyclerView() {
        mFragmentBottomSheetPeopleListBinding.rvListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        if(getActivity() instanceof HomeActivity)
        mLikersListRecyclerViewAdapter = new LikersListRecyclerViewAdapter(getActivity(), likersResponse, (HomeActivity)getActivity(), this);
        else if(getActivity() instanceof NotificationActivity)
            mLikersListRecyclerViewAdapter = new LikersListRecyclerViewAdapter(getActivity(), likersResponse, (NotificationActivity)getActivity(), this);
        mFragmentBottomSheetPeopleListBinding.rvListView.setAdapter(mLikersListRecyclerViewAdapter);
        mFragmentBottomSheetPeopleListBinding.rvListView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                if (page > 0)
                    getLikersList(page, offset);
            }
        });
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_DROP_DOWN_CLICKED:
                dismiss();
                break;
        }
    }

    private void onChanged(LikersListResponse likersListResponse) {
        if (likersListResponse != null) {
            if (likersListResponse.getCode()==200) {
                likersResponse.clear();
                page = likersListResponse.getNextCount();
                likersResponse.addAll(likersListResponse.getResult());
                mLikersListRecyclerViewAdapter.notifyDataSetChanged();
            }
            else if (likersListResponse.getCode()==301 ||likersListResponse.getCode()==302){
                Toast.makeText(getActivity(), likersListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                ((BaseActivity)Objects.requireNonNull(getActivity())).autoLogOut();
            }
        }
    }

    @Override
    public void onUserProfile(String userId) {
        dismiss();
    }
}
