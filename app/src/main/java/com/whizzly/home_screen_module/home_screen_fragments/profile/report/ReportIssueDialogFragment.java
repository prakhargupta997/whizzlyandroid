package com.whizzly.home_screen_module.home_screen_fragments.profile.report;


import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.DialogReportIssueBinding;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.feeds_response.Result;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;

import java.util.Objects;

public class ReportIssueDialogFragment extends DialogFragment implements OnClickSendListener {
    private Activity mActivity;
    private DialogReportIssueBinding mDialogReportIssueBinding;
    private ReportIssueViewModel mReportIssueViewModel;
    private int mReasonToReport;
    private String mReportedUserId;
    private int mReportType;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getData();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(AppConstants.ClassConstants.IS_FOR)) {
            switch (getArguments().getInt(AppConstants.ClassConstants.IS_FOR)) {
                case AppConstants.ClassConstants.FOR_REPORT_LINK:
                    Result result = getArguments().getParcelable(AppConstants.ClassConstants.VIDEO_DETAILS);
                    if (result != null) {
                        mReportedUserId = String.valueOf(result.getId());
                        if (result.getType().equalsIgnoreCase(getString(R.string.txt_collab)))
                            mReportType = 2;
                        else
                            mReportType = 3;
                    }
                    break;
                case AppConstants.ClassConstants.FOR_REPORT_USER:
                    mReportType = 1;
                    mReportedUserId = getArguments().getString(AppConstants.ClassConstants.OTHER_USER_ID);
                    break;
            }

        }
    }

    private void setReportPostViews() {
        mDialogReportIssueBinding.rbFirst.setText(R.string.s_harrassment_or_bullying);
        mDialogReportIssueBinding.rbSecond.setText(R.string.s_spam);
        mDialogReportIssueBinding.rbThird.setText(R.string.s_copyright_infringment);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mActivity = getActivity();
        mDialogReportIssueBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_report_issue, container, false);
        mReportIssueViewModel = ViewModelProviders.of(this).get(ReportIssueViewModel.class);
        mDialogReportIssueBinding.setViewModel(mReportIssueViewModel);
        mReportIssueViewModel.setGenericListeners(((BaseActivity) mActivity).getErrorObserver(), ((BaseActivity) mActivity).getFailureResponseObserver());
        mReportIssueViewModel.setClickListener(this);
        setCancelable(false);
        observeLiveData();
        setListener();
        return mDialogReportIssueBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mReportType == 2 || mReportType == 3) {
            setReportPostViews();
        }
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_REPORT_VIEW_CLICKED:
                if (mReasonToReport < 1) {
                    mDialogReportIssueBinding.tvErrorReportIssue.setVisibility(View.VISIBLE);
                } else if (mReasonToReport == 4) {
                    if (mDialogReportIssueBinding.etDescription.getText().length() > 0) {
                        hitReportApi();
                    } else {
                        mDialogReportIssueBinding.tvErrorReportIssue.setText(R.string.please_describe_the_reason_to_report);
                        mDialogReportIssueBinding.tvErrorReportIssue.setVisibility(View.VISIBLE);
                    }
                } else {
                    mDialogReportIssueBinding.tvErrorReportIssue.setVisibility(View.GONE);
                    hitReportApi();

                }
                break;
            case AppConstants.ClassConstants.ON_CANCEL_VIEW_CLICKED:
                dismiss();

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
            dialog.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    private void hitReportApi() {
        if (AppUtils.isNetworkAvailable(mActivity)) {
            mReportIssueViewModel.reportUser(mReportType, mReportedUserId, mReasonToReport, mDialogReportIssueBinding.etDescription.getText().toString().trim());
            Toast.makeText(mActivity, getString(R.string.reported_successfully), Toast.LENGTH_SHORT).show();
            dismiss();

        } else
            Toast.makeText(mActivity, getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();


    }


    private void setListener() {
        mDialogReportIssueBinding.rgReport.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                mDialogReportIssueBinding.tvErrorReportIssue.setVisibility(View.GONE);
                switch (id) {
                    case R.id.rb_first:
                        mReasonToReport = 1;
                        hideDescriptionView();
                        break;
                    case R.id.rb_second:
                        mReasonToReport = 2;
                        hideDescriptionView();
                        break;
                    case R.id.rb_third:
                        mReasonToReport = 3;
                        hideDescriptionView();
                        break;
                    case R.id.rb_fourth:
                        mReasonToReport = 4;
                        showDescriptionView();
                        break;
                    default:
                        mReasonToReport = -1;

                }
            }
        });

        mDialogReportIssueBinding.etDescription.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });
    }

    private void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void hideDescriptionView() {
        TransitionManager.beginDelayedTransition(mDialogReportIssueBinding.llReport);
        mDialogReportIssueBinding.etDescription.setVisibility(View.GONE);
    }

    private void showDescriptionView() {
        TransitionManager.beginDelayedTransition(mDialogReportIssueBinding.llReport);
        mDialogReportIssueBinding.etDescription.setVisibility(View.VISIBLE);
    }

    private void observeLiveData() {
        mReportIssueViewModel.getReportLiveData().observe(this, reportResponse -> {
            if (reportResponse != null) {
                if (reportResponse.getCode() == 301 || reportResponse.getCode() == 302) {
                    Toast.makeText(mActivity, reportResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) Objects.requireNonNull(mActivity)).autoLogOut();
                } else if (reportResponse.getCode() == 200)
                    Toast.makeText(mActivity, reportResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
            dismiss();
        });
    }


}
