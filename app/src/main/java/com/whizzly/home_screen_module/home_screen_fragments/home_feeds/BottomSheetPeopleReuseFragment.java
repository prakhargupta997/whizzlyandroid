package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.BottomSheetPeopleReuseFragmentBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.interfaces.OnOpenUserProfile;
import com.whizzly.models.reuse_response.Result;
import com.whizzly.models.reuse_response.ReuseResponse;
import com.whizzly.notification.NotificationActivity;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class BottomSheetPeopleReuseFragment extends BottomSheetDialogFragment implements OnClickSendListener, OnOpenUserProfile {

    private BottomSheetPeopleReuseViewModel mViewModel;
    private BottomSheetPeopleReuseFragmentBinding mBottomSheetPeopleReuseFragmentBinding;
    private ReuseRecyclerViewAdapter mReuseRecyclerViewAdapter;
    private String mVideoType, mVideoId, mUserId, mTotalReuseCount;
//    private int page = 1, offset = 20;
    private ArrayList<Result> results;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        results = new ArrayList<>();
        getData();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBottomSheetPeopleReuseFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.bottom_sheet_people_reuse_fragment, container, false);
        mViewModel = ViewModelProviders.of(this).get(BottomSheetPeopleReuseViewModel.class);
        mBottomSheetPeopleReuseFragmentBinding.setViewModel(mViewModel);
        mViewModel.setGenericListeners(((BaseActivity) Objects.requireNonNull(getActivity())).getErrorObserver(), ((BaseActivity) getActivity()).getFailureResponseObserver());
        mViewModel.setOnClickSendListener(this);
        mViewModel.getLikersListResponseRichMediatorLiveData().observe(this, new Observer<ReuseResponse>() {
            @Override
            public void onChanged(@Nullable ReuseResponse reuseResponse) {
                if (reuseResponse!=null){
                    if (reuseResponse.getCode()==200 &&reuseResponse.getResult().size() > 0) {
                        results.addAll(reuseResponse.getResult());
                        mReuseRecyclerViewAdapter.notifyDataSetChanged();
                    }
                    else if (reuseResponse.getCode()==302 || reuseResponse.getCode()==301){
                        Toast.makeText(getActivity(), reuseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        ((BaseActivity)getActivity()).autoLogOut();
                    }
                }

            }
        });
        getLikersList(/*page, offset*/);
        setRecyclerView();
        mBottomSheetPeopleReuseFragmentBinding.tvReuseCount.setText(String.format("%s  %s", mTotalReuseCount,getString(R.string.txt_reused)));
        return mBottomSheetPeopleReuseFragmentBinding.getRoot();
    }

    private void getData() {
        Bundle videoData = getArguments();
        if (videoData != null) {
            mVideoId = videoData.getString(AppConstants.ClassConstants.FEED_VIDEO_ID);
            mVideoType = videoData.getString(AppConstants.ClassConstants.VIDEO_TYPE);
            mTotalReuseCount = videoData.getString(AppConstants.ClassConstants.TOTAL_REUSE);
            mUserId = String.valueOf(DataManager.getInstance().getUserId());
        }
    }

    private void getLikersList(/*int page, int offset*/) {
        HashMap<String, String> reuseQuery = new HashMap<>();
        reuseQuery.put(AppConstants.NetworkConstants.VIDEO_ID, mVideoId);
        reuseQuery.put(AppConstants.NetworkConstants.VIDEO_TYPE, mVideoType);
        reuseQuery.put(AppConstants.NetworkConstants.USER_ID, mUserId);/*
        reuseQuery.put(AppConstants.NetworkConstants.PAGE, String.valueOf(page));
        reuseQuery.put(AppConstants.NetworkConstants.LIMIT, String.valueOf(offset));*/
        mViewModel.getReuseList(reuseQuery);
    }

    private void setRecyclerView() {
        mBottomSheetPeopleReuseFragmentBinding.rvListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (getActivity() instanceof HomeActivity)
        mReuseRecyclerViewAdapter = new ReuseRecyclerViewAdapter(getActivity(), results, (HomeActivity)getActivity(), this);
        else if (getActivity() instanceof NotificationActivity)
            mReuseRecyclerViewAdapter = new ReuseRecyclerViewAdapter(getActivity(), results, (NotificationActivity)getActivity(), this);
        mBottomSheetPeopleReuseFragmentBinding.rvListView.setAdapter(mReuseRecyclerViewAdapter);
        /*mBottomSheetPeopleReuseFragmentBinding.rvListView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                if (page != -1)
                    getLikersList(page, offset);
            }
        });*/
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_DROP_DOWN_CLICKED:
                dismiss();
                break;
        }
    }

    @Override
    public void onUserProfile(String userId) {
        dismiss();
    }
}
