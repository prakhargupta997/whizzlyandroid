package com.whizzly.home_screen_module.home_screen_fragments.profile.settings.change_language;

import android.content.Context;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableField;

import com.whizzly.BR;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.submit_profile.SubmitProfileBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class ChangeLanguageModel extends BaseObservable {

    private final Context context;
    public ObservableField<Boolean> isLanguageEnglish = new ObservableField<>();


    ChangeLanguageModel(Context context) {
        this.context = context;
    }

    @Bindable
    public boolean getIsLanguageEnglish() {
        return isLanguageEnglish.get();
    }

    public void setIsLanguageEnglish(boolean value) {
        isLanguageEnglish.set(value);
        notifyPropertyChanged(BR.isLanguageEnglish);
    }

    public void hitChangeLanguageApi(RichMediatorLiveData<SubmitProfileBean> mChangeLanguageRichMediatorLiveData) {
        if (AppUtils.isNetworkAvailable(context)) {

            DataManager.getInstance().hitSubmitProfile(getChangeLanguageData()).enqueue(new NetworkCallback<SubmitProfileBean>() {
                @Override
                public void onSuccess(SubmitProfileBean submitProfileBean) {
                    mChangeLanguageRichMediatorLiveData.setValue(submitProfileBean);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    mChangeLanguageRichMediatorLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    mChangeLanguageRichMediatorLiveData.setError(t);
                }
            });

        }
    }

    private HashMap<String, String> getChangeLanguageData() {

        HashMap<String, String> params = new HashMap<>();

        String userId = String.valueOf(DataManager.getInstance().getUserId());

        int userLanguage = 0;

        if (getIsLanguageEnglish()) {
            userLanguage = 1;

        } else {
            userLanguage = 2;
        }

        params.put(AppConstants.NetworkConstants.PARAM_USER_ID, userId);
        params.put(AppConstants.NetworkConstants.PARAM_USER_LANGUAGE, String.valueOf(userLanguage));


        return params;
    }
}
