package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;

import androidx.lifecycle.ViewModel;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class HomeFragmentViewModel extends ViewModel {

    private OnClickSendListener mOnClickSendListener;

    public void onPopularClicked() {
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_HOME_POPULAR_CLICKED);
    }

    public void onFriendsClicked() {
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_HOME_FRIENDS_CLICKED);
    }

    public void onCollabClicked() {
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_HOME_COLLAB_CLICKED);
    }

    public void setOnClickSendListener(OnClickSendListener mOnClickSendListener) {
        this.mOnClickSendListener = mOnClickSendListener;
    }
}
