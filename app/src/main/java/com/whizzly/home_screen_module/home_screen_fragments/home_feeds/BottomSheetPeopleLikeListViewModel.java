package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.likers_list_response.LikersListResponse;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class BottomSheetPeopleLikeListViewModel extends ViewModel {

    private RichMediatorLiveData<LikersListResponse> likersListResponseRichMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private OnClickSendListener onClickSendListener;

    public RichMediatorLiveData<LikersListResponse> getLikersListResponseRichMediatorLiveData() {
        return likersListResponseRichMediatorLiveData;
    }

    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureObserver;
        initializeLiveData();
    }

    private void initializeLiveData() {
        if (likersListResponseRichMediatorLiveData == null) {
            likersListResponseRichMediatorLiveData = new RichMediatorLiveData<LikersListResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
    }

    public void getLikersList(HashMap<String, String> likersQuery) {
        DataManager.getInstance().hitLikersListApi(likersQuery).enqueue(new NetworkCallback<LikersListResponse>() {
            @Override
            public void onSuccess(LikersListResponse likersListResponse) {
                likersListResponseRichMediatorLiveData.setValue(likersListResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                likersListResponseRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                likersListResponseRichMediatorLiveData.setError(t);
            }
        });
    }

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void onDropDownClicked(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_DROP_DOWN_CLICKED);
    }
}
