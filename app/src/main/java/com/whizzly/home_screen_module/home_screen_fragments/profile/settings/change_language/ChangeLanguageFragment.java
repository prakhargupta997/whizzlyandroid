package com.whizzly.home_screen_module.home_screen_fragments.profile.settings.change_language;

import android.app.Activity;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.res.Configuration;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.whizzly.R;
import com.whizzly.databinding.FragmentChangeLanguageBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.home_screen_module.home_screen_activities.InnerFragmentsHostActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.Locale;

public class ChangeLanguageFragment extends Fragment implements OnClickSendListener {

    private ChangeLanguageModel mChangeLanguageModel;
    private ChangeLanguageViewModel mChangeLanguageViewModel;
    private Activity mActivity;
    private FragmentChangeLanguageBinding mFragmentChangeLanguageBinding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mActivity = getActivity();
        mFragmentChangeLanguageBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_change_language, container, false);
        mChangeLanguageViewModel = ViewModelProviders.of(this).get(ChangeLanguageViewModel.class);
        mFragmentChangeLanguageBinding.setViewModel(mChangeLanguageViewModel);
        mChangeLanguageModel = new ChangeLanguageModel(mActivity);
        mFragmentChangeLanguageBinding.setModel(mChangeLanguageModel);
        mChangeLanguageViewModel.setOnClickSendListener(this);
        mChangeLanguageViewModel.setModel(mChangeLanguageModel);
        mChangeLanguageViewModel.setGenericListeners(((InnerFragmentsHostActivity) mActivity).getErrorObserver(), ((InnerFragmentsHostActivity) mActivity).getFailureResponseObserver());;

        if(DataManager.getInstance().isNotchDevice()){
            mFragmentChangeLanguageBinding.view.setVisibility(View.VISIBLE);
        }else{
            mFragmentChangeLanguageBinding.view.setVisibility(View.GONE);
        }

        return mFragmentChangeLanguageBinding.getRoot();
    }

    private void changeLanguage(String lang){
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getActivity().getBaseContext().getResources().updateConfiguration(config,
                getActivity().getBaseContext().getResources().getDisplayMetrics());
        getActivity().finishAffinity();
        Intent refresh = new Intent(getActivity(), HomeActivity.class);
        startActivity(refresh);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onClickSend(int code) {
        switch (code)
        {
            case AppConstants.ClassConstants.ON_BACK_PRESSED:
                mActivity.onBackPressed();
                break;
            case AppConstants.ClassConstants.ON_ENGLISH_SELECTED:

                DataManager.getInstance().setLanguage("en");
                changeLanguage("en");
//                ((Whizzly)getActivity().getApplicationContext()).changeLang(DataManager.getInstance().getLanguage());
                break;
            case AppConstants.ClassConstants.ON_ITALIAN_SELECTED:
                DataManager.getInstance().setLanguage("it");
                changeLanguage("it");
//                ((Whizzly)getActivity().getApplicationContext()).changeLang(DataManager.getInstance().getLanguage());
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(DataManager.getInstance().getLanguage().equalsIgnoreCase("it")){
            mChangeLanguageModel.setIsLanguageEnglish(false);
        }else {
            mChangeLanguageModel.setIsLanguageEnglish(true);
        }
    }
}
