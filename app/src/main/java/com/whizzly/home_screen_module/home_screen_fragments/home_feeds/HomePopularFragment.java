package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;

import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.HomePupolarFragmentBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.interfaces.OnActionDetectListener;
import com.whizzly.interfaces.OnLikeDislikeUpdate;
import com.whizzly.models.feeds_response.Result;
import com.whizzly.models.privacy_status.PrivacyStatusResponseBean;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.VideoPlayerRecyclerAdapter;
import com.whizzly.utils.VideoPlayerViewHolder;

import java.util.ArrayList;
import java.util.Objects;

public class HomePopularFragment extends Fragment implements OnLikeDislikeUpdate, OnClickAdapterListener, OnActionDetectListener {

    private HomePopularViewModel mViewModel;
    private HomePupolarFragmentBinding homePupolarFragmentBinding;
    private ArrayList<Result> results = new ArrayList<>();
    private VideoPlayerRecyclerAdapter videosViewPagerAdapter;
    private int page = 1;
    private Observer<PrivacyStatusResponseBean> mSavedVideoObserver;
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (isVisible() && isAdded()) {
                homePupolarFragmentBinding.rvHomeFeeds.playVideo(false);
            }
        }
    };
    private boolean isFromRefresh = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        homePupolarFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.home_pupolar_fragment, container, false);
        mViewModel = ViewModelProviders.of(this).get(HomePopularViewModel.class);
        homePupolarFragmentBinding.setViewModel(mViewModel);
        mViewModel.setGenericListeners(((HomeActivity) Objects.requireNonNull(getActivity())).getErrorObserver(), ((HomeActivity) Objects.requireNonNull(getActivity())).getFailureResponseObserver());
        mViewModel.getmFeedsResponseRichMediatorLiveData().observe(this, feedsResponse -> {
            if (feedsResponse != null) {
                homePupolarFragmentBinding.pbLoading.setVisibility(View.GONE);
                if (homePupolarFragmentBinding.srvFeeds.isRefreshing()) {
                    homePupolarFragmentBinding.srvFeeds.setRefreshing(false);
                }
                page = feedsResponse.getNextCount();
                if (isFromRefresh) {
                    results.clear();
                    getDataFromApi(false);
                }
                if (feedsResponse != null && feedsResponse.getResult().size()>0)
                    results.addAll(feedsResponse.getResult());
                if (results.size() == 0) {
                    homePupolarFragmentBinding.ivNoData.setVisibility(View.VISIBLE);
                } else {
                    homePupolarFragmentBinding.ivNoData.setVisibility(View.GONE);
                }
                homePupolarFragmentBinding.rvHomeFeeds.setMediaObjects(results);
                videosViewPagerAdapter.notifyDataSetChanged();
                handler.postDelayed(runnable, 1000);
            }
        });
        homePupolarFragmentBinding.noInternetConnection.btnRetryFeeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataFromApi(false);
            }
        });
        setViewPager();
        observeSavedVideoLiveData();
        getDataFromApi(false);
        return homePupolarFragmentBinding.getRoot();
    }

    private void setViewPager() {
        homePupolarFragmentBinding.srvFeeds.setColorSchemeResources(R.color.colorAccent);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        homePupolarFragmentBinding.rvHomeFeeds.setLayoutManager(linearLayoutManager);
        homePupolarFragmentBinding.rvHomeFeeds.setMediaObjects(results);
        videosViewPagerAdapter = new VideoPlayerRecyclerAdapter(results, getActivity(), getChildFragmentManager(), this);
        homePupolarFragmentBinding.rvHomeFeeds.setAdapter(videosViewPagerAdapter);
        PagerSnapHelper pagerHelper = new PagerSnapHelper();
        pagerHelper.attachToRecyclerView(homePupolarFragmentBinding.rvHomeFeeds);
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                if (direction == ItemTouchHelper.LEFT) {
                    ((VideoPlayerViewHolder) viewHolder).onSwipeLeft();
                }
            }

            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };

        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(homePupolarFragmentBinding.rvHomeFeeds);
        homePupolarFragmentBinding.srvFeeds.setOnRefreshListener(() -> {
            page = 1;
            getDataFromApi(true);
        });
        homePupolarFragmentBinding.rvHomeFeeds.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                videosViewPagerAdapter.notifyDataSetChanged();
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    getDataFromApi(false);
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                /*((HomeActivity) getActivity()).showTopView();
                ((HomeActivity) getActivity()).mActivityHomeBinding.llBottom.setVisibility(View.VISIBLE);*/
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (homePupolarFragmentBinding.rvHomeFeeds != null) {
            homePupolarFragmentBinding.rvHomeFeeds.getAdapter().notifyDataSetChanged();
        }
        homePupolarFragmentBinding.rvHomeFeeds.resumePlayer();
    }

    private void getDataFromApi(boolean isFromRefresh) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            if (AppUtils.isNetworkAvailable(getActivity())) {
                this.isFromRefresh = isFromRefresh;
                if (page > 0) {
                    homePupolarFragmentBinding.noInternetConnection.rlMainLayout.setVisibility(View.GONE);
                    mViewModel.hitFeedsApi(page, 30);
                    homePupolarFragmentBinding.pbLoading.setVisibility(View.VISIBLE);
                }
            } else {
                homePupolarFragmentBinding.noInternetConnection.rlMainLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        homePupolarFragmentBinding.rvHomeFeeds.pausePlayer();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        homePupolarFragmentBinding.rvHomeFeeds.releasePlayer();
        handler.removeCallbacks(runnable);
    }

    @Override
    public void onLikeDislikeUpdate(int position, int status) {
        Result result = results.get(position);
        result.setIsLiked(status);
        if (status == 0)
            result.setTotalLikes(result.getTotalLikes() - 1);
        else
            result.setTotalLikes(result.getTotalLikes() + 1);
        results.set(position, result);
    }

    @Override
    public void onAdapterClicked(int code, Object obj, int position) {

    }

    public Observer<PrivacyStatusResponseBean> geSavedFeedsObserver() {
        return mSavedVideoObserver;
    }

    private void observeSavedVideoLiveData() {
        mSavedVideoObserver = privacyStatusResponseBean -> {
            if (privacyStatusResponseBean != null) {
                if (privacyStatusResponseBean.getCode() == 200) {
                    if (privacyStatusResponseBean.getResult().getIsLocked() != null && privacyStatusResponseBean.getResult().getVideoId() != null) {
                        for (int i = 0; i < results.size(); i++) {
                            if (results.get(i).getId().equals(privacyStatusResponseBean.getResult().getVideoId())) {
                                results.get(i).setSavedVideoPrivacy(privacyStatusResponseBean.getResult().getIsLocked());
                                break;
                            }
                        }
                    }
                    Toast.makeText(getActivity(), privacyStatusResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (privacyStatusResponseBean.getCode() == 301 || privacyStatusResponseBean.getCode() == 302) {
                    Toast.makeText(getActivity(), privacyStatusResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) getActivity()).autoLogOut();
                } else
                    Toast.makeText(getActivity(), getActivity().getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();

            }

        };
    }

    @Override
    public void onActionDone(Boolean action) {
        if (action) {
            homePupolarFragmentBinding.rvHomeFeeds.pausePlayer();
        } else {
            homePupolarFragmentBinding.rvHomeFeeds.resumePlayer();
        }
    }
}