package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;


import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.whizzly.R;
import com.whizzly.databinding.FragmentHomeBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.interfaces.OnBackPressedListener;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements OnClickSendListener, OnBackPressedListener {


    private FragmentHomeBinding mFragmentHomeBinding;
    private HomeFragmentViewModel mHomeFragmentViewModel;
    private FragmentManager mFragmentManager;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mFragmentHomeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        mHomeFragmentViewModel = ViewModelProviders.of(this).get(HomeFragmentViewModel.class);
        mFragmentHomeBinding.setViewModel(mHomeFragmentViewModel);
        mHomeFragmentViewModel.setOnClickSendListener(this);
        mFragmentManager = getChildFragmentManager();
        ((HomeActivity)getActivity()).setOnBackPressedListener(this);
        onClickSend(AppConstants.ClassConstants.ON_HOME_POPULAR_CLICKED);
        return mFragmentHomeBinding.getRoot();
    }


    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_HOME_FRIENDS_CLICKED:
                mFragmentHomeBinding.tvFriends.setTextColor(Color.parseColor("#FFFFFF"));
                mFragmentHomeBinding.tvPopular.setTextColor(Color.parseColor("#9DFFFFFF"));
                mFragmentHomeBinding.tvCollab.setTextColor(Color.parseColor("#9DFFFFFF"));
                ((HomeActivity)getActivity()).selectBottomNav(true, false, false, false, true);

                HomeFriendsFragment homeFriendsFragment = new HomeFriendsFragment();
                mFragmentManager.beginTransaction().replace(R.id.fl_container, homeFriendsFragment, HomeFriendsFragment.class.getCanonicalName())
                        .addToBackStack(HomeFriendsFragment.class.getCanonicalName()).commitAllowingStateLoss();
                break;
            case AppConstants.ClassConstants.ON_HOME_COLLAB_CLICKED:
                mFragmentHomeBinding.tvFriends.setTextColor(Color.parseColor("#9DFFFFFF"));
                mFragmentHomeBinding.tvPopular.setTextColor(Color.parseColor("#9DFFFFFF"));
                mFragmentHomeBinding.tvCollab.setTextColor(Color.parseColor("#FFFFFF"));
                HomeCollabFragment homeCollabFragment = new HomeCollabFragment();
                ((HomeActivity)getActivity()).selectBottomNav(true, false, false, false, false);
                mFragmentManager.beginTransaction().replace(R.id.fl_container, homeCollabFragment, HomeCollabFragment.class.getCanonicalName())
                        .addToBackStack(HomeCollabFragment.class.getCanonicalName()).commitAllowingStateLoss();
                break;
            case AppConstants.ClassConstants.ON_HOME_POPULAR_CLICKED:
                mFragmentHomeBinding.tvFriends.setTextColor(Color.parseColor("#9DFFFFFF"));
                mFragmentHomeBinding.tvPopular.setTextColor(Color.parseColor("#FFFFFF"));
                mFragmentHomeBinding.tvCollab.setTextColor(Color.parseColor("#9DFFFFFF"));
                HomePopularFragment homePopularFragment = new HomePopularFragment();
                ((HomeActivity)getActivity()).selectBottomNav(true, false, false, false, true);
                mFragmentManager.beginTransaction().replace(R.id.fl_container, homePopularFragment, HomePopularFragment.class.getCanonicalName())
                        .addToBackStack(HomePopularFragment.class.getCanonicalName()).commitAllowingStateLoss();
                break;
        }
    }

    public void showVisibility(){
        mFragmentHomeBinding.llTopViews.setVisibility(View.VISIBLE);
    }

    public void hideVisibility(){
        mFragmentHomeBinding.llTopViews.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        if(mFragmentManager.findFragmentById(R.id.fl_container) instanceof HomePopularFragment){
            getActivity().finish();
        }else{
            onClickSend(AppConstants.ClassConstants.ON_HOME_POPULAR_CLICKED);
        }
    }
}
