package com.whizzly.home_screen_module.home_screen_fragments.profile.edit_post;

import android.content.Context;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableField;
import com.whizzly.R;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.change_password.ChangePasswordModel;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class EditPostModel  extends BaseObservable {
    private Context mContext;
    public ObservableField<String> videoTitle = new ObservableField<>();
    private ObservableField<String> videoHashtag=new ObservableField<>();

    public EditPostModel(Context context) {
        this.mContext=context;
    }

    @Bindable
    public String getVideoTitle() {
        return videoTitle.get();
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle.set(videoTitle);
       // notifyPropertyChanged(BR.videoTitle);
    }

    @Bindable
    public String getVideoHashTag() {
        return videoHashtag.get();
    }

    public void setVideoHashTag(String videoHashtag) {
        this.videoHashtag.set(videoHashtag);
    }



    public void hitEditPostApi(Integer videoId, String type, RichMediatorLiveData<ChangePasswordModel> mEditFeedsLiveData) {
        DataManager.getInstance().hitEditFeedsApi(getVideoData(videoId,type)).enqueue(new NetworkCallback<ChangePasswordModel>() {
            @Override
            public void onSuccess(ChangePasswordModel changePasswordModel) {
                mEditFeedsLiveData.setValue(changePasswordModel);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
            mEditFeedsLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
          mEditFeedsLiveData.setError(t);
            }
        });

    }
    public String isValidTitle(){
        if(getVideoTitle() == null){
            return mContext.getString(R.string.txt_error_empty_caption);
        }else {
            if(getVideoTitle().isEmpty()){
                return mContext.getString(R.string.txt_error_empty_caption);
            }else{
                return null;
            }
        }
    }

    public String isValidHashtag(){
        if(getVideoHashTag() == null){
            return mContext.getString(R.string.txt_error_empty_hashtag);
        }else {
            if (getVideoHashTag().isEmpty()) {
                return mContext.getString(R.string.txt_error_empty_hashtag);
            } else {
                return null;
            }
        }
    }
    private HashMap<String,String> getVideoData(Integer videoId, String type) {
        String hashtagString = getVideoHashTag();
        String[] hashtag= hashtagString.split(" ");
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < hashtag.length; i++) {
            if(i == 0){
                stringBuilder.append(hashtag[i].replace("#", ""));
            }else {
                stringBuilder.append(hashtag[i].replace("#", ","));
            }
        }

        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put(AppConstants.NetworkConstants.USER_ID,String.valueOf(DataManager.getInstance().getUserId()));
        hashMap.put(AppConstants.NetworkConstants.VIDEO_ID,String.valueOf(videoId));
        if (type.equalsIgnoreCase(mContext.getString(R.string.txt_collab)))
        hashMap.put(AppConstants.NetworkConstants.VIDEO_TYPE,String.valueOf(2));
        else
        hashMap.put(AppConstants.NetworkConstants.VIDEO_TYPE,String.valueOf(1));
        if (getVideoTitle()!=null)
        hashMap.put(AppConstants.NetworkConstants.ARGUMENT_VIDEO_TITLE,getVideoTitle());
        hashMap.put(AppConstants.NetworkConstants.HASHTAG, stringBuilder.toString());
        return hashMap;
    }
}
