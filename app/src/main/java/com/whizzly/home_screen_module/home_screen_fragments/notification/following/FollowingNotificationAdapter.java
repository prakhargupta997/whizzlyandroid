package com.whizzly.home_screen_module.home_screen_fragments.notification.following;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.whizzly.R;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.ItemRowNotificationFollowBinding;

public class FollowingNotificationAdapter extends RecyclerView.Adapter<FollowingNotificationAdapter.ViewHolder> {
    private OnClickAdapterListener onClickAdapterListener;
    private ViewGroup viewGroup;
    private ItemRowNotificationFollowBinding itemRowNotificationBinding;

    public FollowingNotificationAdapter(OnClickAdapterListener onClickAdapterListener) {
        this.onClickAdapterListener=onClickAdapterListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        itemRowNotificationBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_row_notification_follow, viewGroup, false);
        this.viewGroup = viewGroup;
        return new ViewHolder(itemRowNotificationBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
       viewHolder.itemRowNotificationFollowBinding.tvFollow.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return 10;
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        ItemRowNotificationFollowBinding itemRowNotificationFollowBinding;
        public ViewHolder(ItemRowNotificationFollowBinding itemRowNotificationBinding) {
            super(itemRowNotificationBinding.getRoot());
            this.itemRowNotificationFollowBinding=itemRowNotificationBinding;
        }
      /*  public void bind(Result result) {
            itemRowNotificationBinding.setVariable(BR.model, result);
            itemRowNotificationBinding.executePendingBindings();
        }*/
    }
}
