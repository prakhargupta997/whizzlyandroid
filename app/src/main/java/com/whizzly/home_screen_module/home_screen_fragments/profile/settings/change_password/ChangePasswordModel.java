package com.whizzly.home_screen_module.home_screen_fragments.profile.settings.change_password;

import android.content.Context;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import android.widget.Toast;

import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class ChangePasswordModel extends BaseObservable {

    private final Context context;
    public ObservableBoolean isOldShowPassword = new ObservableBoolean(false);
    public ObservableBoolean isConfirmShowPassword = new ObservableBoolean(false);
    public ObservableBoolean isNewShowPassword = new ObservableBoolean(false);
    private ObservableField<String> oldPassword = new ObservableField<>();
    private ObservableField<String> newPassword = new ObservableField<>();
    private ObservableField<String> confirmPassword = new ObservableField<>();


    ChangePasswordModel(Context context) {
        this.context = context;
    }

    @Bindable
    public String getOldPassword() {
        return oldPassword.get();

    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword.set(oldPassword);
        notifyPropertyChanged(BR.oldPassword);
    }

    @Bindable
    public String getNewPassword() {
        return newPassword.get();
    }

    public void setNewPassword(String newPassword) {
        this.newPassword.set(newPassword);
        notifyPropertyChanged(BR.newPassword);
    }

    @Bindable
    public String getConfirmPassword() {
        return confirmPassword.get();
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword.set(confirmPassword);
        notifyPropertyChanged(BR.confirmPassword);
    }

    public String isOldPasswordValid() {
        if (getOldPassword() == null) {
            return context.getString(R.string.txt_error_blank_op_change_password);
        } else {
            if (getOldPassword().isEmpty()) {
                return context.getString(R.string.txt_error_blank_op_change_password);
            } else {
                return null;
            }
        }
    }

    public String isEmptyPasswordValid() {
        if (getNewPassword() == null) {
            return context.getString(R.string.txt_error_blank_np_change_password);
        } else {
            if (getNewPassword().isEmpty()) {
                return context.getString(R.string.txt_error_blank_np_change_password);
            } else if (getNewPassword().length() < 8) {
                return context.getString(R.string.txt_error_password_length);
            } else {
                return null;
            }
        }
    }

    public String isConfirmPasswordValid() {
        if (getConfirmPassword() == null) {
            return context.getString(R.string.txt_error_blank_cp_change_password);
        } else {
            if (getConfirmPassword().isEmpty()) {
                return context.getString(R.string.txt_error_blank_cp_change_password);
            } else if (!getConfirmPassword().equals(getNewPassword())) {
                return context.getString(R.string.txt_error_change_pass_confirm_pass);
            } else {
                return null;
            }
        }
    }

    private HashMap<String, String> getChangePasswordData() {

        HashMap<String, String> notificationStatus = new HashMap<>();

        String userId = String.valueOf(DataManager.getInstance().getUserId());

        notificationStatus.put(AppConstants.NetworkConstants.PARAM_USER_ID, userId);
        notificationStatus.put(AppConstants.NetworkConstants.PARAM_OLD_PASS, oldPassword.get());
        notificationStatus.put(AppConstants.NetworkConstants.PARAM_NEW_PASS, newPassword.get());

        return notificationStatus;
    }

    public boolean isFormFieldsValid() {
        return (isOldPasswordValid() == null && isEmptyPasswordValid() == null && isConfirmPasswordValid() == null);
    }

    public void showErrorMessages() {
        if (isOldPasswordValid() != null) {
            Toast.makeText(context, isOldPasswordValid(), Toast.LENGTH_SHORT).show();
        } else if (isEmptyPasswordValid() != null) {
            Toast.makeText(context, isEmptyPasswordValid(), Toast.LENGTH_SHORT).show();
        } else if (isConfirmPasswordValid() != null) {
            Toast.makeText(context, isConfirmPasswordValid(), Toast.LENGTH_SHORT).show();
        }
    }

    public void hitApiToChangePassword(final RichMediatorLiveData<com.whizzly.models.change_password.ChangePasswordModel> changePasswordRichMediatorLiveData) {


        if (AppUtils.isNetworkAvailable(context)) {

            DataManager.getInstance().hitChangePasswordApi(getChangePasswordData()).enqueue(new NetworkCallback<com.whizzly.models.change_password.ChangePasswordModel>() {
                @Override
                public void onSuccess(com.whizzly.models.change_password.ChangePasswordModel submitProfileBean) {
                    changePasswordRichMediatorLiveData.setValue(submitProfileBean);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    changePasswordRichMediatorLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    changePasswordRichMediatorLiveData.setError(t);
                }
            });

        }


    }

    @Bindable
    public boolean getIsOldShowPassword() {
        return isOldShowPassword.get();
    }

    public void setIsOldShowPassword(boolean isOldShowPassword) {
        this.isOldShowPassword.set(isOldShowPassword);
        notifyPropertyChanged(BR.isOldShowPassword);

    }

    @Bindable
    public boolean getIsConfirmShowPassword() {
        return isConfirmShowPassword.get();

    }

    public void setIsConfirmShowPassword(boolean isConfirmShowPassword) {
        this.isConfirmShowPassword.set(isConfirmShowPassword);
        notifyPropertyChanged(BR.isConfirmShowPassword);
    }

    @Bindable
    public boolean getIsNewShowPassword() {
        return isNewShowPassword.get();

    }

    public void setIsNewShowPassword(boolean isNewShowPassword) {
        this.isNewShowPassword.set(isNewShowPassword);
        notifyPropertyChanged(BR.isNewShowPassword);

    }
}
