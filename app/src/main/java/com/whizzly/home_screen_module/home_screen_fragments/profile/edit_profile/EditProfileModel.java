package com.whizzly.home_screen_module.home_screen_fragments.profile.edit_profile;

import android.content.Context;
import androidx.databinding.BaseObservable;
import androidx.databinding.ObservableField;
import android.widget.Toast;

import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.profile.ProfileInfo;
import com.whizzly.models.submit_profile.SubmitProfileBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class EditProfileModel extends BaseObservable {

    ObservableField<ProfileInfo> profileInfo = new ObservableField<>();

    private Context context;

    public EditProfileModel(Context context) {
        this.context = context;
    }

    public ProfileInfo getProfileInfo() {
        return profileInfo.get();
    }

    public void setProfileInfo(ProfileInfo profileInfo) {
        this.profileInfo.set(profileInfo);
        notifyPropertyChanged(BR.profileData);
    }

    public void setProfileImage(String url) {
        this.profileInfo.get().setProfileImage(url);
        notifyPropertyChanged(BR.profileData);
    }



    public HashMap<String, String> getData(int gender) {
        int userId = DataManager.getInstance().getUserId();
        HashMap<String, String> submitProfile = new HashMap<>();
        if (getProfileInfo().getUserName() != null)
            submitProfile.put(AppConstants.NetworkConstants.SUBMIT_PROFILE_PARAM_1, getProfileInfo().getUserName().trim());
        submitProfile.put(AppConstants.NetworkConstants.SUBMIT_PROFILE_PARAM_2, String.valueOf(userId));
        if (getProfileInfo().getFirstName() != null)
            submitProfile.put(AppConstants.NetworkConstants.SUBMIT_PROFILE_PARAM_3, getProfileInfo().getFirstName().trim());
        submitProfile.put(AppConstants.NetworkConstants.SUBMIT_PROFILE_PARAM_5, getProfileInfo().getProfileImage());
        if (getProfileInfo().getUserBio() != null)
            submitProfile.put(AppConstants.NetworkConstants.USER_BIO, getProfileInfo().getUserBio().trim());
        if (gender!=-1)
            submitProfile.put(AppConstants.NetworkConstants.GENDER, String.valueOf(gender));

        return submitProfile;
    }

    public String isValidName() {
        if (getProfileInfo().getFirstName() == null) {
            return context.getString(R.string.txt_error_name_empty);
        } else {
            if (getProfileInfo().getFirstName().isEmpty()) {
                return context.getString(R.string.txt_error_name_empty);
            } else {
                return null;
            }
        }
    }


    public void hitSubmitProfileInfo(final RichMediatorLiveData<SubmitProfileBean> submitProfileBeanRichMediatorLiveData, HashMap<String, String> submitProfile) {
        if (AppUtils.isNetworkAvailable(context)) {
            DataManager.getInstance().hitSubmitProfile(submitProfile).enqueue(new NetworkCallback<SubmitProfileBean>() {
                @Override
                public void onSuccess(SubmitProfileBean submitProfileBean) {
                    submitProfileBeanRichMediatorLiveData.setValue(submitProfileBean);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    submitProfileBeanRichMediatorLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    submitProfileBeanRichMediatorLiveData.setError(t);
                }
            });

        } else {
            Toast.makeText(context, context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }

}
