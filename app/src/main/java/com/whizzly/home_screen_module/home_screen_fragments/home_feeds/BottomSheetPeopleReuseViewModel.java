package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.reuse_response.ReuseResponse;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class BottomSheetPeopleReuseViewModel extends ViewModel {
    private RichMediatorLiveData<ReuseResponse> likersListResponseRichMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private OnClickSendListener onClickSendListener;

    public RichMediatorLiveData<ReuseResponse> getLikersListResponseRichMediatorLiveData() {
        return likersListResponseRichMediatorLiveData;
    }

    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureObserver;
        initializeLiveData();
    }

    private void initializeLiveData() {
        if (likersListResponseRichMediatorLiveData == null) {
            likersListResponseRichMediatorLiveData = new RichMediatorLiveData<ReuseResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
    }

    public void getReuseList(HashMap<String, String> reuseQuery) {
        DataManager.getInstance().hitReuseListApi(reuseQuery).enqueue(new NetworkCallback<ReuseResponse>() {
            @Override
            public void onSuccess(ReuseResponse reuseResponse) {
                likersListResponseRichMediatorLiveData.setValue(reuseResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                likersListResponseRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                likersListResponseRichMediatorLiveData.setError(t);
            }
        });
    }

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void onDropDownClicked(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_DROP_DOWN_CLICKED);
    }}
