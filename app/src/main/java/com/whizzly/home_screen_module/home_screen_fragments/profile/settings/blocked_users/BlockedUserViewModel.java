package com.whizzly.home_screen_module.home_screen_fragments.profile.settings.blocked_users;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.blockedUsers.BlockedUsersResponseBean;
import com.whizzly.utils.AppConstants;

public class BlockedUserViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;
    private BlockedUserModel mBlockedUserModel;
    private RichMediatorLiveData<BlockedUsersResponseBean> mBlockedUsersRichMediatorLiveData;
    private RichMediatorLiveData<BlockedUsersResponseBean> mUnblockedUserRichMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<FailureResponse> validateLiveData;

    public RichMediatorLiveData<BlockedUsersResponseBean> getBlockedUsersRichMediatorLiveData()
    {
        return mBlockedUsersRichMediatorLiveData;
    }

    public RichMediatorLiveData<BlockedUsersResponseBean> getmUnblockedUserRichMediatorLiveData() {
        return mUnblockedUserRichMediatorLiveData;
    }

    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (mBlockedUsersRichMediatorLiveData == null) {
            mBlockedUsersRichMediatorLiveData = new RichMediatorLiveData<BlockedUsersResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

        if (mUnblockedUserRichMediatorLiveData == null) {
            mUnblockedUserRichMediatorLiveData = new RichMediatorLiveData<BlockedUsersResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }


        if (validateLiveData == null)
            validateLiveData = new MutableLiveData<>();
    }



    public void onBackClick() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_BLOCKED_BACK_PRESSED);
    }

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void setModel(BlockedUserModel mBlockedUserModel) {
        this.mBlockedUserModel = mBlockedUserModel;
    }
}
