package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;

import android.content.Context;
import android.content.DialogInterface;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.ItemRowVideoCommentBinding;
import com.whizzly.interfaces.OnSendCommentDataListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.comment_delete.CommentDeleteResponse;
import com.whizzly.models.comment_list.Result;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class CommentListAdapter extends RecyclerView.Adapter<CommentListAdapter.ViewHolder> {

    private ItemRowVideoCommentBinding itemRowVideoCommentBinding;
    private int size;
    private List<Result> results;
    private OnSendCommentDataListener onSendCommentDataListener;
    private String videoType;
    private Context context;

    public CommentListAdapter(Context context, int size, List<Result> results, OnSendCommentDataListener onSendCommentDataListener, String videoType) {
        this.size = size;
        this.context = context;
        this.videoType = videoType;
        this.results = results;
        this.onSendCommentDataListener = onSendCommentDataListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        itemRowVideoCommentBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_row_video_comment, viewGroup, false);
        return new ViewHolder(itemRowVideoCommentBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(results.get(i));
        viewHolder.itemRowVideoCommentBinding.ivCommentThumb.setMaxWidth(size);
        viewHolder.itemRowVideoCommentBinding.ivCommentThumb.setMaxHeight(size);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_comment_placeholder);
        requestOptions.circleCrop();
        Glide.with(viewHolder.itemRowVideoCommentBinding.ivCommentThumb).load(results.get(i).getCommentUserProfileImage()).apply(requestOptions).into(viewHolder.itemRowVideoCommentBinding.ivCommentThumb);
        final GestureDetector gestureDetector = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
            public void onLongPress(MotionEvent e) {
                super.onLongPress(e);
                new AlertDialog.Builder(context).setTitle(context.getResources().getString(R.string.txt_delete_comment)).setMessage(context.getResources().getString(R.string.txt_delete_comment_text))
                        .setNegativeButton(context.getResources().getString(R.string.txt_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setPositiveButton(context.getResources().getString(R.string.txt_delete), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        HashMap<String, String> commentDeleteMap = new HashMap<>();
                        commentDeleteMap.put(AppConstants.ClassConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
                        commentDeleteMap.put(AppConstants.ClassConstants.COMMENT_ID, String.valueOf(results.get(i).getCommentId()));
                        if (videoType.equalsIgnoreCase("collab")) {
                            commentDeleteMap.put(AppConstants.ClassConstants.VIDEO_TYPE, "2");
                        } else {
                            commentDeleteMap.put(AppConstants.ClassConstants.VIDEO_TYPE, "1");
                        }

                        DataManager.getInstance().hitCommentDeleteApi(commentDeleteMap).enqueue(new NetworkCallback<CommentDeleteResponse>() {
                            @Override
                            public void onSuccess(CommentDeleteResponse commentDeleteResponse) {
                                if (commentDeleteResponse.getCode() == 200) {
                                    Toast.makeText(context, context.getString(R.string.txt_comment_deleted), Toast.LENGTH_SHORT).show();
                                    results.remove(i);
                                    notifyDataSetChanged();
                                }else if (commentDeleteResponse.getCode() == 301 && commentDeleteResponse.getCode() == 302) {
                                    Toast.makeText(context, commentDeleteResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    ((BaseActivity) context).autoLogOut();
                                }
                            }

                            @Override
                            public void onFailure(FailureResponse failureResponse) {

                            }

                            @Override
                            public void onError(Throwable t) {

                            }
                        });
                        dialog.dismiss();
                    }
                }).create().show();
            }

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                onSendCommentDataListener.onCommentClicked(results.get(i), i);
                return true;
            }

            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }
        });


        viewHolder.itemRowVideoCommentBinding.ivCommentThumb.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ItemRowVideoCommentBinding itemRowVideoCommentBinding;

        public ViewHolder(@NonNull ItemRowVideoCommentBinding itemRowVideoCommentBinding) {
            super(itemRowVideoCommentBinding.getRoot());
            this.itemRowVideoCommentBinding = itemRowVideoCommentBinding;
        }

        void bind(Result result) {
            itemRowVideoCommentBinding.setVariable(BR.model, result);
            itemRowVideoCommentBinding.executePendingBindings();
        }
    }
}
