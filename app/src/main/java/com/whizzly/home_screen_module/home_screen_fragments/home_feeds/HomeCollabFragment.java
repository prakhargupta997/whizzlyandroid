package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.chart_module.ReuseVideosChartActivity;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.FragmentHomeCollabBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.collab.collab_videos.CategoryVideo;
import com.whizzly.models.collab.collab_videos.CollabVideosResponseBean;
import com.whizzly.models.collab.collab_videos.VideoDatum;
import com.whizzly.notification.NotificationActivity;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;

import java.util.ArrayList;
import java.util.Objects;

public class HomeCollabFragment extends Fragment implements OnClickAdapterListener, OnClickSendListener {
    private HomeCollabViewModel mViewModel;
    private FragmentHomeCollabBinding mFragmentHomeCollabBinding;
    private Activity mActivity;
    private ExoPlayer mExoPlayer;
    private HomeCollabAdapter mHomeCollabAdapter;
    private ArrayList<VideoDatum> mCollabCategoryList;
    private String mUrl;
    private String mThumbNailUrl;
    private ArrayList<String> mUrls = new ArrayList<>();

    public static HomeCollabFragment newInstance() {
        return new HomeCollabFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mFragmentHomeCollabBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home_collab, container, false);
        mActivity = getActivity();
        mViewModel = ViewModelProviders.of(this).get(HomeCollabViewModel.class);
        mFragmentHomeCollabBinding.setViewModel(mViewModel);
        mViewModel.setGenericListeners(((HomeActivity) Objects.requireNonNull(getActivity())).getErrorObserver(), ((HomeActivity) Objects.requireNonNull(getActivity())).getFailureResponseObserver());
        mViewModel.setClickListener(this);
        observeLiveData();
        mCollabCategoryList = new ArrayList<>();
        setRecyclerView();
        getCollabListing();
        return mFragmentHomeCollabBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void setRecyclerView() {
        mFragmentHomeCollabBinding.rvFeeds.setLayoutManager(new LinearLayoutManager(getActivity()));
        mHomeCollabAdapter = new HomeCollabAdapter(getActivity(), mCollabCategoryList, this);
        mFragmentHomeCollabBinding.rvFeeds.setAdapter(mHomeCollabAdapter);

    }

    private void observeLiveData() {
        mViewModel.getCollabVideosLiveData().observe(this, new Observer<CollabVideosResponseBean>() {
            @Override
            public void onChanged(@Nullable CollabVideosResponseBean response) {
                mFragmentHomeCollabBinding.pbMain.setVisibility(View.GONE);
                if (response != null) {
                    if (response.getCode() == 200) {
                        if (response.getResult().getAdminVideo() != null && response.getResult().getAdminVideo().size() > 0 && response.getResult().getAdminVideo().get(0).getVideoUrl() != null) {
                            for (int i = 0; i < response.getResult().getAdminVideo().size(); i++) {
                                mUrls.add(response.getResult().getAdminVideo().get(i).getVideoUrl());
                            }
//                            mUrl = response.getResult().getAdminVideo().get(0).getVideoUrl();

                            setExoplayer(true);
                            mThumbNailUrl = response.getResult().getAdminVideo().get(0).getThumbnailUrl();
                        }
                        ArrayList<VideoDatum> list = new ArrayList<>();
                        list.addAll(response.getResult().getVideoData());
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getCategoryVideo().size() > 0)
                                mCollabCategoryList.add(response.getResult().getVideoData().get(i));

                        }
                        if (mCollabCategoryList.size() > 0)
                            mFragmentHomeCollabBinding.ivNoData.setVisibility(View.GONE);
                        else
                            mFragmentHomeCollabBinding.ivNoData.setVisibility(View.VISIBLE);

                        mHomeCollabAdapter.notifyDataSetChanged();
                    } else if (response.getCode() == 301 || response.getCode() == 302) {
                        Toast.makeText(mActivity, response.getMessage(), Toast.LENGTH_SHORT).show();
                        ((BaseActivity) Objects.requireNonNull(mActivity)).autoLogOut();
                    }
                }

            }
        });
    }

    private void getCollabListing() {
        if (AppUtils.isNetworkAvailable(mActivity))
            mViewModel.getCollabVideoListing();
        else
            Toast.makeText(mActivity, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();

    }


    private void setExoplayer(boolean playWhenReady) {
        if (mExoPlayer == null) {
            TrackSelection.Factory adaptiveTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(new DefaultBandwidthMeter());

            mExoPlayer = ExoPlayerFactory.newSimpleInstance(getActivity(),
                    new DefaultRenderersFactory(getActivity()),
                    new DefaultTrackSelector(adaptiveTrackSelectionFactory),
                    new DefaultLoadControl());

            mExoPlayer.setPlayWhenReady(playWhenReady);
            mExoPlayer.setRepeatMode(Player.REPEAT_MODE_ALL);
            MediaSource mediaSource = null;
            ConcatenatingMediaSource concatenatingMediaSource = new ConcatenatingMediaSource();
            for (int i = 0; i < mUrls.size(); i++) {
                mediaSource = buildMediaSource(Uri.parse(mUrls.get(0)));
//                concatenatingMediaSource.addMediaSource(mediaSource);
            }
            mExoPlayer.addListener(new Player.DefaultEventListener() {
                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    super.onPlayerStateChanged(playWhenReady, playbackState);
                    switch (playbackState) {
                        case Player.STATE_BUFFERING:
//                            mFragmentHomeCollabBinding.pbMain.setVisibility(View.VISIBLE);
                            break;
                        case Player.STATE_READY:
                            mFragmentHomeCollabBinding.pbMain.setVisibility(View.GONE);
                            break;
                    }
                }
            });
            if (mediaSource != null) {
                mExoPlayer.prepare(mediaSource, true, false);
                mFragmentHomeCollabBinding.pvVideo.setPlayer(mExoPlayer);
            }
        }
    }

    private MediaSource buildMediaSource(Uri uri) {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getActivity(),
                Util.getUserAgent(getActivity(), getString(R.string.app_name)), bandwidthMeter);
        return new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mExoPlayer == null && mUrl != null) {
            setExoplayer(true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mExoPlayer == null && mUrl != null) {
            getCollabListing();
        }

        if (mExoPlayer != null)
            mExoPlayer.setPlayWhenReady(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            if (mExoPlayer != null)
                mExoPlayer.setPlayWhenReady(false);
        }
    }

    private void releasePlayer() {
        if (mExoPlayer != null) {
            mExoPlayer.setPlayWhenReady(false);
            mExoPlayer.stop();
            mExoPlayer.release();
            mExoPlayer = null;
        }
    }

    @Override
    public void onDestroy() {
        releasePlayer();
        super.onDestroy();
    }

    @Override
    public void onAdapterClicked(int code, Object obj, int position) {
        switch (code) {
            case AppConstants.ClassConstants.ON_VIEW_MORE_VIEW_CLICKED:
                if (obj != null && obj instanceof VideoDatum) {
                    releasePlayer();
                    VideoDatum videoDatum = (VideoDatum) obj;
                    Intent intent = new Intent(mActivity, CategoryVideosActivity.class);
                    intent.putExtra(AppConstants.ClassConstants.VIDEO_DETAILS, videoDatum);
                    intent.putExtra(AppConstants.ClassConstants.VIDEO_THUMBNAIL_URL, mThumbNailUrl);
                    intent.putExtra(AppConstants.ClassConstants.VIDEO_FILE_URI, mUrls.get(0));
                    startActivity(intent);
                }
                break;
            case AppConstants.ClassConstants.ON_CATEGORY_VIDEO_CLICKED:
                if (obj != null && obj instanceof CategoryVideo) {
                    releasePlayer();
                    CategoryVideo categoryVideo = (CategoryVideo) obj;
                    Intent intent = new Intent(mActivity, NotificationActivity.class);
                    intent.putExtra(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.FROM_HOME_COLLAB);
                    intent.putExtra(AppConstants.ClassConstants.NOTIFICATION_DATA, categoryVideo);
                    startActivity(intent);

                }
        }
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_CHART_VIEW_CLICKED:
                if (mExoPlayer != null)
                    mExoPlayer.setPlayWhenReady(false);
                Intent intent = new Intent(mActivity, ReuseVideosChartActivity.class);
                startActivity(intent);
                break;
        }
    }
}
