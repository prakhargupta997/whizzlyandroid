package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.fragment.VideoPreviewFragment;
import com.whizzly.collab_module.fragment.VideoReplaceFragment;
import com.whizzly.databinding.ActivityVideoPreviewBinding;
import com.whizzly.models.feeds_response.Result;
import com.whizzly.utils.AppConstants;

public class VideoPreviewActivity extends BaseActivity {

    private ActivityVideoPreviewBinding activityVideoPreviewBinding;
    private Result result;
    private int isFrom;

    private void getData() {
        Intent intent = getIntent();
        if (getIntent() != null) {
            result = intent.getParcelableExtra(AppConstants.ClassConstants.FEEDS_DATA);
            isFrom = intent.getIntExtra(AppConstants.ClassConstants.FRAGMENT_IS_FROM, 0);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityVideoPreviewBinding = DataBindingUtil.setContentView(this, R.layout.activity_video_preview);
        getData();
        VideoReplaceFragment videoReplaceFragment = new VideoReplaceFragment();
        Bundle replaceBundle = new Bundle();
        replaceBundle.putInt(AppConstants.ClassConstants.FRAGMENT_IS_FROM, isFrom);
        replaceBundle.putParcelable(AppConstants.ClassConstants.FEEDS_DATA, result);
        videoReplaceFragment.setArguments(replaceBundle);
        addFragment(activityVideoPreviewBinding.flContainer.getId(), videoReplaceFragment, VideoReplaceFragment.class.getCanonicalName());
    }
}
