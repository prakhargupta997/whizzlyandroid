package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.ErrorDialog;
import com.whizzly.databinding.FragmentShareBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.home_screen_module.home_screen_activities.InnerFragmentsHostActivity;
import com.whizzly.home_screen_module.home_screen_fragments.profile.report.ReportIssueDialogFragment;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.interfaces.PermissionListener;
import com.whizzly.models.feeds_response.Result;
import com.whizzly.notification.FeedsFragment;
import com.whizzly.notification.NotificationActivity;
import com.whizzly.utils.AppConstants;

import org.jetbrains.annotations.NotNull;

import java.io.File;


public class BottomSheetShareFragment extends BottomSheetDialogFragment implements OnClickSendListener, PermissionListener {

    private static final int SAVE_INTENT_REQUEST_CODE = 101;
    private FragmentShareBinding mFragmentShareBinding;
    private BottomSheetShareViewModel mBottomSheetShareViewModel;
    private Result result;
    private Activity mActivity;
    private String isFrom;
    private boolean isFirstTime;
    private String downloadPath = "";
    private String downloadFileName = "";
    private String downloadDirectory = "";

    public BottomSheetShareFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mActivity = getActivity();
        mFragmentShareBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_share, container, false);
        getArgumentsData();
        mBottomSheetShareViewModel = ViewModelProviders.of(this).get(BottomSheetShareViewModel.class);
        mBottomSheetShareViewModel.setGenericListeners(((BaseActivity) getActivity()).getErrorObserver(), ((BaseActivity) getActivity()).getFailureResponseObserver());
        mFragmentShareBinding.setViewModel(mBottomSheetShareViewModel);
        setViews();
        observeLiveData();
        mBottomSheetShareViewModel.setOnClickListener(this);
        if (getActivity() instanceof HomeActivity)
            ((HomeActivity) getActivity()).setOnPermissionListener(this::onPermissionGiven);
        else if (getActivity() instanceof NotificationActivity)
            ((NotificationActivity) getActivity()).setOnPermissionListener(this::onPermissionGiven);
        return mFragmentShareBinding.getRoot();
    }

    private void observeLiveData() {
        mBottomSheetShareViewModel.getDeleteResponseLiveData().observe(getActivity(), ((BaseActivity) getActivity()).getDeleteObserver());
        Fragment fragment = getParentFragment();
        if (fragment != null) {
            if (fragment instanceof HomeFriendsFragment)
                mBottomSheetShareViewModel.getSaveVideoLiveData().observe(getActivity(), ((HomeFriendsFragment) fragment).geSavedFeedsObserver());
            else if (fragment instanceof HomePopularFragment)
                mBottomSheetShareViewModel.getSaveVideoLiveData().observe(getActivity(), ((HomePopularFragment) fragment).geSavedFeedsObserver());
            else if (fragment instanceof FeedsFragment)
                mBottomSheetShareViewModel.getSaveVideoLiveData().observe(getActivity(), ((FeedsFragment) fragment).geSavedFeedsObserver());

        }
    }

    private void setViews() {

        if (result.getSavedVideoPrivacy() == 1) {
            mFragmentShareBinding.tvPublicPrivate.setText(getString(R.string.s_save_as_public));
            mFragmentShareBinding.tvPublicPrivate.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_feed_video_share_make_public, 0, 0);
        } else {
            mFragmentShareBinding.tvPublicPrivate.setText(getString(R.string.s_save_as_private));
            mFragmentShareBinding.tvPublicPrivate.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_feed_video_share_make_private, 0, 0);

        }

        if (result != null && String.valueOf(result.getUserId()).equals(mBottomSheetShareViewModel.getUserId())) {
            mFragmentShareBinding.tvDelete.setVisibility(View.VISIBLE);
            mFragmentShareBinding.tvEdit.setVisibility(View.VISIBLE);
            mFragmentShareBinding.tvReportVideo.setVisibility(View.GONE);

        } else {
            mFragmentShareBinding.tvEdit.setVisibility(View.GONE);
            mFragmentShareBinding.tvDelete.setVisibility(View.GONE);
            mFragmentShareBinding.tvReportVideo.setVisibility(View.VISIBLE);

        }


    }

    private void getArgumentsData() {
        if (getArguments() != null && getArguments().containsKey(AppConstants.ClassConstants.IS_FROM)) {
            isFrom = getArguments().getString(AppConstants.ClassConstants.IS_FROM);
            if (getArguments().getParcelable(AppConstants.ClassConstants.VIDEO_DETAILS) != null)
                result = getArguments().getParcelable(AppConstants.ClassConstants.VIDEO_DETAILS);

        }
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_CANCEL_VIEW_CLICKED:
                dismiss();
                break;
            case AppConstants.ClassConstants.ON_EDIT_VIEW_CLICKED:
                Intent intent = new Intent(mActivity, InnerFragmentsHostActivity.class);
                intent.putExtra(AppConstants.ClassConstants.FRAGMENT_TYPE, AppConstants.ClassConstants.FRAGMENT_TYPE_EDIT_POST);
                intent.putExtra(AppConstants.ClassConstants.FEED_DATA, result);
                startActivity(intent);
                dismiss();
                break;
            case AppConstants.ClassConstants.ON_REPORT_VIEW_CLICKED:
                DialogFragment reportLinkDialog = new ReportIssueDialogFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelable(AppConstants.ClassConstants.VIDEO_DETAILS, result);
                bundle.putInt(AppConstants.ClassConstants.IS_FOR, AppConstants.ClassConstants.FOR_REPORT_LINK);
                reportLinkDialog.setArguments(bundle);
                reportLinkDialog.show(((BaseActivity) mActivity).getSupportFragmentManager(), ReportIssueDialogFragment.class.getCanonicalName());
                dismiss();

                break;
            case AppConstants.ClassConstants.ON_DELETE_VIEW_CLICKED:
                showConfirmationDialog();
                dismiss();

                break;
            case AppConstants.ClassConstants.ON_WHATSAPP_VIEW_CLICKED:
                shareURL(mActivity, result.getDeepLink(), "com.whatsapp");
                dismiss();

                break;
            case AppConstants.ClassConstants.ON_FACEBOOK_VIEW_CLICKED:
                shareURL(mActivity, result.getDeepLink(), "com.facebook.katana");
                break;
            case AppConstants.ClassConstants.ON_GMAIL_VIEW_CLICKED:
                shareURL(mActivity, result.getDeepLink(), "com.google.android.gm");
                dismiss();

                break;
            case AppConstants.ClassConstants.ON_INSTAGRAM_VIEW_CLICKED:
                shareURL(mActivity, result.getDeepLink(), "com.instagram.android");
                dismiss();

                break;
            case AppConstants.ClassConstants.ON_MESSAGE_CLICKED:
                onMessageClicked();
                dismiss();

                break;
            case AppConstants.ClassConstants.ON_COPY_LINK_CLICKED:
                copyLink();
                dismiss();
                break;
            case AppConstants.ClassConstants.ON_SAVE_LINK_CLICKED:
                mBottomSheetShareViewModel.hitSaveVideoApi(result.getId(), getVideoType(result.getType()), result.getSavedVideoPrivacy());
                dismiss();
                break;
            case AppConstants.ClassConstants.ON_DOWNLOAD_VIEW_CLICKED:
                if (checkPermissionsForCamera()) {
                    saveVideo();
                    dismiss();
                }

                break;

        }
    }

    public boolean checkPermissionsForCamera() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    ActivityCompat.requestPermissions((mActivity), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, SAVE_INTENT_REQUEST_CODE);
                } else {
                    if (!isFirstTime) {
                        ActivityCompat.requestPermissions((mActivity), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, SAVE_INTENT_REQUEST_CODE);
                        isFirstTime = true;
                    } else {
                        ErrorDialog errorDialog = new ErrorDialog();
                        if (mActivity instanceof HomeActivity)
                            errorDialog.show(((HomeActivity) mActivity).getSupportFragmentManager(), ErrorDialog.class.getCanonicalName());
                    }
                }
                return false;
            } else
                return true;
        } else
            return true;
    }

    private boolean hasPermission(int[] grantResults) {
        boolean hasPermissions = false;
        int permissionCount = 0;
        for (int grantResult : grantResults) {
            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                hasPermissions = true;
                permissionCount++;
            }
        }
        return hasPermissions && permissionCount == grantResults.length;
    }

    private String getDownloadDirectory() {
        String appDirectoryName = "Whizzly";
        final File videoRoot = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MOVIES), appDirectoryName);
        return videoRoot.getAbsolutePath() + "/";
    }

    private void saveVideo() {
        downloadDirectory = getDownloadDirectory();
        downloadFileName = "Whizzly_video_" + System.currentTimeMillis() + ".mp4";
        downloadPath = downloadDirectory + downloadFileName;
        PRDownloader.download(result.getWaterMarkUrl(), downloadDirectory, downloadFileName)
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {
                        Toast.makeText(mActivity, "Woohoo! Downloading this interesting Collab.", Toast.LENGTH_SHORT).show();
                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {

                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {
                    }
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        ContentValues values = new ContentValues(3);
                        values.put(MediaStore.Video.Media.TITLE, result.getVideoDescription());
                        values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
                        values.put(MediaStore.Video.Media.DATA, downloadPath);
                        mActivity.getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);
                        Toast.makeText(mActivity, mActivity.getString(R.string.txt_video_saved_successfully), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(Error error) {
                        saveVideo();
                    }


                });
        dismiss();
    }

    private boolean isPackageInstalled(String packageName) {
        boolean found = true;
        PackageManager pm = getActivity().getPackageManager();
        try {
            pm.getPackageInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            found = false;
        }
        return found;
    }

    private void onMessageClicked() {
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.setData(Uri.parse("sms:"));
        sendIntent.putExtra("sms_body", result.getDeepLink());
        startActivity(sendIntent);
    }

    private void copyLink() {
        ClipboardManager clipboard = (ClipboardManager) mActivity.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Video", result.getDeepLink());
        clipboard.setPrimaryClip(clip);
        Toast.makeText(mActivity, getResources().getString(R.string.txt_copied_link_successfully), Toast.LENGTH_SHORT).show();

    }


    public void shareURL(Context context, String mUrl, String mPackageName) {
        if (isPackageInstalled(mPackageName)) {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.setPackage(mPackageName);
            i.putExtra(Intent.EXTRA_SUBJECT, "com.whizzly");
            i.putExtra(Intent.EXTRA_TEXT, mUrl);
            context.startActivity(Intent.createChooser(i, getResources().getString(R.string.txt_share_url)));
        } else {
            Toast.makeText(context, getResources().getString(R.string.txt_app_not_installed), Toast.LENGTH_SHORT).show();
        }
    }


    private void showConfirmationDialog() {
        new AlertDialog.Builder(getActivity())
                .setMessage(R.string.txt_delete_post_dialog)
                .setPositiveButton(R.string.txt_yes, (dialogInterface, i) -> {
                    DeleteApiHit(dialogInterface);
                    dismiss();
                })
                .setNegativeButton(R.string.txt_no, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    dismiss();
                })
                .create().show();
    }


    private void DeleteApiHit(DialogInterface dialogInterface) {
        mBottomSheetShareViewModel.hitDeletePostApi(result.getId(), getVideoType(result.getType()), 1);
        dialogInterface.dismiss();
    }

    private int getVideoType(String type) {
        int videoType;
        if (type.equalsIgnoreCase("Collab"))
            videoType = 2;
        else
            videoType = 1;
        return videoType;
    }


    @Override
    public void onPermissionGiven(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == SAVE_INTENT_REQUEST_CODE && grantResults.length == permissions.length && hasPermission(grantResults))
            saveVideo();
        else
            dismiss();
    }
}
