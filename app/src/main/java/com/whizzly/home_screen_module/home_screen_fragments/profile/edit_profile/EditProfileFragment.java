package com.whizzly.home_screen_module.home_screen_fragments.profile.edit_profile;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dnitinverma.amazons3library.AmazonS3;
import com.dnitinverma.amazons3library.AmazonS3Constants;
import com.dnitinverma.amazons3library.interfaces.AmazonCallback;
import com.dnitinverma.amazons3library.model.MediaBean;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.cropper.CropImage;
import com.whizzly.cropper.CropImageView;
import com.whizzly.databinding.DialogImageSelectBinding;
import com.whizzly.databinding.FragmentEditProfileBinding;
import com.whizzly.dialog.dialogViewModels.ImageSelectDialogViewModel;
import com.whizzly.home_screen_module.home_screen_activities.InnerFragmentsHostActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.profile.ProfileInfo;
import com.whizzly.models.submit_profile.SubmitProfileBean;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.CameraHelper;
import com.whizzly.utils.DataManager;

import java.util.Objects;

import static com.whizzly.utils.CameraHelper.CAMERA_INTENT_REQUEST_CODE;
import static com.whizzly.utils.CameraHelper.GALLERY_INTENT_REQUEST_CODE;

public class EditProfileFragment extends Fragment implements OnClickSendListener, AmazonCallback, CameraHelper.ImageCallBack, CameraHelper.PermissionGranted {

    private Activity mActivity;
    private FragmentEditProfileBinding mFragmentProfileBinding;
    private EditProfileModel mEditProfileModel;
    private EditProfileViewModel mEditProfileViewModel;
    private AmazonS3 mAmazonS3;
    private CameraHelper mCameraHelper;
    private Dialog dialog;
    private String picturePath;
    private boolean isImageChanged = false;
    private ProfileInfo profileInfo;
    private int mGender = -1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mFragmentProfileBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_profile, container, false);
        mEditProfileViewModel = ViewModelProviders.of(this).get(EditProfileViewModel.class);
        mFragmentProfileBinding.setViewModel(mEditProfileViewModel);
        mEditProfileModel = new EditProfileModel(mActivity);
        mFragmentProfileBinding.setModel(mEditProfileModel);
        mEditProfileViewModel.setOnClickSendListener(this);

        getData();
        mFragmentProfileBinding.toolbar.ivBack.setOnClickListener(v -> mActivity.onBackPressed());

        mFragmentProfileBinding.toolbar.tvToolbarText.setText(R.string.txt_edit_profile);
        mFragmentProfileBinding.toolbar.ivBack.setVisibility(View.VISIBLE);
        if(DataManager.getInstance().isNotchDevice()){
            mFragmentProfileBinding.toolbar.view.setVisibility(View.VISIBLE);
        }else{
            mFragmentProfileBinding.toolbar.view.setVisibility(View.GONE);
        }
        mEditProfileViewModel.setGenericListeners(((InnerFragmentsHostActivity) mActivity).getErrorObserver(),
                ((InnerFragmentsHostActivity) mActivity).getFailureResponseObserver());

        mEditProfileViewModel.getEditProfileResponseLiveData().observe(this, new Observer<SubmitProfileBean>() {
            @Override
            public void onChanged(@Nullable SubmitProfileBean submitProfileBean) {
                mFragmentProfileBinding.bar.setVisibility(View.GONE);
                if (submitProfileBean != null) {
                    if (submitProfileBean.getCode() == 301 || submitProfileBean.getCode() == 302) {
                        Toast.makeText(mActivity, submitProfileBean.getMessage(), Toast.LENGTH_SHORT).show();
                        ((BaseActivity) getActivity()).autoLogOut();
                    } else if (submitProfileBean.getCode() == 200) {
                        DataManager.getInstance().setProfilePic(submitProfileBean.getResult().getProfileImage());
                        Toast.makeText(getActivity(), submitProfileBean.getMessage(), Toast.LENGTH_SHORT).show();
                        mActivity.onBackPressed();
                    } else {
                        Toast.makeText(getActivity(), submitProfileBean.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        setListener();
        return mFragmentProfileBinding.getRoot();
    }

    private void setListener() {
        mFragmentProfileBinding.rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup arg0, int id) {
                switch (id) {
                    case R.id.rb_male:
                        mGender = 1;
                        break;
                    case R.id.rb_female:
                        mGender = 2;
                        break;
                    case R.id.rb_other:
                        mGender = 3;
                        break;
                    default:
                        mGender = -1;

                }
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (profileInfo.getProfileImage() != null)
            Glide.with(this).load(profileInfo.getProfileImage()).into(mFragmentProfileBinding.ivProfile);
        if (profileInfo.getGender() > 0) {
            if (profileInfo.getGender() == 1)
                mFragmentProfileBinding.rbMale.setChecked(true);
            else if (profileInfo.getGender() == 2)
                mFragmentProfileBinding.rbFemale.setChecked(true);
            else if(profileInfo.getGender() == 3)
                mFragmentProfileBinding.rbOther.setChecked(true);
            else {
                mFragmentProfileBinding.rbFemale.setChecked(false);
                mFragmentProfileBinding.rbMale.setChecked(false);
                mFragmentProfileBinding.rbOther.setChecked(false);
            }


        }

    }

    private void initializeAmazonS3() {
        mAmazonS3 = new AmazonS3();
        mAmazonS3.setActivity(mActivity);
        mAmazonS3.setCallback(this);
    }

    private void init() {
        mActivity = getActivity();
        mCameraHelper = CameraHelper.getInstance();
        mCameraHelper.setCOntext(mActivity);
        mCameraHelper.setImageCallBack(this);
        initializeAmazonS3();
    }

    @Override
    public void uploadSuccess(MediaBean bean) {
        mEditProfileModel.hitSubmitProfileInfo(mEditProfileViewModel.getEditProfileResponseLiveData(), mEditProfileModel.getData(mGender));
        updateDataToFirebase(bean.getServerUrl());
    }

    private void updateDataToFirebase(String photoUrl) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child(AppConstants.FirebaseConstants.USERS).child(String.valueOf(DataManager.getInstance()
                .getUserId())).child(AppConstants.FirebaseConstants.PROFILE_IMAGE).setValue(photoUrl).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Log.e("onComplete: ", "Profile Picture Updated!");
            }
        });

    }

    @Override
    public void uploadFailed(MediaBean bean) {
        mAmazonS3.upload(bean);
    }

    @Override
    public void uploadProgress(MediaBean bean) {

    }

    @Override
    public void uploadError(Exception e, MediaBean imageBean) {
        mAmazonS3.upload(imageBean);
    }

    @Override
    public void onClickSend(int code) {

        switch (code) {
            case AppConstants.ClassConstants.CHANGE_PROFILE_CLICKED:
                showProfileImageDialog();
                break;
            case AppConstants.ClassConstants.ON_EDIT_PROFILE_SAVE:
                if (mEditProfileModel.isValidName() == null) {
                    if (AppUtils.isNetworkAvailable(mActivity)) {
                        //mActivityProfileSetUpBinding.bar.setVisibility(View.VISIBLE);
                        if (isImageChanged) {
                            uploadImage();
                            String imageUrl = AmazonS3Constants.AMAZON_SERVER_URL + getFileName(Uri.parse(picturePath));
                            mEditProfileModel.setProfileImage(imageUrl);
                        } else {
                            mEditProfileModel.hitSubmitProfileInfo(mEditProfileViewModel.getEditProfileResponseLiveData(), mEditProfileModel.getData(mGender));
                        }
                        mFragmentProfileBinding.bar.setVisibility(View.VISIBLE);
                        isImageChanged = false;
                    } else {
                        Toast.makeText(mActivity, getString(R.string.txt_no_internet_connection), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (mEditProfileModel.isValidName() != null) {
                        showToast(mEditProfileModel.isValidName());
                    }
                }
                break;

        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        result = uri.getPath();
        int cut = Objects.requireNonNull(result).lastIndexOf('/');
        if (cut != -1) {
            result = result.substring(cut + 1);
        }
        return result;
    }

    private void showToast(String text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
    }

    private void showProfileImageDialog() {
        dialog = new Dialog(mActivity, R.style.dialog);
        DialogImageSelectBinding binding = DataBindingUtil.inflate(LayoutInflater.from(mActivity), R.layout.dialog_image_select, null, false);

        ImageSelectDialogViewModel imageSelectDialogViewModel = new ImageSelectDialogViewModel(new OnClickSendListener() {
            @Override
            public void onClickSend(int code) {

                switch (code) {
                    case AppConstants.ClassConstants.EDIT_PROFILE_CAMERA:
                        mCameraHelper.openCamera(mActivity, null);
                        dialog.dismiss();
                        break;
                    case AppConstants.ClassConstants.EDIT_PROFILE_GALLERY:
                        mCameraHelper.openGallery(mActivity, null);
                        dialog.dismiss();
                        break;
                    case AppConstants.ClassConstants.EDIT_PROFILE_CANCEL:
                        dialog.dismiss();
                        break;
                }
            }
        });
        binding.setViewModel(imageSelectDialogViewModel);
        dialog.setContentView(binding.getRoot());
        binding.dialogImagePickerRootRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_INTENT_REQUEST_CODE) {
            mCameraHelper.onActivityResult(requestCode, resultCode, data);
        } else if (requestCode == CAMERA_INTENT_REQUEST_CODE) {
            mCameraHelper.onActivityResult(requestCode, resultCode, data);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Uri resultUri = null;
            if (result != null) {
                resultUri = result.getUri();
                picturePath = resultUri.getPath();
                setProfileImageLocalSource();
                isImageChanged = true;
            }
        }
    }

    private void setProfileImageLocalSource() {
        if (picturePath != null && !picturePath.isEmpty()) {
            RequestOptions requestOptions = new RequestOptions();
            Glide.with(mActivity)
                    .load(picturePath)
                    .apply(requestOptions.circleCrop())
                    .into(mFragmentProfileBinding.ivProfile);
        }

    }

    private void uploadImage() {
        if (picturePath != null && !picturePath.isEmpty()) {
            MediaBean mediaBean = addDataInBean(picturePath);
            mAmazonS3.upload(mediaBean);
        }
    }

    private MediaBean addDataInBean(String path) {
        MediaBean bean = new MediaBean();
        bean.setName("sample");
        bean.setMediaPath(path);
        return bean;
    }

    @Override
    public void onImageSuccess(String path) {
        CropImage.activity(Uri.parse("file:///" + path))
                .setCropShape(CropImageView.CropShape.OVAL)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setMinCropResultSize(60, 60)
                .setFixAspectRatio(true)
                .setMultiTouchEnabled(false)
                .setAutoZoomEnabled(true)
                .setAspectRatio(1, 1)
                .start(mActivity);
    }

    @Override
    public void onImageFailure(String errorText, int imageType) {

    }

    @Override
    public void onPermissionGranted(int requestCode) {
        switch (requestCode) {
            case CAMERA_INTENT_REQUEST_CODE:
                mCameraHelper.openCamera(mActivity, null);
                break;

            case GALLERY_INTENT_REQUEST_CODE:
                mCameraHelper.openGallery(mActivity, null);
                break;
        }
    }

    public void getData() {
        if (getArguments() != null && getArguments().containsKey(AppConstants.ClassConstants.USER_INFO)) {
            profileInfo = getArguments().getParcelable(AppConstants.ClassConstants.USER_INFO);
            mEditProfileModel.setProfileInfo(profileInfo);
        }
    }
}
