package com.whizzly.home_screen_module.home_screen_fragments.user;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.search.users.UserSearchResponseBean;
import com.whizzly.utils.DataManager;

public class UserViewModel extends ViewModel {
    private UserModel mUserModel;

    private RichMediatorLiveData<UserSearchResponseBean> mUserSearchResponseBeanLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;

    public void setModel(UserModel userModel) {
        this.mUserModel=userModel;
    }

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver)
    {
        this.errorObserver=errorObserver;
        this.failureResponseObserver=failureResponseObserver;
        initLiveData();
    }


    private void initLiveData() {
        if (mUserSearchResponseBeanLiveData == null) {
            mUserSearchResponseBeanLiveData = new RichMediatorLiveData<UserSearchResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

    }



    public RichMediatorLiveData<UserSearchResponseBean> getUserSearchLiveData() {
        return mUserSearchResponseBeanLiveData;
    }

    public void getUserListing(String mSearch, int mPage) {
        mUserModel.hitUserListingApi(mSearch,mPage,mUserSearchResponseBeanLiveData);
    }

    public Integer getUserId() {
        return DataManager.getInstance().getUserId();
    }
}
