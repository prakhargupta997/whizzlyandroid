package com.whizzly.home_screen_module.home_screen_fragments.profile;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.appbar.AppBarLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.chat.ChatActivity;
import com.whizzly.chat.model.Room;
import com.whizzly.databinding.FragmentProfileBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.home_screen_module.home_screen_activities.InnerFragmentsHostActivity;
import com.whizzly.home_screen_module.home_screen_fragments.home_feeds.HomeFragment;
import com.whizzly.home_screen_module.home_screen_fragments.profile.followers.BottomSheetFollowersListFragment;
import com.whizzly.home_screen_module.home_screen_fragments.profile.posted.ProfileVideosFragment;
import com.whizzly.home_screen_module.home_screen_fragments.profile.report.ReportIssueDialogFragment;
import com.whizzly.home_screen_module.home_screen_fragments.profile.settings.RankingFragment;
import com.whizzly.interfaces.OnBackPressedListener;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.blockedUsers.BlockedUsersResponseBean;
import com.whizzly.models.followUnfollow.FollowUnfollowResponseBean;
import com.whizzly.models.followers_following_response.FollowersResponseBean;
import com.whizzly.models.profile.ProfileResponseBean;
import com.whizzly.utils.AppBarStateChangeListener;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;
import com.whizzly.utils.GenericFragmentPagerAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements OnClickSendListener, OnBackPressedListener {

    private ProfileModel mProfileModel;
    private ProfileViewModel mProfileViewModel;
    private Observer<ProfileResponseBean> observer;
    private Observer<BlockedUsersResponseBean> blockObserver;
    private Observer<FollowUnfollowResponseBean> followUnfollowObserver;
    private Activity mActivity;
    private FragmentProfileBinding mFragmentProfileBinding;
    private int accountType;
    private String userId;
    private FollowersResponseBean mFollowersResponseBean;
    private FollowersResponseBean mFollowingresponseBean;
    private int mOffset = 20, mPage = 1;
    private Dialog dialog;
    private List<Fragment> mList = new ArrayList<>();
    private int mIsFrom;
    private DatabaseReference mDatabaseReference;
    private String roomId;
    private boolean isFromRefresh = false;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getArgumentData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mActivity = getActivity();
        mFragmentProfileBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        mProfileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
        mFragmentProfileBinding.setViewModel(mProfileViewModel);
        mProfileModel = new ProfileModel(mActivity);
        mFragmentProfileBinding.setModel(mProfileModel);
        mProfileViewModel.setOnClickSendListener(this);
        mProfileViewModel.setModel(mProfileModel);
        mProfileViewModel.setGenericListeners(((BaseActivity) getActivity()).getErrorObserver(), ((BaseActivity) getActivity()).getFailureResponseObserver());
        mProfileModel.setAccountType(accountType);
        observeApiResponse();
        observeBlockResponse();
        if(DataManager.getInstance().isNotchDevice()){
            mFragmentProfileBinding.view.setVisibility(View.VISIBLE);
            mFragmentProfileBinding.viewConstraint.setVisibility(View.VISIBLE);
        }else{
            mFragmentProfileBinding.viewConstraint.setVisibility(View.GONE);
            mFragmentProfileBinding.view.setVisibility(View.GONE);
        }
        observeFollowUnfollowResponse();
        observeFollowerLiveData();
        observeFollowingLiveData();
        ((BaseActivity) getActivity()).setOnBackPressedListener(this);
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        if (accountType == AppConstants.OTHER_USER_PROFILE)
            mFragmentProfileBinding.ivBack.setVisibility(View.VISIBLE);
        else {
            if (mIsFrom == AppConstants.ClassConstants.FROM_COMMENTS)
                mFragmentProfileBinding.ivBack.setVisibility(View.VISIBLE);
            else
                mFragmentProfileBinding.ivBack.setVisibility(View.GONE);
        }

        mFragmentProfileBinding.ivBack.setOnClickListener(v ->
                getActivity().onBackPressed());
        return mFragmentProfileBinding.getRoot();
    }

    private void setListener() {
        mFragmentProfileBinding.srvProfile.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                hitApiToFetchProfileData(accountType);
                mPage = 1;
                getFollowerOrFollowingList(mPage, mOffset);
                if (mList != null && mList.size() > 0) {
                    for (int i = 0; i < mList.size(); i++) {
                        ((ProfileVideosFragment) mList.get(i)).hitVideoApi(1, 1, true);
                    }
                }
            }
        });
    }

    private void observeFollowerLiveData() {
        mProfileViewModel.getFollowersListliveData().observe(this, new Observer<FollowersResponseBean>() {
            @Override
            public void onChanged(@Nullable FollowersResponseBean response) {
                if (response != null) {
                    if (response.getCode() == 200) {
                        if (mFragmentProfileBinding.srvProfile.isRefreshing()) {
                            mFragmentProfileBinding.srvProfile.setRefreshing(false);
                        }
                        mFollowersResponseBean = response;
                        /*int count = 0;
                        for (int i = 0; i < mFollowersResponseBean.getResult().size(); i++) {
                            if (mFollowersResponseBean.getResult().get(i).getUserStatus() == 0) {
                                count++;
                            }
                        }*/
                        mFollowersResponseBean.setCount(mFollowersResponseBean.getResult().size());
                        mFragmentProfileBinding.tvFollowers.setText(String.valueOf(mFollowersResponseBean.getResult().size()));

                    } else if (response.getCode() == 301 || response.getCode() == 302) {
                        Toast.makeText(mActivity, response.getMessage(), Toast.LENGTH_SHORT).show();
                        ((BaseActivity) Objects.requireNonNull(getActivity())).autoLogOut();

                    }
                }
            }
        });
    }

    public int getAccountType() {
        return accountType;
    }

    private void observeFollowingLiveData() {
        mProfileViewModel.getFollowingListLiveData().observe(this, new Observer<FollowersResponseBean>() {
            @Override
            public void onChanged(@Nullable FollowersResponseBean response) {
                if (response != null) {
                    if (response.getCode() == 200) {
                        if (mFragmentProfileBinding.srvProfile.isRefreshing()) {
                            mFragmentProfileBinding.srvProfile.setRefreshing(false);
                        }
                        mFollowingresponseBean = response;
                        /*int count = 0;
                        for (int i = 0; i < mFollowingresponseBean.getResult().size(); i++) {
                            if (mFollowingresponseBean.getResult().get(i).getUserStatus() == 0) {
                                count++;
                            }
                        }*/
                        mFollowingresponseBean.setCount(mFollowingresponseBean.getResult().size());
                        mFragmentProfileBinding.tvFollowing.setText(String.valueOf(mFollowingresponseBean.getResult().size()));

                    } else if (response.getCode() == 301 || response.getCode() == 302) {
                        Toast.makeText(mActivity, response.getMessage(), Toast.LENGTH_SHORT).show();
                        ((BaseActivity) Objects.requireNonNull(getActivity())).autoLogOut();
                    }
                }

            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViewPager();
        setListener();
        setViews();
        // hitApiToFetchProfileData(accountType);
        getFollowerOrFollowingList(mPage, mOffset);
    }

    private void setViews() {
        mFragmentProfileBinding.abToolbar.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {
                if (state == State.EXPANDED) {
                    mFragmentProfileBinding.srvProfile.setEnabled(true);
                } else {
                    mFragmentProfileBinding.srvProfile.setEnabled(false);
                }
            }
        });
    }

    private void getArgumentData() {
        Bundle profileDetails = getArguments();
        if (profileDetails != null) {
            accountType = profileDetails.getInt(AppConstants.ClassConstants.ACCOUNT_TYPE);
            userId = profileDetails.getString(AppConstants.ClassConstants.USER_ID);
            if (profileDetails.containsKey(AppConstants.ClassConstants.IS_FROM))
                mIsFrom = profileDetails.getInt(AppConstants.ClassConstants.IS_FROM, 0);

        }
    }

    private void setupViewPager() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, accountType);
        if (accountType == AppConstants.OTHER_USER_PROFILE) {
            bundle.putString(AppConstants.ClassConstants.USER_ID, userId);
        }
        bundle.putInt(AppConstants.ClassConstants.VIDEO_TYPE, AppConstants.VIDEO_TYPE_POSTED);
        ProfileVideosFragment postedFragment = new ProfileVideosFragment();
        postedFragment.setArguments(bundle);


        Bundle bundle1 = new Bundle();
        bundle1.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, accountType);
        if (accountType == AppConstants.OTHER_USER_PROFILE) {
            bundle1.putString(AppConstants.ClassConstants.USER_ID, userId);
        }
        bundle1.putInt(AppConstants.ClassConstants.VIDEO_TYPE, AppConstants.VIDEO_TYPE_SAVED);
        ProfileVideosFragment savedFragment = new ProfileVideosFragment();
        savedFragment.setArguments(bundle1);

        mList.add(postedFragment);
        mList.add(savedFragment);


        List<String> titles = new ArrayList<>();

        titles.add(getString(R.string.s_posted));
        titles.add(getString(R.string.s_saved));

        GenericFragmentPagerAdapter pagerAdapter = new GenericFragmentPagerAdapter(getChildFragmentManager(), mList, titles);
        mFragmentProfileBinding.layout.vpProfile.setAdapter(pagerAdapter);
        mFragmentProfileBinding.layout.slidingTabs.setupWithViewPager(mFragmentProfileBinding.layout.vpProfile);
    }

    private int ratingCount = 0;

    private void observeApiResponse() {
        observer = profileResponseBean -> {
            mFragmentProfileBinding.pbMain.setVisibility(View.GONE);
            if (profileResponseBean != null) {
                if (profileResponseBean.getCode() == 200) {
                    if (mFragmentProfileBinding.srvProfile.isRefreshing()) {
                        mFragmentProfileBinding.srvProfile.setRefreshing(false);
                    }
                    ratingCount = profileResponseBean.getResult().getReusesByOthers()+profileResponseBean.getResult().getReusesByOthers();
                    mFragmentProfileBinding.abToolbar.setVisibility(View.VISIBLE);
                    mFragmentProfileBinding.layout.vpProfile.setVisibility(View.VISIBLE);
                    mProfileModel.setProfileData(profileResponseBean);
                    if (mProfileModel.getProfileData().getResult().getUserBio() != null && !mProfileModel.getProfileData().getResult().getUserBio().equals(""))
                        mFragmentProfileBinding.tvDescription.setVisibility(View.VISIBLE);
                    else
                        mFragmentProfileBinding.tvDescription.setVisibility(View.INVISIBLE);


                    if (accountType == AppConstants.OTHER_USER_PROFILE) {
                        if (profileResponseBean.getResult().getIsBlocked() == 1) {
                            mFragmentProfileBinding.tvUnblock.setVisibility(View.VISIBLE);
                            mFragmentProfileBinding.btnFollow.setVisibility(View.INVISIBLE);
                            mFragmentProfileBinding.btnMessage.setVisibility(View.INVISIBLE);
                            mFragmentProfileBinding.btnEditProfile.setVisibility(View.INVISIBLE);
                        } else {
                            mFragmentProfileBinding.tvUnblock.setVisibility(View.INVISIBLE);
                            mFragmentProfileBinding.btnFollow.setVisibility(View.VISIBLE);
                            mFragmentProfileBinding.btnMessage.setVisibility(View.VISIBLE);
                            mFragmentProfileBinding.btnEditProfile.setVisibility(View.INVISIBLE);
                        }
                    }
                } else if (profileResponseBean.getCode() == 301 || profileResponseBean.getCode() == 302) {
                    Toast.makeText(mActivity, profileResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) Objects.requireNonNull(getActivity())).autoLogOut();
                } else {

                    Toast.makeText(mActivity, profileResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

        };
    }

    private void observeBlockResponse() {
        blockObserver = blockedUsersResponseBean -> {
            if (blockedUsersResponseBean != null) {
                if (blockedUsersResponseBean.getCode() == 200) {
                    manageBlockView(blockedUsersResponseBean.getMessage());
                    Toast.makeText(mActivity, blockedUsersResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (blockedUsersResponseBean.getCode() == 301 || blockedUsersResponseBean.getCode() == 302) {
                    Toast.makeText(mActivity, blockedUsersResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) Objects.requireNonNull(getActivity())).autoLogOut();
                } else {
                    Toast.makeText(mActivity, blockedUsersResponseBean.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

        };
    }

    private void observeFollowUnfollowResponse() {

        followUnfollowObserver = followUnfollowResponseBean -> {
            if (followUnfollowResponseBean != null) {
                if (followUnfollowResponseBean.getCode() == 200) {
                    mProfileModel.toggleFollowStatus();
                    mFragmentProfileBinding.executePendingBindings();
                } else if (followUnfollowResponseBean.getCode() == 301 || followUnfollowResponseBean.getCode() == 302) {
                    Toast.makeText(mActivity, followUnfollowResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) Objects.requireNonNull(getActivity())).autoLogOut();
                } else {
                    Toast.makeText(mActivity, followUnfollowResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

        };
    }

    @Override
    public void onClickSend(int code) {

        switch (code) {
            case AppConstants.ClassConstants.SUGGESTED_BACK_PRESSED:
                getActivity().onBackPressed();
                break;

            case AppConstants.ClassConstants.PROFILE_SETTINGS_PRESSED:
                if (accountType == AppConstants.MY_PROFILE) {
                    Intent settingsIntent = new Intent(mActivity, InnerFragmentsHostActivity.class);
                    settingsIntent.putExtra(AppConstants.ClassConstants.RATING_COUNT, ratingCount);
                    settingsIntent.putExtra(AppConstants.ClassConstants.FRAGMENT_TYPE, AppConstants.ClassConstants.FRAGMENT_TYPE_SETTINGS);
                    startActivity(settingsIntent);
                } else if (accountType == AppConstants.OTHER_USER_PROFILE) {
                    //showOtherUserSettingsDialog();
                    showOtherUserProfileSettingsDialog();
                }

                break;
            case AppConstants.ClassConstants.EDIT_PROFILE_CLICKED:
                if (mProfileModel.getProfileData() != null) {
                    Intent editProfileIntent = new Intent(mActivity, InnerFragmentsHostActivity.class);
                    editProfileIntent.putExtra(AppConstants.ClassConstants.FRAGMENT_TYPE, AppConstants.ClassConstants.FRAGMENT_TYPE_EDIT_PROFILE);
                    editProfileIntent.putExtra(AppConstants.ClassConstants.USER_INFO, mProfileModel.getProfileData().getResult());
                    startActivity(editProfileIntent);
                }
                break;

            case AppConstants.ClassConstants.FOLLOW_BUTTON_PRESSED:
                mProfileModel.hitApiToFollowUnfollow(mProfileViewModel.getmFollowUnfollowResponseBeanRichMediatorLiveData(), userId);

                break;
            case AppConstants.ClassConstants.FOLLOWERS_VIEW_CLICKED:
                if (mFollowersResponseBean.getCount() > 0) {
                    BottomSheetFollowersListFragment bottomSheetFollowersListFragment = new BottomSheetFollowersListFragment();
                    Bundle bundle = new Bundle();
                    mFollowersResponseBean.setType(AppConstants.ClassConstants.FOLLOWERS);
                    mFollowersResponseBean.setAccountType(accountType);
                    if (accountType == AppConstants.OTHER_USER_PROFILE)
                        mFollowingresponseBean.setOtherUserId(userId);
                    bundle.putParcelable(AppConstants.ClassConstants.FOLLOWER_FOLLOWING_RESPONSE, mFollowersResponseBean);
                    bottomSheetFollowersListFragment.setArguments(bundle);
                    bottomSheetFollowersListFragment.show(getChildFragmentManager(), BottomSheetFollowersListFragment.class.getCanonicalName());
                } else
                    Toast.makeText(mActivity, R.string.s_no_followers_at_this_moment, Toast.LENGTH_SHORT).show();
                break;
            case AppConstants.ClassConstants.FOLLOWING_VIEW_CLICKED:
                if (mFollowingresponseBean.getCount() > 0) {
                    BottomSheetFollowersListFragment bottomSheetFollowingListFragment = new BottomSheetFollowersListFragment();
                    Bundle bundle1 = new Bundle();
                    mFollowingresponseBean.setType(AppConstants.ClassConstants.FOLLOWING);
                    mFollowingresponseBean.setAccountType(accountType);
                    if (accountType == AppConstants.OTHER_USER_PROFILE)
                        mFollowingresponseBean.setOtherUserId(userId);
                    bundle1.putParcelable(AppConstants.ClassConstants.FOLLOWER_FOLLOWING_RESPONSE, mFollowingresponseBean);
                    bottomSheetFollowingListFragment.setArguments(bundle1);
                    bottomSheetFollowingListFragment.show(getChildFragmentManager(), BottomSheetFollowersListFragment.class.getName());
                } else
                    Toast.makeText(mActivity, R.string.s_no_following_user_at_this_moment, Toast.LENGTH_SHORT).show();

                break;
            case AppConstants.ClassConstants.UNBLOCKED_VIEW_CLICKED:
                openConfirmationDialog();
                break;

            case AppConstants.ClassConstants.ON_MESSAGE_CLICKED:
                addRoomDataToFirebase();
                Intent intent = new Intent(getActivity(), ChatActivity.class);
                intent.putExtra(AppConstants.ClassConstants.ROOM_ID, roomId);
                intent.putExtra(AppConstants.ClassConstants.USER_NAME, mProfileModel.getProfileData().getResult().getUserName());
                intent.putExtra(AppConstants.ClassConstants.USER_ID, userId);
                intent.putExtra(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_PROFILE);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;

            case AppConstants.ClassConstants.ON_RANKING_CLICKED:
                RankingFragment rankingFragment = new RankingFragment();
                Bundle rankingBundle = new Bundle();
                rankingBundle.putInt(AppConstants.ClassConstants.RATING_COUNT, ratingCount);
                rankingFragment.setArguments(rankingBundle);
                rankingFragment.show(getChildFragmentManager(), RankingFragment.class.getCanonicalName());
                break;
        }
    }

    private void addRoomDataToFirebase() {
        if (accountType == AppConstants.OTHER_USER_PROFILE) {
            String currentUserId = String.valueOf(DataManager.getInstance().getUserId());
            if (DataManager.getInstance().getUserId() > Integer.valueOf(userId)) {
                roomId = currentUserId + "_" + userId;
            } else {
                roomId = userId + "_" + currentUserId;
            }

            mDatabaseReference.child(AppConstants.FirebaseConstants.INBOX).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (!dataSnapshot.hasChild(roomId)) {
                        mDatabaseReference.child(AppConstants.FirebaseConstants.INBOX).child(currentUserId).child(userId).setValue(roomId);
                        mDatabaseReference.child(AppConstants.FirebaseConstants.INBOX).child(userId).child(currentUserId).setValue(roomId);
                        setRoomData(roomId);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


        }
    }

    private void setRoomData(String roomId) {
        mDatabaseReference.child(AppConstants.FirebaseConstants.ROOM).child(roomId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null && dataSnapshot.child(AppConstants.FirebaseConstants.CREATED_AT).exists()) {
                    mDatabaseReference.child(AppConstants.FirebaseConstants.ROOM).child(roomId).child(AppConstants.FirebaseConstants.LAST_ACTIVE).setValue(System.currentTimeMillis());
                } else {
                    mDatabaseReference.child(AppConstants.FirebaseConstants.ROOM).child(roomId).setValue(new Room(System.currentTimeMillis(), System.currentTimeMillis(), "Direct", roomId, "0", "0", ""));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void openConfirmationDialog() {
        new AlertDialog.Builder(getActivity())
                .setMessage(getActivity().getString(R.string.s_unblock) + " " + mProfileModel.getProfileData().getResult().getUserName() + "?")
                .setPositiveButton(getActivity().getString(R.string.s_unblock), (dialogInterface, i) -> blockUser("unblock"))
                .setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss())
                .create().show();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mProfileViewModel.getmProfileResponseBeanRichMediatorLiveData().observe(this, observer);
        mProfileViewModel.getBlockUnblockBeanRichMediatorLiveData().observe(this, blockObserver);
        mProfileViewModel.getmFollowUnfollowResponseBeanRichMediatorLiveData().observe(this, followUnfollowObserver);
        hitApiToFetchProfileData(accountType);

    }

    private void hitApiToFetchProfileData(int accountType) {
        if (AppUtils.isNetworkAvailable(getActivity())) {
            if (!mFragmentProfileBinding.srvProfile.isRefreshing())
                mFragmentProfileBinding.pbMain.setVisibility(View.VISIBLE);
            if (accountType == AppConstants.MY_PROFILE) {
                mProfileViewModel.hitApiToFetchMyProfileData();
            } else
                mProfileViewModel.hitApiToFetchOtherUserProfile(userId);
        } else {
            mFragmentProfileBinding.abToolbar.setVisibility(View.VISIBLE);
            mFragmentProfileBinding.layout.vpProfile.setVisibility(View.VISIBLE);
            mFragmentProfileBinding.tvName.setText(DataManager.getInstance().getName());
            mFragmentProfileBinding.tvUserName.setText(DataManager.getInstance().getUserName());
            Toast.makeText(mActivity, getActivity().getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }


    private void manageBlockView(String message) {
        if (accountType == 1) {
            if (message.contains("unblock")) {
                mFragmentProfileBinding.tvUnblock.setVisibility(View.INVISIBLE);
                mFragmentProfileBinding.btnFollow.setVisibility(View.VISIBLE);
                mFragmentProfileBinding.btnMessage.setVisibility(View.VISIBLE);
                mFragmentProfileBinding.btnEditProfile.setVisibility(View.INVISIBLE);
            } else {
                mFragmentProfileBinding.tvUnblock.setVisibility(View.VISIBLE);
                mFragmentProfileBinding.btnFollow.setVisibility(View.INVISIBLE);
                mFragmentProfileBinding.btnMessage.setVisibility(View.INVISIBLE);
                mFragmentProfileBinding.btnEditProfile.setVisibility(View.INVISIBLE);
            }
        }
    }

    public void blockUser(String blockAction) {
        mProfileModel.hitApiToBlockUnblockUser(mProfileViewModel.getBlockUnblockBeanRichMediatorLiveData(), userId, blockAction);
    }

    private void getFollowerOrFollowingList(int page, int offset) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(mProfileViewModel.getUserId()));
        if (accountType == AppConstants.MY_PROFILE)
            hashMap.put(AppConstants.NetworkConstants.PARAM_OTHER_USER_ID, String.valueOf(mProfileViewModel.getUserId()));
        else
            hashMap.put(AppConstants.NetworkConstants.PARAM_OTHER_USER_ID, userId);

        hashMap.put(AppConstants.NetworkConstants.PAGE, String.valueOf(page));
        hashMap.put(AppConstants.NetworkConstants.LIMIT, String.valueOf(offset));
        if (AppUtils.isNetworkAvailable(getActivity())) {
            mProfileViewModel.getFollowersListing(hashMap);
            mProfileViewModel.getFollowingListing(hashMap);
        } else
            Toast.makeText(mActivity, getActivity().getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();

    }

    public void showOtherUserProfileSettingsDialog() {
        PopupMenu popup = new PopupMenu(mActivity, mFragmentProfileBinding.ivMore);
        popup.getMenuInflater().inflate(R.menu.settings_menu, popup.getMenu());
        if (accountType == AppConstants.OTHER_USER_PROFILE)
            popup.getMenu().getItem(0).setVisible(false);
        else
            popup.getMenu().getItem(0).setVisible(true);

        if (mFragmentProfileBinding.tvUnblock.getVisibility() == View.VISIBLE) {
            popup.getMenu().getItem(2).setTitle("Unblock User");
        } else {
            popup.getMenu().getItem(2).setTitle("Block User");
        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.block_user:
                        if (mFragmentProfileBinding.tvUnblock.getVisibility() == View.VISIBLE) {
                            blockUserFromProfile("unblock");
                        } else {
                            blockUserFromProfile("block");
                        }


                        break;
                    case R.id.tv_report_abuse:
                        DialogFragment reportIssueDialog = new ReportIssueDialogFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString(AppConstants.ClassConstants.OTHER_USER_ID, userId);
                        bundle.putInt(AppConstants.ClassConstants.IS_FOR, AppConstants.ClassConstants.FOR_REPORT_USER);
                        reportIssueDialog.setArguments(bundle);
                        reportIssueDialog.show(((HomeActivity) mActivity).getSupportFragmentManager(), ReportIssueDialogFragment.class.getCanonicalName());
                        break;
                }
                return true;
            }
        });

        popup.show();

    }


    private void blockUserFromProfile(String status) {
        Fragment fragment = currentVisibleFragment();
        if (fragment instanceof ProfileFragment) {
            ((ProfileFragment) fragment).blockUser(status);
        }
    }

    private Fragment currentVisibleFragment() {
        return getActivity().getSupportFragmentManager()
                .findFragmentById(R.id.fl_container);

    }


    @Override
    public void onBackPressed() {
        if (getActivity() != null) {
            switch (mIsFrom) {
                case AppConstants.ClassConstants.FROM_COMMENTS:
                case AppConstants.ClassConstants.IS_FROM_NOTIFICATION_FRAGMENT:
                case AppConstants.ClassConstants.IS_FROM_USER_LISTING_FRAGMENT:
                case AppConstants.ClassConstants.IS_FROM_CHAT_ACTIVITY:
                case AppConstants.ClassConstants.IS_FROM_FEED_FRAGMENT:
                case AppConstants.ClassConstants.IS_FROM_FEED_RV:
                case AppConstants.ClassConstants.IS_FROM_NOTIFICATION_ACTIVITY:
                case AppConstants.ClassConstants.IS_FROM_FEED_PREVIEW_FRAGMENT:
                case AppConstants.ClassConstants.IS_FROM_INBOX_ACTIVITY:
                    getActivity().getSupportFragmentManager().popBackStack();
                    ((BaseActivity) getActivity()).setOnBackPressedListener(null);
                    break;
                case AppConstants.ClassConstants.IS_FROM_USER_LISTING_MY_PROFILE_FRAGMENT:
                    ((HomeActivity) getActivity()).selectBottomNav(false, true, false, false, false);
                    getActivity().getSupportFragmentManager().popBackStack();
                    ((BaseActivity) getActivity()).setOnBackPressedListener(null);
                    break;
                case AppConstants.ClassConstants.IS_FROM_HOME_ACTIVITY:
                    if (accountType == AppConstants.MY_PROFILE) {
                        ((BaseActivity) getActivity()).addFragment(R.id.fl_container, new HomeFragment(), HomeFragment.class.getCanonicalName());
                        ((BaseActivity) getActivity()).setOnBackPressedListener(null);
                        ((HomeActivity) getActivity()).selectBottomNav(true, false, false, false, true);
                    } else {
                        getActivity().getSupportFragmentManager().popBackStack();
                        ((BaseActivity) getActivity()).setOnBackPressedListener(null);
                    }
                    break;
            }
        }
    }

    public void updateProfile() {
        hitApiToFetchProfileData(accountType);
        mPage = 1;
        getFollowerOrFollowingList(mPage, mOffset);

    }
}
