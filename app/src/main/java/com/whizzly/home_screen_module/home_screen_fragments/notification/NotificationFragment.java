package com.whizzly.home_screen_module.home_screen_fragments.notification;


import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.whizzly.R;
import com.whizzly.chat.InboxListActivity;
import com.whizzly.databinding.FragmentNotificationBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.home_screen_module.home_screen_fragments.notification.following.FollowingNotificationsFragment;
import com.whizzly.home_screen_module.home_screen_fragments.notification.personal.PersonalNotificationsFragment;
import com.whizzly.interfaces.OnBackPressedListener;
import com.whizzly.utils.GenericFragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment implements OnBackPressedListener {

    private FragmentNotificationBinding mFragmentNotificationBinding;
    private NotificationFragmentViewModel mNotificationFragmentViewModel;
    private ArrayList<Fragment> mList;
    private GenericFragmentPagerAdapter pagerAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFragmentNotificationBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification, container, false);
        mNotificationFragmentViewModel = ViewModelProviders.of(this).get(NotificationFragmentViewModel.class);
        mFragmentNotificationBinding.setViewModel(mNotificationFragmentViewModel);
        openNotificationFragment();
        ((HomeActivity)getActivity()).setOnBackPressedListener(this);
        setViews();
        return mFragmentNotificationBinding.getRoot();
    }

    private void openNotificationFragment() {
        PersonalNotificationsFragment personalNotificationsFragment = new PersonalNotificationsFragment();
            getChildFragmentManager().beginTransaction().add(R.id.fl_container, personalNotificationsFragment).commit();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //setUpViewPager();
    }

    private void setUpViewPager() {
        mList = new ArrayList<>();

        FollowingNotificationsFragment followingNotificationsFragment = new FollowingNotificationsFragment();
        PersonalNotificationsFragment personalNotificationsFragment = new PersonalNotificationsFragment();

        mList.add(personalNotificationsFragment);
        mList.add(followingNotificationsFragment);

        List<String> titles = new ArrayList<>();

        titles.add(getString(R.string.s_you).toUpperCase());
        titles.add(getString(R.string.txt_following).toUpperCase());

        pagerAdapter = new GenericFragmentPagerAdapter(getChildFragmentManager(), mList, titles);

        mFragmentNotificationBinding.vpNotification.setAdapter(pagerAdapter);
        mFragmentNotificationBinding.tlNotification.setupWithViewPager(mFragmentNotificationBinding.vpNotification);

    }

    private void setViews() {
        mFragmentNotificationBinding.toolbar.ivNotificationOptions.setVisibility(View.VISIBLE);
        mFragmentNotificationBinding.toolbar.tvToolbarText.setText(getString(R.string.txt_notifications));
        mFragmentNotificationBinding.toolbar.ivNotificationOptions.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), InboxListActivity.class);
            startActivity(intent);
        });
    }


    @Override
    public void onBackPressed() {

    }
}
