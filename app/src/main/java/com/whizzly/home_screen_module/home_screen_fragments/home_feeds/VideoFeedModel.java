package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;

import com.whizzly.BR;

public class VideoFeedModel extends BaseObservable {
    private ObservableBoolean videoFeedLiked = new ObservableBoolean(false);

    @Bindable
    public boolean getVideoFeedLiked() {
        return videoFeedLiked.get();
    }

    public void setVideoFeedLiked(boolean videoFeedLiked) {
        this.videoFeedLiked.set(videoFeedLiked);
        notifyPropertyChanged(BR.videoFeedLiked);
    }

}
