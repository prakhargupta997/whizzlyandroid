package com.whizzly.home_screen_module.home_screen_fragments.profile.settings.suppot_center;


import android.content.Context;
import android.widget.Toast;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableField;

import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.submit_profile.SubmitProfileBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import java.util.HashMap;


public class SupportCenterModel extends BaseObservable {
    public ObservableField<String> message = new ObservableField<>();
    public ObservableField<String> email = new ObservableField<>();
    public ObservableField<String> subject = new ObservableField<>();
    private OnClickSendListener mClickSendListener;
    private Context context;

    public SupportCenterModel(Context context) {
        this.context = context;
    }

    public boolean isFieldsValid() {
        return (isEmailValid() == null && isSubjectdValid() == null && isMessageValid() == null);

    }

    private String isEmailValid() {
        if (getEmail() == null) {
            return context.getString(R.string.txt_error_empty_email);
        } else {
            if (getEmail().isEmpty()) {
                return context.getString(R.string.txt_error_empty_email);
            }
            if (getEmail().isEmpty()) {
                return context.getString(R.string.txt_error_empty_email);
            } else if (!AppUtils.isEmailValid(getEmail())) {
                return context.getString(R.string.txt_email_invalid);
            } else {
                return null;
            }
        }
    }

    @Bindable
    private String getEmail() {
        return email.get();
    }

    public void setEmail(String email) {
        this.email.set(email);
        notifyPropertyChanged(BR.email);

    }

    private String isSubjectdValid() {
        if (getSubject() == null) {
            return context.getString(R.string.txt_error_empty_subject);
        } else {
            if (getSubject().isEmpty()) {
                return context.getString(R.string.txt_error_empty_subject);
            } else {
                return null;
            }
        }
    }

    @Bindable
    private String getSubject() {
        return subject.get();

    }

    public void setSubject(String subject) {
        this.subject.set(subject);
        notifyPropertyChanged(BR.subject);

    }

    private String isMessageValid() {
        if (getMessage() == null) {
            return context.getString(R.string.txt_error_empty_message);
        } else {
            if (getMessage().isEmpty()) {
                return context.getString(R.string.txt_error_empty_message);
            } else {
                return null;
            }
        }
    }

    @Bindable
    private String getMessage() {
        return message.get();
    }

    public void setMessage(String message) {
        this.message.set(message);
        notifyPropertyChanged(BR.message);

    }

    public void showErrorMessages() {
        if (isEmailValid() != null) {
            Toast.makeText(context, isEmailValid(), Toast.LENGTH_SHORT).show();
        } else if (isSubjectdValid() != null) {
            Toast.makeText(context, isSubjectdValid(), Toast.LENGTH_SHORT).show();
        } else if (isMessageValid() != null) {
            Toast.makeText(context, isMessageValid(), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    public void hitSupportCenterApi(RichMediatorLiveData<SubmitProfileBean> supportCenterLiveData) {

        if (AppUtils.isNetworkAvailable(context)) {

            DataManager.getInstance().hitSupportCenterApi(getSupportData()).enqueue(new NetworkCallback<SubmitProfileBean>() {
                @Override
                public void onSuccess(SubmitProfileBean submitProfileBean) {
                    DataManager.getInstance().setEmail(getEmail());
                    supportCenterLiveData.setValue(submitProfileBean);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    supportCenterLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    supportCenterLiveData.setError(t);

                }
            });

        }


    }

    private HashMap<String, String> getSupportData() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(AppConstants.NetworkConstants.PARAM_USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        hashMap.put(AppConstants.USER_EMAIL, email.get().trim());
        hashMap.put(AppConstants.NetworkConstants.SUBJECT, subject.get());
        hashMap.put(AppConstants.NetworkConstants.MESSAGE, message.get());

        return hashMap;
    }

}
