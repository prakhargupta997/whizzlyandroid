package com.whizzly.home_screen_module.home_screen_fragments.profile.report;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.report.ReportResponse;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class ReportIssueViewModel extends ViewModel {
    private OnClickSendListener onClickSendListener;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private RichMediatorLiveData<ReportResponse> mReportLiveData;

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureResponseObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (mReportLiveData == null) {
            mReportLiveData = new RichMediatorLiveData<ReportResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
    }

    public RichMediatorLiveData<ReportResponse> getReportLiveData() {
        return mReportLiveData;
    }

    public void onReportViewClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_REPORT_VIEW_CLICKED);
    }

    public void onCancelViewClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CANCEL_VIEW_CLICKED);
    }

    public void setClickListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void reportUser(int reportType, String reportedUserId, int reasonToReport, String description) {
        DataManager.getInstance().hitReportApi(getData(reportType, reportedUserId, reasonToReport, description)).enqueue(new NetworkCallback<ReportResponse>() {
            @Override
            public void onSuccess(ReportResponse reportResponse) {
                mReportLiveData.setValue(reportResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mReportLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mReportLiveData.setError(t);
            }
        });
    }

    private HashMap<String, Object> getData(int reportType, String reportedUserId, int reasonToReport, String description) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        hashMap.put(AppConstants.NetworkConstants.REPORTED_ID, reportedUserId);
        hashMap.put(AppConstants.NetworkConstants.REPORT_TYPE, reportType);
        hashMap.put(AppConstants.NetworkConstants.REASON_TO_REPORT, reasonToReport);
        if (reasonToReport == 4)
            hashMap.put(AppConstants.NetworkConstants.DESCRIPTION, description);
        return hashMap;
    }
}
