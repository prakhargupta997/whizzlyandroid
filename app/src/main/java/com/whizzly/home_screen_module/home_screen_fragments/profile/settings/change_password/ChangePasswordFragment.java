package com.whizzly.home_screen_module.home_screen_fragments.profile.settings.change_password;

import android.app.Activity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.FragmentChangePasswordBinding;
import com.whizzly.home_screen_module.home_screen_activities.InnerFragmentsHostActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import java.util.Objects;

public class ChangePasswordFragment extends Fragment implements OnClickSendListener {

    private Activity mActivity;
    private FragmentChangePasswordBinding mFragmentChangePasswordBinding;
    private ChangePasswordViewModel mChangePasswordViewModel;
    private ChangePasswordModel mChangePasswordModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mActivity = getActivity();
        mFragmentChangePasswordBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_change_password, container, false);
        mChangePasswordViewModel = ViewModelProviders.of(this).get(ChangePasswordViewModel.class);
        mFragmentChangePasswordBinding.setViewModel(mChangePasswordViewModel);
        mChangePasswordModel = new ChangePasswordModel(mActivity);



        mFragmentChangePasswordBinding.setModel(mChangePasswordModel);
        mChangePasswordViewModel.setOnClickSendListener(this);
        mChangePasswordViewModel.setModel(mChangePasswordModel);
        mChangePasswordViewModel.setGenericListeners(((InnerFragmentsHostActivity) mActivity).getErrorObserver(), ((InnerFragmentsHostActivity) mActivity).getFailureResponseObserver());
        mChangePasswordViewModel.getChangePasswordMediatorLiveData().observe(this, submitProfileBean -> {
            mFragmentChangePasswordBinding.bar.setVisibility(View.GONE);
            if (submitProfileBean != null) {
                if (submitProfileBean.getCode().equalsIgnoreCase("200")) {
                    Toast.makeText(mActivity, R.string.s_pass_changed_successfully, Toast.LENGTH_LONG).show();
                    ((InnerFragmentsHostActivity) mActivity).removeFragmentFromLeft(R.id.fl_container_inner);
                } else if (submitProfileBean.getCode().equals("301") || submitProfileBean.getCode().equals("302")) {
                    Toast.makeText(mActivity, submitProfileBean.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) Objects.requireNonNull(mActivity)).autoLogOut();
                } else {
                    Toast.makeText(mActivity, submitProfileBean.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

        });

        mFragmentChangePasswordBinding.toolbar.ivBack.setVisibility(View.VISIBLE);
        mFragmentChangePasswordBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.onBackPressed();
            }
        });

        if(DataManager.getInstance().isNotchDevice()){
            mFragmentChangePasswordBinding.toolbar.view.setVisibility(View.VISIBLE);
        }else{
            mFragmentChangePasswordBinding.toolbar.view.setVisibility(View.GONE);
        }
        mFragmentChangePasswordBinding.toolbar.tvToolbarText.setText(getResources().getString(R.string.txt_change_password));
        return mFragmentChangePasswordBinding.getRoot();

    }

    @Override
    public void onClickSend(int code) {

        switch (code) {
            case AppConstants.ClassConstants.CHANGE_PASSWORD_SAVE_CLICKED:
                if (AppUtils.isNetworkAvailable(getActivity())) {
                    if (mChangePasswordModel.isFormFieldsValid()) {
                        mFragmentChangePasswordBinding.bar.setVisibility(View.VISIBLE);
                        mChangePasswordModel.hitApiToChangePassword(mChangePasswordViewModel.getChangePasswordMediatorLiveData());
                    } else {
                        mChangePasswordModel.showErrorMessages();
                    }
                } else {
                    Toast.makeText(mActivity, getActivity().getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
                }
                break;
            case AppConstants.ClassConstants.SUGGESTED_BACK_PRESSED:
                mActivity.onBackPressed();
                break;
        }
    }


}
