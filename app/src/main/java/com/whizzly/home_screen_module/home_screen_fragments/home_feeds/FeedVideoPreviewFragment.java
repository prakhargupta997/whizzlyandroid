package com.whizzly.home_screen_module.home_screen_fragments.home_feeds;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.fragment.SuggestedVideoFragment;
import com.whizzly.databinding.ActivityFeedVideoPreviewBinding;
import com.whizzly.home_feed_comments.CameraCommentActivity;
import com.whizzly.home_feed_comments.FeedsCommentPreviewActivity;
import com.whizzly.home_screen_module.home_screen_fragments.profile.ProfileFragment;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.interfaces.OnSendCommentDataListener;
import com.whizzly.models.privacy_status.PrivacyStatusResponseBean;
import com.whizzly.models.profileVideo.VideoInfo;
import com.whizzly.models.video_list_response.Result;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;
import com.whizzly.utils.EndlessRecyclerOnScrollListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import static com.google.android.exoplayer2.ui.AspectRatioFrameLayout.RESIZE_MODE_FIT;
import static com.google.android.exoplayer2.ui.AspectRatioFrameLayout.RESIZE_MODE_ZOOM;

public class FeedVideoPreviewFragment extends Fragment implements OnClickSendListener, OnSendCommentDataListener {

    private ActivityFeedVideoPreviewBinding mActivityFeedVideoPreviewBinding;
    private FeedVideoPreviewViewModel mFeedVideoPreviewViewModel;
    private FeedVideoPreviewModel mFeedVideoPreviewModel;
    private int isFrom;
    private VideoInfo mVideoInfo;
    private Result resultInfo;
    private ExoPlayer mExoPlayer;
    private com.whizzly.models.feeds_response.Result result;
    private ArrayList<com.whizzly.models.comment_list.Result> commentResultsList;
    private int commentPage = 1;
    private CommentListAdapter commentListAdapter;
    private int accountType;
    private Observer<PrivacyStatusResponseBean> mSavedVideoObserver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getData();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mActivityFeedVideoPreviewBinding = DataBindingUtil.inflate(inflater, R.layout.activity_feed_video_preview, null, false);
        mFeedVideoPreviewViewModel = ViewModelProviders.of(this).get(FeedVideoPreviewViewModel.class);
        mActivityFeedVideoPreviewBinding.setViewModel(mFeedVideoPreviewViewModel);
        mFeedVideoPreviewModel = new FeedVideoPreviewModel();
        mActivityFeedVideoPreviewBinding.setModel(mFeedVideoPreviewModel);
        mFeedVideoPreviewViewModel.setOnClickSendListener(this);
        mFeedVideoPreviewViewModel.setGenericListeners(((BaseActivity) getActivity()).getFailureResponseObserver(), ((BaseActivity) getActivity()).getErrorObserver());
        mFeedVideoPreviewViewModel.getmVideoDetailsRichMediatorLiveData().observe(this, videoDetailsResponse -> {
            if (videoDetailsResponse != null) {
                if (videoDetailsResponse.getCode() == 200) {
                    setDataToViews(videoDetailsResponse.getResult());
                    result = videoDetailsResponse.getResult();
                    setExoplayer(result.getUrl());
                    if (commentPage > 0)
                        mFeedVideoPreviewViewModel.hitCommentListingApi(videoCommentHashMap());
                } else if (videoDetailsResponse.getCode() == 301 || videoDetailsResponse.getCode() == 302) {
                    Toast.makeText(getActivity(), videoDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) Objects.requireNonNull(getActivity())).autoLogOut();
                }
            }
        });


        mFeedVideoPreviewViewModel.getLikeDislikeResponseRichMediatorLiveData().observe(this, likeDislikeResponse -> {

        });

        mFeedVideoPreviewViewModel.getCommentListingBeanRichMediatorLiveData().observe(this, commentListingBean -> {
            if (commentListingBean != null) {
                if (commentListingBean.getCode() == 200) {
                    getActivity().runOnUiThread(() -> {
                        commentResultsList.clear();
                        for (int i = 0; i < commentListingBean.getResult().size(); i++) {
                            if (commentListingBean.getResult().get(i).getUserStatus() == 0)
                                commentResultsList.addAll(commentListingBean.getResult());
                        }
                        commentListAdapter.notifyDataSetChanged();
                        commentPage = commentListingBean.getNextCount();
                    });

                } else if (commentListingBean.getCode() == 301 || commentListingBean.getCode() == 302) {
                    Toast.makeText(getActivity(), commentListingBean.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) getActivity()).autoLogOut();
                }
            }
        });
        commentResultsList = new ArrayList<>();

//        hitVideoDetailsApi();
        mActivityFeedVideoPreviewBinding.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mExoPlayer != null)
                    mExoPlayer.setPlayWhenReady(false);
                releasePlayer();
                if (result != null) {
                    if (isFrom == AppConstants.ClassConstants.IS_FROM_PROFILE) {
                        ProfileFragment profileFragment = new ProfileFragment();
                        Bundle bundle = new Bundle();
                        bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.OTHER_USER_PROFILE);
                        bundle.putString(AppConstants.ClassConstants.USER_ID, String.valueOf(result.getUserId()));
                        bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_FEED_PREVIEW_FRAGMENT);
                        profileFragment.setArguments(bundle);
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, profileFragment).addToBackStack(ProfileFragment.class.getCanonicalName()).commit();
                    } else if (isFrom == AppConstants.ClassConstants.IS_FROM_SUGGESTED) {
                        SuggestedVideoFragment suggestedVideoFragment = new SuggestedVideoFragment();
                        Bundle musicBundle = new Bundle();
                        musicBundle.putString(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_FEED);
                        musicBundle.putInt(AppConstants.ClassConstants.MUSIC_ID, (result.getMusicId()));
                        musicBundle.putString(AppConstants.ClassConstants.MUSIC_COLLAB_COUNT, String.valueOf((result.getMusicReuseCount())));
                        musicBundle.putString(AppConstants.ClassConstants.MUSIC_NAME, (result.getMusicTitle()));
                        musicBundle.putString(AppConstants.ClassConstants.MUSIC_COMPOSER, (result.getMusicDescription()));
                        musicBundle.putString(AppConstants.ClassConstants.MUSIC_THUMBNAIL, (result.getMusicThumbnail()));
                        musicBundle.putString(AppConstants.ClassConstants.MUSIC_LINK, (result.getMusicUrl()));
                        suggestedVideoFragment.setArguments(musicBundle);
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, suggestedVideoFragment).commit();
                    }
                }
            }
        });
        return mActivityFeedVideoPreviewBinding.getRoot();
    }

    private HashMap<String, String> videoCommentHashMap() {
        HashMap<String, String> videoCommentQuery = new HashMap<>();
        videoCommentQuery.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        videoCommentQuery.put(AppConstants.NetworkConstants.VIDEO_ID, String.valueOf(result.getId()));
        videoCommentQuery.put(AppConstants.NetworkConstants.VIDEO_TYPE, result.getType());
        videoCommentQuery.put(AppConstants.NetworkConstants.PAGE, String.valueOf(commentPage));
        return videoCommentQuery;
    }

    private void setCommentsRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mActivityFeedVideoPreviewBinding.rvCommentsList.setLayoutManager(linearLayoutManager);
        commentListAdapter = new CommentListAdapter(getActivity(), mActivityFeedVideoPreviewBinding.ivAddComment.getHeight(), commentResultsList, this, result.getType());
        mActivityFeedVideoPreviewBinding.rvCommentsList.setAdapter(commentListAdapter);
        mActivityFeedVideoPreviewBinding.rvCommentsList.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                if (commentPage > 1)
                    mFeedVideoPreviewViewModel.hitCommentListingApi(videoCommentHashMap());
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void setExoplayer(String url) {
        TrackSelection.Factory adaptiveTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(new DefaultBandwidthMeter());

        mExoPlayer = ExoPlayerFactory.newSimpleInstance(getActivity(),
                new DefaultRenderersFactory(getActivity()),
                new DefaultTrackSelector(adaptiveTrackSelectionFactory),
                new DefaultLoadControl());

        mExoPlayer.setPlayWhenReady(true);
        mExoPlayer.setRepeatMode(Player.REPEAT_MODE_ONE);
        MediaSource mediaSource = buildMediaSource(Uri.parse(url));
        mExoPlayer.addListener(new Player.DefaultEventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                super.onPlayerStateChanged(playWhenReady, playbackState);
                switch (playbackState) {
                    case Player.STATE_BUFFERING:
                        mActivityFeedVideoPreviewBinding.flProgress.setVisibility(View.VISIBLE);
                        break;
                    case Player.STATE_READY:
                        mActivityFeedVideoPreviewBinding.ivVideoThumbnail.setVisibility(View.GONE);
                        mActivityFeedVideoPreviewBinding.flProgress.setVisibility(View.GONE);
                        break;
                }
            }
        });
        mExoPlayer.prepare(mediaSource, true, false);
        mActivityFeedVideoPreviewBinding.pvVideo.setPlayer(mExoPlayer);
    }

    private MediaSource buildMediaSource(Uri uri) {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getActivity(),
                Util.getUserAgent(getActivity(), getString(R.string.app_name)), bandwidthMeter);
        return new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri);

    }

    @SuppressLint("CheckResult")
    private void setDataToViews(com.whizzly.models.feeds_response.Result mFeedsData) {
        mActivityFeedVideoPreviewBinding.tvUserName.setText(mFeedsData.getUserName());
        mActivityFeedVideoPreviewBinding.tvLikes.setText(String.valueOf(mFeedsData.getTotalLikes()));
        mActivityFeedVideoPreviewBinding.tvReuseCount.setText(String.valueOf(mFeedsData.getTotalReuse()));
//        mActivityFeedVideoPreviewBinding.tvVideoDescription.setText(mFeedsData.getVideoDescription());

        String hashtag = mFeedsData.getVideoHastags();
        String[] hashtags = hashtag.split(",");
        StringBuilder hashtagBuillder = new StringBuilder();
        hashtagBuillder.append(mFeedsData.getVideoDescription() + " ");
        if (!TextUtils.isEmpty(hashtag))
            for (int i = 0; i < hashtags.length; i++) {
                if (i == 0) {
                    hashtagBuillder.append("#" + hashtags[i]);
                } else {
                    hashtagBuillder.append(" #" + hashtags[i]);
                }
            }


        int userCount = 0;
        if (mFeedsData.getCount() > 1) {
            hashtagBuillder.append(" with ");
            if (!TextUtils.isEmpty(mFeedsData.getUsersName())) {
                String userName = mFeedsData.getUsersName();
                String[] userNames = userName.split(",");
               /* for (int i = 0; i < userNames.length; i++) {
                    if (Arrays.asList(userNames).contains(mFeedsData.getUserName())) {
                        userCount++;
                    }
                }

                if (userCount > 0) {
                    for (int i = 0; i < userNames.length; i++) {
                        if (mFeedsData.getUserName().equalsIgnoreCase(userNames[i])) {
                            userNames = removeTheElement(userNames, i);
                            break;
                        }
                    }
                }*/

                for (int i = 0; i < userNames.length; i++) {
                    if (i == 0) {
                        hashtagBuillder.append("@" + userNames[i]);
                    } else {
                        hashtagBuillder.append(" @" + userNames[i]);
                    }
                }

                mActivityFeedVideoPreviewBinding.tvCollabWith.setVisibility(View.GONE);

            } else {
                mActivityFeedVideoPreviewBinding.tvCollabWith.setVisibility(View.GONE);
            }
        }

        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(hashtagBuillder);
        if (!TextUtils.isEmpty(mFeedsData.getVideoHastags()) && !TextUtils.isEmpty(mFeedsData.getVideoDescription())) {
            if (mFeedsData.getCount() > 1) {
                spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), 0, mFeedsData.getVideoDescription().length(), 0);
                spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), mFeedsData.getVideoDescription().length(), mFeedsData.getVideoDescription().length() + mFeedsData.getVideoHastags().length() + hashtags.length + 1, 0);
                spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), mFeedsData.getVideoDescription().length() + mFeedsData.getVideoHastags().length() + hashtags.length + 1, mFeedsData.getVideoDescription().length() + mFeedsData.getVideoHastags().length() + hashtags.length + 1 + " with ".length(), 0);
                spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), mFeedsData.getVideoDescription().length() + mFeedsData.getVideoHastags().length() + hashtags.length + " with ".length(), hashtagBuillder.length(), 0);
            } else {
                spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), 0, mFeedsData.getVideoDescription().length(), 0);
                spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), mFeedsData.getVideoDescription().length(), mFeedsData.getVideoDescription().length() + mFeedsData.getVideoHastags().length() + hashtags.length + 1, 0);
                spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), mFeedsData.getVideoDescription().length() + mFeedsData.getVideoHastags().length() + hashtags.length + 1, mFeedsData.getVideoDescription().length() + mFeedsData.getVideoHastags().length() + hashtags.length + 1, 0);
                spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), mFeedsData.getVideoDescription().length() + mFeedsData.getVideoHastags().length() + hashtags.length, hashtagBuillder.length(), 0);
            }
        } else {
            if (!TextUtils.isEmpty(mFeedsData.getVideoHastags()) && TextUtils.isEmpty(mFeedsData.getVideoDescription())) {
                if (mFeedsData.getCount() > 1) {
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), 0, mFeedsData.getVideoHastags().length() + hashtags.length + 1, 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), mFeedsData.getVideoHastags().length() + hashtags.length + 1, mFeedsData.getVideoHastags().length() + hashtags.length + 1 + " with ".length(), 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), mFeedsData.getVideoHastags().length() + hashtags.length + " with ".length(), hashtagBuillder.length(), 0);
                } else {
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), 0, mFeedsData.getVideoHastags().length() + hashtags.length + 1, 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), mFeedsData.getVideoHastags().length() + hashtags.length + 1, mFeedsData.getVideoHastags().length() + hashtags.length + 1, 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), mFeedsData.getVideoHastags().length() + hashtags.length, hashtagBuillder.length(), 0);
                }
            }

            if (TextUtils.isEmpty(mFeedsData.getVideoHastags()) && !TextUtils.isEmpty(mFeedsData.getVideoDescription())) {
                if (mFeedsData.getCount() > 1) {
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), 0, mFeedsData.getVideoDescription().length(), 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), mFeedsData.getVideoDescription().length(), mFeedsData.getVideoDescription().length() + 1, 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), mFeedsData.getVideoDescription().length() + 1, mFeedsData.getVideoDescription().length() + 1 + " with ".length(), 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), mFeedsData.getVideoDescription().length() + " with ".length(), hashtagBuillder.length(), 0);
                } else {
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), 0, mFeedsData.getVideoDescription().length(), 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), mFeedsData.getVideoDescription().length(), mFeedsData.getVideoDescription().length() + 1, 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), mFeedsData.getVideoDescription().length() + 1, mFeedsData.getVideoDescription().length() + 1, 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), mFeedsData.getVideoDescription().length(), hashtagBuillder.length(), 0);
                }
            }

            if (TextUtils.isEmpty(mFeedsData.getVideoHastags()) && TextUtils.isEmpty(mFeedsData.getVideoDescription())) {
                if (mFeedsData.getCount() > 1) {
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), 0, " with ".length(), 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), " with ".length(), hashtagBuillder.length(), 0);
                }
            }
        }


        if (mFeedsData.getVideoHastags() != null)
            mActivityFeedVideoPreviewBinding.tvHashtags.setText(spannableStringBuilder);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.error(R.drawable.ic_feed_user_pic_placeholder);
        requestOptions.placeholder(R.drawable.ic_feed_user_pic_placeholder);
        requestOptions.circleCrop();
        Glide.with(this).load(mFeedsData.getProfileImage()).apply(requestOptions).into(mActivityFeedVideoPreviewBinding.ivUserProfileIcon);
        if (mFeedsData.getIsLiked() == 0) {
            mFeedVideoPreviewModel.setVideoFeedLiked(false);
        } else {
            mFeedVideoPreviewModel.setVideoFeedLiked(true);
        }
        if (null == mFeedsData.getMusicId()) {
            mActivityFeedVideoPreviewBinding.ivMusicIcon.setVisibility(View.GONE);
            mActivityFeedVideoPreviewBinding.tvMusicName.setVisibility(View.GONE);
        } else {
            mActivityFeedVideoPreviewBinding.ivMusicIcon.setVisibility(View.VISIBLE);
            mActivityFeedVideoPreviewBinding.tvMusicName.setVisibility(View.VISIBLE);
            mActivityFeedVideoPreviewBinding.tvMusicName.setText(String.format("%s - %s", mFeedsData.getMusicTitle(), mFeedsData.getMusicDescription()));
            mActivityFeedVideoPreviewBinding.ivMusicIcon.startAnimation(rotateAnimation());
        }
        mActivityFeedVideoPreviewBinding.ivVideoThumbnail.setVisibility(View.VISIBLE);
        Glide.with(this).load(mFeedsData.getThumbnailUrl()).into(mActivityFeedVideoPreviewBinding.ivVideoThumbnail);

        if (mFeedsData.getType().equals("video")) {
            mActivityFeedVideoPreviewBinding.ivReuseCount.setVisibility(View.GONE);
            mActivityFeedVideoPreviewBinding.tvReuseCount.setVisibility(View.GONE);
        } else {
            mActivityFeedVideoPreviewBinding.ivReuseCount.setVisibility(View.VISIBLE);
            mActivityFeedVideoPreviewBinding.tvReuseCount.setVisibility(View.VISIBLE);
        }

        mActivityFeedVideoPreviewBinding.tvUserFollowerCount.setText(String.valueOf(mFeedsData.getFollowerCount()));
        setCommentsRecyclerView();
        if (mFeedsData.getCount() == 2) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            mActivityFeedVideoPreviewBinding.ivVideoThumbnail.setLayoutParams(layoutParams);
            mActivityFeedVideoPreviewBinding.ivVideoThumbnail.setScaleType(ImageView.ScaleType.FIT_START);
            mActivityFeedVideoPreviewBinding.pvVideo.setResizeMode(RESIZE_MODE_FIT);
        } else {
            mActivityFeedVideoPreviewBinding.ivVideoThumbnail.setScaleType(ImageView.ScaleType.FIT_XY);
            mActivityFeedVideoPreviewBinding.pvVideo.setResizeMode(RESIZE_MODE_ZOOM);
        }

        if (accountType == AppConstants.MY_PROFILE) {
            mActivityFeedVideoPreviewBinding.ivFeedOptions.setVisibility(View.VISIBLE);
        } else if (accountType == AppConstants.OTHER_USER_PROFILE) {
            mActivityFeedVideoPreviewBinding.ivFeedOptions.setVisibility(View.INVISIBLE);
        }

    }

    private RotateAnimation rotateAnimation() {
        RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(3000);
        rotate.setRepeatCount(Animation.INFINITE);
        rotate.setInterpolator(new LinearInterpolator());
        return rotate;
    }

    public void getData() {
        Bundle intent = getArguments();
        isFrom = intent.getInt(AppConstants.ClassConstants.IS_FROM, 0);
        if (isFrom == AppConstants.ClassConstants.IS_FROM_PROFILE) {
            mVideoInfo = (VideoInfo) intent.getSerializable(AppConstants.ClassConstants.VIDEO_DETAILS);
        } else if (isFrom == AppConstants.ClassConstants.IS_FROM_SUGGESTED) {
            resultInfo = intent.getParcelable(AppConstants.ClassConstants.VIDEO_DETAILS);
        }
        accountType = intent.getInt(AppConstants.ClassConstants.ACCOUNT_TYPE);
    }


    // Function to remove the element
    public String[] removeTheElement(String[] arr,
                                     int index) {

        // If the array is empty
        // or the index is not in array range
        // return the original array
        if (arr == null
                || index < 0
                || index >= arr.length) {

            return arr;
        }

        // Create another array of size one less
        String[] anotherArray = new String[arr.length - 1];

        // Copy the elements except the index
        // from original array to the other array
        for (int i = 0, k = 0; i < arr.length; i++) {

            // if the index is
            // the removal element index
            if (i == index) {
                continue;
            }

            // if the index is not
            // the removal element index
            anotherArray[k++] = arr[i];
        }

        // return the resultant array
        return anotherArray;
    }

    @Override
    public void onResume() {
        super.onResume();
        hitVideoDetailsApi();


    }

    private void hitVideoDetailsApi() {
        mActivityFeedVideoPreviewBinding.flProgress.setVisibility(View.VISIBLE);
        HashMap<String, String> videoQuery = new HashMap<>();
        if (isFrom == AppConstants.ClassConstants.IS_FROM_PROFILE) {
            videoQuery.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
            videoQuery.put(AppConstants.NetworkConstants.VIDEO_ID, String.valueOf(mVideoInfo.getId()));
            videoQuery.put(AppConstants.NetworkConstants.VIDEO_TYPE, mVideoInfo.getType());
            mFeedVideoPreviewViewModel.hitVideoDetailsApi(videoQuery);
        } else if (isFrom == AppConstants.ClassConstants.IS_FROM_SUGGESTED) {
            videoQuery.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
            videoQuery.put(AppConstants.NetworkConstants.VIDEO_ID, String.valueOf(resultInfo.getCollabId()));
            videoQuery.put(AppConstants.NetworkConstants.VIDEO_TYPE, "collab");
            mFeedVideoPreviewViewModel.hitVideoDetailsApi(videoQuery);
        }
    }

    private void releasePlayer() {
        if (mExoPlayer != null) {
            mExoPlayer.setPlayWhenReady(false);
            mExoPlayer.stop(true);
            mExoPlayer.release();
            mExoPlayer = null;
        }
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_VIDEO_COMMENT_CLICKED:
                releasePlayer();
                Intent commentIntent = new Intent(getActivity(), CameraCommentActivity.class);
                commentIntent.putExtra(AppConstants.ClassConstants.FEEDS_DATA, result);
                startActivity(commentIntent);
                break;
            case AppConstants.ClassConstants.ON_FEED_OPTION_CLICKED:
                if (result != null) {
                    releasePlayer();
                    BottomSheetShareFragment sheetShareFragment = new BottomSheetShareFragment();
                    Bundle shareSheetBundle = new Bundle();
                    shareSheetBundle.putString(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.FROM_PROFILE);
                    shareSheetBundle.putParcelable(AppConstants.ClassConstants.VIDEO_DETAILS, result);
                    sheetShareFragment.setArguments(shareSheetBundle);
                    sheetShareFragment.show(getChildFragmentManager(), BottomSheetShareFragment.class.getCanonicalName());
                }
                break;
            case AppConstants.ClassConstants.ON_VIDEO_REUSE_COUNT_CLICKED:
                if (result.getTotalReuse() > 0) {
                    BottomSheetPeopleReuseFragment bottomSheetPeopleReuseFragment = new BottomSheetPeopleReuseFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.ClassConstants.VIDEO_TYPE, result.getType());
                    bundle.putString(AppConstants.ClassConstants.FEED_VIDEO_ID, String.valueOf(result.getId()));
                    bundle.putString(AppConstants.ClassConstants.TOTAL_REUSE, String.valueOf(result.getTotalReuse()));
                    bottomSheetPeopleReuseFragment.setArguments(bundle);
                    bottomSheetPeopleReuseFragment.show(getChildFragmentManager(), BottomSheetPeopleReuseFragment.class.getCanonicalName());
                }
                break;
            case AppConstants.ClassConstants.ON_VIDEO_REUSE_CLICKED:
                if (AppUtils.isNetworkAvailable(getActivity())) {
                    Intent intent = new Intent(getActivity(), VideoPreviewActivity.class);
                    intent.putExtra(AppConstants.ClassConstants.FRAGMENT_IS_FROM, AppConstants.ClassConstants.IS_FROM_FEEDS);
                    intent.putExtra(AppConstants.ClassConstants.FEEDS_DATA, result);
                    startActivity(intent);
                } else {
                    Toast.makeText(getActivity(), getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
                }
                break;
            case AppConstants.ClassConstants.ON_VIDEO_LIKE_COUNT_CLICKED:
                if (result.getTotalLikes() > 0) {
                    BottomSheetPeopleLikeListFragment bottomSheetPeopleLikeListFragment = new BottomSheetPeopleLikeListFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.ClassConstants.VIDEO_TYPE, result.getType());
                    bundle.putString(AppConstants.ClassConstants.FEED_VIDEO_ID, String.valueOf(result.getId()));
                    bundle.putString(AppConstants.ClassConstants.TOTAL_LIKES, String.valueOf(result.getTotalLikes()));
                    bottomSheetPeopleLikeListFragment.setArguments(bundle);
                    bottomSheetPeopleLikeListFragment.show(getChildFragmentManager(), BottomSheetPeopleLikeListFragment.class.getCanonicalName());
                }
                break;
            case AppConstants.ClassConstants.ON_VIDEO_LIKE_CLICKED:
                if (result.getIsLiked() == 0) {
                    mFeedVideoPreviewModel.setVideoFeedLiked(true);
                    likeDislikeVideo(1);
                    result.setIsLiked(1);
                    mActivityFeedVideoPreviewBinding.tvLikes.setText(String.valueOf(result.getTotalLikes() + 1));
                } else {
                    mFeedVideoPreviewModel.setVideoFeedLiked(false);
                    likeDislikeVideo(0);
                    result.setIsLiked(0);
                    mActivityFeedVideoPreviewBinding.tvLikes.setText(String.valueOf(result.getTotalLikes()));
                }
                break;
            case AppConstants.ClassConstants.ON_USER_PROFILE_CLICKED:
                releasePlayer();
                if (!String.valueOf(DataManager.getInstance().getUserId()).equals(result.getUserId())) {
                    ProfileFragment profileFragment = new ProfileFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.OTHER_USER_PROFILE);
                    bundle.putString(AppConstants.ClassConstants.USER_ID, result.getUserId());
                    bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_FEED_PREVIEW_FRAGMENT);
                    profileFragment.setArguments(bundle);
                    Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, profileFragment).addToBackStack(ProfileFragment.class.getCanonicalName()).commit();
                } else {
                    ProfileFragment profileFragment = new ProfileFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.MY_PROFILE);
                    bundle.putString(AppConstants.ClassConstants.USER_ID, result.getUserId());
                    bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_FEED_PREVIEW_FRAGMENT);
                    profileFragment.setArguments(bundle);
                    Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, profileFragment).addToBackStack(ProfileFragment.class.getCanonicalName()).commit();
                }
                break;
            case AppConstants.ClassConstants.ON_MUSIC_ICON_CLICKED:
                releasePlayer();
                SuggestedVideoFragment suggestedVideoFragment = new SuggestedVideoFragment();
                Bundle musicBundle = new Bundle();
                musicBundle.putString(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_FEED);
                musicBundle.putInt(AppConstants.ClassConstants.MUSIC_ID, (result.getMusicId()));
                musicBundle.putString(AppConstants.ClassConstants.MUSIC_COLLAB_COUNT, String.valueOf((result.getMusicReuseCount())));
                musicBundle.putString(AppConstants.ClassConstants.MUSIC_NAME, (result.getMusicTitle()));
                musicBundle.putString(AppConstants.ClassConstants.MUSIC_COMPOSER, (result.getMusicDescription()));
                musicBundle.putString(AppConstants.ClassConstants.MUSIC_THUMBNAIL, (result.getMusicThumbnail()));
                musicBundle.putString(AppConstants.ClassConstants.MUSIC_LINK, (result.getMusicUrl()));
                suggestedVideoFragment.setArguments(musicBundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, suggestedVideoFragment).commit();
                break;


        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }

    }


    @Override
    public void onCommentClicked(com.whizzly.models.comment_list.Result result, int position) {
        releasePlayer();
        Intent intent = new Intent(getActivity(), FeedsCommentPreviewActivity.class);
        intent.putExtra(AppConstants.ClassConstants.ON_VIDEO_FEED_COMMENT_CLICKED, commentResultsList);
        intent.putExtra(AppConstants.ClassConstants.COMMENT_POSITION, position);
        startActivity(intent);
    }

    private void likeDislikeVideo(int status) {
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put(AppConstants.NetworkConstants.LIKE_DISLIKE_STATUS, String.valueOf(status));
        queryMap.put(AppConstants.NetworkConstants.VIDEO_TYPE, result.getType());
        queryMap.put(AppConstants.NetworkConstants.VIDEO_ID, String.valueOf(result.getId()));
        queryMap.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        mFeedVideoPreviewViewModel.hitLikeDislikeApi(queryMap);
    }

    public Observer<PrivacyStatusResponseBean> geSavedFeedsObserver() {
        return mSavedVideoObserver;
    }

    private void observeSavedVideoLiveData() {
        mSavedVideoObserver = privacyStatusResponseBean -> {
            if (privacyStatusResponseBean != null) {
                if (privacyStatusResponseBean.getCode() == 200) {
                    if (privacyStatusResponseBean.getResult().getIsLocked() != null && privacyStatusResponseBean.getResult().getVideoId() != null) {
                        result.setSavedVideoPrivacy(privacyStatusResponseBean.getResult().getIsLocked());
                    }
                    Toast.makeText(getActivity(), privacyStatusResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (privacyStatusResponseBean.getCode() == 301 || privacyStatusResponseBean.getCode() == 302) {
                    Toast.makeText(getActivity(), privacyStatusResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) getActivity()).autoLogOut();
                } else
                    Toast.makeText(getActivity(), getActivity().getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();

            }

        };
    }
}
