package com.whizzly.home_screen_module.home_screen_models;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;

import com.whizzly.BR;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.post.VideoPostResponse;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.DataManager;

import java.util.HashMap;


public class HomeScreenModel extends BaseObservable {
    private ObservableBoolean isHomeSelected = new ObservableBoolean(true);
    private ObservableBoolean isSearchSelected = new ObservableBoolean(false);
    private ObservableBoolean isNotificationSelected = new ObservableBoolean(false);
    private ObservableBoolean isProfileSelected = new ObservableBoolean(false);
    private ObservableBoolean isOtherSelected = new ObservableBoolean(false);


    @Bindable
    public boolean getIsHomeSelected() {
        return isHomeSelected.get();
    }

    public void setIsHomeSelected(boolean isHomeSelected) {
        this.isHomeSelected.set(isHomeSelected);
        notifyPropertyChanged(BR.isHomeSelected);
    }

    @Bindable
    public boolean getIsSearchSelected() {
        return isSearchSelected.get();
    }

    public void setIsSearchSelected(boolean isSearchSelected) {
        this.isSearchSelected.set(isSearchSelected);
        notifyPropertyChanged(BR.isSearchSelected);
    }

    @Bindable
    public boolean getIsNotificationSelected() {
        return isNotificationSelected.get();
    }

    public void setIsNotificationSelected(boolean isNotificationSelected) {
        this.isNotificationSelected.set(isNotificationSelected);
        notifyPropertyChanged(BR.isNotificationSelected);
    }

    @Bindable
    public boolean getIsProfileSelected() {
        return isProfileSelected.get();
    }

    public void setIsProfileSelected(boolean isProfileSelected) {
        this.isProfileSelected.set(isProfileSelected);
        notifyPropertyChanged(BR.isProfileSelected);
    }

    public void hitPostArgumentApi(HashMap<String, String> videoDetails, RichMediatorLiveData<VideoPostResponse> videoPostResponseRichMediatorLiveData){
        DataManager.getInstance().postArgument(videoDetails).enqueue(new NetworkCallback<VideoPostResponse>() {
            @Override
            public void onSuccess(VideoPostResponse responseBody) {
                videoPostResponseRichMediatorLiveData.setValue(responseBody);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                videoPostResponseRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                videoPostResponseRichMediatorLiveData.setError(t);
            }
        });
    }


    @Bindable
    public boolean getIsOtherSelected() {
        return isOtherSelected.get();
    }

    public void setIsOtherSelected(boolean isOtherSelected) {
        this.isOtherSelected.set(isOtherSelected);
        notifyPropertyChanged(BR.isOtherSelected);
    }
}
