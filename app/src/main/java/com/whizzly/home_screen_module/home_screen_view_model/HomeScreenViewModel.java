package com.whizzly.home_screen_module.home_screen_view_model;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.post.VideoPostResponse;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import okhttp3.RequestBody;

public class HomeScreenViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;
    private RichMediatorLiveData<VideoPostResponse> videoPostResponseRichMediatorLiveData;
    private RichMediatorLiveData<VideoPostResponse> argumentPostResponseRichMediatorLiveData;

    public RichMediatorLiveData<VideoPostResponse> getArgumentPostResponseRichMediatorLiveData() {
        return argumentPostResponseRichMediatorLiveData;
    }

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void onHomeClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_HOME_CLICKED);
    }

    public void onSearchClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_SEARCH_CLICKED);
    }

    public void onAddCollabClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_ADD_COLLAB_CLICKED);
    }

    public void onNotificationClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_NOTIFICATION_CLICKED);
    }

    public void onProfileClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_PROFILE_CLICKED);
    }

    public RichMediatorLiveData<VideoPostResponse> getVideoPostResponseRichMediatorLiveData() {
        return videoPostResponseRichMediatorLiveData;
    }

    public void setGenericListeners(Observer<FailureResponse> failureResponseObserver, Observer<Throwable> error) {
        if (videoPostResponseRichMediatorLiveData == null) {
            videoPostResponseRichMediatorLiveData = new RichMediatorLiveData<VideoPostResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return error;
                }
            };

        }

        if (argumentPostResponseRichMediatorLiveData == null) {
            argumentPostResponseRichMediatorLiveData = new RichMediatorLiveData<VideoPostResponse>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return error;
                }
            };
        }

    }

    public void hitPostApi(RequestBody argumentData, String userId) {
        DataManager.getInstance().postVideo(argumentData, userId).enqueue(new NetworkCallback<VideoPostResponse>() {
            @Override
            public void onSuccess(VideoPostResponse responseBody) {
                videoPostResponseRichMediatorLiveData.setValue(responseBody);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                videoPostResponseRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                videoPostResponseRichMediatorLiveData.setError(t);
            }
        });
    }
}
