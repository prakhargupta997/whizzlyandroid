package com.whizzly.chat;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.chat.model.Message;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.change_password.ChangePasswordModel;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class ChatActivityViewModel extends ViewModel {
    private RichMediatorLiveData<ChangePasswordModel> mChatNotificationLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private OnClickSendListener onClickSendListener;

    public void setGenericeListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureResponseObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (mChatNotificationLiveData == null) {
            mChatNotificationLiveData = new RichMediatorLiveData<ChangePasswordModel>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
    }

    public void onSmileyClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CHAT_SMILEY_CLICKED);
    }

    public void onSendClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CHAT_SEND_CLICKED);
    }

    public void onGIFClicked(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CHAT_GIF_CLICKED);
    }

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public RichMediatorLiveData<ChangePasswordModel> getChatNotificationLiveData() {
        return mChatNotificationLiveData;
    }

    public void hitChatNotificationApi(Message message, String roomId) {
        DataManager.getInstance().hitChatMessageNotificationApi(getChatData(message, roomId)).enqueue(new NetworkCallback<ChangePasswordModel>() {
            @Override
            public void onSuccess(ChangePasswordModel notificationResponse) {
                mChatNotificationLiveData.setValue(notificationResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mChatNotificationLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mChatNotificationLiveData.setError(t);
            }
        });
    }

    public HashMap<String, String> getChatData(Message message, String roomId) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(AppConstants.ClassConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        hashMap.put(AppConstants.NetworkConstants.SENDER_USER_ID, message.idSender);
        hashMap.put(AppConstants.NetworkConstants.RECIEVER_USER_ID, message.idReceiver);
        hashMap.put(AppConstants.NetworkConstants.CHAT_USER_NAME, DataManager.getInstance().getUserName());
        hashMap.put(AppConstants.NetworkConstants.MESSAGE, message.text);
        hashMap.put(AppConstants.NetworkConstants.TIME, String.valueOf(message.timestamp));
        hashMap.put(AppConstants.NetworkConstants.ROOM_ID, roomId);
        hashMap.put(AppConstants.NetworkConstants.TEXT_TYPE, message.messageType);
        hashMap.put(AppConstants.NetworkConstants.PROFILE_PIC, DataManager.getInstance().getProfilePic());
        return hashMap;
    }

    public void setRoomId(String roomId) {
        DataManager.getInstance().setRoomId(roomId);
    }
}
