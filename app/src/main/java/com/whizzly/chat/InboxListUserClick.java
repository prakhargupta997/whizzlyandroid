package com.whizzly.chat;

import com.whizzly.chat.model.ChatBean;

public interface InboxListUserClick {
    public void onChatUserClicked(ChatBean chatBeanModel);
}
