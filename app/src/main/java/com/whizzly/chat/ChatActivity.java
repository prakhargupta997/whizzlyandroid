package com.whizzly.chat;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.giphy.sdk.core.models.Media;
import com.giphy.sdk.core.models.enums.RatingType;
import com.giphy.sdk.core.models.enums.RenditionType;
import com.giphy.sdk.ui.GPHContentType;
import com.giphy.sdk.ui.GPHSettings;
import com.giphy.sdk.ui.GiphyCoreUI;
import com.giphy.sdk.ui.themes.DarkTheme;
import com.giphy.sdk.ui.views.GiphyDialogFragment;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vanniktech.emoji.EmojiPopup;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.chat.model.Message;
import com.whizzly.chat.model.Room;
import com.whizzly.chat.model.TimeBean;
import com.whizzly.databinding.ActivityChatBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.home_screen_module.home_screen_fragments.profile.ProfileFragment;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.change_password.ChangePasswordModel;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static com.giphy.sdk.ui.themes.GridType.waterfall;

public class ChatActivity extends BaseActivity implements OnClickSendListener {

    private ActivityChatBinding mActivityChatBinding;
    private ChatActivityViewModel mChatActivityViewModel;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mDatabaseReference;
    private String roomId, userId, userName;
    private EmojiPopup emojiPopup;
    private ArrayList<Object> mMessageArrayList;
    private ChatRecyclerViewAdapter mChatRecyclerViewAdapter;
    private ChildEventListener mChatChildEventListener;
    private boolean isBlocked;
    private int mIsFrom;
    private ChildEventListener mRoomStatusListener;

    public static long getDaysBetweenDates(long start, long end) {
        long numberOfDays = 0;
        numberOfDays = getUnitBetweenDates(start, end, TimeUnit.DAYS);
        return numberOfDays;
    }

    private static long getUnitBetweenDates(long start, long end, TimeUnit unit) {
        long timeDiff = start - end;
        return unit.convert(timeDiff, TimeUnit.MILLISECONDS);
    }

    private void getData() {
        roomId = getIntent().getStringExtra(AppConstants.ClassConstants.ROOM_ID);
        userId = getIntent().getStringExtra(AppConstants.ClassConstants.USER_ID);
        userName = getIntent().getStringExtra(AppConstants.ClassConstants.USER_NAME);
        mIsFrom = getIntent().getIntExtra(AppConstants.ClassConstants.IS_FROM, 1);
        isBlocked = getIntent().getBooleanExtra(AppConstants.ClassConstants.IS_BLOCKED, false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityChatBinding = DataBindingUtil.setContentView(this, R.layout.activity_chat);
        mChatActivityViewModel = ViewModelProviders.of(this).get(ChatActivityViewModel.class);
        mActivityChatBinding.setViewModel(mChatActivityViewModel);
        mChatActivityViewModel.setGenericeListeners(getErrorObserver(), getFailureResponseObserver());
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        GiphyCoreUI.INSTANCE.configure(this, "JmOliKtWWO5aK46QW13O6YJM26pIoaIN", true);
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mChatActivityViewModel.setOnClickSendListener(this);
        mMessageArrayList = new ArrayList<>();
        AppUtils.setStatusBarGradiant(this);
        observeLiveData();
        getData();
        setViews();
        mChatActivityViewModel.setRoomId(roomId);
        clickOnToolbar();
        setRecyclerView();
        setUpEmojiPopup();
        getMessageData();
        if (DataManager.getInstance().isNotchDevice()) {
            mActivityChatBinding.toolbar.view.setVisibility(View.GONE);
        } else {
            mActivityChatBinding.toolbar.view.setVisibility(View.GONE);
        }
    }

    private void setGIFView() {
        GPHSettings settings = new GPHSettings(waterfall, DarkTheme.INSTANCE, new GPHContentType[]{GPHContentType.gif}, true, true, false, RatingType.pg13, RenditionType.fixedWidth, RenditionType.looping);
        GiphyDialogFragment giphyDialogFragment = GiphyDialogFragment.Companion.newInstance(settings);
        giphyDialogFragment.setGifSelectionListener(new GiphyDialogFragment.GifSelectionListener() {
            @Override
            public void onGifSelected(@NotNull Media media) {
                sendGifInMessage(media.getImages().getOriginal().getGifUrl());
                giphyDialogFragment.dismiss();
            }

            @Override
            public void onDismissed() {

            }
        });
        giphyDialogFragment.show(getSupportFragmentManager(), "giphy_dialog");
    }

    private void sendGifInMessage(String gifUrl) {
        if (gifUrl != null) {
            String id = mDatabaseReference.child(AppConstants.FirebaseConstants.MESSAGES).child(roomId).push().getKey();
            Message message = new Message();
            message.setIdReceiver(userId);
            message.setFirebaseKey(id);
            message.setIdSender(String.valueOf(DataManager.getInstance().getUserId()));
            message.setMessageType(AppConstants.FirebaseConstants.MESSAGE_TYPE_GIF);
            message.setText(gifUrl);
            message.setTimestamp(System.currentTimeMillis());

            if (!AppUtils.isNetworkAvailable(this)) {
                AppConstants.ClassConstants.mChatPushQueue.add(mChatActivityViewModel.getChatData(message, roomId));
            }

            mDatabaseReference.child(AppConstants.FirebaseConstants.MESSAGES).child(roomId).child(id).setValue(message).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    if (AppUtils.isNetworkAvailable(ChatActivity.this))
                        mChatActivityViewModel.hitChatNotificationApi(message, roomId);
                }
            });

        }
    }

    private void clickOnToolbar() {
        mActivityChatBinding.toolbar.tvToolbarText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileFragment profileFragment = new ProfileFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_CHAT_ACTIVITY);
                bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.OTHER_USER_PROFILE);
                bundle.putString(AppConstants.ClassConstants.USER_ID, String.valueOf(userId));
                profileFragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, profileFragment).addToBackStack(ProfileFragment.class.getCanonicalName()).commit();
            }
        });
    }

    private void observeLiveData() {
        mChatActivityViewModel.getChatNotificationLiveData().observe(this, new Observer<ChangePasswordModel>() {
            @Override
            public void onChanged(@Nullable ChangePasswordModel response) {
                if (response != null) {
                    if (response.getCode().equals("301") || response.getCode().equals("302")) {
                        Toast.makeText(ChatActivity.this, response.getMessage(), Toast.LENGTH_SHORT).show();
                        ChatActivity.this.autoLogOut();

                    }
                }
            }
        });
    }

    private void setRecyclerView() {
        mActivityChatBinding.rvMessages.setLayoutManager(new LinearLayoutManager(this));
        mChatRecyclerViewAdapter = new ChatRecyclerViewAdapter(mMessageArrayList, this);
        mActivityChatBinding.rvMessages.setAdapter(mChatRecyclerViewAdapter);
    }

    private void setViews() {
        mActivityChatBinding.toolbar.tvToolbarText.setText(userName);
        mActivityChatBinding.toolbar.ivBack.setVisibility(View.VISIBLE);
        mActivityChatBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (mIsFrom) {
                    case AppConstants.ClassConstants.FROM_NOTIFICATION:
                    case AppConstants.ClassConstants.FROM_SPLASH:
                        Intent homeIntent = new Intent(ChatActivity.this, HomeActivity.class);
                        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(homeIntent);
                        break;
                    case AppConstants.ClassConstants.IS_FROM_PROFILE:
                    case AppConstants.ClassConstants.FROM_INBOX:
                        ChatActivity.this.onBackPressed();
                        break;
                }
            }
        });
        mActivityChatBinding.etMessage.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }
        });

        mActivityChatBinding.rvMessages.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (bottom < oldBottom) {
                    mActivityChatBinding.rvMessages.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mActivityChatBinding.rvMessages.scrollToPosition(mMessageArrayList.size() - 1);
                        }
                    }, 100);
                }
            }
        });

        if (isBlocked) {
            mActivityChatBinding.tvBlocked.setVisibility(View.VISIBLE);
            mActivityChatBinding.llMessage.setVisibility(View.INVISIBLE);
        } else {
            mActivityChatBinding.tvBlocked.setVisibility(View.GONE);
            mActivityChatBinding.llMessage.setVisibility(View.VISIBLE);
        }

    }

    private void getMessageData() {
        mMessageArrayList.clear();
        mChatChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Message message = dataSnapshot.getValue(Message.class);
                if (mMessageArrayList.size() > 0 && (mMessageArrayList.get(mMessageArrayList.size() - 1) instanceof Message)) {
                    if (getDaysBetweenDates(((Message) mMessageArrayList.get(mMessageArrayList.size() - 1)).getTimestamp(), message.getTimestamp()) > 0) {
                        String time = DateUtils.getRelativeTimeSpanString(ChatActivity.this, message.getTimestamp()).toString();
                        TimeBean timeBean = new TimeBean();
                        timeBean.setTime(time);
                        mMessageArrayList.add(timeBean);
                    }
                } else {
                    if (mMessageArrayList.size() == 0) {
                        String time = DateUtils.getRelativeTimeSpanString(ChatActivity.this, message.getTimestamp()).toString();
                        TimeBean timeBean = new TimeBean();
                        timeBean.setTime(time);
                        mMessageArrayList.add(timeBean);
                    }
                }
                mMessageArrayList.add(message);
                mChatRecyclerViewAdapter.notifyDataSetChanged();
                mActivityChatBinding.tvNoMessage.setVisibility(View.GONE);
                mActivityChatBinding.rvMessages.scrollToPosition(mMessageArrayList.size() - 1);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mDatabaseReference.child(AppConstants.FirebaseConstants.MESSAGES).child(roomId).addChildEventListener(mChatChildEventListener);

        mRoomStatusListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Room changedRoomStatus = dataSnapshot.getValue(Room.class);
                if (changedRoomStatus.getRoomId().equals(roomId)) {
                    if (changedRoomStatus.getRoomStatusAdmin().equals("1")) {
                        mActivityChatBinding.tvBlocked.setVisibility(View.VISIBLE);
                        mActivityChatBinding.tvBlocked.setText(getResources().getString(R.string.txt_user_no_longer_exists));
                        mActivityChatBinding.llMessage.setVisibility(View.GONE);
                    } else if (changedRoomStatus.getRoomStatusAdmin().equals("2")) {
                        mActivityChatBinding.tvBlocked.setVisibility(View.VISIBLE);
                        mActivityChatBinding.tvBlocked.setText(getResources().getString(R.string.txt_user_deactivated));
                        mActivityChatBinding.llMessage.setVisibility(View.GONE);
                    } else if (changedRoomStatus.getRoomStatusUser().equals("1") &&
                            !TextUtils.isEmpty(changedRoomStatus.getBlockedUserId()) &&
                            TextUtils.equals(changedRoomStatus.getBlockedUserId(), String.valueOf(DataManager.getInstance().getUserId()))) {
                        mActivityChatBinding.tvBlocked.setVisibility(View.VISIBLE);
                        mActivityChatBinding.llMessage.setVisibility(View.GONE);
                    } else if (changedRoomStatus.getRoomStatusUser().equals("1") &&
                            !TextUtils.isEmpty(changedRoomStatus.getBlockedUserId()) &&
                            !TextUtils.equals(changedRoomStatus.getBlockedUserId(), String.valueOf(DataManager.getInstance().getUserId()))) {
                        mActivityChatBinding.tvBlocked.setVisibility(View.VISIBLE);
                        mActivityChatBinding.tvBlocked.setText(getResources().getString(R.string.txt_blocked_by) + " " + userName);
                        mActivityChatBinding.llMessage.setVisibility(View.GONE);
                    }else if (changedRoomStatus.getRoomStatusUser().equals("1") &&
                            !TextUtils.isEmpty(changedRoomStatus.getBlockedUserId()) &&
                            TextUtils.equals(changedRoomStatus.getBlockedUserId(), String.valueOf(DataManager.getInstance().getUserId()))) {
                        mActivityChatBinding.tvBlocked.setVisibility(View.VISIBLE);
                        mActivityChatBinding.tvBlocked.setText(getResources().getText(R.string.txt_please_unblock_the_user_to_send_message));
                        mActivityChatBinding.llMessage.setVisibility(View.GONE);
                    } else if(changedRoomStatus.getRoomStatusUser().equals("0") && changedRoomStatus.getRoomStatusAdmin().equals("0")){
                        mActivityChatBinding.tvBlocked.setVisibility(View.GONE);
                        mActivityChatBinding.llMessage.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mDatabaseReference.child(AppConstants.FirebaseConstants.ROOM).addChildEventListener(mRoomStatusListener);

        mRoomStatusValueListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Room changedRoomStatus = dataSnapshot.getValue(Room.class);
                if (changedRoomStatus != null && changedRoomStatus.getRoomId().equals(roomId)) {
                    if (changedRoomStatus.getRoomStatusAdmin().equals("1")) {
                        mActivityChatBinding.tvBlocked.setVisibility(View.VISIBLE);
                        mActivityChatBinding.tvBlocked.setText(getResources().getString(R.string.txt_user_no_longer_exists));
                        mActivityChatBinding.llMessage.setVisibility(View.GONE);
                    } else if (changedRoomStatus.getRoomStatusAdmin().equals("2")) {
                        mActivityChatBinding.tvBlocked.setVisibility(View.VISIBLE);
                        mActivityChatBinding.tvBlocked.setText(getResources().getString(R.string.txt_user_deactivated));
                        mActivityChatBinding.llMessage.setVisibility(View.GONE);
                    } else if (changedRoomStatus.getRoomStatusUser().equals("1") &&
                            !TextUtils.isEmpty(changedRoomStatus.getBlockedUserId()) &&
                            TextUtils.equals(changedRoomStatus.getBlockedUserId(), String.valueOf(DataManager.getInstance().getUserId()))) {
                        mActivityChatBinding.tvBlocked.setVisibility(View.VISIBLE);
                        mActivityChatBinding.llMessage.setVisibility(View.GONE);
                    } else if (changedRoomStatus.getRoomStatusUser().equals("1") &&
                            !TextUtils.isEmpty(changedRoomStatus.getBlockedUserId()) &&
                            !TextUtils.equals(changedRoomStatus.getBlockedUserId(), String.valueOf(DataManager.getInstance().getUserId()))) {
                        mActivityChatBinding.tvBlocked.setVisibility(View.VISIBLE);
                        mActivityChatBinding.tvBlocked.setText(getResources().getString(R.string.txt_blocked_by) + " " + userName);
                        mActivityChatBinding.llMessage.setVisibility(View.GONE);
                    } else if (changedRoomStatus.getRoomStatusUser().equals("1") &&
                            !TextUtils.isEmpty(changedRoomStatus.getBlockedUserId()) &&
                            TextUtils.equals(changedRoomStatus.getBlockedUserId(), String.valueOf(DataManager.getInstance().getUserId()))) {
                        mActivityChatBinding.tvBlocked.setVisibility(View.VISIBLE);
                        mActivityChatBinding.tvBlocked.setText(getResources().getString(R.string.txt_blocked_by) + " " + userName);
                        mActivityChatBinding.llMessage.setVisibility(View.GONE);
                    } else if (changedRoomStatus.getRoomStatusUser().equals("0") && changedRoomStatus.getRoomStatusAdmin().equals("0")) {
                        mActivityChatBinding.tvBlocked.setVisibility(View.GONE);
                        mActivityChatBinding.llMessage.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        mDatabaseReference.child(AppConstants.FirebaseConstants.ROOM).child(roomId).addListenerForSingleValueEvent(mRoomStatusValueListener);
    }

    private ValueEventListener mRoomStatusValueListener;

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_CHAT_SEND_CLICKED:
                if (mActivityChatBinding.etMessage.getText().toString().trim().length() > 0)
                    sendMessageToFirebase(roomId);
                mActivityChatBinding.etMessage.setText("");
                mActivityChatBinding.etMessage.requestFocus();
                break;
            case AppConstants.ClassConstants.ON_CHAT_SMILEY_CLICKED:
                emojiPopup.toggle();
                mActivityChatBinding.etMessage.requestFocus();
                break;
            case AppConstants.ClassConstants.ON_CHAT_GIF_CLICKED:
                setGIFView();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (emojiPopup != null && emojiPopup.isShowing()) {
            emojiPopup.dismiss();
        } else if (mIsFrom == AppConstants.ClassConstants.FROM_SPLASH) {
            Intent intent = new Intent(this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            super.onBackPressed();
        }

    }


    @Override
    protected void onStop() {
        if (emojiPopup != null) {
            emojiPopup.dismiss();
        }
        mDatabaseReference.child(AppConstants.FirebaseConstants.ROOM).removeEventListener(mRoomStatusListener);
        mDatabaseReference.child(AppConstants.FirebaseConstants.MESSAGES).child(roomId).removeEventListener(mChatChildEventListener);
        mDatabaseReference.child(AppConstants.FirebaseConstants.ROOM).child(roomId).removeEventListener(mRoomStatusValueListener);
        super.onStop();
    }

    private void setUpEmojiPopup() {
        emojiPopup = EmojiPopup.Builder.fromRootView(mActivityChatBinding.getRoot())
                .setOnEmojiBackspaceClickListener(v -> {

                })
                .setOnEmojiClickListener((emoji, imageView) -> {

                })
                .setOnEmojiPopupShownListener(() -> mActivityChatBinding.btnSmiley.setImageResource(R.drawable.ic_keyboard))
                .setOnSoftKeyboardOpenListener(keyBoardHeight -> {
                })
                .setOnEmojiPopupDismissListener(() -> mActivityChatBinding.btnSmiley.setImageResource(R.drawable.ic_messages_conversations_emoji))
                .setOnSoftKeyboardCloseListener(() -> {
                }).build(mActivityChatBinding.etMessage);
    }


    private void sendMessageToFirebase(String roomId) {
        if (mActivityChatBinding.etMessage.getText().toString().length() > 0) {
            String id = mDatabaseReference.child(AppConstants.FirebaseConstants.MESSAGES).child(roomId).push().getKey();
            Message message = new Message();
            message.setIdReceiver(userId);
            message.setFirebaseKey(id);
            message.setIdSender(String.valueOf(DataManager.getInstance().getUserId()));
            message.setMessageType(AppConstants.FirebaseConstants.MESSAGE_TYPE);
            message.setText(mActivityChatBinding.etMessage.getText().toString().trim());
            message.setTimestamp(System.currentTimeMillis());

            if (!AppUtils.isNetworkAvailable(this)) {
                AppConstants.ClassConstants.mChatPushQueue.add(mChatActivityViewModel.getChatData(message, roomId));
            }

            mDatabaseReference.child(AppConstants.FirebaseConstants.MESSAGES).child(roomId).child(id).setValue(message).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    if (AppUtils.isNetworkAvailable(ChatActivity.this))
                        mChatActivityViewModel.hitChatNotificationApi(message, roomId);
                }
            });

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mChatActivityViewModel.setRoomId("");
    }
}
