package com.whizzly.chat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.chat.model.ChatBean;
import com.whizzly.chat.model.ListnersBean;
import com.whizzly.chat.model.Message;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.ActivityInboxListBinding;
import com.whizzly.home_screen_module.home_screen_fragments.profile.ProfileFragment;
import com.whizzly.models.blockedUsers.Result;
import com.whizzly.models.firebase_model.FirebaseUserDetailModel;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.ArrayList;
import java.util.HashMap;

public class InboxListActivity extends BaseActivity implements InboxListUserClick, OnClickAdapterListener {

    private ActivityInboxListBinding mActivityInboxListBinding;
    private InboxListViewModel mInboxListViewModel;
    private InboxListRecyclerViewAdapter mInboxListRecyclerViewAdapter;
    private ArrayList<ChatBean> inboxArrayList;
    private DatabaseReference mDatabaseReference;
    private ValueEventListener mInboxValueEventListener;
    private ChatBean mChatBean;
    private ValueEventListener mUserDataEventListener;
    private ArrayList<ListnersBean> mListenersList;
    private ValueEventListener mLastMessageEventListener;
    private ArrayList<Result> blockedUsers = new ArrayList<>();
    private HashMap<String, ChatBean> mChatUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mActivityInboxListBinding = DataBindingUtil.setContentView(this, R.layout.activity_inbox_list);
        mInboxListViewModel = ViewModelProviders.of(this).get(InboxListViewModel.class);
        mActivityInboxListBinding.setViewModel(mInboxListViewModel);
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mListenersList = new ArrayList<>();
        mChatUsers = new HashMap<>();
        mInboxListViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver());
        setViews();
        setRecyclerView();
        observerBlockedUsersResponse();
        if(DataManager.getInstance().isNotchDevice()){
            mActivityInboxListBinding.toolbar.view.setVisibility(View.VISIBLE);
        }else{
            mActivityInboxListBinding.toolbar.view.setVisibility(View.GONE);
        }
    }

    private void observerBlockedUsersResponse() {

        mInboxListViewModel.getBlockedUsersRichMediatorLiveData().observe(this, blockedUsersResponseBean -> {
            if (blockedUsersResponseBean != null && blockedUsersResponseBean.getCode() == 200) {
                if (blockedUsersResponseBean.getResult() != null && blockedUsersResponseBean.getResult().size() > 0)
                    blockedUsers.clear();
                blockedUsers.addAll(blockedUsersResponseBean.getResult());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mInboxListViewModel.hitApiToFetchBlockedList(mInboxListViewModel.getBlockedUsersRichMediatorLiveData());
        getInboxList();
    }

    private void setRecyclerView() {
        inboxArrayList = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mActivityInboxListBinding.rvChatList.setLayoutManager(linearLayoutManager);
        mActivityInboxListBinding.rvChatList.addItemDecoration(new DividerItemDecoration(mActivityInboxListBinding.rvChatList.getContext(), linearLayoutManager.getOrientation()));
        mInboxListRecyclerViewAdapter = new InboxListRecyclerViewAdapter(inboxArrayList, this, this, this);
        mActivityInboxListBinding.rvChatList.setAdapter(mInboxListRecyclerViewAdapter);
    }

    private void setViews() {
        mActivityInboxListBinding.toolbar.tvToolbarText.setText(getString(R.string.txt_messages));
        mActivityInboxListBinding.toolbar.ivBack.setVisibility(View.VISIBLE);
        mActivityInboxListBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getInboxList() {
        mInboxValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot childSnapshot :
                        dataSnapshot.getChildren()) {
                    mChatBean = new ChatBean();
                    String userId = childSnapshot.getKey();
                    String roomId = (String) childSnapshot.getValue();
                    mChatBean.setRoomId(roomId);
                    mChatBean.setUserId(userId);
                    mChatUsers.put(userId, mChatBean);
                    DatabaseReference databaseReference = mDatabaseReference.child(AppConstants.FirebaseConstants.MESSAGES).child(roomId);
                    Query lastQuery = databaseReference.orderByKey().limitToLast(1);
                    lastQuery.addListenerForSingleValueEvent(mLastMessageEventListener);
                    ListnersBean listnersBean = new ListnersBean();
                    listnersBean.setDatabaseReference(databaseReference);
                    listnersBean.setValueEventListener(mLastMessageEventListener);
                    mListenersList.add(listnersBean);
                    mDatabaseReference.child(AppConstants.FirebaseConstants.USERS).child(userId).addListenerForSingleValueEvent(mUserDataEventListener);
                    ListnersBean listnersBean1 = new ListnersBean();
                    listnersBean1.setDatabaseReference(mDatabaseReference.child(AppConstants.FirebaseConstants.USERS).child(userId));
                    listnersBean1.setValueEventListener(mUserDataEventListener);
                    mListenersList.add(listnersBean1);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        mUserDataEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                FirebaseUserDetailModel firebaseUserDetailModel = dataSnapshot.getValue(FirebaseUserDetailModel.class);
                if (firebaseUserDetailModel != null) {
                    String key = dataSnapshot.getKey();
                    if (mChatUsers.containsKey(key)) {
                        ChatBean chatBean = mChatUsers.get(key);
                        chatBean.setUserName(firebaseUserDetailModel.getUserName());
                        chatBean.setProfileUrl(firebaseUserDetailModel.getProfileImage());
                        mChatUsers.put(key, chatBean);
                    }
                    inboxArrayList.clear();
                    inboxArrayList.addAll(mChatUsers.values());
                    if (inboxArrayList.size() > 0) {
                        mActivityInboxListBinding.ivNoData.setVisibility(View.GONE);
                        mInboxListRecyclerViewAdapter.notifyDataSetChanged();
                    } else {
                        mActivityInboxListBinding.ivNoData.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        mLastMessageEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 :
                        dataSnapshot.getChildren()) {
                    Message message = dataSnapshot1.getValue(Message.class);
                    String[] users = dataSnapshot.getKey().split("_");
                    if (mChatUsers.containsKey(users[0])) {
                        ChatBean chatBean = mChatUsers.get(users[0]);
                        chatBean.setLastMessage(message.getText());
                        chatBean.setTime(message.getTimestamp());
                        mChatUsers.put(users[0], chatBean);
                    } else if (mChatUsers.containsKey(users[1])) {
                        ChatBean chatBean = mChatUsers.get(users[1]);
                        chatBean.setLastMessage(message.getText());
                        chatBean.setTime(message.getTimestamp());
                        mChatUsers.put(users[1], chatBean);
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        mDatabaseReference.child(AppConstants.FirebaseConstants.INBOX).child(String.valueOf(DataManager.getInstance().getUserId())).addValueEventListener(mInboxValueEventListener);
        ListnersBean listnersBean = new ListnersBean();
        listnersBean.setValueEventListener(mInboxValueEventListener);
        listnersBean.setDatabaseReference(mDatabaseReference.child(AppConstants.FirebaseConstants.INBOX).child(String.valueOf(DataManager.getInstance().getUserId())));
        mListenersList.add(listnersBean);
    }

    private boolean checkIsBlocked(String userId) {
        if (blockedUsers != null) {
            for (int i = 0; i < blockedUsers.size(); i++) {
                if (blockedUsers.get(i).getUserId().equalsIgnoreCase(userId)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void onChatUserClicked(ChatBean chatBeanModel) {

        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(AppConstants.ClassConstants.ROOM_ID, chatBeanModel.getRoomId());
        intent.putExtra(AppConstants.ClassConstants.USER_ID, chatBeanModel.getUserId());
        intent.putExtra(AppConstants.ClassConstants.USER_NAME, chatBeanModel.getUserName());
        intent.putExtra(AppConstants.ClassConstants.IS_BLOCKED, checkIsBlocked(chatBeanModel.getUserId()));
        intent.putExtra(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.FROM_INBOX);
        startActivity(intent);
    }

    private void removeListeners() {
        for (int i = 0; i < mListenersList.size(); i++) {
            mListenersList.get(i).getDatabaseReference().removeEventListener(mListenersList.get(i).getValueEventListener());
        }
    }

    @Override
    protected void onPause() {
        inboxArrayList.clear();
        removeListeners();
        super.onPause();
    }

    @Override
    public void onAdapterClicked(int code, Object obj, int position) {
        switch (code) {
            case AppConstants.ClassConstants.ON_PROFILE_CLICKED:
                if (obj != null && obj instanceof ChatBean) {
                    ChatBean chatBean = (ChatBean) obj;
                    ProfileFragment profileFragment = new ProfileFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_INBOX_ACTIVITY);
                    bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.OTHER_USER_PROFILE);
                    bundle.putString(AppConstants.ClassConstants.USER_ID, String.valueOf(chatBean.getUserId()));
                    profileFragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, profileFragment).addToBackStack(ProfileFragment.class.getCanonicalName()).commit();

                }
                break;
        }
    }
}
