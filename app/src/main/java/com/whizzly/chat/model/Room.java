package com.whizzly.chat.model;

public class Room {
    private long lastActive, createdAt;
    private String chatType, roomId;
    private String roomStatusAdmin = "0";
    private String roomStatusUser = "0";
    private String blockedUserId = "";
   public Room(){

   }
    public Room(long lastActive, long createdAt, String chatType, String roomId, String roomStatusAdmin, String roomStatusUser, String blockedUserId) {
        this.lastActive = lastActive;
        this.createdAt = createdAt;
        this.chatType = chatType;
        this.roomId = roomId;
        this.roomStatusAdmin = roomStatusAdmin;
        this.roomStatusUser = roomStatusUser;
        this.blockedUserId = blockedUserId;
    }

    public String getRoomStatusUser() {
        return roomStatusUser;
    }

    public void setRoomStatusUser(String roomStatusUser) {
        this.roomStatusUser = roomStatusUser;
    }

    public long getLastActive() {
        return lastActive;
    }

    public void setLastActive(long lastActive) {
        this.lastActive = lastActive;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public String getChatType() {
        return chatType;
    }

    public void setChatType(String chatType) {
        this.chatType = chatType;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRoomStatusAdmin() {
        return roomStatusAdmin;
    }

    public void setRoomStatusAdmin(String roomStatusAdmin) {
        this.roomStatusAdmin = roomStatusAdmin;
    }

    public String getBlockedUserId() {
        return blockedUserId;
    }

    public void setBlockedUserId(String blockedUserId) {
        this.blockedUserId = blockedUserId;
    }
}
