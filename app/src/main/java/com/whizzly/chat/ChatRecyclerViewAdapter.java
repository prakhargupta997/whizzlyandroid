package com.whizzly.chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.chat.model.Message;
import com.whizzly.chat.model.TimeBean;
import com.whizzly.databinding.ItemRowChatGifRecieverBinding;
import com.whizzly.databinding.ItemRowChatGifSenderBinding;
import com.whizzly.databinding.ItemRowChatRecieverBinding;
import com.whizzly.databinding.ItemRowChatSenderBinding;
import com.whizzly.databinding.ItemRowChatTimeBinding;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.ArrayList;

import static com.whizzly.utils.AppConstants.FirebaseConstants.MESSAGE_TYPE;
import static com.whizzly.utils.AppConstants.FirebaseConstants.MESSAGE_TYPE_GIF;

public class ChatRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Object> mMessageArrayList;
    private Context mContext;

    private ItemRowChatSenderBinding mItemRowChatSenderBinding;
    private ItemRowChatRecieverBinding mItemRowChatRecieverBinding;
    private ItemRowChatTimeBinding mItemRowChatTimeBinding;
    private ItemRowChatGifSenderBinding mItemRowChatGifSenderBinding;
    private ItemRowChatGifRecieverBinding mItemRowChatGifRecieverBinding;


    public ChatRecyclerViewAdapter(ArrayList<Object> mMessageArrayList, Context mContext) {
        this.mMessageArrayList = mMessageArrayList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mItemRowChatRecieverBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.item_row_chat_reciever, viewGroup, false);
        mItemRowChatSenderBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.item_row_chat_sender, viewGroup, false);
        mItemRowChatTimeBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.item_row_chat_time, viewGroup, false);
        mItemRowChatGifSenderBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.item_row_chat_gif_sender, viewGroup, false);
        mItemRowChatGifRecieverBinding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.item_row_chat_gif_reciever, viewGroup, false);
        switch (i) {
            case 1:
                return new RowChatSenderViewHolder(mItemRowChatSenderBinding);
            case 2:
                return new RowChatRecieverViewHolder(mItemRowChatRecieverBinding);
            case 3:
                return new RowChatTimeViewHolder(mItemRowChatTimeBinding);
            case 4:
                return new RowChatRecieverGIFViewHolder(mItemRowChatGifRecieverBinding, viewGroup.getWidth());
            case 5:
                return new RowChatSenderGIFViewHolder(mItemRowChatGifSenderBinding, viewGroup.getWidth());
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        switch (getItemViewType(i)){
            case 1:
                ((RowChatSenderViewHolder)viewHolder).bind((Message) mMessageArrayList.get(i));
                break;
            case 2:
                ((RowChatRecieverViewHolder)viewHolder).bind((Message) mMessageArrayList.get(i));
                break;
            case 3:
                ((RowChatTimeViewHolder)viewHolder).bind((TimeBean) mMessageArrayList.get(i));
                break;
            case 4:
                ((RowChatRecieverGIFViewHolder) viewHolder).bind((Message) mMessageArrayList.get(i));
                break;
            case 5:
                ((RowChatSenderGIFViewHolder) viewHolder).bind((Message) mMessageArrayList.get(i));
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mMessageArrayList.get(position) instanceof Message) {
            if (((Message) mMessageArrayList.get(position)).getIdSender().equalsIgnoreCase(String.valueOf(DataManager.getInstance().getUserId()))) {
                if (((Message) mMessageArrayList.get(position)).getMessageType().equalsIgnoreCase(MESSAGE_TYPE_GIF)) {
                    return 5;
                } else if (((Message) mMessageArrayList.get(position)).getMessageType().equalsIgnoreCase(MESSAGE_TYPE)) {
                    return 1;
                }
            } else {
                if (((Message) mMessageArrayList.get(position)).getMessageType().equalsIgnoreCase(MESSAGE_TYPE_GIF)) {
                    return 4;
                } else if (((Message) mMessageArrayList.get(position)).getMessageType().equalsIgnoreCase(MESSAGE_TYPE)) {
                    return 2;
                }
            }
        } else if (mMessageArrayList.get(position) instanceof TimeBean) {
            return 3;
        }
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return mMessageArrayList.size();
    }

    public class RowChatTimeViewHolder extends RecyclerView.ViewHolder {
        private ItemRowChatTimeBinding itemRowChatTimeBinding;

        public RowChatTimeViewHolder(@NonNull ItemRowChatTimeBinding itemRowChatTimeBinding) {
            super(itemRowChatTimeBinding.getRoot());
            this.itemRowChatTimeBinding = itemRowChatTimeBinding;
        }
        public void bind(TimeBean timeBean){
            itemRowChatTimeBinding.setVariable(BR.model, timeBean);
            itemRowChatTimeBinding.tvMessageTime.setText(timeBean.getTime());
            itemRowChatTimeBinding.executePendingBindings();
        }
    }

    public class RowChatSenderViewHolder extends RecyclerView.ViewHolder {
        private ItemRowChatSenderBinding itemRowChatSenderBinding;

        public RowChatSenderViewHolder(@NonNull ItemRowChatSenderBinding itemRowChatSenderBinding) {
            super(itemRowChatSenderBinding.getRoot());
            this.itemRowChatSenderBinding = itemRowChatSenderBinding;
        }
        public void bind(Message message){
            itemRowChatSenderBinding.setVariable(BR.model, message);
            itemRowChatSenderBinding.tvMessage.setText(message.getText());
            itemRowChatSenderBinding.executePendingBindings();
        }
    }

    public class RowChatSenderGIFViewHolder extends RecyclerView.ViewHolder {
        private ItemRowChatGifSenderBinding itemRowChatGifSenderBinding;
        private int width;

        public RowChatSenderGIFViewHolder(@NonNull ItemRowChatGifSenderBinding itemRowChatGifSenderBinding, int width) {
            super(itemRowChatGifSenderBinding.getRoot());
            this.itemRowChatGifSenderBinding = itemRowChatGifSenderBinding;
            this.width = width;
        }

        public void bind(Message message) {
            itemRowChatGifSenderBinding.setVariable(BR.model, message);
            itemRowChatGifSenderBinding.ivMessage.setMinimumWidth((int)(width*0.80));
            Glide.with(itemView.getContext()).asGif().load(message.getText()).into(itemRowChatGifSenderBinding.ivMessage);
            itemRowChatGifSenderBinding.executePendingBindings();
        }
    }

    public class RowChatRecieverGIFViewHolder extends RecyclerView.ViewHolder {
        private ItemRowChatGifRecieverBinding itemRowChatGifRecieverBinding;
        private int width;

        public RowChatRecieverGIFViewHolder(@NonNull ItemRowChatGifRecieverBinding itemRowChatGifRecieverBinding, int width) {
            super(itemRowChatGifRecieverBinding.getRoot());
            this.itemRowChatGifRecieverBinding = itemRowChatGifRecieverBinding;
            this.width = width;
        }

        public void bind(Message message) {
            itemRowChatGifRecieverBinding.setVariable(BR.model, message);
            itemRowChatGifRecieverBinding.ivMessage.setMinimumWidth((int)(width*0.80));
            Glide.with(itemView.getContext()).asGif().load(message.getText()).into(itemRowChatGifRecieverBinding.ivMessage);
            itemRowChatGifRecieverBinding.executePendingBindings();
        }
    }

    public class RowChatRecieverViewHolder extends RecyclerView.ViewHolder {
        private ItemRowChatRecieverBinding itemRowChatRecieverBinding;

        public RowChatRecieverViewHolder(@NonNull ItemRowChatRecieverBinding itemRowChatRecieverBinding) {
            super(itemRowChatRecieverBinding.getRoot());
            this.itemRowChatRecieverBinding = itemRowChatRecieverBinding;
        }

        public void bind(Message message){
            itemRowChatRecieverBinding.setVariable(BR.model, message);
            itemRowChatRecieverBinding.tvMessage.setText(message.getText());
            FirebaseDatabase.getInstance().getReference().child(AppConstants.FirebaseConstants.USERS).child(message.getIdSender()).child(AppConstants.FirebaseConstants.PROFILE_IMAGE).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.placeholder(R.drawable.ic_comment_placeholder);
                    Glide.with(mContext).load(dataSnapshot.getValue()).apply(requestOptions.circleCrop()).into(itemRowChatRecieverBinding.ivUserProfile);
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            itemRowChatRecieverBinding.executePendingBindings();
        }
    }

}
