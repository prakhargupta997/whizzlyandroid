package com.whizzly.chat;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.blockedUsers.BlockedUsersResponseBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class InboxListViewModel extends ViewModel {
    private RichMediatorLiveData<BlockedUsersResponseBean> mBlockedUsersRichMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;

    public RichMediatorLiveData<BlockedUsersResponseBean> getBlockedUsersRichMediatorLiveData() {
        return mBlockedUsersRichMediatorLiveData;
    }

    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (mBlockedUsersRichMediatorLiveData == null) {
            mBlockedUsersRichMediatorLiveData = new RichMediatorLiveData<BlockedUsersResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
    }

    private HashMap<String, String> getData() {

        HashMap<String, String> params = new HashMap<>();

        String userId = String.valueOf(DataManager.getInstance().getUserId());

        params.put(AppConstants.NetworkConstants.PARAM_USER_ID, userId);


        return params;
    }


    public void hitApiToFetchBlockedList(RichMediatorLiveData<BlockedUsersResponseBean> mBlockedUsersRichMediatorLiveData) {

        DataManager.getInstance().getBlockedUsersList(getData()).enqueue(new NetworkCallback<BlockedUsersResponseBean>() {
            @Override
            public void onSuccess(BlockedUsersResponseBean blockedUsersBean) {
                mBlockedUsersRichMediatorLiveData.setValue(blockedUsersBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mBlockedUsersRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mBlockedUsersRichMediatorLiveData.setError(t);
            }
        });

    }

}
