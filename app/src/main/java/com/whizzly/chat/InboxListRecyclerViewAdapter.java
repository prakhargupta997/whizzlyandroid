package com.whizzly.chat;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.whizzly.R;
import com.whizzly.chat.model.ChatBean;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.ItemRowChatBinding;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;

import java.util.ArrayList;

public class InboxListRecyclerViewAdapter extends RecyclerView.Adapter<InboxListRecyclerViewAdapter.ViewHolder> {

    private ItemRowChatBinding mItemRowChatBinding;
    private ArrayList<ChatBean> mChatBeanArrayList;
    private Context context;
    private InboxListUserClick chatListUserClick;
    private OnClickAdapterListener mOnClickAdapterListener;

    public InboxListRecyclerViewAdapter(ArrayList<ChatBean> mChatBeanArrayList, Context context, InboxListUserClick chatListUserClick, OnClickAdapterListener onClickAdapterListener) {
        this.mChatBeanArrayList = mChatBeanArrayList;
        this.context = context;
        this.chatListUserClick = chatListUserClick;
        this.mOnClickAdapterListener = onClickAdapterListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mItemRowChatBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_row_chat, viewGroup, false);
        return new ViewHolder(mItemRowChatBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(mChatBeanArrayList.get(i));
        mItemRowChatBinding.ivUserProfileIcon.setOnClickListener(v ->
                mOnClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_PROFILE_CLICKED, mChatBeanArrayList.get(i), i));


    }

    @Override
    public int getItemCount() {
        return mChatBeanArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ItemRowChatBinding itemRowChatBinding;

        public ViewHolder(@NonNull ItemRowChatBinding itemRowChatBinding) {
            super(itemRowChatBinding.getRoot());
            this.itemRowChatBinding = itemRowChatBinding;
        }

        void bind(ChatBean chatBean) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.error(R.drawable.ic_comment_placeholder);
            requestOptions.circleCrop();
            requestOptions.placeholder(R.drawable.ic_comment_placeholder);
            Glide.with(context).load(chatBean.getProfileUrl()).apply(requestOptions).into(itemRowChatBinding.ivUserProfileIcon);
            if(chatBean.getLastMessage()!=null && chatBean.getLastMessage().startsWith("https://") && chatBean.getLastMessage().endsWith(".gif")){
                itemRowChatBinding.tvLastMessage.setCompoundDrawablesWithIntrinsicBounds(context.getResources().getDrawable(R.drawable.ic_gif), null, null, null);
            }else{
                itemRowChatBinding.tvLastMessage.setText(chatBean.getLastMessage());
            }

            itemRowChatBinding.tvUserName.setText(chatBean.getUserName());
            itemRowChatBinding.tvTime.setText(AppUtils.getTime(chatBean.getTime()));
            itemRowChatBinding.getRoot().setOnClickListener(v -> chatListUserClick.onChatUserClicked(chatBean));
        }
    }
}
