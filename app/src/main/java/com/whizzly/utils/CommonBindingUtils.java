package com.whizzly.utils;

import androidx.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.net.Uri;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.whizzly.R;

public class CommonBindingUtils {
    @BindingAdapter("app:error")
    public static void setError(TextInputLayout til, String error) {
        if (error!=null && !error.isEmpty())
            til.setError(error);
        else
            til.setError(null);
    }

    @BindingAdapter({"app:isSlideUp"})
    public static void slidingUpViews(View view, boolean isAnimation) {
        if (isAnimation) {
            Animation bottomToUp = AnimationUtils.loadAnimation(view.getContext(), R.anim.bottom_up);
            view.startAnimation(bottomToUp);
        }
    }

    @BindingAdapter("app:set_image")
    public static void setImage(View view, Bitmap image) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_profile_setup_user_placeholder);

        Glide.with(view.getContext())
                .load(image)
                .apply(requestOptions)
                .into((ImageView)view);
    }

    @BindingAdapter("app:set_bitmap")
    public static void setBitmap(View view, Bitmap image) {
        Glide.with(view.getContext())
                .load(image)
                .into((ImageView)view);
    }

    @BindingAdapter("app:set_image_url")
    public static void setImageUrl(View view, Uri image) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_profile_setup_user_placeholder);
        Glide.with(view.getContext())
                .load(image)
                .apply(requestOptions)
                .into((ImageView)view);
    }

    @BindingAdapter("setImageFromNetwork")
    public static void setImageFromNetwork(ImageView view, String image) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_profile_setup_user_placeholder);
        requestOptions.error(R.drawable.ic_profile_setup_user_placeholder);
        requestOptions.circleCrop();
        Glide.with(view.getContext())
                .load(image)
                .apply(requestOptions)
                .into((ImageView)view);
    }



    @BindingAdapter("app:set_song_image_url")
    public static void setSongImageUrl(View view, Uri image) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.music);
        Glide.with(view.getContext())
                .load(image)
                .apply(requestOptions)
                .into((ImageView)view);
    }

    @BindingAdapter("app:set_song_image")
    public static void setSongImage(View view, String image) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.music);
        Glide.with(view.getContext())
                .load(image)
                .apply(requestOptions)
                .into((ImageView) view);
    }

}
