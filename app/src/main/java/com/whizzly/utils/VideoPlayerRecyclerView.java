package com.whizzly.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.whizzly.R;
import com.whizzly.models.feeds_response.Result;

import java.util.ArrayList;

public class VideoPlayerRecyclerView extends RecyclerView {


    CacheDataSourceFactory caching;
    private View viewHolderParent = null;
    private FrameLayout frameLayout = null;
    private PlayerView videoSurfaceView = null;
    private SimpleExoPlayer videoPlayer = null;
    // vars
    private ArrayList<Result> mediaObjects = new ArrayList<>();
    private int videoSurfaceDefaultHeight = 0;
    private int screenDefaultHeight = 0;
    private int playPosition = -1;
    private boolean isVideoViewAdded = false;
    // controlling playback state
    private VolumeState volumeState = null;
    private FrameLayout flProgressBar;
    private TextView tvMusicName;
    private ConstraintLayout clLayout;
    private ImageView ivFeedOptions;
    private ImageView ivMusicIcon;
    private RecyclerView rvCommentsList;
    private ImageView ivAddComment;
    private TextView tvReuseCount;
    private ImageView ivReuseCount;
    private ImageView ivLikes;
    private TextView tvLikes;
    private boolean isDisappeared = false;

    public VideoPlayerRecyclerView(@NonNull Context context) {
        super(context);
        init(context);
    }

    public VideoPlayerRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public VideoPlayerRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {


        caching = new CacheDataSourceFactory(
                context,
                (100 * 1024 * 1024),
                (5 * 1024 * 1024)
        );

        Display display = ((WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        videoSurfaceDefaultHeight = point.x;
        screenDefaultHeight = point.y;

        videoSurfaceView = new PlayerView(context);
        videoSurfaceView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_ZOOM);

        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        // 2. Create the player
        videoPlayer = ExoPlayerFactory.newSimpleInstance(context, trackSelector);
        // Bind the player to the view.
        videoSurfaceView.setUseController(false);
        videoSurfaceView.setPlayer(videoPlayer);
        setVolumeControl(VolumeState.ON);
        addOnChildAttachStateChangeListener(new OnChildAttachStateChangeListener() {
            @Override
            public void onChildViewAttachedToWindow(@NonNull View view) {
            }

            @Override
            public void onChildViewDetachedFromWindow(@NonNull View view) {
                if (viewHolderParent != null && viewHolderParent == view) {
                    resetVideoView();
                }
            }
        });

        videoPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                switch (playbackState) {
                    case Player.STATE_BUFFERING:
//                        flProgressBar.setVisibility(VISIBLE);
                        break;
                    case Player.STATE_ENDED:
//                        flProgressBar.setVisibility(GONE);
                        videoPlayer.seekTo(0);
                        break;
                    case Player.STATE_READY:
                        flProgressBar.setVisibility(GONE);
                        if (!isVideoViewAdded) {
                            addVideoView();
                        }
                        break;
                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });
    }

    private int getVisibleVideoSurfaceHeight(int playPosition) {
        int at = playPosition - ((LinearLayoutManager) getLayoutManager()).findFirstVisibleItemPosition();
        View child = getChildAt(at);
        if (child == null) {
            return 0;
        }
        int[] location = new int[2];
        child.getLocationInWindow(location);
        if (location[1] < 0) {
            return location[1] + videoSurfaceDefaultHeight;
        } else {
            return screenDefaultHeight - location[1];
        }
    }

    public void playVideo(Boolean isEndOfList) {
        int targetPosition;
        if (!isEndOfList) {
            int startPosition = ((LinearLayoutManager) getLayoutManager()).findFirstVisibleItemPosition();
            int endPosition = ((LinearLayoutManager) getLayoutManager()).findLastVisibleItemPosition();

            // if there is more than 2 list-items on the screen, set the difference to be 1
            if (endPosition - startPosition > 1) {
                endPosition = startPosition + 1;
            }

            // something is wrong. return.
            if (startPosition < 0 || endPosition < 0) {
                return;
            }

            // if there is more than 1 list-item on the screen
            if (startPosition != endPosition) {
                int startPositionVideoHeight = getVisibleVideoSurfaceHeight(startPosition);
                int endPositionVideoHeight = getVisibleVideoSurfaceHeight(endPosition);
                if (startPositionVideoHeight > endPositionVideoHeight)
                    targetPosition = startPosition;
                else
                    targetPosition = endPosition;
            } else {
                targetPosition = startPosition;
            }
        } else {
            targetPosition = mediaObjects.size() - 1;
        }
        // video is already playing so return
        if (targetPosition == playPosition) {
            return;
        }
        // set the position of the list-item that is to be played
        playPosition = targetPosition;
        if (videoSurfaceView == null) {
            return;
        }
        if (videoPlayer == null) {
            return;
        }
        videoSurfaceView.setVisibility(View.INVISIBLE);
        removeVideoView(videoSurfaceView);
        int currentPosition = targetPosition - ((LinearLayoutManager) getLayoutManager()).findFirstVisibleItemPosition();
        View child = getChildAt(currentPosition);
        if (child == null) {
            return;
        }
        VideoPlayerViewHolder holder = (VideoPlayerViewHolder) child.getTag();
        if (holder == null) {
            playPosition = -1;
            return;
        }
        viewHolderParent = holder.itemView;
        frameLayout = viewHolderParent.findViewById(R.id.media_container);
        flProgressBar = viewHolderParent.findViewById(R.id.fl_progress);
        clLayout = viewHolderParent.findViewById(R.id.cl_layout);
        tvLikes = viewHolderParent.findViewById(R.id.tv_likes);
        ivLikes = viewHolderParent.findViewById(R.id.iv_likes);
        ivReuseCount = viewHolderParent.findViewById(R.id.iv_reuse_count);
        tvReuseCount = viewHolderParent.findViewById(R.id.tv_reuse_count);
        ivAddComment = viewHolderParent.findViewById(R.id.iv_add_comment);
        rvCommentsList = viewHolderParent.findViewById(R.id.rv_comments_list);
        tvMusicName = viewHolderParent.findViewById(R.id.tv_music_name);
        ivMusicIcon = viewHolderParent.findViewById(R.id.iv_music_icon);
        ivFeedOptions = viewHolderParent.findViewById(R.id.iv_feed_options);
        if(!isDisappeared) {
            flProgressBar.setVisibility(VISIBLE);
            clLayout.setVisibility(View.VISIBLE);
            tvLikes.setVisibility(View.VISIBLE);
            ivLikes.setVisibility(View.VISIBLE);
            ivReuseCount.setVisibility(View.VISIBLE);
            tvReuseCount.setVisibility(View.VISIBLE);
            ivAddComment.setVisibility(View.VISIBLE);
            rvCommentsList.setVisibility(View.VISIBLE);
            if (mediaObjects.get(targetPosition).getMusicDescription() != null) {
                ivMusicIcon.setVisibility(View.VISIBLE);
                tvMusicName.setVisibility(View.VISIBLE);
            } else {
                ivMusicIcon.setVisibility(View.GONE);
                tvMusicName.setVisibility(View.GONE);
            }
            ivFeedOptions.setVisibility(View.VISIBLE);
        }else{
            flProgressBar.setVisibility(GONE);
            clLayout.setVisibility(View.GONE);
            tvLikes.setVisibility(View.GONE);
            ivLikes.setVisibility(View.GONE);
            ivReuseCount.setVisibility(View.GONE);
            tvReuseCount.setVisibility(View.GONE);
            ivAddComment.setVisibility(View.GONE);
            rvCommentsList.setVisibility(View.GONE);
            if (mediaObjects.get(targetPosition).getMusicDescription() != null) {
                ivMusicIcon.setVisibility(View.GONE);
                tvMusicName.setVisibility(View.GONE);
            } else {
                ivMusicIcon.setVisibility(View.GONE);
                tvMusicName.setVisibility(View.GONE);
            }
            ivFeedOptions.setVisibility(View.GONE);
        }
        String mediaUrl = mediaObjects.get(targetPosition).getUrl();
        if (mediaUrl != null) {
            if (mediaObjects.get(targetPosition).getCount() == 2) {
                videoSurfaceView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
            } else {
                videoSurfaceView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_ZOOM);
            }
            DefaultExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
            ExtractorMediaSource mediaSource = new ExtractorMediaSource(Uri.parse(mediaUrl), caching, extractorsFactory, null, null);
            videoPlayer.prepare(mediaSource);
            videoPlayer.setPlayWhenReady(true);
        }
        videoSurfaceView.setPlayer(videoPlayer);
        addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (!recyclerView.canScrollVertically(1)) {
                        playVideo(true);
                    } else {
                        playVideo(false);
                    }
                }

            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    isDisappeared = true;
                    LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent(AppConstants.ClassConstants.ACTION_HIDE_VIEWS));
                    clLayout.setVisibility(View.GONE);
                    tvLikes.setVisibility(View.GONE);
                    ivLikes.setVisibility(View.GONE);
                    ivReuseCount.setVisibility(View.GONE);
                    tvReuseCount.setVisibility(View.GONE);
                    ivAddComment.setVisibility(View.GONE);
                    rvCommentsList.setVisibility(View.GONE);

                    tvMusicName.setVisibility(View.GONE);
                    ivMusicIcon.setVisibility(View.GONE);

                    ivFeedOptions.setVisibility(View.GONE);
                } else if (dy < 0) {
                    isDisappeared = false;
                    LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent(AppConstants.ClassConstants.ACTION_SHOW_VIEWS));
                    clLayout.setVisibility(View.VISIBLE);
                    tvLikes.setVisibility(View.VISIBLE);
                    ivLikes.setVisibility(View.VISIBLE);
                    ivReuseCount.setVisibility(View.VISIBLE);
                    tvReuseCount.setVisibility(View.VISIBLE);
                    ivAddComment.setVisibility(View.VISIBLE);
                    rvCommentsList.setVisibility(View.VISIBLE);
                    if (mediaObjects.get(targetPosition).getMusicDescription() != null) {
                        ivMusicIcon.setVisibility(View.VISIBLE);
                        tvMusicName.setVisibility(View.VISIBLE);
                    } else {
                        tvMusicName.setVisibility(View.GONE);
                        ivMusicIcon.setVisibility(View.GONE);
                    }

                    ivFeedOptions.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    // Remove the old player
    private void removeVideoView(PlayerView videoView) {

        ViewGroup parent = null;
        parent = (ViewGroup) videoView.getParent();

        if (parent == null)
            return;

        int index = parent.indexOfChild(videoView);
        if (index >= 0) {
            parent.removeViewAt(index);
            isVideoViewAdded = false;
        }

    }

    private void addVideoView() {
        frameLayout.addView(videoSurfaceView);
        isVideoViewAdded = true;
        videoSurfaceView.requestFocus();
        videoSurfaceView.setVisibility(View.VISIBLE);
        videoSurfaceView.setAlpha(1f);

        //   thumbnail.setVisibility(GONE);
    }

    private void resetVideoView() {
        if (isVideoViewAdded) {
            removeVideoView(videoSurfaceView);
            playPosition = -1;
            videoSurfaceView.setVisibility(View.INVISIBLE);
        }
    }

    public void releasePlayer() {
        if (videoPlayer != null) {
            videoPlayer.setPlayWhenReady(false);
            videoPlayer.stop();
            videoPlayer.release();
            videoPlayer = null;
        }
        viewHolderParent = null;
    }

    public void pausePlayer() {
        if (videoPlayer != null) {
            videoPlayer.setPlayWhenReady(false);
        }
    }

    public void resumePlayer() {
        if (videoPlayer != null) {
            videoPlayer.setPlayWhenReady(true);
            if (frameLayout != null && isVideoViewAdded) {
                videoSurfaceView.setVisibility(VISIBLE);
            }
        }
    }

    private void toggleVolume() {
        if (videoPlayer != null) {
            if (volumeState == VolumeState.OFF) {
                setVolumeControl(VolumeState.ON);

            } else if (volumeState == VolumeState.ON) {
                setVolumeControl(VolumeState.OFF);

            }
        }
    }

    private void setVolumeControl(VolumeState state) {
        volumeState = state;
        if (state == VolumeState.OFF) {
            videoPlayer.setVolume(0f);
        } else if (state == VolumeState.ON) {
            videoPlayer.setVolume(1f);
        }
    }

    public void setMediaObjects(ArrayList<Result> mediaObjects) {
        this.mediaObjects = mediaObjects;
    }


    private enum VolumeState {
        ON, OFF
    }
}
