package com.whizzly.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import androidx.appcompat.widget.AppCompatSeekBar;
import android.util.AttributeSet;
import android.util.TypedValue;


public class SeekbarWithIndex extends AppCompatSeekBar {
    public SeekbarWithIndex(Context context) {
        super(context);
        init();
    }

    public SeekbarWithIndex(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SeekbarWithIndex(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private Paint paint;

    private Rect bounds;
    private void init(){
        paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setTextSize(sp2px(14));
        bounds = new Rect();
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        String label = String.valueOf(getProgress());
        paint.getTextBounds(label, 0, label.length(), bounds);
        float x = ((float) getProgress() * (getWidth() - 2 * getThumbOffset()) / getMax() +
                (1 - (float) getProgress() / getMax()) * bounds.width() / 2 - bounds.width() / 2
                + getThumbOffset() / label.length())*0.9f+40;
        canvas.drawText(label, x, 35, paint);
    }

    private int sp2px(int sp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, getResources().getDisplayMetrics());
    }
}
