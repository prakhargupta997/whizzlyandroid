package com.whizzly.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.transition.TransitionManager;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.fragment.SuggestedVideoFragment;
import com.whizzly.databinding.LayoutVideoListItemBinding;
import com.whizzly.home_feed_comments.CameraCommentActivity;
import com.whizzly.home_feed_comments.FeedsCommentPreviewActivity;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.home_screen_module.home_screen_fragments.home_feeds.BottomSheetPeopleLikeListFragment;
import com.whizzly.home_screen_module.home_screen_fragments.home_feeds.BottomSheetPeopleReuseFragment;
import com.whizzly.home_screen_module.home_screen_fragments.home_feeds.BottomSheetShareFragment;
import com.whizzly.home_screen_module.home_screen_fragments.home_feeds.CommentListAdapter;
import com.whizzly.home_screen_module.home_screen_fragments.home_feeds.VideoPreviewActivity;
import com.whizzly.home_screen_module.home_screen_fragments.profile.ProfileFragment;
import com.whizzly.interfaces.OnActionDetectListener;
import com.whizzly.interfaces.OnSendCommentDataListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.comment_list.CommentListingBean;
import com.whizzly.models.feeds_response.Result;
import com.whizzly.models.like_dislike_response.LikeDislikeResponse;
import com.whizzly.network.NetworkCallback;
import com.whizzly.search_module.hashtagvideos.HashTagVideosActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import retrofit2.Call;

public class VideoPlayerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, OnSendCommentDataListener {
    private LayoutVideoListItemBinding layoutVideoListItemBinding;
    private Activity context;
    private CommentListAdapter commentListAdapter;
    private int commentPage = 1;
    private Result result;
    private ArrayList<com.whizzly.models.comment_list.Result> commentResultsList;
    private FragmentManager fragmentManager;
    private boolean isDisappear = false;
    private OnActionDetectListener mOnActionDetectListener;
    private GestureDetector gestureDetector;
    private Call<CommentListingBean> commentListingBeanCall;

    VideoPlayerViewHolder(@NonNull LayoutVideoListItemBinding itemView) {
        super(itemView.getRoot());
        layoutVideoListItemBinding = itemView;
    }

    private HashMap<String, String> videoCommentHashMap(Result result) {
        HashMap<String, String> videoCommentQuery = new HashMap<>();
        videoCommentQuery.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        videoCommentQuery.put(AppConstants.NetworkConstants.VIDEO_ID, String.valueOf(result.getId()));
        videoCommentQuery.put(AppConstants.NetworkConstants.VIDEO_TYPE, result.getType());
        videoCommentQuery.put(AppConstants.NetworkConstants.PAGE, String.valueOf(commentPage));
        return videoCommentQuery;
    }

    void onBind(Result result, Activity context, FragmentManager fragmentManager, OnActionDetectListener onActionDetectListener) {
        commentResultsList = new ArrayList<>();
        gestureDetector = new GestureDetector(context, new GestureListener());
        this.context = context;
        this.result = result;
        this.fragmentManager = fragmentManager;
        layoutVideoListItemBinding.getRoot().setTag(this);
        layoutVideoListItemBinding.ivAddComment.setOnClickListener(this);
        layoutVideoListItemBinding.ivLikes.setOnClickListener(this);
        layoutVideoListItemBinding.ivFeedOptions.setOnClickListener(this);
        layoutVideoListItemBinding.ivMusicIcon.setOnClickListener(this);
        layoutVideoListItemBinding.ivUserProfileIcon.setOnClickListener(this);
        layoutVideoListItemBinding.tvLikes.setOnClickListener(this);
        layoutVideoListItemBinding.ivReuseCount.setOnClickListener(this);
        layoutVideoListItemBinding.tvMusicName.setOnClickListener(this);
        layoutVideoListItemBinding.tvReuseCount.setOnClickListener(this);
        layoutVideoListItemBinding.tvUserName.setOnClickListener(this);
//        layoutVideoListItemBinding.mediaContainer.setOnClickListener(this);
        isDisappear = false;
        TransitionManager.beginDelayedTransition(layoutVideoListItemBinding.rlMainView);
        this.mOnActionDetectListener = onActionDetectListener;
        setDataToViews(result);
        setCommentRecyclerView(result);
        setListener();
        hitCommentListingApi(videoCommentHashMap(result));
    }

    private void setListener() {
        layoutVideoListItemBinding.mediaContainer.setOnTouchListener((v, event) -> {
            gestureDetector.onTouchEvent(event);
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mOnActionDetectListener.onActionDone(true);
                    return true;
                case MotionEvent.ACTION_UP:
                    mOnActionDetectListener.onActionDone(false);
                    return true;
            }
            return false;
        });
    }

    private void setDataToViews(Result result) {
        layoutVideoListItemBinding.tvUserName.setText(String.format("@%s", result.getUserName()));
        layoutVideoListItemBinding.tvLikes.setText(String.valueOf(result.getTotalLikes()));
        layoutVideoListItemBinding.tvReuseCount.setText(String.valueOf(result.getTotalReuse()));
        layoutVideoListItemBinding.tvVideoDescription.setVisibility(View.INVISIBLE);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.error(R.drawable.ic_feed_user_pic_placeholder);
        requestOptions.placeholder(R.drawable.ic_feed_user_pic_placeholder);
        requestOptions.circleCrop();


        String hashtag = result.getVideoHastags();
        String[] hashtags = hashtag.split(",");
        StringBuilder hashtagBuillder = new StringBuilder();
        hashtagBuillder.append(result.getVideoDescription() + " ");
        if (!TextUtils.isEmpty(hashtag))
            for (int i = 0; i < hashtags.length; i++) {
                if (i == 0) {
                    hashtagBuillder.append("#" + hashtags[i]);
                } else {
                    hashtagBuillder.append(" #" + hashtags[i]);
                }
            }
        if (result.getCount() > 1) {
            hashtagBuillder.append(" with ");
            if (!TextUtils.isEmpty(result.getUsersName())) {
                String userName = result.getUsersName();
                String[] userNames = userName.split(",");
                for (int i = 0; i < userNames.length; i++) {
                    if (i == 0) {
                        hashtagBuillder.append("@" + userNames[i]);
                    } else {
                        hashtagBuillder.append(" @" + userNames[i]);
                    }
                }
                layoutVideoListItemBinding.tvCollabWith.setVisibility(View.GONE);
            } else {
                layoutVideoListItemBinding.tvCollabWith.setVisibility(View.GONE);
            }
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(hashtagBuillder);
        if (!TextUtils.isEmpty(result.getVideoHastags()) && !TextUtils.isEmpty(result.getVideoDescription())) {
            if (result.getCount() > 1) {
                spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), 0, result.getVideoDescription().length(), 0);
                spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), result.getVideoDescription().length(), result.getVideoDescription().length() + result.getVideoHastags().length() + hashtags.length + 1, 0);
                spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), result.getVideoDescription().length() + result.getVideoHastags().length() + hashtags.length + 1, result.getVideoDescription().length() + result.getVideoHastags().length() + hashtags.length + 1 + " with ".length(), 0);
                spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), result.getVideoDescription().length() + result.getVideoHastags().length() + hashtags.length + " with ".length(), hashtagBuillder.length(), 0);
            } else {
                spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), 0, result.getVideoDescription().length(), 0);
                spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), result.getVideoDescription().length(), result.getVideoDescription().length() + result.getVideoHastags().length() + hashtags.length + 1, 0);
                spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), result.getVideoDescription().length() + result.getVideoHastags().length() + hashtags.length + 1, result.getVideoDescription().length() + result.getVideoHastags().length() + hashtags.length + 1, 0);
                spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), result.getVideoDescription().length() + result.getVideoHastags().length() + hashtags.length, hashtagBuillder.length(), 0);
            }
        } else {
            if (!TextUtils.isEmpty(result.getVideoHastags()) && TextUtils.isEmpty(result.getVideoDescription())) {
                if (result.getCount() > 1) {
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), 0, result.getVideoHastags().length() + hashtags.length + 1, 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), result.getVideoHastags().length() + hashtags.length + 1, result.getVideoHastags().length() + hashtags.length + 1 + " with ".length(), 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), result.getVideoHastags().length() + hashtags.length + " with ".length(), hashtagBuillder.length(), 0);
                } else {
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), 0, result.getVideoHastags().length() + hashtags.length, 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), result.getVideoHastags().length() + hashtags.length, result.getVideoHastags().length() + hashtags.length, 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), result.getVideoHastags().length() + hashtags.length, hashtagBuillder.length(), 0);
                }
            }

            if (TextUtils.isEmpty(result.getVideoHastags()) && !TextUtils.isEmpty(result.getVideoDescription())) {
                if (result.getCount() > 1) {
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), 0, result.getVideoDescription().length(), 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), result.getVideoDescription().length(), result.getVideoDescription().length(), 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), result.getVideoDescription().length() + 1, result.getVideoDescription().length() + 1 + " with ".length(), 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), result.getVideoDescription().length() + " with ".length(), hashtagBuillder.length(), 0);
                } else {
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), 0, result.getVideoDescription().length(), 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), result.getVideoDescription().length(), result.getVideoDescription().length(), 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), result.getVideoDescription().length(), result.getVideoDescription().length(), 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), result.getVideoDescription().length(), hashtagBuillder.length(), 0);
                }
            }

            if (TextUtils.isEmpty(result.getVideoHastags()) && TextUtils.isEmpty(result.getVideoDescription())) {
                if (result.getCount() > 1) {
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), 0, " with ".length(), 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), " with ".length(), hashtagBuillder.length(), 0);
                }
            }
        }


        if (result.getVideoHastags() != null)
            layoutVideoListItemBinding.tvHashtags.setText(spannableStringBuilder);

        Glide.with(context).load(result.getProfileImage()).apply(requestOptions).into(layoutVideoListItemBinding.ivUserProfileIcon);
        if (result.getIsLiked() == 0) {
            Glide.with(context).load(R.drawable.ic_clap).apply(requestOptions).into(layoutVideoListItemBinding.ivLikes);
        } else {
            Glide.with(context).load(R.drawable.ic_clap_filled).apply(requestOptions).into(layoutVideoListItemBinding.ivLikes);
        }
        if (null == result.getMusicId()) {
            layoutVideoListItemBinding.ivMusicIcon.setVisibility(View.GONE);
            layoutVideoListItemBinding.tvMusicName.setVisibility(View.GONE);
        } else {
            layoutVideoListItemBinding.ivMusicIcon.setVisibility(View.VISIBLE);
            layoutVideoListItemBinding.tvMusicName.setVisibility(View.VISIBLE);
            layoutVideoListItemBinding.tvMusicName.setText(String.format("%s - %s", result.getMusicTitle(), result.getMusicDescription()));
            layoutVideoListItemBinding.ivMusicIcon.startAnimation(rotateAnimation());
        }

        layoutVideoListItemBinding.tvUserFollowerCount.setText(String.valueOf(result.getFollowerCount()));

        layoutVideoListItemBinding.lavClap.addAnimatorListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                layoutVideoListItemBinding.lavClap.setVisibility(View.GONE);
            }
        });
    }

    private RotateAnimation rotateAnimation() {
        RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(3000);
        rotate.setRepeatCount(Animation.INFINITE);
        rotate.setInterpolator(new LinearInterpolator());
        return rotate;
    }

    private void setCommentRecyclerView(Result result) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        layoutVideoListItemBinding.rvCommentsList.setLayoutManager(linearLayoutManager);
        commentListAdapter = new CommentListAdapter(context, layoutVideoListItemBinding.ivAddComment.getWidth(), commentResultsList, this, result.getType());
        layoutVideoListItemBinding.rvCommentsList.setAdapter(commentListAdapter);
        layoutVideoListItemBinding.rvCommentsList.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                if (commentPage > 1)
                    hitCommentListingApi(videoCommentHashMap(result));
            }
        });
    }

    public void hitCommentListingApi(HashMap<String, String> commentQuery) {
        if (commentListingBeanCall != null) {
            commentListingBeanCall.cancel();
        }
        commentListingBeanCall = DataManager.getInstance().getCommentListApi(commentQuery);
        commentListingBeanCall.enqueue(new NetworkCallback<CommentListingBean>() {
            @Override
            public void onSuccess(CommentListingBean commentListingBean) {
                if (commentListingBean != null) {
                    if (commentListingBean.getCode() == 200) {
                        commentResultsList.clear();
                        commentResultsList.addAll(commentListingBean.getResult());
                        commentListAdapter.notifyDataSetChanged();
                    } else if (commentListingBean.getCode() == 301 || commentListingBean.getCode() == 302) {
                        Toast.makeText(context, commentListingBean.getMessage(), Toast.LENGTH_SHORT).show();
                        ((BaseActivity) context).autoLogOut();
                    } else {
                        Toast.makeText(context, commentListingBean.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onFailure(FailureResponse failureResponse) {

            }

            @Override
            public void onError(Throwable t) {

            }
        });
    }

    private void likeDislikeVideo(int status) {
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put(AppConstants.NetworkConstants.LIKE_DISLIKE_STATUS, String.valueOf(status));
        queryMap.put(AppConstants.NetworkConstants.VIDEO_TYPE, result.getType());
        queryMap.put(AppConstants.NetworkConstants.VIDEO_ID, String.valueOf(result.getId()));
        queryMap.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        hitLikeDislikeApi(queryMap);
    }

    private void hitLikeDislikeApi(HashMap<String, String> queryMap) {
        DataManager.getInstance().hitLikeDislikeApi(queryMap).enqueue(new NetworkCallback<LikeDislikeResponse>() {
            @Override
            public void onSuccess(LikeDislikeResponse likeDislikeResponse) {
                if (likeDislikeResponse != null) {
                    if (likeDislikeResponse.getCode() == 301 || likeDislikeResponse.getCode() == 302) {
                        Toast.makeText(context, likeDislikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        ((BaseActivity) context).autoLogOut();
                    } else if (likeDislikeResponse.getCode() == 200) {
                        if (result.getIsLiked() == 0) {
                            layoutVideoListItemBinding.lavClap.setVisibility(View.VISIBLE);
                            layoutVideoListItemBinding.lavClap.playAnimation();
                            layoutVideoListItemBinding.ivLikes.setImageDrawable(context.getDrawable(R.drawable.ic_clap_filled));
                            result.setIsLiked(1);
                            layoutVideoListItemBinding.tvLikes.setText(String.valueOf(result.getTotalLikes() + 1));
                            result.setTotalLikes(result.getTotalLikes() + 1);

                        } else {
                            layoutVideoListItemBinding.ivLikes.setImageDrawable(context.getDrawable(R.drawable.ic_clap));
                            layoutVideoListItemBinding.tvLikes.setText(String.valueOf(result.getTotalLikes() - 1));
                            result.setTotalLikes(result.getTotalLikes() - 1);
                            result.setIsLiked(0);
                        }
                    } else
                        Toast.makeText(context, likeDislikeResponse.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {

            }

            @Override
            public void onError(Throwable t) {

            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_likes:
                if (result.getIsLiked() == 0) {
                    likeDislikeVideo(1);
                } else {
                    likeDislikeVideo(0);
                }
                break;
            case R.id.tv_likes:
                if (result.getTotalLikes() > 0) {
                    BottomSheetPeopleLikeListFragment bottomSheetPeopleLikeListFragment = new BottomSheetPeopleLikeListFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.ClassConstants.VIDEO_TYPE, result.getType());
                    bundle.putString(AppConstants.ClassConstants.FEED_VIDEO_ID, String.valueOf(result.getId()));
                    bundle.putString(AppConstants.ClassConstants.TOTAL_LIKES, String.valueOf(result.getTotalLikes()));
                    bottomSheetPeopleLikeListFragment.setArguments(bundle);
                    bottomSheetPeopleLikeListFragment.show(fragmentManager, BottomSheetPeopleLikeListFragment.class.getCanonicalName());
                }
                break;
            case R.id.iv_add_comment:
                Intent commentIntent = new Intent(context, CameraCommentActivity.class);
                commentIntent.putExtra(AppConstants.ClassConstants.FEEDS_DATA, result);
                context.startActivity(commentIntent);
                break;
            case R.id.iv_reuse_count:
                if (AppUtils.isNetworkAvailable(Objects.requireNonNull(context))) {
                    Intent intent = new Intent(context, VideoPreviewActivity.class);
                    intent.putExtra(AppConstants.ClassConstants.FRAGMENT_IS_FROM, AppConstants.ClassConstants.IS_FROM_FEEDS);
                    intent.putExtra(AppConstants.ClassConstants.FEEDS_DATA, result);
                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tv_reuse_count:
                if (result.getTotalReuse() > 0) {
                    BottomSheetPeopleReuseFragment bottomSheetPeopleReuseFragment = new BottomSheetPeopleReuseFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.ClassConstants.VIDEO_TYPE, result.getType());
                    bundle.putString(AppConstants.ClassConstants.FEED_VIDEO_ID, String.valueOf(result.getId()));
                    bundle.putString(AppConstants.ClassConstants.TOTAL_REUSE, String.valueOf(result.getTotalReuse()));
                    bottomSheetPeopleReuseFragment.setArguments(bundle);
                    bottomSheetPeopleReuseFragment.show(fragmentManager, BottomSheetPeopleReuseFragment.class.getCanonicalName());
                }
                break;
            case R.id.iv_user_profile_icon:
                if (!String.valueOf(DataManager.getInstance().getUserId()).equals(result.getUserId())) {

                    ProfileFragment profileFragment = new ProfileFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_FEED_RV);
                    bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.OTHER_USER_PROFILE);
                    bundle.putString(AppConstants.ClassConstants.USER_ID, result.getUserId());
                    profileFragment.setArguments(bundle);
                    ((HomeActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, profileFragment).addToBackStack(ProfileFragment.class.getCanonicalName()).commit();
                } else {
                    ProfileFragment profileFragment = new ProfileFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_FEED_RV);
                    bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.MY_PROFILE);
                    bundle.putString(AppConstants.ClassConstants.USER_ID, result.getUserId());
                    profileFragment.setArguments(bundle);
                    ((HomeActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, profileFragment).addToBackStack(ProfileFragment.class.getCanonicalName()).commit();
                    ((HomeActivity) context).selectBottomNav(false, false, false, true, false);
                }
                break;
            case R.id.iv_feed_options:
                BottomSheetShareFragment bottomSheetShareFragment = new BottomSheetShareFragment();
                Bundle shareBundle = new Bundle();
                shareBundle.putString(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_FEED);
                shareBundle.putParcelable(AppConstants.ClassConstants.VIDEO_DETAILS, result);
                bottomSheetShareFragment.setArguments(shareBundle);
                bottomSheetShareFragment.show(fragmentManager, BottomSheetShareFragment.class.getCanonicalName());
                break;
            case R.id.tv_music_name:
            case R.id.iv_music_icon:
                SuggestedVideoFragment suggestedVideoFragment = new SuggestedVideoFragment();
                Bundle musicBundle = new Bundle();
                musicBundle.putString(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_FEED);
                musicBundle.putInt(AppConstants.ClassConstants.MUSIC_ID, (result.getMusicId()));
                musicBundle.putString(AppConstants.ClassConstants.MUSIC_COLLAB_COUNT, String.valueOf((result.getMusicReuseCount())));
                musicBundle.putString(AppConstants.ClassConstants.MUSIC_NAME, (result.getMusicTitle()));
                musicBundle.putString(AppConstants.ClassConstants.MUSIC_COMPOSER, (result.getMusicDescription()));
                musicBundle.putString(AppConstants.ClassConstants.MUSIC_THUMBNAIL, (result.getMusicThumbnail()));
                musicBundle.putString(AppConstants.ClassConstants.MUSIC_LINK, (result.getMusicUrl()));
                suggestedVideoFragment.setArguments(musicBundle);
                ((HomeActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, suggestedVideoFragment).addToBackStack(SuggestedVideoFragment.class.getCanonicalName()).commit();
                break;
            case R.id.tv_user_name:
                if (!String.valueOf(DataManager.getInstance().getUserId()).equals(result.getUserId())) {

                    ProfileFragment profileFragment = new ProfileFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_FEED_RV);
                    bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.OTHER_USER_PROFILE);
                    bundle.putString(AppConstants.ClassConstants.USER_ID, result.getUserId());
                    profileFragment.setArguments(bundle);
                    ((HomeActivity) context).selectBottomNav(true, false, false, false, false);
                    ((HomeActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, profileFragment).addToBackStack(ProfileFragment.class.getCanonicalName()).commit();
                } else {
                    ProfileFragment profileFragment = new ProfileFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_FEED_RV);
                    bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.MY_PROFILE);
                    bundle.putString(AppConstants.ClassConstants.USER_ID, result.getUserId());
                    profileFragment.setArguments(bundle);
                    ((HomeActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, profileFragment).addToBackStack(ProfileFragment.class.getCanonicalName()).commit();
                    ((HomeActivity) context).selectBottomNav(false, false, false, true, false);
                }
                break;
        }
    }

    @Override
    public void onCommentClicked(com.whizzly.models.comment_list.Result result, int position) {
        Intent intent = new Intent(context, FeedsCommentPreviewActivity.class);
        intent.putExtra(AppConstants.ClassConstants.COMMENT_POSITION, position);
        intent.putExtra(AppConstants.ClassConstants.ON_VIDEO_FEED_COMMENT_CLICKED, commentResultsList);
        context.startActivity(intent);
    }


    private void onDoubleClick() {
        if (result.getIsLiked() == 0) {
            likeDislikeVideo(1);
        }
    }

    private void onSingleClick() {
        context.runOnUiThread(() -> {
            if (!isDisappear) {
                isDisappear = true;
                TransitionManager.beginDelayedTransition(layoutVideoListItemBinding.rlMainView);
                layoutVideoListItemBinding.clLayout.setVisibility(View.GONE);
                layoutVideoListItemBinding.tvLikes.setVisibility(View.GONE);
                layoutVideoListItemBinding.ivLikes.setVisibility(View.GONE);
                layoutVideoListItemBinding.ivReuseCount.setVisibility(View.GONE);
                layoutVideoListItemBinding.tvReuseCount.setVisibility(View.GONE);
                layoutVideoListItemBinding.ivAddComment.setVisibility(View.GONE);
                layoutVideoListItemBinding.rvCommentsList.setVisibility(View.GONE);
                layoutVideoListItemBinding.tvMusicName.setVisibility(View.GONE);
                layoutVideoListItemBinding.ivMusicIcon.setVisibility(View.GONE);
                layoutVideoListItemBinding.ivFeedOptions.setVisibility(View.GONE);
                ((HomeActivity) context).mActivityHomeBinding.llBottom.setVisibility(View.GONE);
                ((HomeActivity) context).hideTopView();
            } else {
                isDisappear = false;
                TransitionManager.beginDelayedTransition(layoutVideoListItemBinding.rlMainView);
                layoutVideoListItemBinding.clLayout.setVisibility(View.VISIBLE);
                layoutVideoListItemBinding.tvLikes.setVisibility(View.VISIBLE);
                layoutVideoListItemBinding.ivLikes.setVisibility(View.VISIBLE);
                layoutVideoListItemBinding.ivReuseCount.setVisibility(View.VISIBLE);
                layoutVideoListItemBinding.tvReuseCount.setVisibility(View.VISIBLE);
                layoutVideoListItemBinding.ivAddComment.setVisibility(View.VISIBLE);
                layoutVideoListItemBinding.rvCommentsList.setVisibility(View.VISIBLE);
                if (null == result.getMusicId()) {
                    layoutVideoListItemBinding.ivMusicIcon.setVisibility(View.GONE);
                    layoutVideoListItemBinding.tvMusicName.setVisibility(View.GONE);
                } else {
                    layoutVideoListItemBinding.ivMusicIcon.setVisibility(View.VISIBLE);
                    layoutVideoListItemBinding.tvMusicName.setVisibility(View.VISIBLE);
                }
                layoutVideoListItemBinding.ivFeedOptions.setVisibility(View.VISIBLE);
                ((HomeActivity) context).mActivityHomeBinding.llBottom.setVisibility(View.VISIBLE);
                ((HomeActivity) context).showTopView();
            }
        });
    }

    public void onSwipeLeft() {
        Intent intent = new Intent(context, HashTagVideosActivity.class);
        intent.putExtra(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_FEEDS);
        intent.putExtra(AppConstants.NetworkConstants.VIDEO_ID, result.getId());
        context.startActivity(intent);
    }

    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {


        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            return super.onSingleTapUp(e);
        }


        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            onSingleClick();
            return super.onSingleTapConfirmed(e);
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            onDoubleClick();
            return super.onDoubleTap(e);
        }

        @Override
        public void onLongPress(MotionEvent e) {
            super.onLongPress(e);
        }
    }

}