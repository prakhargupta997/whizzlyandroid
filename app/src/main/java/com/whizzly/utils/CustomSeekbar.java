package com.whizzly.utils;

import android.content.Context;
import android.util.AttributeSet;

public class CustomSeekbar extends androidx.appcompat.widget.AppCompatSeekBar {
    public CustomSeekbar(Context context) {
        super(context);
    }

    public CustomSeekbar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomSeekbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }




}
