package com.whizzly.utils;

import android.annotation.TargetApi;
import android.os.Build;
import com.google.android.material.appbar.AppBarLayout;
import androidx.core.view.ViewCompat;
import androidx.appcompat.widget.Toolbar;

import com.whizzly.R;

public class ToolbarElevationOffsetListener implements AppBarLayout.OnOffsetChangedListener {
    private final Toolbar mToolbar;
    private float mTargetElevation;

    public ToolbarElevationOffsetListener(Toolbar toolbar) {
        mToolbar = toolbar;
        mTargetElevation = mToolbar.getContext().getResources().getDimension(R.dimen.app_bar_elevation);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        offset = Math.abs(offset);
        mTargetElevation = Math.max(mTargetElevation, appBarLayout.getTargetElevation());
        if (offset >= appBarLayout.getTotalScrollRange() - mToolbar.getHeight()) {
            float flexibleSpace = appBarLayout.getTotalScrollRange() - offset;
            float ratio = 1 - (flexibleSpace / mToolbar.getHeight());
            float elevation = ratio * mTargetElevation;
            setToolbarElevation(elevation);
        } else {
            setToolbarElevation(0);
        }

    }

    private void setToolbarElevation(float targetElevation) {
        if (mToolbar != null)
            ViewCompat.setElevation(mToolbar, targetElevation);
        mToolbar.setAlpha(targetElevation);
    }
}