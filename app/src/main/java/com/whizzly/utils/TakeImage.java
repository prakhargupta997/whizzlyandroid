package com.whizzly.utils;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import android.widget.Toast;

import com.whizzly.interfaces.ImageCallback;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.app.Activity.RESULT_OK;

public class TakeImage {

    private static TakeImage getImage;
    private Activity activity;
    private Uri mImageCaptureUri;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL = 0x003;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL = 0x005;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 0x004;
    private static final int CAMERA_IMAGE_INTENT = 0x001;
    private static final int GALLERY_IMAGE_INTENT = 0x002;
    private ImageCallback callback;

    private TakeImage(Activity activity, ImageCallback callback) {
        this.activity = activity;
        this.callback = callback;
    }

    public static TakeImage getInstance(Activity mActivity, ImageCallback callback)
    {
        getImage = new TakeImage(mActivity,callback);
        return getImage;
    }


    private void fromCamera() {
        if (hasPermission(Manifest.permission.CAMERA, MY_PERMISSIONS_REQUEST_CAMERA)) {
            if (hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL)) {
                Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    try {
                        mImageCaptureUri = FileProvider.getUriForFile(activity, activity.getApplicationContext().getPackageName() + ".provider", createImageFile());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    mImageCaptureUri = Uri.fromFile(new File(getFilePath()));
                }
                intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                try {
                    intentCamera.putExtra("return-data", true);
                    activity.startActivityForResult(intentCamera, CAMERA_IMAGE_INTENT);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }

    private void fromGallery() {
        if (hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE, MY_PERMISSIONS_REQUEST_READ_EXTERNAL)) {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            activity.startActivityForResult(intent, GALLERY_IMAGE_INTENT);
        }
    }


    private boolean hasPermission(String permission, int reqId) {
        int result = ContextCompat.checkSelfPermission(activity, permission);
        if (result == PackageManager.PERMISSION_GRANTED) return true;
        else {
            ActivityCompat.requestPermissions(activity,
                    new String[]{permission}, reqId);
            return false;
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fromCamera();
                } else {
                    Toast.makeText(activity, "Permission denied!", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fromCamera();
                } else {
                    Toast.makeText(activity, "Permission denied!", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fromGallery();
                } else {
                    Toast.makeText(activity, "Permission denied!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_IMAGE_INTENT) {
                if (isSdCardAvailable()) {
                    activity.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
                }
                callback.onSuccess(mImageCaptureUri);
            } else if (requestCode == GALLERY_IMAGE_INTENT) {
                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = null;
                if (selectedImage != null) {
                    c = activity.getContentResolver().query(selectedImage, filePath, null, null, null);
                }
                if (c != null) {
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    String picturePath = c.getString(columnIndex);
                    c.close();
                    File file = new File(picturePath);
                    mImageCaptureUri = Uri.fromFile(file);
                }
                callback.onSuccess(mImageCaptureUri);
            }
        }
    }

    private String getFilePath() {
        File file = new File(AppConstants.APP_IMAGE_FOLDER);
        if (!file.exists()) {
            file.mkdirs();
        }
        return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");

    }

    private boolean isSdCardAvailable() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }
}
