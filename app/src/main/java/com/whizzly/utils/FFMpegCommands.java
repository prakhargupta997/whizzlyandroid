package com.whizzly.utils;

import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.util.Log;

import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpegLoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

public class FFMpegCommands {
    private static FFMpegCommands ourInstance = null;
    private Context context;
    private FFmpeg fFmpeg;

    private FFMpegCommands(Context context) {
        this.context = context;

    }

    public static FFMpegCommands getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new FFMpegCommands(context);
        }
        return ourInstance;
    }

    public void initializeFFmpeg() throws FFmpegNotSupportedException {
        fFmpeg = FFmpeg.getInstance(context);
        fFmpeg.loadBinary(new FFmpegLoadBinaryResponseHandler() {
            @Override
            public void onFailure() {
                Log.e("onFailure: ", "Failed to load ffmpeg");
            }

            @Override
            public void onSuccess() {
                Log.e("onSuccess: ", "Successfully loaded ffmpeg");
            }

            @Override
            public void onStart() {
                Log.e("onStart: ", "Started loading ffmpeg");
            }

            @Override
            public void onFinish() {
                Log.e("onFinish: ", "Finished loading ffmpeg");
            }
        });
    }

    public void initializeFFmpeg(FFMpegCommandListener ffMpegCommandListener) throws FFmpegNotSupportedException {
        fFmpeg = FFmpeg.getInstance(context);
        fFmpeg.loadBinary(new FFmpegLoadBinaryResponseHandler() {
            @Override
            public void onFailure() {
                ffMpegCommandListener.onFailure("");
                Log.e("onFailure: ", "Failed to load ffmpeg");
            }

            @Override
            public void onSuccess() {
                ffMpegCommandListener.onSuccess("");
                Log.e("onSuccess: ", "Successfully loaded ffmpeg");

            }

            @Override
            public void onStart() {
                ffMpegCommandListener.onStart();
                Log.e("onStart: ", "Started loading ffmpeg");
            }

            @Override
            public void onFinish() {
                ffMpegCommandListener.onFinish();
                Log.e("onFinish: ", "Finished loading ffmpeg");
            }
        });
    }

    private int[] getVideoWidth(String path) {
        int[] dimen = new int[2];
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(path);
        int width = Integer.valueOf(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
        int height = Integer.valueOf(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));
        dimen[0] = width;
        dimen[1] = height;
        retriever.release();

        return dimen;
    }

    public void mergeTwoVideos(String fileOne, String fileTwo, String finalFile, FFMpegCommandListener ffMpegCommandListener) throws FFmpegCommandAlreadyRunningException {
        String[] ffmpegCommand = {"-i", fileOne, "-i", fileTwo, "-filter_complex", "hstack=shortest=1", "-c:v", "h264", "-preset", "ultrafast", "-threads", "15", "-thread_type", "frame", "-crf", "20", finalFile};
        Log.e("FFMPEG COMMAND: ", ffmpegCommand.toString());
        fFmpeg.execute(ffmpegCommand, new FFmpegExecuteResponseHandler() {
            @Override
            public void onSuccess(String message) {
                Log.e("onSuccess: ", message);
                ffMpegCommandListener.onSuccess(message);
            }

            @Override
            public void onProgress(String message) {
                Log.e("onProgress: ", message);
                ffMpegCommandListener.onProgress(message);
            }

            @Override
            public void onFailure(String message) {
                Log.e("onFailure: ", message);
                ffMpegCommandListener.onFailure(message);
            }

            @Override
            public void onStart() {
                ffMpegCommandListener.onStart();
            }

            @Override
            public void onFinish() {
                ffMpegCommandListener.onFinish();
            }
        });
    }

    public void mergeThreeVideos(String fileOne, String fileTwo, String fileThree, String finalFile, FFMpegCommandListener ffMpegCommandListener) throws FFmpegCommandAlreadyRunningException {

        int width = 960;

        String[] mergeThreeVideo = {"-i", fileOne, "-i", fileTwo, "-i", fileThree, "-filter_complex", "[0:v]pad=" + width + ":0:(ow-iw)/2[top];[1:v][2:v]hstack[bottom];[top][bottom]vstack=inputs=2[v]", "-map", "[v]", "-c:v", "h264", "-preset", "ultrafast", "-threads", "16", "-thread_type", "frame", "-crf", "18", finalFile};
        Log.e("FFMPEG COMMAND: ", mergeThreeVideo.toString());
        fFmpeg.execute(mergeThreeVideo, new FFmpegExecuteResponseHandler() {
            @Override
            public void onSuccess(String message) {
                Log.e("onSuccess: ", message);
                ffMpegCommandListener.onSuccess(message);
            }

            @Override
            public void onProgress(String message) {
                Log.e("onProgress: ", message);
                ffMpegCommandListener.onProgress(message);
            }

            @Override
            public void onFailure(String message) {
                Log.e("onFailure: ", message);
                ffMpegCommandListener.onFailure(message);
            }

            @Override
            public void onStart() {
                ffMpegCommandListener.onStart();
            }

            @Override
            public void onFinish() {
                ffMpegCommandListener.onFinish();
            }
        });
    }

    public void mergeFourVideos(String fileOne, String fileTwo, String fileThree, String fileFour, String finalFile, FFMpegCommandListener ffMpegCommandListener) throws FFmpegCommandAlreadyRunningException {
        String[] mergeFourVideos = {"-i", fileOne, "-i", fileTwo, "-i", fileThree, "-i", fileFour, "-filter_complex", "[0:v][1:v]hstack[top];[2:v][3:v]hstack[bottom];[top][bottom]vstack=inputs=2[v]", "-map", "[v]", "-c:v", "h264", "-preset", "ultrafast", "-threads", "16", "-thread_type", "frame", "-crf", "20", finalFile};
        Log.e("FFMPEG COMMAND: ", mergeFourVideos.toString());
        fFmpeg.execute(mergeFourVideos, new FFmpegExecuteResponseHandler() {
            @Override
            public void onSuccess(String message) {
                Log.e("onSuccess: ", message);
                ffMpegCommandListener.onSuccess(message);
            }

            @Override
            public void onProgress(String message) {
                Log.e("onProgress: ", message);
                ffMpegCommandListener.onProgress(message);
            }

            @Override
            public void onFailure(String message) {
                Log.e("onFailure: ", message);
                ffMpegCommandListener.onFailure(message);
            }

            @Override
            public void onStart() {
                ffMpegCommandListener.onStart();
            }

            @Override
            public void onFinish() {
                ffMpegCommandListener.onFinish();
            }
        });
    }

    public void mergeFiveVideos(String fileOne, String fileTwo, String fileThree, String fileFour, String fileFive, String finalFile, FFMpegCommandListener ffMpegCommandListener) throws FFmpegCommandAlreadyRunningException {
        int width = 960;
        String[] mergeFiveVideos = {"-i", fileOne, "-i", fileTwo, "-i", fileThree, "-i", fileFour, "-i", fileFive, "-filter_complex", "[0:v]pad=" + width + ":0:(ow-iw)/2[top];[1:v][2:v]hstack[mid];[3:v][4:v]hstack[bottom];[mid][bottom][top]vstack=inputs=3[v]", "-map", "[v]", "-c:v", "h264", "-preset", "ultrafast", "-threads", "16", "-thread_type", "frame", "-crf", "20", finalFile};
        Log.e("FFMPEG COMMAND: ", mergeFiveVideos.toString());
        fFmpeg.execute(mergeFiveVideos, new FFmpegExecuteResponseHandler() {
            @Override
            public void onSuccess(String message) {
                Log.e("onSuccess: ", message);
                ffMpegCommandListener.onSuccess(message);
            }

            @Override
            public void onProgress(String message) {
                Log.e("onProgress: ", message);
                ffMpegCommandListener.onProgress(message);
            }

            @Override
            public void onFailure(String message) {
                Log.e("onFailure: ", message);
                ffMpegCommandListener.onFailure(message);
            }

            @Override
            public void onStart() {
                ffMpegCommandListener.onStart();
            }

            @Override
            public void onFinish() {
                ffMpegCommandListener.onFinish();
            }
        });
    }

    public void mergeSixVideos(String fileOne, String fileTwo, String fileThree, String fileFour, String fileFive, String fileSix, String finalFile, FFMpegCommandListener ffMpegCommandListener) throws FFmpegCommandAlreadyRunningException {
        String[] mergeSixVideos = {"-i", fileOne, "-i", fileTwo, "-i", fileThree, "-i", fileFour, "-i", fileFive, "-i", fileSix, "-filter_complex", "[0:v][1:v]hstack[top];[2:v][3:v]hstack[mid];[4:v][5:v]hstack[bottom];[top][mid][bottom]vstack=inputs=3[v]", "-map", "[v]", "-c:v", "h264", "-preset", "ultrafast", "-threads", "16", "-thread_type", "frame", "-crf", "20", finalFile};
        Log.e("FFMPEG COMMAND: ", mergeSixVideos.toString());
        fFmpeg.execute(mergeSixVideos, new FFmpegExecuteResponseHandler() {
            @Override
            public void onSuccess(String message) {
                Log.e("onSuccess: ", message);
                ffMpegCommandListener.onSuccess(message);
            }

            @Override
            public void onProgress(String message) {
                Log.e("onProgress: ", message);
                ffMpegCommandListener.onProgress(message);
            }

            @Override
            public void onFailure(String message) {
                Log.e("onFailure: ", message);
                ffMpegCommandListener.onFailure(message);
            }

            @Override
            public void onStart() {
                ffMpegCommandListener.onStart();
            }

            @Override
            public void onFinish() {
                ffMpegCommandListener.onFinish();
            }
        });
    }


    public void mergeSongWithVideo(String videoFile, String audioFile, String audioVideoMergedFile, FFMpegCommandListener ffMpegCommandListener) {
        String audioCommand = "-i " + videoFile + " -i " + audioFile + " -c copy -shortest -map 0:v -map 1:a " + audioVideoMergedFile;
        Log.e("FFMPEG COMMAND: ", audioCommand);
        try {
            fFmpeg.execute(audioCommand.split(" "), new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    Log.e("onSuccess: ", message);
                    ffMpegCommandListener.onSuccess(message);
                }

                @Override
                public void onProgress(String message) {
                    Log.e("onProgress: ", message);
                    ffMpegCommandListener.onProgress(message);
                }

                @Override
                public void onFailure(String message) {
                    Log.e("onFailure: ", message);
                    ffMpegCommandListener.onFailure(message);
                }

                @Override
                public void onStart() {
                    ffMpegCommandListener.onStart();
                }

                @Override
                public void onFinish() {
                    ffMpegCommandListener.onFinish();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    public void reduceNoise(String videoFileOne, String outputFile, FFMpegCommandListener ffMpegCommandListener) {
        String command = "-i " + videoFileOne + " -af highpass=f=200,lowpass=f=3000 -threads 23 -c:v h264 -preset ultrafast " + outputFile;
        Log.e("FFMPEG COMMAND: ", command);
        try {
            fFmpeg.execute(command.split(" "), new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    Log.e("onSuccess: ", message);
                    ffMpegCommandListener.onSuccess(message);
                }

                @Override
                public void onProgress(String message) {
                    Log.e("onProgress: ", message);
                    ffMpegCommandListener.onProgress(message);
                }

                @Override
                public void onFailure(String message) {
                    Log.e("onFailure: ", message);
                    ffMpegCommandListener.onFailure(message);
                }

                @Override
                public void onStart() {
                    ffMpegCommandListener.onStart();
                }

                @Override
                public void onFinish() {
                    ffMpegCommandListener.onFinish();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    public void mergeTwoAudioFiles(String audioOne, String audioTwo, String mergedAudio, FFMpegCommandListener ffMpegCommandListener) {
        String mergeTwoAudio = "-i " + audioOne + " -i " + audioTwo + " -filter_complex " +
                "[0]agate=threshold=0.003:makeup=3:release=50:attack=5:knee=4:detection=peak[a];" +
                "[1][0]sidechaincompress=threshold=0.003:makeup=2[b];" +
                "[a][b]amix -preset ultrafast " + mergedAudio;

        Log.e("FFMPEG COMMAND: ", mergeTwoAudio);

        try {
            fFmpeg.execute(mergeTwoAudio.split(" "), new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    Log.e("onSuccess: ", message);
                    ffMpegCommandListener.onSuccess(message);
                }

                @Override
                public void onProgress(String message) {
                    Log.e("onProgress: ", message);
                    ffMpegCommandListener.onProgress(message);
                }

                @Override
                public void onFailure(String message) {
                    Log.e("onFailure: ", message);
                    ffMpegCommandListener.onFailure(message);
                }

                @Override
                public void onStart() {
                    ffMpegCommandListener.onStart();
                }

                @Override
                public void onFinish() {
                    ffMpegCommandListener.onFinish();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    public void mergeThreeAudioFiles(String audioOne, String audioTwo, String audioThree, String mergedAudio, String mergedAudioFile, FFMpegCommandListener ffMpegCommandListener) {
        String mergeTwoAudio = "-i " + audioTwo + " -i " + audioOne + " -filter_complex " +
                "[0]agate=threshold=0.003:makeup=3:release=50:attack=5:knee=4:detection=peak[a];" +
                "[1][0]sidechaincompress=threshold=0.0056:ratio=20:makeup=2[b];" +
                "[a][b]amix -preset ultrafast " + mergedAudio;

        Log.e("FFMPEG COMMAND: ", mergeTwoAudio);

        try {
            fFmpeg.execute(mergeTwoAudio.split(" "), new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    Log.e("onSuccess: ", message);
                    String mergeThreeAudio = "-i " + mergedAudio + " -i " + audioThree + " -filter_complex " +
                            "[0]agate=threshold=0.003:makeup=3:release=50:attack=5:knee=4:detection=peak[a];" +
                            "[1][0]sidechaincompress=threshold=0.0056:ratio=20:makeup=2[b];" +
                            "[a][b]amix -preset ultrafast " + mergedAudioFile;

                    Log.e("FFMPEG COMMAND: ", mergeThreeAudio);

                    try {
                        fFmpeg.execute(mergeThreeAudio.split(" "), new FFmpegExecuteResponseHandler() {
                            @Override
                            public void onSuccess(String message) {
                                Log.e("onSuccess: ", message);
                                ffMpegCommandListener.onSuccess(message);
                            }

                            @Override
                            public void onProgress(String message) {
                                Log.e("onProgress: ", message);
                                ffMpegCommandListener.onProgress(message);
                            }

                            @Override
                            public void onFailure(String message) {
                                Log.e("onFailure: ", message);
                                ffMpegCommandListener.onFailure(message);
                            }

                            @Override
                            public void onStart() {
                                ffMpegCommandListener.onStart();
                            }

                            @Override
                            public void onFinish() {
                                ffMpegCommandListener.onFinish();
                            }
                        });
                    } catch (FFmpegCommandAlreadyRunningException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onProgress(String message) {
                    Log.e("onProgress: ", message);
                    ffMpegCommandListener.onProgress(message);
                }

                @Override
                public void onFailure(String message) {
                    Log.e("onFailure: ", message);
                    ffMpegCommandListener.onFailure(message);
                }

                @Override
                public void onStart() {
                    ffMpegCommandListener.onStart();
                }

                @Override
                public void onFinish() {
                    ffMpegCommandListener.onFinish();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    public void mergeFourAudioFiles(String audioOne, String audioTwo, String audioThree, String audioFour, String mergedAudio, String mergedAudioTwo, String mergedAudioThree, FFMpegCommandListener ffMpegCommandListener) {
        String mergeTwoAudio = "-i " + audioTwo + " -i " + audioOne + " -filter_complex " +
                "[0]agate=threshold=0.003:makeup=3:release=50:attack=5:knee=4:detection=peak[a];" +
                "[1][0]sidechaincompress=threshold=0.0056:ratio=20:makeup=2[b];" +
                "[a][b]amix -preset ultrafast " + mergedAudio;

        Log.e("FFMPEG COMMAND: ", mergeTwoAudio);

        try {
            fFmpeg.execute(mergeTwoAudio.split(" "), new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    Log.e("onSuccess: ", message);
                    String mergeThreeAudio = "-i " + mergedAudio + " -i " + audioThree + " -filter_complex " +
                            "[0]agate=threshold=0.003:makeup=3:release=50:attack=5:knee=4:detection=peak[a];" +
                            "[1][0]sidechaincompress=threshold=0.0056:ratio=20:makeup=2[b];" +
                            "[a][b]amix -preset ultrafast " + mergedAudioTwo;

                    Log.e("FFMPEG COMMAND: ", mergeThreeAudio);

                    try {
                        fFmpeg.execute(mergeThreeAudio.split(" "), new FFmpegExecuteResponseHandler() {
                            @Override
                            public void onSuccess(String message) {
                                Log.e("onSuccess: ", message);
                                String mergeThreeAudio = "-i " + mergedAudioTwo + " -i " + audioFour + " -filter_complex " +
                                        "[0]agate=threshold=0.003:makeup=3:release=50:attack=5:knee=4:detection=peak[a];" +
                                        "[1][0]sidechaincompress=threshold=0.0056:ratio=20:makeup=2[b];" +
                                        "[a][b]amix -preset ultrafast " + mergedAudioThree;

                                Log.e("FFMPEG COMMAND: ", mergeThreeAudio);

                                try {
                                    fFmpeg.execute(mergeThreeAudio.split(" "), new FFmpegExecuteResponseHandler() {
                                        @Override
                                        public void onSuccess(String message) {
                                            Log.e("onSuccess: ", message);
                                            ffMpegCommandListener.onSuccess(message);
                                        }

                                        @Override
                                        public void onProgress(String message) {
                                            Log.e("onProgress: ", message);
                                            ffMpegCommandListener.onProgress(message);
                                        }

                                        @Override
                                        public void onFailure(String message) {
                                            Log.e("onFailure: ", message);
                                            ffMpegCommandListener.onFailure(message);
                                        }

                                        @Override
                                        public void onStart() {
                                            ffMpegCommandListener.onStart();
                                        }

                                        @Override
                                        public void onFinish() {
                                            ffMpegCommandListener.onFinish();
                                        }
                                    });
                                } catch (FFmpegCommandAlreadyRunningException e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onProgress(String message) {
                                Log.e("onProgress: ", message);
                                ffMpegCommandListener.onProgress(message);
                            }

                            @Override
                            public void onFailure(String message) {
                                Log.e("onFailure: ", message);
                                ffMpegCommandListener.onFailure(message);
                            }

                            @Override
                            public void onStart() {
                                ffMpegCommandListener.onStart();
                            }

                            @Override
                            public void onFinish() {
                                ffMpegCommandListener.onFinish();
                            }
                        });
                    } catch (FFmpegCommandAlreadyRunningException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onProgress(String message) {
                    Log.e("onProgress: ", message);
                    ffMpegCommandListener.onProgress(message);
                }

                @Override
                public void onFailure(String message) {
                    Log.e("onFailure: ", message);
                    ffMpegCommandListener.onFailure(message);
                }

                @Override
                public void onStart() {
                    ffMpegCommandListener.onStart();
                }

                @Override
                public void onFinish() {
                    ffMpegCommandListener.onFinish();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    public void mergeFiveAudioFiles(String audioOne, String audioTwo, String audioThree, String audioFour, String audioFive, String mergedAudio, String mergedAudioTwo, String mergedAudioThree, String mergedAudioFour, FFMpegCommandListener ffMpegCommandListener) {
        String mergeTwoAudio = "-i " + audioTwo + " -i " + audioOne + " -filter_complex " +
                "[0]agate=threshold=0.003:makeup=3:release=50:attack=5:knee=4:detection=peak[a];" +
                "[1][0]sidechaincompress=threshold=0.0056:ratio=20:makeup=2[b];" +
                "[a][b]amix -preset ultrafast " + mergedAudio;

        Log.e("FFMPEG COMMAND: ", mergeTwoAudio);

        try {
            fFmpeg.execute(mergeTwoAudio.split(" "), new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    Log.e("onSuccess: ", message);
                    String mergeThreeAudio = "-i " + mergedAudio + " -i " + audioThree + " -filter_complex " +
                            "[0]agate=threshold=0.003:makeup=3:release=50:attack=5:knee=4:detection=peak[a];" +
                            "[1][0]sidechaincompress=threshold=0.0056:ratio=20:makeup=2[b];" +
                            "[a][b]amix -preset ultrafast " + mergedAudioTwo;

                    Log.e("FFMPEG COMMAND: ", mergeThreeAudio);

                    try {
                        fFmpeg.execute(mergeThreeAudio.split(" "), new FFmpegExecuteResponseHandler() {
                            @Override
                            public void onSuccess(String message) {
                                Log.e("onSuccess: ", message);
                                String mergeThreeAudio = "-i " + mergedAudioTwo + " -i " + audioFour + " -filter_complex " +
                                        "[0]agate=threshold=0.003:makeup=3:release=50:attack=5:knee=4:detection=peak[a];" +
                                        "[1][0]sidechaincompress=threshold=0.0056:ratio=20:makeup=2[b];" +
                                        "[a][b]amix -preset ultrafast " + mergedAudioThree;

                                Log.e("FFMPEG COMMAND: ", mergeThreeAudio);

                                try {
                                    fFmpeg.execute(mergeThreeAudio.split(" "), new FFmpegExecuteResponseHandler() {
                                        @Override
                                        public void onSuccess(String message) {
                                            Log.e("onSuccess: ", message);
                                            String mergeThreeAudio = "-i " + mergedAudioThree + " -i " + audioFive + " -filter_complex " +
                                                    "[0]agate=threshold=0.003:makeup=3:release=50:attack=5:knee=4:detection=peak[a];" +
                                                    "[1][0]sidechaincompress=threshold=0.0056:ratio=20:makeup=2[b];" +
                                                    "[a][b]amix -preset ultrafast " + mergedAudioFour;

                                            Log.e("FFMPEG COMMAND: ", mergeThreeAudio);

                                            try {
                                                fFmpeg.execute(mergeThreeAudio.split(" "), new FFmpegExecuteResponseHandler() {
                                                    @Override
                                                    public void onSuccess(String message) {
                                                        Log.e("onSuccess: ", message);
                                                        ffMpegCommandListener.onSuccess(message);
                                                    }

                                                    @Override
                                                    public void onProgress(String message) {
                                                        Log.e("onProgress: ", message);
                                                        ffMpegCommandListener.onProgress(message);
                                                    }

                                                    @Override
                                                    public void onFailure(String message) {
                                                        Log.e("onFailure: ", message);
                                                        ffMpegCommandListener.onFailure(message);
                                                    }

                                                    @Override
                                                    public void onStart() {
                                                        ffMpegCommandListener.onStart();
                                                    }

                                                    @Override
                                                    public void onFinish() {
                                                        ffMpegCommandListener.onFinish();
                                                    }
                                                });
                                            } catch (FFmpegCommandAlreadyRunningException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onProgress(String message) {
                                            Log.e("onProgress: ", message);
                                            ffMpegCommandListener.onProgress(message);
                                        }

                                        @Override
                                        public void onFailure(String message) {
                                            Log.e("onFailure: ", message);
                                            ffMpegCommandListener.onFailure(message);
                                        }

                                        @Override
                                        public void onStart() {
                                            ffMpegCommandListener.onStart();
                                        }

                                        @Override
                                        public void onFinish() {
                                            ffMpegCommandListener.onFinish();
                                        }
                                    });
                                } catch (FFmpegCommandAlreadyRunningException e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onProgress(String message) {
                                Log.e("onProgress: ", message);
                                ffMpegCommandListener.onProgress(message);
                            }

                            @Override
                            public void onFailure(String message) {
                                Log.e("onFailure: ", message);
                                ffMpegCommandListener.onFailure(message);
                            }

                            @Override
                            public void onStart() {
                                ffMpegCommandListener.onStart();
                            }

                            @Override
                            public void onFinish() {
                                ffMpegCommandListener.onFinish();
                            }
                        });
                    } catch (FFmpegCommandAlreadyRunningException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onProgress(String message) {
                    Log.e("onProgress: ", message);
                    ffMpegCommandListener.onProgress(message);
                }

                @Override
                public void onFailure(String message) {
                    Log.e("onFailure: ", message);
                    ffMpegCommandListener.onFailure(message);
                }

                @Override
                public void onStart() {
                    ffMpegCommandListener.onStart();
                }

                @Override
                public void onFinish() {
                    ffMpegCommandListener.onFinish();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    public void mergeSixAudioFiles(String audioOne, String audioTwo, String audioThree, String audioFour, String audioFive, String audioSix, String mergedAudio, String mergedAudioTwo, String mergedAudioThree, String mergedAudioFour
            , String mergedAudioFive, FFMpegCommandListener ffMpegCommandListener) {
        String mergeTwoAudio = "-i " + audioTwo + " -i " + audioOne + " -filter_complex " +
                "[0]agate=threshold=0.003:makeup=3:release=50:attack=5:knee=4:detection=peak[a];" +
                "[1][0]sidechaincompress=threshold=0.0056:ratio=20:makeup=2[b];" +
                "[a][b]amix -preset ultrafast " + mergedAudio;

        Log.e("FFMPEG COMMAND: ", mergeTwoAudio);

        try {
            fFmpeg.execute(mergeTwoAudio.split(" "), new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    Log.e("onSuccess: ", message);
                    String mergeThreeAudio = "-i " + mergedAudio + " -i " + audioThree + " -filter_complex " +
                            "[0]agate=threshold=0.003:makeup=3:release=50:attack=5:knee=4:detection=peak[a];" +
                            "[1][0]sidechaincompress=threshold=0.0056:ratio=20:makeup=2[b];" +
                            "[a][b]amix -preset ultrafast " + mergedAudioTwo;

                    Log.e("FFMPEG COMMAND: ", mergeThreeAudio);

                    try {
                        fFmpeg.execute(mergeThreeAudio.split(" "), new FFmpegExecuteResponseHandler() {
                            @Override
                            public void onSuccess(String message) {
                                Log.e("onSuccess: ", message);
                                String mergeThreeAudio = "-i " + mergedAudioTwo + " -i " + audioFour + " -filter_complex " +
                                        "[0]agate=threshold=0.003:makeup=3:release=50:attack=5:knee=4:detection=peak[a];" +
                                        "[1][0]sidechaincompress=threshold=0.0056:ratio=20:makeup=2[b];" +
                                        "[a][b]amix -preset ultrafast " + mergedAudioThree;

                                Log.e("FFMPEG COMMAND: ", mergeThreeAudio);

                                try {
                                    fFmpeg.execute(mergeThreeAudio.split(" "), new FFmpegExecuteResponseHandler() {
                                        @Override
                                        public void onSuccess(String message) {
                                            Log.e("onSuccess: ", message);
                                            String mergeThreeAudio = "-i " + mergedAudioThree + " -i " + audioFive + " -filter_complex " +
                                                    "[0]agate=threshold=0.003:makeup=3:release=50:attack=5:knee=4:detection=peak[a];" +
                                                    "[1][0]sidechaincompress=threshold=0.0056:ratio=20:makeup=2[b];" +
                                                    "[a][b]amix -preset ultrafast " + mergedAudioFour;

                                            Log.e("FFMPEG COMMAND: ", mergeThreeAudio);

                                            try {
                                                fFmpeg.execute(mergeThreeAudio.split(" "), new FFmpegExecuteResponseHandler() {
                                                    @Override
                                                    public void onSuccess(String message) {
                                                        Log.e("onSuccess: ", message);
                                                        String mergeThreeAudio = "-i " + mergedAudioFour + " -i " + audioSix + " -filter_complex " +
                                                                "[0]agate=threshold=0.003:makeup=3:release=50:attack=5:knee=4:detection=peak[a];" +
                                                                "[1][0]sidechaincompress=threshold=0.0056:ratio=20:makeup=2[b];" +
                                                                "[a][b]amix -preset ultrafast " + mergedAudioFive;

                                                        Log.e("FFMPEG COMMAND: ", mergeThreeAudio);

                                                        try {
                                                            fFmpeg.execute(mergeThreeAudio.split(" "), new FFmpegExecuteResponseHandler() {
                                                                @Override
                                                                public void onSuccess(String message) {
                                                                    Log.e("onSuccess: ", message);
                                                                    ffMpegCommandListener.onSuccess(message);
                                                                }

                                                                @Override
                                                                public void onProgress(String message) {
                                                                    Log.e("onProgress: ", message);
                                                                    ffMpegCommandListener.onProgress(message);
                                                                }

                                                                @Override
                                                                public void onFailure(String message) {
                                                                    Log.e("onFailure: ", message);
                                                                    ffMpegCommandListener.onFailure(message);
                                                                }

                                                                @Override
                                                                public void onStart() {
                                                                    ffMpegCommandListener.onStart();
                                                                }

                                                                @Override
                                                                public void onFinish() {
                                                                    ffMpegCommandListener.onFinish();
                                                                }
                                                            });
                                                        } catch (FFmpegCommandAlreadyRunningException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }

                                                    @Override
                                                    public void onProgress(String message) {
                                                        Log.e("onProgress: ", message);
                                                        ffMpegCommandListener.onProgress(message);
                                                    }

                                                    @Override
                                                    public void onFailure(String message) {
                                                        Log.e("onFailure: ", message);
                                                        ffMpegCommandListener.onFailure(message);
                                                    }

                                                    @Override
                                                    public void onStart() {
                                                        ffMpegCommandListener.onStart();
                                                    }

                                                    @Override
                                                    public void onFinish() {
                                                        ffMpegCommandListener.onFinish();
                                                    }
                                                });
                                            } catch (FFmpegCommandAlreadyRunningException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onProgress(String message) {
                                            Log.e("onProgress: ", message);
                                            ffMpegCommandListener.onProgress(message);
                                        }

                                        @Override
                                        public void onFailure(String message) {
                                            Log.e("onFailure: ", message);
                                            ffMpegCommandListener.onFailure(message);
                                        }

                                        @Override
                                        public void onStart() {
                                            ffMpegCommandListener.onStart();
                                        }

                                        @Override
                                        public void onFinish() {
                                            ffMpegCommandListener.onFinish();
                                        }
                                    });
                                } catch (FFmpegCommandAlreadyRunningException e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onProgress(String message) {
                                Log.e("onProgress: ", message);
                                ffMpegCommandListener.onProgress(message);
                            }

                            @Override
                            public void onFailure(String message) {
                                Log.e("onFailure: ", message);
                                ffMpegCommandListener.onFailure(message);
                            }

                            @Override
                            public void onStart() {
                                ffMpegCommandListener.onStart();
                            }

                            @Override
                            public void onFinish() {
                                ffMpegCommandListener.onFinish();
                            }
                        });
                    } catch (FFmpegCommandAlreadyRunningException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onProgress(String message) {
                    Log.e("onProgress: ", message);
                    ffMpegCommandListener.onProgress(message);
                }

                @Override
                public void onFailure(String message) {
                    Log.e("onFailure: ", message);
                    ffMpegCommandListener.onFailure(message);
                }

                @Override
                public void onStart() {
                    ffMpegCommandListener.onStart();
                }

                @Override
                public void onFinish() {
                    ffMpegCommandListener.onFinish();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }


    public void compressCommand(String inputFile, String outputFile, FFMpegCommandListener ffMpegCommandListener) {
        int[] dimen = getVideoWidth(inputFile);

        if (dimen[0] % 2 != 0) {
            dimen[0] = dimen[0] / 2 - dimen[0] / 2 % 2;
        } else {
            dimen[0] = dimen[0] / 2;
        }

        if (dimen[1] % 2 != 0) {
            dimen[1] = dimen[1] / 2 - dimen[1] / 2 % 2;
        } else {
            dimen[1] = dimen[1] / 2;
        }
        String command = "-i " + inputFile + " -vf scale=" + dimen[0] + ":" + dimen[1] + " -c:a copy -c:v h264 -crf 27 -preset ultrafast " + outputFile;
        Log.e("FFMPEG COMMAND: ", command);
        try {
            fFmpeg.execute(command.split(" "), new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    Log.e("onSuccess: ", message);
                    ffMpegCommandListener.onSuccess(message);
                }

                @Override
                public void onProgress(String message) {
                    Log.e("onProgress: ", message);
                    ffMpegCommandListener.onProgress(message);
                }

                @Override
                public void onFailure(String message) {
                    Log.e("onFailure: ", message);
                    ffMpegCommandListener.onFailure(message);
                }

                @Override
                public void onStart() {
                    ffMpegCommandListener.onStart();
                }

                @Override
                public void onFinish() {
                    ffMpegCommandListener.onFinish();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {

        }
    }

    public void getFrameFromVideo(String inputFile, String outputFile, FFMpegCommandListener ffMpegCommandListener) {
        String command = "-i " + inputFile + " -ss 00:00:00.100 -vframes 1 " + outputFile;
        Log.e("FFMPEG COMMAND: ", command);
        try {
            fFmpeg.execute(command.split(" "), new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    Log.e("onSuccess: ", message);
                    ffMpegCommandListener.onSuccess(message);
                }

                @Override
                public void onProgress(String message) {
                    Log.e("onProgress: ", message);
                    ffMpegCommandListener.onProgress(message);
                }

                @Override
                public void onFailure(String message) {
                    Log.e("onFailure: ", message);
                    ffMpegCommandListener.onFailure(message);
                }

                @Override
                public void onStart() {
                    ffMpegCommandListener.onStart();
                }

                @Override
                public void onFinish() {
                    ffMpegCommandListener.onFinish();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    public void moveAtomWithVideos(String inputFile, String outputFile, FFMpegCommandListener ffMpegCommandListener) throws FFmpegCommandAlreadyRunningException {
        String command = "-i " + inputFile + " -codec copy -map 0 -movflags +faststart " + outputFile;
        Log.e("FFMPEG COMMAND: ", command);
        fFmpeg.execute(command.split(" "), new FFmpegExecuteResponseHandler() {
            @Override
            public void onSuccess(String message) {
                Log.e("onSuccess: ", message);
                ffMpegCommandListener.onSuccess(message);
            }

            @Override
            public void onProgress(String message) {
                Log.e("onProgress: ", message);
                ffMpegCommandListener.onProgress(message);
            }

            @Override
            public void onFailure(String message) {
                Log.e("onFailure: ", message);
                ffMpegCommandListener.onFailure(message);
            }

            @Override
            public void onStart() {
                ffMpegCommandListener.onStart();
            }

            @Override
            public void onFinish() {
                ffMpegCommandListener.onFinish();
            }
        });
    }

    public void compressVideo(String inputFile, String outputFile, FFMpegCommandListener ffMpegCommandListener) {
        String command = "-i " + inputFile + " -vcodec h264 -acodec aac -crf 27 -preset ultrafast " + outputFile;
        Log.e("FFMPEG COMMAND: ", command);
        try {
            fFmpeg.execute(command.split(" "), new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    Log.e("onSuccess: ", message);
                    ffMpegCommandListener.onSuccess(message);
                }

                @Override
                public void onProgress(String message) {
                    Log.e("onProgress: ", message);
                    ffMpegCommandListener.onProgress(message);
                }

                @Override
                public void onFailure(String message) {
                    Log.e("onFailure: ", message);
                    ffMpegCommandListener.onFailure(message);
                }

                @Override
                public void onStart() {
                    ffMpegCommandListener.onStart();
                }

                @Override
                public void onFinish() {
                    ffMpegCommandListener.onFinish();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    public void concatenateVideos(String textFile, String outputFile, FFMpegCommandListener ffMpegCommandListener) {
        String concatenateVideos = "-f concat -safe 0 -protocol_whitelist file -i " + textFile + " -c copy " + outputFile;
        Log.e("FFMPEG COMMAND: ", concatenateVideos);
        try {
            fFmpeg.execute(concatenateVideos.split(" "), new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    ffMpegCommandListener.onSuccess(message);
                    Log.e("onSuccess: ", message);
                }

                @Override
                public void onProgress(String message) {
                    ffMpegCommandListener.onProgress(message);
                    Log.e("onProgress: ", message);
                }

                @Override
                public void onFailure(String message) {
                    ffMpegCommandListener.onFailure(message);
                    Log.e("onFailure: ", message);
                }

                @Override
                public void onStart() {
                    ffMpegCommandListener.onStart();
                }

                @Override
                public void onFinish() {
                    ffMpegCommandListener.onFinish();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }


    public void trimVideo(String inputFile, String outputFile, String startTime, String duration, FFMpegCommandListener ffMpegCommandListener) {
        String trimCommand = "-i " + inputFile + " -ss " + startTime + " -t " + duration + " -c copy -map 0 -movflags +faststart " + outputFile;
        Log.e("FFMPEG COMMAND: ", trimCommand);
        try {
            fFmpeg.execute(trimCommand.split(" "), new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    Log.e("onSuccess: ", message);
                    ffMpegCommandListener.onSuccess(message);
                }

                @Override
                public void onProgress(String message) {
                    Log.e("onProgress: ", message);
                    ffMpegCommandListener.onProgress(message);
                }

                @Override
                public void onFailure(String message) {
                    Log.e("onFailure: ", message);
                    ffMpegCommandListener.onFailure(message);
                }

                @Override
                public void onStart() {
                    ffMpegCommandListener.onStart();
                }

                @Override
                public void onFinish() {
                    ffMpegCommandListener.onFinish();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }


    public void addTextToVideo(String videoFile, String outputFile, String imageFile, FFMpegCommandListener ffMpegCommandListener) {
        String addTextCommand = "-i " + videoFile + " -i " + imageFile + " -filter_complex [1][0]scale2ref[i][m];[m][i]overlay[v] -map [v] -map 0:a -c:v libx264 -c:a copy -crf 27 -preset ultrafast " + outputFile;
        Log.e("FFMPEG COMMAND: ", addTextCommand);
        try {
            fFmpeg.execute(addTextCommand.split(" "), new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    Log.e("onSuccess: ", message);
                    ffMpegCommandListener.onSuccess(message);
                }

                @Override
                public void onProgress(String message) {
                    Log.e("onProgress: ", message);
                    ffMpegCommandListener.onProgress(message);
                }

                @Override
                public void onFailure(String message) {
                    Log.e("onFailure: ", message);
                    ffMpegCommandListener.onFailure(message);
                }

                @Override
                public void onStart() {
                    ffMpegCommandListener.onStart();
                }

                @Override
                public void onFinish() {
                    ffMpegCommandListener.onFinish();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }

    }

    public void scaleVideo(String inputFile, String outputFile, FFMpegCommandListener ffMpegCommandListener) {
        String scaleCommand = "-i " + inputFile + " -vf scale=480:640 -c:v libx264 -c:a copy -crf 27 -preset ultrafast " + outputFile;
        Log.e("FFMPEG COMMAND: ", scaleCommand);
        try {
            fFmpeg.execute(scaleCommand.split(" "), new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    Log.e("onSuccess: ", message);
                    ffMpegCommandListener.onSuccess(message);
                }

                @Override
                public void onProgress(String message) {
                    Log.e("onProgress: ", message);
                    ffMpegCommandListener.onProgress(message);
                }

                @Override
                public void onFailure(String message) {
                    Log.e("onFailure: ", message);
                    ffMpegCommandListener.onFailure(message);
                }

                @Override
                public void onStart() {
                    ffMpegCommandListener.onStart();
                }

                @Override
                public void onFinish() {
                    ffMpegCommandListener.onFinish();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    public void addWaterMark(String waterMarkFile, String inputFile, String outputFile, FFMpegCommandListener ffMpegCommandListener) {
        String[] scaleCommand = {"-i", inputFile, "-i", waterMarkFile, "-filter_complex", "overlay=main_w-overlay_w-10:main_h-overlay_h-10", "-codec:a", "copy", "-preset", "ultrafast", "-async", "1", outputFile};
        Log.e("FFMPEG COMMAND: ", scaleCommand.toString());
        try {
            fFmpeg.execute(scaleCommand, new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    Log.e("onSuccess: ", message);
                    ffMpegCommandListener.onSuccess(message);
                }

                @Override
                public void onProgress(String message) {
                    Log.e("onProgress: ", message);
                    ffMpegCommandListener.onProgress(message);
                }

                @Override
                public void onFailure(String message) {
                    Log.e("onFailure: ", message);
                    ffMpegCommandListener.onFailure(message);
                }

                @Override
                public void onStart() {
                    ffMpegCommandListener.onStart();
                }

                @Override
                public void onFinish() {
                    ffMpegCommandListener.onFinish();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    public void getAudioFile(String videoFile, String audioFile, FFMpegCommandListener ffMpegCommandListener) {
        String command = "-i " + videoFile + " -vn -acodec mp3 -ab 320k -ar 44100 -ac 2 " + audioFile;
        Log.e("FFMPEG COMMAND: ", command);
        try {
            fFmpeg.execute(command.split(" "), new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    Log.e("onSuccess", message);
                    ffMpegCommandListener.onSuccess(message);
                }

                @Override
                public void onProgress(String message) {
                    Log.e("onProgress: ", message);
                    ffMpegCommandListener.onProgress(message);
                }

                @Override
                public void onFailure(String message) {
                    Log.e("onFailure: ", message);
                    ffMpegCommandListener.onFailure(message);
                }

                @Override
                public void onStart() {
                    ffMpegCommandListener.onStart();
                }

                @Override
                public void onFinish() {
                    ffMpegCommandListener.onFinish();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    public void increaseAudioInVideo(String inputVideo, String outputVideo, FFMpegCommandListener ffMpegCommandListener) {
        String command = "-i " + inputVideo + " -filter_complex [0:a]volume=2.5[a1] -map 0:v -map [a1] -preset ultrafast " + outputVideo;
        Log.e("FFMPEG COMMAND: ", command);
        try {
            fFmpeg.execute(command.split(" "), new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    ffMpegCommandListener.onSuccess(message);
                    Log.e("onSuccess", message);
                }

                @Override
                public void onProgress(String message) {
                    ffMpegCommandListener.onProgress(message);
                    Log.e("onProgress", message);
                }

                @Override
                public void onFailure(String message) {
                    ffMpegCommandListener.onFailure(message);
                    Log.e("onFailure", message);
                }

                @Override
                public void onStart() {
                    ffMpegCommandListener.onStart();
                }

                @Override
                public void onFinish() {
                    ffMpegCommandListener.onFinish();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    public void getVideoFile(String videoFileWithAudio, String videoFileWithoutAudio, FFMpegCommandListener ffMpegCommandListener) {
        String command = "-i " + videoFileWithAudio + " -an -vcodec copy " + videoFileWithoutAudio;
        Log.e("FFMPEG COMMAND: ", command);
        try {
            fFmpeg.execute(command.split(" "), new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    ffMpegCommandListener.onSuccess(message);
                    Log.e("onSuccess: ", message);
                }

                @Override
                public void onProgress(String message) {
                    ffMpegCommandListener.onProgress(message);
                    Log.e("onProgress: ", message);
                }

                @Override
                public void onFailure(String message) {
                    ffMpegCommandListener.onFailure(message);
                    Log.e("onFailure: ", message);
                }

                @Override
                public void onStart() {
                    ffMpegCommandListener.onStart();
                }

                @Override
                public void onFinish() {
                    ffMpegCommandListener.onFinish();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    public void increaseAudioVolume(String mAudioFilePath, String audioFile, FFMpegCommandListener ffMpegCommandListener) {
        String command = "-i " + mAudioFilePath + " -af volume=2.5 " + audioFile;
        Log.e("FFMPEG COMMAND: ", command);
        try {
            fFmpeg.execute(command.split(" "), new FFmpegExecuteResponseHandler() {
                @Override
                public void onSuccess(String message) {
                    ffMpegCommandListener.onSuccess(message);
                    Log.e("onSuccess: ", message);
                }

                @Override
                public void onProgress(String message) {
                    ffMpegCommandListener.onProgress(message);
                    Log.e("onProgress: ", message);
                }

                @Override
                public void onFailure(String message) {
                    ffMpegCommandListener.onFailure(message);
                    Log.e("onFailure: ", message);
                }

                @Override
                public void onStart() {
                    ffMpegCommandListener.onStart();
                }

                @Override
                public void onFinish() {
                    ffMpegCommandListener.onFinish();
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }
}