package com.whizzly.utils;

import android.content.Context;
import android.text.TextUtils;

import com.whizzly.models.blockedUsers.BlockedUsersResponseBean;
import com.whizzly.models.change_password.ChangePasswordModel;
import com.whizzly.models.collab.collab_videos.CollabVideosResponseBean;
import com.whizzly.models.collab.collab_videos.allvideos.CategoryVideosResponse;
import com.whizzly.models.comment_delete.CommentDeleteResponse;
import com.whizzly.models.comment_list.CommentListingBean;
import com.whizzly.models.feeds_response.FeedsResponse;
import com.whizzly.models.followUnfollow.FollowUnfollowResponseBean;
import com.whizzly.models.followers_following_response.FollowersResponseBean;
import com.whizzly.models.forgot_password.ResetPasswordBean;
import com.whizzly.models.forgot_password.ValidateOTPBean;
import com.whizzly.models.gifModels.GIFModel;
import com.whizzly.models.like_dislike_response.LikeDislikeResponse;
import com.whizzly.models.likers_list_response.LikersListResponse;
import com.whizzly.models.music_list_response.GetMusicResponseBean;
import com.whizzly.models.notifications.NotificationResponse;
import com.whizzly.models.post.VideoPostResponse;
import com.whizzly.models.post_comment.PostCommentBean;
import com.whizzly.models.privacy_status.PrivacyStatusResponseBean;
import com.whizzly.models.profile.ProfileResponseBean;
import com.whizzly.models.profileVideo.ProfileVideosResponseBean;
import com.whizzly.models.report.ReportResponse;
import com.whizzly.models.reuse_response.ReuseResponse;
import com.whizzly.models.reuse_video.ReuseVideoBean;
import com.whizzly.models.reuse_videos_chart.ReuseVideoChartResponse;
import com.whizzly.models.search.users.UserSearchResponseBean;
import com.whizzly.models.search.videos.VideoSearchResponseBean;
import com.whizzly.models.sign_up_beans.SignUpBean;
import com.whizzly.models.forgot_password.ForgotPasswordBean;
import com.whizzly.models.login_response.LoginResponseBean;
import com.whizzly.models.social_login_response.SocialLoginResponseBean;
import com.whizzly.models.social_signup_response.SocialSignupResponseBean;
import com.whizzly.models.submit_profile.SubmitProfileBean;
import com.whizzly.models.suggested_videos_info.SuggestedVideoInfoResponse;
import com.whizzly.models.validate_user_bean.ValidateUserBean;
import com.whizzly.models.video_details.VideoDetailsResponse;
import com.whizzly.models.video_list_response.VideoListResponseBean;
import com.whizzly.network.RestApi;

import java.util.Calendar;
import java.util.HashMap;

import okhttp3.RequestBody;
import retrofit2.Call;

public class DataManager {
    private static DataManager instance;
    private RestApi apiManager;
    private PreferenceManager mPrefManager;

    private DataManager(Context context) {
        mPrefManager = PreferenceManager.getInstance(context);
    }

    public static DataManager getInstance() {
        if (instance == null) {
            throw new IllegalStateException("Call init() before getInstance()");
        }
        return instance;
    }

    public synchronized static DataManager init(Context context) {
        if (instance == null) {
            instance = new DataManager(context);
        }
        return instance;
    }

    public void initApiManager() {
        apiManager = RestApi.getInstance();
    }

    public String getAccessToken() {
        return mPrefManager.getString(AppConstants.PreferenceConstants.ACCESS_TOKEN);
    }

    public void setAccessToken(String accessToken) {
        mPrefManager.putString(AppConstants.PreferenceConstants.ACCESS_TOKEN, accessToken);
    }

    public int getUserId() {
        return mPrefManager.getInt(AppConstants.PreferenceConstants.USER_ID);
    }

    public void setUserId(int userId) {
        mPrefManager.putInt(AppConstants.PreferenceConstants.USER_ID, userId);
    }

    public void setIsProfileSetUp(int isProfileSetUp) {
        mPrefManager.putInt(AppConstants.PreferenceConstants.IS_PROFILE_SETUP, isProfileSetUp);
    }

    public int isProfileSetUp() {
        return mPrefManager.getInt(AppConstants.PreferenceConstants.IS_PROFILE_SETUP);
    }

    public String getUserType() {
        return mPrefManager.getString(AppConstants.PreferenceConstants.USER_TYPE);
    }

    public void setUserType(String userType) {
        mPrefManager.putString(AppConstants.PreferenceConstants.USER_TYPE, userType);
    }
    public int getLoginOrSignUpTyp(){
        return mPrefManager.getInt(AppConstants.PreferenceConstants.IS_OPEN_THROUGH);
    }
    public void setLoginOrSignUpType(int type){
        mPrefManager.putInt(AppConstants.PreferenceConstants.IS_OPEN_THROUGH,type);
    }

    public Call<ValidateUserBean> hitValidateUsername(String user, String language) {
        return apiManager.hitValidateUsername(user, language);
    }

    public Call<SignUpBean> hitNormalSignUpApi(HashMap<String, Object> signUpDetails) {
        signUpDetails.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitNormalSignUp(signUpDetails);
    }

    public void setLanguage(String lang){
        mPrefManager.putString(AppConstants.PreferenceConstants.LANGUAGE, lang);
    }

    public String getLanguage() {
        if(!TextUtils.isEmpty(mPrefManager.getString(AppConstants.PreferenceConstants.LANGUAGE))){
            return mPrefManager.getString(AppConstants.PreferenceConstants.LANGUAGE);
        }else {
            return "en";
        }
    }

    public Call<LoginResponseBean> hitLoginApi(HashMap<String, Object> loginDetails) {
        loginDetails.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitLoginApi(loginDetails);
    }

    public Call<SocialLoginResponseBean> hitSocialLogin(HashMap<String, Object> stringObjectHashMap) {
        stringObjectHashMap.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitSocialLoginApi(stringObjectHashMap);
    }

    public Call<ForgotPasswordBean> hitForgotPassword(String email, String language) {
        return apiManager.hitForgotPassword(email, language);
    }

    public Call<SubmitProfileBean> hitSubmitProfile(HashMap<String, String> data) {
        data.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitSubmitProfile(data);
    }


    public void saveUserName(String username) {
        mPrefManager.putString(AppConstants.PreferenceConstants.USER_NAME, username);
    }

    public String getUserName() {
        return mPrefManager.getString(AppConstants.PreferenceConstants.USER_NAME);
    }

    public Call<ValidateOTPBean> hitValidateOtp(HashMap<String, Object> enterOtp) {
        enterOtp.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitValidateOtp(enterOtp);
    }

    public Call<ResetPasswordBean> hitResetPasswordApi(HashMap<String, Object> resetPassMap) {
        resetPassMap.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitResetPassword(resetPassMap);
    }

    public Call<SocialSignupResponseBean> hitSocialSignUpApi(HashMap<String, Object> socialDetails) {
        socialDetails.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitSocialSignupApi(socialDetails);
    }

    public String getEmail() {
        return mPrefManager.getString(AppConstants.PreferenceConstants.USER_EMAIL);
    }

    public void setEmail(String email) {
        mPrefManager.putString(AppConstants.PreferenceConstants.USER_EMAIL, email);
    }

    public Call<GetMusicResponseBean> hitGetMusicList(HashMap<String, Object> musicParams) {
        musicParams.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitGetMusicList(musicParams);
    }

    public void setName(String firstName) {
        mPrefManager.putString(AppConstants.PreferenceConstants.NAME, firstName);
    }

    public void setProfilePic(String profileImage) {
        mPrefManager.putString(AppConstants.PreferenceConstants.PROFILE_IMAGE, profileImage);
    }

    public String getName() {
        return mPrefManager.getString(AppConstants.PreferenceConstants.NAME);
    }

    public String getProfilePic() {
        return mPrefManager.getString(AppConstants.PreferenceConstants.PROFILE_IMAGE);
    }

    public Call<VideoListResponseBean> getVideoList(String suggestedVideoParam, String userId, String language) {
        return apiManager.hitGetVideoList(suggestedVideoParam, userId, language);
    }

    public Call<VideoPostResponse> postVideo(RequestBody postVideoBean, String userId) {
        return apiManager.hitPostVideo(postVideoBean, userId);
    }


    public Call<ProfileResponseBean> getProfile(String otherUserId, String language) {
        if (otherUserId.isEmpty()) {
            otherUserId = String.valueOf(getUserId());
        }
        return apiManager.getProfile(String.valueOf(getUserId()), otherUserId, language);
    }

    public Call<ProfileVideosResponseBean> getProfileVideos(HashMap<String, String> params) {
        params.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.getProfileVideos(params);
    }

    public Call<BlockedUsersResponseBean> getBlockedUsersList(HashMap<String, String> params) {
        params.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.getBlockedUsersList(params);
    }

    public Call<BlockedUsersResponseBean> hitBlockUnblockUserApi(HashMap<String, String> params) {
        params.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitBlockUnblockUserApi(params);
    }

    public Call<FollowUnfollowResponseBean> hitFollowUnfollowApi(HashMap<String, String> params) {
        params.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitFollowUnfollowApi(params);
    }


    public Call<VideoPostResponse> postArgument(HashMap<String, String> videoDetails) {
        videoDetails.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitPostArgument(videoDetails);

    }

    public Call<FeedsResponse> hitFeedsApi(HashMap<String, String> feedsQuery) {
        feedsQuery.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitFeedsApi(feedsQuery);
    }

    public Call<LikersListResponse> hitLikersListApi(HashMap<String, String> likersQuery) {
        likersQuery.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitLikersListApi(likersQuery);
    }

    public Call<LikeDislikeResponse> hitLikeDislikeApi(HashMap<String, String> status) {
        status.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitLikeDislikeApi(status);
    }

    public Call<ReuseResponse> hitReuseListApi(HashMap<String, String> reuseQuery) {
        reuseQuery.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitReuseListApi(reuseQuery);
    }

    public Call<ResetPasswordBean> hitLogOutApi(String userId, String language) {
        return apiManager.hitLogOutApi(userId, language);
    }

    public Call<ReuseVideoBean> hitCollabInfoApi(HashMap<String, String> collabQuery) {
        collabQuery.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitCollabInfoApi(collabQuery);
    }

    public Call<CommentListingBean> getCommentListApi(HashMap<String, String> commentQuery) {
        commentQuery.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitCommentListingApi(commentQuery);
    }

    public Call<PostCommentBean> postComment(HashMap<String, String> postCommentRequest) {
        postCommentRequest.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitPostCommentApi(postCommentRequest);
    }

    public Call<FollowersResponseBean> getFollowingListing(HashMap<String, String> hashMap) {
        hashMap.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitFollowingListingApi(hashMap);
    }

    public Call<FollowersResponseBean> getFollowersListing(HashMap<String, String> hashMap) {
        hashMap.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitFollowersListing(hashMap);
    }

    public Call<SubmitProfileBean> hitSupportCenterApi(HashMap<String, String> supportData) {
        supportData.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitSupportCenterApi(supportData);
    }

    public Call<ChangePasswordModel> hitChangePasswordApi(HashMap<String, String> changePassQuery) {
        changePassQuery.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitChangePasswordApi(changePassQuery);
    }

    public Call<com.whizzly.models.search.search_video.VideoSearchResponseBean> hitVideoSearchApi(HashMap<String, String> searchData) {
        searchData.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitVideoSearchApi(searchData);
    }

    public Call<UserSearchResponseBean> hitUserSearchApi(HashMap<String, String> searchData) {
        searchData.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitUserSearchApi(searchData);
    }

    public Call<VideoDetailsResponse> hitVideoDetailsApi(HashMap<String, String> videoQuery) {
        videoQuery.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitVideoDetailsApi(videoQuery);
    }

    public Call<VideoSearchResponseBean> hitExactSearchApi(HashMap<String, String> data) {
        data.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitExactSearchApi(data);
    }

    public Call<CollabVideosResponseBean> hitCategoryRelatedVideosApi(String userId, String language) {
        return apiManager.hitCategoryRelatedVideosApi(userId, language);
    }

    public Call<CategoryVideosResponse> hitViewMoreVideosApi(HashMap<String, String> data) {
        data.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitViewMoreVideosApi(data);
    }


    public Call<ChangePasswordModel> hitEditFeedsApi(HashMap<String, String> videoData) {
        videoData.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitEditFeedsApi(videoData);
    }

    public Call<ReportResponse> hitReportApi(HashMap<String, Object> reportData) {
        reportData.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitReportApi(reportData);
    }

    public Call<ChangePasswordModel> hitDeletePostApi(HashMap<String, String> data) {
        data.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitDeletePostApi(data);
    }

    public void clearPrefernces() {
        mPrefManager.clearAllPrefs();
    }

    public Call<PrivacyStatusResponseBean> hitPrivacyStatusApi(HashMap<String, String> dataForStatus) {
        dataForStatus.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitPrivacyStatusApi(dataForStatus);
    }

    public void saveDeviceToken(String token) {
        mPrefManager.putString(AppConstants.ClassConstants.DEVICE_TOKEN, token);
    }

    public String getDeviceToken() {
        return mPrefManager.getString(AppConstants.ClassConstants.DEVICE_TOKEN);
    }

    public Call<ChangePasswordModel> hitChatMessageNotificationApi(HashMap<String, String> chatData) {
        chatData.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitChatNotificationApi(chatData);
    }

    public Call<NotificationResponse> hitNotificationListingApi(String userId, String language) {
        return apiManager.hitNotificationListingApi(userId, language);
    }

    public Call<ReuseVideoChartResponse> hitReusedVideosChartApi(HashMap<String, String> data) {
        data.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitReusedVideosChartApi(data);
    }

    public Call<PrivacyStatusResponseBean> hitSaveFeedApi(HashMap<String, String> saveFeedData) {
        saveFeedData.put(AppConstants.NetworkConstants.LANGUAGE, DataManager.getInstance().getLanguage());
        return apiManager.hitSaveFeedVideo(saveFeedData);
    }

    public void setNotificationStatus(String status) {
        mPrefManager.putString(AppConstants.ClassConstants.IS_NOTIFICATION_ENABLED,status);
    }

    public String getNotificationStatus() {
        return mPrefManager.getString(AppConstants.ClassConstants.IS_NOTIFICATION_ENABLED);
    }

    public void setRoomId(String roomId) {
        mPrefManager.putString(AppConstants.NetworkConstants.ROOM_ID,roomId);
    }

    public String getRoomId() {
        return mPrefManager.getString(AppConstants.NetworkConstants.ROOM_ID);
    }

    public Call<SuggestedVideoInfoResponse> getSuggestedVideos(HashMap<String, String> suggestedVideoData) {
        return apiManager.getSuggestedVideos(suggestedVideoData);
    }

    public Call<CommentDeleteResponse> hitCommentDeleteApi(HashMap<String, String> commentHashMap) {
        return apiManager.hitDeleteComment(commentHashMap);
    }

    public Call<GIFModel> getGifList(HashMap<String, String> gifQuery){
        return apiManager.getGifsList(gifQuery);
    }

    public void setIsNotchDevice(boolean isNotch) {
        mPrefManager.putBoolean(AppConstants.ClassConstants.IS_NOTCH_DEVICE, isNotch);
    }

    public boolean isNotchDevice(){
        return mPrefManager.getBoolean(AppConstants.ClassConstants.IS_NOTCH_DEVICE);
    }
}
