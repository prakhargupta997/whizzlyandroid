package com.whizzly.utils;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.whizzly.R;
import com.whizzly.databinding.LayoutVideoListItemBinding;
import com.whizzly.interfaces.OnActionDetectListener;
import com.whizzly.models.feeds_response.Result;

import java.util.ArrayList;

public class VideoPlayerRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutVideoListItemBinding mLayoutVideoListItemBinding;
    private ArrayList<Result> mResultList;
    private Activity context;
    private FragmentManager fragmentManager;
    private OnActionDetectListener onActionDetectListener;

    public VideoPlayerRecyclerAdapter(ArrayList<Result> mResultList, Activity context, FragmentManager fragmentManager, OnActionDetectListener onActionDetectListener) {
        this.mResultList = mResultList;
        this.fragmentManager = fragmentManager;
        this.context = context;
        this.onActionDetectListener = onActionDetectListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mLayoutVideoListItemBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.layout_video_list_item, viewGroup, false);
        return new VideoPlayerViewHolder(mLayoutVideoListItemBinding);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        ((VideoPlayerViewHolder) viewHolder).onBind(mResultList.get(i), context, fragmentManager, onActionDetectListener);
    }


    @Override
    public int getItemCount() {
        return mResultList.size();
    }
}