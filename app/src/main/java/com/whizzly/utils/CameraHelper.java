package com.whizzly.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.media.MediaScannerConnection;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import android.view.Surface;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.Whizzly;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.dialog.ErrorDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CameraHelper implements Serializable {
    public final static int CAMERA_INTENT_REQUEST_CODE = 1111;
    public final static int GALLERY_INTENT_REQUEST_CODE = 1112;
    public final static int MAX_IMAGES_LIMIT = 8;
    public final static int IMAGE_TYPE_CAMERA = 1;
    public final static int IMAGE_TYPE_GALLERY = 2;
    public final static int SINGLE_IMAGE_SELECTION = 1;
    public final static int MULTI_IMAGE_SELECTION = 2;
    public final static int MEDIA_TYPE_IMAGE = 1;
    public final static int MEDIA_TYPE_VIDEO = 2;
    private final String FOLDER_NAME = "Whizzly";
    //private int imageSelectionType=SINGLE_IMAGE_SELECTION; //default
    private ImageCallBack imageCallBack;
    private static volatile CameraHelper cameraHelper;
    private Uri photoUri;
    private String mCurrentPhotoPath;
    private Context context;

    private CameraHelper() {
        if (cameraHelper != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }

    public static CameraHelper getInstance() {
        if (cameraHelper == null) {
            synchronized (CameraHelper.class) {
                if (cameraHelper == null)
                    cameraHelper = new CameraHelper();
            }
        }
        return cameraHelper;
    }

    public Bitmap getVideoThumbnail(String url) {
        return ThumbnailUtils.createVideoThumbnail(url, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
    }

    public  Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }




    protected CameraHelper readResolve() {
        return getInstance();
    }

    /**
     * this method is used to open the camera to capture the image
     * @param context activity context from which it is called
     */
    public void openCamera(Activity context,Fragment fragment) {
        if (checkPermissionsForCamera(context,fragment)) {
            Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intentCamera.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                try {
                    File file = createImageFile();
                    photoUri = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", file);
                    mCurrentPhotoPath = "file:" + file.getAbsolutePath();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                photoUri = Uri.fromFile(new File(getFilePath()));
            }
            intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            try {
                intentCamera.putExtra("return-data", true);
                if(fragment==null)
                    ((Activity) context).startActivityForResult(intentCamera, CAMERA_INTENT_REQUEST_CODE);
                else
                    fragment.startActivityForResult(intentCamera, CAMERA_INTENT_REQUEST_CODE);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * this method is used to fetch the image from the gallery
     *
     * @param context activity context from which it is called
     */
    public void openGallery(Context context, Fragment fragment) {
        //Toast.makeText(context,"Unable to open gallery, Please try other options",Toast.LENGTH_SHORT).show();

        try
        {
            if (checkPermissionsForGallery(context,fragment)) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                if(fragment==null)
                    ((Activity) context).startActivityForResult(intent, GALLERY_INTENT_REQUEST_CODE);
                else
                    fragment.startActivityForResult(intent, GALLERY_INTENT_REQUEST_CODE);

            }
        }
        catch (Exception e)
        {
            Toast.makeText(context,context.getResources().getString(R.string.txt_unable_open_gallery),Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }

    private static boolean isFirstTime = false;

    /**
     * this method is used to check runtime permissions required for camera
     *
     * @param context activity context from which it is called
     */
    public boolean checkPermissionsForCamera(Activity context,Fragment fragment) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if(fragment==null)
                    if (ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        ActivityCompat.requestPermissions((context), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_INTENT_REQUEST_CODE);
                    }else{
                        if(!isFirstTime){
                            ActivityCompat.requestPermissions((context), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_INTENT_REQUEST_CODE);
                            isFirstTime = true;
                        }else {
                            ErrorDialog errorDialog = new ErrorDialog();
                            if(context instanceof BaseActivity)
                            errorDialog.show(((BaseActivity)context).getSupportFragmentManager(), ErrorDialog.class.getCanonicalName());
                        }
                    }
                else
                    ActivityCompat.requestPermissions((context), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_INTENT_REQUEST_CODE);
                return false;
            } else
                return true;
        } else
            return true;
    }

    /**
     * this method is used to check runtime permissions required for gallery
     *
     * @param context activity context from which it is called
     */
    public boolean checkPermissionsForGallery(Context context,Fragment fragment) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if(fragment==null)
                    ActivityCompat.requestPermissions(((Activity) context), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, GALLERY_INTENT_REQUEST_CODE);
                else
                    fragment.requestPermissions( new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, GALLERY_INTENT_REQUEST_CODE);

                return false;
            } else
                return true;
        } else
            return true;
    }

    /**
     * this method is called from the activity or fragment's onActivity result method whenever that was called
     *
     * @param requestCode reqcode for which startActivityForResult was called
     * @param resultCode  result code to determine whether result is fetched succesfully or not
     * @param data        intent to get the required data when result is fetched
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CAMERA_INTENT_REQUEST_CODE:
                    Uri imageUri;
                    if (photoUri != null) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            imageUri = Uri.parse(mCurrentPhotoPath);
                        } else {
                            imageUri = photoUri;
                        }
                        if (imageCallBack != null) {
                            if (imageUri != null) {
                                if (new File(imageUri.getPath()).length() > 0) {
                                    MediaScannerConnection.scanFile(context.getApplicationContext(), new String[]{imageUri.getPath()}, new String[]{"image/*"}, null);
                                    imageCallBack.onImageSuccess(imageUri.getPath());
                                } else
                                    imageCallBack.onImageFailure(Whizzly.getInstance().getString(R.string.the_image_you_selected_is_not_a_photo), IMAGE_TYPE_CAMERA);
                            } else
                                throw new RuntimeException(Whizzly.getInstance().getString(R.string.please_set_camera_callback_first_to_start_capture_or_fetch_image));
                        }
                    }
                    break;
                case GALLERY_INTENT_REQUEST_CODE:
                    if (imageCallBack != null)
                    {
                        imageUri = data.getData();

                        if(imageUri!=null)
                        imageUri = Uri.parse(AppUtils.getRealPathFromURI(context, imageUri));
                        else
                            throw new RuntimeException(Whizzly.getInstance().getString(R.string.image_not_vailable));


                        imageCallBack.onImageSuccess(imageUri.getPath());
                    }
                    else
                        throw new RuntimeException(Whizzly.getInstance().getString(R.string.please_set_camera_callback_first_to_start_capture_or_fetch_image));
                    break;
            }
        }
    }

    public ImageCallBack getImageCallBack() {
        return imageCallBack;
    }

    public void setImageCallBack(ImageCallBack imageCallBack) {
        this.imageCallBack = imageCallBack;
    }

    /**
     * this method is used to provide the exact path on which image will be saved when camera
     * is going to capture the image
     * it is for N or above
     *
     * @return file on which image will be saved
     * @throws IOException exception raised during file creation
     */
    private File createImageFile() throws IOException {
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), FOLDER_NAME);
        if (!storageDir.exists())
            storageDir.mkdirs();
        return File.createTempFile(
                imageFileName, ".jpg", storageDir
        );
    }

    /**
     * this method is used to provide the exact path on which image will be saved when camera
     * is going to capture the image
     * it is for M or below
     *
     * @return path on which image will be saved
     */
    private String getFilePath() {
        File file = new File(Environment.getExternalStorageDirectory().toString() + "/" + FOLDER_NAME);
        if (!file.exists()) {
            file.mkdirs();
        }
        return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
    }

    /**
     * interface used to get the callback in the activity or fragment
     * whenever image is fetched successfully or error occured
     */
    public interface ImageCallBack {
        void onImageSuccess(String path);
        void onImageFailure(String errorText, int imageType);
    }

    public void setCOntext(Context cOntext)
    {
        this.context=cOntext;
    }

    /** Check if this device has a camera */
    public boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    /** A safe way to get an instance of the Camera object. */
    public Camera getCameraInstance(int cameraId) {
        Camera c = null;
        try {
            c = Camera.open(cameraId); // attempt to get a Camera instance
        }
        catch (Exception e){
            e.printStackTrace();
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }
    public static Camera.Size getOptimalSize(List<Camera.Size> sizes, int w, int h) {
        // Use a very small tolerance because we want an exact match.
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) w / h;
        if (sizes == null)
            return null;

        Camera.Size optimalSize = null;

        // Start with max value and refine as we iterate over available preview sizes. This is the
        // minimum difference between view and camera height.
        double minDiff = Double.MAX_VALUE;

        // Target view height
        int targetHeight = h;

        // Try to find a preview size that matches aspect ratio and the target view size.
        // Iterate over all available sizes and pick the largest size that can fit in the view and
        // still maintain the aspect ratio.
        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        // Cannot find preview size that matches the aspect ratio, ignore the requirement
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    public static int getCameraDisplayOrientation(Activity activity, int cameraId) {
        Camera.CameraInfo info =
                new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay()
                .getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        return result;
    }

    public interface PermissionGranted {
        void onPermissionGranted(int requestCode);
    }

}
