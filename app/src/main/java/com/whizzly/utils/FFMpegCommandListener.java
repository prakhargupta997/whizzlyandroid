package com.whizzly.utils;

public abstract class FFMpegCommandListener {
    public abstract void onSuccess(String message);

    public abstract void onProgress(String message);

    public abstract void onFailure(String message);

    public abstract void onStart();

    public abstract void onFinish();
}
