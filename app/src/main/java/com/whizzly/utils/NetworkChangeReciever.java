package com.whizzly.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.HashMap;

public class NetworkChangeReciever extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {

        if (checkInternet(context)) {
            while (AppConstants.ClassConstants.mChatPushQueue.size() != 0) {
                HashMap<String, String> mChatHashMap = AppConstants.ClassConstants.mChatPushQueue.poll();
                if (mChatHashMap != null)
                    DataManager.getInstance().hitChatMessageNotificationApi(mChatHashMap);
            }
        }

    }

    boolean checkInternet(Context context) {
        ServiceManager serviceManager = new ServiceManager(context);
        return serviceManager.isNetworkAvailable();
    }

}