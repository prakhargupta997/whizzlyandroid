package com.whizzly.collab_module.viewmodel;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.collab_module.model.SuggestedVideoFragmentModel;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.video_list_response.VideoListResponseBean;
import com.whizzly.utils.AppConstants;

public class SuggestedVideoFragmentViewModel extends ViewModel {

    private SuggestedVideoFragmentModel mSuggestedVideoFragmentModel;
    private RichMediatorLiveData<VideoListResponseBean> mVideoListResponseBeanRichMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private OnClickSendListener onClickSendListener;

    public void setSuggestedVideoFragmentModel(SuggestedVideoFragmentModel mSuggestedVideoFragmentModel) {
        this.mSuggestedVideoFragmentModel = mSuggestedVideoFragmentModel;
    }

    public RichMediatorLiveData<VideoListResponseBean> getmVideoListResponseBeanRichMediatorLiveData() {
        return mVideoListResponseBeanRichMediatorLiveData;
    }

    public void initializeLiveData(Observer<FailureResponse> failureResponseObserver, Observer<Throwable> errorObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        setVideoListResponseBeanRichMediatorLiveData();
    }

    public void setVideoListResponseBeanRichMediatorLiveData() {
        if (mVideoListResponseBeanRichMediatorLiveData == null) {
            mVideoListResponseBeanRichMediatorLiveData = new RichMediatorLiveData<VideoListResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
    }

    public void hitSuggestedListApi(String suggestedVideoParams) {
        mSuggestedVideoFragmentModel.hitSuggestedVideo(mVideoListResponseBeanRichMediatorLiveData, suggestedVideoParams);
    }

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void onBackClick() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.SUGGESTED_BACK_PRESSED);
    }

    public void goToCropper() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.SUGGESTED_CROP_AUDIO);
    }
}
