package com.whizzly.collab_module.viewmodel;

import androidx.lifecycle.ViewModel;

import com.whizzly.collab_module.model.CollabBarModel;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class CollabBarViewModel extends ViewModel {

    private OnClickSendListener mOnClickSendListener;
    private CollabBarModel mCollabBarModel;

    public void setCollabBarModel(CollabBarModel mCollabBarModel){
        this.mCollabBarModel = mCollabBarModel;
    }

    public void setOnClickSendListener(OnClickSendListener mOnClickSendListener){
        this.mOnClickSendListener = mOnClickSendListener;
    }

    public void onDoneClicked(){
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_COLLAB_BAR_DONE);
    }

    public void onCancelClicked(){
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_COLLAB_BAR_CANCEL);
    }


    public void onClickUndo(){
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_UNDO_CLICKED);
    }

    public void onClickRedo(){
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_REDO_CLICKED);
    }

    public void onClickAdd(){
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_ADD_CLICKED);
    }

    public void onClickA(){
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.COLLAB_BAR_A);
        mCollabBarModel.setSelected(AppConstants.ClassConstants.COLLAB_BAR_A);
    }

    public void onClickB(){
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.COLLAB_BAR_B);
        mCollabBarModel.setSelected(AppConstants.ClassConstants.COLLAB_BAR_B);
    }

    public void onClickC(){
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.COLLAB_BAR_C);
        mCollabBarModel.setSelected(AppConstants.ClassConstants.COLLAB_BAR_C);
    }

    public void onClickD(){
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.COLLAB_BAR_D);
        mCollabBarModel.setSelected(AppConstants.ClassConstants.COLLAB_BAR_D);
    }

    public void onClickE(){
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.COLLAB_BAR_E);
        mCollabBarModel.setSelected(AppConstants.ClassConstants.COLLAB_BAR_E);
    }

    public void onClickF(){
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.COLLAB_BAR_F);
        mCollabBarModel.setSelected(AppConstants.ClassConstants.COLLAB_BAR_F);
    }

    public void onClickAll(){
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.COLLAB_BAR_ALL);
        mCollabBarModel.setSelected(AppConstants.ClassConstants.COLLAB_BAR_ALL);
    }
}
