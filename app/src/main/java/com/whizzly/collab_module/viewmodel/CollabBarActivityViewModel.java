package com.whizzly.collab_module.viewmodel;

import androidx.lifecycle.ViewModel;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class CollabBarActivityViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;

    public void setOnClickSendListener(OnClickSendListener onClickSendListener){
        this.onClickSendListener = onClickSendListener;
    }

    public void closeRecording(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CLICK_DISCARD_RECORDING);
    }

    public void saveRecording(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CLICK_APPLY_COLORS);
    }

    public void chooseColor(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CLICK_COLLAB_BAR);
    }

    public void onBackPressed(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CLICK_COLLAB_BACK);
    }

    public void onClickPostVideo(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CLICK_POST_VIDEO);
    }
}
