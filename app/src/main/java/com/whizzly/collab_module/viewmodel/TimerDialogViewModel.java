package com.whizzly.collab_module.viewmodel;

import androidx.lifecycle.ViewModel;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class TimerDialogViewModel extends ViewModel {
    private OnClickSendListener onClickSendListener;

    public void setOnClickSendListener(OnClickSendListener onClickSendListener){
        this.onClickSendListener = onClickSendListener;
    }


    public void onClickFiveSecs(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CLICK_5_SECS);
    }

    public void onClickTenSecs(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CLICK_10_SECS);
    }

    public void onClickFifteenSecs(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CLICK_15_SECS);
    }

    public void onCancelClicked(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CLICK_CANCEL);
    }
}
