package com.whizzly.collab_module.viewmodel;

import androidx.lifecycle.ViewModel;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class ChooseTempleteActivityViewModel extends ViewModel {
    private OnClickSendListener onClickSendListener;

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void onOneTempleteSelected() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CHOOSE_TEMPLETE_ONE);
    }

    public void onTwoTempleteSelected() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CHOOSE_TEMPLETE_TWO);
    }

    public void onThreeTempleteSelected() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CHOOSE_TEMPLETE_THREE);
    }

    public void onFourTempleteSelected() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CHOOSE_TEMPLETE_FOUR);
    }

    public void onFiveTempleteSelected() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CHOOSE_TEMPLETE_FIVE);
    }

    public void onSixTempleteSelected() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CHOOSE_TEMPLETE_SIX);
    }
}
