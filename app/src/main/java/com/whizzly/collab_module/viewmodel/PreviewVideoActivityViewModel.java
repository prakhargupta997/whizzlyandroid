package com.whizzly.collab_module.viewmodel;

import androidx.lifecycle.ViewModel;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class PreviewVideoActivityViewModel extends ViewModel {
    private OnClickSendListener onClickSendListener;

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void onPostClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.PREVIEW_VIDEO_ON_POST_CLICKED);
    }

    public void onCloseClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.PREVIEW_VIDEO_ON_CLOSE_CLICKED);
    }
}
