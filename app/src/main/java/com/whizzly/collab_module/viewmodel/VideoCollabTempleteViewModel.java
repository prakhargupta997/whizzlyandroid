package com.whizzly.collab_module.viewmodel;

import androidx.lifecycle.ViewModel;

import com.whizzly.collab_module.model.VideoCollabTempleteModel;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class VideoCollabTempleteViewModel extends ViewModel {

    private VideoCollabTempleteModel mVideoCollabTempleteModel;
    private OnClickSendListener mOnClickSendListener;

    public void setModel(VideoCollabTempleteModel mVideoCollabTempleteModel){
        this.mVideoCollabTempleteModel = mVideoCollabTempleteModel;
    }

    public void setOnClickSendListener(OnClickSendListener mOnClickSendListener){
        this.mOnClickSendListener = mOnClickSendListener;
    }

    public void onSelectTwoTemp(){
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_TEMP_SELECT_TWO);
    }

    public void onSelectThreeTemp(){
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_TEMP_SELECT_THREE);
    }

    public void onSelectFourTemp(){
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_TEMP_SELECT_FOUR);
    }

    public void onSelectFiveTemp(){
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_TEMP_SELECT_FIVE);
    }

    public void onSelectSixTemp(){
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_TEMP_SELECT_SIX);
    }

    public void onCollabClicked(){
        mVideoCollabTempleteModel.setIsCollabClicked(true);
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_COLLAB_CLICKED);
    }

    public void onArgumentClicked(){
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_ARGUMENT_CLICKED);
    }

    public void onCloseClicked(){
        mOnClickSendListener.onClickSend(AppConstants.ClassConstants.ON_SUGGESTION_CLOSE_CLICKED);
    }
}
