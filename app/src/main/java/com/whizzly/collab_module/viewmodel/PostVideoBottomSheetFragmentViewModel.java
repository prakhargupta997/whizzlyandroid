package com.whizzly.collab_module.viewmodel;

import androidx.lifecycle.ViewModel;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class PostVideoBottomSheetFragmentViewModel extends ViewModel {
    private OnClickSendListener onClickSendListener;

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void onSaveAsPrivateClicked() {
        onClickSendListener.onClickSend(AppConstants
                .ClassConstants.POST_VIDEO_PRIVATE_CLICKED);
    }

    public void onSaveAsPublicClicked() {
        onClickSendListener.onClickSend(AppConstants
                .ClassConstants.POST_VIDEO_PUBLIC_CLICKED);
    }

    public void onFacebookClicked(){
        onClickSendListener.onClickSend(AppConstants
                .ClassConstants.POST_VIDEO_FACEBOOK_CLICKED);
    }

    public void onInstagramClicked(){
        onClickSendListener.onClickSend(AppConstants
                .ClassConstants.POST_VIDEO_INSTAGRAM_CLICKED);
    }
}
