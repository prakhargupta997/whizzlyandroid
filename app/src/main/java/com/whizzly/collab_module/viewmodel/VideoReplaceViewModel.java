package com.whizzly.collab_module.viewmodel;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.reuse_video.ReuseVideoBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class VideoReplaceViewModel extends ViewModel {
    public RichMediatorLiveData<ReuseVideoBean> getmReuseVideoBeanRichMediatorLiveData() {
        return mReuseVideoBeanRichMediatorLiveData;
    }

    private RichMediatorLiveData<ReuseVideoBean> mReuseVideoBeanRichMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;

    public void setGenericObservers(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver){
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureResponseObserver;
        setRichMediatorLiveData();
    }

    private void setRichMediatorLiveData() {
        if(mReuseVideoBeanRichMediatorLiveData == null){
            mReuseVideoBeanRichMediatorLiveData = new RichMediatorLiveData<ReuseVideoBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
    }

    public void hitReuseApi(HashMap<String, String> reuseQuery) {
        DataManager.getInstance().hitCollabInfoApi(reuseQuery).enqueue(new NetworkCallback<ReuseVideoBean>() {
            @Override
            public void onSuccess(ReuseVideoBean reuseVideoBean) {
                mReuseVideoBeanRichMediatorLiveData.setValue(reuseVideoBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mReuseVideoBeanRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mReuseVideoBeanRichMediatorLiveData.setError(t);
            }
        });
    }
}
