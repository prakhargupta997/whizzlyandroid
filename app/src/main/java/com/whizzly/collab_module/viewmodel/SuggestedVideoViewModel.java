package com.whizzly.collab_module.viewmodel;

import androidx.lifecycle.ViewModel;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class SuggestedVideoViewModel extends ViewModel {

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    private OnClickSendListener onClickSendListener;

    public void closeRecording(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_SUGGESTION_CLOSE_CLICKED);
    }

    public void saveRecording(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_SUGGESTION_SAVE_RECORDING);
    }

    public void toggleFlash(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_SUGGESTION_TOGGLE_FLASH);
    }

    public void onCameraFlip(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_SUGGESTION_CAMERA_FLIP);
    }

    public void onTimerClicked(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_SUGGESTION_TIMER_CLICKED);
    }

    public void onGalleryClick(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_SUGGESTION_GALLERY_CLICKED);
    }

    public void removeSegment(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_SUGGESTION_REMOVE_SEGMENT);
    }
}
