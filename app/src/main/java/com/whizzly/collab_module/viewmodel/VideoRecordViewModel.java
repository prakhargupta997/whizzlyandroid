package com.whizzly.collab_module.viewmodel;

import androidx.lifecycle.ViewModel;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class VideoRecordViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;

    public void setOnClickSendListener(OnClickSendListener onClickSendListener){
        this.onClickSendListener = onClickSendListener;
    }

    public void closeRecording(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CLOSE_VIDEO_RECORDING);
    }

    public void saveRecording(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_RECORD_SAVE);
    }

    public void collabBar(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_COLLAB_BAR_CLICKED);
    }

    public void chooseMusic(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CLICK_MUSIC_SELECTION);
    }

    public void removeSegment(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_REMOVE_SEGMENT_CLICKED);
    }

    public void toggleFlash(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_COLLAB_TOGGLE_FLASH);
    }

    public void onCameraFlip(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_COLLAB_FLIP_CAMERA);
    }

    public void onTimerClicked(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_COLLAB_TIMER_CLICK);
    }

    public void onGalleryClick(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_COLLAB_GALLERY_CLICK);
    }

    public void onRecordClicked(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_RECORD_CLICKED);
    }
}
