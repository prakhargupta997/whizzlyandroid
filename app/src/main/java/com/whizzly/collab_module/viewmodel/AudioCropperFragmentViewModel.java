package com.whizzly.collab_module.viewmodel;

import androidx.lifecycle.ViewModel;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class AudioCropperFragmentViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;

    public void setOnSendClickListener(OnClickSendListener onSendClickListener){
        this.onClickSendListener = onSendClickListener;
    }

    public void onResetClicked(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_AUDIO_CROPPER_RESET);
    }

    public void onDoneClicked(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_AUDIO_CROPPER_DONE);
    }
}
