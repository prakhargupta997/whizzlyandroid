package com.whizzly.collab_module.viewmodel;

import androidx.lifecycle.ViewModel;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class ChooseTempleteSizeViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void onSixClick() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_TEMP_SIZE_SIX);
    }

    public void onFiveClick() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_TEMP_SIZE_FIVE);
    }

    public void onFourClick() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_TEMP_SIZE_FOUR);
    }

    public void onThreeClick() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_TEMP_SIZE_THREE);
    }

    public void onSecondClick() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_TEMP_SIZE_TWO);
    }
}
