package com.whizzly.collab_module.viewmodel;

import androidx.lifecycle.ViewModel;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class SuggestionPreviewActivityViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void onCloseClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.SUGGESTION_CLOSE_CLICKED);
    }

    public void onPostClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.SUGGESTION_POST_CLICKED);
    }
}
