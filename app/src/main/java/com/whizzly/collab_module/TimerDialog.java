package com.whizzly.collab_module;

import android.annotation.SuppressLint;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.graphics.Point;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.whizzly.R;
import com.whizzly.collab_module.interfaces.OnTimerDialogTimeSelector;
import com.whizzly.collab_module.viewmodel.TimerDialogViewModel;
import com.whizzly.databinding.DialogTimerBinding;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class TimerDialog extends DialogFragment implements OnClickSendListener {
    private DialogTimerBinding mDialogTimerBinding;
    private TimerDialogViewModel mTimerDialogViewModel;
    private OnTimerDialogTimeSelector onTimerDialogTimeSelector;

    public TimerDialog(){}

    @SuppressLint("ValidFragment")
    public TimerDialog(OnTimerDialogTimeSelector onTimerDialogTimeSelector){
        this.onTimerDialogTimeSelector = onTimerDialogTimeSelector;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mDialogTimerBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_timer, container, false);
        mTimerDialogViewModel = ViewModelProviders.of(this).get(TimerDialogViewModel.class);
        mDialogTimerBinding.setViewModel(mTimerDialogViewModel);
        mTimerDialogViewModel.setOnClickSendListener(this);
        setCancelable(false);
        return mDialogTimerBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        Point size = new Point();

        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);

        int width = size.x;

        window.setLayout((int) (width * 0.75), WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
    }

    @Override
    public void onClickSend(int code) {
        switch (code){
            case AppConstants
                    .ClassConstants.ON_CLICK_5_SECS:
                    onTimerDialogTimeSelector.onTimeSelected(5000);
                dismiss();
                break;
            case AppConstants
                    .ClassConstants.ON_CLICK_10_SECS:
                    onTimerDialogTimeSelector.onTimeSelected(10000);
                dismiss();
                break;
            case AppConstants
                    .ClassConstants.ON_CLICK_15_SECS:
                    onTimerDialogTimeSelector.onTimeSelected(15000);
                dismiss();
                break;
            case AppConstants
                    .ClassConstants.ON_CLICK_CANCEL:
                    dismiss();
                break;
        }
    }
}
