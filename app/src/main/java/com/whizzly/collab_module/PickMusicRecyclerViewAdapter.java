package com.whizzly.collab_module;

import android.content.Context;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.ItemRowSongBinding;
import com.whizzly.models.music_list_response.Result_;
import com.whizzly.utils.AppConstants;

import java.util.ArrayList;

public class PickMusicRecyclerViewAdapter extends RecyclerView.Adapter<PickMusicRecyclerViewAdapter.ViewHolder> {

    public ItemRowSongBinding itemRowSongBinding;
    private OnClickAdapterListener onClickAdapterListener;
    private ArrayList<Result_> musicDetailsBeanArrayList;
    private Context context;

    public PickMusicRecyclerViewAdapter(ArrayList<Result_> musicDetailsBeanArrayList, OnClickAdapterListener onClickAdapterListener, Context context) {
        this.musicDetailsBeanArrayList = musicDetailsBeanArrayList;
        this.onClickAdapterListener = onClickAdapterListener;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        itemRowSongBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_row_song, viewGroup, false);
        return new ViewHolder(itemRowSongBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(musicDetailsBeanArrayList.get(i), i);
        viewHolder.itemRowSongBinding.ivMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_MUSIC_CARD_SUGGEST_CLICK, musicDetailsBeanArrayList.get(i), i);
            }
        });

        viewHolder.itemRowSongBinding.btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_MUSIC_CARD_PLAY_CLICK, musicDetailsBeanArrayList.get(i), i);
            }
        });

        viewHolder.itemRowSongBinding.clSongRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_MUSIC_CARD_EXPAND, musicDetailsBeanArrayList.get(i), i);
            }
        });

        viewHolder.itemRowSongBinding.btnRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_MUSIC_CARD_COLLAB_CLICK, musicDetailsBeanArrayList.get(i), i);
            }
        });
        final float[] firstX = {0};
        int minDistance = 50;
        viewHolder.itemRowSongBinding.clSongRecord.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        firstX[0] = event.getX();
                        return true;
                    case MotionEvent.ACTION_UP:
                        float secondX = event.getX();
                        if (Math.abs(secondX - firstX[0]) > minDistance) {
                            if (secondX < firstX[0]) {
                                onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_MUSIC_CARD_SUGGEST_CLICK, musicDetailsBeanArrayList.get(i), i);
                            }
                        }else {
                            onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_MUSIC_CARD_EXPAND, musicDetailsBeanArrayList.get(i), i);
                        }
                        return true;
                }
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return musicDetailsBeanArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ItemRowSongBinding itemRowSongBinding;

        public ViewHolder(ItemRowSongBinding itemRowSongBinding) {
            super(itemRowSongBinding.getRoot());
            this.itemRowSongBinding = itemRowSongBinding;
        }

        public void bind(Result_ result_, int position) {
            itemRowSongBinding.setVariable(BR.model, result_);
            itemRowSongBinding.executePendingBindings();
        }
    }
}
