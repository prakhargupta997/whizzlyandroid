package com.whizzly.collab_module.beans;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;

public class VideoDataBean implements Parcelable {
    public static final Creator<VideoDataBean> CREATOR = new Creator<VideoDataBean>() {
        @Override
        public VideoDataBean createFromParcel(Parcel source) {
            return new VideoDataBean(source);
        }

        @Override
        public VideoDataBean[] newArray(int size) {
            return new VideoDataBean[size];
        }
    };
    private String videoFramesBean;
    private String mVideoLength, mVideoUri, mVideoFilePath, mAudioFilePath, videoHashtag, videoTitle, mMergedVideoPath;
    private int mMusicId;
    private int mTempleteSize;
    private int frameNumber;
    private int isMandatory;
    private int privacyType;
    private int clickedSocailMedia;
    private int collabId;
    private String videoThumbnailUrl;
    private HashMap<String, ArrayList<CollabBarBean>> mColorBarMap;
    public VideoDataBean() {
    }

    protected VideoDataBean(Parcel in) {
        this.videoFramesBean = in.readString();
        this.mVideoLength = in.readString();
        this.mVideoUri = in.readString();
        this.mVideoFilePath = in.readString();
        this.mAudioFilePath = in.readString();
        this.videoHashtag = in.readString();
        this.videoTitle = in.readString();
        this.mMergedVideoPath = in.readString();
        this.mMusicId = in.readInt();
        this.mTempleteSize = in.readInt();
        this.frameNumber = in.readInt();
        this.isMandatory = in.readInt();
        this.privacyType = in.readInt();
        this.collabId = in.readInt();
        this.videoThumbnailUrl = in.readString();
        this.mColorBarMap = (HashMap<String, ArrayList<CollabBarBean>>) in.readSerializable();
        this.clickedSocailMedia = in.readInt();
    }

    public int getClickedSocailMedia() {
        return clickedSocailMedia;
    }

    public void setClickedSocailMedia(int clickedSocailMedia) {
        this.clickedSocailMedia = clickedSocailMedia;
    }

    public int getCollabId() {
        return collabId;
    }

    public void setCollabId(int collabId) {
        this.collabId = collabId;
    }

    public String getVideoFramesBean() {
        return videoFramesBean;
    }

    public void setVideoFramesBean(String videoFramesBean) {
        this.videoFramesBean = videoFramesBean;
    }

    public String getmVideoLength() {
        return mVideoLength;
    }

    public void setmVideoLength(String mVideoLength) {
        this.mVideoLength = mVideoLength;
    }

    public String getmVideoUri() {
        return mVideoUri;
    }

    public void setmVideoUri(String mVideoUri) {
        this.mVideoUri = mVideoUri;
    }

    public String getmVideoFilePath() {
        return mVideoFilePath;
    }

    public void setmVideoFilePath(String mVideoFilePath) {
        this.mVideoFilePath = mVideoFilePath;
    }

    public String getmAudioFilePath() {
        return mAudioFilePath;
    }

    public void setmAudioFilePath(String mAudioFilePath) {
        this.mAudioFilePath = mAudioFilePath;
    }

    public String getVideoHashtag() {
        return videoHashtag;
    }

    public void setVideoHashtag(String videoHashtag) {
        this.videoHashtag = videoHashtag;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getmMergedVideoPath() {
        return mMergedVideoPath;
    }

    public void setmMergedVideoPath(String mMergedVideoPath) {
        this.mMergedVideoPath = mMergedVideoPath;
    }

    public int getmMusicId() {
        return mMusicId;
    }

    public void setmMusicId(int mMusicId) {
        this.mMusicId = mMusicId;
    }

    public int getmTempleteSize() {
        return mTempleteSize;
    }

    public void setmTempleteSize(int mTempleteSize) {
        this.mTempleteSize = mTempleteSize;
    }

    public int getFrameNumber() {
        return frameNumber;
    }

    public void setFrameNumber(int frameNumber) {
        this.frameNumber = frameNumber;
    }

    public int getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(int isMandatory) {
        this.isMandatory = isMandatory;
    }

    public int getPrivacyType() {
        return privacyType;
    }

    public void setPrivacyType(int privacyType) {
        this.privacyType = privacyType;
    }

    public HashMap<String, ArrayList<CollabBarBean>> getmColorBarMap() {
        return mColorBarMap;
    }

    public void setmColorBarMap(HashMap<String, ArrayList<CollabBarBean>> mColorBarMap) {
        this.mColorBarMap = mColorBarMap;
    }

    public String getVideoThumbnailUrl() {
        return videoThumbnailUrl;
    }

    public void setVideoThumbnailUrl(String videoThumbnailUrl) {
        this.videoThumbnailUrl = videoThumbnailUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.videoFramesBean);
        dest.writeString(this.mVideoLength);
        dest.writeString(this.mVideoUri);
        dest.writeString(this.mVideoFilePath);
        dest.writeString(this.mAudioFilePath);
        dest.writeString(this.videoHashtag);
        dest.writeString(this.videoTitle);
        dest.writeString(this.mMergedVideoPath);
        dest.writeInt(this.mMusicId);
        dest.writeInt(this.mTempleteSize);
        dest.writeInt(this.frameNumber);
        dest.writeInt(this.isMandatory);
        dest.writeInt(this.privacyType);
        dest.writeInt(this.collabId);
        dest.writeString(this.videoThumbnailUrl);
        dest.writeSerializable(this.mColorBarMap);
        dest.writeInt(this.clickedSocailMedia);
    }
}
