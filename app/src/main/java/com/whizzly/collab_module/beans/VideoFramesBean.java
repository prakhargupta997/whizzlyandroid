package com.whizzly.collab_module.beans;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class VideoFramesBean implements Parcelable {
    public static final Creator<VideoFramesBean> CREATOR = new Creator<VideoFramesBean>() {
        @Override
        public VideoFramesBean createFromParcel(Parcel source) {
            return new VideoFramesBean(source);
        }

        @Override
        public VideoFramesBean[] newArray(int size) {
            return new VideoFramesBean[size];
        }
    };
    private Bitmap frame;
    private int colorCode = 0;
    private String description;

    public VideoFramesBean() {
    }


    protected VideoFramesBean(Parcel in) {
        this.frame = in.readParcelable(Bitmap.class.getClassLoader());
        this.colorCode = in.readInt();
        this.description = in.readString();
    }

    public Bitmap getFrame() {
        return frame;
    }

    public void setFrame(Bitmap frame) {
        this.frame = frame;
    }

    public int getColorCode() {
        return colorCode;
    }

    public void setColorCode(int colorCode) {
        this.colorCode = colorCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.frame, flags);
        dest.writeInt(this.colorCode);
        dest.writeString(this.description);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
