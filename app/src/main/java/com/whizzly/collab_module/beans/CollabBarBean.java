package com.whizzly.collab_module.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CollabBarBean implements Parcelable {

    @SerializedName("start_time")
    @Expose
    private float startTime;
    @SerializedName("end_time")
    @Expose
    private float endTime;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("color_code")
    @Expose
    private String colorCode;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("duration_in_millies")
    @Expose
    private long durationInMillies;

    public CollabBarBean() {
    }

    public long getDurationInMillies() {
        return durationInMillies;
    }

    public void setDurationInMillies(long durationInMillies) {
        this.durationInMillies = durationInMillies;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public float getEndTime() {
        return endTime;
    }

    public void setEndTime(float endTime) {
        this.endTime = endTime;
    }

    public float getStartTime() {
        return startTime;
    }

    public void setStartTime(float startTime) {
        this.startTime = startTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(this.startTime);
        dest.writeFloat(this.endTime);
        dest.writeString(this.code);
        dest.writeString(this.colorCode);
        dest.writeString(this.description);
        dest.writeLong(this.durationInMillies);
    }

    protected CollabBarBean(Parcel in) {
        this.startTime = in.readFloat();
        this.endTime = in.readFloat();
        this.code = in.readString();
        this.colorCode = in.readString();
        this.description = in.readString();
        this.durationInMillies = in.readLong();
    }

    public static final Creator<CollabBarBean> CREATOR = new Creator<CollabBarBean>() {
        @Override
        public CollabBarBean createFromParcel(Parcel source) {
            return new CollabBarBean(source);
        }

        @Override
        public CollabBarBean[] newArray(int size) {
            return new CollabBarBean[size];
        }
    };
}
