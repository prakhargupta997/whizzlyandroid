
package com.whizzly.collab_module.collab_model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CollabVideoArray implements Parcelable {

    @SerializedName("video_title")
    @Expose
    private String videoTitle;
    @SerializedName("video_url")
    @Expose
    private String videoUrl;
    @SerializedName("video_thumbnail_url")
    @Expose
    private String videoThumbnailUrl;
    @SerializedName("video_duration")
    @Expose
    private Integer videoDuration;
    @SerializedName("video_frame_no")
    @Expose
    private Integer videoFrameNo;
    @SerializedName("video_is_mandatory")
    @Expose
    private Integer videoIsMandatory;

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoThumbnailUrl() {
        return videoThumbnailUrl;
    }

    public void setVideoThumbnailUrl(String videoThumbnailUrl) {
        this.videoThumbnailUrl = videoThumbnailUrl;
    }

    public Integer getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(Integer videoDuration) {
        this.videoDuration = videoDuration;
    }

    public Integer getVideoFrameNo() {
        return videoFrameNo;
    }

    public void setVideoFrameNo(Integer videoFrameNo) {
        this.videoFrameNo = videoFrameNo;
    }

    public Integer getVideoIsMandatory() {
        return videoIsMandatory;
    }

    public void setVideoIsMandatory(Integer videoIsMandatory) {
        this.videoIsMandatory = videoIsMandatory;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.videoTitle);
        dest.writeString(this.videoUrl);
        dest.writeString(this.videoThumbnailUrl);
        dest.writeValue(this.videoDuration);
        dest.writeValue(this.videoFrameNo);
        dest.writeValue(this.videoIsMandatory);
    }

    public CollabVideoArray() {
    }

    protected CollabVideoArray(Parcel in) {
        this.videoTitle = in.readString();
        this.videoUrl = in.readString();
        this.videoThumbnailUrl = in.readString();
        this.videoDuration = (Integer) in.readValue(Integer.class.getClassLoader());
        this.videoFrameNo = (Integer) in.readValue(Integer.class.getClassLoader());
        this.videoIsMandatory = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<CollabVideoArray> CREATOR = new Creator<CollabVideoArray>() {
        @Override
        public CollabVideoArray createFromParcel(Parcel source) {
            return new CollabVideoArray(source);
        }

        @Override
        public CollabVideoArray[] newArray(int size) {
            return new CollabVideoArray[size];
        }
    };
}
