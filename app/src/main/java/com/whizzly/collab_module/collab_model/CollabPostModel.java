
package com.whizzly.collab_module.collab_model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CollabPostModel implements Parcelable {

    @SerializedName("collab_id")
    @Expose
    private Integer collabId;
    @SerializedName("collab_url")
    @Expose
    private String collabUrl;
    @SerializedName("collab_hashtags")
    @Expose
    private String collabHashtags;
    @SerializedName("collab_duration")
    @Expose
    private Integer collabDuration;
    @SerializedName("collab_music_id")
    @Expose
    private Integer collabMusicId;
    @SerializedName("collab_music_url")
    @Expose
    private String collabMusicUrl;
    @SerializedName("collab_privacy_type_id")
    @Expose
    private Integer collabPrivacyTypeId;
    @SerializedName("collab_thumbnail_url")
    @Expose
    private String collabThumbnailUrl;
    @SerializedName("collab_title")
    @Expose
    private String collabTitle;
    @SerializedName("collab_total_frames")
    @Expose
    private Integer collabTotalFrames;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("collab_watermark_url")
    @Expose
    private String collabWatermarkUrl;
    @SerializedName("collab_additional_info")
    @Expose
    private String collabAdditionalInfo;
    @SerializedName("collab_video_array")
    @Expose
    private List<CollabVideoArray> collabVideoArray = null;

    public Integer getCollabId() {
        return collabId;
    }

    public void setCollabId(Integer collabId) {
        this.collabId = collabId;
    }

    public String getCollabUrl() {
        return collabUrl;
    }

    public void setCollabUrl(String collabUrl) {
        this.collabUrl = collabUrl;
    }

    public String getCollabHashtags() {
        return collabHashtags;
    }

    public void setCollabHashtags(String collabHashtags) {
        this.collabHashtags = collabHashtags;
    }

    public Integer getCollabDuration() {
        return collabDuration;
    }

    public void setCollabDuration(Integer collabDuration) {
        this.collabDuration = collabDuration;
    }

    public Integer getCollabMusicId() {
        return collabMusicId;
    }

    public void setCollabMusicId(Integer collabMusicId) {
        this.collabMusicId = collabMusicId;
    }

    public String getCollabMusicUrl() {
        return collabMusicUrl;
    }

    public void setCollabMusicUrl(String collabMusicUrl) {
        this.collabMusicUrl = collabMusicUrl;
    }

    public Integer getCollabPrivacyTypeId() {
        return collabPrivacyTypeId;
    }

    public void setCollabPrivacyTypeId(Integer collabPrivacyTypeId) {
        this.collabPrivacyTypeId = collabPrivacyTypeId;
    }

    public String getCollabThumbnailUrl() {
        return collabThumbnailUrl;
    }

    public void setCollabThumbnailUrl(String collabThumbnailUrl) {
        this.collabThumbnailUrl = collabThumbnailUrl;
    }

    public String getCollabTitle() {
        return collabTitle;
    }

    public void setCollabTitle(String collabTitle) {
        this.collabTitle = collabTitle;
    }

    public Integer getCollabTotalFrames() {
        return collabTotalFrames;
    }

    public void setCollabTotalFrames(Integer collabTotalFrames) {
        this.collabTotalFrames = collabTotalFrames;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCollabWatermarkUrl() {
        return collabWatermarkUrl;
    }

    public void setCollabWatermarkUrl(String collabWatermarkUrl) {
        this.collabWatermarkUrl = collabWatermarkUrl;
    }

    public String getCollabAdditionalInfo() {
        return collabAdditionalInfo;
    }

    public void setCollabAdditionalInfo(String collabAdditionalInfo) {
        this.collabAdditionalInfo = collabAdditionalInfo;
    }

    public List<CollabVideoArray> getCollabVideoArray() {
        return collabVideoArray;
    }

    public void setCollabVideoArray(List<CollabVideoArray> collabVideoArray) {
        this.collabVideoArray = collabVideoArray;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.collabId);
        dest.writeString(this.collabUrl);
        dest.writeString(this.collabHashtags);
        dest.writeValue(this.collabDuration);
        dest.writeValue(this.collabMusicId);
        dest.writeString(this.collabMusicUrl);
        dest.writeValue(this.collabPrivacyTypeId);
        dest.writeString(this.collabThumbnailUrl);
        dest.writeString(this.collabTitle);
        dest.writeValue(this.collabTotalFrames);
        dest.writeValue(this.userId);
        dest.writeString(this.collabWatermarkUrl);
        dest.writeString(this.collabAdditionalInfo);
        dest.writeList(this.collabVideoArray);
    }

    public CollabPostModel() {
    }

    protected CollabPostModel(Parcel in) {
        this.collabId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.collabUrl = in.readString();
        this.collabHashtags = in.readString();
        this.collabDuration = (Integer) in.readValue(Integer.class.getClassLoader());
        this.collabMusicId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.collabMusicUrl = in.readString();
        this.collabPrivacyTypeId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.collabThumbnailUrl = in.readString();
        this.collabTitle = in.readString();
        this.collabTotalFrames = (Integer) in.readValue(Integer.class.getClassLoader());
        this.userId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.collabWatermarkUrl = in.readString();
        this.collabAdditionalInfo = in.readString();
        this.collabVideoArray = new ArrayList<CollabVideoArray>();
        in.readList(this.collabVideoArray, CollabVideoArray.class.getClassLoader());
    }

    public static final Creator<CollabPostModel> CREATOR = new Creator<CollabPostModel>() {
        @Override
        public CollabPostModel createFromParcel(Parcel source) {
            return new CollabPostModel(source);
        }

        @Override
        public CollabPostModel[] newArray(int size) {
            return new CollabPostModel[size];
        }
    };
}
