package com.whizzly.collab_module;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.ItemRowSuggestedVideoBinding;
import com.whizzly.models.video_list_response.Result;
import com.whizzly.utils.AppConstants;

import java.util.ArrayList;

public class SuggestVideoRecyclerViewAdapter extends RecyclerView.Adapter<SuggestVideoRecyclerViewAdapter.ViewHolder> {

    private ItemRowSuggestedVideoBinding itemRowSuggestedVideoBinding;
    private ArrayList<Result> videoListBeans = new ArrayList<>();
    private OnClickAdapterListener onClickAdapterListener;
    private int width = 0;
    private ViewGroup viewGroup;

    public SuggestVideoRecyclerViewAdapter(ArrayList<Result> videoListBeans, @Nullable OnClickAdapterListener onClickAdapterListener) {
        if (this.videoListBeans != null)
            this.videoListBeans.clear();
        this.videoListBeans = videoListBeans;
        this.onClickAdapterListener = onClickAdapterListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        itemRowSuggestedVideoBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_row_suggested_video, viewGroup, false);
        this.viewGroup = viewGroup;
        return new ViewHolder(itemRowSuggestedVideoBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(videoListBeans.get(i));
        RequestOptions requestOptions = new RequestOptions();
        width = viewGroup.getWidth() / 3;
        viewHolder.itemRowSuggestedVideoBinding.ivVideo.setMinimumHeight(width);
        viewHolder.itemRowSuggestedVideoBinding.ivVideo.setMinimumWidth(width);
        requestOptions.placeholder(R.drawable.ic_choose_image_gallery);
        Glide.with(itemRowSuggestedVideoBinding.getRoot()).load(videoListBeans.get(i).getCollabThumbnail()).apply(requestOptions).into(itemRowSuggestedVideoBinding.ivVideo);
        viewHolder.itemRowSuggestedVideoBinding.ivVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onClickAdapterListener != null)
                    onClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_SUGGESTION_VIDEO_CLICKED, videoListBeans.get(i), i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return videoListBeans.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ItemRowSuggestedVideoBinding itemRowSuggestedVideoBinding;

        public ViewHolder(@NonNull ItemRowSuggestedVideoBinding itemRowSuggestedVideoBinding) {
            super(itemRowSuggestedVideoBinding.getRoot());
            this.itemRowSuggestedVideoBinding = itemRowSuggestedVideoBinding;
        }

        public void bind(Result result) {
            itemRowSuggestedVideoBinding.setVariable(BR.model, result);
            itemRowSuggestedVideoBinding.executePendingBindings();
        }
    }
}
