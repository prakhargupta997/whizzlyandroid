package com.whizzly.collab_module.gallery_trimmer;

import android.annotation.SuppressLint;

import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;

import androidx.databinding.DataBindingUtil;

import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.activity.CollabBarColorActivity;
import com.whizzly.collab_module.activity.PreviewSuggestionActivity;
import com.whizzly.collab_module.VideoFrameRecyclerViewAdapter;
import com.whizzly.collab_module.beans.CollabBarBean;
import com.whizzly.collab_module.beans.VideoFramesBean;
import com.whizzly.databinding.ActivityCollabGalleryTrimmerBinding;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.DownloadVideoModel;
import com.whizzly.self_collab.JoinSelfCollabActivity;
import com.whizzly.self_collab.SelfCollabVideoDataBean;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;
import com.whizzly.utils.FFMpegCommandListener;
import com.whizzly.utils.FFMpegCommands;
import com.whizzly.models.video_list_response.Result;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class CollabGalleryTrimmerActivity extends BaseActivity implements OnClickSendListener {

    public ArrayList<VideoFramesBean> mVideoFrameBeanList = new ArrayList<>();
    private ActivityCollabGalleryTrimmerBinding mActivityCollabGalleryTrimmerBinding;
    private CollabGalleryTrimmerViewModel mCollabGalleryTrimmerViewModel;
    private String imageFile = "";
    private Uri mFileUri;
    private String videoLengthInSecs = "";
    private String mFilePath = "";
    private ExoPlayer mExoplayer;
    private long duration_millisec;
    private Handler handler = new Handler();
    private long videoStart, videoEnd;
    private final Runnable updateProgressAction = () -> updateProgressBar();
    private FFMpegCommands mFFfMpegCommands;
    private int isFrom = 0;
    private ArrayList<DownloadVideoModel> mDownloadVideoModels;
    private Result mResult;
    private int templeteSize;
    private int frameNo = 0;
    private long maxLength = 45000;
    private ArrayList<SelfCollabVideoDataBean> mSelfCollabVideoDataBeanArrayList = new ArrayList<>();
    private String mMusicId = "";
    private int totalNoOfFrames = 0;
    private ArrayList<CollabBarBean> mCollabBarMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityCollabGalleryTrimmerBinding = DataBindingUtil.setContentView(this, R.layout.activity_collab_gallery_trimmer);
        mCollabGalleryTrimmerViewModel = ViewModelProviders.of(this).get(CollabGalleryTrimmerViewModel.class);
        mActivityCollabGalleryTrimmerBinding.setViewModel(mCollabGalleryTrimmerViewModel);
        mCollabGalleryTrimmerViewModel.setOnClickSendListener(this);
        getData();
        mActivityCollabGalleryTrimmerBinding.flProgressBar.setVisibility(View.VISIBLE);
        mFFfMpegCommands = FFMpegCommands.getInstance(this);
        long screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        mActivityCollabGalleryTrimmerBinding.rangeSlider.setRangeChangeListener((view, leftPinIndex, rightPinIndex) -> {
            videoStart = (duration_millisec / screenWidth) * (long)leftPinIndex;
            videoEnd = (duration_millisec / screenWidth) * (long)rightPinIndex;
            mExoplayer.setPlayWhenReady(false);
            mActivityCollabGalleryTrimmerBinding.ivPlayVideo.setImageDrawable(getDrawable(R.drawable.ic_play_circle_outline_white_48dp));
            mExoplayer.seekTo(videoStart);
        });
        new GetFrameTask().execute();
        setRecyclerView();
    }

    private void getData() {
        Intent intent = getIntent();
        if (intent.hasExtra(AppConstants.ClassConstants.IS_FROM)) {
            isFrom = intent.getIntExtra(AppConstants.ClassConstants.IS_FROM, 0);
            if (isFrom == 1) {
                mFilePath = intent.getStringExtra(AppConstants.ClassConstants.VIDEO_FILE_PATH);
                mFileUri = Uri.parse(mFilePath);
                setExoplayer();
            } else if (isFrom == 3) {
                mFilePath = intent.getStringExtra(AppConstants.ClassConstants.VIDEO_FILE_PATH);
                mFileUri = Uri.parse(mFilePath);
                mSelfCollabVideoDataBeanArrayList = (ArrayList<SelfCollabVideoDataBean>) intent.getSerializableExtra(AppConstants.ClassConstants.VIDEO_DATA);
                mMusicId = intent.getStringExtra(AppConstants.ClassConstants.MUSIC_ID);
                totalNoOfFrames = intent.getIntExtra(AppConstants.ClassConstants.FRAMES, 2);
                templeteSize = intent.getIntExtra(AppConstants.ClassConstants.TEMPLETE_SIZE, 2);
                mCollabBarMap = intent.getParcelableArrayListExtra(AppConstants.ClassConstants.COLOR_BAR_DATA);
                setExoplayer();
            } else {
                mFilePath = intent.getStringExtra(AppConstants.ClassConstants.COLLAB_FILE_PATH);
                mResult = intent.getParcelableExtra(AppConstants.ClassConstants.SUGGESTED_VIDEO_RESULT_DATA);
                frameNo = intent.getIntExtra(AppConstants.ClassConstants.FRAME_NUMBER, 0);
                mDownloadVideoModels = (ArrayList<DownloadVideoModel>) intent.getSerializableExtra(AppConstants.ClassConstants.SUGGESTED_VIDEO_DOWNLOAD_VIDEO);
                templeteSize = mResult.getTemplateSize();
                setMaxLength(Long.parseLong(mResult.getCollabInfo().get(0).getVideoDuration()));
                mFileUri = Uri.parse(mFilePath);
                setExoplayer();
            }
        }


    }

    private void setExoplayer() {
        if (mExoplayer == null) {
            mExoplayer = ExoPlayerFactory.newSimpleInstance(this,
                    new DefaultRenderersFactory(this),
                    new DefaultTrackSelector(), new DefaultLoadControl());
            mExoplayer.setRepeatMode(Player.REPEAT_MODE_OFF);
            Uri uri = Uri.parse(mFilePath);
            MediaSource mediaSource = buildMediaSource(uri);
            mActivityCollabGalleryTrimmerBinding.pvVideoFromFiles.setPlayer(mExoplayer);
            mExoplayer.prepare(mediaSource, true, false);

        }
    }

    private void updateProgressBar() {
        long position = mExoplayer == null ? 0 : mExoplayer.getCurrentPosition();
        handler.removeCallbacks(updateProgressAction);
        int playbackState = mExoplayer == null ? Player.STATE_IDLE : mExoplayer.getPlaybackState();
        if (playbackState != Player.STATE_IDLE && playbackState != Player.STATE_ENDED) {
            long delayMs;
            if (mExoplayer.getPlayWhenReady() && playbackState == Player.STATE_READY) {
                delayMs = 1000 - (position % 1000);
                if (delayMs < 200) {
                    delayMs += 1000;
                }
            } else {
                delayMs = 1000;
            }

            if (position >= videoEnd) {
                mExoplayer.setPlayWhenReady(false);
                mExoplayer.seekTo(videoStart);
                mActivityCollabGalleryTrimmerBinding.ivPlayVideo.setImageDrawable(getDrawable(R.drawable.ic_play_circle_outline_white_48dp));
                handler.removeCallbacks(updateProgressAction);
            }

            handler.postDelayed(updateProgressAction, delayMs);
        }
    }

    private MediaSource buildMediaSource(Uri uri) {
        return new ExtractorMediaSource.Factory(
                new DefaultDataSourceFactory(this, "whizzly-app")).
                createMediaSource(uri);
    }

    private void getFramesFromVideo() {
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            retriever.setDataSource(this, mFileUri);
        } catch (Exception e) {
            System.out.println("Exception= " + e);
        }
        mVideoFrameBeanList.clear();
        String duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        duration_millisec = Integer.parseInt(duration); //duration in millisec
        videoLengthInSecs = String.valueOf(duration_millisec / 1000);  //millisec to sec.
        int mHeightView = getResources().getDimensionPixelOffset(R.dimen.frames_video_height);
        int viewWidth = getWindowManager().getDefaultDisplay().getWidth() - 200;
        int numThumbs = (int) Math.floor(((float) viewWidth) / mHeightView);
        final long interval = duration_millisec / numThumbs;
        long duration_second = duration_millisec / 1000;
        if (duration_second > 1) {
            for (int i = 0; i < numThumbs; i++) {
                VideoFramesBean videoFramesBean = new VideoFramesBean();
                videoFramesBean.setFrame(retriever.getFrameAtTime(i * interval, MediaMetadataRetriever.OPTION_CLOSEST_SYNC));
                mVideoFrameBeanList.add(videoFramesBean);
            }
        } else {
            for (int i = 0; i <= numThumbs; i++) {
                VideoFramesBean videoFramesBean = new VideoFramesBean();
                videoFramesBean.setFrame(retriever.getFrameAtTime(i));
                mVideoFrameBeanList.add(videoFramesBean);
            }
        }
        saveBitmap();
        retriever.release();
    }

    private void setRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mActivityCollabGalleryTrimmerBinding.rvVideoFrames.setLayoutManager(linearLayoutManager);
        VideoFrameRecyclerViewAdapter mVideoFrameRecyclerViewAdapter = new VideoFrameRecyclerViewAdapter(mVideoFrameBeanList, 60);
        mActivityCollabGalleryTrimmerBinding.rvVideoFrames.setAdapter(mVideoFrameRecyclerViewAdapter);
    }

    private String getImageDirectory() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "image_" + System.currentTimeMillis() + ".jpeg";
    }

    private void saveBitmap() {
        imageFile = getImageDirectory();
        try (FileOutputStream out = new FileOutputStream(imageFile)) {
            if (mVideoFrameBeanList != null && mVideoFrameBeanList.size() > 0 && mVideoFrameBeanList.get(0).getFrame() != null)
                mVideoFrameBeanList.get(0).getFrame().compress(Bitmap.CompressFormat.PNG, 60, out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setMaxLength(long maxLength) {
        this.maxLength = maxLength * 1000;
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.VIDEO_TRIMMER_ON_CLOSE_RECORDING:
                onBackPressed();
                break;
            case AppConstants.ClassConstants.VIDEO_TRIMMER_ON_SAVE_RECORDING:
                mActivityCollabGalleryTrimmerBinding.ivSaveRecording.setEnabled(false);
                long millis = videoStart;
                mActivityCollabGalleryTrimmerBinding.flProgressBar.setVisibility(View.VISIBLE);
                String startTime = String.format(Locale.getDefault(), "%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                        TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                        TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                Log.e("start_time: ", startTime);
                long duration = videoEnd - videoStart;
                String durationTime = String.format(Locale.getDefault(), "%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(duration),
                        TimeUnit.MILLISECONDS.toMinutes(duration) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(duration)),
                        TimeUnit.MILLISECONDS.toSeconds(duration) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration)));
                Log.e("duration_time: ", durationTime);
                String trimmerVideoPath = getVideoFilePath(this);
                if (mFilePath.contains(" ")) {
                    copyFile(mFilePath, getFileName(Uri.parse(mFilePath)), getCopyVideoPath(this));
                    mFilePath = getCopyVideoPath(this) + getFileName(Uri.parse(mFilePath));
                }

                if (duration > maxLength) {
                    mActivityCollabGalleryTrimmerBinding.flProgressBar.setVisibility(View.GONE);
                    Toast.makeText(this, getString(R.string.txt_cant_trim_more_than_45_sec) + " " + ((int) (maxLength / 1000)) + " " + getString(R.string.txt_seconds), Toast.LENGTH_SHORT).show();
                } else {
                    mFFfMpegCommands.trimVideo(mFilePath, trimmerVideoPath, startTime, durationTime, new FFMpegCommandListener() {
                        @Override
                        public void onSuccess(String message) {
                            scaleVideo(trimmerVideoPath);
                        }

                        @Override
                        public void onProgress(String message) {

                        }

                        @Override
                        public void onFailure(String message) {

                        }

                        @Override
                        public void onStart() {

                        }

                        @Override
                        public void onFinish() {

                        }
                    });
                }
                break;
            case AppConstants.ClassConstants.VIDEO_TRIMMER_ON_PLAY_PAUSE:
                if (mExoplayer.getPlayWhenReady()) {
                    mExoplayer.setPlayWhenReady(false);
                    mActivityCollabGalleryTrimmerBinding.ivPlayVideo.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_circle_outline_white_48dp));
                } else {
                    mExoplayer.setPlayWhenReady(true);
                    handler.post(updateProgressAction);
                    mActivityCollabGalleryTrimmerBinding.ivPlayVideo.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_outline_white_48dp));
                }
                break;
        }
    }

    private void scaleVideo(String inputFile) {
        String finalVideo = getVideoFilePath(this);
        mFFfMpegCommands.scaleVideo(inputFile, finalVideo, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                Log.e("onSuccess: ", message);
                ArrayList<String> strings = new ArrayList<>();
                strings.add(finalVideo);
                mActivityCollabGalleryTrimmerBinding.ivSaveRecording.setEnabled(true);
                mActivityCollabGalleryTrimmerBinding.flProgressBar.setVisibility(View.GONE);
                finish();
                if (isFrom == 1) {
                    Intent intent = new Intent(CollabGalleryTrimmerActivity.this, CollabBarColorActivity.class);
                    intent.putExtra(AppConstants.ClassConstants.VIDEO_FILE_PATH, strings);
                    startActivity(intent);
                } else if (isFrom == 3) {
                    Intent intent = new Intent(CollabGalleryTrimmerActivity.this, JoinSelfCollabActivity.class);
                    mSelfCollabVideoDataBeanArrayList.add(new SelfCollabVideoDataBean());
                    intent.putExtra(AppConstants.ClassConstants.VIDEO_DATA, mSelfCollabVideoDataBeanArrayList);
                    intent.putExtra(AppConstants.ClassConstants.MUSIC_ID, mMusicId);
                    intent.putExtra(AppConstants.ClassConstants.TEMPLETE_SIZE, templeteSize);
                    intent.putExtra(AppConstants.ClassConstants.VIDEO_FILE_PATH, strings);
                    intent.putExtra(AppConstants.ClassConstants.FRAMES, totalNoOfFrames);
                    intent.putExtra(AppConstants.ClassConstants.COLOR_BAR_DATA, mCollabBarMap);
                    intent.putExtra(AppConstants.ClassConstants.COLLAB_FILE_PATH, strings);
                    startActivity(intent);
                } else {
                    Intent previewIntent = new Intent(CollabGalleryTrimmerActivity.this, PreviewSuggestionActivity.class);
                    previewIntent.putExtra(AppConstants.ClassConstants.SUGGESTED_VIDEO_RESULT_DATA, mResult);
                    previewIntent.putExtra(AppConstants.ClassConstants.SUGGESTED_VIDEO_URL, strings);
                    previewIntent.putExtra(AppConstants.ClassConstants.FRAME_NUMBER, frameNo);
                    previewIntent.putExtra(AppConstants.ClassConstants.SUGGESTED_VIDEO_DOWNLOAD_VIDEO, mDownloadVideoModels);
                    previewIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(previewIntent);
                }
            }

            @Override
            public void onProgress(String message) {
                Log.e("onProgress: ", message);
            }

            @Override
            public void onFailure(String message) {
                Log.e("onFailure: ", message);
            }

            @Override
            public void onStart() {

            }

            @Override
            public void onFinish() {

            }
        });
    }

    public String getFileName(Uri uri) {
        String result = null;
        result = uri.getPath();
        int cut = Objects.requireNonNull(result).lastIndexOf('/');
        if (cut != -1) {
            result = result.substring(cut + 1);
        }
        return result;
    }

    private void copyFile(String inputPath, String inputFile, String outputPath) {

        InputStream in = null;
        OutputStream out = null;
        try {

            //create output directory if it doesn't exist
            File dir = new File(outputPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }


            in = new FileInputStream(inputPath);
            out = new FileOutputStream(outputPath + inputFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file (You have now copied the file)
            out.flush();
            out.close();
            out = null;

        } catch (FileNotFoundException fnfe1) {
            Log.e("tag", fnfe1.getMessage());
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        }

    }

    private String getVideoFilePath(Context context) {
        final File dir = context.getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "trimmed_video_" + System.currentTimeMillis() + ".mp4";
    }

    private String getCopyVideoPath(Context context) {
        final File dir = context.getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"));
    }


    @SuppressLint("StaticFieldLeak")
    private class GetFrameTask extends AsyncTask<String, Boolean, Boolean> {
        @Override
        protected Boolean doInBackground(String... strings) {
            getFramesFromVideo();
            return false;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mActivityCollabGalleryTrimmerBinding.ivPlayVideo.setVisibility(View.GONE);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            mActivityCollabGalleryTrimmerBinding.rvVideoFrames.getAdapter().notifyDataSetChanged();
            mActivityCollabGalleryTrimmerBinding.flProgressBar.setVisibility(View.GONE);
            mActivityCollabGalleryTrimmerBinding.ivPlayVideo.setVisibility(View.VISIBLE);
            videoStart = 0;
            videoEnd = duration_millisec;
        }
    }
}
