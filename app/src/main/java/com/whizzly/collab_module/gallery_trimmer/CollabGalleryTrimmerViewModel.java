package com.whizzly.collab_module.gallery_trimmer;

import androidx.lifecycle.ViewModel;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class CollabGalleryTrimmerViewModel extends ViewModel {
    private OnClickSendListener onClickSendListener;

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void saveRecording() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.VIDEO_TRIMMER_ON_SAVE_RECORDING);
    }

    public void onCloseRecording() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.VIDEO_TRIMMER_ON_CLOSE_RECORDING);
    }

    public void onPlayPauseVideo(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.VIDEO_TRIMMER_ON_PLAY_PAUSE);
    }
}
