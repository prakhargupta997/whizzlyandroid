package com.whizzly.collab_module;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.whizzly.models.AddVideoTempleteModel;
import com.whizzly.utils.AppConstants;

import java.util.ArrayList;

public class SuggestedVideoRvAdapter extends RecyclerView.Adapter<SuggestedVideoRvAdapter.ViewHolder> {

    private ArrayList<Object> mRvVideoCollabList;
    private Context context;

    public SuggestedVideoRvAdapter(ArrayList<Object> mRvVideoCollabList, Context context) {
        this.mRvVideoCollabList = mRvVideoCollabList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        return null;
    }

    @Override
    public int getItemViewType(int position) {
        if(mRvVideoCollabList.get(position) instanceof AddVideoTempleteModel){
            return AppConstants.ClassConstants.SUGGESTED_VIDEO_ADD_TEMPLETE;
        }
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return mRvVideoCollabList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
