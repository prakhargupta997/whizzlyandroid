package com.whizzly.collab_module.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.beans.CollabBarBean;
import com.whizzly.collab_module.collab_model.CollabPostModel;
import com.whizzly.collab_module.collab_model.CollabVideoArray;
import com.whizzly.collab_module.fragment.PostVideoBottomSheetFragment;
import com.whizzly.collab_module.viewmodel.PreviewVideoActivityViewModel;
import com.whizzly.databinding.ActivityPreviewVideoBinding;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class PreviewVideoActivity extends BaseActivity implements OnClickSendListener {

    private ActivityPreviewVideoBinding mActivityPreviewVideoBinding;
    private PreviewVideoActivityViewModel mPreviewVideoActivityViewModel;
    private String videoFilePath, videoFileUri, videoLength;
    private ExoPlayer exoPlayer;
    private int mTempleteSize;
    private String mFinalVideoPath = "";
    private String mAudioFilePath = "";
    private int isMandatory;
    private int frameNumber;
    private int mMusicId;
    private String imageFile = "";
    private ArrayList<CollabBarBean> mCollabBarBeans = new ArrayList<>();
    private int mCollabId;
    private String mVideoThumbnailUrl;
    private Rect viewRect;
    private long durationInMillies;
    private PostVideoBottomSheetFragment postVideoBottomSheetFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        mActivityPreviewVideoBinding = DataBindingUtil.setContentView(this, R.layout.activity_preview_video);
        mPreviewVideoActivityViewModel = ViewModelProviders.of(this).get(PreviewVideoActivityViewModel.class);
        mActivityPreviewVideoBinding.setViewModel(mPreviewVideoActivityViewModel);
        mPreviewVideoActivityViewModel.setOnClickSendListener(this);
        getData();
        postVideoBottomSheetFragment = new PostVideoBottomSheetFragment();
        clearStatusBar();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void getData() {
        Intent intent = getIntent();
        mCollabId = intent.getIntExtra(AppConstants.ClassConstants.COLLAB_ID, 0);
        mTempleteSize = intent.getIntExtra(AppConstants.ClassConstants.TEMPLETE_SIZE, 0);
        imageFile = intent.getStringExtra(AppConstants.ClassConstants.FRAME);
        mFinalVideoPath = intent.getStringExtra(AppConstants.ClassConstants.FINAL_VIDEO_FILE_PATH);
        mAudioFilePath = intent.getStringExtra(AppConstants.ClassConstants.SONG_FILE_PATH);
        videoFilePath = intent.getStringExtra(AppConstants.ClassConstants.VIDEO_FILE_PATH);
        durationInMillies = intent.getLongExtra(AppConstants.ClassConstants.DURATION_IN_MILLIES, 0);
        videoFileUri = intent.getStringExtra(AppConstants.ClassConstants.VIDEO_FILE_URI);
        mMusicId = intent.getIntExtra(AppConstants.ClassConstants.MUSIC_ID, 0);
        mVideoThumbnailUrl = intent.getStringExtra(AppConstants.ClassConstants.VIDEO_THUMBNAIL_URL);
        videoLength = intent.getStringExtra(AppConstants.ClassConstants.VIDEO_LENGTH);
        frameNumber = intent.getIntExtra(AppConstants.ClassConstants.FRAME_NUMBER, 0);
        isMandatory = intent.getIntExtra(AppConstants.ClassConstants.IS_MANDATORY, 0);
        mCollabBarBeans = intent.getParcelableArrayListExtra(AppConstants.ClassConstants.COLOR_BAR_DATA);
        setColorBar();
    }

    private void initializePlayer() {
        exoPlayer = ExoPlayerFactory.newSimpleInstance(this,
                new DefaultRenderersFactory(this),
                new DefaultTrackSelector(), new DefaultLoadControl());
        exoPlayer.setPlayWhenReady(true);
        exoPlayer.setRepeatMode(Player.REPEAT_MODE_ONE);
        Uri uri = Uri.parse(mFinalVideoPath);
        MediaSource mediaSource = buildMediaSource(uri);
        mActivityPreviewVideoBinding.pvPreview.setPlayer(exoPlayer);
        exoPlayer.prepare(mediaSource);
    }

    private MediaSource buildMediaSource(Uri uri) {
        return new ExtractorMediaSource.Factory(
                new DefaultDataSourceFactory(this, "whizzly-app")).
                createMediaSource(uri);
    }

    private void discardRecording() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.txt_go_back)
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> discard())
                .setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss())
                .create().show();
    }

    private void discard() {
        File file = new File(mAudioFilePath);
        if (file.delete()) {
            Log.e("deleteRecording: ", "Deleted audio File");
        }

        File file1 = new File(mFinalVideoPath);
        if (file1.delete()) {
            Log.e("deleteRecording: ", "Deleted final File");
        }

        File file2 = new File(videoFilePath);
        if (file2.delete()) {
            Log.e("deleteRecording: ", "Deleted video File");
        }

        finish();
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants
                    .ClassConstants.PREVIEW_VIDEO_ON_CLOSE_CLICKED:
                discardRecording();
                break;
            case AppConstants
                    .ClassConstants.PREVIEW_VIDEO_ON_POST_CLICKED:
                openBottomFragment();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23) {
            initializePlayer();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (exoPlayer != null)
            exoPlayer.setPlayWhenReady(false);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (exoPlayer != null)
            exoPlayer.setPlayWhenReady(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releasePlayer();
    }

    private void releasePlayer() {
        if (exoPlayer != null) {
            exoPlayer.setPlayWhenReady(false);
            exoPlayer.release();
            exoPlayer = null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if ((Util.SDK_INT <= 23 || exoPlayer == null)) {
            initializePlayer();
        }
    }

    private void openBottomFragment() {
        exoPlayer.setPlayWhenReady(false);
        CollabPostModel collabPostModel = new CollabPostModel();
        collabPostModel.setCollabHashtags(null);
        collabPostModel.setCollabPrivacyTypeId(null);
        collabPostModel.setCollabWatermarkUrl(null);
        collabPostModel.setCollabAdditionalInfo(new Gson().toJson(mCollabBarBeans));
        collabPostModel.setCollabMusicUrl(mAudioFilePath);
        collabPostModel.setCollabThumbnailUrl(imageFile);
        collabPostModel.setCollabTitle(null);
        collabPostModel.setCollabTotalFrames(mTempleteSize);
        collabPostModel.setCollabUrl(mFinalVideoPath);
        collabPostModel.setCollabMusicId(mMusicId);
        collabPostModel.setUserId(DataManager.getInstance().getUserId());
        collabPostModel.setCollabDuration(Integer.valueOf(videoLength));
        collabPostModel.setCollabId(0);

        ArrayList<CollabVideoArray> collabVideoArrays = new ArrayList<>();
        CollabVideoArray collabVideoArray = new CollabVideoArray();
        collabVideoArray.setVideoTitle("");
        collabVideoArray.setVideoFrameNo(frameNumber);
        collabVideoArray.setVideoThumbnailUrl(imageFile);
        collabVideoArray.setVideoIsMandatory(isMandatory);
        collabVideoArray.setVideoUrl(mFinalVideoPath);
        collabVideoArray.setVideoDuration(Integer.valueOf(videoLength));
        collabVideoArrays.add(collabVideoArray);
        collabPostModel.setCollabVideoArray(collabVideoArrays);

        Bundle postVideoArguments = new Bundle();
        postVideoArguments.putString("is_from", "1");
        postVideoArguments.putParcelable("post_data", collabPostModel);
        postVideoBottomSheetFragment.setArguments(postVideoArguments);
        postVideoBottomSheetFragment.show(getSupportFragmentManager(), PostVideoBottomSheetFragment.class.getCanonicalName());
    }

    private void setColorBar() {
        setIndeterminateProgressBar();
    }

    private void setIndeterminateProgressBar() {
        mActivityPreviewVideoBinding.llVideoColor.removeAllViews();
        int screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) mActivityPreviewVideoBinding.llVideoColor.getLayoutParams();
        for (int i = 0; i < mCollabBarBeans.size(); i++) {
            View view = new View(this);
            float start = (mCollabBarBeans.get(i).getStartTime() * screenWidth) / durationInMillies;
            float end = (mCollabBarBeans.get(i).getEndTime() * screenWidth) / durationInMillies;
            float width = end - start;
            view.setBackgroundColor(Color.parseColor(mCollabBarBeans.get(i).getColorCode()));
            RelativeLayout.LayoutParams viewLayoutParams = null;
            switch (mCollabBarBeans.get(i).getCode()) {
                case "0":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (mTempleteSize + 1)), this));
                    view.setY((float) dpToPx(20 / (mTempleteSize + 1), this));
                    break;
                case "1":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (mTempleteSize + 1)), this));
                    view.setY((float) dpToPx(2 * (20 / (mTempleteSize + 1)), this));
                    break;
                case "2":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (mTempleteSize + 1)), this));
                    view.setY((float) dpToPx(3 * (20 / (mTempleteSize + 1)), this));
                    break;
                case "3":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (mTempleteSize + 1)), this));
                    view.setY((float) dpToPx(4 * (20 / (mTempleteSize + 1)), this));
                    break;
                case "4":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (mTempleteSize + 1)), this));
                    view.setY((float) dpToPx(5 * (20 / (mTempleteSize + 1)), this));
                    break;
                case "5":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (mTempleteSize + 1)), this));
                    view.setY((float) dpToPx(6 * (20 / (mTempleteSize + 1)), this));
                    break;
                case "6":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (mTempleteSize + 1)), this));
                    view.setY(0);
                    break;
            }
            view.setX(start);
            view.setLayoutParams(viewLayoutParams);
            mActivityPreviewVideoBinding.llVideoColor.addView(view);
        }
    }

    private int dpToPx(float dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }
}
