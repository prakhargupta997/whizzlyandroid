package com.whizzly.collab_module.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.daasuu.camerarecorder.CameraRecordListener;
import com.daasuu.camerarecorder.CameraRecorder;
import com.daasuu.camerarecorder.CameraRecorderBuilder;
import com.daasuu.camerarecorder.LensFacing;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.camera_utils.widget.SampleGLView;
import com.whizzly.collab_module.TimerDialog;
import com.whizzly.collab_module.fragment.PickMusicFragment;
import com.whizzly.collab_module.gallery_trimmer.CollabGalleryTrimmerActivity;
import com.whizzly.collab_module.interfaces.OnSendSongUri;
import com.whizzly.collab_module.interfaces.OnTimerDialogTimeSelector;
import com.whizzly.collab_module.model.VideoRecordModel;
import com.whizzly.collab_module.viewmodel.VideoRecordViewModel;
import com.whizzly.databinding.ActivityVideoRecordBinding;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

import java.io.File;
import java.util.ArrayList;


public class VideoRecordActivity extends BaseActivity implements ActivityCompat.OnRequestPermissionsResultCallback, OnClickSendListener, OnTimerDialogTimeSelector, OnSendSongUri {
    public static final Integer MAX_VIDEO_LENGTH = 30000;
    private static boolean isFirstTime = false;
    protected CameraRecorder cameraRecorder;
    protected LensFacing lensFacing = LensFacing.FRONT;
    protected int videoWidth = 480;
    protected int videoHeight = 640;
    private VideoRecordModel mVideoRecordModel;
    private boolean mIsRecordingVideo;
    private String mNextVideoAbsolutePath;
    private ExoPlayer mExoPlayer;
    private int mMusicId;
    private ArrayList<String> mVideoArrayList;
    private ActivityVideoRecordBinding mActivityVideoRecordBinding;
    private ArrayList<Long> mProgress = new ArrayList<>();
    private String videoFromFileManager;
    private SampleGLView sampleGLView;
    private String audioUrl;
    private boolean isRecordedWithHeadPhones = false;
    private int currentVolume = -1;
    private int templateSize;

    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityVideoRecordBinding = DataBindingUtil.setContentView(this, R.layout.activity_video_record);
        VideoRecordViewModel mVideoRecordViewModel = ViewModelProviders.of(this).get(VideoRecordViewModel.class);
        mVideoRecordModel = new VideoRecordModel();
        mActivityVideoRecordBinding.setViewModel(mVideoRecordViewModel);
        mVideoArrayList = new ArrayList<>();
        mActivityVideoRecordBinding.setModel(mVideoRecordModel);
        templateSize = getIntent().getIntExtra(AppConstants.TEMPLATE_SIZE, 0);
        mVideoRecordViewModel.setOnClickSendListener(this);
        recordUsingPressedButton();
        setProgressBar();
    }

    private boolean getMicrophoneAvailable() {
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        return audioManager.isWiredHeadsetOn();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (checkPermissionsForCamera(this)) {
            setUpCamera();
        }
        if (!getMicrophoneAvailable()) {
            AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
            currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, (int) (audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) * 0.75), AudioManager.FLAG_PLAY_SOUND);
        }
    }

    private void releaseCamera() {
        if (sampleGLView != null) {
            sampleGLView.onPause();
        }

        if (cameraRecorder != null) {
            cameraRecorder.stop();
            cameraRecorder.release();
            cameraRecorder = null;
        }

        if (sampleGLView != null) {
            mActivityVideoRecordBinding.camera.removeView(sampleGLView);
            sampleGLView = null;
        }
    }

    private void setUpCameraView() {
        runOnUiThread(() -> {
            FrameLayout frameLayout = mActivityVideoRecordBinding.camera;
            frameLayout.removeAllViews();
            sampleGLView = null;
            sampleGLView = new SampleGLView(getApplicationContext());
            sampleGLView.setTouchListener((event, width, height) -> {
                if (cameraRecorder == null) return;
                cameraRecorder.changeManualFocusPoint(event.getX(), event.getY(), width, height);
            });
            frameLayout.addView(sampleGLView);
        });
    }

    private void setUpCamera() {
        setUpCameraView();
        setCamera();
        cameraRecorder = new CameraRecorderBuilder(this, sampleGLView)
                .cameraRecordListener(new CameraRecordListener() {
                    @Override
                    public void onGetFlashSupport(boolean flashSupport) {

                    }

                    @Override
                    public void onRecordComplete() {
                        mVideoRecordModel.setIsRemoveVisible(true);
                        mIsRecordingVideo = false;
                        mVideoRecordModel.setIsVideoStarted(true);
                        mVideoRecordModel.setIsRecordPressed(false);
                        mVideoArrayList.add(mNextVideoAbsolutePath);
                        mActivityVideoRecordBinding.pbRecordingProgress.pause();
                        mActivityVideoRecordBinding.pbRecordingProgress.addDivider();
                        mProgress.add(mActivityVideoRecordBinding.pbRecordingProgress.progressInMillies());
                        mNextVideoAbsolutePath = null;
                    }

                    @Override
                    public void onRecordStart() {
                        isRecordedWithHeadPhones = getMicrophoneAvailable();
                    }

                    @Override
                    public void onError(Exception exception) {
                        Log.e("CameraRecorder", exception.toString());
                    }

                    @Override
                    public void onCameraThreadFinish() {
                        runOnUiThread(() -> setUpCamera());
                    }
                })
                .lensFacing(lensFacing)
                .cameraSize(1280, 720)
                .videoSize(480, 640)
                .build();
    }

    @Override
    protected void onPause() {
        super.onPause();
        releasePlayer();
    }

    public boolean checkPermissionsForCamera(Activity context) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.CAMERA)
                        || ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        || ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.RECORD_AUDIO)) {
                    ActivityCompat.requestPermissions((context), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO}, AppConstants.ClassConstants.VIDEO_INTENT_REQUEST_CODE);
                } else {
                    if (!isFirstTime) {
                        ActivityCompat.requestPermissions((context), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO}, AppConstants.ClassConstants.VIDEO_INTENT_REQUEST_CODE);
                        isFirstTime = true;
                    } else {
                        com.whizzly.dialog.ErrorDialog errorDialog = new com.whizzly.dialog.ErrorDialog();
                        if (context instanceof VideoRecordActivity)
                            errorDialog.show(((VideoRecordActivity) context).getSupportFragmentManager(), com.whizzly.dialog.ErrorDialog.class.getCanonicalName());
                    }
                }
                return false;
            } else
                return true;
        } else
            return true;
    }

    private void releasePlayer() {
        if (mExoPlayer != null) {
            mExoPlayer.setPlayWhenReady(false);
            mExoPlayer.release();
            mExoPlayer = null;
        }
    }

    private void setProgressBar() {
        mActivityVideoRecordBinding.pbRecordingProgress.setCornerRadius(0f);
        mActivityVideoRecordBinding.pbRecordingProgress.setDividerColor(Color.WHITE);
        mActivityVideoRecordBinding.pbRecordingProgress.setDividerEnabled(true);
        mActivityVideoRecordBinding.pbRecordingProgress.setDividerWidth(2);
        mActivityVideoRecordBinding.pbRecordingProgress.setShader(new int[]{getResources().getColor(R.color.Green), getResources().getColor(R.color.Green), getResources().getColor(R.color.Green)});
        mActivityVideoRecordBinding.pbRecordingProgress.enableAutoProgressView(MAX_VIDEO_LENGTH);
        mActivityVideoRecordBinding.pbRecordingProgress.pause();
    }

    private void setCamera() {
        mNextVideoAbsolutePath = getVideoFilePath(this);
    }

    private String getVideoFilePath(Context context) {
        final File dir = context.getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "video_" + System.currentTimeMillis() + ".mp4";
    }

    @SuppressLint("DefaultLocale")
    public void startRecording() {
        mNextVideoAbsolutePath = getVideoFilePath(this);
        mVideoRecordModel.setIsRecordPressed(true);
        mVideoRecordModel.setIsVideoStarted(true);
        mVideoRecordModel.setIsRemoveVisible(false);
        mIsRecordingVideo = true;
        cameraRecorder.start(mNextVideoAbsolutePath);
        mActivityVideoRecordBinding.tvRecordingTime.setVisibility(View.VISIBLE);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mActivityVideoRecordBinding.tvRecordingTime.setText(String.format("00:%02d", mActivityVideoRecordBinding.pbRecordingProgress.progressInMillies() / 1000));
                handler.postDelayed(this, 1000);
            }
        }, 0);


        if (mProgress.size() > 0)
            mActivityVideoRecordBinding.pbRecordingProgress.resume(mProgress.get(mProgress.size() - 1));
        else
            mActivityVideoRecordBinding.pbRecordingProgress.resume();
        mActivityVideoRecordBinding.tvMessage.setVisibility(View.INVISIBLE);
    }

    private void stopRecordingVideo() {
        cameraRecorder.stop();
        mActivityVideoRecordBinding.tvMessage.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_COLLAB_FLIP_CAMERA:
                switchCamera();
                break;
            case AppConstants.ClassConstants.ON_COLLAB_TOGGLE_FLASH:
                toggleFlash();
                break;
            case AppConstants.ClassConstants.ON_COLLAB_TIMER_CLICK:
                new TimerDialog(this).show(getSupportFragmentManager(), TimerDialog.class.getCanonicalName());
                break;
            case AppConstants.ClassConstants.ON_CLOSE_VIDEO_RECORDING:
                discardRecording();
                break;
            case AppConstants.ClassConstants.ON_REMOVE_SEGMENT_CLICKED:
                showRemoveSegmentDialog();
                break;
            case AppConstants.ClassConstants.ON_RECORD_SAVE:
                gotoCollabActivity();
                break;
            case AppConstants.ClassConstants.ON_CLICK_MUSIC_SELECTION:
                releasePlayer();
                if (!mVideoRecordModel.getIsStartTimer())
                    goToMusicFragment();
                break;
            case AppConstants.ClassConstants.ON_COLLAB_GALLERY_CLICK:
                chooseVideoFromGallery();
                break;
        }
    }

    private void goToMusicFragment() {
        addFragmentFromRight(R.id.fl_container, new PickMusicFragment());
    }

    private void chooseVideoFromGallery() {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Video"), AppConstants.ClassConstants.REQUEST_TAKE_GALLERY_VIDEO);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == AppConstants.ClassConstants.REQUEST_TAKE_GALLERY_VIDEO) {
                Uri selectedImageUri = data.getData();
                // MEDIA GALLERY
                videoFromFileManager = getPath(this, selectedImageUri);
                if (videoFromFileManager != null) {
                    Intent intent = new Intent(getApplicationContext(),
                            CollabGalleryTrimmerActivity.class);
                    intent.putExtra(AppConstants.ClassConstants.IS_FROM, 1);
                    intent.putExtra(AppConstants.ClassConstants.VIDEO_FILE_PATH, videoFromFileManager);
                    startActivity(intent);
                    finish();
                }
            }
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    private void gotoCollabActivity() {
        if (mExoPlayer != null)
            mExoPlayer.setPlayWhenReady(false);
        releaseCamera();
        releasePlayer();
        if (currentVolume != -1) {
            AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume, AudioManager.FLAG_PLAY_SOUND);
        }
        Intent intent = new Intent(VideoRecordActivity.this, CollabBarColorActivity.class);
        intent.putExtra(AppConstants.ClassConstants.VIDEO_FILE_PATH, mVideoArrayList);
        intent.putExtra(AppConstants.ClassConstants.MUSIC_ID, mMusicId);
        intent.putExtra(AppConstants.TEMPLATE_SIZE, templateSize);
        if (audioUrl != null)
            intent.putExtra(AppConstants.ClassConstants.AUDIO_FILE, audioUrl);
        intent.putExtra(AppConstants.ClassConstants.IS_RECORDED_WITH_HEADPHONES, isRecordedWithHeadPhones ? 1 : 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        mProgress.clear();
        mVideoArrayList.clear();
        mActivityVideoRecordBinding.tvRecordingTime.setVisibility(View.INVISIBLE);
        mActivityVideoRecordBinding.tvRecordingTime.setText("");
        mActivityVideoRecordBinding.pbRecordingProgress.reset();

    }

    @Override
    public void onBackPressed() {
        discardRecording();
    }

    private void showRemoveSegmentDialog() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.txt_error_remove_segment)
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> deleteRecording())
                .setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss())
                .create().show();
    }

    private void discardRecording() {
        new AlertDialog.Builder(this)
                .setMessage("You came so far.. \nYou still wants to go back?")
                .setPositiveButton(android.R.string.yes, (dialogInterface, i) -> discard())
                .setNegativeButton(android.R.string.no, (dialogInterface, i) -> dialogInterface.dismiss())
                .create().show();
    }

    private void discard() {
        if (cameraRecorder != null) {
            cameraRecorder.release();
            cameraRecorder = null;
        }

        if (currentVolume != -1) {
            AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume, AudioManager.FLAG_PLAY_SOUND);
        }
        releasePlayer();
        finish();
    }



    private void deleteRecording() {
        if (mVideoArrayList.size() > 0) {
            String fileToRemove = mVideoArrayList.remove(mVideoArrayList.size() - 1);
            File file = new File(fileToRemove);
            if (file.delete()) {
                Log.e("deleteRecording: ", "Deleted File");
            }
            mVideoRecordModel.setIsRemoveVisible(true);
        }
        if (mProgress.size() > 1) {
            mProgress.remove(mProgress.size() - 1);
            mActivityVideoRecordBinding.pbRecordingProgress.removeDivider();
            mActivityVideoRecordBinding.pbRecordingProgress.updateProgress(mProgress.get(mProgress.size() - 1));
        } else if (mProgress.size() == 1) {
            mProgress.remove(mProgress.size() - 1);
            mActivityVideoRecordBinding.pbRecordingProgress.removeDivider();
            mActivityVideoRecordBinding.pbRecordingProgress.updateProgress(0);
        }
    }

    public void switchCamera() {
        releaseCamera();
        if (lensFacing == LensFacing.BACK) {
            lensFacing = LensFacing.FRONT;
            mVideoRecordModel.setIsFlashClicked(false);
        } else {
            lensFacing = LensFacing.BACK;
            mVideoRecordModel.setIsFlashClicked(false);
        }
    }

    private void toggleFlash() {
        if (mVideoRecordModel.getIsFlashClicked()) {
            if (cameraRecorder != null && cameraRecorder.isFlashSupport()) {
                cameraRecorder.switchFlashMode();
                cameraRecorder.changeAutoFocus();
            }
            mVideoRecordModel.setIsFlashClicked(false);
        } else {
            if (cameraRecorder != null && cameraRecorder.isFlashSupport()) {
                cameraRecorder.switchFlashMode();
                cameraRecorder.changeAutoFocus();
            }
            mVideoRecordModel.setIsFlashClicked(true);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void recordUsingPressedButton() {
        try {
            mActivityVideoRecordBinding.ivRecord.setOnTouchListener((view, motionEvent) -> {
                if (!mVideoRecordModel.getIsStartTimer()) {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_UP:
                            if (mIsRecordingVideo) {
                                mIsRecordingVideo = false;
                                if (mExoPlayer != null) {
                                    mExoPlayer.setPlayWhenReady(false);
                                }
                                stopRecordingVideo();
                            }
                            return false;
                    }
                }
                return false;
            });


            mActivityVideoRecordBinding.ivRecord.setOnLongClickListener(v -> {
                if (!mIsRecordingVideo) {
                    mIsRecordingVideo = true;
                    startRecording();
                    if (mExoPlayer != null) {
                        mExoPlayer.setPlayWhenReady(true);
                    }
                }
                return true;
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setExoplayer(Uri musicUri) {
        Context mContext = getApplicationContext();
        if (mExoPlayer == null) {
            mExoPlayer =
                    ExoPlayerFactory.newSimpleInstance(this,
                            new DefaultRenderersFactory(this), new DefaultTrackSelector(), new
                                    DefaultLoadControl());
        }
        mExoPlayer.setPlayWhenReady(false);
        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(
                        mContext, Util.getUserAgent(mContext, "Whizzly"), null);
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        MediaSource mediaSource =
                new ExtractorMediaSource(
                        musicUri, dataSourceFactory, extractorsFactory, null, null);
        mExoPlayer.prepare(mediaSource, true, true);
    }

    @Override
    public void onTimeSelected(int time) {
        setRecordAfterTimer(time);
        mVideoRecordModel.setIsStartTimer(true);
        mVideoRecordModel.setTime("" + ((time / 1000) + 1));
    }

    private void setRecordAfterTimer(int time) {
        new CountDownTimer(time, 1000) {
            @Override
            public void onTick(long l) {
                mVideoRecordModel.setTime("" + l / 1000);
            }

            @Override
            public void onFinish() {
                startRecording();
                mVideoRecordModel.setIsStartTimer(false);
            }
        }.start();
    }

    @Override
    public void onSendSong(String uri, int musicId) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        audioUrl = Uri.parse(uri).getPath();
        Log.e("onSendSong: ", audioUrl + " " + getDuration(uri));
        int songDuration = getDuration(uri);
        if (songDuration != 0) {
            mActivityVideoRecordBinding.pbRecordingProgress.enableAutoProgressView(songDuration * 1000);
            mActivityVideoRecordBinding.pbRecordingProgress.pause();
        }
        mMusicId = musicId;
        setExoplayer(Uri.parse(uri));
    }

    private int getDuration(String uriPath) {
        Uri uri = Uri.parse(uriPath);
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(getApplicationContext(), uri);
        String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        mmr.release();
        return Integer.parseInt(durationStr) / 1000;
    }
}
