package com.whizzly.collab_module.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.beans.CollabBarBean;
import com.whizzly.collab_module.beans.VideoFramesBean;
import com.whizzly.collab_module.fragment.CollabBarFragment;
import com.whizzly.collab_module.interfaces.OnSendTimeRange;
import com.whizzly.collab_module.model.CollabBarActivityModel;
import com.whizzly.collab_module.viewmodel.CollabBarActivityViewModel;
import com.whizzly.databinding.ActivityCollabBarColorBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;
import com.whizzly.utils.FFMpegCommandListener;
import com.whizzly.utils.FFMpegCommands;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import static android.view.View.GONE;

public class CollabBarColorActivity extends BaseActivity implements OnClickSendListener, OnSendTimeRange {

    public ArrayList<VideoFramesBean> mVideoFrameBeanList;
    private ActivityCollabBarColorBinding activityCollabBarColorBinding;
    private String mFilePath;
    private Uri mFileUri;
    private String videoLengthInSecs = "";
    private CollabBarActivityModel mCollabBarActivityModel;
    private ExoPlayer exoPlayer;
    private FFMpegCommands mFFmpegCommands;
    private String mAudioFilePath;
    private int mMusicId;
    private String imageFile = "";
    private String mVideoMuteFilePath;
    private ArrayList<String> mVideoStack = new ArrayList<>();
    private int isRecordedUsingEarphones = 0;
    private int templateSize = 0;
    private String musicFile = "";
    private int frameWidth = 60;
    private long durationInMillies = 0;
    private ArrayList<CollabBarBean> mCollabBarBeans = new ArrayList<>();

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityCollabBarColorBinding = DataBindingUtil.setContentView(this, R.layout.activity_collab_bar_color);
        CollabBarActivityViewModel mCollabBarActivityViewModel = ViewModelProviders.of(this).get(CollabBarActivityViewModel.class);
        activityCollabBarColorBinding.setViewModel(mCollabBarActivityViewModel);
        mCollabBarActivityModel = new CollabBarActivityModel();
        activityCollabBarColorBinding.setModel(mCollabBarActivityModel);
        mCollabBarActivityViewModel.setOnClickSendListener(this);
        mVideoFrameBeanList = new ArrayList<>();
        clearStatusBar();
        activityCollabBarColorBinding.pbLoading.setVisibility(View.VISIBLE);
        getData();
        mFFmpegCommands = FFMpegCommands.getInstance(this);
        try {
            mFFmpegCommands.initializeFFmpeg();
        } catch (FFmpegNotSupportedException e) {
            e.printStackTrace();
        }

        if (mVideoStack.size() > 1) {
            concatVideos(addVideosPathToTextFile(mVideoStack));
        } else {
            mFilePath = mVideoStack.get(0);
            File file = new File(mFilePath);
            mFileUri = Uri.fromFile(file);
            extractAudio();
            startAsyncTask();
        }
    }

    private String getAudioFilePath(Context context) {
        final File dir = context.getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "audio_" + System.currentTimeMillis() + ".mp3";
    }

    private String getVideoFile() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "videoWithOutAudio_" + System.currentTimeMillis() + ".mp4";
    }

    private String getConcatFile() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "concat_" + System.currentTimeMillis() + ".mp4";
    }

    private String addVideosPathToTextFile(ArrayList<String> videoPathList) {
        final File dir = getExternalFilesDir(null);
        String textFile = (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "concat_text_" + System.currentTimeMillis() + ".txt";
        File file = new File(textFile);
        try {
            FileOutputStream f = new FileOutputStream(file);
            PrintWriter pw = new PrintWriter(f);
            for (int i = 0; i < videoPathList.size(); i++) {
                pw.println("file " + videoPathList.get(i));
            }
            pw.flush();
            pw.close();
            f.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return textFile;
    }

    private void concatVideos(String textFile) {
        mFilePath = getConcatFile();
        mFFmpegCommands.concatenateVideos(textFile, mFilePath, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                File file = new File(mFilePath);
                mFileUri = Uri.fromFile(file);
                extractAudio();
                startAsyncTask();
            }

            @Override
            public void onProgress(String message) {

            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onStart() {

            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (exoPlayer != null)
            exoPlayer.setPlayWhenReady(false);
        discardRecording();
    }

    private void extractAudio() {
        mAudioFilePath = getAudioFilePath(this);
        mVideoMuteFilePath = getVideoFile();
        try {
            mFFmpegCommands.getAudioFile(mFilePath, mAudioFilePath, new FFMpegCommandListener() {
                @Override
                public void onSuccess(String message) {
                    mFFmpegCommands.getVideoFile(mFilePath, mVideoMuteFilePath, new FFMpegCommandListener() {
                        @Override
                        public void onSuccess(String message) {
                            if (isRecordedUsingEarphones == 1 && getIntent().hasExtra(AppConstants.ClassConstants.AUDIO_FILE)) {
                                String audioFile = getAudioFilePath(CollabBarColorActivity.this);
                                mFFmpegCommands.increaseAudioVolume(mAudioFilePath, audioFile, new FFMpegCommandListener() {
                                    @Override
                                    public void onSuccess(String message) {
                                        mAudioFilePath = audioFile;
                                        String audioFile = getAudioFilePath(CollabBarColorActivity.this);
                                        mFFmpegCommands.mergeTwoAudioFiles(mAudioFilePath, musicFile, audioFile, new FFMpegCommandListener() {
                                            @Override
                                            public void onSuccess(String message) {
                                                mAudioFilePath = audioFile;
                                                mFilePath = getVideoFile();
                                                mFFmpegCommands.mergeSongWithVideo(mVideoMuteFilePath, audioFile, mFilePath, new FFMpegCommandListener() {
                                                    @Override
                                                    public void onSuccess(String message) {

                                                        initializePlayer(mFilePath);
                                                        exoPlayer.setPlayWhenReady(true);
                                                        activityCollabBarColorBinding.pbLoading.setVisibility(GONE);
                                                    }

                                                    @Override
                                                    public void onProgress(String message) {

                                                    }

                                                    @Override
                                                    public void onFailure(String message) {

                                                    }

                                                    @Override
                                                    public void onStart() {

                                                    }

                                                    @Override
                                                    public void onFinish() {

                                                    }
                                                });
                                            }

                                            @Override
                                            public void onProgress(String message) {

                                            }

                                            @Override
                                            public void onFailure(String message) {

                                            }

                                            @Override
                                            public void onStart() {

                                            }

                                            @Override
                                            public void onFinish() {

                                            }
                                        });
                                    }

                                    @Override
                                    public void onProgress(String message) {

                                    }

                                    @Override
                                    public void onFailure(String message) {

                                    }

                                    @Override
                                    public void onStart() {

                                    }

                                    @Override
                                    public void onFinish() {

                                    }
                                });
                            } else {
                                initializePlayer(mFilePath);
                                exoPlayer.setPlayWhenReady(true);
                                activityCollabBarColorBinding.pbLoading.setVisibility(GONE);
                            }
                        }

                        @Override
                        public void onProgress(String message) {

                        }

                        @Override
                        public void onFailure(String message) {

                        }

                        @Override
                        public void onStart() {

                        }

                        @Override
                        public void onFinish() {

                        }
                    });
                }

                @Override
                public void onProgress(String message) {

                }

                @Override
                public void onFailure(String message) {

                }

                @Override
                public void onStart() {

                }

                @Override
                public void onFinish() {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializePlayer(String path) {
        if (exoPlayer == null) {
            exoPlayer = ExoPlayerFactory.newSimpleInstance(this,
                    new DefaultRenderersFactory(this),
                    new DefaultTrackSelector(), new DefaultLoadControl());
            exoPlayer.setPlayWhenReady(false);
            exoPlayer.setRepeatMode(Player.REPEAT_MODE_ONE);
            Uri uri = Uri.parse(path);
            MediaSource mediaSource = buildMediaSource(uri);
            exoPlayer.addListener(new Player.EventListener() {
                @Override
                public void onPlayerError(ExoPlaybackException error) {
                    Log.e("onPlayerError: ", error.getMessage());
                }
            });
            activityCollabBarColorBinding.videoView.setPlayer(exoPlayer);
            exoPlayer.prepare(mediaSource, true, false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        pausePlayer();
    }

    public void pausePlayer() {
        if (exoPlayer != null) {
            exoPlayer.setPlayWhenReady(false);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releasePlayer();
    }

    private void releasePlayer() {
        if (exoPlayer != null) {
            exoPlayer.setPlayWhenReady(false);
            exoPlayer.stop();
            exoPlayer.release();
            exoPlayer = null;
        }
    }

    private MediaSource buildMediaSource(Uri uri) {
        return new ExtractorMediaSource.Factory(
                new DefaultDataSourceFactory(this, "whizzly-app")).
                createMediaSource(uri);
    }

    private void getData() {
        Intent intent = getIntent();
        mVideoStack = (ArrayList<String>) intent.getSerializableExtra(AppConstants.ClassConstants.VIDEO_FILE_PATH);
        mMusicId = intent.getIntExtra(AppConstants.ClassConstants.MUSIC_ID, 0);
        templateSize = intent.getIntExtra(AppConstants.TEMPLATE_SIZE, 0);
        isRecordedUsingEarphones = intent.getIntExtra(AppConstants.ClassConstants.IS_RECORDED_WITH_HEADPHONES, 0);
        if (intent.hasExtra(AppConstants.ClassConstants.AUDIO_FILE)) {
            musicFile = intent.getStringExtra(AppConstants.ClassConstants.AUDIO_FILE);
        }
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_CLICK_DISCARD_RECORDING:
                discardRecording();
                break;
            case AppConstants.ClassConstants.ON_CLICK_APPLY_COLORS:
                mCollabBarActivityModel.setIsDonePressed(true);
                break;
            case AppConstants.ClassConstants.ON_CLICK_COLLAB_BAR:
                activityCollabBarColorBinding.ivCollabBarColor.setEnabled(false);
                new Handler().postDelayed(() -> activityCollabBarColorBinding.ivCollabBarColor.setEnabled(true), 2000);
                CollabBarFragment collabBarFragment = new CollabBarFragment();
                Bundle collabBundle = new Bundle();
                collabBundle.putString(AppConstants.ClassConstants.VIDEO_FILE_PATH, mFilePath);
                collabBundle.putInt(AppConstants.ClassConstants.TEMPLETE_SIZE, templateSize);
                collabBundle.putString(AppConstants.ClassConstants.VIDEO_FILE_URI, String.valueOf(Uri.parse(mFilePath)));
                collabBundle.putString(AppConstants.ClassConstants.VIDEO_LENGTH, videoLengthInSecs);
                collabBundle.putInt(AppConstants.ClassConstants.FRAME_SIZE, frameWidth);
                if(mCollabBarBeans.size()>0){
                    collabBundle.putParcelableArrayList(AppConstants.ClassConstants.COLOR_BAR_DATA, mCollabBarBeans);
                }
                collabBundle.putLong(AppConstants.ClassConstants.DURATION_IN_MILLIES, durationInMillies);
                collabBundle.putParcelableArrayList(AppConstants.ClassConstants.FRAMES_LIST, mVideoFrameBeanList);
                collabBarFragment.setArguments(collabBundle);
                collabBarFragment.setCancelable(false);
                collabBarFragment.show(getSupportFragmentManager(), CollabBarFragment.class.getCanonicalName());
                break;
            case AppConstants.ClassConstants.ON_CLICK_COLLAB_BACK:
                mCollabBarActivityModel.setIsDonePressed(false);
                break;
            case AppConstants.ClassConstants.ON_CLICK_POST_VIDEO:
                postData();
                break;
            case AppConstants.ClassConstants.ON_COLLAB_BAR_DONE:
                removeFramgentToBottom(R.id.fl_container);
                mCollabBarActivityModel.setIsDonePressed(true);
                break;
            case AppConstants.ClassConstants.ON_COLLAB_BAR_CANCEL:
                removeFramgentToBottom(R.id.fl_container);
                mCollabBarActivityModel.setIsDonePressed(false);
                break;
        }
    }

    private void postData() {
        if (exoPlayer != null)
            exoPlayer.setPlayWhenReady(false);
        Intent intent = new Intent(this, ChooseTempleteActivity.class);
        intent.putExtra(AppConstants.ClassConstants.FRAME, imageFile);
        intent.putExtra(AppConstants.ClassConstants.FINAL_VIDEO_FILE_PATH, mFilePath);
        intent.putExtra(AppConstants.ClassConstants.TEMPLETE_SIZE, templateSize);
        intent.putExtra(AppConstants.ClassConstants.SONG_FILE_PATH, mAudioFilePath);
        intent.putExtra(AppConstants.ClassConstants.VIDEO_FILE_PATH, mVideoMuteFilePath);
        intent.putExtra(AppConstants.ClassConstants.VIDEO_FILE_URI, String.valueOf(mFileUri));
        intent.putExtra(AppConstants.ClassConstants.DURATION_IN_MILLIES, durationInMillies);
        intent.putExtra(AppConstants.ClassConstants.VIDEO_LENGTH, String.valueOf(durationInMillies));
        intent.putExtra(AppConstants.ClassConstants.MUSIC_ID, mMusicId);
        intent.putExtra(AppConstants.ClassConstants.VIDEO_THUMBNAIL_URL, imageFile);
        intent.putExtra(AppConstants.ClassConstants.COLOR_BAR_DATA, mCollabBarBeans);
        startActivity(intent);
    }

    private String getImageDirectory() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "image_" + System.currentTimeMillis() + ".jpeg";
    }

    private void saveBitmap(Bitmap bitmap) {
        imageFile = getImageDirectory();
        try (FileOutputStream out = new FileOutputStream(imageFile)) {
            if (bitmap != null)
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (exoPlayer != null) {
            if (!exoPlayer.getPlayWhenReady())
                exoPlayer.setPlayWhenReady(true);
        }
    }

    private void startAsyncTask() {
        getFramesFromVideo();
    }

    private void getFramesFromVideo() {
        try {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            try {
                retriever.setDataSource(this, mFileUri);
            } catch (Exception e) {
                System.out.println("Exception= " + e);
            }
            mVideoFrameBeanList.clear();
            String duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            if (duration != null) {
                durationInMillies = Integer.parseInt(duration); //duration in millisec
                videoLengthInSecs = String.valueOf(durationInMillies / 1000);  //millisec to sec.
                int numberOfFrames = getNumberOfFramesOnScreen();
                frameWidth = getWindowManager().getDefaultDisplay().getWidth() / numberOfFrames;
                int timeInterval = (int) (durationInMillies / numberOfFrames);
                for (int i = 1; i <= numberOfFrames; i++) {
                    VideoFramesBean videoFramesBean = new VideoFramesBean();
                    videoFramesBean.setFrame(retriever.getFrameAtTime((long) (timeInterval * i * 1000000), MediaMetadataRetriever.OPTION_CLOSEST_SYNC));
                    mVideoFrameBeanList.add(videoFramesBean);
                }
                saveBitmap(retriever.getFrameAtTime(1000));
            }
            retriever.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getNumberOfFramesOnScreen() {
        int number = 1;
        int screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        for (int i = 60; i < screenWidth; i++) {
            if (screenWidth % i == 0) {
                number = i;
                break;
            }
        }
        return screenWidth / (number * 2);
    }

    private void discardRecording() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.txt_discard_video)
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> discard())
                .setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss())
                .create().show();
    }

    private void discard() {
        File file = new File(mFilePath);
        if (file.delete()) {
            Log.e("deleteRecording: ", "Deleted File");
        }
        releasePlayer();
        finish();
    }

    private int dpToPx(float dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    @Override
    public void onSendTimeRange(ArrayList<CollabBarBean> collabBarBeanArrayList) {
        mCollabBarBeans = collabBarBeanArrayList;
        setIndeteminateProgressBar(mCollabBarBeans);
    }

    private void setIndeteminateProgressBar(ArrayList<CollabBarBean> mCollabBarBeans){
        activityCollabBarColorBinding.vBar.removeAllViews();
        int screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) activityCollabBarColorBinding.vBar.getLayoutParams();
        for (int i = 0; i < mCollabBarBeans.size(); i++) {
            View view = new View(this);
            float start = (mCollabBarBeans.get(i).getStartTime() * screenWidth) / durationInMillies;
            float end = (mCollabBarBeans.get(i).getEndTime() * screenWidth) / durationInMillies;
            float width = end - start;
            view.setBackgroundColor(Color.parseColor(mCollabBarBeans.get(i).getColorCode()));
            RelativeLayout.LayoutParams viewLayoutParams = null;
            switch (mCollabBarBeans.get(i).getCode()) {
                case "0":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(20 / (templateSize + 1), this));
                    break;
                case "1":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(2 * (20 / (templateSize + 1)), this));
                    break;
                case "2":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(3 * (20 / (templateSize + 1)), this));
                    break;
                case "3":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(4 * (20 / (templateSize + 1)), this));
                    break;
                case "4":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(5 * (20 / (templateSize + 1)), this));
                    break;
                case "5":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(6 * (20 / (templateSize + 1)), this));
                    break;
                case "6":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY(0);
                    break;
            }
            view.setX(start);
            view.setLayoutParams(viewLayoutParams);
            activityCollabBarColorBinding.vBar.addView(view);
        }
        activityCollabBarColorBinding.vBar.setLayoutParams(layoutParams);
    }
}
