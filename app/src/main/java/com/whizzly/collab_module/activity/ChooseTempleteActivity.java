package com.whizzly.collab_module.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.viewmodel.ChooseTempleteActivityViewModel;
import com.whizzly.collab_module.beans.CollabBarBean;
import com.whizzly.databinding.ActivityChooseTempleteBinding;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.self_collab.SelfCollabRecordActivity;
import com.whizzly.self_collab.SelfCollabVideoDataBean;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class ChooseTempleteActivity extends BaseActivity implements OnClickSendListener {

    private ActivityChooseTempleteBinding mActivityChooseTempleteBinding;
    private ChooseTempleteActivityViewModel mChooseTempleteActivityViewModel;
    private String videoLength, videoFileUri, videoFilePath;
    private boolean isClickedOne = false, isClickTwo = false, isClickThree = false, isClickFour = false, isClickFive = false, isClickSix = false;
    private int mTempleteSize = 0, mMusicId;
    private String mFinalVideoPath, mAudioFilePath;
    private String imagePath = "";
    private long durationInMillies;
    private int collabId;
    private int click = 1;
    private ArrayList<CollabBarBean> mCollabBarMap = new ArrayList<>();
    private String mVideoThumbnailUrl;

    private void getData() {
        Intent intent = getIntent();
        collabId = intent.getIntExtra(AppConstants.ClassConstants.COLLAB_ID, 0);
        mMusicId = intent.getIntExtra(AppConstants.ClassConstants.MUSIC_ID, 0);
        mTempleteSize = intent.getIntExtra(AppConstants.ClassConstants.TEMPLETE_SIZE, 0);
        imagePath = intent.getStringExtra(AppConstants.ClassConstants.FRAME);
        mFinalVideoPath = intent.getStringExtra(AppConstants.ClassConstants.FINAL_VIDEO_FILE_PATH);
        mAudioFilePath = intent.getStringExtra(AppConstants.ClassConstants.SONG_FILE_PATH);
        durationInMillies = intent.getLongExtra(AppConstants.ClassConstants.DURATION_IN_MILLIES, 0);
        videoFilePath = intent.getStringExtra(AppConstants.ClassConstants.VIDEO_FILE_PATH);
        videoFileUri = intent.getStringExtra(AppConstants.ClassConstants.VIDEO_FILE_URI);
        mVideoThumbnailUrl = intent.getStringExtra(AppConstants.ClassConstants.VIDEO_THUMBNAIL_URL);
        videoLength = intent.getStringExtra(AppConstants.ClassConstants.VIDEO_LENGTH);
        mCollabBarMap = intent.getParcelableArrayListExtra(AppConstants.ClassConstants.COLOR_BAR_DATA);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityChooseTempleteBinding = DataBindingUtil.setContentView(this, R.layout.activity_choose_templete);
        mChooseTempleteActivityViewModel = ViewModelProviders.of(this).get(ChooseTempleteActivityViewModel.class);
        mActivityChooseTempleteBinding.setViewModel(mChooseTempleteActivityViewModel);
        mChooseTempleteActivityViewModel.setOnClickSendListener(this);
        mActivityChooseTempleteBinding.toolbar.ivBack.setVisibility(View.VISIBLE);
        mActivityChooseTempleteBinding.toolbar.tvToolbarText.setText(R.string.txt_choose_templete);
        mActivityChooseTempleteBinding.toolbar.tvDone.setVisibility(View.VISIBLE);
        mActivityChooseTempleteBinding.toolbar.tvDone.setOnClickListener(view -> {
            if (isClickedOne || isClickTwo || isClickThree || isClickFour || isClickFive || isClickSix) {
                /*Intent intent = new Intent(ChooseTempleteActivity.this, PreviewVideoActivity.class);
                intent.putExtra(AppConstants.ClassConstants.FRAME, imagePath);
                intent.putExtra(AppConstants.ClassConstants.VIDEO_FILE_PATH, Uri.parse(videoFileUri).getPath());
                intent.putExtra(AppConstants.ClassConstants.VIDEO_FILE_URI, videoFileUri);
                intent.putExtra(AppConstants.ClassConstants.VIDEO_LENGTH, videoLength);
                intent.putExtra(AppConstants.ClassConstants.FINAL_VIDEO_FILE_PATH, mFinalVideoPath);
                intent.putExtra(AppConstants.ClassConstants.SONG_FILE_PATH, mAudioFilePath);
                intent.putExtra(AppConstants.ClassConstants.IS_MANDATORY, 1);
                intent.putExtra(AppConstants.ClassConstants.MUSIC_ID, mMusicId);
                intent.putExtra(AppConstants.ClassConstants.TEMPLETE_SIZE, mTempleteSize);
                intent.putExtra(AppConstants.ClassConstants.DURATION_IN_MILLIES, durationInMillies);
                intent.putExtra(AppConstants.ClassConstants.VIDEO_THUMBNAIL_URL, mVideoThumbnailUrl);
                intent.putExtra(AppConstants.ClassConstants.FRAME_NUMBER, getClick());
                intent.putExtra(AppConstants.ClassConstants.COLOR_BAR_DATA, mCollabBarMap);
                startActivity(intent);*/

                Intent intent = new Intent(ChooseTempleteActivity.this, SelfCollabRecordActivity.class);
                SelfCollabVideoDataBean selfCollabVideoDataBean = new SelfCollabVideoDataBean();
                selfCollabVideoDataBean.setFrameNumber(String.valueOf(getClick()));
                selfCollabVideoDataBean.setImageFrame(mVideoThumbnailUrl);
                selfCollabVideoDataBean.setFinalVideo(mFinalVideoPath);
                selfCollabVideoDataBean.setAudioPath(mAudioFilePath);
                selfCollabVideoDataBean.setVideoPath(videoFilePath);
                selfCollabVideoDataBean.setVideoUri(videoFileUri);
                selfCollabVideoDataBean.setVideoLength(videoLength);
                ArrayList<SelfCollabVideoDataBean> selfCollabVideoDataBeans = new ArrayList<>();
                selfCollabVideoDataBeans.add(selfCollabVideoDataBean);
                intent.putExtra(AppConstants.ClassConstants.VIDEO_DATA, selfCollabVideoDataBeans);
                intent.putExtra(AppConstants.ClassConstants.TEMPLETE_SIZE, mTempleteSize);
                intent.putExtra(AppConstants.ClassConstants.MUSIC_ID, mMusicId);
                intent.putExtra(AppConstants.ClassConstants.FRAMES, 2);
                intent.putExtra(AppConstants.ClassConstants.COLOR_BAR_DATA, mCollabBarMap);
                startActivity(intent);

            } else {
                Toast.makeText(ChooseTempleteActivity.this, R.string.txt_select_frame, Toast.LENGTH_SHORT).show();
            }
        });

        mActivityChooseTempleteBinding.fabAddAnother.setOnClickListener(v -> {
            Intent intent = new Intent(ChooseTempleteActivity.this, SelfCollabRecordActivity.class);
            SelfCollabVideoDataBean selfCollabVideoDataBean = new SelfCollabVideoDataBean();
            selfCollabVideoDataBean.setFrameNumber(String.valueOf(getClick()));
            selfCollabVideoDataBean.setImageFrame(mVideoThumbnailUrl);
            selfCollabVideoDataBean.setFinalVideo(mFinalVideoPath);
            selfCollabVideoDataBean.setAudioPath(mAudioFilePath);
            selfCollabVideoDataBean.setVideoPath(videoFilePath);
            selfCollabVideoDataBean.setVideoUri(videoFileUri);
            selfCollabVideoDataBean.setVideoLength(videoLength);
            ArrayList<SelfCollabVideoDataBean> selfCollabVideoDataBeans = new ArrayList<>();
            selfCollabVideoDataBeans.add(selfCollabVideoDataBean);
            intent.putExtra(AppConstants.ClassConstants.VIDEO_DATA, selfCollabVideoDataBeans);
            intent.putExtra(AppConstants.ClassConstants.TEMPLETE_SIZE, mTempleteSize);
            intent.putExtra(AppConstants.ClassConstants.MUSIC_ID, mMusicId);
            intent.putExtra(AppConstants.ClassConstants.FRAMES, 2);
            intent.putExtra(AppConstants.ClassConstants.COLOR_BAR_DATA, mCollabBarMap);
            startActivity(intent);
        });

        mActivityChooseTempleteBinding.toolbar.ivBack.setOnClickListener(view -> onBackPressed());
        getData();
        setTemplete();
        setClick(1);
        if(DataManager.getInstance().isNotchDevice()){
            mActivityChooseTempleteBinding.toolbar.view.setVisibility(View.VISIBLE);
        }else{
            mActivityChooseTempleteBinding.toolbar.view.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants
                    .ClassConstants.ON_CHOOSE_TEMPLETE_ONE:
                setClick(1);
                break;
            case AppConstants
                    .ClassConstants.ON_CHOOSE_TEMPLETE_TWO:
                setClick(2);
                break;
            case AppConstants
                    .ClassConstants.ON_CHOOSE_TEMPLETE_THREE:
                setClick(3);
                break;
            case AppConstants
                    .ClassConstants.ON_CHOOSE_TEMPLETE_FOUR:
                setClick(4);
                break;
            case AppConstants
                    .ClassConstants.ON_CHOOSE_TEMPLETE_FIVE:
                setClick(5);
                break;
            case AppConstants
                    .ClassConstants.ON_CHOOSE_TEMPLETE_SIX:
                setClick(6);
                break;
        }
    }

    public int getClick() {
        return click;
    }

    private void setClick(int code) {
        this.click = code;
        switch (code) {
            case 1:
                /*if (isClickedOne) {
                    isClickedOne = false;
                    mActivityChooseTempleteBinding.ivFrameOne.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.llAddVideoOne.setVisibility(View.VISIBLE);
                } else */{
                    mActivityChooseTempleteBinding.ivFrameOne.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.ivFrameTwo.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameThree.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameFour.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameFive.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameSix.setVisibility(View.GONE);

                    mActivityChooseTempleteBinding.llAddVideoOne.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.llAddVideoTwo.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoThree.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoFour.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoFive.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoSix.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.ivFrameOne.setImageURI(imageFile());
                    isClickedOne = true;
                    isClickTwo = false;
                    isClickThree = false;
                    isClickFour = false;
                    isClickFive = false;
                    isClickSix = false;
                }
                break;
            case 2:
                /*if (isClickTwo) {
                    isClickTwo = false;
                    mActivityChooseTempleteBinding.ivFrameTwo.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.llAddVideoTwo.setVisibility(View.VISIBLE);
                } else */{
                    mActivityChooseTempleteBinding.ivFrameTwo.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoTwo.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameTwo.setImageURI(imageFile());

                    mActivityChooseTempleteBinding.ivFrameOne.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameThree.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameFour.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameFive.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameSix.setVisibility(View.GONE);

                    mActivityChooseTempleteBinding.llAddVideoOne.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoThree.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoFour.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoFive.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoSix.setVisibility(View.VISIBLE);
                    isClickedOne = false;
                    isClickTwo = true;
                    isClickThree = false;
                    isClickFour = false;
                    isClickFive = false;
                    isClickSix = false;
                }
                break;
            case 3:
                /*if (isClickThree) {
                    isClickThree = false;
                    mActivityChooseTempleteBinding.ivFrameThree.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.llAddVideoThree.setVisibility(View.VISIBLE);
                } else */{
                    mActivityChooseTempleteBinding.ivFrameThree.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoThree.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameThree.setImageURI(imageFile());

                    mActivityChooseTempleteBinding.ivFrameOne.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameTwo.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameFour.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameFive.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameSix.setVisibility(View.GONE);

                    mActivityChooseTempleteBinding.llAddVideoOne.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoTwo.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoFour.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoFive.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoSix.setVisibility(View.VISIBLE);
                    isClickedOne = false;
                    isClickTwo = false;
                    isClickThree = true;
                    isClickFour = false;
                    isClickFive = false;
                    isClickSix = false;
                }

                break;
            case 4:
               /* if (isClickFour) {
                    isClickFour = false;
                    mActivityChooseTempleteBinding.ivFrameFour.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.llAddVideoFour.setVisibility(View.VISIBLE);
                } else */{
                    mActivityChooseTempleteBinding.ivFrameFour.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoFour.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameFour.setImageURI(imageFile());

                    mActivityChooseTempleteBinding.ivFrameOne.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameTwo.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameThree.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameFive.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameSix.setVisibility(View.GONE);

                    mActivityChooseTempleteBinding.llAddVideoOne.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoTwo.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoThree.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoFive.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoSix.setVisibility(View.VISIBLE);

                    isClickedOne = false;
                    isClickTwo = false;
                    isClickThree = false;
                    isClickFour = true;
                    isClickFive = false;
                    isClickSix = false;
                }

                break;
            case 5:
                /*if (isClickFive) {
                    isClickFive = false;
                    mActivityChooseTempleteBinding.ivFrameFive.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.llAddVideoFive.setVisibility(View.VISIBLE);
                } else*/ {
                    mActivityChooseTempleteBinding.ivFrameOne.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameTwo.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameThree.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameFour.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameFive.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.ivFrameSix.setVisibility(View.GONE);

                    mActivityChooseTempleteBinding.llAddVideoOne.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoTwo.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoFour.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoThree.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoFive.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.llAddVideoSix.setVisibility(View.VISIBLE);

                    mActivityChooseTempleteBinding.ivFrameFive.setImageURI(imageFile());
                    isClickedOne = false;
                    isClickTwo = false;
                    isClickThree = false;
                    isClickFour = false;
                    isClickFive = true;
                    isClickSix = false;
                }

                break;
            case 6:
                /*if (isClickSix) {
                    isClickSix = false;
                    mActivityChooseTempleteBinding.ivFrameSix.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.llAddVideoSix.setVisibility(View.VISIBLE);
                } else*/ {

                    mActivityChooseTempleteBinding.ivFrameOne.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameTwo.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameThree.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameFour.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameFive.setVisibility(View.GONE);
                    mActivityChooseTempleteBinding.ivFrameSix.setVisibility(View.VISIBLE);

                    mActivityChooseTempleteBinding.llAddVideoOne.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoTwo.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoThree.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoFour.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoFive.setVisibility(View.VISIBLE);
                    mActivityChooseTempleteBinding.llAddVideoSix.setVisibility(View.GONE);

                    mActivityChooseTempleteBinding.ivFrameSix.setImageURI(imageFile());
                    isClickedOne = false;
                    isClickTwo = false;
                    isClickThree = false;
                    isClickFour = false;
                    isClickFive = false;
                    isClickSix = true;
                }
                break;
        }
    }

    private Uri imageFile() {
        File file = new File(imagePath);
        return Uri.fromFile(file);
    }

    private void setTemplete() {
        switch (mTempleteSize) {
            case 2:
                mActivityChooseTempleteBinding.llTopViews.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.llMiddleViews.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.llBottomView.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvOneVideo.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvTwoVideo.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvThreeVideo.setVisibility(View.GONE);
                mActivityChooseTempleteBinding.cvFourVideo.setVisibility(View.GONE);
                mActivityChooseTempleteBinding.cvFiveVideo.setVisibility(View.GONE);
                mActivityChooseTempleteBinding.cvSixVideo.setVisibility(View.GONE);
                break;
            case 3:
                mActivityChooseTempleteBinding.llTopViews.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.llMiddleViews.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.llBottomView.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvOneVideo.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvTwoVideo.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvThreeVideo.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvFourVideo.setVisibility(View.GONE);
                mActivityChooseTempleteBinding.cvFiveVideo.setVisibility(View.GONE);
                mActivityChooseTempleteBinding.cvSixVideo.setVisibility(View.GONE);
                break;
            case 4:
                mActivityChooseTempleteBinding.llTopViews.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.llMiddleViews.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.llBottomView.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvOneVideo.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvTwoVideo.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvThreeVideo.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvFourVideo.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvFiveVideo.setVisibility(View.GONE);
                mActivityChooseTempleteBinding.cvSixVideo.setVisibility(View.GONE);
                break;
            case 5:
                mActivityChooseTempleteBinding.llTopViews.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.llMiddleViews.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.llBottomView.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvOneVideo.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvTwoVideo.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvThreeVideo.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvFourVideo.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvFiveVideo.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvSixVideo.setVisibility(View.GONE);
                break;
            case 6:
                mActivityChooseTempleteBinding.llTopViews.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.llMiddleViews.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.llBottomView.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvOneVideo.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvTwoVideo.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvThreeVideo.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvFourVideo.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvFiveVideo.setVisibility(View.VISIBLE);
                mActivityChooseTempleteBinding.cvSixVideo.setVisibility(View.VISIBLE);
                break;
        }
    }
}
