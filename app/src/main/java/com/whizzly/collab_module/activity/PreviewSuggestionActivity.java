package com.whizzly.collab_module.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.beans.CollabBarBean;
import com.whizzly.collab_module.beans.VideoFramesBean;
import com.whizzly.collab_module.collab_model.CollabPostModel;
import com.whizzly.collab_module.collab_model.CollabVideoArray;
import com.whizzly.collab_module.fragment.CollabBarFragment;
import com.whizzly.collab_module.fragment.PostVideoBottomSheetFragment;
import com.whizzly.collab_module.interfaces.OnSendTimeRange;
import com.whizzly.collab_module.interfaces.OnSuccessRecordMerge;
import com.whizzly.collab_module.viewmodel.SuggestionPreviewActivityViewModel;
import com.whizzly.databinding.ActivityPreviewSuggestionBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.DownloadVideoModel;
import com.whizzly.models.video_list_response.Result;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;
import com.whizzly.utils.FFMpegCommandListener;
import com.whizzly.utils.FFMpegCommands;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

public class PreviewSuggestionActivity extends BaseActivity implements OnClickSendListener, OnSuccessRecordMerge, OnSendTimeRange/*, OnFinalMergeVideo*/ {

    public ArrayList<VideoFramesBean> mVideoFrameBeanList = new ArrayList<>();
    private ActivityPreviewSuggestionBinding mActivityPreviewSuggestionBinding;
    private SuggestionPreviewActivityViewModel mSuggestionPreviewActivityViewModel;
    private ExoPlayer mExoPlayer;
    private String mMergedVideoPath = "";
    private FFMpegCommands mFfMpegCommands;
    private ArrayList<DownloadVideoModel> mDownloadVideoModels;
    private Result mResult;
    private int templeteSize;
    private int frameNo = 0;
    private ArrayList<String> suggestedVideo;
    private String mergedVideo = "";
    private OnSuccessRecordMerge onSuccessRecordMerge;
    private String videoLength;
    private String mVideoThumbnailUrl;
    private String mAudioFilePath, mVideoMuteFilePath;
    private String mConcatenatedFile = "";
    private String mImage;
    private PostVideoBottomSheetFragment postVideoBottomSheetFragment;
    private String scaledVideo = "";
    private String newVideoFile;
    private ArrayList<CollabBarBean> mCollabBarBeans = new ArrayList<>();
    private int frameWidth;
    private long durationInMillies = 0;
    private String userId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityPreviewSuggestionBinding = DataBindingUtil.setContentView(this, R.layout.activity_preview_suggestion);
        mSuggestionPreviewActivityViewModel = ViewModelProviders.of(this).get(SuggestionPreviewActivityViewModel.class);
        mActivityPreviewSuggestionBinding.setViewModel(mSuggestionPreviewActivityViewModel);
        mSuggestionPreviewActivityViewModel.setOnClickSendListener(this);
        getData();
        setAdditionalInfoToBar(mCollabBarBeans);
        mActivityPreviewSuggestionBinding.ivCollabBar.setOnClickListener(view -> {
            mActivityPreviewSuggestionBinding.ivCollabBar.setEnabled(false);
            new Handler().postDelayed(() -> mActivityPreviewSuggestionBinding.ivCollabBar.setEnabled(true), 2000);
            CollabBarFragment collabBarFragment = new CollabBarFragment();
            Bundle collabBundle = new Bundle();
            collabBundle.putString(AppConstants.ClassConstants.VIDEO_FILE_PATH, mMergedVideoPath);
            collabBundle.putInt(AppConstants.ClassConstants.TEMPLETE_SIZE, templeteSize);
            collabBundle.putString(AppConstants.ClassConstants.VIDEO_FILE_URI, Uri.fromFile(new File(mMergedVideoPath)).toString());
            collabBundle.putString(AppConstants.ClassConstants.VIDEO_LENGTH, String.valueOf(durationInMillies));
            collabBundle.putInt(AppConstants.ClassConstants.FRAME_SIZE, frameWidth);
            if (mCollabBarBeans.size() > 0) {
                collabBundle.putParcelableArrayList(AppConstants.ClassConstants.COLOR_BAR_DATA, mCollabBarBeans);
            }
            collabBundle.putLong(AppConstants.ClassConstants.DURATION_IN_MILLIES, durationInMillies);
            collabBundle.putParcelableArrayList(AppConstants.ClassConstants.FRAMES_LIST, mVideoFrameBeanList);
            collabBarFragment.setArguments(collabBundle);
            collabBarFragment.setCancelable(false);
            collabBarFragment.show(getSupportFragmentManager(), CollabBarFragment.class.getCanonicalName());
        });
        mActivityPreviewSuggestionBinding.flProgressBar.setVisibility(View.VISIBLE);

        if (userId.equals(String.valueOf(DataManager.getInstance().getUserId()))) {
            mActivityPreviewSuggestionBinding.ivCollabBar.setVisibility(View.VISIBLE);
        } else {
            mActivityPreviewSuggestionBinding.ivCollabBar.setVisibility(View.GONE);
        }

        postVideoBottomSheetFragment = new PostVideoBottomSheetFragment();
        onSuccessRecordMerge = this;
        mFfMpegCommands = FFMpegCommands.getInstance(this);
        if (suggestedVideo.size() > 1) {
            try {
                mFfMpegCommands.initializeFFmpeg(new FFMpegCommandListener() {
                    @Override
                    public void onSuccess(String message) {
                        concatVideos(addVideosPathToTextFile(suggestedVideo));
                    }

                    @Override
                    public void onProgress(String message) {

                    }

                    @Override
                    public void onFailure(String message) {

                    }

                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onFinish() {

                    }
                });
            } catch (FFmpegNotSupportedException e) {
                e.printStackTrace();
            }
        } else {
            try {
                mFfMpegCommands.initializeFFmpeg();
            } catch (FFmpegNotSupportedException e) {
                e.printStackTrace();
            }
            newVideoFile = suggestedVideo.get(0);
            getFrameFromVideo();
            new ExtractAudio().execute();
            /*mFfMpegCommands.increaseAudioInVideo(suggestedVideo.get(0), newVideoFile, new FFMpegCommandListener() {
                @Override
                public void onSuccess(String message) {
                    getFrameFromVideo();
                    new ExtractAudio().execute();
                }

                @Override
                public void onProgress(String message) {

                }

                @Override
                public void onFailure(String message) {

                }

                @Override
                public void onStart() {

                }

                @Override
                public void onFinish() {

                }
            });*/


        }
    }

    private void setExoPlayer() {
        mExoPlayer = ExoPlayerFactory.newSimpleInstance(this,
                new DefaultRenderersFactory(this),
                new DefaultTrackSelector(), new DefaultLoadControl());
        mExoPlayer.setPlayWhenReady(true);
        mExoPlayer.setRepeatMode(Player.REPEAT_MODE_ONE);
        Uri uri = Uri.parse(mMergedVideoPath);
        MediaSource mediaSource = buildMediaSource(uri);
        mExoPlayer.prepare(mediaSource);
        mActivityPreviewSuggestionBinding.pvPreview.setPlayer(mExoPlayer);
    }

    private void getFramesFromVideo() {
        try {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            try {
                retriever.setDataSource(this, Uri.fromFile(new File(mMergedVideoPath)));
            } catch (Exception e) {
                System.out.println("Exception= " + e);
            }
            mVideoFrameBeanList.clear();
            String duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            if (duration != null) {
                durationInMillies = Long.parseLong(duration); //duration in millisec
                int numberOfFrames = getNumberOfFramesOnScreen();
                frameWidth = getWindowManager().getDefaultDisplay().getWidth() / numberOfFrames;
                int timeInterval = (int) (durationInMillies / numberOfFrames);
                for (int i = 1; i <= numberOfFrames; i++) {
                    VideoFramesBean videoFramesBean = new VideoFramesBean();
                    videoFramesBean.setFrame(retriever.getFrameAtTime((long) (timeInterval * i * 1000000), MediaMetadataRetriever.OPTION_CLOSEST_SYNC));
                    mVideoFrameBeanList.add(videoFramesBean);
                }
            }
            retriever.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getNumberOfFramesOnScreen() {
        int number = 1;
        int screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        for (int i = 60; i < screenWidth; i++) {
            if (screenWidth % i == 0) {
                number = i;
                break;
            }
        }
        return screenWidth / (number * 2);
    }

    private void setAdditionalInfoToBar(ArrayList<CollabBarBean> mCollabBarBeans) {
        mActivityPreviewSuggestionBinding.rlColorBar.removeAllViews();
        int screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) mActivityPreviewSuggestionBinding.rlColorBar.getLayoutParams();
        for (int i = 0; i < mCollabBarBeans.size(); i++) {
            long durationInMillies = mCollabBarBeans.get(i).getDurationInMillies();
            View view = new View(this);
            float start = (mCollabBarBeans.get(i).getStartTime() * screenWidth) / durationInMillies;
            float end = (mCollabBarBeans.get(i).getEndTime() * screenWidth) / durationInMillies;
            float width = end - start;
            view.setBackgroundColor(Color.parseColor(mCollabBarBeans.get(i).getColorCode()));
            RelativeLayout.LayoutParams viewLayoutParams = null;
            int templateSize = mResult.getTemplateSize();
            switch (mCollabBarBeans.get(i).getCode()) {
                case "0":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(20 / (templateSize + 1), this));
                    break;
                case "1":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(2 * (20 / (templateSize + 1)), this));
                    break;
                case "2":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(3 * (20 / (templateSize + 1)), this));
                    break;
                case "3":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(4 * (20 / (templateSize + 1)), this));
                    break;
                case "4":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(5 * (20 / (templateSize + 1)), this));
                    break;
                case "5":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(6 * (20 / (templateSize + 1)), this));
                    break;
                case "6":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY(0);
                    break;
            }
            view.setX(start);
            view.setLayoutParams(viewLayoutParams);
            mActivityPreviewSuggestionBinding.rlColorBar.addView(view);
        }
        mActivityPreviewSuggestionBinding.rlColorBar.setLayoutParams(layoutParams);
    }

    private int dpToPx(float size, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, size, context.getResources().getDisplayMetrics());
    }

    private MediaSource buildMediaSource(Uri uri) {
        return new ExtractorMediaSource.Factory(
                new DefaultDataSourceFactory(this, "whizzly-app")).
                createMediaSource(uri);
    }

    private String getVideoFilePath() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "merged_video" + System.currentTimeMillis() + ".mp4";
    }

    private void getData() {
        Intent previewIntent = getIntent();
        if (previewIntent != null) {
            mResult = previewIntent.getParcelableExtra(AppConstants.ClassConstants.SUGGESTED_VIDEO_RESULT_DATA);
            suggestedVideo = (ArrayList<String>) previewIntent.getSerializableExtra(AppConstants.ClassConstants.SUGGESTED_VIDEO_URL);
            frameNo = previewIntent.getIntExtra(AppConstants.ClassConstants.FRAME_NUMBER, 0);
            mDownloadVideoModels = (ArrayList<DownloadVideoModel>) previewIntent.getSerializableExtra(AppConstants.ClassConstants.SUGGESTED_VIDEO_DOWNLOAD_VIDEO);
            templeteSize = mResult.getTemplateSize();
            userId = previewIntent.getStringExtra(AppConstants.ClassConstants.USER_ID);
            mCollabBarBeans = new Gson().fromJson(mResult.getAdditinalInfo(), new TypeToken<ArrayList<CollabBarBean>>() {
            }.getType());
        }
    }

    private String getConcatFile() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "concat_" + System.currentTimeMillis() + ".mp4";
    }

    private String addVideosPathToTextFile(ArrayList<String> videoPathList) {
        final File dir = getExternalFilesDir(null);
        String textFile = (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "concat_text_" + System.currentTimeMillis() + ".txt";
        File file = new File(textFile);
        try {
            FileOutputStream f = new FileOutputStream(file);
            PrintWriter pw = new PrintWriter(f);
            for (int i = 0; i < videoPathList.size(); i++) {
                pw.println("file " + videoPathList.get(i));
            }
            pw.flush();
            pw.close();
            f.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return textFile;
    }

    private String getAudioFilePath(Context context) {
        final File dir = context.getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "audio_" + System.currentTimeMillis() + ".mp4";
    }

    private String getVideoFile() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "videoWithOutAudio_" + System.currentTimeMillis() + ".mp4";
    }

    private String getScaledVideo() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "scaledVideo_" + System.currentTimeMillis() + ".mp4";
    }

    private void extractAudio() {

        if (suggestedVideo.size() == 1) {
            mAudioFilePath = getAudioFilePath(this);
            mVideoMuteFilePath = getVideoFile();
            mFfMpegCommands.getAudioFile(newVideoFile, mAudioFilePath, new FFMpegCommandListener() {
                @Override
                public void onSuccess(String message) {
                    mFfMpegCommands.getVideoFile(newVideoFile, mVideoMuteFilePath, new FFMpegCommandListener() {
                        @Override
                        public void onSuccess(String message) {
                            scaleVideo();
                        }

                        @Override
                        public void onProgress(String message) {

                        }

                        @Override
                        public void onFailure(String message) {

                        }

                        @Override
                        public void onStart() {

                        }

                        @Override
                        public void onFinish() {

                        }
                    });
                }

                @Override
                public void onProgress(String message) {

                }

                @Override
                public void onFailure(String message) {

                }

                @Override
                public void onStart() {

                }

                @Override
                public void onFinish() {

                }
            });

        } else if (suggestedVideo.size() > 0) {
            mAudioFilePath = getAudioFilePath(this);
            mVideoMuteFilePath = getVideoFile();
            mFfMpegCommands.getAudioFile(newVideoFile, mAudioFilePath, new FFMpegCommandListener() {
                @Override
                public void onSuccess(String message) {
                    mFfMpegCommands.getVideoFile(newVideoFile, mVideoMuteFilePath, new FFMpegCommandListener() {
                        @Override
                        public void onSuccess(String message) {

                        }

                        @Override
                        public void onProgress(String message) {

                        }

                        @Override
                        public void onFailure(String message) {

                        }

                        @Override
                        public void onStart() {

                        }

                        @Override
                        public void onFinish() {

                        }
                    });
                }

                @Override
                public void onProgress(String message) {

                }

                @Override
                public void onFailure(String message) {

                }

                @Override
                public void onStart() {

                }

                @Override
                public void onFinish() {

                }
            });
        }
    }

    private void concatVideos(String textFile) {
        mConcatenatedFile = getConcatFile();
        mFfMpegCommands.concatenateVideos(textFile, mConcatenatedFile, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                newVideoFile = mConcatenatedFile;
                getFrameFromVideo();
                new ExtractAudio().execute();
                /*mFfMpegCommands.increaseAudioInVideo(mConcatenatedFile, newVideoFile, new FFMpegCommandListener() {
                    @Override
                    public void onSuccess(String message) {

                        getFrameFromVideo();
                        new ExtractAudio().execute();
                    }

                    @Override
                    public void onProgress(String message) {

                    }

                    @Override
                    public void onFailure(String message) {

                    }

                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onFinish() {

                    }
                });*/

            }

            @Override
            public void onProgress(String message) {

            }

            @Override
            public void onFailure(String message) {
            }

            @Override
            public void onStart() {

            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void callMergeCommands() {
        DownloadVideoModel downloadVideoModel = new DownloadVideoModel();
//        downloadVideoModel.setAudioFile(mAudioFilePath);
        downloadVideoModel.setDownlaodUri(Uri.fromFile(new File(scaledVideo)).toString());
        downloadVideoModel.setFrameNumber(frameNo);
        mDownloadVideoModels.add(downloadVideoModel);
        Collections.sort(mDownloadVideoModels, (o1, o2) -> {
            return String.valueOf(o1.getFrameNumber()).compareTo(String.valueOf(o2.getFrameNumber()));
        });

        int size = mDownloadVideoModels.size();
        if (size > templeteSize) {
            size = templeteSize;
        }

        switch (size) {
            case 2:
                mergedVideo = getVideoFilePath();
                mergeTwoVideos(mDownloadVideoModels.get(0).getDownlaodUri(), mDownloadVideoModels.get(1).getDownlaodUri(), mergedVideo);
                mActivityPreviewSuggestionBinding.pvPreview.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
                break;
            case 3:
                mergedVideo = getVideoFilePath();
                mergeThreeVideos(mDownloadVideoModels.get(0).getDownlaodUri(), mDownloadVideoModels.get(1).getDownlaodUri(), mDownloadVideoModels.get(2).getDownlaodUri(), mergedVideo);
                mActivityPreviewSuggestionBinding.pvPreview.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
                break;
            case 4:
                mergedVideo = getVideoFilePath();
                mergeFourVideos(mDownloadVideoModels.get(0).getDownlaodUri(), mDownloadVideoModels.get(1).getDownlaodUri(), mDownloadVideoModels.get(2).getDownlaodUri(), mDownloadVideoModels.get(3).getDownlaodUri(), mergedVideo);
                mActivityPreviewSuggestionBinding.pvPreview.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
                break;
            case 5:
                mergedVideo = getVideoFilePath();
                mergeFiveVideos(mDownloadVideoModels.get(0).getDownlaodUri(), mDownloadVideoModels.get(1).getDownlaodUri(), mDownloadVideoModels.get(2).getDownlaodUri(), mDownloadVideoModels.get(3).getDownlaodUri(), mDownloadVideoModels.get(4).getDownlaodUri(), mergedVideo);
                mActivityPreviewSuggestionBinding.pvPreview.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
                break;
            case 6:
                mergedVideo = getVideoFilePath();
                mergeSixVideos(mDownloadVideoModels.get(0).getDownlaodUri(), mDownloadVideoModels.get(1).getDownlaodUri(), mDownloadVideoModels.get(2).getDownlaodUri(), mDownloadVideoModels.get(3).getDownlaodUri(), mDownloadVideoModels.get(4).getDownlaodUri(), mDownloadVideoModels.get(5).getDownlaodUri(), mergedVideo);
                mActivityPreviewSuggestionBinding.pvPreview.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mExoPlayer != null) {
            mExoPlayer.setPlayWhenReady(false);
            mExoPlayer = null;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mExoPlayer != null)
            mExoPlayer.release();
    }

    private void discardRecording() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.txt_discard_video)
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
                    finishAffinity();
                    Intent intent = new Intent(this, HomeActivity.class);
                    startActivity(intent);
                })
                .setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss())
                .create().show();
    }

    @Override
    public void onBackPressed() {
        discardRecording();
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants
                    .ClassConstants.SUGGESTION_CLOSE_CLICKED:
                discardRecording();
                break;
            case AppConstants
                    .ClassConstants.SUGGESTION_POST_CLICKED:
                openBottomFragment();
                break;
        }
    }

    private void getFramesFromSuggestedVideo() {
        mVideoThumbnailUrl = getImageDirectory();
        mFfMpegCommands.getFrameFromVideo(mMergedVideoPath, mVideoThumbnailUrl, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                Log.e("onSuccess: ", message);
                mActivityPreviewSuggestionBinding.flProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onProgress(String message) {
                Log.e("onProgress: ", message);

            }

            @Override
            public void onFailure(String message) {
                Log.e("onFailure: ", message);

            }

            @Override
            public void onStart() {

            }

            @Override
            public void onFinish() {
                mActivityPreviewSuggestionBinding.flProgressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getFrameFromVideo() {
        mImage = getImageDirectory();
        mFfMpegCommands.getFrameFromVideo(newVideoFile, mImage, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                VideoFramesBean videoFramesBean = new VideoFramesBean();
                videoFramesBean.setFrame(BitmapFactory.decodeFile(mImage));
            }

            @Override
            public void onProgress(String message) {

            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onStart() {

            }

            @Override
            public void onFinish() {

            }
        });
    }

    private String getImageDirectory() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "image_" + System.currentTimeMillis() + ".png";
    }

    private void openBottomFragment() {
        Bundle postVideoArguments = new Bundle();
        CollabPostModel collabPostModel = new CollabPostModel();
        collabPostModel.setCollabId(mResult.getCollabId());
        collabPostModel.setCollabMusicId(mResult.getMusicId());
        collabPostModel.setCollabDuration(Integer.valueOf(videoLength));
        collabPostModel.setUserId(DataManager.getInstance().getUserId());
        collabPostModel.setCollabUrl(mMergedVideoPath);
        collabPostModel.setCollabTotalFrames(mResult.getTemplateSize());
        collabPostModel.setCollabTitle(mResult.getTitle());
        collabPostModel.setCollabWatermarkUrl(null);
        collabPostModel.setCollabThumbnailUrl(mVideoThumbnailUrl);
        collabPostModel.setCollabMusicUrl(mAudioFilePath);
        collabPostModel.setCollabHashtags(mResult.getHashtag());
        collabPostModel.setCollabPrivacyTypeId(null);
        collabPostModel.setCollabAdditionalInfo(new Gson().toJson(mCollabBarBeans));
        ArrayList<CollabVideoArray> collabVideoArrays = new ArrayList<>();
        CollabVideoArray collabVideoArray = new CollabVideoArray();
        collabVideoArray.setVideoDuration(Integer.valueOf(videoLength));
        collabVideoArray.setVideoIsMandatory(0);
        collabVideoArray.setVideoThumbnailUrl(mImage);
        collabVideoArray.setVideoFrameNo(frameNo);
        collabVideoArray.setVideoTitle("");
        collabVideoArray.setVideoUrl(mVideoMuteFilePath);
        collabVideoArrays.add(collabVideoArray);
        collabPostModel.setCollabVideoArray(collabVideoArrays);
        postVideoArguments.putParcelable("post_data", collabPostModel);
        postVideoArguments.putString("is_from", "0");
        postVideoBottomSheetFragment.setArguments(postVideoArguments);
        postVideoBottomSheetFragment.show(getSupportFragmentManager(), PostVideoBottomSheetFragment.class.getCanonicalName());
    }

    private void mergeTwoVideos(String fileOne, String fileTwo, String finalFile) {
        try {
            mFfMpegCommands.mergeTwoVideos(fileOne, fileTwo, finalFile, new FFMpegCommandListener() {
                @Override
                public void onSuccess(String message) {
                    onSuccessRecordMerge.onMergeSuccess();
                }

                @Override
                public void onProgress(String message) {
                }

                @Override
                public void onFailure(String message) {
                }

                @Override
                public void onStart() {
                }

                @Override
                public void onFinish() {
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    private void mergeThreeVideos(String fileOne, String fileTwo, String fileThree, String finalFile) {
        try {
            mFfMpegCommands.mergeThreeVideos(fileOne, fileTwo, fileThree, finalFile, new FFMpegCommandListener() {
                @Override
                public void onSuccess(String message) {
                    onSuccessRecordMerge.onMergeSuccess();
                }

                @Override
                public void onProgress(String message) {

                }

                @Override
                public void onFailure(String message) {

                }

                @Override
                public void onStart() {
                }

                @Override
                public void onFinish() {
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    private void mergeFourVideos(String fileOne, String fileTwo, String fileThree, String fileFour, String finalFile) {
        try {
            mFfMpegCommands.mergeFourVideos(fileOne, fileTwo, fileThree, fileFour, finalFile, new FFMpegCommandListener() {
                @Override
                public void onSuccess(String message) {
                    onSuccessRecordMerge.onMergeSuccess();
                }

                @Override
                public void onProgress(String message) {

                }

                @Override
                public void onFailure(String message) {
                }

                @Override
                public void onStart() {
                }

                @Override
                public void onFinish() {
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    private void mergeFiveVideos(String fileOne, String fileTwo, String fileThree, String fileFour, String fileFive, String finalFile) {
        try {
            mFfMpegCommands.mergeFiveVideos(fileOne, fileTwo, fileThree, fileFour, fileFive, finalFile, new FFMpegCommandListener() {
                @Override
                public void onSuccess(String message) {
                    onSuccessRecordMerge.onMergeSuccess();
                }

                @Override
                public void onProgress(String message) {

                }

                @Override
                public void onFailure(String message) {
                }

                @Override
                public void onStart() {
                }

                @Override
                public void onFinish() {
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    private void mergeSixVideos(String downlaodUri, String downlaodUri1, String downlaodUri2, String downlaodUri3, String downloadUri4, String suggestedVideo, String mergedVideo) {
        try {
            mFfMpegCommands.mergeSixVideos(downlaodUri, downlaodUri1, downlaodUri2, downlaodUri3, downloadUri4, suggestedVideo, mergedVideo, new FFMpegCommandListener() {
                @Override
                public void onSuccess(String message) {
                    onSuccessRecordMerge.onMergeSuccess();
                }

                @Override
                public void onProgress(String message) {

                }

                @Override
                public void onFailure(String message) {
                }

                @Override
                public void onStart() {
                }

                @Override
                public void onFinish() {
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    private void mergeTwoAudio(String fileOne, String fileTwo, String finalFile) {

        mFfMpegCommands.mergeTwoAudioFiles(fileOne, fileTwo, finalFile, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                initializeFfmpeg(finalFile);
            }

            @Override
            public void onProgress(String message) {
            }

            @Override
            public void onFailure(String message) {
            }

            @Override
            public void onStart() {
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void mergeThreeAudios(String fileOne, String fileTwo, String fileThree, String finalFile, String finalFileTwo) {
        mFfMpegCommands.mergeThreeAudioFiles(fileOne, fileTwo, fileThree, finalFile, finalFileTwo, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                initializeFfmpeg(finalFile);
            }

            @Override
            public void onProgress(String message) {

            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onStart() {
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void mergeFourAudios(String fileOne, String fileTwo, String fileThree, String fileFour, String finalFile, String finalFileTwo, String finalFileThree) {
        mFfMpegCommands.mergeFourAudioFiles(fileOne, fileTwo, fileThree, fileFour, finalFile, finalFileTwo, finalFileThree, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                initializeFfmpeg(finalFile);
            }

            @Override
            public void onProgress(String message) {

            }

            @Override
            public void onFailure(String message) {
            }

            @Override
            public void onStart() {
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void mergeFiveAudios(String fileOne, String fileTwo, String fileThree, String fileFour, String fileFive, String finalFile, String finalFileTwo, String finalFileThree, String finalFileFour) {
        mFfMpegCommands.mergeFiveAudioFiles(fileOne, fileTwo, fileThree, fileFour, fileFive, finalFile, finalFileTwo, finalFileThree, finalFileFour, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                initializeFfmpeg(finalFile);
            }

            @Override
            public void onProgress(String message) {

            }

            @Override
            public void onFailure(String message) {
            }

            @Override
            public void onStart() {
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void mergeSixAudios(String fileOne, String fileTwo, String fileThree, String fileFour, String fileFive, String fileSix, String finalFile, String finalFileTwo, String finalFileThree, String finalFileFour, String finalFileFive) {
        mFfMpegCommands.mergeSixAudioFiles(fileOne, fileTwo, fileThree, fileFour, fileFive, fileSix, finalFile, finalFileTwo, finalFileThree, finalFileFour, finalFileFive, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                initializeFfmpeg(finalFileFive);
            }

            @Override
            public void onProgress(String message) {

            }

            @Override
            public void onFailure(String message) {
            }

            @Override
            public void onStart() {
            }

            @Override
            public void onFinish() {
            }
        });
    }

    @Override
    public void onMergeSuccess() {
        int size = mDownloadVideoModels.size() + 1;
        if (size > templeteSize) {
            size = templeteSize;
        }
        switch (size) {
            case 2:
                if (mDownloadVideoModels.get(1).getAudioFile() != null && !mDownloadVideoModels.get(1).getAudioFile().isEmpty())
                    initializeFfmpeg(mDownloadVideoModels.get(1).getAudioFile());
                else if(mDownloadVideoModels.get(0).getAudioFile() != null && !mDownloadVideoModels.get(0).getAudioFile().isEmpty()){
                    initializeFfmpeg(mDownloadVideoModels.get(0).getAudioFile());
                }
//                    mergeTwoAudio(mDownloadVideoModels.get(0).getAudioFile(), mAudioFilePath, getAudioFilePath(this));
                break;
            case 3:
                if (mDownloadVideoModels.get(0).getAudioFile() != null && !mDownloadVideoModels.get(1).getAudioFile().isEmpty() && mDownloadVideoModels.get(0).getAudioFile() != null && !mDownloadVideoModels.get(1).getAudioFile().isEmpty())
                    mergeTwoAudio(mDownloadVideoModels.get(0).getAudioFile(), mDownloadVideoModels.get(1).getAudioFile(), getAudioFilePath(this));
//                    mergeThreeAudios(mDownloadVideoModels.get(0).getAudioFile(), mDownloadVideoModels.get(1).getAudioFile(), mAudioFilePath, getAudioFilePath(this), getAudioFilePath(this));
                else if (mDownloadVideoModels.get(0).getAudioFile() != null && !mDownloadVideoModels.get(0).getAudioFile().isEmpty())
                    initializeFfmpeg(mDownloadVideoModels.get(0).getAudioFile());
//                    mergeTwoAudio(mDownloadVideoModels.get(0).getAudioFile(), mAudioFilePath, getAudioFilePath(this));
                else if (mDownloadVideoModels.get(1).getAudioFile() != null && !mDownloadVideoModels.get(1).getAudioFile().isEmpty())
                    initializeFfmpeg(mDownloadVideoModels.get(1).getAudioFile());
//                    mergeTwoAudio(mDownloadVideoModels.get(1).getAudioFile(), mAudioFilePath, getAudioFilePath(this));
                break;
            case 4:
                if (mDownloadVideoModels.get(0).getAudioFile() != null && !mDownloadVideoModels.get(0).getAudioFile().isEmpty() && mDownloadVideoModels.get(1).getAudioFile() != null && !mDownloadVideoModels.get(1).getAudioFile().isEmpty() && mDownloadVideoModels.get(2).getAudioFile() != null && !mDownloadVideoModels.get(2).getAudioFile().isEmpty())
                    mergeThreeAudios(mDownloadVideoModels.get(0).getAudioFile(), mDownloadVideoModels.get(1).getAudioFile(), mDownloadVideoModels.get(2).getAudioFile(), getAudioFilePath(this), getAudioFilePath(this));
//                    mergeFourAudios(mDownloadVideoModels.get(0).getAudioFile(), mDownloadVideoModels.get(1).getAudioFile(), mDownloadVideoModels.get(2).getAudioFile(), mAudioFilePath, getAudioFilePath(this), getAudioFilePath(this), getAudioFilePath(this));
                else if (mDownloadVideoModels.get(0).getAudioFile() != null && !mDownloadVideoModels.get(0).getAudioFile().isEmpty() && mDownloadVideoModels.get(1).getAudioFile() != null && !mDownloadVideoModels.get(1).getAudioFile().isEmpty())
                    mergeTwoAudio(mDownloadVideoModels.get(0).getAudioFile(), mDownloadVideoModels.get(1).getAudioFile(), getAudioFilePath(this));
//                    mergeThreeAudios(mDownloadVideoModels.get(0).getAudioFile(), mDownloadVideoModels.get(1).getAudioFile(), mAudioFilePath, getAudioFilePath(this), getAudioFilePath(this));
                else if (mDownloadVideoModels.get(0).getAudioFile() != null && !mDownloadVideoModels.get(0).getAudioFile().isEmpty() && mDownloadVideoModels.get(2).getAudioFile() != null && !mDownloadVideoModels.get(2).getAudioFile().isEmpty())
                    mergeTwoAudio(mDownloadVideoModels.get(0).getAudioFile(), mDownloadVideoModels.get(1).getAudioFile(), getAudioFilePath(this));
//                    mergeThreeAudios(mDownloadVideoModels.get(0).getAudioFile(), mDownloadVideoModels.get(1).getAudioFile(), mAudioFilePath, getAudioFilePath(this), getAudioFilePath(this));
                else if (mDownloadVideoModels.get(1).getAudioFile() != null && !mDownloadVideoModels.get(1).getAudioFile().isEmpty() && mDownloadVideoModels.get(2).getAudioFile() != null && !mDownloadVideoModels.get(2).getAudioFile().isEmpty())
                    mergeTwoAudio(mDownloadVideoModels.get(1).getAudioFile(), mDownloadVideoModels.get(2).getAudioFile(), getAudioFilePath(this));
//                    mergeThreeAudios(mDownloadVideoModels.get(1).getAudioFile(), mDownloadVideoModels.get(2).getAudioFile(), mAudioFilePath, getAudioFilePath(this), getAudioFilePath(this));
                else if (mDownloadVideoModels.get(0).getAudioFile() != null && !mDownloadVideoModels.get(0).getAudioFile().isEmpty())
                    initializeFfmpeg(mDownloadVideoModels.get(0).getAudioFile());
//                    mergeTwoAudio(mDownloadVideoModels.get(0).getAudioFile(), mAudioFilePath, getAudioFilePath(this));
                else if (mDownloadVideoModels.get(1).getAudioFile() != null && !mDownloadVideoModels.get(1).getAudioFile().isEmpty())
                    initializeFfmpeg(mDownloadVideoModels.get(1).getAudioFile());
//                    mergeTwoAudio(mDownloadVideoModels.get(1).getAudioFile(), mAudioFilePath, getAudioFilePath(this));
                else if (mDownloadVideoModels.get(2).getAudioFile() != null && !mDownloadVideoModels.get(2).getAudioFile().isEmpty())
                    initializeFfmpeg(mDownloadVideoModels.get(2).getAudioFile());
//                    mergeTwoAudio(mDownloadVideoModels.get(2).getAudioFile(), mAudioFilePath, getAudioFilePath(this));
                break;
            case 5:
                if (mDownloadVideoModels.get(0).getAudioFile() != null && !mDownloadVideoModels.get(0).getAudioFile().isEmpty() &&
                        mDownloadVideoModels.get(1).getAudioFile() != null && !mDownloadVideoModels.get(1).getAudioFile().isEmpty() &&
                        mDownloadVideoModels.get(2).getAudioFile() != null && !mDownloadVideoModels.get(2).getAudioFile().isEmpty() &&
                        mDownloadVideoModels.get(3).getAudioFile() != null && !mDownloadVideoModels.get(3).getAudioFile().isEmpty())
                    mergeFourAudios(mDownloadVideoModels.get(0).getAudioFile(), mDownloadVideoModels.get(1).getAudioFile(),
                            mDownloadVideoModels.get(2).getAudioFile(), mDownloadVideoModels.get(3).getAudioFile(),
                            getAudioFilePath(this),
                            getAudioFilePath(this), getAudioFilePath(this));
                    /*mergeFiveAudios(mDownloadVideoModels.get(0).getAudioFile(), mDownloadVideoModels.get(1).getAudioFile(),
                            mDownloadVideoModels.get(2).getAudioFile(), mDownloadVideoModels.get(3).getAudioFile(),
                            mAudioFilePath, getAudioFilePath(this), getAudioFilePath(this),
                            getAudioFilePath(this), getAudioFilePath(this));*/
                else if (mDownloadVideoModels.get(0).getAudioFile() != null && !mDownloadVideoModels.get(0).getAudioFile().isEmpty() &&
                        mDownloadVideoModels.get(1).getAudioFile() != null && !mDownloadVideoModels.get(1).getAudioFile().isEmpty() &&
                        mDownloadVideoModels.get(2).getAudioFile() != null && !mDownloadVideoModels.get(2).getAudioFile().isEmpty())
                    mergeThreeAudios(mDownloadVideoModels.get(0).getAudioFile(), mDownloadVideoModels.get(1).getAudioFile(),
                            mDownloadVideoModels.get(2).getAudioFile(),
                            getAudioFilePath(this),
                            getAudioFilePath(this));
                    /*mergeFourAudios(mDownloadVideoModels.get(0).getAudioFile(), mDownloadVideoModels.get(1).getAudioFile(),
                            mDownloadVideoModels.get(2).getAudioFile(),
                            mAudioFilePath, getAudioFilePath(this), getAudioFilePath(this),
                            getAudioFilePath(this));*/
                else if (mDownloadVideoModels.get(0).getAudioFile() != null && !mDownloadVideoModels.get(0).getAudioFile().isEmpty() &&
                        mDownloadVideoModels.get(1).getAudioFile() != null && !mDownloadVideoModels.get(1).getAudioFile().isEmpty() &&
                        mDownloadVideoModels.get(3).getAudioFile() != null && !mDownloadVideoModels.get(3).getAudioFile().isEmpty())
                    mergeThreeAudios(mDownloadVideoModels.get(0).getAudioFile(), mDownloadVideoModels.get(1).getAudioFile(),
                            mDownloadVideoModels.get(3).getAudioFile(),
                            getAudioFilePath(this),
                            getAudioFilePath(this));
                    /*mergeFourAudios(mDownloadVideoModels.get(0).getAudioFile(), mDownloadVideoModels.get(1).getAudioFile(),
                            mDownloadVideoModels.get(3).getAudioFile(),
                            mAudioFilePath, getAudioFilePath(this), getAudioFilePath(this),
                            getAudioFilePath(this));*/
                else if (mDownloadVideoModels.get(1).getAudioFile() != null && !mDownloadVideoModels.get(1).getAudioFile().isEmpty() &&
                        mDownloadVideoModels.get(2).getAudioFile() != null && !mDownloadVideoModels.get(2).getAudioFile().isEmpty() &&
                        mDownloadVideoModels.get(3).getAudioFile() != null && !mDownloadVideoModels.get(3).getAudioFile().isEmpty())
                    mergeThreeAudios(mDownloadVideoModels.get(1).getAudioFile(), mDownloadVideoModels.get(2).getAudioFile(),
                            mDownloadVideoModels.get(3).getAudioFile(),
                            getAudioFilePath(this),
                            getAudioFilePath(this));
                    /*mergeFourAudios(mDownloadVideoModels.get(1).getAudioFile(), mDownloadVideoModels.get(2).getAudioFile(),
                            mDownloadVideoModels.get(3).getAudioFile(),
                            mAudioFilePath, getAudioFilePath(this), getAudioFilePath(this),
                            getAudioFilePath(this));*/
                else if (mDownloadVideoModels.get(0).getAudioFile() != null && !mDownloadVideoModels.get(0).getAudioFile().isEmpty() &&
                        mDownloadVideoModels.get(1).getAudioFile() != null && !mDownloadVideoModels.get(1).getAudioFile().isEmpty())
                    mergeTwoAudio(mDownloadVideoModels.get(0).getAudioFile(), mDownloadVideoModels.get(1).getAudioFile(), getAudioFilePath(this));
//                    mergeThreeAudios(mDownloadVideoModels.get(0).getAudioFile(), mDownloadVideoModels.get(1).getAudioFile(), mAudioFilePath, getAudioFilePath(this), getAudioFilePath(this));
                else if (mDownloadVideoModels.get(0).getAudioFile() != null && !mDownloadVideoModels.get(0).getAudioFile().isEmpty() &&
                        mDownloadVideoModels.get(2).getAudioFile() != null && !mDownloadVideoModels.get(2).getAudioFile().isEmpty())
                    mergeTwoAudio(mDownloadVideoModels.get(0).getAudioFile(), mDownloadVideoModels.get(2).getAudioFile(), getAudioFilePath(this));
//                    mergeThreeAudios(mDownloadVideoModels.get(0).getAudioFile(), mDownloadVideoModels.get(2).getAudioFile(), mAudioFilePath, getAudioFilePath(this), getAudioFilePath(this));
                else if (mDownloadVideoModels.get(0).getAudioFile() != null && !mDownloadVideoModels.get(0).getAudioFile().isEmpty() &&
                        mDownloadVideoModels.get(3).getAudioFile() != null && !mDownloadVideoModels.get(3).getAudioFile().isEmpty())
                    mergeTwoAudio(mDownloadVideoModels.get(0).getAudioFile(), mDownloadVideoModels.get(3).getAudioFile(), getAudioFilePath(this));
//                    mergeThreeAudios(mDownloadVideoModels.get(0).getAudioFile(), mDownloadVideoModels.get(3).getAudioFile(), mAudioFilePath, getAudioFilePath(this), getAudioFilePath(this));
                else if (mDownloadVideoModels.get(1).getAudioFile() != null && !mDownloadVideoModels.get(1).getAudioFile().isEmpty() &&
                        mDownloadVideoModels.get(2).getAudioFile() != null && !mDownloadVideoModels.get(2).getAudioFile().isEmpty())
                    mergeTwoAudio(mDownloadVideoModels.get(1).getAudioFile(), mDownloadVideoModels.get(2).getAudioFile(), getAudioFilePath(this));
//                    mergeThreeAudios(mDownloadVideoModels.get(1).getAudioFile(), mDownloadVideoModels.get(2).getAudioFile(), mAudioFilePath, getAudioFilePath(this), getAudioFilePath(this));
                else if (mDownloadVideoModels.get(1).getAudioFile() != null && !mDownloadVideoModels.get(1).getAudioFile().isEmpty() &&
                        mDownloadVideoModels.get(3).getAudioFile() != null && !mDownloadVideoModels.get(3).getAudioFile().isEmpty())
                    mergeTwoAudio(mDownloadVideoModels.get(1).getAudioFile(), mDownloadVideoModels.get(3).getAudioFile(), getAudioFilePath(this));
//                    mergeThreeAudios(mDownloadVideoModels.get(1).getAudioFile(), mDownloadVideoModels.get(3).getAudioFile(), mAudioFilePath, getAudioFilePath(this), getAudioFilePath(this));
                else if (mDownloadVideoModels.get(2).getAudioFile() != null && !mDownloadVideoModels.get(2).getAudioFile().isEmpty() &&
                        mDownloadVideoModels.get(3).getAudioFile() != null && !mDownloadVideoModels.get(3).getAudioFile().isEmpty())
                    mergeTwoAudio(mDownloadVideoModels.get(2).getAudioFile(), mDownloadVideoModels.get(3).getAudioFile(), getAudioFilePath(this));
//                    mergeThreeAudios(mDownloadVideoModels.get(2).getAudioFile(), mDownloadVideoModels.get(3).getAudioFile(), mAudioFilePath, getAudioFilePath(this), getAudioFilePath(this));
                else if (mDownloadVideoModels.get(0).getAudioFile() != null && !mDownloadVideoModels.get(0).getAudioFile().isEmpty())
                    initializeFfmpeg(mDownloadVideoModels.get(0).getAudioFile());
//                    mergeTwoAudio(mDownloadVideoModels.get(0).getAudioFile(), mAudioFilePath, getAudioFilePath(this));
                else if (mDownloadVideoModels.get(1).getAudioFile() != null && !mDownloadVideoModels.get(1).getAudioFile().isEmpty())
                    initializeFfmpeg(mDownloadVideoModels.get(1).getAudioFile());
//                    mergeTwoAudio(mDownloadVideoModels.get(1).getAudioFile(), mAudioFilePath, getAudioFilePath(this));
                else if (mDownloadVideoModels.get(2).getAudioFile() != null && !mDownloadVideoModels.get(2).getAudioFile().isEmpty())
                    initializeFfmpeg(mDownloadVideoModels.get(2).getAudioFile());
//                    mergeTwoAudio(mDownloadVideoModels.get(2).getAudioFile(), mAudioFilePath, getAudioFilePath(this));
                else if (mDownloadVideoModels.get(3).getAudioFile() != null && !mDownloadVideoModels.get(3).getAudioFile().isEmpty())
                    initializeFfmpeg(mDownloadVideoModels.get(3).getAudioFile());
//                    mergeTwoAudio(mDownloadVideoModels.get(3).getAudioFile(), mAudioFilePath, getAudioFilePath(this));
                break;
            case 6:
                ArrayList<String> mMusicFiles = new ArrayList<>();
                for (int i = 0; i < mDownloadVideoModels.size(); i++) {
                    if (mDownloadVideoModels.get(i).getAudioFile() != null && !mDownloadVideoModels.get(i).getAudioFile().isEmpty()) {
                        mMusicFiles.add(mDownloadVideoModels.get(i).getAudioFile());
                    }
                }
                if (mMusicFiles.size() > 0) {
                    switch (mMusicFiles.size()) {
                        case 1:
                            initializeFfmpeg(mMusicFiles.get(0));
//                            mergeTwoAudio(mMusicFiles.get(0), mAudioFilePath, getAudioFilePath(this));
                            break;
                        case 2:
                            mergeTwoAudio(mMusicFiles.get(0), mMusicFiles.get(1), getAudioFilePath(this));
//                            mergeThreeAudios(mMusicFiles.get(0), mMusicFiles.get(1), mAudioFilePath, getAudioFilePath(this), getAudioFilePath(this));
                            break;
                        case 3:
                            mergeThreeAudios(mMusicFiles.get(0), mMusicFiles.get(1), mMusicFiles.get(2), getAudioFilePath(this), getAudioFilePath(this));
                           /* mergeFourAudios(mMusicFiles.get(0), mMusicFiles.get(1),
                                    mMusicFiles.get(2),
                                    mAudioFilePath, getAudioFilePath(this), getAudioFilePath(this),
                                    getAudioFilePath(this));*/
                            break;
                        case 4:
                            mergeFourAudios(mMusicFiles.get(0), mMusicFiles.get(1),
                                    mMusicFiles.get(2),
                                    mMusicFiles.get(3), getAudioFilePath(this), getAudioFilePath(this),
                                    getAudioFilePath(this));
                            /*mergeFiveAudios(mMusicFiles.get(0), mMusicFiles.get(1),
                                    mMusicFiles.get(2), mMusicFiles.get(3),
                                    mAudioFilePath, getAudioFilePath(this), getAudioFilePath(this),
                                    getAudioFilePath(this), getAudioFilePath(this));*/
                            break;
                        case 5:
                            mergeFiveAudios(mMusicFiles.get(0), mMusicFiles.get(1),
                                    mMusicFiles.get(2), mMusicFiles.get(3),
                                    mMusicFiles.get(4), getAudioFilePath(this), getAudioFilePath(this),
                                    getAudioFilePath(this), getAudioFilePath(this));
//                            mergeSixAudios(mMusicFiles.get(0), mMusicFiles.get(1), mMusicFiles.get(2), mMusicFiles.get(3), mMusicFiles.get(4), mAudioFilePath, getAudioFilePath(this), getAudioFilePath(this), getAudioFilePath(this), getAudioFilePath(this), getAudioFilePath(this));
                            break;
                    }
                }
                break;
        }
    }

    private String getFinalVideoFilePath() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "merged_with_audio_video" + System.currentTimeMillis() + ".mp4";
    }

    private void initializeFfmpeg(String finalFile) {
        mAudioFilePath = finalFile;
        mMergedVideoPath = getFinalVideoFilePath();
        mFfMpegCommands.mergeSongWithVideo(mergedVideo, finalFile, mMergedVideoPath, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                new Handler().postDelayed(() -> {
                    setExoPlayer();
                    getFramesFromSuggestedVideo();
                    getFramesFromVideo();
                    try{
                        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                        mediaMetadataRetriever.setDataSource(mMergedVideoPath);
                        String duration = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                        int duratio = Integer.parseInt(duration) / 1000;
                        if(duration.isEmpty()){
                            videoLength = mResult.getCollabInfo().get(0).getVideoDuration();
                        }
                    }catch (Exception e){
                        videoLength = mResult.getCollabInfo().get(0).getVideoDuration();
                    }

                }, 200);

            }

            @Override
            public void onProgress(String message) {

            }

            @Override
            public void onFailure(String message) {
                mActivityPreviewSuggestionBinding.flProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onStart() {

            }

            @Override
            public void onFinish() {

            }
        });

    }

    private void scaleVideo() {
        scaledVideo = getScaledVideo();
        mFfMpegCommands.scaleVideo(mVideoMuteFilePath, scaledVideo, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                callMergeCommands();
            }

            @Override
            public void onProgress(String message) {

            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onStart() {

            }

            @Override
            public void onFinish() {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mExoPlayer != null)
            mExoPlayer.setPlayWhenReady(true);
    }

    @Override
    public void onSendTimeRange(ArrayList<CollabBarBean> collabBarBeanArrayList) {
        mCollabBarBeans = collabBarBeanArrayList;
        setAdditionalInfoToBar(mCollabBarBeans);
    }

    private class ExtractAudio extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            mVideoMuteFilePath = newVideoFile;
            scaleVideo();
//            extractAudio();
            return mVideoMuteFilePath;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


        }
    }
}