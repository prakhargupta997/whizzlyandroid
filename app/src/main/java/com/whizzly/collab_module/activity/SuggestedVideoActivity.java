package com.whizzly.collab_module.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Point;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.daasuu.camerarecorder.CameraRecordListener;
import com.daasuu.camerarecorder.CameraRecorder;
import com.daasuu.camerarecorder.CameraRecorderBuilder;
import com.daasuu.camerarecorder.LensFacing;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.camera_utils.widget.SampleGLView;
import com.whizzly.collab_module.TimerDialog;
import com.whizzly.collab_module.beans.CollabBarBean;
import com.whizzly.collab_module.gallery_trimmer.CollabGalleryTrimmerActivity;
import com.whizzly.collab_module.interfaces.OnTimerDialogTimeSelector;
import com.whizzly.collab_module.model.SuggestedVideoModel;
import com.whizzly.collab_module.viewmodel.SuggestedVideoViewModel;
import com.whizzly.databinding.ActivitySuggestedVideoBinding;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.AnimationBean;
import com.whizzly.models.DownloadVideoModel;
import com.whizzly.models.video_list_response.CollabInfo;
import com.whizzly.models.video_list_response.Result;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.progress_with_divider.CountDownTimerWithPause;
import com.whizzly.utils.progress_with_divider.SegmentedProgressBar;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class SuggestedVideoActivity extends BaseActivity implements OnClickSendListener, OnTimerDialogTimeSelector {

    private static boolean isFirstTime = false;
    protected LensFacing lensFacing = LensFacing.FRONT;
    long durationInMillies = 0;
    private ActivitySuggestedVideoBinding mActivitySuggestedVideoBinding;
    private SuggestedVideoViewModel mSuggestedVideoViewModel;
    private SuggestedVideoModel mSuggestedVideoModel;
    private FrameLayout mFlCamera;
    private SampleGLView sampleGLView;
    private CountDownTimerWithPause mCountDownTimerWithPause;
    private CameraRecorder mCameraRecorder;
    private boolean mIsRecordingVideo;
    private String mNextVideoAbsolutePath;
    private ArrayList<Object> mCollabArrayList;
    private SegmentedProgressBar mPbRecordingBar;
    private int frameNo;
    private ArrayList<DownloadVideoModel> mDownloadedVideos = new ArrayList<>();
    private SimpleExoPlayer mExoplayerOne, mExoplayerTwo, mExoplayerThree, mExoplayerFour, mExoplayerFive, mExoplayerSix, mExoplayerMusic;
    private boolean playWhenReady = false;
    private Result mResult;
    private String mMusicUri;
    private ArrayList<Long> mProgress = new ArrayList<>();
    private ArrayList<String> mVideoStack;
    private String videoFromFileManager;
    private ArrayList<CollabBarBean> collabBarBeans = new ArrayList<>();
    private int currentVolume = -1;
    private int totalNoOfFrames = 2;
    private Handler handler = new Handler();
    private ArrayList<AnimationBean> mAnimationList = new ArrayList<>();
    private ImageView ivActiveRecording;
    private boolean isAnimationStarted = false;
    private String userId = "";
    private TextView tvTimer;

    public static String getPath(final Context context, final Uri uri) {
        // DocumentProvider
        if (DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == AppConstants.ClassConstants.VIDEO_INTENT_REQUEST_CODE) {
            boolean isPermitted = false;
            if (permissions.length == grantResults.length)
                for (int grantResult : grantResults) {
                    isPermitted = (grantResult == PackageManager.PERMISSION_GRANTED);
                    if (!isPermitted) {
                        break;
                    }
                }
            if (isPermitted) {
                setMusicPlayer(mMusicUri);
                setView();
                setProgressBar();
                if (checkPermissionsForCamera(this, null))
                    initializeCamera();
            } else {
                checkPermissionsForCamera(this, null);
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        releaseExoplayer();
        if (currentVolume != -1) {
            AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume, AudioManager.FLAG_PLAY_SOUND);
        }
    }

    private void getData() {
        Intent collabData = getIntent();
        if (collabData != null) {
            mMusicUri = collabData.getStringExtra(AppConstants.ClassConstants.MUSIC_URI);
            mResult = collabData.getParcelableExtra(AppConstants.ClassConstants.SUGGESTED_VIDEO_DATA);
            mDownloadedVideos = (ArrayList<DownloadVideoModel>) collabData.getSerializableExtra(AppConstants.ClassConstants.DOWNLOADED_VIDEOS);
            frameNo = collabData.getIntExtra(AppConstants.ClassConstants.FRAME_NUMBER, 1);
            userId = collabData.getStringExtra(AppConstants.ClassConstants.USER_ID);
            mCollabArrayList.addAll((ArrayList<Object>) collabData.getSerializableExtra(AppConstants.ClassConstants.SUGGESTED_VIDEO_COLLAB_LIST));
            Collections.sort(mDownloadedVideos);
        }
    }

    private void setAdditionalInfoToBar(String additionalInfoToBar) {
        collabBarBeans = new Gson().fromJson(additionalInfoToBar, new TypeToken<ArrayList<CollabBarBean>>() {
        }.getType());
        mActivitySuggestedVideoBinding.rlVideoColor.removeAllViews();
        int screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) mActivitySuggestedVideoBinding.rlVideoColor.getLayoutParams();
        if(collabBarBeans.size()>0) {
            for (int i = 0; i < collabBarBeans.size(); i++) {
                durationInMillies = collabBarBeans.get(i).getDurationInMillies();
                View view = new View(this);
                float start = (collabBarBeans.get(i).getStartTime() * screenWidth) / durationInMillies;
                float end = (collabBarBeans.get(i).getEndTime() * screenWidth) / durationInMillies;
                float width = end - start;
                view.setBackgroundColor(Color.parseColor(collabBarBeans.get(i).getColorCode()));
                RelativeLayout.LayoutParams viewLayoutParams = null;
                int templateSize = mResult.getTemplateSize();
                switch (collabBarBeans.get(i).getCode()) {
                    case "0":
                        viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                        view.setY((float) dpToPx(20 / (templateSize + 1), this));
                        break;
                    case "1":
                        viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                        view.setY((float) dpToPx(2 * (20 / (templateSize + 1)), this));
                        break;
                    case "2":
                        viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                        view.setY((float) dpToPx(3 * (20 / (templateSize + 1)), this));
                        break;
                    case "3":
                        viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                        view.setY((float) dpToPx(4 * (20 / (templateSize + 1)), this));
                        break;
                    case "4":
                        viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                        view.setY((float) dpToPx(5 * (20 / (templateSize + 1)), this));
                        break;
                    case "5":
                        viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                        view.setY((float) dpToPx(6 * (20 / (templateSize + 1)), this));
                        break;
                    case "6":
                        viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                        view.setY(0);
                        break;
                }
                view.setX(start);
                view.setLayoutParams(viewLayoutParams);
                mActivitySuggestedVideoBinding.rlVideoColor.addView(view);
            }
        }else{
            mActivitySuggestedVideoBinding.rlVideoColor.setBackgroundColor(getResources().getColor(R.color.green));
        }
        mActivitySuggestedVideoBinding.rlVideoColor.setLayoutParams(layoutParams);
    }

    private void addAdditionalInfo() {
        ArrayList<CollabBarBean> additionalInfo = new Gson().fromJson(mResult.getAdditinalInfo(), new TypeToken<ArrayList<CollabBarBean>>() {
        }.getType());
        if (additionalInfo != null && additionalInfo.size() > 0) {
            mActivitySuggestedVideoBinding.rlVideoColor.setVisibility(View.VISIBLE);
            setAdditionalInfoToBar(mResult.getAdditinalInfo());
        } else {
            mActivitySuggestedVideoBinding.rlVideoColor.setVisibility(View.VISIBLE);
        }
    }

    private int dpToPx(float size, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, size, context.getResources().getDisplayMetrics());
    }

    public boolean checkPermissionsForCamera(Activity context, Fragment fragment) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                if (fragment == null)
                    if (ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.CAMERA)
                            || ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.RECORD_AUDIO)) {
                        ActivityCompat.requestPermissions((context), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO}, AppConstants.ClassConstants.VIDEO_INTENT_REQUEST_CODE);
                    } else {
                        if (!isFirstTime) {
                            ActivityCompat.requestPermissions((context), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO}, AppConstants.ClassConstants.VIDEO_INTENT_REQUEST_CODE);
                            isFirstTime = true;
                        } else {
                            com.whizzly.dialog.ErrorDialog errorDialog = new com.whizzly.dialog.ErrorDialog();
                            if (context instanceof SuggestedVideoActivity)
                                errorDialog.show(((SuggestedVideoActivity) context).getSupportFragmentManager(), com.whizzly.dialog.ErrorDialog.class.getCanonicalName());
                        }
                    }
                else
                    ActivityCompat.requestPermissions((context), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO}, AppConstants.ClassConstants.VIDEO_INTENT_REQUEST_CODE);
                return false;
            } else
                return true;
        } else
            return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        mActivitySuggestedVideoBinding = DataBindingUtil.setContentView(this, R.layout.activity_suggested_video);
        mSuggestedVideoViewModel = ViewModelProviders.of(this).get(SuggestedVideoViewModel.class);
        mSuggestedVideoModel = new SuggestedVideoModel();
        mActivitySuggestedVideoBinding.setModel(mSuggestedVideoModel);
        mActivitySuggestedVideoBinding.setViewModel(mSuggestedVideoViewModel);
        mSuggestedVideoViewModel.setOnClickSendListener(this);
        mCollabArrayList = new ArrayList<>();
        mVideoStack = new ArrayList<>();
        clearStatusBar();
        getData();
        addAdditionalInfo();
        recordUsingPressedButton();
        stopRecording();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setMusicPlayer(mMusicUri);
        setView();
        setProgressBar();
        if (!getMicrophoneAvailable()) {
            AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
            currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, (int) (audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) * 0.75), AudioManager.FLAG_PLAY_SOUND);
        }
    }

    private boolean getMicrophoneAvailable() {
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        return audioManager.isWiredHeadsetOn();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void setMusicPlayer(String musicFileUri) {
        Context mContext = getApplicationContext();
        if (mExoplayerMusic == null) {
            mExoplayerMusic =
                    ExoPlayerFactory.newSimpleInstance(this,
                            new DefaultRenderersFactory(this), new DefaultTrackSelector(), new
                                    DefaultLoadControl());
        }
        mExoplayerMusic.setPlayWhenReady(playWhenReady);
        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(
                        mContext, Util.getUserAgent(mContext, "Whizzly"), null);
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        MediaSource mediaSource =
                new ExtractorMediaSource(
                        Uri.parse(musicFileUri), dataSourceFactory, extractorsFactory, null, null);
        mExoplayerMusic.prepare(mediaSource, true, true);
    }

    private void setView() {
        int numberOfFrames = mDownloadedVideos.size();
        totalNoOfFrames = numberOfFrames + 1;
        switch (totalNoOfFrames) {
            case 2:
                mActivitySuggestedVideoBinding.llCameraOneTwo.setVisibility(View.INVISIBLE);
                mActivitySuggestedVideoBinding.llVideoFiveSix.setVisibility(View.INVISIBLE);
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mActivitySuggestedVideoBinding.llCameraThreeFour.getLayoutParams();
                layoutParams.weight = 2.0f;
                mActivitySuggestedVideoBinding.llCameraThreeFour.setLayoutParams(layoutParams);
                mActivitySuggestedVideoBinding.llCameraThreeFour.setVisibility(View.VISIBLE);
                tvTimer = mActivitySuggestedVideoBinding.tvTimerThree;
                mFlCamera = mActivitySuggestedVideoBinding.actvThree;
                mPbRecordingBar = mActivitySuggestedVideoBinding.pbRecordThree;
                selectFrames(View.GONE, View.GONE, View.VISIBLE, View.VISIBLE, View.GONE, View.GONE);
                break;
            case 3:
                LinearLayout.LayoutParams layoutParams1 = (LinearLayout.LayoutParams) mActivitySuggestedVideoBinding.llCameraThreeFour.getLayoutParams();
                layoutParams1.weight = 1.0f;
                mActivitySuggestedVideoBinding.llCameraThreeFour.setLayoutParams(layoutParams1);
                mActivitySuggestedVideoBinding.llVideoFiveSix.setVisibility(View.GONE);
                tvTimer = mActivitySuggestedVideoBinding.tvTimerThree;
                mFlCamera = mActivitySuggestedVideoBinding.actvThree;
                mPbRecordingBar = mActivitySuggestedVideoBinding.pbRecordThree;
                selectFrames(View.VISIBLE, View.VISIBLE, View.VISIBLE, View.GONE, View.GONE, View.GONE);
                break;
            case 4:
                mFlCamera = mActivitySuggestedVideoBinding.actvOne;
                mPbRecordingBar = mActivitySuggestedVideoBinding.pbRecordOne;
                tvTimer = mActivitySuggestedVideoBinding.tvTimerOne;
                mActivitySuggestedVideoBinding.llVideoFiveSix.setVisibility(View.GONE);
                selectFrames(View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.GONE, View.GONE);
                break;
            case 5:
                mFlCamera = mActivitySuggestedVideoBinding.actvFive;
                mPbRecordingBar = mActivitySuggestedVideoBinding.pbRecordFive;
                tvTimer = mActivitySuggestedVideoBinding.tvTimerFive;
                selectFrames(View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.GONE);
                break;
            case 6:
                LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) mActivitySuggestedVideoBinding.llCameraOneTwo.getLayoutParams();
                layoutParams2.weight = 1.0f;
                mActivitySuggestedVideoBinding.llCameraOneTwo.setLayoutParams(layoutParams2);

                LinearLayout.LayoutParams layoutParams3 = (LinearLayout.LayoutParams) mActivitySuggestedVideoBinding.llCameraThreeFour.getLayoutParams();
                layoutParams3.weight = 1.0f;
                mActivitySuggestedVideoBinding.llCameraThreeFour.setLayoutParams(layoutParams3);

                LinearLayout.LayoutParams layoutParams4 = (LinearLayout.LayoutParams) mActivitySuggestedVideoBinding.llVideoFiveSix.getLayoutParams();
                layoutParams4.weight = 1.0f;
                mActivitySuggestedVideoBinding.llVideoFiveSix.setLayoutParams(layoutParams4);

                mFlCamera = mActivitySuggestedVideoBinding.actvOne;
                tvTimer = mActivitySuggestedVideoBinding.tvTimerOne;
                mPbRecordingBar = mActivitySuggestedVideoBinding.pbRecordOne;
                selectFrames(View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE);
                break;
        }
        if (checkPermissionsForCamera(this, null))
            initializeCamera();

    }

    private void setDotColor(String frameNo, ImageView imageView) {
        if(collabBarBeans.size()>0) {
            switch (frameNo) {
                case "1":
                    imageView.setColorFilter(Color.parseColor("#009359"), android.graphics.PorterDuff.Mode.SRC_IN);
                    break;
                case "2":
                    imageView.setColorFilter(Color.parseColor("#FFB437"), android.graphics.PorterDuff.Mode.SRC_IN);
                    break;
                case "3":
                    imageView.setColorFilter(Color.parseColor("#3F7AE9"), android.graphics.PorterDuff.Mode.SRC_IN);
                    break;
                case "4":
                    imageView.setColorFilter(Color.parseColor("#FF9640"), android.graphics.PorterDuff.Mode.SRC_IN);
                    break;
                case "5":
                    imageView.setColorFilter(Color.parseColor("#F85458"), android.graphics.PorterDuff.Mode.SRC_IN);
                    break;
                case "6":
                    imageView.setColorFilter(Color.parseColor("#8E5BE8"), android.graphics.PorterDuff.Mode.SRC_IN);
                    break;
            }
        }else{
            imageView.setVisibility(View.INVISIBLE);
        }
    }

    private void setFlag() {
        switch (frameNo) {
            case 1:
                mActivitySuggestedVideoBinding.ivFlag.setBackgroundColor(Color.parseColor("#009359"));
                break;
            case 2:
                mActivitySuggestedVideoBinding.ivFlag.setBackgroundColor(Color.parseColor("#FFB437"));
                break;
            case 3:
                mActivitySuggestedVideoBinding.ivFlag.setBackgroundColor(Color.parseColor("#3F7AE9"));
                break;
            case 4:
                mActivitySuggestedVideoBinding.ivFlag.setBackgroundColor(Color.parseColor("#FF9640"));
                break;
            case 5:
                mActivitySuggestedVideoBinding.ivFlag.setBackgroundColor(Color.parseColor("#F85458"));
                break;
            case 6:
                mActivitySuggestedVideoBinding.ivFlag.setBackgroundColor(Color.parseColor("#8E5BE8"));
                break;
        }
    }

    private void selectFrames(int one, int two, int three, int four, int five, int six) {
        mActivitySuggestedVideoBinding.flCamOne.setVisibility(one);
        mActivitySuggestedVideoBinding.flCamTwo.setVisibility(two);
        mActivitySuggestedVideoBinding.flCamThree.setVisibility(three);
        mActivitySuggestedVideoBinding.flCamFour.setVisibility(four);
        mActivitySuggestedVideoBinding.flCamFive.setVisibility(five);
        mActivitySuggestedVideoBinding.flCamSix.setVisibility(six);
        setFlag();
        setAnimationList();
        switch (mFlCamera.getId()) {
            case R.id.actv_one:
                switch (totalNoOfFrames) {
                    case 2:
                        if (mDownloadedVideos.size() > 0) {
                            mExoplayerTwo = setExoPlayer(mDownloadedVideos.get(0).getDownlaodUri());
                            mActivitySuggestedVideoBinding.vvTwo.setPlayer(mExoplayerTwo);
                            mActivitySuggestedVideoBinding.vvTwo.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            setDotColor(String.valueOf(mDownloadedVideos.get(0).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorTwo);
                            setDotColor(String.valueOf(frameNo), mActivitySuggestedVideoBinding.ivFrameColorOne);
                            ivActiveRecording = mActivitySuggestedVideoBinding.ivFrameColorOne;
                        }
                        break;
                    case 3:
                        if (mDownloadedVideos.size() > 1) {
                            mExoplayerTwo = setExoPlayer(mDownloadedVideos.get(0).getDownlaodUri());
                            mExoplayerThree = setExoPlayer(mDownloadedVideos.get(1).getDownlaodUri());
                            mActivitySuggestedVideoBinding.vvTwo.setPlayer(mExoplayerTwo);
                            mActivitySuggestedVideoBinding.vvThree.setPlayer(mExoplayerThree);
                            mActivitySuggestedVideoBinding.vvTwo.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySuggestedVideoBinding.vvThree.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            setDotColor(String.valueOf(frameNo), mActivitySuggestedVideoBinding.ivFrameColorOne);
                            setDotColor(String.valueOf(mDownloadedVideos.get(0).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorTwo);
                            setDotColor(String.valueOf(mDownloadedVideos.get(1).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorThree);
                            ivActiveRecording = mActivitySuggestedVideoBinding.ivFrameColorOne;
                        }
                        break;
                    case 4:
                        if (mDownloadedVideos.size() > 2) {
                            mExoplayerTwo = setExoPlayer(mDownloadedVideos.get(0).getDownlaodUri());
                            mExoplayerThree = setExoPlayer(mDownloadedVideos.get(1).getDownlaodUri());
                            mExoplayerFour = setExoPlayer(mDownloadedVideos.get(2).getDownlaodUri());
                            mActivitySuggestedVideoBinding.vvTwo.setPlayer(mExoplayerTwo);
                            mActivitySuggestedVideoBinding.vvThree.setPlayer(mExoplayerThree);
                            mActivitySuggestedVideoBinding.vvFour.setPlayer(mExoplayerFour);
                            mActivitySuggestedVideoBinding.vvTwo.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySuggestedVideoBinding.vvThree.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySuggestedVideoBinding.vvFour.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            setDotColor(String.valueOf(frameNo), mActivitySuggestedVideoBinding.ivFrameColorOne);
                            setDotColor(String.valueOf(mDownloadedVideos.get(0).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorTwo);
                            setDotColor(String.valueOf(mDownloadedVideos.get(1).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorThree);
                            setDotColor(String.valueOf(mDownloadedVideos.get(2).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorFour);
                            ivActiveRecording = mActivitySuggestedVideoBinding.ivFrameColorOne;
                        }
                        break;
                    case 5:
                        if (mDownloadedVideos.size() > 3) {
                            mExoplayerTwo = setExoPlayer(mDownloadedVideos.get(0).getDownlaodUri());
                            mExoplayerThree = setExoPlayer(mDownloadedVideos.get(1).getDownlaodUri());
                            mExoplayerFour = setExoPlayer(mDownloadedVideos.get(2).getDownlaodUri());
                            mExoplayerFive = setExoPlayer(mDownloadedVideos.get(3).getDownlaodUri());
                            mActivitySuggestedVideoBinding.vvTwo.setPlayer(mExoplayerTwo);
                            mActivitySuggestedVideoBinding.vvThree.setPlayer(mExoplayerThree);
                            mActivitySuggestedVideoBinding.vvFour.setPlayer(mExoplayerFour);
                            mActivitySuggestedVideoBinding.vvFive.setPlayer(mExoplayerFive);
                            mActivitySuggestedVideoBinding.vvTwo.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySuggestedVideoBinding.vvThree.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySuggestedVideoBinding.vvFour.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySuggestedVideoBinding.vvFive.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
                            setDotColor(String.valueOf(frameNo), mActivitySuggestedVideoBinding.ivFrameColorOne);
                            setDotColor(String.valueOf(mDownloadedVideos.get(0).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorTwo);
                            setDotColor(String.valueOf(mDownloadedVideos.get(1).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorThree);
                            setDotColor(String.valueOf(mDownloadedVideos.get(2).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorFour);
                            setDotColor(String.valueOf(mDownloadedVideos.get(3).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorFive);
                            ivActiveRecording = mActivitySuggestedVideoBinding.ivFrameColorOne;
                        }
                        break;
                    case 6:
                        if (mDownloadedVideos.size() > 4) {
                            mExoplayerTwo = setExoPlayer(mDownloadedVideos.get(0).getDownlaodUri());
                            mExoplayerThree = setExoPlayer(mDownloadedVideos.get(1).getDownlaodUri());
                            mExoplayerFour = setExoPlayer(mDownloadedVideos.get(2).getDownlaodUri());
                            mExoplayerFive = setExoPlayer(mDownloadedVideos.get(3).getDownlaodUri());
                            mExoplayerSix = setExoPlayer(mDownloadedVideos.get(4).getDownlaodUri());
                            mActivitySuggestedVideoBinding.vvTwo.setPlayer(mExoplayerTwo);
                            mActivitySuggestedVideoBinding.vvThree.setPlayer(mExoplayerThree);
                            mActivitySuggestedVideoBinding.vvFour.setPlayer(mExoplayerFour);
                            mActivitySuggestedVideoBinding.vvFive.setPlayer(mExoplayerFive);
                            mActivitySuggestedVideoBinding.vvSix.setPlayer(mExoplayerSix);
                            mActivitySuggestedVideoBinding.vvTwo.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySuggestedVideoBinding.vvThree.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySuggestedVideoBinding.vvFour.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySuggestedVideoBinding.vvFive.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySuggestedVideoBinding.vvSix.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            setDotColor(String.valueOf(frameNo), mActivitySuggestedVideoBinding.ivFrameColorOne);
                            setDotColor(String.valueOf(mDownloadedVideos.get(0).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorTwo);
                            setDotColor(String.valueOf(mDownloadedVideos.get(1).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorThree);
                            setDotColor(String.valueOf(mDownloadedVideos.get(2).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorFour);
                            setDotColor(String.valueOf(mDownloadedVideos.get(3).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorFive);
                            setDotColor(String.valueOf(mDownloadedVideos.get(4).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorSix);
                            ivActiveRecording = mActivitySuggestedVideoBinding.ivFrameColorOne;
                        }
                        break;
                }
                setVideoViews(View.GONE, View.VISIBLE, View.VISIBLE);
                setProgressBars(View.VISIBLE, View.GONE, View.GONE);
                setViews(View.VISIBLE, View.GONE, View.GONE);
                break;
            case R.id.actv_three:
                switch (totalNoOfFrames) {
                    case 2:
                        if (mDownloadedVideos.size() > 0) {
                            mExoplayerOne = setExoPlayer(mDownloadedVideos.get(0).getDownlaodUri());
                            mActivitySuggestedVideoBinding.vvFour.setPlayer(mExoplayerOne);
                            setDotColor(String.valueOf(frameNo), mActivitySuggestedVideoBinding.ivFrameColorThree);
                            setDotColor(String.valueOf(mDownloadedVideos.get(0).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorFour);
                            ivActiveRecording = mActivitySuggestedVideoBinding.ivFrameColorThree;
                        }
                        break;
                    case 3:
                        if (mDownloadedVideos.size() > 1) {
                            mExoplayerOne = setExoPlayer(mDownloadedVideos.get(0).getDownlaodUri());
                            mExoplayerTwo = setExoPlayer(mDownloadedVideos.get(1).getDownlaodUri());
                            mActivitySuggestedVideoBinding.vvOne.setPlayer(mExoplayerOne);
                            mActivitySuggestedVideoBinding.vvTwo.setPlayer(mExoplayerTwo);
                            mActivitySuggestedVideoBinding.vvOne.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySuggestedVideoBinding.vvTwo.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            Display display = getWindowManager().getDefaultDisplay();
                            Point point = new Point();
                            display.getSize(point);

                            ConstraintSet set = new ConstraintSet();
                            set.constrainHeight(mActivitySuggestedVideoBinding.actvThree.getId(),
                                    ConstraintSet.MATCH_CONSTRAINT);
                            set.constrainWidth(mActivitySuggestedVideoBinding.actvThree.getId(),
                                    point.x / 2);

                            set.connect(mActivitySuggestedVideoBinding.actvThree.getId(), ConstraintSet.LEFT,
                                    ConstraintSet.PARENT_ID, ConstraintSet.LEFT, 0);
                            set.connect(mActivitySuggestedVideoBinding.actvThree.getId(), ConstraintSet.RIGHT,
                                    ConstraintSet.PARENT_ID, ConstraintSet.RIGHT, 0);
                            set.connect(mActivitySuggestedVideoBinding.actvThree.getId(), ConstraintSet.TOP,
                                    ConstraintSet.PARENT_ID, ConstraintSet.TOP, 0);
                            set.connect(mActivitySuggestedVideoBinding.actvThree.getId(), ConstraintSet.BOTTOM,
                                    ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM, 0);
                            set.applyTo(mActivitySuggestedVideoBinding.flCamThree);
                            setDotColor(String.valueOf(frameNo), mActivitySuggestedVideoBinding.ivFrameColorThree);
                            setDotColor(String.valueOf(mDownloadedVideos.get(0).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorOne);
                            setDotColor(String.valueOf(mDownloadedVideos.get(1).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorTwo);
                            ivActiveRecording = mActivitySuggestedVideoBinding.ivFrameColorThree;
                        }
                        break;
                    case 4:
                        if (mDownloadedVideos.size() > 2) {
                            mExoplayerOne = setExoPlayer(mDownloadedVideos.get(0).getDownlaodUri());
                            mExoplayerTwo = setExoPlayer(mDownloadedVideos.get(1).getDownlaodUri());
                            mExoplayerFour = setExoPlayer(mDownloadedVideos.get(2).getDownlaodUri());
                            mActivitySuggestedVideoBinding.vvOne.setPlayer(mExoplayerOne);
                            mActivitySuggestedVideoBinding.vvTwo.setPlayer(mExoplayerTwo);
                            mActivitySuggestedVideoBinding.vvFour.setPlayer(mExoplayerFour);
                            setDotColor(String.valueOf(mDownloadedVideos.get(0).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorOne);
                            setDotColor(String.valueOf(mDownloadedVideos.get(1).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorTwo);
                            setDotColor(String.valueOf(mDownloadedVideos.get(2).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorFour);
                        }
                        break;
                    case 5:
                        if (mDownloadedVideos.size() > 3) {
                            mExoplayerOne = setExoPlayer(mDownloadedVideos.get(0).getDownlaodUri());
                            mExoplayerTwo = setExoPlayer(mDownloadedVideos.get(1).getDownlaodUri());
                            mExoplayerFour = setExoPlayer(mDownloadedVideos.get(2).getDownlaodUri());
                            mExoplayerFive = setExoPlayer(mDownloadedVideos.get(3).getDownlaodUri());
                            mActivitySuggestedVideoBinding.vvOne.setPlayer(mExoplayerOne);
                            mActivitySuggestedVideoBinding.vvTwo.setPlayer(mExoplayerTwo);
                            mActivitySuggestedVideoBinding.vvFour.setPlayer(mExoplayerFour);
                            mActivitySuggestedVideoBinding.vvFive.setPlayer(mExoplayerFive);
                            mActivitySuggestedVideoBinding.vvFive.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
                            setDotColor(String.valueOf(mDownloadedVideos.get(0).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorOne);
                            setDotColor(String.valueOf(mDownloadedVideos.get(1).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorTwo);
                            setDotColor(String.valueOf(mDownloadedVideos.get(2).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorFour);
                            setDotColor(String.valueOf(mDownloadedVideos.get(3).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorFive);
                        }
                        break;
                    case 6:
                        if (mDownloadedVideos.size() > 4) {
                            mExoplayerOne = setExoPlayer(mDownloadedVideos.get(0).getDownlaodUri());
                            mExoplayerTwo = setExoPlayer(mDownloadedVideos.get(1).getDownlaodUri());
                            mExoplayerFour = setExoPlayer(mDownloadedVideos.get(2).getDownlaodUri());
                            mExoplayerFive = setExoPlayer(mDownloadedVideos.get(3).getDownlaodUri());
                            mExoplayerSix = setExoPlayer(mDownloadedVideos.get(4).getDownlaodUri());
                            mActivitySuggestedVideoBinding.vvOne.setPlayer(mExoplayerOne);
                            mActivitySuggestedVideoBinding.vvTwo.setPlayer(mExoplayerTwo);
                            mActivitySuggestedVideoBinding.vvFour.setPlayer(mExoplayerFour);
                            mActivitySuggestedVideoBinding.vvFive.setPlayer(mExoplayerFive);
                            mActivitySuggestedVideoBinding.vvSix.setPlayer(mExoplayerSix);
                            setDotColor(String.valueOf(mDownloadedVideos.get(0).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorOne);
                            setDotColor(String.valueOf(mDownloadedVideos.get(1).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorTwo);
                            setDotColor(String.valueOf(mDownloadedVideos.get(2).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorFour);
                            setDotColor(String.valueOf(mDownloadedVideos.get(3).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorFive);
                            setDotColor(String.valueOf(mDownloadedVideos.get(4).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorSix);
                        }
                        break;
                }
                setVideoViews(View.VISIBLE, View.GONE, View.VISIBLE);
                setProgressBars(View.GONE, View.VISIBLE, View.GONE);
                setViews(View.GONE, View.VISIBLE, View.GONE);
                break;

            case R.id.actv_five:
                switch (totalNoOfFrames) {
                    case 5:
                        if (mDownloadedVideos.size() > 3) {
                            mExoplayerOne = setExoPlayer(mDownloadedVideos.get(0).getDownlaodUri());
                            mExoplayerTwo = setExoPlayer(mDownloadedVideos.get(1).getDownlaodUri());
                            mExoplayerFour = setExoPlayer(mDownloadedVideos.get(2).getDownlaodUri());
                            mExoplayerThree = setExoPlayer(mDownloadedVideos.get(3).getDownlaodUri());
                            mActivitySuggestedVideoBinding.vvOne.setPlayer(mExoplayerOne);
                            mActivitySuggestedVideoBinding.vvTwo.setPlayer(mExoplayerTwo);
                            mActivitySuggestedVideoBinding.vvFour.setPlayer(mExoplayerFour);
                            mActivitySuggestedVideoBinding.vvThree.setPlayer(mExoplayerThree);
                            mActivitySuggestedVideoBinding.vvOne.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySuggestedVideoBinding.vvTwo.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySuggestedVideoBinding.vvThree.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySuggestedVideoBinding.vvFour.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            Display display = getWindowManager().getDefaultDisplay();
                            Point point = new Point();
                            display.getSize(point);
                            ConstraintSet set = new ConstraintSet();
                            set.constrainHeight(mActivitySuggestedVideoBinding.actvFive.getId(),
                                    ConstraintSet.MATCH_CONSTRAINT);
                            set.constrainWidth(mActivitySuggestedVideoBinding.actvFive.getId(),
                                    point.x / 2);

                            set.connect(mActivitySuggestedVideoBinding.actvFive.getId(), ConstraintSet.LEFT,
                                    ConstraintSet.PARENT_ID, ConstraintSet.LEFT, 0);
                            set.connect(mActivitySuggestedVideoBinding.actvFive.getId(), ConstraintSet.RIGHT,
                                    ConstraintSet.PARENT_ID, ConstraintSet.RIGHT, 0);
                            set.connect(mActivitySuggestedVideoBinding.actvFive.getId(), ConstraintSet.TOP,
                                    ConstraintSet.PARENT_ID, ConstraintSet.TOP, 0);
                            set.connect(mActivitySuggestedVideoBinding.actvFive.getId(), ConstraintSet.BOTTOM,
                                    ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM, 0);
                            set.applyTo(mActivitySuggestedVideoBinding.flCamFive);
                            setDotColor(String.valueOf(frameNo), mActivitySuggestedVideoBinding.ivFrameColorFive);
                            setDotColor(String.valueOf(mDownloadedVideos.get(0).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorOne);
                            setDotColor(String.valueOf(mDownloadedVideos.get(1).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorTwo);
                            setDotColor(String.valueOf(mDownloadedVideos.get(2).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorFour);
                            setDotColor(String.valueOf(mDownloadedVideos.get(3).getFrameNumber()), mActivitySuggestedVideoBinding.ivFrameColorThree);
                            ivActiveRecording = mActivitySuggestedVideoBinding.ivFrameColorFive;
                        }
                        break;
                }
                setVideoViews(View.VISIBLE, View.VISIBLE, View.GONE);
                setProgressBars(View.GONE, View.GONE, View.VISIBLE);
                setViews(View.GONE, View.GONE, View.VISIBLE);
                break;
        }
        muteExoplayers();
    }

    private void muteExoplayers() {
        switch (mDownloadedVideos.size()) {
            case 2:
                if (mExoplayerOne != null)
                    mExoplayerOne.setVolume(0F);
                break;
            case 3:
                if (mExoplayerTwo != null)
                    mExoplayerTwo.setVolume(0F);
                if (mExoplayerThree != null)
                    mExoplayerThree.setVolume(0F);
                break;
            case 4:
                if (mExoplayerOne != null)
                    mExoplayerOne.setVolume(0F);
                if (mExoplayerThree != null)
                    mExoplayerThree.setVolume(0F);
                if (mExoplayerFour != null)
                    mExoplayerFour.setVolume(0F);
                break;
            case 5:
                if (mExoplayerTwo != null)
                    mExoplayerTwo.setVolume(0F);
                if (mExoplayerThree != null)
                    mExoplayerThree.setVolume(0F);
                if (mExoplayerFour != null)
                    mExoplayerFour.setVolume(0F);
                if (mExoplayerFive != null)
                    mExoplayerFive.setVolume(0F);
                break;
        }
    }

    private void setUpCameraView() {
        FrameLayout frameLayout = mFlCamera;
        frameLayout.removeAllViews();
        sampleGLView = null;
        sampleGLView = new SampleGLView(getApplicationContext());
        sampleGLView.setTouchListener((event, width, height) -> {
            if (mCameraRecorder == null) return;
            mCameraRecorder.changeManualFocusPoint(event.getX(), event.getY(), width, height);
        });
        frameLayout.addView(sampleGLView);
    }

    private void initializeCamera() {
        setUpCameraView();
        mCameraRecorder = new CameraRecorderBuilder(this, sampleGLView)
                .cameraRecordListener(new CameraRecordListener() {
                    @Override
                    public void onGetFlashSupport(boolean flashSupport) {

                    }

                    @Override
                    public void onRecordComplete() {
                        mIsRecordingVideo = false;
                        mProgress.add(mPbRecordingBar.progressInMillies());
                        mNextVideoAbsolutePath = null;
                    }

                    @Override
                    public void onRecordStart() {
                        if (!getMicrophoneAvailable()) {
                            AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
                            currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, (int) (audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) * 0.75), AudioManager.FLAG_PLAY_SOUND);
                        }
                    }

                    @Override
                    public void onError(Exception exception) {
                        Log.e("CameraRecorder", exception.toString());
                    }

                    @Override
                    public void onCameraThreadFinish() {
                        runOnUiThread(() -> {
                            initializeCamera();
                        });
                    }
                })
                .mute(true)
                .videoSize(480, 640)
                .cameraSize(720, 1280)
                .lensFacing(lensFacing)
                .build();
    }

    private SimpleExoPlayer setExoPlayer(String downlaodUri) {
        SimpleExoPlayer exoPlayer = ExoPlayerFactory.newSimpleInstance(this,
                new DefaultRenderersFactory(this),
                new DefaultTrackSelector(), new DefaultLoadControl());
        exoPlayer.setPlayWhenReady(playWhenReady);
        exoPlayer.setRepeatMode(Player.REPEAT_MODE_OFF);
        Uri uri = Uri.parse(downlaodUri);
        MediaSource mediaSource = buildMediaSource(uri);
        exoPlayer.prepare(mediaSource);
        return exoPlayer;
    }

    private MediaSource buildMediaSource(Uri uri) {
        return new ExtractorMediaSource.Factory(
                new DefaultDataSourceFactory(this, "whizzly-app")).
                createMediaSource(uri);
    }

    private void setVideoViews(int one, int two, int three) {
        mActivitySuggestedVideoBinding.vvOne.setVisibility(one);
        mActivitySuggestedVideoBinding.vvTwo.setVisibility(View.VISIBLE);
        mActivitySuggestedVideoBinding.vvThree.setVisibility(two);
        mActivitySuggestedVideoBinding.vvFour.setVisibility(View.VISIBLE);
        mActivitySuggestedVideoBinding.vvFive.setVisibility(three);
        mActivitySuggestedVideoBinding.vvSix.setVisibility(View.VISIBLE);

    }

    private void setViews(int one, int three, int five) {
        mActivitySuggestedVideoBinding.viewOne.setVisibility(View.INVISIBLE);
        mActivitySuggestedVideoBinding.viewTwo.setVisibility(View.GONE);
        mActivitySuggestedVideoBinding.viewThree.setVisibility(View.INVISIBLE);
        mActivitySuggestedVideoBinding.viewFour.setVisibility(View.GONE);
        mActivitySuggestedVideoBinding.viewFive.setVisibility(View.INVISIBLE);
        mActivitySuggestedVideoBinding.viewSix.setVisibility(View.GONE);
    }

    private void setProgressBars(int one, int three, int five) {
        mActivitySuggestedVideoBinding.pbRecordOne.setVisibility(View.INVISIBLE);
        mActivitySuggestedVideoBinding.pbRecordTwo.setVisibility(View.GONE);
        mActivitySuggestedVideoBinding.pbRecordThree.setVisibility(View.INVISIBLE);
        mActivitySuggestedVideoBinding.pbRecordFour.setVisibility(View.GONE);
        mActivitySuggestedVideoBinding.pbRecordFive.setVisibility(View.INVISIBLE);
        mActivitySuggestedVideoBinding.pbRecordSix.setVisibility(View.GONE);
    }

    private void setProgressBar() {
        mPbRecordingBar.setCornerRadius(0f);
        mPbRecordingBar.setDividerColor(Color.WHITE);
        mPbRecordingBar.setDividerEnabled(true);
        mPbRecordingBar.setDividerWidth(2);
        mPbRecordingBar.setShader(new int[]{getResources().getColor(R.color.Green), getResources().getColor(R.color.Green), getResources().getColor(R.color.Green)});
        mPbRecordingBar.enableAutoProgressView(getMaxVideoDuration());
        mPbRecordingBar.pause();
    }

    private void setAnimationList() {
        ArrayList<CollabBarBean> additionalInfo = new Gson().fromJson(mResult.getAdditinalInfo(), new TypeToken<ArrayList<CollabBarBean>>() {
        }.getType());
        for (int i = 0; i < additionalInfo.size(); i++) {
            if (frameNo - 1 == Integer.parseInt(additionalInfo.get(i).getCode())) {
                mAnimationList.add(setAnimation(additionalInfo.get(i).getStartTime(), additionalInfo.get(i).getEndTime(), additionalInfo.get(i).getDescription()));
            }
        }
    }

    private AnimationBean setAnimation(float start, float end, String description) {
        AnimationBean animationBean = new AnimationBean();
        Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animationBean.setAnimation(animation);
        animationBean.setStartTime(start);
        animationBean.setEndTime(end);
        animationBean.setDescription(description);
        return animationBean;
    }

    private int getMaxVideoDuration() {
        for (int i = 0; i < mCollabArrayList.size(); i++) {
            if (mCollabArrayList.get(i) instanceof CollabInfo) {
                String duration = ((CollabInfo) mCollabArrayList.get(i)).getVideoDuration();
                if (duration != null)
                    return Integer.parseInt(duration);
                break;
            }

        }
        return 30000;
    }

    private String getVideoFilePath() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "video_" + System.currentTimeMillis() + ".mp4";
    }

    private void startRecordingVideo() {
        setFlagProgress();
        if (mNextVideoAbsolutePath == null || mNextVideoAbsolutePath.isEmpty()) {
            mNextVideoAbsolutePath = getVideoFilePath();
        }
        mSuggestedVideoModel.setIsRecordPressed(true);
        mSuggestedVideoModel.setIsVideoStarted(true);
        mSuggestedVideoModel.setIsRemoveVisible(false);
        mIsRecordingVideo = true;
        mCameraRecorder.start(mNextVideoAbsolutePath);
        if (mProgress.size() > 0)
            mPbRecordingBar.resume(mProgress.get(mProgress.size() - 1));
        else
            mPbRecordingBar.resume();
    }

    private void setFlagProgress() {
        durationInMillies = Long.parseLong(mResult.getCollabInfo().get(0).getVideoDuration());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                float valueX = ((float) getWindowManager().getDefaultDisplay().getWidth() / (float) durationInMillies) * (float) mPbRecordingBar.progressInMillies();
                if (valueX < (float) getWindowManager().getDefaultDisplay().getWidth()) {
                    mActivitySuggestedVideoBinding.ivFlag.setX(valueX);
                    handler.postDelayed(this, 1);
                    for (int i = 0; i < mAnimationList.size(); i++) {
                        if (mPbRecordingBar.progressInMillies() >= (long) mAnimationList.get(i).getStartTime() && !isAnimationStarted) {
                            ivActiveRecording.startAnimation(mAnimationList.get(i).getAnimation());
                            mActivitySuggestedVideoBinding.tvColorDescription.setSelected(true);
                            tvTimer.setVisibility(View.GONE);
                            mActivitySuggestedVideoBinding.tvColorDescription.setVisibility(View.VISIBLE);
                            mActivitySuggestedVideoBinding.tvColorDescription.setText(mAnimationList.get(i).getDescription());
                            isAnimationStarted = true;

                        }
                        if (mPbRecordingBar.progressInMillies() > (long) mAnimationList.get(i).getEndTime()) {
                            ivActiveRecording.clearAnimation();
                            isAnimationStarted = false;
                            mActivitySuggestedVideoBinding.tvColorDescription.setVisibility(View.INVISIBLE);
                        }

                        if (mPbRecordingBar.progressInMillies() >= (long) mAnimationList.get(i).getStartTime() - 1500 && mPbRecordingBar.progressInMillies() < (long) mAnimationList.get(i).getStartTime()){
                            tvTimer.setVisibility(View.VISIBLE);
                            tvTimer.setText("5");
                        }

                        if (mPbRecordingBar.progressInMillies() >= (long) mAnimationList.get(i).getStartTime() - 1200 && mPbRecordingBar.progressInMillies() < (long) mAnimationList.get(i).getStartTime()){
                            tvTimer.setVisibility(View.VISIBLE);
                            tvTimer.setText("4");
                        }

                        if (mPbRecordingBar.progressInMillies() >= (long) mAnimationList.get(i).getStartTime() - 900 && mPbRecordingBar.progressInMillies() < (long) mAnimationList.get(i).getStartTime()){
                            tvTimer.setVisibility(View.VISIBLE);
                            tvTimer.setText("3");
                        }

                        if (mPbRecordingBar.progressInMillies() >= (long) mAnimationList.get(i).getStartTime() - 600  && mPbRecordingBar.progressInMillies() < (long) mAnimationList.get(i).getStartTime()){
                            tvTimer.setVisibility(View.VISIBLE);
                            tvTimer.setText("2");
                        }

                        if (mPbRecordingBar.progressInMillies() >= (long) mAnimationList.get(i).getStartTime() - 300 && mPbRecordingBar.progressInMillies() < (long) mAnimationList.get(i).getStartTime()){
                            tvTimer.setVisibility(View.VISIBLE);
                            tvTimer.setText("1");
                        }

                            break;
                    }
                }
            }
        }, 1);
    }

    private void pauseExoplayer() {
        if (null != mExoplayerMusic) {
            mExoplayerMusic.setPlayWhenReady(false);
        }

        if (null != mExoplayerOne) {
            mExoplayerOne.setPlayWhenReady(false);
        }

        if (null != mExoplayerTwo) {
            mExoplayerTwo.setPlayWhenReady(false);
        }

        if (null != mExoplayerThree) {
            mExoplayerThree.setPlayWhenReady(false);
        }

        if (null != mExoplayerFour) {
            mExoplayerFour.setPlayWhenReady(false);
        }

        if (null != mExoplayerFive) {
            mExoplayerFive.setPlayWhenReady(false);
        }

        if (null != mExoplayerSix) {
            mExoplayerSix.setPlayWhenReady(false);
        }
    }

    private void releaseExoplayer() {
        if (null != mExoplayerMusic) {
            mExoplayerMusic.setPlayWhenReady(false);
            mExoplayerMusic.release();
            mExoplayerMusic = null;
        }

        if (null != mExoplayerOne) {
            mExoplayerOne.setPlayWhenReady(false);
            mExoplayerOne.release();
            mExoplayerOne = null;
        }

        if (null != mExoplayerTwo) {
            mExoplayerTwo.setPlayWhenReady(false);
            mExoplayerTwo.release();
            mExoplayerTwo = null;
        }

        if (null != mExoplayerThree) {
            mExoplayerThree.setPlayWhenReady(false);
            mExoplayerThree.release();
            mExoplayerThree = null;
        }

        if (null != mExoplayerFour) {
            mExoplayerFour.setPlayWhenReady(false);
            mExoplayerFour.release();
            mExoplayerFour = null;
        }

        if (null != mExoplayerFive) {
            mExoplayerFive.setPlayWhenReady(false);
            mExoplayerFive.release();
            mExoplayerFive = null;
        }

        if (null != mExoplayerSix) {
            mExoplayerSix.setPlayWhenReady(false);
            mExoplayerSix.release();
            mExoplayerSix = null;
        }
    }

    private void stopRecordingVideo() {
        mSuggestedVideoModel.setIsRemoveVisible(true);
        mIsRecordingVideo = false;
        mSuggestedVideoModel.setIsVideoStarted(false);
        mSuggestedVideoModel.setIsRecordPressed(false);
        mCameraRecorder.stop();
        mVideoStack.add(mNextVideoAbsolutePath);
        mPbRecordingBar.pause();
        mPbRecordingBar.addDivider();
        mNextVideoAbsolutePath = null;
        handler.removeCallbacks(null);
    }

    private void showRemoveSegmentDialog() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.txt_error_remove_segment)
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> deleteRecording())
                .setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss())
                .create().show();
    }

    private void recordUsingPressedButton() {
        try {
            mActivitySuggestedVideoBinding.ivRecord.setOnTouchListener((view, motionEvent) -> {
                if (!mSuggestedVideoModel.getIsStartTimer()) {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_UP:
                            if (mIsRecordingVideo) {
                                mIsRecordingVideo = false;
                                pauseExoplayer();
                                stopRecordingVideo();
                                mCountDownTimerWithPause.pause();
                            }
                            break;
                    }
                }
                return false;
            });

            mActivitySuggestedVideoBinding.ivRecord.setOnLongClickListener(v -> {
                if (!mIsRecordingVideo) {
                    mIsRecordingVideo = true;
                    startRecordingVideo();
                    startExoplayer();
                    mCountDownTimerWithPause.resume();
                }
                return true;
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopRecording() {
        mCountDownTimerWithPause = new CountDownTimerWithPause(getMaxVideoDuration(), 1000, false) {
            public void onTick(long millisUntilFinished) {
                if (millisUntilFinished / 1000 == 0) {
                    stopRecordingVideo();
                }
            }

            public void onFinish() {
                stopRecordingVideo();
            }
        }.create();
    }

    private void startExoplayer() {
        if (mExoplayerOne != null) {
            playWhenReady = true;
            mExoplayerOne.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerTwo != null) {
            playWhenReady = true;
            mExoplayerTwo.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerThree != null) {
            playWhenReady = true;
            mExoplayerThree.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerFour != null) {
            playWhenReady = true;
            mExoplayerFour.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerFive != null) {
            playWhenReady = true;
            mExoplayerFive.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerSix != null) {
            playWhenReady = true;
            mExoplayerSix.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerMusic != null) {
            playWhenReady = true;
            mExoplayerMusic.setPlayWhenReady(playWhenReady);
        }
    }

    private void seekExoplayer(long seekTo) {
        if (mExoplayerOne != null) {
            playWhenReady = false;
            mExoplayerOne.seekTo(seekTo);
            mExoplayerOne.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerTwo != null) {
            playWhenReady = false;
            mExoplayerTwo.seekTo(seekTo);
            mExoplayerTwo.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerThree != null) {
            playWhenReady = false;
            mExoplayerThree.seekTo(seekTo);
            mExoplayerThree.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerFour != null) {
            playWhenReady = false;
            mExoplayerFour.seekTo(seekTo);
            mExoplayerFour.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerFive != null) {
            playWhenReady = false;
            mExoplayerFive.seekTo(seekTo);
            mExoplayerFive.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerSix != null) {
            playWhenReady = false;
            mExoplayerSix.seekTo(seekTo);
            mExoplayerSix.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerMusic != null) {
            playWhenReady = false;
            mExoplayerMusic.seekTo(seekTo);
            mExoplayerMusic.setPlayWhenReady(playWhenReady);
        }
    }

    private void deleteRecording() {
        if (mVideoStack.size() > 0) {
            String fileToRemove = mVideoStack.remove(mVideoStack.size() - 1);
            File file = new File(fileToRemove);
            if (file.delete()) {
                Log.e("deleteRecording: ", "Deleted File");
            }
            mSuggestedVideoModel.setIsRemoveVisible(true);
        }
        if (mProgress.size() > 1) {
            mProgress.remove(mProgress.size() - 1);
            mPbRecordingBar.removeDivider();
            mPbRecordingBar.updateProgress(mProgress.get(mProgress.size() - 1));
            seekExoplayer(mProgress.get(mProgress.size() - 1));
        } else if (mProgress.size() == 1) {
            mProgress.clear();
            mPbRecordingBar.removeDivider();
            mPbRecordingBar.updateProgress(0);
            seekExoplayer(0);
        }
    }

    public void switchCamera() {
        releaseCamera();
        if (lensFacing == LensFacing.BACK) {
            lensFacing = LensFacing.FRONT;
            mSuggestedVideoModel.setIsFlashClicked(false);
        } else {
            lensFacing = LensFacing.BACK;
            mSuggestedVideoModel.setIsFlashClicked(false);
        }
    }

    private void releaseCamera() {
        if (sampleGLView != null) {
            sampleGLView.onPause();
        }

        if (mCameraRecorder != null) {
            mCameraRecorder.stop();
            mCameraRecorder.release();
            mCameraRecorder = null;
        }

        if (sampleGLView != null) {
            mFlCamera.removeView(sampleGLView);
            sampleGLView = null;
        }
    }

    private void toggleFlash() {
        if (mSuggestedVideoModel.getIsFlashClicked()) {
            if (mCameraRecorder != null && mCameraRecorder.isFlashSupport()) {
                mCameraRecorder.switchFlashMode();
                mCameraRecorder.changeAutoFocus();
            }
            mSuggestedVideoModel.setIsFlashClicked(false);
        } else {
            if (mCameraRecorder != null && mCameraRecorder.isFlashSupport()) {
                mCameraRecorder.switchFlashMode();
                mCameraRecorder.changeAutoFocus();
            }
            mSuggestedVideoModel.setIsFlashClicked(true);
        }
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_SUGGESTION_CAMERA_FLIP:
                switchCamera();
                break;
            case AppConstants.ClassConstants.ON_SUGGESTION_TOGGLE_FLASH:
                toggleFlash();
                break;
            case AppConstants.ClassConstants.ON_SUGGESTION_TIMER_CLICKED:
                new TimerDialog(this).show(getSupportFragmentManager(), TimerDialog.class.getCanonicalName());
                break;
            case AppConstants.ClassConstants.ON_SUGGESTION_CLOSE_CLICKED:
                discardRecording();
                break;
            case AppConstants.ClassConstants.ON_SUGGESTION_REMOVE_SEGMENT:
                showRemoveSegmentDialog();
                break;
            case AppConstants.ClassConstants.ON_SUGGESTION_SAVE_RECORDING:
                goToPreviewActivity();
                break;
            case AppConstants.ClassConstants.ON_SUGGESTION_GALLERY_CLICKED:
                chooseVideoFromGallery();
                break;
        }
    }

    private void chooseVideoFromGallery() {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Video"), AppConstants.ClassConstants.REQUEST_TAKE_GALLERY_VIDEO);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == AppConstants.ClassConstants.REQUEST_TAKE_GALLERY_VIDEO) {
                Uri selectedImageUri = data.getData();
                // MEDIA GALLERY
                videoFromFileManager = getPath(this, selectedImageUri);
                if (videoFromFileManager != null) {
                    ArrayList<String> strings = new ArrayList<>();
                    strings.add(videoFromFileManager);
                    Intent intent = new Intent(getApplicationContext(),
                            CollabGalleryTrimmerActivity.class);
                    intent.putExtra(AppConstants.ClassConstants.SUGGESTED_VIDEO_RESULT_DATA, mResult);
                    intent.putExtra(AppConstants.ClassConstants.COLLAB_FILE_PATH, videoFromFileManager);
                    intent.putExtra(AppConstants.ClassConstants.FRAME_NUMBER, frameNo);
                    intent.putExtra(AppConstants.ClassConstants.IS_FROM, 2);
                    intent.putExtra(AppConstants.ClassConstants.SUGGESTED_VIDEO_DOWNLOAD_VIDEO, mDownloadedVideos);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        }
    }

    private void discardRecording() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.txt_discard_collab)
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> discard())
                .setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss())
                .create().show();
    }

    private void discard() {
        onBackPressed();
    }

    private void goToPreviewActivity() {
        Intent previewIntent = new Intent(this, PreviewSuggestionActivity.class);
        previewIntent.putExtra(AppConstants.ClassConstants.SUGGESTED_VIDEO_RESULT_DATA, mResult);
        previewIntent.putExtra(AppConstants.ClassConstants.SUGGESTED_VIDEO_URL, mVideoStack);
        previewIntent.putExtra(AppConstants.ClassConstants.FRAME_NUMBER, frameNo);
        previewIntent.putExtra(AppConstants.ClassConstants.USER_ID, userId);
        previewIntent.putExtra(AppConstants.ClassConstants.SUGGESTED_VIDEO_DOWNLOAD_VIDEO, mDownloadedVideos);
        startActivity(previewIntent);
    }

    @Override
    public void onTimeSelected(int time) {
        setRecordAfterTimer(time);
        mSuggestedVideoModel.setIsStartTimer(true);
        mSuggestedVideoModel.setTime("" + ((time / 1000) + 1));
    }

    private void setRecordAfterTimer(int time) {
        new CountDownTimer(time, 1000) {
            @Override
            public void onTick(long l) {
                mSuggestedVideoModel.setTime("" + ((l / 1000) + 1));
            }

            @Override
            public void onFinish() {
                startRecordingVideo();
                mSuggestedVideoModel.setIsStartTimer(false);
            }
        }.start();
    }
}