package com.whizzly.collab_module.activity;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.model.ChooseTempleteSizeModel;
import com.whizzly.collab_module.viewmodel.ChooseTempleteSizeViewModel;
import com.whizzly.collab_module.beans.CollabBarBean;
import com.whizzly.databinding.ActivityChooseTempleteSizeBinding;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.ArrayList;
import java.util.HashMap;

public class ChooseTempleteSizeActivity extends BaseActivity implements OnClickSendListener {

    private ChooseTempleteSizeModel mChooseTempleteSizeModel;
    private ChooseTempleteSizeViewModel mChooseTempleteSizeViewModel;
    private ActivityChooseTempleteSizeBinding mActivityChooseTempleteSizeBinding;
    private int mTempleteSize = 0, mMusicId;
    private String mFinalVideoPath, mAudioFilePath;
    private String imagePath = "";
    private int collabId;
    private String videoLength, videoFileUri, videoFilePath;
    private HashMap<String, ArrayList<CollabBarBean>> mCollabBarMap = new HashMap<>();
    private String mVideoThumbnailUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityChooseTempleteSizeBinding = DataBindingUtil.setContentView(this, R.layout.activity_choose_templete_size);
        mChooseTempleteSizeViewModel = ViewModelProviders.of(this).get(ChooseTempleteSizeViewModel.class);
        mActivityChooseTempleteSizeBinding.setViewModel(mChooseTempleteSizeViewModel);
        mChooseTempleteSizeModel = new ChooseTempleteSizeModel();
//        AppUtils.setStatusBarGradiant(this);
        mChooseTempleteSizeViewModel.setOnClickSendListener(this);
        mActivityChooseTempleteSizeBinding.setModel(mChooseTempleteSizeModel);
        getData();
        setViews();
    }

    private void setViews() {
        if(DataManager.getInstance().isNotchDevice()){
            mActivityChooseTempleteSizeBinding.toolbar.view.setVisibility(View.VISIBLE);
        }else{
            mActivityChooseTempleteSizeBinding.toolbar.view.setVisibility(View.GONE);
        }
        mActivityChooseTempleteSizeBinding.toolbar.tvToolbarText.setText(getString(R.string.txt_choose_templete_theme));
        mActivityChooseTempleteSizeBinding.toolbar.tvDone.setVisibility(View.VISIBLE);
        mActivityChooseTempleteSizeBinding.toolbar.tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mChooseTempleteSizeModel.getIsTwoClicked() || !mChooseTempleteSizeModel.getIsThreeClicked() ||
                        !mChooseTempleteSizeModel.getIsFourClicked() && !mChooseTempleteSizeModel.getIsFiveClicked() &&
                                !mChooseTempleteSizeModel.getIsSixClicked()) {
                    Intent intent = new Intent(ChooseTempleteSizeActivity.this, ChooseTempleteActivity.class);
                    intent.putExtra(AppConstants.ClassConstants.FRAME, imagePath);
                    intent.putExtra(AppConstants.ClassConstants.FINAL_VIDEO_FILE_PATH, mFinalVideoPath);
                    intent.putExtra(AppConstants.ClassConstants.SONG_FILE_PATH, mAudioFilePath);
                    intent.putExtra(AppConstants.ClassConstants.VIDEO_FILE_PATH, videoFilePath);
                    intent.putExtra(AppConstants.ClassConstants.TEMPLETE_SIZE, mTempleteSize);
                    intent.putExtra(AppConstants.ClassConstants.VIDEO_FILE_URI, videoFileUri);
                    intent.putExtra(AppConstants.ClassConstants.VIDEO_LENGTH, videoLength);
                    intent.putExtra(AppConstants.ClassConstants.MUSIC_ID, mMusicId);
                    intent.putExtra(AppConstants.ClassConstants.VIDEO_THUMBNAIL_URL, mVideoThumbnailUrl);
                    intent.putExtra(AppConstants.ClassConstants.COLOR_BAR_DATA, mCollabBarMap);
                    startActivity(intent);
                } else {
                    Toast.makeText(ChooseTempleteSizeActivity.this, getString(R.string.txt_error_no_template_selected), Toast.LENGTH_SHORT).show();
                }
            }
        });

        mActivityChooseTempleteSizeBinding.toolbar.ivBack.setVisibility(View.VISIBLE);
        mActivityChooseTempleteSizeBinding.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private int isRecordingUsingHeadPhones = 0;

    private void getData() {
        Intent intent = getIntent();
        collabId = intent.getIntExtra(AppConstants.ClassConstants.COLLAB_ID, 0);
        mMusicId = intent.getIntExtra(AppConstants.ClassConstants.MUSIC_ID, 0);
        imagePath = intent.getStringExtra(AppConstants.ClassConstants.FRAME);
        mFinalVideoPath = intent.getStringExtra(AppConstants.ClassConstants.FINAL_VIDEO_FILE_PATH);
        mAudioFilePath = intent.getStringExtra(AppConstants.ClassConstants.SONG_FILE_PATH);
        videoFilePath = intent.getStringExtra(AppConstants.ClassConstants.VIDEO_FILE_PATH);
        videoFileUri = intent.getStringExtra(AppConstants.ClassConstants.VIDEO_FILE_URI);
        mVideoThumbnailUrl = intent.getStringExtra(AppConstants.ClassConstants.VIDEO_THUMBNAIL_URL);
        videoLength = intent.getStringExtra(AppConstants.ClassConstants.VIDEO_LENGTH);
        mCollabBarMap = (HashMap<String, ArrayList<CollabBarBean>>) intent.getSerializableExtra(AppConstants.ClassConstants.COLOR_BAR_DATA);
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_TEMP_SIZE_TWO:
                mTempleteSize = 2;
                setView(true, false, false, false, false);
                break;
            case AppConstants.ClassConstants.ON_TEMP_SIZE_THREE:
                mTempleteSize = 3;
                setView(false, true, false, false, false);
                break;
            case AppConstants.ClassConstants.ON_TEMP_SIZE_FOUR:
                mTempleteSize = 4;
                setView(false, false, true, false, false);
                break;
            case AppConstants.ClassConstants.ON_TEMP_SIZE_FIVE:
                mTempleteSize = 5;
                setView(false, false, false, true, false);
                break;
            case AppConstants.ClassConstants.ON_TEMP_SIZE_SIX:
                mTempleteSize = 6;
                setView(false, false, false, false, true);
                break;
        }
    }

    private void setView(boolean twoClicked, boolean threeClicked, boolean fourClicked, boolean fiveClicked, boolean sixClicked) {
        mChooseTempleteSizeModel.setIsTwoClicked(twoClicked);
        mChooseTempleteSizeModel.setIsThreeClicked(threeClicked);
        mChooseTempleteSizeModel.setIsFourClicked(fourClicked);
        mChooseTempleteSizeModel.setIsFiveClicked(fiveClicked);
        mChooseTempleteSizeModel.setIsSixClicked(sixClicked);
    }
}
