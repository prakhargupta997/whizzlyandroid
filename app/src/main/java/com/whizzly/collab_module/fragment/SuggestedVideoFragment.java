package com.whizzly.collab_module.fragment;


import android.app.Activity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.SuggestVideoRecyclerViewAdapter;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.collab_module.model.SuggestedVideoFragmentModel;
import com.whizzly.collab_module.viewmodel.SuggestedVideoFragmentViewModel;
import com.whizzly.databinding.FragmentSuggestedVideoBinding;
import com.whizzly.home_screen_module.home_screen_fragments.home_feeds.FeedVideoPreviewFragment;
import com.whizzly.interfaces.OnBackPressedListener;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.video_list_response.Result;
import com.whizzly.models.video_list_response.VideoListResponseBean;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;
import com.whizzly.utils.SpacesItemDecorationGrid;
import com.whizzly.utils.ToolbarElevationOffsetListener;

import java.util.ArrayList;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class SuggestedVideoFragment extends Fragment implements OnClickSendListener, OnClickAdapterListener, OnBackPressedListener {
    private FragmentSuggestedVideoBinding mFragmentSuggestedVideoBinding;
    private int musicId;
    private SuggestedVideoFragmentViewModel mSuggestedVideoFragmentViewModel;
    private SuggestedVideoFragmentModel mSuggestedVideoFragmentModel;
    private String musicName, musicCollabCount, musicComposerName, musicUrl, musicThumbnail;
    private Activity mActivity;
    private ArrayList<Result> videoListResponseList;
    private SuggestVideoRecyclerViewAdapter mSuggestVideoRecyclerViewAdapter;
    private Observer<VideoListResponseBean> observer;
    private String isFrom;

    public SuggestedVideoFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getArgumentData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFragmentSuggestedVideoBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_suggested_video, container, false);
        mSuggestedVideoFragmentViewModel = ViewModelProviders.of(this).get(SuggestedVideoFragmentViewModel.class);
        mFragmentSuggestedVideoBinding.setViewModel(mSuggestedVideoFragmentViewModel);
        mSuggestedVideoFragmentModel = new SuggestedVideoFragmentModel();
        mFragmentSuggestedVideoBinding.setModel(mSuggestedVideoFragmentModel);
        mSuggestedVideoFragmentViewModel.setOnClickSendListener(this);
        if(DataManager.getInstance().isNotchDevice()){
            mFragmentSuggestedVideoBinding.view.setVisibility(View.VISIBLE);
        }else{
            mFragmentSuggestedVideoBinding.view.setVisibility(View.GONE);
        }
        videoListResponseList = new ArrayList<>();
          mActivity = getActivity();
        ((BaseActivity) getActivity()).setOnBackPressedListener(this);
        mSuggestedVideoFragmentViewModel.setSuggestedVideoFragmentModel(mSuggestedVideoFragmentModel);
        mSuggestedVideoFragmentViewModel.initializeLiveData(((BaseActivity) getActivity()).getFailureResponseObserver(), ((BaseActivity) getActivity()).getErrorObserver());
        observer = videoListResponseBean -> {
            mFragmentSuggestedVideoBinding.contentSuggestion.flProgressBar.setVisibility(View.GONE);
           if (videoListResponseBean!=null)
           {
               if (videoListResponseBean.getCode() == 200) {
                   if (videoListResponseBean.getResult() != null && videoListResponseBean.getResult().size() > 0) {
                       mFragmentSuggestedVideoBinding.contentSuggestion.flLonely.setVisibility(View.GONE);
                       mSuggestedVideoFragmentModel.setIsEmpty(false);
                       videoListResponseList.clear();
                       enableScroll();
                       mSuggestedVideoFragmentModel.setReuseCount(musicCollabCount);
                       videoListResponseList.addAll(videoListResponseBean.getResult());
                       mSuggestVideoRecyclerViewAdapter.notifyDataSetChanged();
                   } else {
                       mSuggestedVideoFragmentModel.setReuseCount("0");
                       mSuggestedVideoFragmentModel.setIsEmpty(true);
                       disableScroll();
                       mFragmentSuggestedVideoBinding.toolbar.setVisibility(View.GONE);
                       mFragmentSuggestedVideoBinding.contentSuggestion.flLonely.setVisibility(View.VISIBLE);
                   }
               }
               else if (videoListResponseBean.getCode()==301||videoListResponseBean.getCode()==302){
                   Toast.makeText(mActivity, videoListResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                   ((BaseActivity) Objects.requireNonNull(getActivity())).autoLogOut();
               }
               else {
                       disableScroll();
                       mSuggestedVideoFragmentModel.setReuseCount("0");
                       mFragmentSuggestedVideoBinding.toolbar.setVisibility(View.GONE);
                       Toast.makeText(mActivity, videoListResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
               }
           }
        };
        setViews();
        return mFragmentSuggestedVideoBinding.getRoot();
    }

    private void enableScroll() {
        final AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams)
                mFragmentSuggestedVideoBinding.ctToolbar.getLayoutParams();
        params.setScrollFlags(
                AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
                        | AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED
        );
        mFragmentSuggestedVideoBinding.ctToolbar.setLayoutParams(params);
    }

    private void disableScroll() {
        final AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams)
                mFragmentSuggestedVideoBinding.ctToolbar.getLayoutParams();
        params.setScrollFlags(0);
        mFragmentSuggestedVideoBinding.ctToolbar.setLayoutParams(params);
    }

    private void hitSuggestedVideoListApi() {
        mFragmentSuggestedVideoBinding.contentSuggestion.flProgressBar.setVisibility(View.VISIBLE);
        mSuggestedVideoFragmentViewModel.hitSuggestedListApi(String.valueOf(musicId));
    }

    private void getArgumentData() {
        Bundle musicDetail = getArguments();
        if (musicDetail != null) {
            isFrom = musicDetail.getString(AppConstants.ClassConstants.IS_FROM);
            musicId = musicDetail.getInt(AppConstants.ClassConstants.MUSIC_ID);
            musicName = musicDetail.getString(AppConstants.ClassConstants.MUSIC_NAME);
            musicCollabCount = musicDetail.getString(AppConstants.ClassConstants.MUSIC_COLLAB_COUNT);
            musicComposerName = musicDetail.getString(AppConstants.ClassConstants.MUSIC_COMPOSER);
            musicUrl = musicDetail.getString(AppConstants.ClassConstants.MUSIC_LINK);
            musicThumbnail = musicDetail.getString(AppConstants.ClassConstants.MUSIC_THUMBNAIL);
        }
    }

    private void setViews() {
        mSuggestedVideoFragmentModel.setSongName(musicName);
        mSuggestedVideoFragmentModel.setSongComposer(musicComposerName);
        if (musicThumbnail != null)
            mSuggestedVideoFragmentModel.setSongThumbnailUrl(Uri.parse(musicThumbnail));
        mFragmentSuggestedVideoBinding.abToolbar.addOnOffsetChangedListener(new ToolbarElevationOffsetListener(mFragmentSuggestedVideoBinding.toolbar));
        if (isFrom.equalsIgnoreCase(AppConstants.ClassConstants.IS_FROM_FEED)) {
            mFragmentSuggestedVideoBinding.btnRecord.setVisibility(View.GONE);
            setNullableRV();
        } else {
            mFragmentSuggestedVideoBinding.btnRecord.setVisibility(View.VISIBLE);
            setRecyclerView();
        }
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.SUGGESTED_BACK_PRESSED:
                mActivity.onBackPressed();
                break;
            case AppConstants.ClassConstants.SUGGESTED_CROP_AUDIO:
                if (mActivity != null && !mActivity.isFinishing()) {
                    AudioCropperFragment audioCropperFragment = new AudioCropperFragment();
                    Bundle audioCrop = new Bundle();
                    audioCrop.putBoolean(AppConstants.ClassConstants.WAS_GET_CONTENT_INTENT, false);
                    audioCrop.putString(AppConstants.ClassConstants.MUSIC_NAME, musicName);
                    audioCrop.putString(AppConstants.ClassConstants.MUSIC_COMPOSER, musicComposerName);
                    audioCrop.putString(AppConstants.ClassConstants.MUSIC_THUMBNAIL, musicThumbnail);
                    audioCrop.putString(AppConstants.ClassConstants.MUSIC_LINK, musicUrl);
                    audioCrop.putInt(AppConstants.ClassConstants.MUSIC_ID, musicId);
                    audioCropperFragment.setArguments(audioCrop);
                    ((BaseActivity) mActivity).addFragmentFromRight(R.id.fl_container, audioCropperFragment);
                }
                break;
        }
    }

    private void setRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(mActivity, 3);
        mFragmentSuggestedVideoBinding.contentSuggestion.rvSuggestedVideos.setLayoutManager(layoutManager);
        mFragmentSuggestedVideoBinding.contentSuggestion.rvSuggestedVideos.addItemDecoration(new SpacesItemDecorationGrid(getActivity(), 1, 3));
        mSuggestVideoRecyclerViewAdapter = new SuggestVideoRecyclerViewAdapter(videoListResponseList, this);
        mFragmentSuggestedVideoBinding.contentSuggestion.rvSuggestedVideos.setAdapter(mSuggestVideoRecyclerViewAdapter);
    }

    private void setNullableRV() {
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(mActivity, 3);
        mFragmentSuggestedVideoBinding.contentSuggestion.rvSuggestedVideos.setLayoutManager(layoutManager);
        mFragmentSuggestedVideoBinding.contentSuggestion.rvSuggestedVideos.addItemDecoration(new SpacesItemDecorationGrid(getActivity(), 1, 3));
        mSuggestVideoRecyclerViewAdapter = new SuggestVideoRecyclerViewAdapter(videoListResponseList, this);
        mFragmentSuggestedVideoBinding.contentSuggestion.rvSuggestedVideos.setAdapter(mSuggestVideoRecyclerViewAdapter);
    }

    @Override
    public void onAdapterClicked(int code, Object obj, int position) {
        if (isFrom.equalsIgnoreCase(AppConstants.ClassConstants.IS_FROM_FEED)) {
            FeedVideoPreviewFragment feedVideoPreviewFragment = new FeedVideoPreviewFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(AppConstants.ClassConstants.VIDEO_DETAILS, (Result) obj);
            bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_SUGGESTED);
            feedVideoPreviewFragment.setArguments(bundle);
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, feedVideoPreviewFragment).commit();
        } else {
            if (obj instanceof Result) {
                setArgumentsForPreview((Result) obj);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        videoListResponseList.clear();
        mSuggestedVideoFragmentViewModel.getmVideoListResponseBeanRichMediatorLiveData().removeObserver(observer);
    }

    @Override
    public void onResume() {
        super.onResume();
        mSuggestedVideoFragmentViewModel.getmVideoListResponseBeanRichMediatorLiveData().observe(this, observer);
        hitSuggestedVideoListApi();
    }

    private void setArgumentsForPreview(Result preview) {
        Bundle videoBundle = new Bundle();
        videoBundle.putParcelable(AppConstants.ClassConstants.VIDEO_PREVIEW_BUNDLE, preview);
        videoBundle.putInt(AppConstants.ClassConstants.FRAGMENT_IS_FROM, AppConstants.ClassConstants.IS_FROM_SUGGESTED);
        VideoPreviewFragment videoPreviewFragment = new VideoPreviewFragment();
        videoPreviewFragment.setArguments(videoBundle);
        ((BaseActivity) mActivity).addFragmentFromRight(R.id.fl_container, videoPreviewFragment);
    }


    @Override
    public void onBackPressed() {
     if (getActivity()!=null){
         getActivity().getSupportFragmentManager().popBackStack();
         ((BaseActivity)getActivity()).setOnBackPressedListener(null);

     }
    }
}