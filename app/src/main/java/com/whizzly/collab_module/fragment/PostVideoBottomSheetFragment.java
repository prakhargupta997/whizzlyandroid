package com.whizzly.collab_module.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.Gson;
import com.whizzly.R;
import com.whizzly.collab_module.beans.CollabBarBean;
import com.whizzly.collab_module.collab_model.CollabPostModel;
import com.whizzly.collab_module.viewmodel.PostVideoBottomSheetFragmentViewModel;
import com.whizzly.databinding.FragmentPostVideoBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class PostVideoBottomSheetFragment extends BottomSheetDialogFragment implements OnClickSendListener {

    private FragmentPostVideoBinding mFragmentPostVideoBinding;
    private PostVideoBottomSheetFragmentViewModel mPostVideoBottomSheetFragmentViewModel;
    private String imageFile = "";
    private HashMap<String, ArrayList<CollabBarBean>> mColorBarBean = new HashMap<>();
    private String mVideoTitle;
    private String mVideoHashtag;
    private int isShareable = 0;
    private CollabPostModel mCollabPostModel;
    private String description, hashtag;

    public PostVideoBottomSheetFragment() {
        // Required empty public constructor
    }

    private void getData() {
        Bundle bundle = getArguments();
        if (bundle.containsKey("is_from")) switch (bundle.getString("is_from")) {
            case "0":
                mCollabPostModel = bundle.getParcelable("post_data");
                break;
            case "1":
                mCollabPostModel = bundle.getParcelable("post_data");
                break;
            case "2":
                mCollabPostModel = bundle.getParcelable("post_data");
                break;
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString(AppConstants.ClassConstants.VIDEO_HASHTAGS, mFragmentPostVideoBinding.etHashtags.getText().toString());
        outState.putString(AppConstants.ClassConstants.VIDEO_DATA, mFragmentPostVideoBinding.etWriteCaption.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            hashtag = savedInstanceState.getString(AppConstants.ClassConstants.VIDEO_HASHTAGS);
            description = savedInstanceState.getString(AppConstants.ClassConstants.VIDEO_DATA);
        }
        mFragmentPostVideoBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_post_video, container, false);
        mPostVideoBottomSheetFragmentViewModel = ViewModelProviders.of(this).get(PostVideoBottomSheetFragmentViewModel.class);
        mFragmentPostVideoBinding.setViewModel(mPostVideoBottomSheetFragmentViewModel);
        mPostVideoBottomSheetFragmentViewModel.setOnClickSendListener(this);
        mFragmentPostVideoBinding.etHashtags.setText(hashtag);
        mFragmentPostVideoBinding.etWriteCaption.setText(description);
        imageFile = mCollabPostModel.getCollabThumbnailUrl();
        File file = new File(imageFile);
        setViews();
        Glide.with(this).load(Uri.fromFile(file)).into(mFragmentPostVideoBinding.ivVideoFrame);
        mFragmentPostVideoBinding.etHashtags.setOnFocusChangeListener((view, b) -> {
            if (!b)
                hideKeyboard(view);
            else {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(mFragmentPostVideoBinding.etHashtags, InputMethodManager.SHOW_IMPLICIT);
                String text = mFragmentPostVideoBinding.etHashtags.getText().toString();
                if (text.trim().length() != 0) {
                    char last = text.trim().charAt(text.length() - 1);
                    if (last != '#')
                        mFragmentPostVideoBinding.etHashtags.setText(text + " #");
                } else
                    mFragmentPostVideoBinding.etHashtags.setText("#");

                mFragmentPostVideoBinding.etHashtags.setSelection(mFragmentPostVideoBinding.etHashtags.getText().toString().length());
            }
        });

        mFragmentPostVideoBinding.etWriteCaption.setOnFocusChangeListener((view, b) -> {
            if (!b)
                hideKeyboard(view);
        });

        mFragmentPostVideoBinding.etHashtags.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (s.length() > 0) {
                    char last = s.charAt(s.length() - 1);
                    if (last == ' ') {
                        mFragmentPostVideoBinding.etHashtags.setText(String.format("%s#", s));
                        mFragmentPostVideoBinding.etHashtags.setSelection(mFragmentPostVideoBinding.etHashtags.getText().toString().length());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return mFragmentPostVideoBinding.getRoot();
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getData();

    }

    private void setViews() {
        mVideoTitle = mCollabPostModel.getCollabTitle();
        if (mVideoTitle != null && !mVideoTitle.isEmpty())
            mFragmentPostVideoBinding.etWriteCaption.setText(mVideoTitle);
        mVideoHashtag = mCollabPostModel.getCollabHashtags();
        if (mVideoHashtag != null && !mVideoTitle.isEmpty()) {
            String[] hashtags = mVideoHashtag.split(",");
            StringBuilder hashtagBuillder = new StringBuilder();
            if (!TextUtils.isEmpty(mVideoHashtag)) {
                for (int i = 0; i < hashtags.length; i++) {
                    if (i == 0) {
                        hashtagBuillder.append("#" + hashtags[i]);
                    } else {
                        hashtagBuillder.append(" #" + hashtags[i]);
                    }
                }

                mFragmentPostVideoBinding.etHashtags.setText(hashtagBuillder);
            }
        }
    }

    @Override
    public void onClickSend(int code) {
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        String json;
        if (AppConstants.ClassConstants.mColorBarMap != null)
            AppConstants.ClassConstants.mColorBarMap.clear();
        String description = mFragmentPostVideoBinding.etWriteCaption.getText().toString().trim();
        String hashtags = mFragmentPostVideoBinding.etHashtags.getText().toString().trim();

        switch (code) {
            case AppConstants
                    .ClassConstants.POST_VIDEO_PRIVATE_CLICKED:
//                if (!hashtags.isEmpty()) {
                if (AppUtils.isNetworkAvailable(getActivity())) {
                    mCollabPostModel.setCollabHashtags(hashtags);
                    mCollabPostModel.setCollabTitle(description);
                    mCollabPostModel.setCollabPrivacyTypeId(1);
                    json = new Gson().toJson(mCollabPostModel);
                    intent.putExtra(AppConstants.ClassConstants.IS_SHAREABLE, isShareable);
                    intent.putExtra(AppConstants.ClassConstants.VIDEO_DATA, json);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                    Objects.requireNonNull(getActivity()).finishAffinity();
                } else {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
                }
                /*} else {
                    Toast.makeText(getActivity(), R.string.txt_error_no_hashtag, Toast.LENGTH_SHORT).show();
                }*/
                break;
            case AppConstants
                    .ClassConstants.POST_VIDEO_PUBLIC_CLICKED:
/*
                if (!hashtags.isEmpty()) {
*/
                if (AppUtils.isNetworkAvailable(getActivity())) {
                    mCollabPostModel.setCollabPrivacyTypeId(2);
                    mCollabPostModel.setCollabTitle(description);
                    mCollabPostModel.setCollabHashtags(hashtags);
                    json = new Gson().toJson(mCollabPostModel);
                    intent.putExtra(AppConstants.ClassConstants.VIDEO_DATA, json);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(intent);
                    Objects.requireNonNull(getActivity()).finishAffinity();
                } else {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
                }
                break;
            case AppConstants.ClassConstants.POST_VIDEO_FACEBOOK_CLICKED:
                if (mFragmentPostVideoBinding.ivShareFacebook.isSelected()) {
                    mFragmentPostVideoBinding.ivShareFacebook.setSelected(false);
                    isShareable = 0;
                } else {
                    mFragmentPostVideoBinding.ivShareFacebook.setSelected(true);
                    mFragmentPostVideoBinding.ivShareInstagram.setSelected(false);
                    isShareable = AppConstants.ClassConstants.SHARE_ON_FACEBOOK;
                }
                break;
            case AppConstants.ClassConstants.POST_VIDEO_INSTAGRAM_CLICKED:
                if (mFragmentPostVideoBinding.ivShareInstagram.isSelected()) {
                    mFragmentPostVideoBinding.ivShareInstagram.setSelected(false);
                    isShareable = 0;
                } else {
                    mFragmentPostVideoBinding.ivShareInstagram.setSelected(true);
                    mFragmentPostVideoBinding.ivShareFacebook.setSelected(false);
                    isShareable = AppConstants.ClassConstants.SHARE_ON_INSTAGRAM;
                }
                break;
        }
    }
}

