package com.whizzly.collab_module.fragment;


import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.whizzly.R;
import com.whizzly.collab_module.VideoFrameRecyclerViewAdapter;
import com.whizzly.collab_module.beans.CollabBarBean;
import com.whizzly.collab_module.beans.VideoFramesBean;
import com.whizzly.collab_module.interfaces.OnSendTimeRange;
import com.whizzly.collab_module.model.CollabBarModel;
import com.whizzly.collab_module.viewmodel.CollabBarViewModel;
import com.whizzly.databinding.FragmentCollabBarBinding;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import static com.whizzly.utils.AppConstants.ClassConstants.mColorBarMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class CollabBarFragment extends BottomSheetDialogFragment implements OnClickSendListener {

    private FragmentCollabBarBinding mFragmentCollabBarBinding;
    private CollabBarViewModel mCollabBarViewModel;
    private CollabBarModel mCollabBarModel;
    private String mFilePath = "";
    private OnSendTimeRange onSendTimeRange;
    private Uri mVideoFileUri;
    private String duration = "";
    private ExoPlayer mExoPlayer;
    private int templateSize;
    private ArrayList<VideoFramesBean> mVideoFrameBeanList;
    private VideoFrameRecyclerViewAdapter mVideoFrameRecyclerViewAdapter;
    private int frameWidth;
    private ArrayList<CollabBarBean> mColorBarBeans = new ArrayList<>();
    private long durationInMillies;
    private int selectedColor = 0;
    private float leftPinIndex, rightPinIndex;
    private ArrayList<CollabBarBean> mUndoCollabBars = new ArrayList<>();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onSendTimeRange = (OnSendTimeRange) getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getData();
    }

    private void getData() {
        Bundle collabBarBundle = getArguments();
        mFilePath = collabBarBundle.getString(AppConstants.ClassConstants.VIDEO_FILE_PATH);
        mVideoFileUri = Uri.parse(collabBarBundle.getString(AppConstants.ClassConstants.VIDEO_FILE_URI));
        duration = collabBarBundle.getString(AppConstants.ClassConstants.VIDEO_LENGTH);
        durationInMillies = collabBarBundle.getLong(AppConstants.ClassConstants.DURATION_IN_MILLIES);
        mVideoFrameBeanList = collabBarBundle.getParcelableArrayList(AppConstants.ClassConstants.FRAMES_LIST);
        templateSize = collabBarBundle.getInt(AppConstants.ClassConstants.TEMPLETE_SIZE, 0);
        frameWidth = collabBarBundle.getInt(AppConstants.ClassConstants.FRAME_SIZE, 60);
        if (collabBarBundle.containsKey(AppConstants.ClassConstants.COLOR_BAR_DATA))
            mColorBarBeans = collabBarBundle.getParcelableArrayList(AppConstants.ClassConstants.COLOR_BAR_DATA);
    }

    private void setRecyclerView() {
        mFragmentCollabBarBinding.rvFrames.setMinimumHeight(frameWidth);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mFragmentCollabBarBinding.rvFrames.setLayoutManager(linearLayoutManager);
        mVideoFrameRecyclerViewAdapter = new VideoFrameRecyclerViewAdapter(mVideoFrameBeanList, frameWidth);
        mFragmentCollabBarBinding.setAdapter(mVideoFrameRecyclerViewAdapter);
    }

    private void setExoplayer() {
        mExoPlayer = ExoPlayerFactory.newSimpleInstance(getActivity(),
                new DefaultRenderersFactory(getActivity()),
                new DefaultTrackSelector(), new DefaultLoadControl());
        Uri uri = Uri.parse(mFilePath);
        MediaSource mediaSource = buildMediaSource(uri);
        mExoPlayer.setPlayWhenReady(true);
        mExoPlayer.setRepeatMode(Player.REPEAT_MODE_ONE);
        mExoPlayer.getAudioComponent().setVolume(0F);
        mFragmentCollabBarBinding.playerView.setPlayer(mExoPlayer);
        mExoPlayer.prepare(mediaSource, true, false);
    }

    private MediaSource buildMediaSource(Uri uri) {
        return new ExtractorMediaSource.Factory(
                new DefaultDataSourceFactory(Objects.requireNonNull(getActivity()), "whizzly-app")).
                createMediaSource(uri);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFragmentCollabBarBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_collab_bar, container, false);
        mCollabBarViewModel = ViewModelProviders.of(this).get(CollabBarViewModel.class);
        mCollabBarModel = new CollabBarModel();
        mFragmentCollabBarBinding.setViewModel(mCollabBarViewModel);
        mFragmentCollabBarBinding.setModel(mCollabBarModel);
        mCollabBarViewModel.setOnClickSendListener(this);
        mCollabBarViewModel.setCollabBarModel(mCollabBarModel);
        mCollabBarModel.setmVideoDuration(String.valueOf(Integer.parseInt(duration)/1000));
        if (savedInstanceState != null) {
            mFilePath = savedInstanceState.getString(AppConstants.ClassConstants.VIDEO_FILE_PATH);
            mVideoFileUri = Uri.parse(savedInstanceState.getString(AppConstants.ClassConstants.VIDEO_FILE_URI));
            duration = savedInstanceState.getString(AppConstants.ClassConstants.VIDEO_LENGTH);
            mColorBarBeans = savedInstanceState.getParcelableArrayList(AppConstants.ClassConstants.COLOR_BAR_DATA);
        }
        setRecyclerView();
        setExoplayer();
        mFragmentCollabBarBinding.videoStartTime.setText("00:00");
        mFragmentCollabBarBinding.videoEndTime.setText(String.format("00:%02d", Integer.parseInt(duration)/1000));
        getDialog().setOnShowListener(dialogInterface -> {
            BottomSheetDialog d = (BottomSheetDialog) dialogInterface;
            FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
            BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
        });
        setColorSelection(templateSize);
        setRangeSliders();
        setColorBar();
        return mFragmentCollabBarBinding.getRoot();
    }

    private void setRangeSliders() {
        int screenWidth = getActivity().getWindowManager().getDefaultDisplay().getWidth();
        mFragmentCollabBarBinding.rangeSlider.setRangeChangeListener((view, leftPinIndex, rightPinIndex) -> {
            mFragmentCollabBarBinding.tvTimeLeft.setVisibility(View.VISIBLE);
            mFragmentCollabBarBinding.tvTimeRight.setVisibility(View.VISIBLE);
            this.leftPinIndex = leftPinIndex;
            this.rightPinIndex = rightPinIndex;
            float startTime = ((float) durationInMillies / (float) screenWidth) * leftPinIndex;
            float endTime = ((float) durationInMillies / (float) screenWidth) * rightPinIndex;
            mFragmentCollabBarBinding.tvTimeLeft.setText(String.format("00:%02d", (int) (startTime / 1000)));
            mFragmentCollabBarBinding.tvTimeRight.setText(String.format("00:%02d", (int) (endTime / 1000)));
            if ((rightPinIndex - leftPinIndex) < 120) {
                mFragmentCollabBarBinding.tvTimeRight.setX(rightPinIndex + 6);
                mFragmentCollabBarBinding.tvTimeLeft.setX(leftPinIndex - mFragmentCollabBarBinding.tvTimeLeft.getWidth());
            } else {
                mFragmentCollabBarBinding.tvTimeLeft.setX(leftPinIndex);
                mFragmentCollabBarBinding.tvTimeRight.setX(rightPinIndex - mFragmentCollabBarBinding.tvTimeRight.getWidth() + 6);
            }
            mFragmentCollabBarBinding.tvTimeLeft.setY(mFragmentCollabBarBinding.rangeSlider.getY() + (mFragmentCollabBarBinding.rangeSlider.getHeight() / 2) - (mFragmentCollabBarBinding.tvTimeLeft.getHeight() / 2));
            mFragmentCollabBarBinding.tvTimeRight.setY(mFragmentCollabBarBinding.rangeSlider.getY() + (mFragmentCollabBarBinding.rangeSlider.getHeight() / 2) - (mFragmentCollabBarBinding.tvTimeRight.getHeight() / 2));
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mExoPlayer != null) {
            mExoPlayer.release();
            mExoPlayer = null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mExoPlayer != null) {
            mExoPlayer.setPlayWhenReady(true);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mExoPlayer != null) {
            mExoPlayer.setPlayWhenReady(false);
        }
    }

    private void setColorSelection(int templateSize) {
        switch (templateSize) {
            case 2:
                setVisibilityOfColor(View.VISIBLE, View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE);
                break;
            case 3:
                setVisibilityOfColor(View.VISIBLE, View.VISIBLE, View.VISIBLE, View.GONE, View.GONE, View.GONE);
                break;
            case 4:
                setVisibilityOfColor(View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.GONE, View.GONE);
                break;
            case 5:
                setVisibilityOfColor(View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.GONE);
                break;
            case 6:
                setVisibilityOfColor(View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE);
                break;
        }
    }

    private void setVisibilityOfColor(int colorA, int colorB, int colorC, int colorD, int colorE, int colorF) {
        mFragmentCollabBarBinding.llColorA.setVisibility(colorA);
        mFragmentCollabBarBinding.llColorB.setVisibility(colorB);
        mFragmentCollabBarBinding.llColorC.setVisibility(colorC);
        mFragmentCollabBarBinding.llColorD.setVisibility(colorD);
        mFragmentCollabBarBinding.llColorE.setVisibility(colorE);
        mFragmentCollabBarBinding.llColorF.setVisibility(colorF);
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_COLLAB_BAR_DONE:
                if (getActivity() != null) {
                    onSendTimeRange.onSendTimeRange(mColorBarBeans);
                    dismiss();
                }
                break;
            case AppConstants.ClassConstants.ON_COLLAB_BAR_CANCEL:
                dismiss();
                break;
            case AppConstants.ClassConstants.ON_UNDO_CLICKED:
                if (mColorBarBeans.size() > 0) {
                    mUndoCollabBars.add(mColorBarBeans.get(mColorBarBeans.size() - 1));
                    mColorBarBeans.remove(mColorBarBeans.size() - 1);
                    setColorBar();
                }
                break;
            case AppConstants.ClassConstants.ON_REDO_CLICKED:
                if (mUndoCollabBars.size() > 0) {
                    mColorBarBeans.add(mUndoCollabBars.get(mUndoCollabBars.size() - 1));
                    mUndoCollabBars.remove(mUndoCollabBars.size() - 1);
                    setColorBar();
                }
                break;
            case AppConstants.ClassConstants.ON_ADD_CLICKED:
                if (!mFragmentCollabBarBinding.tvDescription.getText().toString().isEmpty() && selectedColor != -1) {
                    int screenWidth = getActivity().getWindowManager().getDefaultDisplay().getWidth();
                    float startTime = ((float) durationInMillies / (float) screenWidth) * leftPinIndex;
                    float endTime = ((float) durationInMillies / (float) screenWidth) * rightPinIndex;
                    CollabBarBean collabBarBean = new CollabBarBean();
                    switch (selectedColor) {
                        case 0:
                            collabBarBean.setCode(String.valueOf(selectedColor));
                            collabBarBean.setColorCode("#009359");
                            collabBarBean.setEndTime(endTime);
                            collabBarBean.setStartTime(startTime);
                            collabBarBean.setDurationInMillies(durationInMillies);
                            collabBarBean.setDescription(mFragmentCollabBarBinding.tvDescription.getText().toString());
                            mColorBarBeans.add(collabBarBean);
                            break;
                        case 1:
                            collabBarBean.setCode(String.valueOf(selectedColor));
                            collabBarBean.setColorCode("#FFB437");
                            collabBarBean.setEndTime(endTime);
                            collabBarBean.setStartTime(startTime);
                            collabBarBean.setDurationInMillies(durationInMillies);
                            collabBarBean.setDescription(mFragmentCollabBarBinding.tvDescription.getText().toString());
                            mColorBarBeans.add(collabBarBean);
                            break;
                        case 2:
                            collabBarBean.setCode(String.valueOf(selectedColor));
                            collabBarBean.setColorCode("#3F7AE9");
                            collabBarBean.setEndTime(endTime);
                            collabBarBean.setStartTime(startTime);
                            collabBarBean.setDurationInMillies(durationInMillies);
                            collabBarBean.setDescription(mFragmentCollabBarBinding.tvDescription.getText().toString());
                            mColorBarBeans.add(collabBarBean);
                            break;
                        case 3:
                            collabBarBean.setCode(String.valueOf(selectedColor));
                            collabBarBean.setColorCode("#FF9640");
                            collabBarBean.setEndTime(endTime);
                            collabBarBean.setStartTime(startTime);
                            collabBarBean.setDurationInMillies(durationInMillies);
                            collabBarBean.setDescription(mFragmentCollabBarBinding.tvDescription.getText().toString());
                            mColorBarBeans.add(collabBarBean);
                            break;
                        case 4:
                            collabBarBean.setCode(String.valueOf(selectedColor));
                            collabBarBean.setColorCode("#F85458");
                            collabBarBean.setEndTime(endTime);
                            collabBarBean.setStartTime(startTime);
                            collabBarBean.setDurationInMillies(durationInMillies);
                            collabBarBean.setDescription(mFragmentCollabBarBinding.tvDescription.getText().toString());
                            mColorBarBeans.add(collabBarBean);
                            break;
                        case 5:
                            collabBarBean.setCode(String.valueOf(selectedColor));
                            collabBarBean.setColorCode("#8E5BE8");
                            collabBarBean.setEndTime(endTime);
                            collabBarBean.setStartTime(startTime);
                            collabBarBean.setDurationInMillies(durationInMillies);
                            collabBarBean.setDescription(mFragmentCollabBarBinding.tvDescription.getText().toString());
                            mColorBarBeans.add(collabBarBean);
                            break;
                        case 6:
                            collabBarBean.setCode(String.valueOf(selectedColor));
                            collabBarBean.setColorCode("#C9598E");
                            collabBarBean.setEndTime(endTime);
                            collabBarBean.setStartTime(startTime);
                            collabBarBean.setDurationInMillies(durationInMillies);
                            collabBarBean.setDescription(mFragmentCollabBarBinding.tvDescription.getText().toString());
                            mColorBarBeans.add(collabBarBean);
                            break;
                    }
                    mFragmentCollabBarBinding.ivAddColor.setImageResource(R.drawable.ic_tab_bar_add);
                    mFragmentCollabBarBinding.tvDescription.setText("");
                    mFragmentCollabBarBinding.rangeSlider.setMaskColor(Color.TRANSPARENT);
                    mFragmentCollabBarBinding.rangeSlider.invalidate();
                    mFragmentCollabBarBinding.rangeSlider.moveThumbByIndex(mFragmentCollabBarBinding.rangeSlider.mLeftThumb, 0);
                    mFragmentCollabBarBinding.rangeSlider.moveThumbByIndex(mFragmentCollabBarBinding.rangeSlider.mRightThumb, 1);
                    mFragmentCollabBarBinding.tvTimeRight.setVisibility(View.GONE);
                    mFragmentCollabBarBinding.tvTimeLeft.setVisibility(View.GONE);
                    leftPinIndex = 0;
                    rightPinIndex = 0;
                    mCollabBarModel.setSelected(-1);
                    selectedColor = -1;
                    setColorBar();
                } else {
                    Toast.makeText(getActivity(), "Please add description to proceed.", Toast.LENGTH_SHORT).show();
                }
                break;
            case AppConstants.ClassConstants.COLLAB_BAR_A:
                mFragmentCollabBarBinding.rangeSlider.invalidate();
                mFragmentCollabBarBinding.rangeSlider.setMaskColor(Color.parseColor("#70009359"));
                mFragmentCollabBarBinding.ivAddColor.setImageResource(R.drawable.ic_camera_collab_added_tick);
                selectedColor = 0;
                mFragmentCollabBarBinding.rangeSlider.refreshDrawableState();
                break;
            case AppConstants.ClassConstants.COLLAB_BAR_B:
                mFragmentCollabBarBinding.rangeSlider.invalidate();
                mFragmentCollabBarBinding.rangeSlider.setMaskColor(Color.parseColor("#70FFB437"));
                mFragmentCollabBarBinding.ivAddColor.setImageResource(R.drawable.ic_camera_collab_added_tick);
                selectedColor = 1;
                mFragmentCollabBarBinding.rangeSlider.refreshDrawableState();
                break;
            case AppConstants.ClassConstants.COLLAB_BAR_C:
                mFragmentCollabBarBinding.rangeSlider.invalidate();
                mFragmentCollabBarBinding.rangeSlider.setMaskColor(Color.parseColor("#703F7AE9"));
                mFragmentCollabBarBinding.ivAddColor.setImageResource(R.drawable.ic_camera_collab_added_tick);
                selectedColor = 2;
                mFragmentCollabBarBinding.rangeSlider.refreshDrawableState();
                break;
            case AppConstants.ClassConstants.COLLAB_BAR_D:
                mFragmentCollabBarBinding.rangeSlider.invalidate();
                mFragmentCollabBarBinding.rangeSlider.setMaskColor(Color.parseColor("#70FF9640"));
                mFragmentCollabBarBinding.ivAddColor.setImageResource(R.drawable.ic_camera_collab_added_tick);
                selectedColor = 3;
                mFragmentCollabBarBinding.rangeSlider.refreshDrawableState();
                break;
            case AppConstants.ClassConstants.COLLAB_BAR_E:
                mFragmentCollabBarBinding.rangeSlider.invalidate();
                mFragmentCollabBarBinding.rangeSlider.setMaskColor(Color.parseColor("#70F85458"));
                mFragmentCollabBarBinding.ivAddColor.setImageResource(R.drawable.ic_camera_collab_added_tick);
                selectedColor = 4;
                mFragmentCollabBarBinding.rangeSlider.refreshDrawableState();
                break;
            case AppConstants.ClassConstants.COLLAB_BAR_F:
                mFragmentCollabBarBinding.rangeSlider.invalidate();
                mFragmentCollabBarBinding.rangeSlider.setMaskColor(Color.parseColor("#708E5BE8"));
                mFragmentCollabBarBinding.ivAddColor.setImageResource(R.drawable.ic_camera_collab_added_tick);
                selectedColor = 5;
                mFragmentCollabBarBinding.rangeSlider.refreshDrawableState();
                break;
            case AppConstants.ClassConstants.COLLAB_BAR_ALL:
                mFragmentCollabBarBinding.rangeSlider.invalidate();
                mFragmentCollabBarBinding.rangeSlider.setMaskColor(Color.parseColor("#70C9598E"));
                mFragmentCollabBarBinding.ivAddColor.setImageResource(R.drawable.ic_camera_collab_added_tick);
                selectedColor = 6;
                mFragmentCollabBarBinding.rangeSlider.refreshDrawableState();
                break;
        }
    }

    private int dpToPx(float dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    private void setColorBar() {
        mFragmentCollabBarBinding.vBar.removeAllViews();
        int screenWidth = getActivity().getWindowManager().getDefaultDisplay().getWidth();
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mFragmentCollabBarBinding.vBar.getLayoutParams();
        for (int i = 0; i < mColorBarBeans.size(); i++) {
            View view = new View(getActivity());
            float start = (mColorBarBeans.get(i).getStartTime() * screenWidth) / durationInMillies;
            float end = (mColorBarBeans.get(i).getEndTime() * screenWidth) / durationInMillies;
            float width = end - start;
            view.setBackgroundColor(Color.parseColor(mColorBarBeans.get(i).getColorCode()));
            RelativeLayout.LayoutParams viewLayoutParams = null;
            switch (mColorBarBeans.get(i).getCode()) {
                case "0":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((30 / (templateSize + 1)), getActivity()));
                    view.setY((float) dpToPx(30 / (templateSize + 1), getActivity()));
                    break;
                case "1":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((30 / (templateSize + 1)), getActivity()));
                    view.setY((float) dpToPx(2 * (30 / (templateSize + 1)), getActivity()));
                    break;
                case "2":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((30 / (templateSize + 1)), getActivity()));
                    view.setY((float) dpToPx(3 * (30 / (templateSize + 1)), getActivity()));
                    break;
                case "3":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((30 / (templateSize + 1)), getActivity()));
                    view.setY((float) dpToPx(4 * (30 / (templateSize + 1)), getActivity()));
                    break;
                case "4":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((30 / (templateSize + 1)), getActivity()));
                    view.setY((float) dpToPx(5 * (30 / (templateSize + 1)), getActivity()));
                    break;
                case "5":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((30 / (templateSize + 1)), getActivity()));
                    view.setY((float) dpToPx(6 * (30 / (templateSize + 1)), getActivity()));
                    break;
                case "6":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((30 / (templateSize + 1)), getActivity()));
                    view.setY(0);
                    break;
            }
            view.setX(start);
            view.setLayoutParams(viewLayoutParams);
            mFragmentCollabBarBinding.vBar.addView(view);
        }
        mFragmentCollabBarBinding.vBar.setLayoutParams(layoutParams);
    }
}
