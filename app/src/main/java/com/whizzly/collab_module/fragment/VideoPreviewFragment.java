package com.whizzly.collab_module.fragment;


import android.app.Activity;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.chart_module.ChartsCollabTempleteFragment;
import com.whizzly.collab_module.activity.VideoRecordActivity;
import com.whizzly.collab_module.beans.CollabBarBean;
import com.whizzly.collab_module.viewmodel.VideoPreviewViewModel;
import com.whizzly.databinding.FragmentVideoPreviewBinding;
import com.whizzly.home_screen_module.home_screen_fragments.home_feeds.VideoPreviewActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.video_list_response.Result;
import com.whizzly.notification.NotificationActivity;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class VideoPreviewFragment extends Fragment implements OnClickSendListener {

    private static final DefaultBandwidthMeter BANDWIDTH_METER =
            new DefaultBandwidthMeter();
    private FragmentVideoPreviewBinding mFragmentVideoPreviewBinding;
    private VideoPreviewViewModel mVideoPreviewViewModel;
    private Result mResult;
    private Activity mActivity;
    private ExoPlayer mExoPlayer;
    private int fragmentFrom;
    private com.whizzly.models.feeds_response.Result mFeedResponse;

    public VideoPreviewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getArgumentData();
    }

    private void getArgumentData() {
        Bundle previewVideo = getArguments();
        if (previewVideo != null) {
            fragmentFrom = previewVideo.getInt(AppConstants.ClassConstants.FRAGMENT_IS_FROM);
            if (fragmentFrom == AppConstants.ClassConstants.IS_FROM_SUGGESTED) {
                mResult = previewVideo.getParcelable(AppConstants.ClassConstants.VIDEO_PREVIEW_BUNDLE);
            } else{
                mFeedResponse = previewVideo.getParcelable(AppConstants.ClassConstants.FEEDS_DATA);
            }
        }
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFragmentVideoPreviewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_video_preview, container, false);
        mVideoPreviewViewModel = ViewModelProviders.of(this).get(VideoPreviewViewModel.class);
        mFragmentVideoPreviewBinding.setViewModel(mVideoPreviewViewModel);
        mFragmentVideoPreviewBinding.flProgressBar.setVisibility(View.VISIBLE);
        mVideoPreviewViewModel.setOnClickSendListener(this);
        if (getActivity() instanceof BaseActivity)
            mVideoPreviewViewModel.setGenericObservers(((BaseActivity) Objects.requireNonNull(getActivity())).getErrorObserver(), ((BaseActivity) getActivity()).getFailureResponseObserver());
        mActivity = getActivity();
        if (fragmentFrom == AppConstants.ClassConstants.IS_FROM_SUGGESTED) {
            setPlayer();
            setVideoView(mResult.getUrl());
        } else {
            if (AppUtils.isNetworkAvailable(Objects.requireNonNull(getActivity())))
                hitReuseApi();
            else
                Toast.makeText(mActivity, getActivity().getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }

        mFragmentVideoPreviewBinding.flProgressBar.setVisibility(View.VISIBLE);

        mVideoPreviewViewModel.getmReuseVideoBeanRichMediatorLiveData().observe(this, reuseVideoBean -> {
            if (reuseVideoBean!=null){
                if (reuseVideoBean.getCode() == 200) {
                    mResult = reuseVideoBean.getResult();
                    setAdditionalInfoToBar(mResult.getAdditinalInfo());
                    setVideoView(mResult.getUrl());
                    setPlayer();
                }
                else if (reuseVideoBean.getCode()==301 ||reuseVideoBean.getCode()==302){
                    Toast.makeText(mActivity, reuseVideoBean.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) Objects.requireNonNull(mActivity)).autoLogOut();
                }
            }

        });
        return mFragmentVideoPreviewBinding.getRoot();
    }

    private void setAdditionalInfoToBar(String additionalInfoToBar){
        ArrayList<CollabBarBean> collabBarBeans = new Gson().fromJson(additionalInfoToBar, new TypeToken<ArrayList<CollabBarBean>>(){}.getType());
        mFragmentVideoPreviewBinding.vBar.removeAllViews();
        int screenWidth = getActivity().getWindowManager().getDefaultDisplay().getWidth();
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mFragmentVideoPreviewBinding.vBar.getLayoutParams();
        for (int i = 0; i < collabBarBeans.size(); i++) {
            long durationInMillies = collabBarBeans.get(i).getDurationInMillies();
            View view = new View(getActivity());
            float start = (collabBarBeans.get(i).getStartTime() * screenWidth) / durationInMillies;
            float end = (collabBarBeans.get(i).getEndTime() * screenWidth) / durationInMillies;
            float width = end - start;
            view.setBackgroundColor(Color.parseColor(collabBarBeans.get(i).getColorCode()));
            RelativeLayout.LayoutParams viewLayoutParams = null;
            int templateSize = mResult.getTemplateSize();
            switch (collabBarBeans.get(i).getCode()) {
                case "0":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), getActivity()));
                    view.setY((float) dpToPx(20 / (templateSize + 1), getActivity()));
                    break;
                case "1":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), getActivity()));
                    view.setY((float) dpToPx(2 * (20 / (templateSize + 1)), getActivity()));
                    break;
                case "2":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), getActivity()));
                    view.setY((float) dpToPx(3 * (20 / (templateSize + 1)), getActivity()));
                    break;
                case "3":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), getActivity()));
                    view.setY((float) dpToPx(4 * (20 / (templateSize + 1)), getActivity()));
                    break;
                case "4":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), getActivity()));
                    view.setY((float) dpToPx(5 * (20 / (templateSize + 1)), getActivity()));
                    break;
                case "5":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), getActivity()));
                    view.setY((float) dpToPx(6 * (20 / (templateSize + 1)), getActivity()));
                    break;
                case "6":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), getActivity()));
                    view.setY(0);
                    break;
            }
            view.setX(start);
            view.setLayoutParams(viewLayoutParams);
            mFragmentVideoPreviewBinding.vBar.addView(view);
        }
        mFragmentVideoPreviewBinding.vBar.setLayoutParams(layoutParams);
    }

    private int dpToPx(float dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    private void setPlayer() {
        if (mResult.getCollabInfo().size() == 2) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            mFragmentVideoPreviewBinding.vvVideo.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
        } else {
            mFragmentVideoPreviewBinding.vvVideo.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
        }
    }

    private void setVideoView(String url) {
        TrackSelection.Factory adaptiveTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(BANDWIDTH_METER);

        mExoPlayer = ExoPlayerFactory.newSimpleInstance(getActivity(),
                new DefaultRenderersFactory(getActivity()),
                new DefaultTrackSelector(adaptiveTrackSelectionFactory),
                new DefaultLoadControl());

        mExoPlayer.setPlayWhenReady(true);
        mExoPlayer.setRepeatMode(Player.REPEAT_MODE_ONE);
        MediaSource mediaSource = buildMediaSource(Uri.parse(url));
        mExoPlayer.addListener(new Player.DefaultEventListener() {
            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                super.onPlayerStateChanged(playWhenReady, playbackState);
                switch (playbackState) {
                    case ExoPlayer.STATE_BUFFERING:
                        mFragmentVideoPreviewBinding.flProgressBar.setVisibility(View.VISIBLE);
                        break;
                    case ExoPlayer.STATE_READY:
                        if(playWhenReady){
                            mFragmentVideoPreviewBinding.flProgressBar.setVisibility(View.GONE);
                        }else {
                            mFragmentVideoPreviewBinding.flProgressBar.setVisibility(View.VISIBLE);
                        }
                        break;
                }
            }
        });
        mExoPlayer.prepare(mediaSource, true, false);
        mFragmentVideoPreviewBinding.vvVideo.setPlayer(mExoPlayer);
    }

    private MediaSource buildMediaSource(Uri uri) {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(Objects.requireNonNull(getActivity()),
                Util.getUserAgent(getActivity(), getString(R.string.app_name)), bandwidthMeter);
        return new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (mExoPlayer != null)
            mExoPlayer.setPlayWhenReady(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mExoPlayer != null) {
            mExoPlayer.setPlayWhenReady(false);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mExoPlayer != null) {
            mExoPlayer.setPlayWhenReady(false);
        }
    }

    private void releasePlayer() {
        if (mExoPlayer != null) {
            mExoPlayer.release();
            mExoPlayer = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        releasePlayer();
    }

    private void onCancelClicked() {
        mExoPlayer.setPlayWhenReady(false);
        releasePlayer();
        getActivity().finish();
    }

    private void onCollabClicked() {
        Bundle collabData = new Bundle();
        collabData.putParcelable(AppConstants.ClassConstants.COLLAB_DATA, mResult);
        if(fragmentFrom == AppConstants.ClassConstants.FROM_CHARTS || fragmentFrom == AppConstants.ClassConstants.FROM_CATEGORY_VIDEOS){
            ChartsCollabTempleteFragment chartsCollabTempleteFragment = new ChartsCollabTempleteFragment();
            chartsCollabTempleteFragment.setArguments(collabData);
            if (getActivity() instanceof VideoPreviewActivity && AppUtils.isNetworkAvailable(getActivity())) {
                ((VideoPreviewActivity) mActivity).addFragmentFromRight(R.id.fl_container, chartsCollabTempleteFragment);
            }
        }else if(fragmentFrom == AppConstants.ClassConstants.FROM_HOME_COLLAB){
            ChartsCollabTempleteFragment chartsCollabTempleteFragment = new ChartsCollabTempleteFragment();
            chartsCollabTempleteFragment.setArguments(collabData);
            if (getActivity() instanceof NotificationActivity && AppUtils.isNetworkAvailable(getActivity())) {
                ((NotificationActivity) mActivity).addFragmentFromRight(R.id.fl_container, chartsCollabTempleteFragment);
            }
        }else {
            VideoReplaceFragment videoReplaceFragment = new VideoReplaceFragment();
            videoReplaceFragment.setArguments(collabData);
            if (getActivity() instanceof VideoRecordActivity)
                ((VideoRecordActivity) mActivity).addFragmentFromRight(R.id.fl_container, videoReplaceFragment);
            else if (getActivity() instanceof VideoPreviewActivity && AppUtils.isNetworkAvailable(getActivity())) {

                ((VideoPreviewActivity) mActivity).addFragmentFromRight(R.id.fl_container, videoReplaceFragment);
            }
        }
    }

    private void hitReuseApi() {
        mFragmentVideoPreviewBinding.flProgressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> reuseQuery = new HashMap<>();
        reuseQuery.put(AppConstants.NetworkConstants.VIDEO_TYPE, "collab");
        reuseQuery.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        reuseQuery.put(AppConstants.NetworkConstants.VIDEO_ID, String.valueOf(mFeedResponse.getId()));
        mVideoPreviewViewModel.hitReuseApi(reuseQuery);
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_PREVIEW_CLOSE_CLICKED:
                onCancelClicked();
                break;
            case AppConstants.ClassConstants.ON_PREVIEW_COLLAB_CLICKED:
                onCollabClicked();
                break;
        }
    }
}
