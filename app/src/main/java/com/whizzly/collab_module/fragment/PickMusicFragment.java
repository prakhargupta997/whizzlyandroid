package com.whizzly.collab_module.fragment;


import android.app.Activity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;

import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.PickMusicRecyclerViewAdapter;
import com.whizzly.collab_module.activity.VideoRecordActivity;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.collab_module.model.PickMusicFragmentModel;
import com.whizzly.collab_module.model.PickMusicViewModel;
import com.whizzly.databinding.FragmentPickMusicBinding;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.music_list_response.GetMusicResponseBean;
import com.whizzly.models.music_list_response.Result_;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;
import com.whizzly.utils.EndlessRecyclerOnScrollListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class PickMusicFragment extends Fragment implements View.OnClickListener, OnClickAdapterListener {
    private FragmentPickMusicBinding mFragmentPickMusicBinding;
    private PickMusicRecyclerViewAdapter musicRecyclerViewAdapter;
    private ArrayList<Result_> mMusicDetailList;
    private Activity mActivity;
    private Observer<Throwable> errorObserver;
    private Observer<FailureResponse> failureResponseObserver;
    private PickMusicFragmentModel mPickMusicFragmentModel;
    private PickMusicViewModel pickMusicViewModel;
    private int page = 1, offset = 20;
    private int lastPosition = -1;
    private ExoPlayer exoPlayer;
    private Observer<GetMusicResponseBean> observer;

    public PickMusicFragment() {
        // Required empty public constructor
    }

    private void initObservers() {
        errorObserver = new Observer<Throwable>() {
            @Override
            public void onChanged(@Nullable Throwable throwable) {

            }
        };

        failureResponseObserver = new Observer<FailureResponse>() {
            @Override
            public void onChanged(@Nullable FailureResponse failureResponse) {
            }
        };
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFragmentPickMusicBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_pick_music, container, false);
        pickMusicViewModel = ViewModelProviders.of(this).get(PickMusicViewModel.class);
        mFragmentPickMusicBinding.setViewModel(pickMusicViewModel);
        mPickMusicFragmentModel = new PickMusicFragmentModel();
        initObservers();
        pickMusicViewModel.setGenericListeners(errorObserver, failureResponseObserver);
        mFragmentPickMusicBinding.setModel(mPickMusicFragmentModel);
        pickMusicViewModel.setPickMusicFragmentModel(mPickMusicFragmentModel);
        setViews();
        addMusic();
        searchMusic();
        mFragmentPickMusicBinding.ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentPickMusicBinding.etSearchMusic.setText("");
                getData(1, 10);

            }
        });
        return mFragmentPickMusicBinding.getRoot();
    }

    private void searchMusic() {
        mFragmentPickMusicBinding.etSearchMusic.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() > 0) {
                    mFragmentPickMusicBinding.ivCross.setVisibility(View.VISIBLE);
                    if (AppUtils.isNetworkAvailable(mActivity)) {
                        hitSearchMusicApi(editable.toString());
                        mFragmentPickMusicBinding.pbLoading.setVisibility(View.VISIBLE);
                    } else
                        Toast.makeText(mActivity, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                } else {
                    mFragmentPickMusicBinding.flLonely.setVisibility(View.GONE);
                    mFragmentPickMusicBinding.ivCross.setVisibility(View.GONE);
                    getData(1, 10);
                }
            }
        });
    }

    private void hitSearchMusicApi(String music) {
        HashMap<String, Object> getMusicListMap = new HashMap<>();
        getMusicListMap.put(AppConstants.NetworkConstants.PARAM_PICK_MUSIC_3, music);
        getMusicListMap.put(AppConstants.NetworkConstants.USER_ID, DataManager.getInstance().getUserId());
        pickMusicViewModel.hitPickMusicApi(getMusicListMap);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setRecyclerView();
        mMusicDetailList.clear();
        mFragmentPickMusicBinding.rvSongList.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (AppUtils.isNetworkAvailable(mActivity)) {
            getData(1, 20);
        } else
            Toast.makeText(mActivity, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
        pickMusicViewModel.getGetMusicResponseBeanRichMediatorLiveData().observe(this, observer);
    }

    private void getData(int page, int offset) {
        HashMap<String, Object> getMusicListMap = new HashMap<>();
        getMusicListMap.put(AppConstants.NetworkConstants.PARAM_PICK_MUSIC_1, page);
        getMusicListMap.put(AppConstants.NetworkConstants.PARAM_PICK_MUSIC_2, offset);
        getMusicListMap.put(AppConstants.NetworkConstants.USER_ID, DataManager.getInstance().getUserId());
        pickMusicViewModel.hitPickMusicApi(getMusicListMap);
    }

    private void setViews() {
        mActivity = getActivity();
        mMusicDetailList = new ArrayList<>();
        mFragmentPickMusicBinding.toolbar.ivBack.setVisibility(View.VISIBLE);
        mFragmentPickMusicBinding.toolbar.ivBack.setOnClickListener(this);
        mFragmentPickMusicBinding.toolbar.tvToolbarText.setText(R.string.txt_pick_music);
        mFragmentPickMusicBinding.pbLoading.setVisibility(View.VISIBLE);
        if(DataManager.getInstance().isNotchDevice()){
            mFragmentPickMusicBinding.toolbar.view.setVisibility(View.VISIBLE);
        }else{
            mFragmentPickMusicBinding.toolbar.view.setVisibility(View.GONE);
        }
    }

    private void setRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
        mFragmentPickMusicBinding.rvSongList.setLayoutManager(linearLayoutManager);
        mFragmentPickMusicBinding.rvSongList.addItemDecoration(new DividerItemDecoration(mActivity, DividerItemDecoration.VERTICAL));
        musicRecyclerViewAdapter = new PickMusicRecyclerViewAdapter(mMusicDetailList, this::onAdapterClicked, getActivity());
        mFragmentPickMusicBinding.rvSongList.setAdapter(musicRecyclerViewAdapter);
        mFragmentPickMusicBinding.rvSongList.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                if (page != -1) {
                    mPickMusicFragmentModel.setIsShowProgressBar(true);
                    if (AppUtils.isNetworkAvailable(mActivity))
                        getData(page, offset);
                    else
                        Toast.makeText(mActivity, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();

                }
            }
        });
    }


    private void addMusic() {
        observer = new Observer<GetMusicResponseBean>() {
            @Override
            public void onChanged(@Nullable GetMusicResponseBean getMusicResponseBean) {
                if (getMusicResponseBean!=null){
                 if (getMusicResponseBean.getCode()==200){
                     mFragmentPickMusicBinding.pbLoading.setVisibility(View.GONE);
                     if (mMusicDetailList != null && mMusicDetailList.size() > 0)
                         mMusicDetailList.clear();

                     if (getMusicResponseBean.getResult() != null)
                         page = getMusicResponseBean.getResult().getNext();
                     if (getMusicResponseBean.getResult() != null && getMusicResponseBean.getResult().getResult() != null && getMusicResponseBean.getResult().getResult().size() > 0) {
                         mMusicDetailList.addAll(getMusicResponseBean.getResult().getResult());
                     } else {
                         mFragmentPickMusicBinding.flLonely.setVisibility(View.VISIBLE);
                     }
                     mFragmentPickMusicBinding.rvSongList.getAdapter().notifyDataSetChanged();
                 }

                 else if (getMusicResponseBean.getCode()==301 || getMusicResponseBean.getCode()==302){
                     Toast.makeText(mActivity, getMusicResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                     ((BaseActivity)Objects.requireNonNull(getActivity())).autoLogOut();
                 }
                }


            }
        };
        pickMusicViewModel.getGetMusicResponseBeanRichMediatorLiveData().observe(this, observer);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                ((VideoRecordActivity) mActivity).removeFragmentFromLeft(R.id.fl_container);
                Objects.requireNonNull(getActivity()).getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                break;
        }
    }

    private void startMusicPlayer(Uri musicUri) {
        exoPlayer = ExoPlayerFactory.newSimpleInstance(getActivity(),
                new DefaultRenderersFactory(getActivity()),
                new DefaultTrackSelector(),
                new DefaultLoadControl());
        MediaSource mediaSource = buildMediaSource(musicUri);
        exoPlayer.prepare(mediaSource, true, false);
    }

    private MediaSource buildMediaSource(Uri uri) {
        return new ExtractorMediaSource.Factory(
                new DefaultHttpDataSourceFactory("exoplayer-codelab")).
                createMediaSource(uri);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
        releasePlayer();
        mMusicDetailList.clear();
        musicRecyclerViewAdapter.notifyDataSetChanged();

    }

    @Override
    public void onStop() {
        super.onStop();
        releasePlayer();
        mMusicDetailList.clear();
        pickMusicViewModel.getGetMusicResponseBeanRichMediatorLiveData().removeObserver(observer);
        musicRecyclerViewAdapter.notifyDataSetChanged();
        page = 1;
    }

    private void releasePlayer() {
        if (exoPlayer != null) {
            exoPlayer.release();
            exoPlayer = null;
        }
    }

    private int previousLocation = -1;
    @Override
    public void onAdapterClicked(int code, Object obj, int position) {
        switch (code) {
            case AppConstants.ClassConstants.ON_MUSIC_CARD_PLAY_CLICK:
                if (obj instanceof Result_) {
                    if (previousLocation == position) {
                        if (exoPlayer != null) {
                            exoPlayer.setPlayWhenReady(false);
                        }
                        ((Result_) obj).setIsPlayPause(false);
                        previousLocation = -1;
                    } else {
                        ((Result_) obj).setIsPlayPause(true);
                        if (previousLocation != -1) {
                            mMusicDetailList.get(previousLocation).setIsPlayPause(true);
                            if (exoPlayer != null) {
                                exoPlayer.setPlayWhenReady(false);
                                exoPlayer.release();
                                exoPlayer = null;
                            }
                        }
                        String musicFile = ((Result_) obj).getMusicFile();
                        startMusicPlayer(Uri.parse(musicFile));
                        if (exoPlayer != null) {
                            exoPlayer.setPlayWhenReady(true);
                        }
                        previousLocation = position;
                    }
                }
                break;
            case AppConstants.ClassConstants.ON_MUSIC_CARD_COLLAB_CLICK:
                if (getActivity() != null && !getActivity().isFinishing()) {
                    AudioCropperFragment audioCropperFragment = new AudioCropperFragment();
                    Bundle audioCrop = new Bundle();
                    audioCrop.putBoolean(AppConstants.ClassConstants.WAS_GET_CONTENT_INTENT, false);
                    audioCrop.putInt(AppConstants.ClassConstants.MUSIC_ID, ((Result_) obj).getMusicId());
                    audioCrop.putString(AppConstants.ClassConstants.MUSIC_NAME, ((Result_) obj).getMusicName());
                    audioCrop.putString(AppConstants.ClassConstants.MUSIC_COMPOSER, ((Result_) obj).getComposerName());
                    audioCrop.putString(AppConstants.ClassConstants.MUSIC_THUMBNAIL, ((Result_) obj).getThumbnail());
                    audioCrop.putString(AppConstants.ClassConstants.MUSIC_LINK, ((Result_) obj).getMusicFile());
                    audioCropperFragment.setArguments(audioCrop);
                    ((VideoRecordActivity) mActivity).addFragmentFromRight(R.id.fl_container, audioCropperFragment);
                }

                if (exoPlayer != null) {
                    if (((Result_) obj).getIsPlayPause()) {
                        exoPlayer.setPlayWhenReady(false);
                        ((Result_) obj).setIsPlayPause(false);
                    }
                }
                break;
            case AppConstants.ClassConstants.ON_MUSIC_CARD_SUGGEST_CLICK:
                if (getActivity() != null && !getActivity().isFinishing()) {
                    SuggestedVideoFragment suggestedVideoFragment = new SuggestedVideoFragment();
                    Bundle bundle = new Bundle();
                    if (obj instanceof Result_) {
                        bundle.putInt(AppConstants.ClassConstants.MUSIC_ID, ((Result_) obj).getMusicId());
                        bundle.putString(AppConstants.ClassConstants.MUSIC_COLLAB_COUNT, ((Result_) obj).getMusicCollabCount());
                        bundle.putString(AppConstants.ClassConstants.MUSIC_NAME, ((Result_) obj).getMusicName());
                        bundle.putString(AppConstants.ClassConstants.MUSIC_COMPOSER, ((Result_) obj).getComposerName());
                        bundle.putString(AppConstants.ClassConstants.MUSIC_THUMBNAIL, ((Result_) obj).getThumbnail());
                        bundle.putString(AppConstants.ClassConstants.MUSIC_LINK, ((Result_) obj).getMusicFile());
                        bundle.putString(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_PICK_MUSIC);
                    }
                    suggestedVideoFragment.setArguments(bundle);
                    ((VideoRecordActivity) getActivity()).addFragmentFromRight(R.id.fl_container, suggestedVideoFragment);
                }

                if (exoPlayer != null) {
                    if (((Result_) obj).getIsPlayPause()) {
                        exoPlayer.setPlayWhenReady(false);
                        ((Result_) obj).setIsPlayPause(false);
                    }
                }
                break;
            case AppConstants.ClassConstants.ON_MUSIC_CARD_EXPAND:

                if (obj instanceof Result_) {
                    if (lastPosition == position) {
                        ((Result_) obj).setIsCardClicked(false);
                        lastPosition = -1;
                    } else {
                        ((Result_) obj).setIsCardClicked(true);
                        if (lastPosition > -1) {
                            mMusicDetailList.get(lastPosition).setIsCardClicked(false);
                            mMusicDetailList.get(lastPosition).setIsPlayPause(false);
                        }
                        lastPosition = position;
                    }

                    if (exoPlayer != null) {
                        exoPlayer.setPlayWhenReady(false);
                    }
                }

                break;
        }
        musicRecyclerViewAdapter.notifyDataSetChanged();
    }
}
