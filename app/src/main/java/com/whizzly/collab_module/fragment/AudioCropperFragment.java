package com.whizzly.collab_module.fragment;


import android.app.AlertDialog;
import android.app.DownloadManager;
import androidx.lifecycle.ViewModelProviders;
import android.content.ContentValues;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.collab_module.model.AudioCropperFragmentModel;
import com.whizzly.collab_module.viewmodel.AudioCropperFragmentViewModel;
import com.whizzly.collab_module.interfaces.OnSendSongUri;
import com.whizzly.collab_module.activity.VideoRecordActivity;
import com.whizzly.databinding.FragmentAudioCropperBinding;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;
import com.whizzly.utils.FileSaveDialog;
import com.whizzly.utils.MarkerView;
import com.whizzly.utils.SamplePlayer;
import com.whizzly.utils.SongMetadataReader;
import com.whizzly.utils.SoundFile;
import com.whizzly.utils.WaveformView;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLConnection;

/**
 * A simple {@link Fragment} subclass.
 */
public class AudioCropperFragment extends Fragment implements MarkerView.MarkerListener,
        WaveformView.WaveformListener, OnClickSendListener {
    private WaveformView mWaveformView;
    private MarkerView mStartMarker;
    private MarkerView mEndMarker;
    private ImageView mPlayButton;
    private SamplePlayer mPlayer;
    private ProgressBar mProgressBar;
    private long mLoadingLastUpdateTime;
    private boolean mLoadingKeepGoing;
    private boolean mFinishActivity;
    private SoundFile mSoundFile;
    private File mFile;
    private String mFilename;
    private String mArtist;
    private String mTitle;
    private int mNewFileKind;
    private String mInfoContent;
    private String mCaption = "";
    private boolean mWasGetContentIntent;
    private boolean mKeyDown;
    private int mWidth;
    private int mMaxPos;
    private int mStartPos;
    private int mEndPos;
    private boolean mStartVisible;
    private boolean mEndVisible;
    private int mLastDisplayedStartPos;
    private int mLastDisplayedEndPos;
    private int mOffset;
    private int mOffsetGoal;
    private int mFlingVelocity;
    private int mPlayStartMsec;
    private int mPlayEndMsec;
    private Handler mHandler;
    private boolean mIsPlaying;
    private boolean mTouchDragging;
    private float mTouchStart;
    private int mTouchInitialOffset;
    private int mTouchInitialStartPos;
    private int mTouchInitialEndPos;
    private long mWaveformTouchStartMsec;
    private float mDensity;
    private int mMarkerLeftInset;
    private int mMarkerRightInset;
    private int mMarkerTopOffset;
    private int mMarkerBottomOffset;
    private Thread mLoadSoundFileThread;
    private Thread mRecordAudioThread;
    private Thread mSaveSoundFileThread;
    private OnSendSongUri onSendSongUri;

    /**
     * This is a special intent action that means "edit a sound file".
     */

    //
    // Public methods and protected overrides
    //
    private SongMetadataReader mSongMetadataReader;
    private Runnable mTimerRunnable = new Runnable() {
        public void run() {
            // Updating an EditText is slow on Android.  Make sure
            // we only do the update if the text has actually changed.
            if (mStartPos != mLastDisplayedStartPos) {
                mLastDisplayedStartPos = mStartPos;
            }

            if (mEndPos != mLastDisplayedEndPos) {
                mLastDisplayedEndPos = mEndPos;
            }

            mHandler.post(mTimerRunnable);
        }
    };
    private String mMusicThumbnail, mSongFileName, mSongFileComposer;
    private FragmentAudioCropperBinding mFragmentAudioCropperBinding;
    private View.OnClickListener mBackClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ((VideoRecordActivity) getActivity()).addFragmentFromRight(R.id.fl_container, new PickMusicFragment());
        }
    };
    private AudioCropperFragmentViewModel mAudioCropperFragmentViewModel;
    private AudioCropperFragmentModel mAudioCropperFragmentModel;
    private View.OnClickListener mPlayListener = new View.OnClickListener() {
        public void onClick(View sender) {
            onPlay(mStartPos);
        }
    };
    private View.OnClickListener mMarkStartListener = new View.OnClickListener() {
        public void onClick(View sender) {
            if (!mIsPlaying) {
                mStartPos = mWaveformView.millisecsToPixels(
                        mPlayer.getCurrentPosition());
                updateDisplay();
                handlePause();
            }
        }
    };
    private View.OnClickListener mMarkEndListener = new View.OnClickListener() {
        public void onClick(View sender) {
            if (!mIsPlaying) {
                mEndPos = mWaveformView.millisecsToPixels(
                        mPlayer.getCurrentPosition());
                updateDisplay();
                handlePause();
            }
        }
    };

    //
    // WaveformListener
    //
    private DownloadManager mDownloadManager;
    private int mMusicId;
    private boolean isClicked = false;

    private void setOnSendSongUri() {
        this.onSendSongUri = (VideoRecordActivity) getActivity();
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        mPlayer = null;
        mIsPlaying = false;
        mLoadSoundFileThread = null;
        mRecordAudioThread = null;
        mSaveSoundFileThread = null;
        Bundle intent = getArguments();
        mWasGetContentIntent = intent.getBoolean(AppConstants.ClassConstants.WAS_GET_CONTENT_INTENT, false);
        mFilename = intent.getString(AppConstants.ClassConstants.MUSIC_LINK);
        mSongFileName = intent.getString(AppConstants.ClassConstants.MUSIC_NAME);
        mSongFileComposer = intent.getString(AppConstants.ClassConstants.MUSIC_COMPOSER);
        mMusicThumbnail = intent.getString(AppConstants.ClassConstants.MUSIC_THUMBNAIL);
        mMusicId = intent.getInt(AppConstants.ClassConstants.MUSIC_ID, 0);
        mSoundFile = null;
        mKeyDown = false;
        mHandler = new Handler();
        mHandler.post(mTimerRunnable);
        if (AppUtils.isNetworkAvailable(getActivity())) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    downloadFile(mFilename, new File(getExternalStoragePath()));
                }
            }).start();
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
        }
    }

    //
    // MarkerListener
    //

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        loadGui();
        mFragmentAudioCropperBinding.getViewModel().setOnSendClickListener(this);
        mAudioCropperFragmentModel.setIsProgressVisible(true);
        setOnSendSongUri();
        mAudioCropperFragmentModel.setSongArtist(mSongFileComposer);
        mAudioCropperFragmentModel.setSongName(mSongFileName);
        if(DataManager.getInstance().isNotchDevice()){
            mFragmentAudioCropperBinding.toolbar.view.setVisibility(View.VISIBLE);
        }else{
            mFragmentAudioCropperBinding.toolbar.view.setVisibility(View.GONE);
        }
        return mFragmentAudioCropperBinding.getRoot();
    }

    private void closeThread(Thread thread) {
        if (thread != null && thread.isAlive()) {
            try {
                thread.join();
            } catch (InterruptedException e) {
            }
        }
    }

    /**
     * Called when the activity is finally destroyed.
     */
    @Override
    public void onDestroy() {
        mLoadingKeepGoing = false;
        closeThread(mLoadSoundFileThread);
        closeThread(mRecordAudioThread);
        closeThread(mSaveSoundFileThread);
        mLoadSoundFileThread = null;
        mRecordAudioThread = null;
        mSaveSoundFileThread = null;

        if (mPlayer != null) {
            if (mPlayer.isPlaying() || mPlayer.isPaused()) {
                mPlayer.stop();
            }
            mPlayer.release();
            mPlayer = null;
        }

        super.onDestroy();
    }

    /**
     * Every time we get a message that our waveform drew, see if we need to
     * animate and trigger another redraw.
     */
    public void waveformDraw() {
        mWidth = mWaveformView.getMeasuredWidth();
        if (mOffsetGoal != mOffset && !mKeyDown)
            updateDisplay();
        else if (mIsPlaying) {
            updateDisplay();
        } else if (mFlingVelocity != 0) {
            updateDisplay();
        }
    }

    public void waveformTouchStart(float x) {
        mTouchDragging = true;
        mTouchStart = x;
        mTouchInitialOffset = mOffset;
        mFlingVelocity = 0;
        mWaveformTouchStartMsec = getCurrentTime();
    }

    public void waveformTouchMove(float x) {
        mOffset = trap((int) (mTouchInitialOffset + (mTouchStart - x)));
        updateDisplay();
    }

    public void waveformTouchEnd() {
        mTouchDragging = false;
        mOffsetGoal = mOffset;

        long elapsedMsec = getCurrentTime() - mWaveformTouchStartMsec;
        if (elapsedMsec < 300) {
            if (mIsPlaying) {
                int seekMsec = mWaveformView.pixelsToMillisecs(
                        (int) (mTouchStart + mOffset));
                if (seekMsec >= mPlayStartMsec &&
                        seekMsec < mPlayEndMsec) {
                    mPlayer.seekTo(seekMsec);
                } else {
                    handlePause();
                }
            } else {
                onPlay((int) (mTouchStart + mOffset));
            }
        }
    }

    public void waveformFling(float vx) {
        mTouchDragging = false;
        mOffsetGoal = mOffset;
        mFlingVelocity = (int) (-vx);
        updateDisplay();
    }

    public void waveformZoomIn() {
//        mWaveformView.zoomIn();
        mStartPos = mWaveformView.getStart();
        mEndPos = mWaveformView.getEnd();
        mMaxPos = mWaveformView.maxPos();
        mOffset = mWaveformView.getOffset();
        mOffsetGoal = mOffset;
        updateDisplay();
    }

    public void waveformZoomOut() {
//        mWaveformView.zoomOut();
        mStartPos = mWaveformView.getStart();
        mEndPos = mWaveformView.getEnd();
        mMaxPos = mWaveformView.maxPos();
        mOffset = mWaveformView.getOffset();
        mOffsetGoal = mOffset;
        updateDisplay();
    }

    public void markerDraw() {
    }

    public void markerTouchStart(MarkerView marker, float x) {
        mTouchDragging = true;
        mTouchStart = x;
        mTouchInitialStartPos = mStartPos;
        mTouchInitialEndPos = mEndPos;
    }

    public void markerTouchMove(MarkerView marker, float x) {
        float delta = x - mTouchStart;

        if (marker == mStartMarker) {
            mStartPos = trap((int) (mTouchInitialStartPos + delta));
            mEndPos = trap((int) (mTouchInitialEndPos + delta));
        } else {
            mEndPos = trap((int) (mTouchInitialEndPos + delta));
            if (mEndPos < mStartPos)
                mEndPos = mStartPos;
        }

        double startTime = mWaveformView.pixelsToSeconds(mStartPos);
        double endTime = mWaveformView.pixelsToSeconds(mEndPos);
        if(endTime - startTime <= 45){
            updateDisplay();
        }else{
            mEndPos = mStartPos+(mWaveformView.secondsToPixels(45));
        }

    }

    public void markerTouchEnd(MarkerView marker) {
        mTouchDragging = false;
        if (marker == mStartMarker) {
            setOffsetGoalStart();
        } else {
            setOffsetGoalEnd();
        }
    }

    public void markerLeft(MarkerView marker, int velocity) {
        mKeyDown = true;

        if (marker == mStartMarker) {
            int saveStart = mStartPos;
            mStartPos = trap(mStartPos - velocity);
            mEndPos = trap(mEndPos - (saveStart - mStartPos));
            setOffsetGoalStart();
        }

        if (marker == mEndMarker) {
            if (mEndPos == mStartPos) {
                mStartPos = trap(mStartPos - velocity);
                mEndPos = mStartPos;
            } else {
                mEndPos = trap(mEndPos - velocity);
            }

            setOffsetGoalEnd();
        }

        updateDisplay();
    }

    public void markerRight(MarkerView marker, int velocity) {
        mKeyDown = true;

        if (marker == mStartMarker) {
            int saveStart = mStartPos;
            mStartPos += velocity;
            if (mStartPos > mMaxPos)
                mStartPos = mMaxPos;
            mEndPos += (mStartPos - saveStart);
            if (mEndPos > mMaxPos)
                mEndPos = mMaxPos;

            setOffsetGoalStart();
        }

        if (marker == mEndMarker) {
            mEndPos += velocity;
            if (mEndPos > mMaxPos)
                mEndPos = mMaxPos;

            setOffsetGoalEnd();
        }

        updateDisplay();
    }

    private void downloadFile(String url, File outputFile) {
        try {
            URL u = new URL(url);
            URLConnection conn = u.openConnection();
            int contentLength = conn.getContentLength();

            DataInputStream stream = new DataInputStream(u.openStream());

            byte[] buffer = new byte[contentLength];
            stream.readFully(buffer);
            stream.close();

            DataOutputStream fos = new DataOutputStream(new FileOutputStream(outputFile));
            fos.write(buffer);
            fos.flush();
            fos.close();
            loadFromFile(Uri.fromFile(outputFile));
        } catch (FileNotFoundException e) {
            return; // swallow a 404
        } catch (IOException e) {
            return; // swallow a 404
        }
    }

    private String getExternalStoragePath() {
        final File dir = getActivity().getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + System.currentTimeMillis() + ".m4a";
    }

    public void markerEnter(MarkerView marker) {
    }

    public void markerKeyUp() {
        mKeyDown = false;
        updateDisplay();
    }

    public void markerFocus(MarkerView marker) {
        mKeyDown = false;
        if (marker == mStartMarker) {
            setOffsetGoalStartNoUpdate();
        } else {
            setOffsetGoalEndNoUpdate();
        }
        mHandler.post(new Runnable() {
            public void run() {
                updateDisplay();
            }
        });
    }

    private void loadGui() {
        // Inflate our UI from its XML layout description.
        mFragmentAudioCropperBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.fragment_audio_cropper, null, false);
        mAudioCropperFragmentViewModel = ViewModelProviders.of(this).get(AudioCropperFragmentViewModel.class);
        mFragmentAudioCropperBinding.setViewModel(mAudioCropperFragmentViewModel);
        mAudioCropperFragmentModel = new AudioCropperFragmentModel();
        mFragmentAudioCropperBinding.toolbar.ivBack.setVisibility(View.VISIBLE);
        mFragmentAudioCropperBinding.toolbar.tvToolbarText.setText(R.string.txt_crop_audio);
        mFragmentAudioCropperBinding.setModel(mAudioCropperFragmentModel);
        mFragmentAudioCropperBinding.toolbar.ivBack.setOnClickListener(mBackClickListener);
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        mDensity = metrics.density;
        mMarkerLeftInset = (int) (10 * mDensity);
        mMarkerRightInset = (int) (16 * mDensity);
        if (mMusicThumbnail != null)
            mAudioCropperFragmentModel.setImageUrl(Uri.parse(mMusicThumbnail));
        mMarkerTopOffset = (int) (10 * mDensity);
        mMarkerBottomOffset = (int) (10 * mDensity);
        mPlayButton = mFragmentAudioCropperBinding.ivPlayPause;
        mPlayButton.setOnClickListener(mPlayListener);
        enableDisableButtons();
        mWaveformView = mFragmentAudioCropperBinding.wfSound;
        mWaveformView.setListener(this);
        mMaxPos = 0;
        mLastDisplayedStartPos = -1;
        mLastDisplayedEndPos = -1;

        if (mSoundFile != null && !mWaveformView.hasSoundFile()) {
            mWaveformView.setSoundFile(mSoundFile);
            mWaveformView.recomputeHeights(mDensity);
            mMaxPos = mWaveformView.maxPos();
        }

        mStartMarker = mFragmentAudioCropperBinding.startmarker;
        mStartMarker.setListener(this);
        mStartMarker.setAlpha(1f);
        mStartMarker.setFocusable(true);
        mStartMarker.setFocusableInTouchMode(true);
        mStartVisible = true;
        mStartMarker.setOnClickListener(mMarkStartListener);

        mEndMarker = mFragmentAudioCropperBinding.endmarker;
        mEndMarker.setListener(this);
        mEndMarker.setAlpha(1f);
        mEndMarker.setFocusable(true);
        mEndMarker.setFocusableInTouchMode(true);
        mEndVisible = true;
        mEndMarker.setOnClickListener(mMarkEndListener);

        updateDisplay();
    }

    private void loadFromFile(Uri uri) {
        mFile = new File(uri.getPath());

        mSongMetadataReader = new SongMetadataReader(
                getActivity(), mFilename);
        mTitle = mSongMetadataReader.getmTitle();
        mArtist = mSongMetadataReader.getmArtist();

        String titleLabel = mTitle;
        if (mArtist != null && mArtist.length() > 0) {
            titleLabel += " - " + mArtist;
        }

        mLoadingLastUpdateTime = getCurrentTime();
        mLoadingKeepGoing = true;
        mFinishActivity = false;

        final SoundFile.ProgressListener listener =
                new SoundFile.ProgressListener() {
                    public boolean reportProgress(double fractionComplete) {
                        long now = getCurrentTime();
                        if (now - mLoadingLastUpdateTime > 100) {
                            mLoadingLastUpdateTime = now;
                        }
                        return mLoadingKeepGoing;
                    }
                };

        // Load the sound file in a background thread
        mLoadSoundFileThread = new Thread() {
            public void run() {
                try {
                    mSoundFile = SoundFile.create(mFile.getAbsolutePath(), listener);

                    if (mSoundFile == null) {
                        String name = mFile.getName().toLowerCase();
                        String[] components = name.split("\\.");
                        return;
                    }
                    mPlayer = new SamplePlayer(mSoundFile);
                } catch (final Exception e) {
                    e.printStackTrace();
                    return;
                }
                if (mLoadingKeepGoing) {
                    Runnable runnable = new Runnable() {
                        public void run() {
                            finishOpeningSoundFile();
                        }
                    };
                    mHandler.post(runnable);
                } else if (mFinishActivity) {
                    getActivity().finish();
                }
            }
        };
        mLoadSoundFileThread.start();
    }

    private void finishOpeningSoundFile() {
        mWaveformView.setSoundFile(mSoundFile);
        mWaveformView.recomputeHeights(mDensity);
        mAudioCropperFragmentModel.setIsProgressVisible(false);
        mMaxPos = mWaveformView.maxPos();
        mLastDisplayedStartPos = -1;
        mLastDisplayedEndPos = -1;

        mTouchDragging = false;

        mOffset = 0;
        mOffsetGoal = 0;
        mFlingVelocity = 0;
        resetPositions();
        if (mEndPos > mMaxPos)
            mEndPos = mMaxPos;

        updateDisplay();
    }

    private synchronized void updateDisplay() {
        if(getActivity()==null || getActivity().isFinishing()){
            return;
        }
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mIsPlaying) {
                    int now = mPlayer.getCurrentPosition();
                    int frames = mWaveformView.millisecsToPixels(now);
                    mWaveformView.setPlayback(frames);
                    setOffsetGoalNoUpdate(frames - mWidth / 2);
                    if (now >= mPlayEndMsec) {
                        handlePause();
                    }
                }

                if (!mTouchDragging) {
                    int offsetDelta;

                    if (mFlingVelocity != 0) {
                        offsetDelta = mFlingVelocity / 10;
                        if (mFlingVelocity > 90) {
                            mFlingVelocity -= 90;
                        } else if (mFlingVelocity < -90) {
                            mFlingVelocity += 90;
                        } else {
                            mFlingVelocity = 0;
                        }

                        mOffset += offsetDelta;

                        if (mOffset + mWidth / 2 > mMaxPos) {
                            mOffset = mMaxPos - mWidth / 2;
                            mFlingVelocity = 0;
                        }
                        if (mOffset < 0) {
                            mOffset = 0;
                            mFlingVelocity = 0;
                        }
                        mOffsetGoal = mOffset;
                    } else {
                        offsetDelta = mOffsetGoal - mOffset;

                        if (offsetDelta > 10)
                            offsetDelta = offsetDelta / 10;
                        else if (offsetDelta > 0)
                            offsetDelta = 1;
                        else if (offsetDelta < -10)
                            offsetDelta = offsetDelta / 10;
                        else if (offsetDelta < 0)
                            offsetDelta = -1;
                        else
                            offsetDelta = 0;

                        mOffset += offsetDelta;
                    }
                }

                mWaveformView.invalidate();
                mWaveformView.setParameters(mStartPos, mEndPos, mOffset);
                int startX = mStartPos - mOffset - mMarkerLeftInset;
                if (startX + mStartMarker.getWidth() >= 0) {
                    if (!mStartVisible) {
                        // Delay this to avoid flicker
                        mHandler.postDelayed(new Runnable() {
                            public void run() {
                                mStartVisible = true;
                                mStartMarker.setAlpha(1f);
                            }
                        }, 900);
                    }
                } else {
                    if (mStartVisible) {
                        mStartMarker.setAlpha(0f);
                        mStartVisible = false;
                    }
                    startX = 0;
                }

                int endX = mEndPos - mOffset - mEndMarker.getWidth() / 2 - mMarkerRightInset;
                if (endX + mEndMarker.getWidth() >= 0) {
                    if (!mEndVisible) {
                        // Delay this to avoid flicker
                        mHandler.postDelayed(new Runnable() {
                            public void run() {
                                mEndVisible = true;
                                mEndMarker.setAlpha(1f);
                            }
                        }, 500);
                    }
                } else {
                    if (mEndVisible) {
                        mEndMarker.setAlpha(0f);
                        mEndVisible = false;
                    }
                    endX = 0;
                }

                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(
                        startX,
                        mMarkerTopOffset,
                        -mStartMarker.getWidth(),
                        -mStartMarker.getHeight());
                mStartMarker.setLayoutParams(params);

                params = new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(
                        endX,
                        mWaveformView.getMeasuredHeight() - mEndMarker.getHeight() - mMarkerBottomOffset,
                        -mStartMarker.getWidth(),
                        -mStartMarker.getHeight());
                mEndMarker.setLayoutParams(params);
            }
        });
    }

    private void enableDisableButtons() {
        if (mIsPlaying) {
            mAudioCropperFragmentModel.setIsPlayClicked(true);
        } else {
            mAudioCropperFragmentModel.setIsPlayClicked(false);
        }
    }

    private void resetPositions() {
        mStartPos = mWaveformView.secondsToPixels(0.0);
        mEndPos = mWaveformView.secondsToPixels(15.0);
    }

    private int trap(int pos) {
        if (pos < 0)
            return 0;
        if (pos > mMaxPos)
            return mMaxPos;
        return pos;
    }

    private void setOffsetGoalStart() {
        setOffsetGoal(mStartPos - mWidth / 2);
    }

    private void setOffsetGoalStartNoUpdate() {
        setOffsetGoalNoUpdate(mStartPos - mWidth / 2);
    }

    private void setOffsetGoalEnd() {
        setOffsetGoal(mEndPos - mWidth/* / 2*/);
    }

    private void setOffsetGoalEndNoUpdate() {
        setOffsetGoalNoUpdate(mEndPos - mWidth / 2);
    }

    private void setOffsetGoal(int offset) {
        setOffsetGoalNoUpdate(offset);
        updateDisplay();
    }

    private void setOffsetGoalNoUpdate(int offset) {
        if (mTouchDragging) {
            return;
        }

        mOffsetGoal = offset;
        if (mOffsetGoal + mWidth / 2 > mMaxPos)
            mOffsetGoal = mMaxPos - mWidth / 2;
        if (mOffsetGoal < 0)
            mOffsetGoal = 0;
    }

    private String formatTime(int pixels) {
        if (mWaveformView != null && mWaveformView.isInitialized()) {
            return formatDecimal(mWaveformView.pixelsToSeconds(pixels));
        } else {
            return "";
        }
    }

    private String formatDecimal(double x) {
        int xWhole = (int) x;
        int xFrac = (int) (100 * (x - xWhole) + 0.5);

        if (xFrac >= 100) {
            xWhole++; //Round up
            xFrac -= 100; //Now we need the remainder after the round up
            if (xFrac < 10) {
                xFrac *= 10; //we need a fraction that is 2 digits long
            }
        }

        if (xFrac < 10)
            return xWhole + ".0" + xFrac;
        else
            return xWhole + "." + xFrac;
    }

    private synchronized void handlePause() {
        if (mPlayer != null && mPlayer.isPlaying()) {
            mPlayer.pause();
        }
        mWaveformView.setPlayback(-1);
        mIsPlaying = false;
        enableDisableButtons();
    }

    private synchronized void onPlay(int startPosition) {
        if (mIsPlaying) {
            handlePause();
            return;
        }

        if (mPlayer == null) {
            // Not initialized yet
            return;
        }

        try {
            mPlayStartMsec = mWaveformView.pixelsToMillisecs(startPosition);
            if (startPosition < mStartPos) {
                mPlayEndMsec = mWaveformView.pixelsToMillisecs(mStartPos);
            } else if (startPosition > mEndPos) {
                mPlayEndMsec = mWaveformView.pixelsToMillisecs(mMaxPos);
            } else {
                mPlayEndMsec = mWaveformView.pixelsToMillisecs(mEndPos);
            }
            mPlayer.setOnCompletionListener(new SamplePlayer.OnCompletionListener() {
                @Override
                public void onCompletion() {
                    handlePause();
                }
            });

            mIsPlaying = true;
            mPlayer.seekTo(mPlayStartMsec);
            mPlayer.start();

            updateDisplay();
            enableDisableButtons();

        } catch (Exception e) {
            return;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mIsPlaying) {
            handlePause();
            mIsPlaying = false;
        }

    }

    private String makeRingtoneFilename(CharSequence title, String extension) {
        final File dir = getActivity().getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/" + title + extension));
    }

    private void saveCroppedMusic(final CharSequence title) {
        double startTime = mWaveformView.pixelsToSeconds(mStartPos);
        double endTime = mWaveformView.pixelsToSeconds(mEndPos);
        final int startFrame = mWaveformView.secondsToFrames(startTime);
        final int endFrame = mWaveformView.secondsToFrames(endTime);
        final int duration = (int) (endTime - startTime + 0.5);

        // Save the sound file in a background thread
        mSaveSoundFileThread = new Thread() {
            public void run() {
                // Try AAC first.
                String outPath = makeRingtoneFilename(title, ".m4a");
                if (outPath == null) {
                    return;
                }
                File outFile = new File(outPath);
                Boolean fallbackToWAV = false;
                try {
                    // Write the new file
                    mSoundFile.WriteFile(outFile, startFrame, endFrame - startFrame);
                } catch (Exception e) {
                    // log the error and try to create a .wav file instead
                    if (outFile.exists()) {
                        outFile.delete();
                    }
                    StringWriter writer = new StringWriter();
                    fallbackToWAV = true;
                }

                // Try to create a .wav file if creating a .m4a file failed.
                if (fallbackToWAV) {
                    outPath = makeRingtoneFilename(title, ".wav");
                    if (outPath == null) {
                        return;
                    }
                    outFile = new File(outPath);
                    try {
                        // create the .wav file
                        mSoundFile.WriteWAVFile(outFile, startFrame, endFrame - startFrame);
                    } catch (Exception e) {
                        // Creating the .wav file also failed. Stop the progress dialog, show an
                        // error message and exit.
                        if (outFile.exists()) {
                            outFile.delete();
                        }
                        return;
                    }
                }

                // Try to load the new file to make sure it worked
                try {
                    final SoundFile.ProgressListener listener =
                            new SoundFile.ProgressListener() {
                                public boolean reportProgress(double frac) {
                                    return true;  // Keep going
                                }
                            };
                    SoundFile.create(outPath, listener);
                } catch (final Exception e) {
                    e.printStackTrace();
                    return;
                }

                final String finalOutPath = outPath;
                Runnable runnable = new Runnable() {
                    public void run() {
                        afterSavingRingtone(title,
                                finalOutPath,
                                duration);
                    }
                };
                mHandler.post(runnable);
            }
        };
        mSaveSoundFileThread.start();
    }

    private void afterSavingRingtone(CharSequence title,
                                     String outPath,
                                     int duration) {
        File outFile = new File(outPath);
        long fileSize = outFile.length();
        if (fileSize <= 512) {
            outFile.delete();
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.alert_title_failure)
                    .setMessage(R.string.too_small_error)
                    .setPositiveButton(R.string.alert_ok_button, null)
                    .setCancelable(false)
                    .show();
            return;
        }

        // Create the database record, pointing to the existing file path
        String mimeType;
        if (outPath.endsWith(".m4a")) {
            mimeType = "audio/mp4a-latm";
        } else if (outPath.endsWith(".wav")) {
            mimeType = "audio/wav";
        } else {
            // This should never happen.
            mimeType = "audio/mpeg";
        }

        String artist = mSongMetadataReader.getmArtist();

        ContentValues values = new ContentValues();
        values.put(MediaStore.MediaColumns.DATA, outPath);
        values.put(MediaStore.MediaColumns.TITLE, title.toString());
        values.put(MediaStore.MediaColumns.SIZE, fileSize);
        values.put(MediaStore.MediaColumns.MIME_TYPE, mimeType);

        values.put(MediaStore.Audio.Media.ARTIST, artist);
        values.put(MediaStore.Audio.Media.DURATION, duration);

        values.put(MediaStore.Audio.Media.IS_MUSIC,
                mNewFileKind == FileSaveDialog.FILE_KIND_MUSIC);
        onSendSongUri.onSendSong(outPath, mMusicId);
        mAudioCropperFragmentModel.setIsProgressVisible(false);
        ((VideoRecordActivity) getActivity()).removeFragmentFromLeft(R.id.fl_container);
        isClicked = false;
    }

    private void onSave() {
        if (mIsPlaying) {
            handlePause();
        }
        if (!isClicked) {
            saveCroppedMusic("music_" + System.currentTimeMillis());
            isClicked = true;
        }

    }

    private long getCurrentTime() {
        return System.nanoTime() / 1000000;
    }


    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_AUDIO_CROPPER_DONE:
                mAudioCropperFragmentModel.setIsProgressVisible(true);
                onSave();

                break;
            case AppConstants.ClassConstants.ON_AUDIO_CROPPER_RESET:
                if (!mIsPlaying)
                    finishOpeningSoundFile();
                break;
        }
    }
}
