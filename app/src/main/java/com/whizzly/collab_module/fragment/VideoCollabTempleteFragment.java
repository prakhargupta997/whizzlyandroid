package com.whizzly.collab_module.fragment;


import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.whizzly.R;
import com.whizzly.arguments.view.ArgumentRecordActivity;
import com.whizzly.collab_module.activity.VideoRecordActivity;
import com.whizzly.collab_module.model.VideoCollabTempleteModel;
import com.whizzly.collab_module.viewmodel.VideoCollabTempleteViewModel;
import com.whizzly.databinding.FragmentVideoCollabTempleteBinding;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import org.jetbrains.annotations.NotNull;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoCollabTempleteFragment extends Fragment implements OnClickSendListener{

    public VideoCollabTempleteFragment() {

    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentVideoCollabTempleteBinding mFragmentVideoCollabTempleteBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_video_collab_templete, container, false);
        VideoCollabTempleteModel mVideoCollabTempleteModel = new VideoCollabTempleteModel();
        mFragmentVideoCollabTempleteBinding.setModel(mVideoCollabTempleteModel);
        VideoCollabTempleteViewModel mVideoCollabTempleteViewModel = ViewModelProviders.of(this).get(VideoCollabTempleteViewModel.class);
        mFragmentVideoCollabTempleteBinding.setViewModel(mVideoCollabTempleteViewModel);
        mVideoCollabTempleteViewModel.setOnClickSendListener(this);
        mVideoCollabTempleteViewModel.setModel(mVideoCollabTempleteModel);
        mFragmentVideoCollabTempleteBinding.rlMainLayout.setOnClickListener(v -> {
            getActivity().getSupportFragmentManager().popBackStack();

        });
        return mFragmentVideoCollabTempleteBinding.getRoot();
    }

    @Override
    public void onClickSend(int code) {

        switch (code) {
            case AppConstants
                    .ClassConstants.ON_COLLAB_CLICKED:
                getActivity().getSupportFragmentManager().popBackStack();
                Intent intent = new Intent(getActivity(), VideoRecordActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case AppConstants
                    .ClassConstants.ON_ARGUMENT_CLICKED:
                getActivity().getSupportFragmentManager().popBackStack();
                Intent argumentIntent = new Intent(getActivity(), ArgumentRecordActivity.class);
                argumentIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                if (getActivity() != null && !getActivity().isFinishing())
                    getActivity().startActivity(argumentIntent);
                break;
            case AppConstants.ClassConstants.ON_SUGGESTION_CLOSE_CLICKED:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }

}
