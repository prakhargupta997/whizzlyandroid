package com.whizzly.collab_module.fragment;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnProgressListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.activity.SuggestedVideoActivity;
import com.whizzly.collab_module.beans.CollabBarBean;
import com.whizzly.collab_module.viewmodel.VideoReplaceViewModel;
import com.whizzly.databinding.FragmentVideoReplaceBinding;
import com.whizzly.models.AddVideoTempleteModel;
import com.whizzly.models.DownloadVideoModel;
import com.whizzly.models.reuse_video.ReuseVideoBean;
import com.whizzly.models.video_list_response.CollabInfo;
import com.whizzly.models.video_list_response.Result;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;
import com.whizzly.utils.FFMpegCommandListener;
import com.whizzly.utils.FFMpegCommands;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoReplaceFragment extends Fragment implements View.OnClickListener {

    int count = 0;
    private VideoReplaceViewModel mVideoReplaceViewModel;
    private FragmentVideoReplaceBinding mFragmentVideoReplaceBinding;
    private ArrayList<CollabInfo> mCollabInfoArrayList;
    private Result mResult;
    private ArrayList<Object> rvVideoList;
    private Activity mActivity;
    private int mNonReplaceableFrame = -1;
    private int mLastPosition = -1;
    private ArrayList<DownloadVideoModel> videoUris = new ArrayList<>();
    private String mMusicFile = "";
    private Thread processAudioThread = new Thread();
    private int threadExecuted = 0;
    private ArrayList<Thread> threads = new ArrayList<>();
    private ArrayList<CollabBarBean> mCollabBarBeans;
    private com.whizzly.models.feeds_response.Result mFeedResponse;

    public VideoReplaceFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCollabInfoArrayList = new ArrayList<>();
        getArgumentData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFragmentVideoReplaceBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_video_replace, container, false);
        mVideoReplaceViewModel = ViewModelProviders.of(this).get(VideoReplaceViewModel.class);
        mFragmentVideoReplaceBinding.setViewModel(mVideoReplaceViewModel);
        if (getActivity() instanceof BaseActivity)
            mVideoReplaceViewModel.setGenericObservers(((BaseActivity) Objects.requireNonNull(getActivity())).getErrorObserver(), ((BaseActivity) getActivity()).getFailureResponseObserver());
        mVideoReplaceViewModel.getmReuseVideoBeanRichMediatorLiveData().observe(this, new Observer<ReuseVideoBean>() {
            @Override
            public void onChanged(ReuseVideoBean reuseVideoBean) {
                mResult = reuseVideoBean.getResult();
                if (mResult != null && mResult.getCollabInfo() != null) {
                    mCollabInfoArrayList.addAll(mResult.getCollabInfo());
                }
                mCollabBarBeans = new Gson().fromJson(mResult.getAdditinalInfo(), new TypeToken<ArrayList<CollabBarBean>>() {
                }.getType());
                initialize();
                downloadVideos();
                downloadMusicFile(mResult.getMusicUrl(), getDownloadDirectory(), "download_audio_" + System.currentTimeMillis() + ".m4a");
            }
        });
        mFragmentVideoReplaceBinding.toolbar.ivBack.setVisibility(View.VISIBLE);
        mFragmentVideoReplaceBinding.toolbar.tvDone.setVisibility(View.INVISIBLE);
        if (DataManager.getInstance().isNotchDevice()) {
            mFragmentVideoReplaceBinding.toolbar.view.setVisibility(View.VISIBLE);
        } else {
            mFragmentVideoReplaceBinding.toolbar.view.setVisibility(View.GONE);
        }
        mFragmentVideoReplaceBinding.toolbar.tvDone.setOnClickListener(view -> {
            setDataToSend();
            if (checkUnselectedVideoRecordList()) {
                goToSuggestedVideoRecordActivity();
            } else {
                Toast.makeText(mActivity, R.string.txt_error_no_frame_select, Toast.LENGTH_SHORT).show();
            }
        });
        mFragmentVideoReplaceBinding.toolbar.tvToolbarText.setText(R.string.txt_join_collab);
        mFragmentVideoReplaceBinding.toolbar.ivBack.setOnClickListener(view -> mActivity.onBackPressed());
        hitReuseApi();
        return mFragmentVideoReplaceBinding.getRoot();
    }

    private void goToSuggestedVideoRecordActivity() {
        if (videoUris.size() + 1 == mCollabInfoArrayList.size() + count) {
            deleteNonReplacableFrame();
            Intent suggestedVideoIntent = new Intent(mActivity, SuggestedVideoActivity.class);
            suggestedVideoIntent.putExtra(AppConstants.ClassConstants.SUGGESTED_VIDEO_COLLAB_LIST, rvVideoList);
            suggestedVideoIntent.putExtra(AppConstants.ClassConstants.SUGGESTED_VIDEO_DATA, mResult);
            suggestedVideoIntent.putExtra(AppConstants.ClassConstants.DOWNLOADED_VIDEOS, videoUris);
            suggestedVideoIntent.putExtra(AppConstants.ClassConstants.USER_ID, String.valueOf(mResult.getUserId()));
            suggestedVideoIntent.putExtra(AppConstants.ClassConstants.FRAME_NUMBER, mLastPosition);
            suggestedVideoIntent.putExtra(AppConstants.ClassConstants.MUSIC_URI, mMusicFile);
            startActivity(suggestedVideoIntent);
        } else {
            Toast.makeText(mActivity, getResources().getString(R.string.txt_plz_wait_for_videos), Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteNonReplacableFrame() {
        for (int j = 0; j < videoUris.size(); j++) {
            if (mLastPosition == videoUris.get(j).getFrameNumber()) {
                videoUris.remove(j);
            }
        }
    }

    private boolean checkUnselectedVideoRecordList() {
        return mLastPosition != -1;
    }

    private void downloadVideo(String url, String dirPath, String fileName, int frameNumber) {
        PRDownloader.download(url, dirPath, fileName)
                .build()
                .setOnStartOrResumeListener(() -> mFragmentVideoReplaceBinding.flDownloading.setVisibility(View.VISIBLE))
                .setOnPauseListener(() -> {
                })
                .setOnCancelListener(() -> {

                })
                .setOnProgressListener(progress -> mFragmentVideoReplaceBinding.flDownloading.setVisibility(View.VISIBLE))
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        threads.add(new Thread(() -> getAudioFile(dirPath + fileName, getAudioFilePath(), dirPath, fileName, frameNumber)));
                        if (threads.size() == mCollabInfoArrayList.size()) {
                            executeThreadOneAfterAnother();
                        }
                    }

                    @Override
                    public void onError(Error error) {
                        if (isAdded() && isVisible()) {
                            Log.e("onError: ", error.isConnectionError() + " " + error.isServerError());
                            downloadVideo(url, dirPath, fileName, frameNumber);
                        }
                    }
                });
    }

    private void executeThreadOneAfterAnother() {
        if (threads.size() > 0) {
            threads.get(0).run();
        }
    }

    private String getAudioFilePath() {
        final File dir = getActivity().getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "audio_" + System.currentTimeMillis() + ".mp4";
    }

    private void getAudioFile(String inputFile, String outputFile, String dirPath, String fileName, int frameNumber) {
        FFMpegCommands.getInstance(getActivity()).getAudioFile(inputFile, outputFile, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                mFragmentVideoReplaceBinding.flDownloading.setVisibility(View.GONE);
                DownloadVideoModel downloadVideoModel = new DownloadVideoModel();
                downloadVideoModel.setDownlaodUri(dirPath + fileName);
                downloadVideoModel.setFrameNumber(frameNumber);
                downloadVideoModel.setAudioFile(outputFile);
                videoUris.add(downloadVideoModel);
                threadExecuted++;
                if (threadExecuted > 0 && threadExecuted < threads.size()) {
                    threads.get(threadExecuted).run();
                }
            }

            @Override
            public void onProgress(String message) {

            }

            @Override
            public void onFailure(String message) {
                mFragmentVideoReplaceBinding.flDownloading.setVisibility(View.GONE);
                DownloadVideoModel downloadVideoModel = new DownloadVideoModel();
                downloadVideoModel.setDownlaodUri(dirPath + fileName);
                downloadVideoModel.setFrameNumber(frameNumber);
                videoUris.add(downloadVideoModel);
                threadExecuted++;
                if (threadExecuted > 0 && threadExecuted < threads.size()) {
                    threads.get(threadExecuted).run();
                }
            }

            @Override
            public void onStart() {
                mFragmentVideoReplaceBinding.flDownloading.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {

            }
        });
    }


    private void downloadMusicFile(String url, String musicPath, String musicFile) {
        mMusicFile = musicPath + musicFile;
        PRDownloader.download(url, musicPath, musicFile)
                .build()
                .setOnStartOrResumeListener(() -> mFragmentVideoReplaceBinding.flDownloading.setVisibility(View.VISIBLE))
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {
                        mFragmentVideoReplaceBinding.flDownloading.setVisibility(View.VISIBLE);
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {
                    }
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        mFragmentVideoReplaceBinding.flDownloading.setVisibility(View.GONE);
                        count = 1;
                    }

                    @Override
                    public void onError(Error error) {
                        downloadMusicFile(url, musicPath, musicFile);
                    }
                });
    }

    private void downloadVideos() {
        for (int i = 0; i < mCollabInfoArrayList.size(); i++) {
            downloadVideo((mCollabInfoArrayList.get(i)).getVideoUrl(), getDownloadDirectory(), "download_video_" + System.currentTimeMillis() + ".mp4", mCollabInfoArrayList.get(i).getFrameNo());
        }
    }

    private String getDownloadDirectory() {
        final File dir = getActivity().getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"));
    }

    private void initialize() {
        rvVideoList = new ArrayList<>();
        mActivity = getActivity();
        setListOfVideos();
        setViews();
        setClickListeners();
    }

    private void getArgumentData() {
        Bundle previewVideo = getArguments();
        if (previewVideo != null) {
            if(previewVideo.containsKey(AppConstants.ClassConstants.FEEDS_DATA)) {
                mFeedResponse = previewVideo.getParcelable(AppConstants.ClassConstants.FEEDS_DATA);
            }
        }


        Bundle collabData = getArguments();
        if (collabData != null && collabData.containsKey(AppConstants.ClassConstants.COLLAB_DATA)) {
            mResult = collabData.getParcelable(AppConstants.ClassConstants.COLLAB_DATA);
            if (mResult != null && mResult.getCollabInfo() != null) {
                mCollabInfoArrayList.addAll(mResult.getCollabInfo());
            }
        }
    }

    private void hitReuseApi() {
        mFragmentVideoReplaceBinding.flDownloading.setVisibility(View.VISIBLE);
        HashMap<String, String> reuseQuery = new HashMap<>();
        reuseQuery.put(AppConstants.NetworkConstants.VIDEO_TYPE, "collab");
        reuseQuery.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        reuseQuery.put(AppConstants.NetworkConstants.VIDEO_ID, String.valueOf(mFeedResponse.getId()));
        mVideoReplaceViewModel.hitReuseApi(reuseQuery);
    }

    private void setListOfVideos() {
        for (int i = 0; i < mResult.getTemplateSize(); i++) {
            rvVideoList.add(new AddVideoTempleteModel());
        }
        for (int i = 0; i < mCollabInfoArrayList.size(); i++) {
            rvVideoList.set(mCollabInfoArrayList.get(i).getFrameNo() - 1, mCollabInfoArrayList.get(i));
        }
        getNonReplacableFrame();
    }

    private void getNonReplacableFrame() {
        for (int i = 0; i < rvVideoList.size(); i++) {
            if (rvVideoList.get(i) instanceof CollabInfo) {
                if (((CollabInfo) rvVideoList.get(i)).getIsReplaceable() == 1) {
                    mNonReplaceableFrame = i;
                }
            }
        }
    }

    private void setClickListeners() {
        mFragmentVideoReplaceBinding.llAddVideoOne.setOnClickListener(this);
        mFragmentVideoReplaceBinding.llAddVideoTwo.setOnClickListener(this);
        mFragmentVideoReplaceBinding.llAddVideoThree.setOnClickListener(this);
        mFragmentVideoReplaceBinding.llAddVideoFour.setOnClickListener(this);
        mFragmentVideoReplaceBinding.llAddVideoFive.setOnClickListener(this);
        mFragmentVideoReplaceBinding.llAddVideoSix.setOnClickListener(this);

        mFragmentVideoReplaceBinding.rlReplaceOne.setOnClickListener(this);
        mFragmentVideoReplaceBinding.rlReplaceTwo.setOnClickListener(this);
        mFragmentVideoReplaceBinding.rlReplaceThree.setOnClickListener(this);
        mFragmentVideoReplaceBinding.rlReplaceFour.setOnClickListener(this);
        mFragmentVideoReplaceBinding.rlReplaceFive.setOnClickListener(this);
        mFragmentVideoReplaceBinding.rlReplaceSix.setOnClickListener(this);

        mFragmentVideoReplaceBinding.tvReplaceOne.setOnClickListener(this);
        mFragmentVideoReplaceBinding.tvReplaceTwo.setOnClickListener(this);
        mFragmentVideoReplaceBinding.tvReplaceThree.setOnClickListener(this);
        mFragmentVideoReplaceBinding.tvReplaceFour.setOnClickListener(this);
        mFragmentVideoReplaceBinding.tvReplaceFive.setOnClickListener(this);
        mFragmentVideoReplaceBinding.tvReplaceSix.setOnClickListener(this);
    }

    private void setViews() {
        for (int i = 0; i < rvVideoList.size(); i++) {
            if (rvVideoList.get(i) instanceof AddVideoTempleteModel) {
                switch (i) {
                    case 0:
                        mFragmentVideoReplaceBinding.cvOneVideo.setVisibility(View.VISIBLE);
                        mFragmentVideoReplaceBinding.llAddVideoOne.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        mFragmentVideoReplaceBinding.cvTwoVideo.setVisibility(View.VISIBLE);
                        mFragmentVideoReplaceBinding.llAddVideoTwo.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        mFragmentVideoReplaceBinding.cvThreeVideo.setVisibility(View.VISIBLE);
                        mFragmentVideoReplaceBinding.llAddVideoThree.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        mFragmentVideoReplaceBinding.cvFourVideo.setVisibility(View.VISIBLE);
                        mFragmentVideoReplaceBinding.llAddVideoFour.setVisibility(View.VISIBLE);
                        break;
                    case 4:
                        mFragmentVideoReplaceBinding.cvFiveVideo.setVisibility(View.VISIBLE);
                        mFragmentVideoReplaceBinding.llAddVideoFive.setVisibility(View.VISIBLE);
                        break;
                    case 5:
                        mFragmentVideoReplaceBinding.cvSixVideo.setVisibility(View.VISIBLE);
                        mFragmentVideoReplaceBinding.llAddVideoSix.setVisibility(View.VISIBLE);
                        break;
                }
            }
        }

        if (mCollabBarBeans.size() > 0) {
            for (int i = 0; i < rvVideoList.size(); i++) {
                switch (Integer.parseInt(mCollabBarBeans.get(i).getCode())) {
                    case 0:
                        mFragmentVideoReplaceBinding.cvOneVideo.setVisibility(View.VISIBLE);
                        mFragmentVideoReplaceBinding.rlReplaceOne.setVisibility(View.VISIBLE);
                        Glide.with(this).load(((CollabInfo) rvVideoList.get(i)).getThumbnailUrl()).into(mFragmentVideoReplaceBinding.ivVideoOne);
                        mFragmentVideoReplaceBinding.tvDescriptionOne.setText(mCollabBarBeans.get(i).getDescription());
                        mFragmentVideoReplaceBinding.ivDotOne.setColorFilter(Color.parseColor("#009359"));
                        break;
                    case 1:
                        mFragmentVideoReplaceBinding.cvTwoVideo.setVisibility(View.VISIBLE);
                        mFragmentVideoReplaceBinding.rlReplaceTwo.setVisibility(View.VISIBLE);
                        Glide.with(this).load(((CollabInfo) rvVideoList.get(i)).getThumbnailUrl()).into(mFragmentVideoReplaceBinding.ivVideoTwo);
                        mFragmentVideoReplaceBinding.tvDescriptionTwo.setText(mCollabBarBeans.get(i).getDescription());
                        mFragmentVideoReplaceBinding.ivDotTwo.setColorFilter(Color.parseColor("#FFB437"));
                        break;
                    case 2:
                        mFragmentVideoReplaceBinding.cvThreeVideo.setVisibility(View.VISIBLE);
                        mFragmentVideoReplaceBinding.rlReplaceThree.setVisibility(View.VISIBLE);
                        Glide.with(this).load(((CollabInfo) rvVideoList.get(i)).getThumbnailUrl()).into(mFragmentVideoReplaceBinding.ivVideoThree);
                        mFragmentVideoReplaceBinding.tvDescriptionThree.setText(mCollabBarBeans.get(i).getDescription());
                        mFragmentVideoReplaceBinding.ivDotThree.setColorFilter(Color.parseColor("#3F7AE9"));
                        break;
                    case 3:
                        mFragmentVideoReplaceBinding.cvFourVideo.setVisibility(View.VISIBLE);
                        mFragmentVideoReplaceBinding.rlReplaceFour.setVisibility(View.VISIBLE);
                        Glide.with(this).load(((CollabInfo) rvVideoList.get(i)).getThumbnailUrl()).into(mFragmentVideoReplaceBinding.ivVideoFour);
                        mFragmentVideoReplaceBinding.tvDescriptionFour.setText(mCollabBarBeans.get(i).getDescription());
                        mFragmentVideoReplaceBinding.ivDotFour.setColorFilter(Color.parseColor("#FF9640"));
                        break;
                    case 4:
                        mFragmentVideoReplaceBinding.cvFiveVideo.setVisibility(View.VISIBLE);
                        mFragmentVideoReplaceBinding.rlReplaceFive.setVisibility(View.VISIBLE);
                        Glide.with(this).load(((CollabInfo) rvVideoList.get(i)).getThumbnailUrl()).into(mFragmentVideoReplaceBinding.ivVideoFive);
                        mFragmentVideoReplaceBinding.tvDescriptionFive.setText(mCollabBarBeans.get(i).getDescription());
                        mFragmentVideoReplaceBinding.ivDotFive.setColorFilter(Color.parseColor("#F85458"));
                        break;
                    case 5:
                        mFragmentVideoReplaceBinding.cvSixVideo.setVisibility(View.VISIBLE);
                        mFragmentVideoReplaceBinding.rlReplaceSix.setVisibility(View.VISIBLE);
                        Glide.with(this).load(((CollabInfo) rvVideoList.get(i)).getThumbnailUrl()).into(mFragmentVideoReplaceBinding.ivVideoSix);
                        mFragmentVideoReplaceBinding.tvDescriptionSix.setText(mCollabBarBeans.get(i).getDescription());
                        mFragmentVideoReplaceBinding.ivDotSix.setColorFilter(Color.parseColor("#8E5BE8"));
                        break;
                }
            }
        } else {
            for (int i = 0; i < rvVideoList.size(); i++) {
                switch (i) {
                    case 0:
                        mFragmentVideoReplaceBinding.cvOneVideo.setVisibility(View.VISIBLE);
                        mFragmentVideoReplaceBinding.rlReplaceOne.setVisibility(View.VISIBLE);
                        Glide.with(this).load(((CollabInfo) rvVideoList.get(i)).getThumbnailUrl()).into(mFragmentVideoReplaceBinding.ivVideoOne);
                        mFragmentVideoReplaceBinding.ivDotOne.setVisibility(View.GONE);
                        break;
                    case 1:
                        mFragmentVideoReplaceBinding.cvTwoVideo.setVisibility(View.VISIBLE);
                        mFragmentVideoReplaceBinding.rlReplaceTwo.setVisibility(View.VISIBLE);
                        Glide.with(this).load(((CollabInfo) rvVideoList.get(i)).getThumbnailUrl()).into(mFragmentVideoReplaceBinding.ivVideoTwo);
                        mFragmentVideoReplaceBinding.ivDotTwo.setVisibility(View.GONE);
                        break;
                    case 2:
                        mFragmentVideoReplaceBinding.cvThreeVideo.setVisibility(View.VISIBLE);
                        mFragmentVideoReplaceBinding.rlReplaceThree.setVisibility(View.VISIBLE);
                        Glide.with(this).load(((CollabInfo) rvVideoList.get(i)).getThumbnailUrl()).into(mFragmentVideoReplaceBinding.ivVideoThree);
                        mFragmentVideoReplaceBinding.ivDotThree.setVisibility(View.GONE);
                        break;
                    case 3:
                        mFragmentVideoReplaceBinding.cvFourVideo.setVisibility(View.VISIBLE);
                        mFragmentVideoReplaceBinding.rlReplaceFour.setVisibility(View.VISIBLE);
                        Glide.with(this).load(((CollabInfo) rvVideoList.get(i)).getThumbnailUrl()).into(mFragmentVideoReplaceBinding.ivVideoFour);
                        mFragmentVideoReplaceBinding.ivDotFour.setVisibility(View.GONE);
                        break;
                    case 4:
                        mFragmentVideoReplaceBinding.cvFiveVideo.setVisibility(View.VISIBLE);
                        mFragmentVideoReplaceBinding.rlReplaceFive.setVisibility(View.VISIBLE);
                        Glide.with(this).load(((CollabInfo) rvVideoList.get(i)).getThumbnailUrl()).into(mFragmentVideoReplaceBinding.ivVideoFive);
                        mFragmentVideoReplaceBinding.ivDotFive.setVisibility(View.GONE);
                        break;
                    case 5:
                        mFragmentVideoReplaceBinding.cvSixVideo.setVisibility(View.VISIBLE);
                        mFragmentVideoReplaceBinding.rlReplaceSix.setVisibility(View.VISIBLE);
                        Glide.with(this).load(((CollabInfo) rvVideoList.get(i)).getThumbnailUrl()).into(mFragmentVideoReplaceBinding.ivVideoSix);
                        mFragmentVideoReplaceBinding.ivDotSix.setVisibility(View.GONE);
                        break;
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_add_video_one:
                if (mNonReplaceableFrame != 0)
                    setVisibilityReplace(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, 1);
                mLastPosition = 1;
                break;
            case R.id.ll_add_video_two:
                if (mNonReplaceableFrame != 1)
                    setVisibilityReplace(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, 2);
                mLastPosition = 2;
                break;
            case R.id.ll_add_video_three:
                if (mNonReplaceableFrame != 2)
                    setVisibilityReplace(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, 3);
                mLastPosition = 3;
                break;
            case R.id.ll_add_video_four:
                if (mNonReplaceableFrame != 3)
                    setVisibilityReplace(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, 4);
                mLastPosition = 4;
                break;
            case R.id.ll_add_video_five:
                if (mNonReplaceableFrame != 4)
                    setVisibilityReplace(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, 5);
                mLastPosition = 5;
                break;
            case R.id.ll_add_video_six:
                if (mNonReplaceableFrame != 5)
                    setVisibilityReplace(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, 6);
                mLastPosition = 6;
                break;
            case R.id.rl_replace_one:
                if (mNonReplaceableFrame != 0) {
                    setVisibilityReplace(View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, 0);
                    mLastPosition = 1;
                } else {
                    setVisibilityReplace(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, 0);
                    Toast.makeText(mActivity, R.string.txt_error_cant_replace_frame, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.rl_replace_two:
                if (mNonReplaceableFrame != 1) {
                    setVisibilityReplace(View.GONE, View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE, 0);
                    mLastPosition = 2;
                } else {
                    setVisibilityReplace(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, 0);
                    Toast.makeText(mActivity, R.string.txt_error_cant_replace_frame, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.rl_replace_three:
                if (mNonReplaceableFrame != 2) {
                    setVisibilityReplace(View.GONE, View.GONE, View.VISIBLE, View.GONE, View.GONE, View.GONE, 0);
                    mLastPosition = 3;
                } else {
                    setVisibilityReplace(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, 0);
                    Toast.makeText(mActivity, R.string.txt_error_cant_replace_frame, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.rl_replace_four:
                if (mNonReplaceableFrame != 3) {
                    setVisibilityReplace(View.GONE, View.GONE, View.GONE, View.VISIBLE, View.GONE, View.GONE, 0);
                    mLastPosition = 4;
                } else {
                    setVisibilityReplace(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, 0);
                    Toast.makeText(mActivity, R.string.txt_error_cant_replace_frame, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.rl_replace_five:
                if (mNonReplaceableFrame != 4) {
                    setVisibilityReplace(View.GONE, View.GONE, View.GONE, View.GONE, View.VISIBLE, View.GONE, 0);
                    mLastPosition = 5;
                } else {
                    setVisibilityReplace(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, 0);
                    Toast.makeText(mActivity, R.string.txt_error_cant_replace_frame, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.rl_replace_six:
                if (mNonReplaceableFrame != 5) {
                    setVisibilityReplace(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.VISIBLE, 0);
                    mLastPosition = 6;
                } else {
                    setVisibilityReplace(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, 0);
                    Toast.makeText(mActivity, R.string.txt_error_cant_replace_frame, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tv_replace_one:
                setDataToSend();
                if (checkUnselectedVideoRecordList()) {
                    goToSuggestedVideoRecordActivity();
                } else {
                    Toast.makeText(mActivity, R.string.txt_error_no_frame_select, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tv_replace_two:
                setDataToSend();
                if (checkUnselectedVideoRecordList()) {
                    goToSuggestedVideoRecordActivity();
                } else {
                    Toast.makeText(mActivity, R.string.txt_error_no_frame_select, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tv_replace_three:
                setDataToSend();
                if (checkUnselectedVideoRecordList()) {
                    goToSuggestedVideoRecordActivity();
                } else {
                    Toast.makeText(mActivity, R.string.txt_error_no_frame_select, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tv_replace_four:
                setDataToSend();
                if (checkUnselectedVideoRecordList()) {
                    goToSuggestedVideoRecordActivity();
                } else {
                    Toast.makeText(mActivity, R.string.txt_error_no_frame_select, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tv_replace_five:
                setDataToSend();
                if (checkUnselectedVideoRecordList()) {
                    goToSuggestedVideoRecordActivity();
                } else {
                    Toast.makeText(mActivity, R.string.txt_error_no_frame_select, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tv_replace_six:
                setDataToSend();
                if (checkUnselectedVideoRecordList()) {
                    goToSuggestedVideoRecordActivity();
                } else {
                    Toast.makeText(mActivity, R.string.txt_error_no_frame_select, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    private void setVisibilityReplace(int one, int two, int three, int four, int five, int six, int onClick) {
        mFragmentVideoReplaceBinding.viewOverlapOne.setVisibility(one);
        mFragmentVideoReplaceBinding.tvReplaceOne.setVisibility(one);
        mFragmentVideoReplaceBinding.viewOverlapTwo.setVisibility(two);
        mFragmentVideoReplaceBinding.tvReplaceTwo.setVisibility(two);
        mFragmentVideoReplaceBinding.viewOverlapThree.setVisibility(three);
        mFragmentVideoReplaceBinding.tvReplaceThree.setVisibility(three);
        mFragmentVideoReplaceBinding.viewOverlapFour.setVisibility(four);
        mFragmentVideoReplaceBinding.tvReplaceFour.setVisibility(four);
        mFragmentVideoReplaceBinding.viewOverlapFive.setVisibility(five);
        mFragmentVideoReplaceBinding.tvReplaceFive.setVisibility(five);
        mFragmentVideoReplaceBinding.viewOverlapSix.setVisibility(six);
        mFragmentVideoReplaceBinding.tvReplaceSix.setVisibility(six);

        switch (onClick) {
            case 1:
                mFragmentVideoReplaceBinding.ivAddVideoOne.setImageResource(R.drawable.drawable_tick_green);
                mFragmentVideoReplaceBinding.tvAddVideoOne.setVisibility(View.GONE);
                mFragmentVideoReplaceBinding.ivAddVideoTwo.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoTwo.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoThree.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoThree.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoFour.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoFour.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoFive.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoFive.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoSix.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoSix.setVisibility(View.VISIBLE);
                break;
            case 2:
                mFragmentVideoReplaceBinding.ivAddVideoOne.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoOne.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoTwo.setImageResource(R.drawable.drawable_tick_green);
                mFragmentVideoReplaceBinding.tvAddVideoTwo.setVisibility(View.GONE);
                mFragmentVideoReplaceBinding.ivAddVideoThree.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoThree.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoFour.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoFour.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoFive.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoFive.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoSix.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoSix.setVisibility(View.VISIBLE);
                break;
            case 3:
                mFragmentVideoReplaceBinding.ivAddVideoOne.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoOne.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoTwo.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoTwo.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoThree.setImageResource(R.drawable.drawable_tick_green);
                mFragmentVideoReplaceBinding.tvAddVideoThree.setVisibility(View.GONE);
                mFragmentVideoReplaceBinding.ivAddVideoFour.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoFour.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoFive.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoFive.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoSix.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoSix.setVisibility(View.VISIBLE);
                break;
            case 4:
                mFragmentVideoReplaceBinding.ivAddVideoOne.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoOne.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoTwo.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoTwo.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoThree.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoThree.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoFour.setImageResource(R.drawable.drawable_tick_green);
                mFragmentVideoReplaceBinding.tvAddVideoFour.setVisibility(View.GONE);
                mFragmentVideoReplaceBinding.ivAddVideoFive.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoFive.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoSix.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoSix.setVisibility(View.VISIBLE);
                break;
            case 5:
                mFragmentVideoReplaceBinding.ivAddVideoOne.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoOne.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoTwo.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoTwo.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoThree.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoThree.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoFour.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoFour.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoFive.setImageResource(R.drawable.drawable_tick_green);
                mFragmentVideoReplaceBinding.tvAddVideoFive.setVisibility(View.GONE);
                mFragmentVideoReplaceBinding.ivAddVideoSix.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoSix.setVisibility(View.VISIBLE);
                break;
            case 6:
                mFragmentVideoReplaceBinding.ivAddVideoOne.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoOne.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoTwo.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoTwo.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoThree.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoThree.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoFour.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoFour.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoFive.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoFive.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoSix.setImageResource(R.drawable.drawable_tick_green);
                mFragmentVideoReplaceBinding.tvAddVideoSix.setVisibility(View.GONE);
                break;
            case 0:
                mFragmentVideoReplaceBinding.ivAddVideoOne.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoOne.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoTwo.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoTwo.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoThree.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoThree.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoFour.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoFour.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoFive.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoFive.setVisibility(View.VISIBLE);
                mFragmentVideoReplaceBinding.ivAddVideoSix.setImageResource(R.drawable.ic_picked_video_added_add_video);
                mFragmentVideoReplaceBinding.tvAddVideoSix.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void setDataToSend() {
        if (mLastPosition != -1 && rvVideoList.size() > mLastPosition) {
            if (rvVideoList.get(mLastPosition) instanceof CollabInfo) {
                CollabInfo collabInfo = (CollabInfo) rvVideoList.get(mLastPosition);
                collabInfo.setId(0);
                rvVideoList.set(mLastPosition, collabInfo);
            } else if (rvVideoList.get(mLastPosition) instanceof AddVideoTempleteModel) {
                AddVideoTempleteModel addVideoTempleteModel = (AddVideoTempleteModel) rvVideoList.get(mLastPosition);
                addVideoTempleteModel.setSelected(true);
                rvVideoList.set(mLastPosition, addVideoTempleteModel);
            }
        }
    }
}
