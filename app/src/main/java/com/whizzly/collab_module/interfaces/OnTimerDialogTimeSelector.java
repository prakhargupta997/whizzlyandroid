package com.whizzly.collab_module.interfaces;

public interface OnTimerDialogTimeSelector {
    void onTimeSelected(int time);
}
