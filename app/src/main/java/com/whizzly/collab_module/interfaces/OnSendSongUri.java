package com.whizzly.collab_module.interfaces;

public interface OnSendSongUri {
    void onSendSong(String uri, int musicId);
}
