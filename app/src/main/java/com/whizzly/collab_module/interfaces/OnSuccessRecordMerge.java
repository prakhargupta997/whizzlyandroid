package com.whizzly.collab_module.interfaces;

public interface OnSuccessRecordMerge {
    public void onMergeSuccess();
}
