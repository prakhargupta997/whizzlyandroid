package com.whizzly.collab_module.interfaces;

public interface OnClickAdapterListener {
    void onAdapterClicked(int code, Object obj, int position);
}
