package com.whizzly.collab_module.interfaces;

import com.whizzly.collab_module.beans.CollabBarBean;

import java.util.ArrayList;
import java.util.HashMap;

public interface OnSendTimeRange {
    public void onSendTimeRange(ArrayList<CollabBarBean> collabBarBeanArrayList);
}
