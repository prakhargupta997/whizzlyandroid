package com.whizzly.collab_module.interfaces;

import com.whizzly.models.music_list_response.Result_;

public interface OnMusicClicked {
    void musicClicked(int code, Result_ result_);
}
