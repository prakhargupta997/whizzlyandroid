package com.whizzly.collab_module.interfaces;

public interface OnFinalMergeVideo {
    public void onFinalMergeVideo();
}
