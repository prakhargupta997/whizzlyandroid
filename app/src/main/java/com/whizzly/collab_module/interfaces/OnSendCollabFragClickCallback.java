package com.whizzly.collab_module.interfaces;

import com.whizzly.collab_module.beans.CollabBarBean;

import java.util.ArrayList;

public interface OnSendCollabFragClickCallback {
    void onSendTimeList(CollabBarBean timeList);
}
