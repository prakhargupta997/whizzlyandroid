package com.whizzly.collab_module;

import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.collab_module.beans.VideoFramesBean;
import com.whizzly.databinding.ItemVideoFrameRowBinding;

import java.util.ArrayList;

public class VideoFrameRecyclerViewAdapter extends RecyclerView.Adapter<VideoFrameRecyclerViewAdapter.ViewHolder> {

    private ArrayList<VideoFramesBean> mVideoFrameList;
    private int frameWidth;

    public VideoFrameRecyclerViewAdapter(ArrayList<VideoFramesBean> mVideoFrameList, int frameWidth) {
        this.mVideoFrameList = mVideoFrameList;
        this.frameWidth = frameWidth;
        setHasStableIds(true);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemVideoFrameRowBinding itemVideoFrameRowBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_video_frame_row, viewGroup, false);
        return new ViewHolder(itemVideoFrameRowBinding);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.itemRowBinding.ivFrame.setMinimumHeight(frameWidth);
        viewHolder.itemRowBinding.ivFrame.setMinimumWidth(frameWidth);
        VideoFramesBean videoFramesBean = mVideoFrameList.get(i);
        Glide.with(viewHolder.itemRowBinding.ivFrame.getContext()).load(videoFramesBean.getFrame()).into(viewHolder.itemRowBinding.ivFrame);
        viewHolder.bind(videoFramesBean);
        if (mVideoFrameList.get(i).getColorCode() != 0)
            viewHolder.itemRowBinding.vOverlap.setBackgroundColor(mVideoFrameList.get(i).getColorCode());
        else{
            viewHolder.itemRowBinding.vOverlap.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    @Override
    public int getItemCount() {
        return mVideoFrameList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ItemVideoFrameRowBinding itemRowBinding;

        ViewHolder(ItemVideoFrameRowBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }

        public void bind(VideoFramesBean obj) {
            itemRowBinding.setVariable(BR.model, obj);
            itemRowBinding.executePendingBindings();
        }
    }
}
