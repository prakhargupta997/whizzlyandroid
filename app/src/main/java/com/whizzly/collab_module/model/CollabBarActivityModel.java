package com.whizzly.collab_module.model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;

import com.whizzly.BR;

public class CollabBarActivityModel extends BaseObservable {
    public ObservableBoolean isDonePressed = new ObservableBoolean(false);

    @Bindable
    public boolean getIsDonePressed() {
        return isDonePressed.get();
    }

    public void setIsDonePressed(boolean isDonePressed) {
        this.isDonePressed.set(isDonePressed);
        notifyPropertyChanged(BR.isDonePressed);
    }

}
