package com.whizzly.collab_module.model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;

import com.whizzly.BR;

public class VideoCollabTempleteModel extends BaseObservable {
    private ObservableBoolean isCollabClicked = new ObservableBoolean(false);

    @Bindable
    public boolean getIsCollabClicked() {
        return isCollabClicked.get();
    }

    public void setIsCollabClicked(boolean isCollabClicked) {
        this.isCollabClicked.set(isCollabClicked);
        notifyPropertyChanged(BR.isCollabClicked);
    }

}
