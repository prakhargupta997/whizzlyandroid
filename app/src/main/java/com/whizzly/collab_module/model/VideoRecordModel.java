package com.whizzly.collab_module.model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.whizzly.BR;

public class VideoRecordModel extends BaseObservable {
    public ObservableBoolean isRecordPressed = new ObservableBoolean(false);
    public ObservableBoolean isFlashClicked = new ObservableBoolean(false);
    public ObservableBoolean isRemoveVisible = new ObservableBoolean(false);
    public ObservableBoolean isVideoStarted = new ObservableBoolean(false);
    public ObservableField<String> time = new ObservableField<>("");
    public ObservableBoolean isStartTimer = new ObservableBoolean(false);

    @Bindable
    public boolean getIsVideoStarted() {
        return isVideoStarted.get();
    }

    public void setIsVideoStarted(boolean isVideoStarted) {
        this.isVideoStarted.set(isVideoStarted);
        notifyPropertyChanged(BR.isVideoStarted);
    }

    @Bindable
    public String getTime() {
        return time.get();
    }

    public void setTime(String time) {
        this.time.set(time);
        notifyPropertyChanged(BR.time);
    }

    @Bindable
    public boolean getIsStartTimer() {
        return isStartTimer.get();
    }

    public void setIsStartTimer(boolean isStartTimer) {
        this.isStartTimer.set(isStartTimer);
        notifyPropertyChanged(BR.isStartTimer);
    }

    @Bindable
    public boolean getIsRemoveVisible() {
        return isRemoveVisible.get();
    }

    public void setIsRemoveVisible(boolean isRemoveVisible) {
        this.isRemoveVisible.set(isRemoveVisible);
        notifyPropertyChanged(BR.isRemoveVisible);
    }

    @Bindable
    public boolean getIsRecordPressed() {
        return isRecordPressed.get();
    }

    public void setIsRecordPressed(boolean isRecordPressed) {
        this.isRecordPressed.set(isRecordPressed);
        notifyPropertyChanged(BR.isRecordPressed);
    }

    @Bindable
    public boolean getIsFlashClicked() {
        return isFlashClicked.get();
    }

    public void setIsFlashClicked(boolean isFlashClicked) {
        this.isFlashClicked.set(isFlashClicked);
        notifyPropertyChanged(BR.isFlashClicked);
    }


}
