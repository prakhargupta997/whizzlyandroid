package com.whizzly.collab_module.model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import android.net.Uri;

import com.whizzly.BR;

public class AudioCropperFragmentModel extends BaseObservable {
    public String songName;

    @Bindable
    public Uri getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(Uri imageUrl) {
        this.imageUrl = imageUrl;
        notifyPropertyChanged(BR.imageUrl);
    }

    private Uri imageUrl;
    public String songArtist;
    public ObservableBoolean isPlayClicked = new ObservableBoolean(false);

    @Bindable
    public boolean getIsProgressVisible() {
        return isProgressVisible.get();
    }

    public void setIsProgressVisible(boolean isProgressVisible) {
        this.isProgressVisible.set(isProgressVisible);
        notifyPropertyChanged(BR.isProgressVisible);
    }

    public ObservableBoolean isProgressVisible = new ObservableBoolean(false);
    @Bindable
    public boolean getIsPlayClicked() {
        return isPlayClicked.get();
    }

    public void setIsPlayClicked(boolean isPlayClicked) {
        this.isPlayClicked.set(isPlayClicked);
        notifyPropertyChanged(BR.isPlayClicked);
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getSongArtist() {
        return songArtist;
    }

    public void setSongArtist(String songArtist) {
        this.songArtist = songArtist;
    }
}
