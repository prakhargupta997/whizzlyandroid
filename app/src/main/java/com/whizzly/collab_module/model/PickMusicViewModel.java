package com.whizzly.collab_module.model;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.music_list_response.GetMusicResponseBean;

import java.util.HashMap;

public class PickMusicViewModel extends ViewModel {

    private PickMusicFragmentModel mPickMusicFragmentModel;
    private RichMediatorLiveData<GetMusicResponseBean> getMusicResponseBeanRichMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;

    public void setPickMusicFragmentModel(PickMusicFragmentModel mPickMusicFragmentModel) {
        this.mPickMusicFragmentModel = mPickMusicFragmentModel;
    }

    public RichMediatorLiveData<GetMusicResponseBean> getGetMusicResponseBeanRichMediatorLiveData() {
        return getMusicResponseBeanRichMediatorLiveData;
    }

    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (getMusicResponseBeanRichMediatorLiveData == null) {
            getMusicResponseBeanRichMediatorLiveData = new RichMediatorLiveData<GetMusicResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
    }



    public void hitPickMusicApi(HashMap<String, Object> musicParam) {
        mPickMusicFragmentModel.hitGetMusicListApi(getMusicResponseBeanRichMediatorLiveData, musicParam);
    }
}
