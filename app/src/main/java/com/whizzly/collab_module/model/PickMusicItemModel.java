package com.whizzly.collab_module.model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.whizzly.BR;

public class PickMusicItemModel extends BaseObservable {
    private ObservableField<String> time = new ObservableField<>("");
    private ObservableBoolean isCardClicked = new ObservableBoolean(false);
    private ObservableBoolean isPlayPause = new ObservableBoolean(true);


    @Bindable
    public boolean getIsCardClicked() {
        return isCardClicked.get();
    }

    public void setIsCardClicked(boolean isCardClicked) {
        this.isCardClicked.set(isCardClicked);
        notifyPropertyChanged(BR.isCardClicked);
    }

    @Bindable
    public boolean getIsPlayPause() {
        return isPlayPause.get();
    }

    public void setIsPlayPause(boolean isPlayPause) {
        this.isPlayPause.set(isPlayPause);
        notifyPropertyChanged(BR.isPlayPause);
    }

    @Bindable
    public String getTime() {
        return time.get();
    }

    public void setTime(String time) {
        this.time.set(time);
        notifyPropertyChanged(BR.time);
    }
}
