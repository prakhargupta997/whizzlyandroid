package com.whizzly.collab_module.model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;

import com.whizzly.BR;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.music_list_response.GetMusicResponseBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class PickMusicFragmentModel extends BaseObservable {
    public ObservableBoolean isFocused = new ObservableBoolean(false);
    public ObservableBoolean isShowProgressBar = new ObservableBoolean(false);

    @Bindable
    public boolean getIsShowProgressBar() {
        return isShowProgressBar.get();
    }

    public void setIsShowProgressBar(boolean isShowProgressBar) {
        this.isShowProgressBar.set(isShowProgressBar);
        notifyPropertyChanged(BR.isShowProgressBar);
    }

    @Bindable
    public boolean getIsFocused() {
        return isFocused.get();
    }

    public void setIsFocused(boolean isFocused) {
        this.isFocused.set(isFocused);
        notifyPropertyChanged(BR.isFocused);
    }


    public void hitGetMusicListApi(RichMediatorLiveData<GetMusicResponseBean> getMusicResponseBeanRichMediatorLiveData, HashMap<String, Object> musicParams) {

        DataManager.getInstance().hitGetMusicList(musicParams).enqueue(new NetworkCallback<GetMusicResponseBean>() {
            @Override
            public void onSuccess(GetMusicResponseBean getMusicResponseBean) {
                getMusicResponseBeanRichMediatorLiveData.setValue(getMusicResponseBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                getMusicResponseBeanRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                getMusicResponseBeanRichMediatorLiveData.setError(t);
            }
        });
    }
}
