package com.whizzly.collab_module.model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableField;

import com.whizzly.BR;

public class CollabBarModel extends BaseObservable {
    private ObservableField<String> videoDuration = new ObservableField<>();
    private ObservableField<Integer> selected = new ObservableField<>();

    @Bindable
    public Integer getSelected() {
        return selected.get();
    }

    public void setSelected(Integer selected) {
        this.selected.set(selected);
        notifyPropertyChanged(BR.selected);
    }

    @Bindable
    public String getVideoDuration() {
        return videoDuration.get();
    }

    public void setmVideoDuration(String mVideoDuration) {
        this.videoDuration.set(mVideoDuration);
        notifyPropertyChanged(BR.videoDuration);
    }
}
