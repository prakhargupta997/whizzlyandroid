package com.whizzly.collab_module.model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import android.net.Uri;

import com.whizzly.BR;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.video_list_response.VideoListResponseBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.DataManager;

public class SuggestedVideoFragmentModel extends BaseObservable {
    private ObservableField<Uri> songThumbnailUrl = new ObservableField<>();
    private ObservableField<String> songName = new ObservableField<>();
    private ObservableField<String> songComposer = new ObservableField<>();
    private ObservableField<String> reuseCount = new ObservableField<>();


    @Bindable
    public boolean getIsEmpty() {
        return isEmpty.get();
    }

    public void setIsEmpty(boolean isEmpty) {
        this.isEmpty.set(isEmpty);
        notifyPropertyChanged(BR.isEmpty);

    }

    private ObservableBoolean isEmpty = new ObservableBoolean(false);

    @Bindable
    public Uri getSongThumbnailUrl() {
        return songThumbnailUrl.get();
    }

    public void setSongThumbnailUrl(Uri songThumbnailUrl) {
        this.songThumbnailUrl.set(songThumbnailUrl);
        notifyPropertyChanged(BR.songThumbnailUrl);
    }

    @Bindable
    public String getSongName() {
        return songName.get();
    }

    public void setSongName(String songName) {
        this.songName.set(songName);
        notifyPropertyChanged(BR.songName);
    }

    @Bindable
    public String getSongComposer() {
        return songComposer.get();
    }

    public void setSongComposer(String songComposer) {
        this.songComposer.set(songComposer);
        notifyPropertyChanged(BR.songComposer);
    }

    @Bindable
    public String getReuseCount() {
        return reuseCount.get();
    }

    public void setReuseCount(String reuseCount) {
        this.reuseCount.set(reuseCount);
        notifyPropertyChanged(BR.reuseCount);
    }

    public void hitSuggestedVideo(RichMediatorLiveData<VideoListResponseBean> videoListBeanRichMediatorLiveData, String videoParams){
        DataManager.getInstance().getVideoList(videoParams, String.valueOf(DataManager.getInstance().getUserId()), DataManager.getInstance().getLanguage()).enqueue(new NetworkCallback<VideoListResponseBean>() {
            @Override
            public void onSuccess(VideoListResponseBean videoListResponseBean) {
                videoListBeanRichMediatorLiveData.setValue(videoListResponseBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                videoListBeanRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                videoListBeanRichMediatorLiveData.setError(t);
            }
        });

    }
}
