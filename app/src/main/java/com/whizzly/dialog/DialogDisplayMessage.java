package com.whizzly.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.whizzly.R;
import com.whizzly.interfaces.DisplayMessageListener;

public class DialogDisplayMessage extends Dialog {

    private String displayMessage;
    private Button bt_ok;
    private TextView tv_description;
    private DisplayMessageListener displayMessageListener;

    public DialogDisplayMessage(Context context, String displayMessage, DisplayMessageListener displayMessageListener){

        super(context);
        this.displayMessage=displayMessage;
        this.displayMessageListener=displayMessageListener;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_message_display);
        tv_description=(TextView)findViewById(R.id.tv_discription);
        bt_ok=(Button)findViewById(R.id.btn_ok);
        tv_description.setText(displayMessage);
        bt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                displayMessageListener.onDisplayMessage();
            }
        });
    }
}
