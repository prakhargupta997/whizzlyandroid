package com.whizzly.dialog.dialogViewModels;

import androidx.lifecycle.ViewModel;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class SettingsDialogViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;

    public SettingsDialogViewModel(OnClickSendListener onClickSendListener)
    {
        this.onClickSendListener = onClickSendListener;
    }

    void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void onClickTurnOnNoti(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.TURN_ON_NOTI);
    }

    public void onClickReportAbuse(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.REPORT_ABUSE);
    }

    public void onClickBlockUser(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.BLOCK_USER);
    }
}
