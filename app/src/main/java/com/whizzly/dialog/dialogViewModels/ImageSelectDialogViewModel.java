package com.whizzly.dialog.dialogViewModels;

import androidx.lifecycle.ViewModel;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class ImageSelectDialogViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;

    public ImageSelectDialogViewModel(OnClickSendListener onClickSendListener)
    {
        this.onClickSendListener = onClickSendListener;
    }



    void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void onClickGallery(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.EDIT_PROFILE_GALLERY);
    }

    public void onClickCamera(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.EDIT_PROFILE_CAMERA);
    }

    public void onClickCancel(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.EDIT_PROFILE_CANCEL);
    }
}
