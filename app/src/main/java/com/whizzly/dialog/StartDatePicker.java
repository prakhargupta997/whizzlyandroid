package com.whizzly.dialog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import android.util.Log;
import android.widget.DatePicker;

import com.whizzly.R;
import com.whizzly.interfaces.OnDateSelectListener;

import java.util.Calendar;

public class StartDatePicker extends DialogFragment implements DatePickerDialog.OnDateSetListener, DatePickerDialog.OnClickListener{

    int year,month,date;
    OnDateSelectListener dateSelectListener;
    Calendar myCalendar;

    public StartDatePicker(){
        super();
    }
    public StartDatePicker(OnDateSelectListener dateSelectListener){
        this.dateSelectListener=dateSelectListener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        myCalendar = Calendar.getInstance();
        if(String.valueOf(year).equals("0")) {

            year = myCalendar.get(Calendar.YEAR);
            month = myCalendar.get(Calendar.MONTH);
            date = myCalendar.get(Calendar.DAY_OF_MONTH);
        }
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.datepicker, this, year, month, date);
        dialog.getDatePicker().setSpinnersShown(true);
        dialog.getDatePicker().setCalendarViewShown(false);
        Calendar calender = Calendar.getInstance();
        calender.add(Calendar.YEAR, -13);
        dialog.getDatePicker().setMaxDate(calender.getTimeInMillis());
        return dialog;

    }

    public void setPreviousDate(String year,String month,String date)
    {
        if(!year.equals("")){
            this.year=Integer.parseInt(year);
        }
        if(!month.equals("")){
            this.month=Integer.parseInt(month);
        }
        if(!date.equals("")){
            this.date=Integer.parseInt(date);
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear,
                          int dayOfMonth) {

        if (year < myCalendar.get(Calendar.YEAR))

            view.updateDate(myCalendar
                    .get(Calendar.YEAR), myCalendar
                    .get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));

        if (monthOfYear < myCalendar.get(Calendar.MONTH) && year == myCalendar.get(Calendar.YEAR))
            view.updateDate(myCalendar
                    .get(Calendar.YEAR), myCalendar
                    .get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));

        if (dayOfMonth < myCalendar.get(Calendar.DAY_OF_MONTH) && year == myCalendar.get(Calendar.YEAR) &&
                monthOfYear == myCalendar.get(Calendar.MONTH))
            view.updateDate(myCalendar
                    .get(Calendar.YEAR), myCalendar
                    .get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));

        this.year = year;
        this.month = monthOfYear;
        this.date = dayOfMonth;
        dateSelectListener.onDateSelect(dateFormat(date),dateFormat(month+1),dateFormat(year));
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {

        if(i == dialogInterface.BUTTON_POSITIVE) {
            dateSelectListener.onDateSelect(dateFormat(date),dateFormat(month),dateFormat(year));
        }
    }


    private String dateFormat(int date){

        String string=String.valueOf(date);
        if(string.length()==1){
            string="0"+string;
            Log.e("DateFormat",string);
        }
        return string;
    }
}