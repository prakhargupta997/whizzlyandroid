package com.whizzly.dialog;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.whizzly.R;
import com.whizzly.databinding.LayoutDialogErrorPermissionBinding;

import java.util.Objects;

public class ErrorDialog extends DialogFragment implements View.OnClickListener {

    private LayoutDialogErrorPermissionBinding mLayoutDialogErrorPermissionBinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mLayoutDialogErrorPermissionBinding = DataBindingUtil.inflate(inflater, R.layout.layout_dialog_error_permission, container, false);
        setClickListener();
        return mLayoutDialogErrorPermissionBinding.getRoot();
    }

    private void setClickListener(){
        mLayoutDialogErrorPermissionBinding.btnCancel.setOnClickListener(this);
        mLayoutDialogErrorPermissionBinding.btnSettings.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_cancel:
                dismiss();
                break;
            case R.id.btn_settings:
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.fromParts("package", Objects.requireNonNull(getActivity()).getPackageName(), null));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                dismiss();
                break;
        }
    }
}
