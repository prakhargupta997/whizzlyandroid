package com.whizzly.home_feed_comments;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.whizzly.R;
import com.whizzly.databinding.ItemRowGifBinding;
import com.whizzly.interfaces.OnRecyclerViewClickListener;
import com.whizzly.models.gifModels.Datum;

import java.util.ArrayList;

class GifAdapter extends RecyclerView.Adapter<GifAdapter.ViewHolder> {

    private ArrayList<Datum> data;
    private OnRecyclerViewClickListener onRecyclerViewClickListener;
    private Context context;
    private ItemRowGifBinding itemRowGifBinding;
    private int width = 0;

    public GifAdapter(ArrayList<Datum> mGifModels, OnRecyclerViewClickListener onRecyclerViewClickListener, Context context) {
        this.onRecyclerViewClickListener = onRecyclerViewClickListener;
        this.data = mGifModels;
        this.context = context;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        itemRowGifBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_row_gif, parent, false);
        width = parent.getWidth() / 2;
        return new ViewHolder(itemRowGifBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
        requestOptions.placeholder(R.drawable.ic_feed_add_options_fun);
        requestOptions.error(R.drawable.ic_feed_add_options_fun);
        Glide.with(context).asGif().load(data.get(position).getImages().getOriginal().getUrl()).apply(requestOptions).into(itemRowGifBinding.ivGif);
        itemRowGifBinding.ivGif.setMinimumWidth(width);
        itemRowGifBinding.ivGif.setMinimumHeight(width);
        itemRowGifBinding.flSelected.setMinimumHeight(width);
        itemRowGifBinding.flSelected.setMinimumWidth(
                width
        );
        itemRowGifBinding.getRoot().setOnClickListener(view -> onRecyclerViewClickListener.onRecyclerViewClicked(data.get(position), position));

        if (data.get(position).getIsSelected() == 1) {
            itemRowGifBinding.flSelected.setVisibility(View.VISIBLE);
        } else {
            itemRowGifBinding.flSelected.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(ItemRowGifBinding itemRowGifBinding) {
            super(itemRowGifBinding.getRoot());
        }
    }
}
