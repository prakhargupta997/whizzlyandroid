package com.whizzly.home_feed_comments;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.whizzly.BR;

public class CameraCommentModel extends BaseObservable {
    private ObservableField<String> timeProgress = new ObservableField<>();
    private ObservableBoolean isFlashClicked = new ObservableBoolean(false);
    private ObservableBoolean isRecordPressed = new ObservableBoolean(false);
    private ObservableBoolean isVideoStarted = new ObservableBoolean(false);

    @Bindable
    public String getTimeProgress() {
        return timeProgress.get();
    }

    public void setTimeProgress(String timeProgress) {
        this.timeProgress.set(timeProgress);
        notifyPropertyChanged(BR.timeProgress);
    }

    @Bindable
    public boolean getIsFlashClicked() {
        return isFlashClicked.get();
    }

    public void setIsFlashClicked(boolean isFlashClicked) {
        this.isFlashClicked.set(isFlashClicked);
        notifyPropertyChanged(BR.isFlashClicked);
    }

    @Bindable
    public boolean getIsRecordPressed() {
        return isRecordPressed.get();
    }

    public void setIsRecordPressed(boolean isRecordPressed) {
        this.isRecordPressed.set(isRecordPressed);
        notifyPropertyChanged(BR.isRecordPressed);
    }

    @Bindable
    public boolean getIsVideoStarted() {
        return isVideoStarted.get();
    }

    public void setIsVideoStarted(boolean isVideoStarted) {
        this.isVideoStarted.set(isVideoStarted);
        notifyPropertyChanged(BR.isVideoStarted);
    }
}
