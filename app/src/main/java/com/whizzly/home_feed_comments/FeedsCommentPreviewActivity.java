package com.whizzly.home_feed_comments;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.giphy.sdk.core.models.enums.RenditionType;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.ActivityFeedsCommentPreviewBinding;
import com.whizzly.home_screen_module.home_screen_fragments.profile.ProfileFragment;
import com.whizzly.interfaces.OnBackPressedListener;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.comment_list.Result;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.ArrayList;

public class FeedsCommentPreviewActivity extends BaseActivity implements OnClickSendListener {

    private static final DefaultBandwidthMeter BANDWIDTH_METER =
            new DefaultBandwidthMeter();
    private ActivityFeedsCommentPreviewBinding activityFeedsCommentPreviewBinding;
    private FeedsCommentPreviewViewModel mFeedsCommentPreviewViewModel;
    private ArrayList<Result> result;
    private OnBackPressedListener onBackPressedListener;
    private ExoPlayer mExoPlayer;
    private int position;
    private Player.DefaultEventListener defaultEventListener = new Player.DefaultEventListener() {
        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            super.onPlayerStateChanged(playWhenReady, playbackState);
            switch (playbackState) {
                case Player.STATE_BUFFERING:
                    if(!isFinishing() && !isDestroyed()) {
                        activityFeedsCommentPreviewBinding.flProgressBar.setVisibility(View.VISIBLE);
                    }
                    break;
                case Player.STATE_READY:
                    if(!isFinishing() && !isDestroyed()) {
                        activityFeedsCommentPreviewBinding.ivCommentThumb.setVisibility(View.GONE);
                        activityFeedsCommentPreviewBinding.flProgressBar.setVisibility(View.GONE);
                        handler.removeCallbacks(null);
                    }
                    break;
                case Player.STATE_ENDED:
                    if(!isFinishing() && !isDestroyed()) {

                        if (++position < result.size())
                            showNextComment();
                        else if(position == result.size()){
                            if (mExoPlayer != null) {
                                mExoPlayer.setPlayWhenReady(false);
                            }
                            handler.post(() -> onBackPressed());
                        }
                    }
                    break;
            }
        }
    };

    private void getData() {
        result = (ArrayList<Result>) getIntent().getSerializableExtra(AppConstants.ClassConstants.ON_VIDEO_FEED_COMMENT_CLICKED);
        position = getIntent().getIntExtra(AppConstants.ClassConstants.COMMENT_POSITION, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityFeedsCommentPreviewBinding = DataBindingUtil.setContentView(this, R.layout.activity_feeds_comment_preview);
        mFeedsCommentPreviewViewModel = ViewModelProviders.of(this).get(FeedsCommentPreviewViewModel.class);
        activityFeedsCommentPreviewBinding.setViewModel(mFeedsCommentPreviewViewModel);
        mFeedsCommentPreviewViewModel.setOnClickSendListener(this);
        getData();
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_profile_setup_user_placeholder);
        requestOptions.circleCrop();
        Glide.with(this).load(result.get(position).getCommentUserProfileImage()).apply(requestOptions).into(activityFeedsCommentPreviewBinding.ivCommentUserImage);
        activityFeedsCommentPreviewBinding.tvFirstName.setText(result.get(position).getCommentUserFirstName());
        activityFeedsCommentPreviewBinding.tvUserName.setText(result.get(position).getCommentUserName());
        activityFeedsCommentPreviewBinding.ivBack.setOnClickListener(v -> onBackPressed());
        if (result.get(position).getCommentType().equalsIgnoreCase("video")) {
            setExoplayer();
            activityFeedsCommentPreviewBinding.ivCommentThumb.setVisibility(View.GONE);
            activityFeedsCommentPreviewBinding.pvCommentPreview.setVisibility(View.VISIBLE);
        } else if(result.get(position).getCommentType().equalsIgnoreCase("image")){
            activityFeedsCommentPreviewBinding.ivCommentThumb.setVisibility(View.VISIBLE);
            activityFeedsCommentPreviewBinding.pvCommentPreview.setVisibility(View.GONE);
            activityFeedsCommentPreviewBinding.flProgressBar.setVisibility(View.VISIBLE);
            Glide.with(this).asGif().load(result.get(position).getComment()).listener(new RequestListener<GifDrawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<GifDrawable> target, boolean isFirstResource) {
                    activityFeedsCommentPreviewBinding.flProgressBar.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(GifDrawable resource, Object model, Target<GifDrawable> target, com.bumptech.glide.load.DataSource dataSource, boolean isFirstResource) {
                    activityFeedsCommentPreviewBinding.flProgressBar.setVisibility(View.GONE);
                    return false;
                }
            }).into(activityFeedsCommentPreviewBinding.ivCommentThumb);
            handler.postDelayed(() -> defaultEventListener.onPlayerStateChanged(false, Player.STATE_ENDED), 4000);
        }else{
            Glide.with(this).asGif().load(result.get(position).getComment()).apply(requestOptions).into(activityFeedsCommentPreviewBinding.ivCommentThumb);
            handler.postDelayed(() -> defaultEventListener.onPlayerStateChanged(false, Player.STATE_ENDED), 4000);
        }


        activityFeedsCommentPreviewBinding.viewTouch.setOnTouchListener((view, motionEvent) -> {
            if(motionEvent.getX()<getWindowManager().getDefaultDisplay().getWidth()/3){
                if (--position >= 0) {
                    handler.removeCallbacks(null);
                    mExoPlayer.setPlayWhenReady(false);
                    mExoPlayer.stop();
                    mExoPlayer.release();
                    showNextComment();
                }
            }else if(motionEvent.getX()>(2*getWindowManager().getDefaultDisplay().getWidth())/3){
                if (++position < result.size()) {
                    handler.removeCallbacks(null);
                    mExoPlayer.setPlayWhenReady(false);
                    mExoPlayer.stop();
                    mExoPlayer.release();
                    showNextComment();
                }
            }else{
                switch (motionEvent.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        if(result.get(position).getCommentType().equalsIgnoreCase("image"))
                        handler.removeCallbacks(null);
                        else
                        mExoPlayer.setPlayWhenReady(false);
                        return true;
                    case MotionEvent.ACTION_UP:
                        if(result.get(position).getCommentType().equalsIgnoreCase("image"))
                            handler.postDelayed(() -> defaultEventListener.onPlayerStateChanged(false, Player.STATE_ENDED), 4000);
                        else
                            mExoPlayer.setPlayWhenReady(true);
                        break;
                }
            }
            return false;
        });
    }



    private void setExoplayer() {
        TrackSelection.Factory adaptiveTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(BANDWIDTH_METER);

        mExoPlayer = ExoPlayerFactory.newSimpleInstance(this,
                new DefaultRenderersFactory(this),
                new DefaultTrackSelector(adaptiveTrackSelectionFactory),
                new DefaultLoadControl());

        mExoPlayer.setPlayWhenReady(true);
        mExoPlayer.setRepeatMode(Player.REPEAT_MODE_OFF);
        MediaSource mediaSource = buildMediaSource(Uri.parse(result.get(position).getComment()));
        mExoPlayer.addListener(defaultEventListener);
        mExoPlayer.prepare(mediaSource, true, false);
        activityFeedsCommentPreviewBinding.pvCommentPreview.setPlayer(mExoPlayer);
    }

    private void showNextComment() {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_profile_setup_user_placeholder);
        requestOptions.circleCrop();
        Glide.with(this).load(result.get(position).getCommentUserProfileImage()).apply(requestOptions).into(activityFeedsCommentPreviewBinding.ivCommentUserImage);
        activityFeedsCommentPreviewBinding.tvFirstName.setText(result.get(position).getCommentUserFirstName());
        activityFeedsCommentPreviewBinding.tvUserName.setText(result.get(position).getCommentUserName());
        activityFeedsCommentPreviewBinding.ivBack.setOnClickListener(v -> onBackPressed());
        if (result.get(position).getCommentType().equalsIgnoreCase("video")) {
            setExoplayer();
            activityFeedsCommentPreviewBinding.ivCommentThumb.setVisibility(View.GONE);
            activityFeedsCommentPreviewBinding.pvCommentPreview.setVisibility(View.VISIBLE);
        } else if(result.get(position).getCommentType().equalsIgnoreCase("image")){
            activityFeedsCommentPreviewBinding.ivCommentThumb.setVisibility(View.VISIBLE);
            activityFeedsCommentPreviewBinding.pvCommentPreview.setVisibility(View.GONE);
            activityFeedsCommentPreviewBinding.flProgressBar.setVisibility(View.VISIBLE);
            Glide.with(this).load(result.get(position).getComment()).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    activityFeedsCommentPreviewBinding.flProgressBar.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, com.bumptech.glide.load.DataSource dataSource, boolean isFirstResource) {
                    activityFeedsCommentPreviewBinding.flProgressBar.setVisibility(View.GONE);
                    return false;
                }
            }).into(activityFeedsCommentPreviewBinding.ivCommentThumb);
            handler.postDelayed(() -> defaultEventListener.onPlayerStateChanged(false, Player.STATE_ENDED), 4000);
        }else{
            Glide.with(this).asGif().load(result.get(position).getComment()).apply(requestOptions).into(activityFeedsCommentPreviewBinding.ivCommentThumb);
            handler.postDelayed(() -> defaultEventListener.onPlayerStateChanged(false, Player.STATE_ENDED), 4000);
        }
    }

    private Handler handler = new Handler();

    private MediaSource buildMediaSource(Uri uri) {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, getString(R.string.app_name)), bandwidthMeter);
        return new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri);

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            if (mExoPlayer != null) {
                mExoPlayer.setPlayWhenReady(false);
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            if (mExoPlayer != null) {
                mExoPlayer.release();
                mExoPlayer = null;
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (onBackPressedListener != null) {
            onBackPressedListener.onBackPressed();
            activityFeedsCommentPreviewBinding.flContainer.setVisibility(View.GONE);
        } else {
            finish();
        }
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_COMMENT_USER_PROFILE_CLICKED:
                if (result.get(position).getUserId() != DataManager.getInstance().getUserId()) {
                    if (mExoPlayer != null) {
                        mExoPlayer.setPlayWhenReady(false);
                    }
                    activityFeedsCommentPreviewBinding.flContainer.setVisibility(View.VISIBLE);
                    activityFeedsCommentPreviewBinding.flProgressBar.setVisibility(View.GONE);
                    ProfileFragment profileFragment = new ProfileFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.OTHER_USER_PROFILE);
                    bundle.putString(AppConstants.ClassConstants.USER_ID, String.valueOf(result.get(position).getUserId()));
                    bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.FROM_COMMENTS);
                    profileFragment.setArguments(bundle);
                    addFragmentWithBackstack(R.id.fl_container, profileFragment, ProfileFragment.class.getCanonicalName());
                }
                break;
        }
    }

    @Override
    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }
}
