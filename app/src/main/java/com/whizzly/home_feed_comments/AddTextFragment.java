package com.whizzly.home_feed_comments;


import android.annotation.SuppressLint;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.whizzly.R;
import com.whizzly.databinding.FragmentAddTextBinding;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.HorizontalSlideColorPicker;

import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class AddTextFragment extends DialogFragment implements OnClickSendListener {

    private FragmentAddTextBinding mFragmentAddTextBinding;
    private OnSendTextListener onSendTextListener;

    public AddTextFragment(OnSendTextListener onSendTextListener) {
        this.onSendTextListener = onSendTextListener;
    }

    public AddTextFragment(){}

    private AddTextViewModel mAddTextViewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFragmentAddTextBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_text, container, false);
        mAddTextViewModel = ViewModelProviders.of(this).get(AddTextViewModel.class);
        mFragmentAddTextBinding.setViewModel(mAddTextViewModel);
        mAddTextViewModel.setOnClickSendListener(this);
        return mFragmentAddTextBinding.getRoot();
    }

    private String textColor = "#FFFFFF";

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentAddTextBinding.etAddText.requestFocus();
        openKeyboard(mFragmentAddTextBinding.etAddText);
        mFragmentAddTextBinding.colorSlider.setOnColorChangedListener(new HorizontalSlideColorPicker.OnColorChangedListener() {
            @Override
            public void onColorChanged(String color) {
                textColor = color;
                mFragmentAddTextBinding.etAddText.setTextColor(Color.parseColor(color));
            }
        });

        mFragmentAddTextBinding.etAddText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mAddedText = s.toString();
            }
        });
    }

    private String mAddedText = "";

    private void openKeyboard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public void onClickSend(int code) {
        switch (code){
            case AppConstants.ClassConstants.ON_ADD_TEXT_DONE_CLICKED:
                onSendTextListener.onTextSend(mAddedText, textColor);
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction().remove(this).commit();
                break;
        }
    }
}
