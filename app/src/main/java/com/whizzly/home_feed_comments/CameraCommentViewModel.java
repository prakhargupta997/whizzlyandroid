package com.whizzly.home_feed_comments;

import androidx.lifecycle.ViewModel;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class CameraCommentViewModel extends ViewModel {
    private OnClickSendListener onClickSendListener;

    public void onCloseClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CLOSE_VIDEO_RECORDING);
    }

    public void onFlipClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_COMMENT_FLIP_CAMERA);
    }

    public void onFlashClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_COMMENT_FLASH_CLICKED);
    }

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void saveRecording() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_COMMENT_SAVE_RECORDING);
    }

    public void onGalleryClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_COMMENT_GALLERY_CLICKED);
    }

    public void onAddTextClicked(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_COMMENT_ADD_TEXT_CLICKED);
    }
}
