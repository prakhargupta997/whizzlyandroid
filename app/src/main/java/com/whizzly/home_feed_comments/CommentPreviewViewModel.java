package com.whizzly.home_feed_comments;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.post_comment.PostCommentBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class CommentPreviewViewModel extends ViewModel {
    private OnClickSendListener onClickSendListener;

    public RichMediatorLiveData<PostCommentBean> getmRichMediatorLiveData() {
        return mRichMediatorLiveData;
    }

    private RichMediatorLiveData<PostCommentBean> mRichMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;

    public void setGenericObservers(Observer<FailureResponse> failureResponseObserver, Observer<Throwable> errorObserver){
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        if(mRichMediatorLiveData == null){
            mRichMediatorLiveData = new RichMediatorLiveData<PostCommentBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
    }

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void onDiscardComment() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_COMMENT_DISCARD_CLICKED);
    }

    public void onAddingText() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_COMMENT_ADD_TEXT_CLICKED);
    }

    public void onPostCommentClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_COMMENT_POST_CLICKED);
    }

    public void hitPostCommentApi(HashMap<String, String> postCommentRequest) {
        DataManager.getInstance().postComment(postCommentRequest).enqueue(new NetworkCallback<PostCommentBean>() {
            @Override
            public void onSuccess(PostCommentBean postCommentBean) {
                mRichMediatorLiveData.setValue(postCommentBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mRichMediatorLiveData.setError(t);
            }
        });
    }
    public void onDoneClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_ADD_TEXT_DONE_CLICKED);
    }
}
