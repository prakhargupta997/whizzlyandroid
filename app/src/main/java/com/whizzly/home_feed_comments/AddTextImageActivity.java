package com.whizzly.home_feed_comments;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.dnitinverma.amazons3library.AmazonS3;
import com.dnitinverma.amazons3library.AmazonS3Constants;
import com.dnitinverma.amazons3library.interfaces.AmazonCallback;
import com.dnitinverma.amazons3library.model.MediaBean;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.ActivityAddTextImageBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.feeds_response.Result;
import com.whizzly.models.post_comment.PostCommentBean;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;
import com.whizzly.utils.HorizontalSlideColorPicker;
import com.whizzly.utils.RotationGestureDetector;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Objects;

public class AddTextImageActivity extends BaseActivity implements OnClickSendListener, RotationGestureDetector.OnRotationGestureListener, AmazonCallback {

    private ActivityAddTextImageBinding activityAddTextImageBinding;
    private AddTextImageViewModel addTextImageViewModel;
    private ScaleGestureDetector mScaleGestureDetector;
    private RotationGestureDetector mRotationDetector;
    private String filePath;
    private String textColor = "#FFFFFF";
    private AmazonS3 mAmazonS3;
    private Result mFeedData;
    private String uploadUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setStatusBarColor(getResources().getColor(R.color.black));
        mFeedData = getIntent().getParcelableExtra(AppConstants.ClassConstants.FEEDS_DATA);
        activityAddTextImageBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_text_image);
        addTextImageViewModel = ViewModelProviders.of(this).get(AddTextImageViewModel.class);
        activityAddTextImageBinding.setViewModel(addTextImageViewModel);
        addTextImageViewModel.setOnClickSendListener(this);
        activityAddTextImageBinding.etAddText.requestFocus();
        setViews();
        addTextImageViewModel.setGenericObservers(getFailureResponseObserver(), getErrorObserver());
        addTextImageViewModel.getmRichMediatorLiveData().observe(this, new Observer<PostCommentBean>() {
            @Override
            public void onChanged(@Nullable PostCommentBean postCommentBean) {
                if (postCommentBean != null) {
                    if (postCommentBean.getCode() == 200) {
                        activityAddTextImageBinding.flProgressBar.setVisibility(View.GONE);
                        Intent intent = new Intent(AddTextImageActivity.this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    } else if (postCommentBean.getCode() == 301 || postCommentBean.getCode() == 302) {
                        Toast.makeText(AddTextImageActivity.this, postCommentBean.getMessage(), Toast.LENGTH_SHORT).show();
                        AddTextImageActivity.this.autoLogOut();
                    }
                }

            }
        });
        openKeyboard(activityAddTextImageBinding.etAddText);
        mAmazonS3 = new AmazonS3();
        mAmazonS3.setActivity(this);
        mAmazonS3.setCallback(this);
    }

    public Bitmap loadBitmapFromView(View v) {
        Bitmap b = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return b;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        mScaleGestureDetector.onTouchEvent(event);
        mRotationDetector.onTouchEvent(event);
        return super.dispatchTouchEvent(event);
    }

    private void setViews() {
        activityAddTextImageBinding.tvDone.setVisibility(View.VISIBLE);
        activityAddTextImageBinding.ivSaveRecording.setVisibility(View.GONE);
        activityAddTextImageBinding.tvAddedText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    v.setX(event.getRawX() - v.getWidth() / 2.0f);
                    v.setY(event.getRawY() - v.getHeight() / 2.0f);
                }
                return true;
            }

        });
        activityAddTextImageBinding.colorSlider.setOnColorChangedListener(new HorizontalSlideColorPicker.OnColorChangedListener() {
            @Override
            public void onColorChanged(String color) {
                textColor = color;
                activityAddTextImageBinding.etAddText.setTextColor(Color.parseColor(color));
            }
        });
        mScaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());
        mRotationDetector = new RotationGestureDetector(this);
    }

    @Override
    public void OnRotation(RotationGestureDetector rotationDetector) {
        float angle = rotationDetector.getAngle();
        activityAddTextImageBinding.tvAddedText.setRotation(360 - angle);
    }

    private void openKeyboard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_ADD_TEXT_DONE_CLICKED:
                activityAddTextImageBinding.tvAddedText.setText(activityAddTextImageBinding.etAddText.getText());
                activityAddTextImageBinding.etAddText.setVisibility(View.GONE);
                activityAddTextImageBinding.tvAddedText.setVisibility(View.VISIBLE);
                activityAddTextImageBinding.tvDone.setVisibility(View.GONE);
                activityAddTextImageBinding.colorSlider.setVisibility(View.GONE);
                activityAddTextImageBinding.ivSaveRecording.setVisibility(View.VISIBLE);
                activityAddTextImageBinding.tvAddedText.setTextColor(Color.parseColor(textColor));
                break;
            case AppConstants.ClassConstants.ON_COMMENT_SAVE_RECORDING:
                Bitmap bitmap = loadBitmapFromView(activityAddTextImageBinding.rlImageWithText);
                try {
                    filePath = getStringPath();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(new File(filePath)));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                postVideoOrImage();
                activityAddTextImageBinding.flProgressBar.setVisibility(View.VISIBLE);
                break;
            case AppConstants.ClassConstants.PREVIEW_VIDEO_ON_CLOSE_CLICKED:
                finish();
                break;
        }
    }

    private String getStringPath() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "image_" + System.currentTimeMillis() + ".jpeg";
    }

    private void postVideoOrImage() {
        MediaBean mediaBean = new MediaBean();
        mediaBean.setMediaPath(filePath);
        mAmazonS3.upload(mediaBean);
        String filename = getFileName(Uri.parse(filePath));
        uploadUrl = AmazonS3Constants.AMAZON_SERVER_URL + filename;
    }

    public String getFileName(Uri uri) {
        String result = null;
        result = uri.getPath();
        int cut = Objects.requireNonNull(result).lastIndexOf('/');
        if (cut != -1) {
            result = result.substring(cut + 1);
        }
        return result;
    }

    @Override
    public void uploadSuccess(MediaBean bean) {
        postComment();
    }

    private void postComment() {
        HashMap<String, String> commentPostRequest = new HashMap<>();
        commentPostRequest.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        commentPostRequest.put(AppConstants.NetworkConstants.VIDEO_ID, String.valueOf(mFeedData.getId()));
        commentPostRequest.put(AppConstants.NetworkConstants.VIDEO_TYPE, mFeedData.getType());
        commentPostRequest.put(AppConstants.NetworkConstants.COMMENT_TYPE, AppConstants.NetworkConstants.COMMENT_TYPE_IMAGE);
        commentPostRequest.put(AppConstants.NetworkConstants.COMMENT_URL, uploadUrl);
        addTextImageViewModel.hitPostCommentApi(commentPostRequest);
    }

    @Override
    public void uploadFailed(MediaBean bean) {
        mAmazonS3.upload(bean);
    }

    @Override
    public void uploadProgress(MediaBean bean) {

    }

    @Override
    public void uploadError(Exception e, MediaBean imageBean) {
        mAmazonS3.upload(imageBean);
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            float mScaleFactor = scaleGestureDetector.getScaleFactor();
            mScaleFactor = Math.max(1.0f, Math.min(mScaleFactor, 3.0f));
            activityAddTextImageBinding.tvAddedText.setTextSize(TypedValue.COMPLEX_UNIT_PX, activityAddTextImageBinding.tvAddedText.getTextSize() * mScaleFactor);
            return true;
        }
    }
}
