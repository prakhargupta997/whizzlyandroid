package com.whizzly.home_feed_comments;

import android.app.AlertDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import androidx.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;

import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.dnitinverma.amazons3library.AmazonS3;
import com.dnitinverma.amazons3library.AmazonS3Constants;
import com.dnitinverma.amazons3library.interfaces.AmazonCallback;
import com.dnitinverma.amazons3library.model.MediaBean;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.ActivityCommentPreviewBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.feeds_response.Result;
import com.whizzly.models.post_comment.PostCommentBean;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;
import com.whizzly.utils.FFMpegCommandListener;
import com.whizzly.utils.FFMpegCommands;
import com.whizzly.utils.RotationGestureDetector;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Objects;

public class CommentPreviewActivity extends BaseActivity implements RotationGestureDetector.OnRotationGestureListener, OnClickSendListener, AmazonCallback, OnSendTextListener {

    private ActivityCommentPreviewBinding mActivityCommentPreviewBinding;
    private CommentPreviewViewModel mCommentPreviewViewModel;
    private Result mFeedResponse;
    private int commentType;
    private String filePath;
    private ExoPlayer mExoplayer;
    private AmazonS3 mAmazonS3;
    private String uploadUrl;
    private float xCoordinate, yCoordinate;
    private ScaleGestureDetector mScaleGestureDetector;
    private RotationGestureDetector mRotationDetector;
    private float mScaleFactor = 1.0f;
    private String mText, mTextColor;
    private float mTextSize = 1.0f;
    private FFMpegCommands ffMpegCommands;
    private boolean isDonePressed = false;
    private float zoomLimit = 3.0f, defaultSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mActivityCommentPreviewBinding = DataBindingUtil.setContentView(this, R.layout.activity_comment_preview);
        mCommentPreviewViewModel = ViewModelProviders.of(this).get(CommentPreviewViewModel.class);
        mActivityCommentPreviewBinding.setViewModel(mCommentPreviewViewModel);
        mCommentPreviewViewModel.setOnClickSendListener(this);
        mCommentPreviewViewModel.setGenericObservers(getFailureResponseObserver(), getErrorObserver());
        mCommentPreviewViewModel.getmRichMediatorLiveData().observe(this, new Observer<PostCommentBean>() {
            @Override
            public void onChanged(@Nullable PostCommentBean postCommentBean) {
                if (postCommentBean!=null){
                    if (postCommentBean.getCode() == 200) {
                        if (mExoplayer != null) {
                            mExoplayer.setPlayWhenReady(false);
                            mExoplayer.release();
                            mExoplayer = null;
                        }
                        mActivityCommentPreviewBinding.flProgressBar.setVisibility(View.GONE);
                        Intent intent = new Intent(CommentPreviewActivity.this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                    else if (postCommentBean.getCode()==301 ||postCommentBean.getCode()==302){
                        Toast.makeText(CommentPreviewActivity.this, postCommentBean.getMessage(), Toast.LENGTH_SHORT).show();
                        ((BaseActivity) CommentPreviewActivity.this).autoLogOut();
                    }
                }

            }
        });
        getData();
        setViews();
        ffMpegCommands = FFMpegCommands.getInstance(this);
        mRotationDetector = new RotationGestureDetector(this);
        mScaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());
        mAmazonS3 = new AmazonS3();
        mAmazonS3.setActivity(this);
        mAmazonS3.setCallback(this);
        mActivityCommentPreviewBinding.tvAddedText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    xCoordinate = event.getRawX() - v.getWidth() / 2.0f;
                    yCoordinate = event.getRawY() - v.getHeight() / 2.0f;
                    v.setX(xCoordinate);
                    v.setY(yCoordinate);

                }
                return true;
            }

        });

        try {
            ffMpegCommands.initializeFFmpeg();
        } catch (FFmpegNotSupportedException e) {
            e.printStackTrace();
        }
    }

    private void setViews() {
        switch (commentType) {
            case AppConstants.ClassConstants.COMMENT_TYPE_IMAGE:
                mActivityCommentPreviewBinding.ivAddText.setVisibility(View.VISIBLE);
                mActivityCommentPreviewBinding.ivImagePreview.setVisibility(View.VISIBLE);
                mActivityCommentPreviewBinding.pvVideoPeview.setVisibility(View.GONE);
                Glide.with(this).asBitmap().load(filePath)
                        .listener(new RequestListener<Bitmap>() {
                                      @Override
                                      public boolean onLoadFailed(@Nullable GlideException e, Object o, Target<Bitmap> target, boolean b) {
                                          return false;
                                      }

                                      @Override
                                      public boolean onResourceReady(Bitmap bitmap, Object o, Target<Bitmap> target, DataSource dataSource, boolean b) {
                                          new Handler().postDelayed(() -> new Handler().postDelayed(() -> {
                                              filePath = getPngStringPath();
                                              try {
                                                  bitmap.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(new File(filePath)));
                                              } catch (FileNotFoundException e) {
                                                  e.printStackTrace();
                                              }
                                              mActivityCommentPreviewBinding.ivImagePreview.setImageURI(Uri.fromFile(new File(filePath)));
                                          }, 1000), 100);


                                          return false;
                                      }
                                  }
                        ).submit();
                break;
            case AppConstants.ClassConstants.COMMENT_TYPE_VIDEO:
                mActivityCommentPreviewBinding.ivImagePreview.setVisibility(View.GONE);
                mActivityCommentPreviewBinding.ivAddText.setVisibility(View.VISIBLE);
                setExoplayer();
                break;
        }
    }

    private String getFilePath() {
        File file = getExternalFilesDir(null);
        return (file == null ? "" : (file.getAbsolutePath() + "/"))
                + "font_file.tff";
    }

    private File createFileFromInputStream(InputStream inputStream) {

        try {
            File f = new File(getFilePath());
            OutputStream outputStream = new FileOutputStream(f);
            byte buffer[] = new byte[1024];
            int length = 0;

            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
            }

            outputStream.close();
            inputStream.close();

            return f;
        } catch (IOException e) {
        }

        return null;
    }

    private void setExoplayer() {
        if (mExoplayer == null) {
            mExoplayer = ExoPlayerFactory.newSimpleInstance(this,
                    new DefaultRenderersFactory(this),
                    new DefaultTrackSelector(), new DefaultLoadControl());
            mExoplayer.setPlayWhenReady(true);
            mExoplayer.setRepeatMode(Player.REPEAT_MODE_ONE);
            Uri uri = Uri.parse(filePath);
            MediaSource mediaSource = buildMediaSource(uri);
            mActivityCommentPreviewBinding.pvVideoPeview.setPlayer(mExoplayer);
            mExoplayer.prepare(mediaSource, true, false);
        }
    }

    private MediaSource buildMediaSource(Uri uri) {
        return new ExtractorMediaSource.Factory(
                new DefaultDataSourceFactory(this, "whizzly-app")).
                createMediaSource(uri);
    }

    private void getData() {
        Intent intent = getIntent();
        mFeedResponse = intent.getParcelableExtra(AppConstants.ClassConstants.FEEDS_DATA);
        commentType = intent.getIntExtra(AppConstants.ClassConstants.COMMENT_TYPE, 1);
        filePath = intent.getStringExtra(AppConstants.ClassConstants.COMMENT_FILE_PATH);
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_COMMENT_ADD_TEXT_CLICKED:
                getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, new AddTextFragment(this::onTextSend)).commit();
                mActivityCommentPreviewBinding.ivAddText.setVisibility(View.GONE);
                mActivityCommentPreviewBinding.btnPostComment.setVisibility(View.GONE);
                break;
            case AppConstants.ClassConstants.ON_COMMENT_DISCARD_CLICKED:
                discardRecording();
                break;
            case AppConstants.ClassConstants.ON_COMMENT_POST_CLICKED:
                if (commentType == AppConstants.ClassConstants.COMMENT_TYPE_IMAGE) {

                    postVideoOrImage();
                    mActivityCommentPreviewBinding.flProgressBar.setVisibility(View.VISIBLE);
                } else if (mActivityCommentPreviewBinding.tvAddedText.getText() != null && mActivityCommentPreviewBinding.tvAddedText.getText().toString().length() > 0) {
                    addTextToVideo();
                } else {
                    mActivityCommentPreviewBinding.flProgressBar.setVisibility(View.VISIBLE);
                    postVideoOrImage();
                }

                break;
            case AppConstants.ClassConstants.ON_ADD_TEXT_DONE_CLICKED:
                if (commentType == AppConstants.ClassConstants.COMMENT_TYPE_IMAGE) {
                    addTextToImage();
                    mActivityCommentPreviewBinding.tvAddedText.setVisibility(View.GONE);
                    Glide.with(this).load(filePath).into(mActivityCommentPreviewBinding.ivImagePreview);
                }
                isDonePressed = true;
                mActivityCommentPreviewBinding.tvDone.setVisibility(View.GONE);
                mActivityCommentPreviewBinding.btnPostComment.setVisibility(View.VISIBLE);
                break;
        }
    }

    private String getVideoFilePath(Context context) {
        final File dir = context.getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "video_with_text_" + System.currentTimeMillis() + ".mp4";
    }

    private void addTextToVideo() {
        Bitmap bitmap = loadBitmapFromView(mActivityCommentPreviewBinding.rlImageWithText);
        String imageFilePath = "";
        try {
            imageFilePath = getPngStringPath();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(new File(imageFilePath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String inputFile = filePath;
        filePath = getVideoFilePath(CommentPreviewActivity.this);

        ffMpegCommands.addTextToVideo(inputFile, filePath, imageFilePath, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                postVideoOrImage();
            }

            @Override
            public void onProgress(String message) {
            }

            @Override
            public void onFailure(String message) {
                mActivityCommentPreviewBinding.flProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onStart() {
                mActivityCommentPreviewBinding.flProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {

            }
        });


    }

    private void postVideoOrImage() {
        MediaBean mediaBean = new MediaBean();
        mediaBean.setMediaPath(filePath);
        mAmazonS3.upload(mediaBean);
        String filename = getFileName(Uri.parse(filePath));
        uploadUrl = AmazonS3Constants.AMAZON_SERVER_URL + filename;
    }

    public String getFileName(Uri uri) {
        String result = null;
        result = uri.getPath();
        int cut = Objects.requireNonNull(result).lastIndexOf('/');
        if (cut != -1) {
            result = result.substring(cut + 1);
        }
        return result;
    }

    private void postComment() {
        HashMap<String, String> commentPostRequest = new HashMap<>();
        commentPostRequest.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        commentPostRequest.put(AppConstants.NetworkConstants.VIDEO_ID, String.valueOf(mFeedResponse.getId()));
        commentPostRequest.put(AppConstants.NetworkConstants.VIDEO_TYPE, mFeedResponse.getType());
        if (commentType == AppConstants.ClassConstants.COMMENT_TYPE_IMAGE)
            commentPostRequest.put(AppConstants.NetworkConstants.COMMENT_TYPE, AppConstants.NetworkConstants.COMMENT_TYPE_IMAGE);
        else
            commentPostRequest.put(AppConstants.NetworkConstants.COMMENT_TYPE, AppConstants.NetworkConstants.COMMENT_TYPE_VIDEO);

        commentPostRequest.put(AppConstants.NetworkConstants.COMMENT_URL, uploadUrl);

        mCommentPreviewViewModel.hitPostCommentApi(commentPostRequest);
    }

    private void discardRecording() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.txt_discard_video)
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> discard())
                .setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss())
                .create().show();
    }

    private void discard() {
        File file = new File(filePath);
        if (file.delete()) {
            Log.e("deleteRecording: ", "Deleted File");
        }
        Intent intent = new Intent(this, CameraCommentActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void uploadSuccess(MediaBean bean) {
        postComment();
    }

    @Override
    public void uploadFailed(MediaBean bean) {
        mAmazonS3.upload(bean);
    }

    @Override
    public void uploadProgress(MediaBean bean) {

    }

    @Override
    public void uploadError(Exception e, MediaBean imageBean) {
        mAmazonS3.upload(imageBean);
    }

    @Override
    public void onTextSend(String text, String textColor) {
        Log.e("onTextSend: ", text + " " + textColor);
        mText = text;
        mTextColor = textColor;
        hideKeyboard();
        mActivityCommentPreviewBinding.btnPostComment.setVisibility(View.GONE);
        mActivityCommentPreviewBinding.ivAddText.setVisibility(View.GONE);
        mActivityCommentPreviewBinding.tvDone.setVisibility(View.VISIBLE);
        mActivityCommentPreviewBinding.tvAddedText.setVisibility(View.VISIBLE);
        mActivityCommentPreviewBinding.tvAddedText.setText(text);
        mActivityCommentPreviewBinding.tvAddedText.setTextColor(Color.parseColor(textColor));
        xCoordinate = mActivityCommentPreviewBinding.tvAddedText.getX();
        yCoordinate = mActivityCommentPreviewBinding.tvAddedText.getY();
        mTextSize = mActivityCommentPreviewBinding.tvAddedText.getTextSize();
        if (mText.length() > 10) {
            zoomLimit = 2.0f;
        }
        defaultSize = mTextSize;
    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        View view = getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mExoplayer != null) {
            mExoplayer.setPlayWhenReady(false);
            mExoplayer.release();
            mExoplayer = null;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mExoplayer != null) {
            mExoplayer.setPlayWhenReady(false);
            mExoplayer.release();
            mExoplayer = null;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!isDonePressed) {
            mScaleGestureDetector.onTouchEvent(event);
            mRotationDetector.onTouchEvent(event);
        }
        return true;
    }

    public Bitmap loadBitmapFromView(View v) {
        Bitmap b = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return b;
    }

    private void addTextToImage() {
        Bitmap bitmap = loadBitmapFromView(mActivityCommentPreviewBinding.rlImageWithText);
        try {
            filePath = getStringPath();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, new FileOutputStream(new File(filePath)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private String getStringPath() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "image_" + System.currentTimeMillis() + ".jpeg";
    }

    private String getPngStringPath() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "image_" + System.currentTimeMillis() + ".png";
    }

    @Override
    public void OnRotation(RotationGestureDetector rotationDetector) {
        float angle = rotationDetector.getAngle();
        mActivityCommentPreviewBinding.tvAddedText.setRotation(360 - angle);
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            mScaleFactor *= scaleGestureDetector.getScaleFactor();
            mScaleFactor = Math.max(1.0f, Math.min(mScaleFactor, zoomLimit));
            mActivityCommentPreviewBinding.tvAddedText.setTextSize(TypedValue.COMPLEX_UNIT_PX, defaultSize * mScaleFactor);
            return true;
        }
    }
}
