package com.whizzly.home_feed_comments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.hardware.camera2.CameraAccessException;
import android.net.Uri;
import android.opengl.GLException;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.daasuu.camerarecorder.CameraRecordListener;
import com.daasuu.camerarecorder.CameraRecorder;
import com.daasuu.camerarecorder.CameraRecorderBuilder;
import com.daasuu.camerarecorder.LensFacing;
import com.giphy.sdk.core.models.Media;
import com.giphy.sdk.core.models.enums.RatingType;
import com.giphy.sdk.core.models.enums.RenditionType;
import com.giphy.sdk.ui.GPHContentType;
import com.giphy.sdk.ui.GPHSettings;
import com.giphy.sdk.ui.GiphyCoreUI;
import com.giphy.sdk.ui.themes.DarkTheme;
import com.giphy.sdk.ui.themes.GridType;
import com.giphy.sdk.ui.themes.LightTheme;
import com.giphy.sdk.ui.themes.Theme;
import com.giphy.sdk.ui.views.GiphyDialogFragment;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.camera_utils.widget.SampleGLView;
import com.whizzly.databinding.ActivityCameraCommentBinding;
import com.whizzly.dialog.ErrorDialog;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.feeds_response.Result;
import com.whizzly.models.post_comment.PostCommentBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;
import com.whizzly.utils.FFMpegCommandListener;
import com.whizzly.utils.FFMpegCommands;
import com.whizzly.utils.Glide4Engine;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.opengles.GL10;

import static com.giphy.sdk.ui.themes.GridType.*;


public class CameraCommentActivity extends BaseActivity implements OnClickSendListener {

    protected CameraRecorder cameraRecorder;
    protected LensFacing lensFacing = LensFacing.FRONT;
    private boolean mIsRecordingVideo;
    private String mNextVideoAbsolutePath;
    private boolean isFirstTime = false;
    private Timer timer = new Timer();
    private int recordingSecs = 0;
    private ActivityCameraCommentBinding mActivityCameraCommentBinding;
    private CameraCommentModel mCameraCommentModel;
    private Result mFeedData;
    private boolean longClickActive = false;
    private long startTime;
    private Handler mPictureHandler = new Handler();
    private boolean isRecordingStarted = false;
    private SampleGLView sampleGLView;
    private Runnable mPictureRunnable = new Runnable() {
        @Override
        public void run() {
            mNextVideoAbsolutePath = getVideoFilePath(CameraCommentActivity.this);
            cameraRecorder.start(mNextVideoAbsolutePath);
            new Handler().postDelayed(() -> {
                cameraRecorder.stop();
                Intent intent = new Intent(CameraCommentActivity.this, CommentPreviewActivity.class);
                intent.putExtra(AppConstants.ClassConstants.COMMENT_FILE_PATH, mNextVideoAbsolutePath);
                intent.putExtra(AppConstants.ClassConstants.COMMENT_TYPE, AppConstants.ClassConstants.COMMENT_TYPE_IMAGE);
                intent.putExtra(AppConstants.ClassConstants.FEEDS_DATA, mFeedData);
                startActivity(intent);
                finish();
            }, 500);
        }
    };

    private void toggleFlash(){
        if (mCameraCommentModel.getIsFlashClicked()) {
            if (cameraRecorder != null && cameraRecorder.isFlashSupport()) {
                cameraRecorder.switchFlashMode();
                cameraRecorder.changeAutoFocus();
            }
            mCameraCommentModel.setIsFlashClicked(false);
        } else {
            if (cameraRecorder != null && cameraRecorder.isFlashSupport()) {
                cameraRecorder.switchFlashMode();
                cameraRecorder.changeAutoFocus();
            }
            mCameraCommentModel.setIsFlashClicked(true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (checkPermissionsForCamera(this)) {
            setUpCamera();
        }
    }

    private void releaseCamera() {
        if (sampleGLView != null) {
            sampleGLView.onPause();
        }

        if (cameraRecorder != null) {
            cameraRecorder.stop();
            cameraRecorder.release();
            cameraRecorder = null;
        }

        if (sampleGLView != null) {
            mActivityCameraCommentBinding.camera.removeView(sampleGLView);
            sampleGLView = null;
        }
    }

    private void setUpCameraView() {
        runOnUiThread(() -> {
            FrameLayout frameLayout = mActivityCameraCommentBinding.camera;
            frameLayout.removeAllViews();
            sampleGLView = null;
            sampleGLView = new SampleGLView(getApplicationContext());
            sampleGLView.setTouchListener((event, width, height) -> {
                if (cameraRecorder == null) return;
                cameraRecorder.changeManualFocusPoint(event.getX(), event.getY(), width, height);
            });
            frameLayout.addView(sampleGLView);
        });
    }

    private void setUpCamera() {
        setUpCameraView();
        setCamera();
        cameraRecorder = new CameraRecorderBuilder(this, sampleGLView)
                .cameraRecordListener(new CameraRecordListener() {
                    @Override
                    public void onGetFlashSupport(boolean flashSupport) {

                    }

                    @Override
                    public void onRecordComplete() {
                        mIsRecordingVideo = false;
                        mCameraCommentModel.setIsVideoStarted(true);
                        mCameraCommentModel.setIsRecordPressed(false);
                    }

                    @Override
                    public void onRecordStart() {

                    }

                    @Override
                    public void onError(Exception exception) {
                        Log.e("CameraRecorder", exception.toString());
                    }

                    @Override
                    public void onCameraThreadFinish() {
                        runOnUiThread(() -> setUpCamera());
                    }
                })
                .cameraSize(1280, 720)
                .lensFacing(lensFacing)
                .build();
    }

    @Override
    public void onPause() {
        super.onPause();
        releaseCamera();
    }

    private void setCamera() {
        mNextVideoAbsolutePath = getVideoFilePath(this);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setOnTouchListener() {
        try {
            mActivityCameraCommentBinding.ivPushToRecord.setOnTouchListener((View view, MotionEvent motionEvent) -> {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        if (longClickActive) {
                            long clickDuration = Calendar.getInstance().getTimeInMillis() - startTime;
                            if (clickDuration > 600) {
                                longClickActive = false;
                                if (!mIsRecordingVideo && recordingSecs < 7) {
                                    mPictureHandler.removeCallbacks(mPictureRunnable);
                                    mCameraCommentModel.setIsRecordPressed(true);
                                    mCameraCommentModel.setIsVideoStarted(true);
                                    mNextVideoAbsolutePath = getVideoFilePath(this);
                                    mActivityCameraCommentBinding.ivSaveRecording.setVisibility(View.INVISIBLE);
                                    cameraRecorder.start(mNextVideoAbsolutePath);
                                    if (!isRecordingStarted)
                                        startProgress();
                                    mIsRecordingVideo = true;
                                    isRecordingStarted = true;
                                }
                            }
                        }
                        break;
                    case MotionEvent.ACTION_DOWN:
                        longClickActive = true;
                        startTime = Calendar.getInstance().getTimeInMillis();
                        mIsRecordingVideo = false;
                        break;
                    case MotionEvent.ACTION_UP:
                        if (!longClickActive) {
                            if (mIsRecordingVideo) {
                                cameraRecorder.stop();
                                mCameraCommentModel.setIsVideoStarted(false);
                                mCameraCommentModel.setIsRecordPressed(false);
                                mActivityCameraCommentBinding.ivSaveRecording.setVisibility(View.VISIBLE);
                                mIsRecordingVideo = false;
                            }
                        } else {
                            if (!isRecordingStarted)
                                mPictureHandler.postDelayed(mPictureRunnable, 2000);
                        }
                        break;
                }
                return false;
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getPngStringPath() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "image_" + System.currentTimeMillis() + ".png";
    }

    public void switchCamera() {
        releaseCamera();
        if (lensFacing == LensFacing.BACK) {
            lensFacing = LensFacing.FRONT;
        } else {
            lensFacing = LensFacing.BACK;
        }
    }

    private void startProgress() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (mIsRecordingVideo) {
                    runOnUiThread(() -> {
                        if (mIsRecordingVideo) {
                            mActivityCameraCommentBinding.tvRecordingTime.setText(String.format("%d s", recordingSecs));
                            recordingSecs++;

                        }
                        if (recordingSecs == 8) {
                            cameraRecorder.stop();
                            mCameraCommentModel.setIsVideoStarted(false);
                            mCameraCommentModel.setIsRecordPressed(false);
                            mIsRecordingVideo = false;
                            mActivityCameraCommentBinding.ivSaveRecording.setVisibility(View.VISIBLE);
                        }
                    });
                }
            }
        }, 1000, 1000);
    }

    public boolean checkPermissionsForCamera(Activity context) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.CAMERA)
                            || ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.RECORD_AUDIO)) {
                        ActivityCompat.requestPermissions((context), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO}, AppConstants.ClassConstants.VIDEO_INTENT_REQUEST_CODE);
                    } else {
                        if (!isFirstTime) {
                            ActivityCompat.requestPermissions((context), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO}, AppConstants.ClassConstants.VIDEO_INTENT_REQUEST_CODE);
                            isFirstTime = true;
                        } else {
                            ErrorDialog errorDialog = new ErrorDialog();
                            if (context instanceof CameraCommentActivity) {
                                errorDialog.show(((CameraCommentActivity) context).getSupportFragmentManager(), ErrorDialog.class.getCanonicalName());
                            }
                        }
                    }
                return false;
            } else
                return true;
        } else
            return true;
    }

    private String getVideoFilePath(Context context) {
        final File dir = context.getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "comment_video_" + System.currentTimeMillis() + ".mp4";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        clearStatusBar();
        mActivityCameraCommentBinding = DataBindingUtil.setContentView(this, R.layout.activity_camera_comment);
        CameraCommentViewModel mCameraCommentViewModel = ViewModelProviders.of(this).get(CameraCommentViewModel.class);
        mCameraCommentModel = new CameraCommentModel();
        FFMpegCommands ffMpegCommands = FFMpegCommands.getInstance(this);
        try {
            ffMpegCommands.initializeFFmpeg();
        } catch (FFmpegNotSupportedException e) {
            e.printStackTrace();
        }
        GiphyCoreUI.INSTANCE.configure(this, "JmOliKtWWO5aK46QW13O6YJM26pIoaIN", true);
        mActivityCameraCommentBinding.setViewModel(mCameraCommentViewModel);
        mActivityCameraCommentBinding.setModel(mCameraCommentModel);
        mCameraCommentViewModel.setOnClickSendListener(this);
        getIntentData();
        setOnTouchListener();
        setCamera();
        setClickListener();
    }

    private void setClickListener() {
        mActivityCameraCommentBinding.ivGif.setOnClickListener(view -> {
           /* GIFFragment gifFragment = new GIFFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(AppConstants.ClassConstants.FEEDS_DATA, mFeedData);
            gifFragment.setArguments(bundle);
            gifFragment.show(getSupportFragmentManager(), gifFragment.getTag());*/
           new Handler().post(new Runnable() {
               @Override
               public void run() {

                   GPHSettings settings = new GPHSettings(waterfall, DarkTheme.INSTANCE, new GPHContentType[]{GPHContentType.gif}, true, true, false, RatingType.pg13, RenditionType.fixedWidth, RenditionType.looping);
                   GiphyDialogFragment giphyDialogFragment = GiphyDialogFragment.Companion.newInstance(settings);
                   giphyDialogFragment.setGifSelectionListener(new GiphyDialogFragment.GifSelectionListener() {
                       @Override
                       public void onGifSelected(@NotNull Media media) {
                           hitCommentApi(media.getImages().getOriginal().getMp4Url());
                           giphyDialogFragment.dismiss();
                       }

                       @Override
                       public void onDismissed() {

                       }
                   });
                   giphyDialogFragment.show(getSupportFragmentManager(), "giphy_dialog");
               }
           });
        });
    }

    private void hitCommentApi(String gifUrl) {
        HashMap<String, String> commentPostRequest = new HashMap<>();
        commentPostRequest.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        commentPostRequest.put(AppConstants.NetworkConstants.VIDEO_ID, String.valueOf(mFeedData.getId()));
        commentPostRequest.put(AppConstants.NetworkConstants.VIDEO_TYPE, mFeedData.getType());
        commentPostRequest.put(AppConstants.NetworkConstants.COMMENT_TYPE, AppConstants.NetworkConstants.COMMENT_TYPE_VIDEO);
        commentPostRequest.put(AppConstants.NetworkConstants.COMMENT_URL, gifUrl);
        DataManager.getInstance().postComment(commentPostRequest).enqueue(new NetworkCallback<PostCommentBean>() {
            @Override
            public void onSuccess(PostCommentBean postCommentBean) {
                finish();
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {

            }

            @Override
            public void onError(Throwable t) {

            }
        });
    }


    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_COMMENT_FLASH_CLICKED:
                toggleFlash();
                break;
            case AppConstants.ClassConstants.ON_COMMENT_FLIP_CAMERA:
                switchCamera();
                break;
            case AppConstants.ClassConstants.ON_CLOSE_VIDEO_RECORDING:
                finish();
                break;
            case AppConstants.ClassConstants.ON_COMMENT_SAVE_RECORDING:
                if (mActivityCameraCommentBinding.tvRecordingTime.getText().toString().length() > 0) {
                    String time = mActivityCameraCommentBinding.tvRecordingTime.getText().toString().split(" ")[0];
                    if (Integer.parseInt(time) > 0) {
                        Intent intent = new Intent(this, CommentPreviewActivity.class);
                        intent.putExtra(AppConstants.ClassConstants.COMMENT_FILE_PATH, mNextVideoAbsolutePath);
                        intent.putExtra(AppConstants.ClassConstants.COMMENT_TYPE, AppConstants.ClassConstants.COMMENT_TYPE_VIDEO);
                        intent.putExtra(AppConstants.ClassConstants.FEEDS_DATA, mFeedData);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(this, getString(R.string.txt_error_min_comment_length), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getString(R.string.txt_error_min_comment_length), Toast.LENGTH_SHORT).show();
                }
                break;
            case AppConstants.ClassConstants.ON_COMMENT_GALLERY_CLICKED:
                getVideoOrImageFromGallery();
                break;
            case AppConstants.ClassConstants.ON_COMMENT_ADD_TEXT_CLICKED:
                Intent intent = new Intent(this, AddTextImageActivity.class);
                intent.putExtra(AppConstants.ClassConstants.FEEDS_DATA, mFeedData);
                startActivity(intent);
                finish();
                break;
        }
    }

    private void getIntentData() {
        mFeedData = getIntent().getParcelableExtra(AppConstants.ClassConstants.FEEDS_DATA);
    }

    private void getVideoOrImageFromGallery() {
        int size = getWindowManager().getDefaultDisplay().getWidth();
        Matisse.from(this)
                .choose(MimeType.ofAll())
                .countable(false)
                .maxSelectable(1)
                .gridExpectedSize(size / 3)
                .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                .thumbnailScale(0.85f)
                .imageEngine(new Glide4Engine())
                .forResult(AppConstants.ClassConstants.REQUEST_FROM_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstants.ClassConstants.REQUEST_FROM_GALLERY && resultCode == RESULT_OK) {
            List<Uri> uris = Matisse.obtainResult(data);
            List<String> filePaths = Matisse.obtainPathResult(data);
            Uri mediaUri = uris.get(0);
            String filePath = filePaths.get(0);
            String mimeType = getContentResolver().getType(mediaUri);
            performActionOnBasisOfFileType(filePath, mimeType);
        }
    }

    private void performActionOnBasisOfFileType(String filePath, String fileType) {
        if (fileType.contains("image")) {
            Intent intent = new Intent(this, CommentPreviewActivity.class);
            intent.putExtra(AppConstants.ClassConstants.COMMENT_FILE_PATH, filePath);
            intent.putExtra(AppConstants.ClassConstants.COMMENT_TYPE, AppConstants.ClassConstants.COMMENT_TYPE_IMAGE);
            intent.putExtra(AppConstants.ClassConstants.FEEDS_DATA, mFeedData);
            startActivity(intent);
            finish();
        } else if (fileType.contains("video")) {
            Intent intent = new Intent(this, VideoTrimmerActivity.class);
            intent.putExtra(AppConstants.ClassConstants.COMMENT_FILE_PATH, filePath);
            intent.putExtra(AppConstants.ClassConstants.COMMENT_TYPE, AppConstants.ClassConstants.COMMENT_TYPE_IMAGE);
            intent.putExtra(AppConstants.ClassConstants.FEEDS_DATA, mFeedData);
            startActivity(intent);
            finish();
        }
    }

    private interface BitmapReadyCallbacks {
        void onBitmapReady(Bitmap bitmap);
    }
}
