package com.whizzly.home_feed_comments;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.post_comment.PostCommentBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class AddTextImageViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;
    private RichMediatorLiveData<PostCommentBean> mRichMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;

    public RichMediatorLiveData<PostCommentBean> getmRichMediatorLiveData() {
        return mRichMediatorLiveData;
    }

    public void setGenericObservers(Observer<FailureResponse> failureResponseObserver, Observer<Throwable> errorObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        if (mRichMediatorLiveData == null) {
            mRichMediatorLiveData = new RichMediatorLiveData<PostCommentBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
    }


    public void onDoneClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_ADD_TEXT_DONE_CLICKED);
    }

    public void saveRecording() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_COMMENT_SAVE_RECORDING);
    }

    public void closePreview() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.PREVIEW_VIDEO_ON_CLOSE_CLICKED);
    }

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void hitPostCommentApi(HashMap<String, String> postCommentRequest) {
        DataManager.getInstance().postComment(postCommentRequest).enqueue(new NetworkCallback<PostCommentBean>() {
            @Override
            public void onSuccess(PostCommentBean postCommentBean) {
                mRichMediatorLiveData.setValue(postCommentBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                mRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                mRichMediatorLiveData.setError(t);
            }
        });
    }
}
