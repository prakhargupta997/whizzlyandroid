package com.whizzly.home_feed_comments;

import androidx.lifecycle.ViewModel;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class FeedsCommentPreviewViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void onOpenProfile() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_COMMENT_USER_PROFILE_CLICKED);
    }
}
