package com.whizzly.home_feed_comments;

import androidx.lifecycle.ViewModel;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class AddTextViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void onDoneClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_ADD_TEXT_DONE_CLICKED);
    }

}
