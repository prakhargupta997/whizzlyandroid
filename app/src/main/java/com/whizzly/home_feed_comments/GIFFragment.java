package com.whizzly.home_feed_comments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.whizzly.R;
import com.whizzly.databinding.FragmentGifBinding;
import com.whizzly.interfaces.OnRecyclerViewClickListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.feeds_response.Result;
import com.whizzly.models.gifModels.Datum;
import com.whizzly.models.gifModels.GIFModel;
import com.whizzly.models.post_comment.PostCommentBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class GIFFragment extends BottomSheetDialogFragment implements OnRecyclerViewClickListener {

    private Result mFeedData;
    private FragmentGifBinding mFragmentGifBinding;
    private ArrayList<Datum> mGifModels = new ArrayList<>();
    private String gifUrl;
    private int lastPosition = -1;


    public GIFFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getData();
    }

    private void getData() {
        mFeedData = getArguments().getParcelable(AppConstants.ClassConstants.FEEDS_DATA);
    }

    private void setRecyclerView() {
        StaggeredGridLayoutManager gridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mFragmentGifBinding.rvGifView.setLayoutManager(gridLayoutManager);
        GifAdapter gifAdapter = new GifAdapter(mGifModels, this, getActivity());
        mFragmentGifBinding.rvGifView.setAdapter(gifAdapter);
    }

    private void setClickListener() {
        mFragmentGifBinding.tvSelect.setOnClickListener(view -> {
            if (!gifUrl.isEmpty()) {
                hitCommentApi();
                dismiss();
                getActivity().finish();
            } else {
                Toast.makeText(getActivity(), "Please select a gif to post as comment.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFragmentGifBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_gif, container, false);
        hitGiphyApi();
        setRecyclerView();
        setClickListener();
        return mFragmentGifBinding.getRoot();
    }

    private void hitGiphyApi() {
        HashMap<String, String> stringStringHashMap = new HashMap<>();
        stringStringHashMap.put("api_key", "HCx8nBOG9CcukG4arnjU5V9lZzEFg9rb");
        stringStringHashMap.put("limit", "20");
        stringStringHashMap.put("rating", "G");
        DataManager.getInstance().getGifList(stringStringHashMap).enqueue(new NetworkCallback<GIFModel>() {
            @Override
            public void onSuccess(GIFModel gifModel) {
                mGifModels.clear();
                mGifModels.addAll(gifModel.getData());
                Objects.requireNonNull(mFragmentGifBinding.rvGifView.getAdapter()).notifyDataSetChanged();
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {

            }

            @Override
            public void onError(Throwable t) {

            }
        });
    }

    private void hitCommentApi() {
        HashMap<String, String> commentPostRequest = new HashMap<>();
        commentPostRequest.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        commentPostRequest.put(AppConstants.NetworkConstants.VIDEO_ID, String.valueOf(mFeedData.getId()));
        commentPostRequest.put(AppConstants.NetworkConstants.VIDEO_TYPE, mFeedData.getType());
        commentPostRequest.put(AppConstants.NetworkConstants.COMMENT_TYPE, AppConstants.NetworkConstants.COMMENT_TYPE_VIDEO);
        commentPostRequest.put(AppConstants.NetworkConstants.COMMENT_URL, gifUrl);
        DataManager.getInstance().postComment(commentPostRequest).enqueue(new NetworkCallback<PostCommentBean>() {
            @Override
            public void onSuccess(PostCommentBean postCommentBean) {

            }

            @Override
            public void onFailure(FailureResponse failureResponse) {

            }

            @Override
            public void onError(Throwable t) {

            }
        });
    }

    @Override
    public void onRecyclerViewClicked(Object o, int pos) {
        if (o instanceof Datum) {
            gifUrl = ((Datum) o).getImages().getOriginal().getMp4();
            if (!gifUrl.isEmpty()) {
                hitCommentApi();
                dismiss();
                getActivity().finish();
            } else {
                Toast.makeText(getActivity(), "Please select a gif to post as comment.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
