package com.whizzly.home_feed_comments;

public interface OnSendTextListener {
    public void onTextSend(String text, String textColor);
}
