package com.whizzly.home_feed_comments;

import androidx.lifecycle.ViewModel;
import androidx.appcompat.widget.AppCompatSeekBar;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class VideoTrimmerViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void saveRecording() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.VIDEO_TRIMMER_ON_SAVE_RECORDING);
    }

    public void onCloseRecording() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.VIDEO_TRIMMER_ON_CLOSE_RECORDING);
    }

    public void onPlayPauseVideo(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.VIDEO_TRIMMER_ON_PLAY_PAUSE);
    }
}
