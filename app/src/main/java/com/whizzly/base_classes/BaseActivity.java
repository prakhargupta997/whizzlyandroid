package com.whizzly.base_classes;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.jaychang.sa.facebook.SimpleAuth;
import com.whizzly.R;
import com.whizzly.interfaces.OnBackPressedListener;
import com.whizzly.login_signup_module.login_screen.LoginActivity;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.change_password.ChangePasswordModel;
import com.whizzly.models.privacy_status.PrivacyStatusResponseBean;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.Locale;
import java.util.Objects;

public class BaseActivity extends AppCompatActivity {

    public boolean isBackPressed = false;
    private Observer<Throwable> errorObserver;
    private Observer<FailureResponse> failureResponseObserver;
    private FragmentManager mFragmentManager;
    private Observer<ChangePasswordModel> mDeleteObserver;
    private Observer<PrivacyStatusResponseBean> mSavedVideoObserver;
    private OnBackPressedListener onBackPressedListener;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String languageToLoad = DataManager.getInstance().getLanguage();
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        mFragmentManager = getSupportFragmentManager();
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            this.getWindow().getAttributes().layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        clearStatusBar();
        observeDeleteLiveData();
        observeSavedVideoLiveData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isBackPressed = false;
    }

    public void clearStatusBar() {
        Window w = getWindow();
        w.getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View view = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);
        if (view instanceof EditText) {
            View w = getCurrentFocus();
            int[] scrcoords = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            if (event.getAction() == MotionEvent.ACTION_UP
                    && (x < w.getLeft() || x >= w.getRight()
                    || y < w.getTop() || y > w.getBottom())) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
            }
        }
        return ret;
    }

    public void manageKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            Objects.requireNonNull(imm).hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void autoLogOut() {
        switch (DataManager.getInstance().getLoginOrSignUpTyp()) {
            case 2:
                SimpleAuth.disconnectFacebook();
                break;
            case 3:
                GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .build();
                GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
                mGoogleSignInClient.signOut();
                break;
            case 4:
                com.jaychang.sa.instagram.SimpleAuth.disconnectInstagram();
                break;
        }
        DataManager.getInstance().clearPrefernces();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(AppConstants.ClassConstants.ON_LOGOUT_CLICKED, "YES");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

    }

    public Observer<Throwable> getErrorObserver() {
        return errorObserver;
    }

    public Observer<FailureResponse> getFailureResponseObserver() {
        return failureResponseObserver;
    }

    public void removeFramgentToBottom(int viewId) {
        Fragment fragment = mFragmentManager.findFragmentById(viewId);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_down);
        if (fragment != null) {
            ft.remove(fragment).commit();
        }
    }

    public void replaceFragmentFromBottom(int viewId, Fragment fragment) {
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_down);
        ft.replace(viewId, fragment).commit();
    }

    public void addFragmentFromBottom(int viewId, Fragment fragment, String tag) {
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_down);
        ft.add(viewId, fragment).addToBackStack(tag).commit();
    }

    public void addFragment(int viewId, Fragment fragment, String tag) {
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        ft.replace(viewId, fragment).commit();
    }

    public void addFragmentFromRight(int viewId, Fragment fragment) {
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        ft.setCustomAnimations(R.anim.anim_right_to_left, R.anim.anim_left_to_right);
        ft.replace(viewId, fragment).addToBackStack(fragment.getClass().getCanonicalName()).commitAllowingStateLoss();
    }

    public void removeFragmentFromLeft(int viewId) {
        Fragment fragment = mFragmentManager.findFragmentById(viewId);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.anim_right_to_left, R.anim.anim_left_to_right);
        if (fragment != null) {
            ft.remove(fragment).commit();
        }
    }

    public Fragment getFragment(int viewId) {
        Fragment fragment = mFragmentManager.findFragmentById(viewId);
        return fragment;
    }

    public void addFragmentWithBackstack(int layoutResId, Fragment fragment, String tag) {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.anim_right_to_left, R.anim.anim_left_to_right)
                .add(layoutResId, fragment, tag)
                .addToBackStack(tag)
                .commitAllowingStateLoss();
    }

    public Observer<ChangePasswordModel> getDeleteObserver() {
        return mDeleteObserver;
    }

    public Observer<PrivacyStatusResponseBean> geSavedFeedsObserver() {
        return mSavedVideoObserver;
    }

    private void observeSavedVideoLiveData() {
        mSavedVideoObserver = privacyStatusResponseBean -> {
            if (privacyStatusResponseBean != null) {
                if (privacyStatusResponseBean.getCode() == 200) {
                    Toast.makeText(BaseActivity.this, privacyStatusResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (privacyStatusResponseBean.getCode() == 301 || privacyStatusResponseBean.getCode() == 302) {
                    Toast.makeText(BaseActivity.this, privacyStatusResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                    autoLogOut();
                } else
                    Toast.makeText(BaseActivity.this, BaseActivity.this.getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();

            }

        };
    }

    private void observeDeleteLiveData() {
        mDeleteObserver = deleteResponseModel -> {
            if (deleteResponseModel != null) {
                if (deleteResponseModel.getCode().equals("200")) {
                    Toast.makeText(BaseActivity.this, deleteResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (deleteResponseModel.getCode().equals("301") || deleteResponseModel.getCode().equals("302")) {
                    Toast.makeText(BaseActivity.this, deleteResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    autoLogOut();
                } else
                    Toast.makeText(BaseActivity.this, BaseActivity.this.getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();

            }

        };
    }

    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }

    @Override
    public void onBackPressed() {
        isBackPressed = true;
        if (onBackPressedListener != null) {
            onBackPressedListener.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        onBackPressedListener = null;
        super.onDestroy();
    }
}
