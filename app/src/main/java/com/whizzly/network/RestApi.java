package com.whizzly.network;

import androidx.annotation.NonNull;
import androidx.core.graphics.TypefaceCompatApi26Impl;

import com.whizzly.BuildConfig;
import com.whizzly.models.blockedUsers.BlockedUsersResponseBean;
import com.whizzly.models.change_password.ChangePasswordModel;
import com.whizzly.models.collab.collab_videos.CollabVideosResponseBean;
import com.whizzly.models.collab.collab_videos.allvideos.CategoryVideosResponse;
import com.whizzly.models.comment_delete.CommentDeleteResponse;
import com.whizzly.models.comment_list.CommentListingBean;
import com.whizzly.models.feeds_response.FeedsResponse;
import com.whizzly.models.followUnfollow.FollowUnfollowResponseBean;
import com.whizzly.models.followers_following_response.FollowersResponseBean;
import com.whizzly.models.forgot_password.ForgotPasswordBean;
import com.whizzly.models.forgot_password.ResetPasswordBean;
import com.whizzly.models.forgot_password.ValidateOTPBean;
import com.whizzly.models.gifModels.GIFModel;
import com.whizzly.models.like_dislike_response.LikeDislikeResponse;
import com.whizzly.models.likers_list_response.LikersListResponse;
import com.whizzly.models.login_response.LoginResponseBean;
import com.whizzly.models.music_list_response.GetMusicResponseBean;
import com.whizzly.models.notifications.NotificationResponse;
import com.whizzly.models.post.VideoPostResponse;
import com.whizzly.models.post_comment.PostCommentBean;
import com.whizzly.models.privacy_status.PrivacyStatusResponseBean;
import com.whizzly.models.profile.ProfileResponseBean;
import com.whizzly.models.profileVideo.ProfileVideosResponseBean;
import com.whizzly.models.report.ReportResponse;
import com.whizzly.models.reuse_response.ReuseResponse;
import com.whizzly.models.reuse_video.ReuseVideoBean;
import com.whizzly.models.reuse_videos_chart.ReuseVideoChartResponse;
import com.whizzly.models.search.users.UserSearchResponseBean;
import com.whizzly.models.search.videos.VideoSearchResponseBean;
import com.whizzly.models.sign_up_beans.SignUpBean;
import com.whizzly.models.social_login_response.SocialLoginResponseBean;
import com.whizzly.models.social_signup_response.SocialSignupResponseBean;
import com.whizzly.models.submit_profile.SubmitProfileBean;
import com.whizzly.models.suggested_videos_info.SuggestedVideoInfoResponse;
import com.whizzly.models.validate_user_bean.ValidateUserBean;
import com.whizzly.models.video_details.VideoDetailsResponse;
import com.whizzly.models.video_list_response.VideoListResponseBean;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * This class is used to handling operation for Service
 */
public class RestApi {
    private static OkHttpClient.Builder httpClient;
    private static Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
            .client(getClient())
            .baseUrl(AppConstants.NetworkConstants.API_BASE_URL_DEV).addConverterFactory(GsonConverterFactory.create());
    private static RestApi instance = new RestApi();

    private static Retrofit.Builder giphyBuilder  = new Retrofit.Builder().client(getClient()).baseUrl("http://api.giphy.com/").addConverterFactory(GsonConverterFactory.create());

    private static OkHttpClient getClient() {
        httpClient = new OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(logging);

        }

        return httpClient.build();
    }

    public static RestApi getInstance() {
        if (instance == null) {
            instance = new RestApi();
        }
        return instance;
    }

    public static <S> S createService(Class<S> aClass) {
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Api-Key", "2994583b1183157b95131e9f656129e4")
                        .header("Authorization", "Basic eC03OWJiOTBkNS0yYmU5LTRkYjUtOWQwZi0xZmY2MDBhMTRlZDY6eC04OTk0NzM5OS04MWQ2LTRkOGMtOTc5NC1lNGU2ZGUyOTBjMDY=")
                        .method(original.method(), original.body());
                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        Retrofit retrofit = retrofitBuilder.client(httpClient.build()).build();
        return retrofit.create(aClass);
    }

    public static <S> S createServiceAfterLogin(Class<S> aClass) {
        try {
            final String accessToken = DataManager.getInstance().getAccessToken();
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(@NonNull Chain chain) throws IOException {
                    Request original = chain.request();
                    Request.Builder requestBuilder = original.newBuilder()
                            .header(AppConstants.NetworkConstants.ACCESS_TOKEN, accessToken)
                            .header("Authorization", "Basic eC03OWJiOTBkNS0yYmU5LTRkYjUtOWQwZi0xZmY2MDBhMTRlZDY6eC04OTk0NzM5OS04MWQ2LTRkOGMtOTc5NC1lNGU2ZGUyOTBjMDY=")
                            .method(original.method(), original.body());
                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });

            Retrofit retrofit = retrofitBuilder.client(httpClient.build()).build();
            return retrofit.create(aClass);
        } catch (Exception e) {

        }
        return null;
    }

    private static <S> S createGiphyService(Class<S> aClass){
        try {
            httpClient.addInterceptor(chain -> {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder()
                        .method(original.method(), original.body());
                Request request = requestBuilder.build();
                return chain.proceed(request);
            });
            Retrofit retrofit = giphyBuilder.client(httpClient.build()).build();
            return retrofit.create(aClass);
        } catch (Exception ignored) {

        }
        return null;
    }


    public static RequestBody getRequestBody(String params) {
        return RequestBody.create(MediaType.parse("text/plain"), params.getBytes());
    }

    //This method is used to create the multipart file data
    public static MultipartBody.Part prepareFilePart(String partName, File file) {

        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*; boundary=----" + System.currentTimeMillis()), file);
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    public Call<ValidateUserBean> hitValidateUsername(String user, String language) {
        return createService(ApiInterface.class).userValidation(user, language);
    }

    public Call<SignUpBean> hitNormalSignUp(HashMap<String, Object> signUpDetails) {
        return createService(ApiInterface.class).signUp(signUpDetails);
    }

    public Call<LoginResponseBean> hitLoginApi(HashMap<String, Object> loginDetails) {
        return createService(ApiInterface.class).login(loginDetails);
    }

    public Call<SocialLoginResponseBean> hitSocialLoginApi(HashMap<String, Object> loginDetails) {
        return createService(ApiInterface.class).socialLogin(loginDetails);
    }

    public Call<ForgotPasswordBean> hitForgotPassword(String email, String language) {
        return createService(ApiInterface.class).forgotPassword(email, language);
    }

    public Call<SubmitProfileBean> hitSubmitProfile(HashMap<String, String> data) {
        return Objects.requireNonNull(createServiceAfterLogin(ApiInterface.class)).setUpProfile(data);
    }

    public Call<ValidateOTPBean> hitValidateOtp(HashMap<String, Object> enterOtp) {
        return createService(ApiInterface.class).validateOTP(enterOtp);
    }

    public Call<ResetPasswordBean> hitResetPassword(HashMap<String, Object> resetPassMap) {
        return createService(ApiInterface.class).resetPassword(resetPassMap);
    }

    public Call<SocialSignupResponseBean> hitSocialSignupApi(HashMap<String, Object> socialDetails) {
        return createService(ApiInterface.class).socialSignUp(socialDetails);
    }

    public Call<GetMusicResponseBean> hitGetMusicList(HashMap<String, Object> musicParams) {
        return Objects.requireNonNull(createServiceAfterLogin(ApiInterface.class)).hitGetMusicList(musicParams);
    }

    public Call<VideoListResponseBean> hitGetVideoList(String suggestedVideoParams, String userId, String language) {
        return Objects.requireNonNull(createServiceAfterLogin(ApiInterface.class)).hitGetVideoList(suggestedVideoParams, userId, language);
    }

    public Call<VideoPostResponse> hitPostVideo(RequestBody postVideoBean, String userId) {
        return Objects.requireNonNull(createServiceAfterLogin(ApiInterface.class)).hitPostVideo(postVideoBean);
    }

    public Call<ProfileResponseBean> getProfile(String myUserId, String otherUserId, String language) {
        return Objects.requireNonNull(createServiceAfterLogin(ApiInterface.class)).getProfile(myUserId, otherUserId, language);
    }

    public Call<ProfileVideosResponseBean> getProfileVideos(HashMap<String, String> params) {
        return Objects.requireNonNull(createServiceAfterLogin(ApiInterface.class)).getProfileVideos(params);
    }

    public Call<BlockedUsersResponseBean> getBlockedUsersList(HashMap<String, String> params) {
        return Objects.requireNonNull(createServiceAfterLogin(ApiInterface.class)).getBlockedUsersList(params);
    }

    public Call<BlockedUsersResponseBean> hitBlockUnblockUserApi(HashMap<String, String> params) {
        return Objects.requireNonNull(createServiceAfterLogin(ApiInterface.class)).hitBlockUnblockUserApi(params);
    }

    public Call<FollowUnfollowResponseBean> hitFollowUnfollowApi(HashMap<String, String> params) {
        return Objects.requireNonNull(createServiceAfterLogin(ApiInterface.class)).hitFollowUnfollowApi(params);
    }


    public Call<VideoPostResponse> hitPostArgument(HashMap<String, String> videoDetails) {
        return Objects.requireNonNull(createServiceAfterLogin(ApiInterface.class)).hitPostArgument(videoDetails);
    }

    public Call<FeedsResponse> hitFeedsApi(HashMap<String, String> feedsQuery) {
        return Objects.requireNonNull(createServiceAfterLogin(ApiInterface.class)).hitFeedsApi(feedsQuery);
    }

    public Call<LikersListResponse> hitLikersListApi(HashMap<String, String> likersQuery) {
        return Objects.requireNonNull(createServiceAfterLogin(ApiInterface.class)).hitLikersListApi(likersQuery);
    }

    public Call<LikeDislikeResponse> hitLikeDislikeApi(HashMap<String, String> status) {
        return Objects.requireNonNull(createServiceAfterLogin(ApiInterface.class)).hitLikeDislikeApi(status);
    }

    public Call<ReuseResponse> hitReuseListApi(HashMap<String, String> reuseQuery) {
        return Objects.requireNonNull(createServiceAfterLogin(ApiInterface.class)).hitReuseListApi(reuseQuery);
    }

    public Call<ResetPasswordBean> hitLogOutApi(String userId, String language) {
        return Objects.requireNonNull(createServiceAfterLogin(ApiInterface.class)).hitLogOutApi(userId, language);
    }

    public Call<ReuseVideoBean> hitCollabInfoApi(HashMap<String, String> collabQuery) {
        return Objects.requireNonNull(createServiceAfterLogin(ApiInterface.class)).hitCollabInfoApi(collabQuery);
    }

    public Call<CommentListingBean> hitCommentListingApi(HashMap<String, String> commentQuery) {
        return Objects.requireNonNull(createServiceAfterLogin(ApiInterface.class)).hitCommentListingApi(commentQuery);
    }

    public Call<PostCommentBean> hitPostCommentApi(HashMap<String, String> postCommentRequest) {
        return Objects.requireNonNull(createServiceAfterLogin(ApiInterface.class)).hitPostCommentApi(postCommentRequest);
    }

    public Call<FollowersResponseBean> hitFollowingListingApi(HashMap<String, String> hashMap) {
        return createServiceAfterLogin(ApiInterface.class).hitFollowingListingApi(hashMap);
    }

    public Call<FollowersResponseBean> hitFollowersListing(HashMap<String, String> hashMap) {
        return createServiceAfterLogin(ApiInterface.class).hitFollowersListingApi(hashMap);
    }

    public Call<SubmitProfileBean> hitSupportCenterApi(HashMap<String, String> supportData) {
        return createServiceAfterLogin(ApiInterface.class).hitSupportCenterApi(supportData);
    }

    public Call<ChangePasswordModel> hitChangePasswordApi(HashMap<String, String> changePassQuery) {
        return createServiceAfterLogin(ApiInterface.class).changePasswordApi(changePassQuery);
    }

    public Call<com.whizzly.models.search.search_video.VideoSearchResponseBean> hitVideoSearchApi(HashMap<String, String> searchData) {
        return createServiceAfterLogin(ApiInterface.class).hitSearchVideoListApi(searchData);
    }

    public Call<UserSearchResponseBean> hitUserSearchApi(HashMap<String, String> searchData) {
        return createServiceAfterLogin(ApiInterface.class).hitSearchUserApi(searchData);
    }

    public Call<VideoDetailsResponse> hitVideoDetailsApi(HashMap<String, String> videoQuery) {
        return createServiceAfterLogin(ApiInterface.class).hitVideoDetailsApi(videoQuery);
    }

    public Call<VideoSearchResponseBean> hitExactSearchApi(HashMap<String, String> data) {
        return createServiceAfterLogin(ApiInterface.class).hitExactSearchApi(data);
    }

    public Call<CollabVideosResponseBean> hitCategoryRelatedVideosApi(String userId, String language) {
        return createServiceAfterLogin(ApiInterface.class).hitCategoryRelatedVideosApi(userId, language);
    }

    public Call<CategoryVideosResponse> hitViewMoreVideosApi(HashMap<String, String> data) {
        return createServiceAfterLogin(ApiInterface.class).hitViewMoreVideosApi(data);
    }

    public Call<ChangePasswordModel> hitEditFeedsApi(HashMap<String, String> videoData) {
        return createServiceAfterLogin(ApiInterface.class).hitEditFeedsApi(videoData);
    }

    public Call<ReportResponse> hitReportApi(HashMap<String, Object> reportData) {
        return createServiceAfterLogin(ApiInterface.class).hitReportApi(reportData);
    }

    public Call<ChangePasswordModel> hitDeletePostApi(HashMap<String, String> data) {
        return createServiceAfterLogin(ApiInterface.class).hitDeletePostApi(data);
    }

    public Call<PrivacyStatusResponseBean> hitPrivacyStatusApi(HashMap<String, String> dataForStatus) {
        return createServiceAfterLogin(ApiInterface.class).hitPrivacyStatusApi(dataForStatus);
    }

    public Call<ChangePasswordModel> hitChatNotificationApi(HashMap<String, String> chatData) {
        return createServiceAfterLogin(ApiInterface.class).hitChatNotificationApi(chatData);
    }

    public Call<NotificationResponse> hitNotificationListingApi(String userId, String language) {
        return createServiceAfterLogin(ApiInterface.class).hitNotificationListingApi(userId, language);
    }

    public Call<ReuseVideoChartResponse> hitReusedVideosChartApi(HashMap<String, String> data) {
        return createServiceAfterLogin(ApiInterface.class).hitReusedVideosChartApi(data);
    }

    public Call<PrivacyStatusResponseBean> hitSaveFeedVideo(HashMap<String, String> saveFeedData) {
        return createServiceAfterLogin(ApiInterface.class).hitSaveFeedVideoApi(saveFeedData);
    }

    public Call<SuggestedVideoInfoResponse> getSuggestedVideos(HashMap<String, String> suggestedVideoData) {
        return createServiceAfterLogin(ApiInterface.class).hitSuggestedVideosApi(suggestedVideoData);
    }

    public Call<CommentDeleteResponse> hitDeleteComment(HashMap<String, String> commentHashMap) {
        return createServiceAfterLogin(ApiInterface.class).hitCommentDeleteApi(commentHashMap);
    }

    public Call<GIFModel> getGifsList(HashMap<String, String> gifQuery){
        return createGiphyService(ApiInterface.class).hitGifList(gifQuery);
    }


}