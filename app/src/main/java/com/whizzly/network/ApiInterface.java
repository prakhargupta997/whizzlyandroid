package com.whizzly.network;

import com.whizzly.models.blockedUsers.BlockedUsersResponseBean;
import com.whizzly.models.change_password.ChangePasswordModel;
import com.whizzly.models.collab.collab_videos.CollabVideosResponseBean;
import com.whizzly.models.collab.collab_videos.allvideos.CategoryVideosResponse;
import com.whizzly.models.comment_delete.CommentDeleteResponse;
import com.whizzly.models.comment_list.CommentListingBean;
import com.whizzly.models.feeds_response.FeedsResponse;
import com.whizzly.models.followUnfollow.FollowUnfollowResponseBean;
import com.whizzly.models.followers_following_response.FollowersResponseBean;
import com.whizzly.models.forgot_password.ForgotPasswordBean;
import com.whizzly.models.forgot_password.ResetPasswordBean;
import com.whizzly.models.forgot_password.ValidateOTPBean;
import com.whizzly.models.gifModels.GIFModel;
import com.whizzly.models.like_dislike_response.LikeDislikeResponse;
import com.whizzly.models.likers_list_response.LikersListResponse;
import com.whizzly.models.login_response.LoginResponseBean;
import com.whizzly.models.music_list_response.GetMusicResponseBean;
import com.whizzly.models.notifications.NotificationResponse;
import com.whizzly.models.post.VideoPostResponse;
import com.whizzly.models.post_comment.PostCommentBean;
import com.whizzly.models.privacy_status.PrivacyStatusResponseBean;
import com.whizzly.models.profile.ProfileResponseBean;
import com.whizzly.models.profileVideo.ProfileVideosResponseBean;
import com.whizzly.models.report.ReportResponse;
import com.whizzly.models.reuse_response.ReuseResponse;
import com.whizzly.models.reuse_video.ReuseVideoBean;
import com.whizzly.models.reuse_videos_chart.ReuseVideoChartResponse;
import com.whizzly.models.search.users.UserSearchResponseBean;
import com.whizzly.models.search.videos.VideoSearchResponseBean;
import com.whizzly.models.sign_up_beans.SignUpBean;
import com.whizzly.models.social_login_response.SocialLoginResponseBean;
import com.whizzly.models.social_signup_response.SocialSignupResponseBean;
import com.whizzly.models.submit_profile.SubmitProfileBean;
import com.whizzly.models.suggested_videos_info.SuggestedVideoInfoResponse;
import com.whizzly.models.validate_user_bean.ValidateUserBean;
import com.whizzly.models.video_details.VideoDetailsResponse;
import com.whizzly.models.video_list_response.VideoListResponseBean;
import com.whizzly.utils.AppConstants;

import java.util.HashMap;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;


/**
 * ApiInterface.java
 * This class act as an interface between Retrofit and Classes used using Retrofit in Application
 *
 * @author Appinvetiv
 * @version 1.0
 * @since 1.0
 */

public interface ApiInterface {

    @FormUrlEncoded
    @POST(AppConstants.LOGIN)
    Call<LoginResponseBean> login(@FieldMap HashMap<String, Object> map);

    @FormUrlEncoded
    @POST(AppConstants.LOGIN)
    Call<SocialLoginResponseBean> socialLogin(@FieldMap HashMap<String, Object> map);

    @FormUrlEncoded
    @POST(AppConstants.SIGN_UP)
    Call<SignUpBean> signUp(@FieldMap HashMap<String, Object> map);

    @FormUrlEncoded
    @POST(AppConstants.SIGN_UP)
    Call<SocialSignupResponseBean> socialSignUp(@FieldMap HashMap<String, Object> socialDetails);

    @FormUrlEncoded
    @POST(AppConstants.VALIDATE_USER)
    Call<ValidateUserBean> userValidation(@Field(AppConstants.NetworkConstants.USER_NAME) String username, @Field(AppConstants.NetworkConstants.LANGUAGE) String language);

    @FormUrlEncoded
    @POST(AppConstants.SETUP_PROFILE)
    Call<SubmitProfileBean> setUpProfile(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @POST(AppConstants.FORGOT_PASSWORD)
    Call<ForgotPasswordBean> forgotPassword(@Field(AppConstants.NetworkConstants.FORGOT_PASSWORD_PARAM) String email, @Field(AppConstants.NetworkConstants.LANGUAGE) String language);

    @FormUrlEncoded
    @POST(AppConstants.NetworkConstants.VALIDATE_OTP)
    Call<ValidateOTPBean> validateOTP(@FieldMap HashMap<String, Object> code);

    @FormUrlEncoded
    @PUT(AppConstants.RESET_PASSWORD)
    Call<ResetPasswordBean> resetPassword(@FieldMap HashMap<String, Object> map);

    @GET(AppConstants.NetworkConstants.GET_MUSIC_LIST_API)
    Call<GetMusicResponseBean> hitGetMusicList(@QueryMap HashMap<String, Object> musicParams);


    @GET(AppConstants.NetworkConstants.SUGGESTED_VIDEO_URL)
    Call<VideoListResponseBean> hitGetVideoList(@Query(AppConstants.NetworkConstants.PARAM_SUGGESTED_VIDEO) String suggestedVideoParams, @Query(AppConstants.NetworkConstants.USER_ID) String userId, @Query(AppConstants.NetworkConstants.LANGUAGE) String language);

    @POST(AppConstants.NetworkConstants.COLLAB_URL)
    Call<VideoPostResponse> hitPostVideo(@Body RequestBody postVideoBean);

    @GET(AppConstants.NetworkConstants.GET_PROFILE)
    Call<ProfileResponseBean> getProfile(@Query(AppConstants.NetworkConstants.PARAM_USER_ID) String myUserId, @Query(AppConstants.NetworkConstants.PARAM_OTHER_USER_ID) String otherUserId, @Query(AppConstants.NetworkConstants.LANGUAGE) String language);

    @GET(AppConstants.NetworkConstants.GET_PROFILE_VIDEO)
    Call<ProfileVideosResponseBean> getProfileVideos(@QueryMap HashMap<String, String> params);

    @GET(AppConstants.NetworkConstants.GET_BLOCKED_USERS_LIST)
    Call<BlockedUsersResponseBean> getBlockedUsersList(@QueryMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST(AppConstants.NetworkConstants.GET_BLOCKED_USERS_LIST)
    Call<BlockedUsersResponseBean> hitBlockUnblockUserApi(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST(AppConstants.NetworkConstants.MANAGE_FOLLOW)
    Call<FollowUnfollowResponseBean> hitFollowUnfollowApi(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST(AppConstants.NetworkConstants.ARGUMENT_END_POINT)
    Call<VideoPostResponse> hitPostArgument(@FieldMap HashMap<String, String> videoDetails);

    @GET(AppConstants.NetworkConstants.GET_FEEDS)
    Call<FeedsResponse> hitFeedsApi(@QueryMap HashMap<String, String> feedsQuery);

    @GET(AppConstants.ClassConstants.LIKERS_LIST)
    Call<LikersListResponse> hitLikersListApi(@QueryMap HashMap<String, String> likersQuery);

    @FormUrlEncoded
    @POST(AppConstants.NetworkConstants.LIKE_DISLIKE)
    Call<LikeDislikeResponse> hitLikeDislikeApi(@FieldMap HashMap<String, String> status);

    @GET(AppConstants.NetworkConstants.REUSE_LIST)
    Call<ReuseResponse> hitReuseListApi(@QueryMap HashMap<String, String> reuseQuery);

    @FormUrlEncoded
    @POST(AppConstants.NetworkConstants.LOGOUT_END_POINT)
    Call<ResetPasswordBean> hitLogOutApi(@Field(AppConstants.NetworkConstants.USER_ID) String userId, @Field(AppConstants.NetworkConstants.LANGUAGE) String language);

    @GET(AppConstants.NetworkConstants.COLLAB_INFO_API)
    Call<ReuseVideoBean> hitCollabInfoApi(@QueryMap HashMap<String, String> collabQuery);

    @GET(AppConstants.NetworkConstants.COMMENT_LISTING_API)
    Call<CommentListingBean> hitCommentListingApi(@QueryMap HashMap<String, String> commentQuery);

    @FormUrlEncoded
    @POST(AppConstants.NetworkConstants.POST_COMMENT)
    Call<PostCommentBean> hitPostCommentApi(@FieldMap HashMap<String, String> postCommentRequest);

    @GET(AppConstants.NetworkConstants.FOLLOWING_LISTING_END_POINT)
    Call<FollowersResponseBean> hitFollowingListingApi(@QueryMap HashMap<String, String> hashMap);

    @GET(AppConstants.NetworkConstants.FOLLOWERS_LISTING_END_POINT)
    Call<FollowersResponseBean> hitFollowersListingApi(@QueryMap HashMap<String, String> hashMap);

    @FormUrlEncoded
    @POST(AppConstants.NetworkConstants.SUPPORT_CENTER_END_POINT)
    Call<SubmitProfileBean> hitSupportCenterApi(@FieldMap HashMap<String, String> supportData);

    @FormUrlEncoded
    @POST(AppConstants.NetworkConstants.CHANGE_PASSWORD_END_POINT)
    Call<ChangePasswordModel> changePasswordApi(@FieldMap HashMap<String, String> changePassQuery);

    @GET(AppConstants.NetworkConstants.VIDEO_SEARCH_END_POINT)
    Call<com.whizzly.models.search.search_video.VideoSearchResponseBean> hitSearchVideoListApi(@QueryMap HashMap<String, String> searchData);

    @GET(AppConstants.NetworkConstants.USER_SEARCH_END_POINT)
    Call<UserSearchResponseBean> hitSearchUserApi(@QueryMap HashMap<String, String> searchData);

    @GET(AppConstants.NetworkConstants.VIDEO_DETAILS_API_END_POINT)
    Call<VideoDetailsResponse> hitVideoDetailsApi(@QueryMap HashMap<String, String> videoQuery);

    @GET(AppConstants.NetworkConstants.EXACT_SEARCH_API_END_POINT)
    Call<VideoSearchResponseBean> hitExactSearchApi(@QueryMap HashMap<String, String> data);

    @GET(AppConstants.NetworkConstants.CATEGORIES_RELATED_VIDEOS_API_END_POINT)
    Call<CollabVideosResponseBean> hitCategoryRelatedVideosApi(@Query("user_id") String userId, @Query(AppConstants.NetworkConstants.LANGUAGE) String language);

    @GET(AppConstants.NetworkConstants.CATEGORY_ALL_VIDEOS_API_END_POINT)
    Call<CategoryVideosResponse> hitViewMoreVideosApi(@QueryMap HashMap<String, String> data);

    @FormUrlEncoded
    @POST(AppConstants.NetworkConstants.EDIT_FEEDS_API)
    Call<ChangePasswordModel> hitEditFeedsApi(@FieldMap HashMap<String, String> videoData);

    @FormUrlEncoded
    @POST(AppConstants.NetworkConstants.REPORT_API_END_POINT)
    Call<ReportResponse> hitReportApi(@FieldMap HashMap<String, Object> reportData);

    @FormUrlEncoded
    @POST(AppConstants.NetworkConstants.DELETE_POST_API_END_POINT)
    Call<ChangePasswordModel> hitDeletePostApi(@FieldMap HashMap<String, String> data);

    @FormUrlEncoded
    @POST(AppConstants.NetworkConstants.CHANGE_VIDEO_PRIVACY_END_POINT)
    Call<PrivacyStatusResponseBean> hitPrivacyStatusApi(@FieldMap HashMap<String, String> dataForStatus);

    @FormUrlEncoded
    @POST(AppConstants.NetworkConstants.CHAT_PUSH_NOTIFICATION_API_END_POINT)
    Call<ChangePasswordModel> hitChatNotificationApi(@FieldMap HashMap<String, String> chatData);

    @GET(AppConstants.NetworkConstants.NOTIFICATION_LISTING_END_POINT)
    Call<NotificationResponse> hitNotificationListingApi(@Query(AppConstants.NetworkConstants.USER_ID) String userId, @Query(AppConstants.NetworkConstants.LANGUAGE) String language);

    @GET(AppConstants.NetworkConstants.REUSE_VIDEOS_CHART_API_END_POINT)
    Call<ReuseVideoChartResponse> hitReusedVideosChartApi(@QueryMap HashMap<String, String> data);

    @FormUrlEncoded
    @POST(AppConstants.NetworkConstants.SAVE_VIDEO_API_END_POINT)
    Call<PrivacyStatusResponseBean> hitSaveFeedVideoApi(@FieldMap HashMap<String, String> saveFeedData);

    @GET(AppConstants.NetworkConstants.SUGGESTED_VIDEO_INFO)
    Call<SuggestedVideoInfoResponse> hitSuggestedVideosApi(@QueryMap HashMap<String, String> suggestedVideoData);

    @FormUrlEncoded
    @POST(AppConstants.NetworkConstants.DELETE_COMMENT_API)
    Call<CommentDeleteResponse> hitCommentDeleteApi(@FieldMap HashMap<String, String> commentHashMap);

    @GET("v1/gifs/trending")
    Call<GIFModel> hitGifList(@QueryMap HashMap<String, String> gifQuery);
}