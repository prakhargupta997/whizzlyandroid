package com.whizzly.network;

import android.util.Log;

import com.whizzly.models.FailureResponse;

import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class NetworkCallback<T> implements Callback<T> {

    public static final int AUTH_FAILED = 99;
    public static final int NO_INTERNET = 9;

    public abstract void onSuccess(T t);

    public abstract void onFailure(FailureResponse failureResponse);

    public abstract void onError(Throwable t);

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        try {
            onSuccess(response.body());
        } catch (Exception e) {
            Log.e("onResponse: ", response.code() + " " + e.getMessage());
        }
    }

    /*private boolean getCode(ResponseBody body) {

    }*/

    private FailureResponse getFailureResponseForAutoLogOut(Response<T> response) {
        FailureResponse failureResponse=new FailureResponse();
        failureResponse.setErrorCode(response.code());
        failureResponse.setErrorMessage(response.message());
        return failureResponse;
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        try {
            if (t instanceof SocketTimeoutException || t instanceof UnknownHostException) {
            FailureResponse failureResponseForNoNetwork = getFailureResponseForNoNetwork();
            onFailure(failureResponseForNoNetwork);
        } else {
            onError(t);
        }
        }catch (Exception e) {
            Log.e("onFailure: ", t.getMessage()+" \n" + e.getMessage());
        }
    }

    private FailureResponse getFailureResponseForNoNetwork() {
        FailureResponse failureResponse = new FailureResponse();
        failureResponse.setErrorMessage("No Network");
        failureResponse.setErrorCode(NO_INTERNET);
        return failureResponse;
    }

    /**
     * Create your custom failure response out of server response
     * Also save Url for any further use
     */
    private FailureResponse getFailureErrorBody(String url, Response<T> errorBody) {
        FailureResponse failureResponse = new FailureResponse();
        failureResponse.setErrorCode(errorBody.code());
        failureResponse.setErrorMessage(errorBody.message());
        return failureResponse;
    }
}
