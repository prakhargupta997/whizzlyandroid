package com.whizzly.login_signup_module.profile_setup;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.DisplayCutout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.dnitinverma.amazons3library.AmazonS3;
import com.dnitinverma.amazons3library.interfaces.AmazonCallback;
import com.dnitinverma.amazons3library.model.MediaBean;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.cropper.CropImage;
import com.whizzly.cropper.CropImageView;
import com.whizzly.databinding.ActivityProfileSetUpBinding;
import com.whizzly.databinding.DialogImageSelectBinding;
import com.whizzly.dialog.dialogViewModels.ImageSelectDialogViewModel;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.firebase_model.FirebaseUserDetailModel;
import com.whizzly.models.submit_profile.SubmitProfileBean;
import com.whizzly.models.validate_user_bean.ValidateUserBean;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.CameraHelper;
import com.whizzly.utils.CameraHelper.ImageCallBack;
import com.whizzly.utils.DataManager;

import static com.whizzly.utils.CameraHelper.CAMERA_INTENT_REQUEST_CODE;
import static com.whizzly.utils.CameraHelper.GALLERY_INTENT_REQUEST_CODE;

public class ProfileSetUpActivity extends BaseActivity implements OnClickSendListener, AmazonCallback, ImageCallBack, CameraHelper.PermissionGranted {

    BottomSheetDialog mBottomSheetDialog;
    private ActivityProfileSetUpBinding mActivityProfileSetUpBinding;
    private ProfileSetupModel mProfileSetupModel;
    private ProfileSetupViewModel mProfileSetupViewModel;
    private AmazonS3 mAmazonS3;
    private CameraHelper mCameraHelper;
    //    private Dialog dialog;
    private String picturePath;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        getData();
        initializeAmazonS3();
    }

    private void getData() {
        String email = mProfileSetupModel.email();
        String name = mProfileSetupModel.getSavedName();
        String profilePic = mProfileSetupModel.profilePic();
        if (profilePic != null && name != null && !TextUtils.isEmpty(name)) {
            mProfileSetupModel.setProfileImage(Uri.parse(profilePic));
            mProfileSetupModel.setName(name);
        }
        if (email != null && !TextUtils.isEmpty(email)) {
            mProfileSetupModel.setEmail(email);
            mActivityProfileSetUpBinding.etEmail.setEnabled(false);
            mActivityProfileSetUpBinding.etEmail.setTextColor(getResources().getColor(R.color.text_secondary));
        }
    }

    private void init() {
        mActivityProfileSetUpBinding = DataBindingUtil.setContentView(this, R.layout.activity_profile_set_up);
        mProfileSetupViewModel = ViewModelProviders.of(this).get(ProfileSetupViewModel.class);
        mActivityProfileSetUpBinding.setViewModel(mProfileSetupViewModel);
        mProfileSetupModel = new ProfileSetupModel(this);
        mActivityProfileSetUpBinding.setModel(mProfileSetupModel);
        mProfileSetupViewModel.setOnClickSendListener(this);
        mActivityProfileSetUpBinding.getRoot().setOnApplyWindowInsetsListener((view, windowInsets) -> {
            DisplayCutout displayCutout = windowInsets.getDisplayCutout();
            if (displayCutout != null) {
                DataManager.getInstance().setIsNotchDevice(true);
            }else{
                DataManager.getInstance().setIsNotchDevice(false);
            }
            return windowInsets;
        });
        mCameraHelper = CameraHelper.getInstance();
        if(DataManager.getInstance().isNotchDevice()){
            mActivityProfileSetUpBinding.toolbar.view.setVisibility(View.VISIBLE);
        }else{
            mActivityProfileSetUpBinding.toolbar.view.setVisibility(View.GONE);
        }
        mCameraHelper.setCOntext(this);
//        AppUtils.setStatusBarGradiant(this);
        mCameraHelper.setImageCallBack(this);
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mProfileSetupViewModel.getProfileModel(mProfileSetupModel);
        mProfileSetupViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver());
        mProfileSetupViewModel.getLoginResponseLiveData().observe(this, new Observer<SubmitProfileBean>() {
            @Override
            public void onChanged(@Nullable SubmitProfileBean submitProfileBean) {
                if (submitProfileBean != null && submitProfileBean.getCode() == 200) {
                    sendToHomeScreen();
                    addDataToFirebase(submitProfileBean.getResult().getProfileImage(), submitProfileBean.getResult().getUserId());
                    mActivityProfileSetUpBinding.bar.setVisibility(View.GONE);
                    mProfileSetupModel.saveUserName(submitProfileBean.getResult().getUserName());
                    mProfileSetupModel.saveProfilePic(submitProfileBean.getResult().getProfileImage());
                    mProfileSetupModel.saveName(submitProfileBean.getResult().getFirstName());
                    mProfileSetupModel.setProfileSetUp(submitProfileBean.getResult().getProfileSetup());
                    Toast.makeText(ProfileSetUpActivity.this, getResources().getText(R.string.txt_profilesetup_successful), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ProfileSetUpActivity.this, submitProfileBean.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        });

        mProfileSetupViewModel.getValidateUserBeanRichMediatorLiveData().observe(this, new Observer<ValidateUserBean>() {
            @Override
            public void onChanged(@Nullable ValidateUserBean validateUserBean) {

            }
        });

        String username = mProfileSetupViewModel.getUserNameFromPref();
        if (username != null && !TextUtils.isEmpty(username)) {
            mProfileSetupModel.setUsername(username);
            mActivityProfileSetUpBinding.etUsername.setEnabled(false);
            mActivityProfileSetUpBinding.etUsername.setTextColor(getResources().getColor(R.color.text_secondary));
        }
    }

    private void sendToHomeScreen() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finishAffinity();
    }

    @Override
    public void onClickSend(int code) {


        switch (code) {
            case AppConstants.ClassConstants.EDIT_PROFILE_PIC:
                showProfileImageDialog();
                break;
           /* case AppConstants.ClassConstants.CALENDER_CLICKED:
                manageKeyboard();
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.YEAR, -13);
                String year = String.valueOf(calendar.get(Calendar.YEAR)), month = String.valueOf(calendar.get(Calendar.MONTH)), date = String.valueOf(calendar.get(Calendar.DATE));
                if (mProfileSetupModel.getDob() != null && mProfileSetupModel.getDob().length() > 0) {
                    String[] fullDate = mProfileSetupModel.getDob().split("/");
                    date = fullDate[0];
                    month = String.valueOf(Integer.parseInt(fullDate[1]) - 1);
                    year = fullDate[2];
                }
                Fragment fragment = getSupportFragmentManager().findFragmentByTag("DatePickerDialog");
                if (fragment == null) {
                    StartDatePicker startDatePicker = new StartDatePicker((date1, month1, year1) -> mProfileSetupModel.setDob(date1 + "/" + month1 + "/" + year1));
                    startDatePicker.setPreviousDate(year, month, date);
                    startDatePicker.show(getSupportFragmentManager(), "DatePickerDialog");
                }
                break;*/
            /*case AppConstants.ClassConstants.EDIT_PROFILE_CAMERA:
                mCameraHelper.openCamera(this, null);
                dialog.dismiss();
                break;
            case AppConstants.ClassConstants.EDIT_PROFILE_GALLERY:
                mCameraHelper.openGallery(this, null);
                dialog.dismiss();
                break;
            case AppConstants.ClassConstants.EDIT_PROFILE_CANCEL:
                dialog.dismiss();
                break;*/
            case AppConstants.ClassConstants.PROFILE_SETUP_SUBMIT_CLICKED:
                if (AppUtils.isNetworkAvailable(this)) {
                    if (mProfileSetupModel.isValidEmail() == null
                            && mProfileSetupModel.isValidName() == null && mProfileSetupModel.isValidUserName() == null)
                        uploadImage();
                    else {
                        if (mProfileSetupModel.isValidName() != null) {
                            Toast.makeText(this, mProfileSetupModel.isValidName(), Toast.LENGTH_SHORT).show();
                        } else if (mProfileSetupModel.isValidUserName() != null) {
                            Toast.makeText(this, mProfileSetupModel.isValidUserName(), Toast.LENGTH_SHORT).show();
                        } else if (mProfileSetupModel.isValidEmail() != null) {
                            Toast.makeText(this, mProfileSetupModel.isValidEmail(), Toast.LENGTH_SHORT).show();
                        }
                    }

                } else {
                    Toast.makeText(this, getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void initializeAmazonS3() {
        mAmazonS3 = new AmazonS3();
        mAmazonS3.setActivity(this);
        mAmazonS3.setCallback(this);
    }

    private void showProfileImageDialog() {
        openBottomSheet();
    }

    @Override
    public void uploadSuccess(MediaBean bean) {
        mProfileSetupModel.hitSubmitProfileInfo(mProfileSetupViewModel.getLoginResponseLiveData(), mProfileSetupViewModel.getData(bean.getServerUrl()));
    }

    @Override
    public void uploadFailed(MediaBean bean) {

    }

    @Override
    public void uploadProgress(MediaBean bean) {

    }

    @Override
    public void uploadError(Exception e, MediaBean imageBean) {

    }

    private void openBottomSheet() {
        mBottomSheetDialog = new BottomSheetDialog(this);
        DialogImageSelectBinding dialogImageSelectBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_image_select, null, false);
        ImageSelectDialogViewModel imageSelectDialogViewModel = new ImageSelectDialogViewModel(new OnClickSendListener() {
            @Override
            public void onClickSend(int code) {

                switch (code) {
                    case AppConstants.ClassConstants.EDIT_PROFILE_CAMERA:
                        mCameraHelper.openCamera(ProfileSetUpActivity.this, null);
                        mBottomSheetDialog.dismiss();
                        break;
                    case AppConstants.ClassConstants.EDIT_PROFILE_GALLERY:
                        mCameraHelper.openGallery(ProfileSetUpActivity.this, null);
                        mBottomSheetDialog.dismiss();
                        break;
                    case AppConstants.ClassConstants.EDIT_PROFILE_CANCEL:
                        mBottomSheetDialog.dismiss();
                        break;
                }
            }
        });
        mBottomSheetDialog.setContentView(dialogImageSelectBinding.getRoot());
        dialogImageSelectBinding.setViewModel(imageSelectDialogViewModel);
        mBottomSheetDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_INTENT_REQUEST_CODE) {
            mCameraHelper.onActivityResult(requestCode, resultCode, data);
        } else if (requestCode == CAMERA_INTENT_REQUEST_CODE) {
            mCameraHelper.onActivityResult(requestCode, resultCode, data);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Uri resultUri = null;
            if (result != null) {
                resultUri = result.getUri();
                picturePath = resultUri.getPath();
                mProfileSetupModel.setProfileImage(resultUri);
            }
        }
    }

    private void uploadImage() {
        if (picturePath != null && !picturePath.isEmpty()) {
            MediaBean mediaBean = addDataInBean(picturePath);
            mAmazonS3.upload(mediaBean);
            mProfileSetupModel.setIsShowProgress(true);
        } else {
            mProfileSetupModel.hitSubmitProfileInfo(mProfileSetupViewModel.getLoginResponseLiveData(), mProfileSetupViewModel.getData(""));
        }
    }

    private void addDataToFirebase(String profilePic, String userId) {
        FirebaseUserDetailModel firebaseUserDetailModel = new FirebaseUserDetailModel();
        firebaseUserDetailModel.setFirstName(mProfileSetupModel.getName());
        firebaseUserDetailModel.setUserName(mProfileSetupModel.getUsername());
        firebaseUserDetailModel.setProfileImage(profilePic);
        firebaseUserDetailModel.setUserEmail(mProfileSetupModel.getEmail());
        firebaseUserDetailModel.setUserId(userId);

        mDatabase.child(AppConstants.FirebaseConstants.USERS).child(userId).setValue(firebaseUserDetailModel).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isComplete()) {
                    Log.e("Firebase Database: ", "Data Added Successfully");
                }
            }
        });

    }

    private MediaBean addDataInBean(String path) {
        MediaBean bean = new MediaBean();
        bean.setName("sample");
        bean.setMediaPath(path);
        return bean;
    }

    @Override
    public void onImageSuccess(String path) {
        CropImage.activity(Uri.parse("file:///" + path))
                .setCropShape(CropImageView.CropShape.OVAL)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setMinCropResultSize(100, 100)
                .setFixAspectRatio(true)
                .setMultiTouchEnabled(false)
                .setAspectRatio(1, 1)
                .start(this);
    }

    @Override
    public void onImageFailure(String errorText, int imageType) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (permissions.length == grantResults.length) {
            switch (requestCode) {
                case CAMERA_INTENT_REQUEST_CODE:
                    if (grantResults.length > 0 && hasPermission(grantResults)) {
                        mCameraHelper.openCamera(this, null);
                    }
                    break;

                case GALLERY_INTENT_REQUEST_CODE:
                    if (grantResults.length > 0 && hasPermission(grantResults)) {
                        mCameraHelper.openGallery(this, null);
                    }
                    break;
            }
        }
    }

    private boolean hasPermission(int[] grantResults) {
        boolean hasPermissions = false;
        int permissionCount = 0;
        for (int grantResult : grantResults) {
            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                hasPermissions = true;
                permissionCount++;
            }
        }
        return hasPermissions && permissionCount == grantResults.length;
    }

    @Override
    public void onPermissionGranted(int requestCode) {
        switch (requestCode) {
            case CAMERA_INTENT_REQUEST_CODE:
                mCameraHelper.openCamera(this, null);
                break;

            case GALLERY_INTENT_REQUEST_CODE:
                mCameraHelper.openGallery(this, null);
                break;
        }
    }
}