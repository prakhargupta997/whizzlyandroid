package com.whizzly.login_signup_module.profile_setup;

import android.content.Context;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import android.net.Uri;
import android.widget.Toast;

import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.submit_profile.SubmitProfileBean;
import com.whizzly.models.validate_user_bean.ValidateUserBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class ProfileSetupModel extends BaseObservable {
    public ObservableField<String> name = new ObservableField<>();
    public ObservableField<String> username = new ObservableField<>();
    private ObservableField<String> email = new ObservableField<>();
    private ObservableField<Uri> mProfileImage = new ObservableField<>();
    private ObservableBoolean isUserNameValid = new ObservableBoolean(false);

    @Bindable
    public boolean getIsShowProgress() {
        return isShowProgress.get();
    }

    public void setIsShowProgress(boolean isShowProgress) {
        this.isShowProgress.set(isShowProgress);
        notifyPropertyChanged(BR.isShowProgress);
    }

    private ObservableBoolean isShowProgress = new ObservableBoolean(false);
    private ObservableBoolean isSearchingUserName = new ObservableBoolean(false);
    private Context context;

    public ProfileSetupModel(Context context) {
        this.context = context;
    }

    @Bindable
    public String getEmail() {
        return email.get();
    }

    public void setEmail(String email) {
        this.email.set(email);
        notifyPropertyChanged(BR.email);
    }

    @Bindable
    public boolean getIsUserNameValid() {
        return isUserNameValid.get();
    }

    public void setIsUserNameValid(boolean isUserNameValid) {
        this.isUserNameValid.set(isUserNameValid);
        notifyPropertyChanged(BR.isUserNameValid);
    }

    @Bindable
    public boolean getIsSearchingUserName() {
        return isSearchingUserName.get();
    }

    public void setIsSearchingUserName(boolean isSearchingUserName) {
        this.isSearchingUserName.set(isSearchingUserName);
        notifyPropertyChanged(BR.isSearchingUserName);
    }

    @Bindable
    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
        notifyPropertyChanged(BR.name);
    }

    @Bindable
    public String getUsername() {
        return username.get();
    }

    public void setUsername(String username) {
        this.username.set(username);
        notifyPropertyChanged(BR.username);
    }

    @Bindable
    public Uri getProfileImage() {
        return mProfileImage.get();
    }

    public void setProfileImage(Uri mProfileImage) {
        this.mProfileImage.set(mProfileImage);
        notifyPropertyChanged(BR.profileImage);
    }

    public String isValidName() {
        if (getName() == null) {
            return context.getString(R.string.txt_error_name_empty);
        } else {
            if (getName().isEmpty()) {
                return context.getString(R.string.txt_error_name_empty);
            } else {
                return null;
            }
        }
    }


    public String isValidUserName() {
        if (getUsername() == null) {
            return context.getString(R.string.txt_empty_username);
        } else {
            if (getUsername().length() == 0) {
                return context.getString(R.string.txt_empty_username);
            } else if (getUsername().contains(" ")) {
                return context.getString(R.string.txt_enter_valid_username);
            } else if (getUsername().length() <= 1) {
                return context.getString(R.string.txt_least_username_characters);
            } else if (getUsername().length() > 20) {
                return context.getString(R.string.txt_longest_username);
            } else {
                return null;
            }
        }
    }

    public String isValidEmail() {
        if (getEmail() == null) {
            return context.getString(R.string.txt_error_empty_email);
        } else {
            if (getEmail().isEmpty()) {
                return context.getString(R.string.txt_error_empty_email);
            } else if (!AppUtils.isEmailValid(getEmail())) {
                return context.getString(R.string.txt_email_invalid);
            } else {
                return null;
            }
        }
    }

    public void hitSubmitProfileInfo(final RichMediatorLiveData<SubmitProfileBean> submitProfileBeanRichMediatorLiveData, HashMap<String, String> submitProfile) {
        if(AppUtils.isNetworkAvailable(context)) {
            if (isValidEmail() == null && isValidName() == null && isValidUserName() == null) {
                setIsShowProgress(true);
                DataManager.getInstance().hitSubmitProfile(submitProfile).enqueue(new NetworkCallback<SubmitProfileBean>() {
                    @Override
                    public void onSuccess(SubmitProfileBean submitProfileBean) {
                        submitProfileBeanRichMediatorLiveData.setValue(submitProfileBean);
                    }

                    @Override
                    public void onFailure(FailureResponse failureResponse) {
                        submitProfileBeanRichMediatorLiveData.setFailure(failureResponse);
                    }

                    @Override
                    public void onError(Throwable t) {
                        submitProfileBeanRichMediatorLiveData.setError(t);
                    }
                });
            } else {
                if (isValidName() != null) {
                    Toast.makeText(context, isValidName(), Toast.LENGTH_SHORT).show();
                } else if (isValidUserName() != null) {
                    Toast.makeText(context, isValidUserName(), Toast.LENGTH_SHORT).show();
                } else if (isValidEmail() != null) {
                    Toast.makeText(context, isValidEmail(), Toast.LENGTH_SHORT).show();
                }
            }
        }else {
            Toast.makeText(context, context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    public String getUserNameFromPref() {
        return DataManager.getInstance().getUserName();
    }


    public void hitValidateUsername(RichMediatorLiveData<ValidateUserBean> validateUserBeanRichMediatorLiveData, String toString) {
        if(AppUtils.isNetworkAvailable(context)) {
            setIsSearchingUserName(true);
            DataManager.getInstance().hitValidateUsername(toString, DataManager.getInstance().getLanguage()).enqueue(new NetworkCallback<ValidateUserBean>() {
                @Override
                public void onSuccess(ValidateUserBean validateUserBean) {
                    validateUserBeanRichMediatorLiveData.setValue(validateUserBean);
                    if (validateUserBean.getCode() == 200) {
                        setIsSearchingUserName(false);
                        setIsUserNameValid(true);
                    } else {
                        setIsSearchingUserName(false);
                        setIsUserNameValid(false);
                        Toast.makeText(context, validateUserBean.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    validateUserBeanRichMediatorLiveData.setFailure(failureResponse);
                    setIsSearchingUserName(false);
                    setIsUserNameValid(false);
                }

                @Override
                public void onError(Throwable t) {
                    validateUserBeanRichMediatorLiveData.setError(t);
                    setIsSearchingUserName(false);
                    setIsUserNameValid(false);
                }
            });
        }else {
            Toast.makeText(context, context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    public void setProfileSetUp(int profileSetup) {
        DataManager.getInstance().setIsProfileSetUp(profileSetup);
    }

    public String getSavedName() {
        return DataManager.getInstance().getName();
    }

    public String email() {
        return DataManager.getInstance().getEmail();
    }

    public String profilePic() {
        return DataManager.getInstance().getProfilePic();
    }

    public void saveName(String firstName) {
        DataManager.getInstance().setName(firstName);
    }

    public void saveProfilePic(String profileImage) {
        DataManager.getInstance().setProfilePic(profileImage);
    }


    public void saveUserName(String userName) {
        DataManager.getInstance().saveUserName(userName);
    }
}
