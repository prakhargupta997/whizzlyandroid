package com.whizzly.login_signup_module.profile_setup;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.databinding.ObservableBoolean;
import android.text.Editable;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.submit_profile.SubmitProfileBean;
import com.whizzly.models.validate_user_bean.ValidateUserBean;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class ProfileSetupViewModel extends ViewModel {
    public ObservableBoolean image = new ObservableBoolean(false);
    private ProfileSetupModel mProfileSetupModel;
    private OnClickSendListener onClickSendListener;
    private RichMediatorLiveData<SubmitProfileBean> mSubmitResponseBeanRichMediatorLiveData;
    private RichMediatorLiveData<ValidateUserBean> validateUserBeanRichMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<FailureResponse> validateLiveData;

    public String getUserNameFromPref() {
        return mProfileSetupModel.getUserNameFromPref();
    }

    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureObserver;
        initLiveData();
    }

    public void getProfileModel(ProfileSetupModel mProfileSetupModel) {
        this.mProfileSetupModel = mProfileSetupModel;
    }

    private void initLiveData() {
        if (mSubmitResponseBeanRichMediatorLiveData == null) {
            mSubmitResponseBeanRichMediatorLiveData = new RichMediatorLiveData<SubmitProfileBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

        if(validateUserBeanRichMediatorLiveData == null){
            validateUserBeanRichMediatorLiveData= new RichMediatorLiveData<ValidateUserBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

        if (validateLiveData == null)
            validateLiveData = new MutableLiveData<>();
    }

    public void afterTextChanged(Editable e){
        mProfileSetupModel.setUsername(e.toString());
        mProfileSetupModel.setIsSearchingUserName(false);
        mProfileSetupModel.setIsUserNameValid(false);
        if(e.toString().length()>2 && (DataManager.getInstance().getUserName() == null)){
            mProfileSetupModel.hitValidateUsername(validateUserBeanRichMediatorLiveData, e.toString());
        }
    }

    public RichMediatorLiveData<SubmitProfileBean> getLoginResponseLiveData() {
        return mSubmitResponseBeanRichMediatorLiveData;
    }

    public RichMediatorLiveData<ValidateUserBean> getValidateUserBeanRichMediatorLiveData(){
        return validateUserBeanRichMediatorLiveData;
    }

    void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void onEditProfilePic() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.EDIT_PROFILE_PIC);
    }

    public void onCalenderClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.CALENDER_CLICKED);
    }

    public HashMap<String, String> getData(String imageUrl) {
        int userId = DataManager.getInstance().getUserId();
        HashMap<String, String> submitProfile = new HashMap<>();
        submitProfile.put(AppConstants.NetworkConstants.SUBMIT_PROFILE_PARAM_1, mProfileSetupModel.getUsername());
        submitProfile.put(AppConstants.NetworkConstants.SUBMIT_PROFILE_PARAM_2, String.valueOf(userId));
        submitProfile.put(AppConstants.NetworkConstants.SUBMIT_PROFILE_PARAM_3, mProfileSetupModel.getName());
        submitProfile.put(AppConstants.NetworkConstants.SUBMIT_PROFILE_PARAM_5, imageUrl);
        return submitProfile;
    }

    public void onSubmitClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.PROFILE_SETUP_SUBMIT_CLICKED);
    }

    public void onClickGallery(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.EDIT_PROFILE_GALLERY);
    }

    public void onClickCamera(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.EDIT_PROFILE_CAMERA);
    }

    public void onClickCancel(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.EDIT_PROFILE_CANCEL);
    }

}
