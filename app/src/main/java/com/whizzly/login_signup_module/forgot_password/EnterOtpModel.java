package com.whizzly.login_signup_module.forgot_password;

import android.content.Context;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import android.widget.Toast;

import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.forgot_password.ForgotPasswordBean;
import com.whizzly.models.forgot_password.ValidateOTPBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class EnterOtpModel extends BaseObservable {
    private ObservableField<String> enterOtp = new ObservableField<>("");
    private ObservableBoolean isTimerFinished = new ObservableBoolean(false);
    private ObservableField<String> timer = new ObservableField<>();
    private Context context;

    public EnterOtpModel(Context context) {
        this.context = context;
    }

    @Bindable
    public boolean getIsTimerFinished() {
        return isTimerFinished.get();
    }

    public void setIsTimerFinished(boolean isTimerFinished) {
        this.isTimerFinished.set(isTimerFinished);
        notifyPropertyChanged(BR.isTimerFinished);
    }

    @Bindable
    public String getTimer() {
        return timer.get();
    }

    public void setTimer(String timer) {
        this.timer.set(timer);
        notifyPropertyChanged(BR.timer);
    }

    @Bindable
    public String getEnterOtp() {
        return enterOtp.get();
    }


    public void setEnterOtp(String enterOtp) {
        this.enterOtp.set(enterOtp);
        notifyPropertyChanged(BR.enterOtp);
    }

    private HashMap<String, Object> setHashmap() {
        HashMap<String, Object> otp = new HashMap<>();
        otp.put("user_id", getUserId());
        otp.put("code", getEnterOtp());
        return otp;
    }

    public void hitValidateOTP(final RichMediatorLiveData<ValidateOTPBean> validateOTPBeanRichMediatorLiveData) {
        notifyPropertyChanged(BR.enterOtp);
        if(AppUtils.isNetworkAvailable(context)) {
            if (getEnterOtp() != null && !getEnterOtp().isEmpty()) {
                DataManager.getInstance().hitValidateOtp(setHashmap()).enqueue(new NetworkCallback<ValidateOTPBean>() {
                    @Override
                    public void onSuccess(ValidateOTPBean validateOTPBean) {
                        validateOTPBeanRichMediatorLiveData.setValue(validateOTPBean);
                    }

                    @Override
                    public void onFailure(FailureResponse failureResponse) {
                        validateOTPBeanRichMediatorLiveData.setFailure(failureResponse);
                    }

                    @Override
                    public void onError(Throwable t) {
                        validateOTPBeanRichMediatorLiveData.setError(t);
                    }
                });
            } else {
                Toast.makeText(context, R.string.txt_error_empty_otp, Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(context, context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    public String getEmail() {
        return DataManager.getInstance().getEmail();
    }

    public int getUserId() {
        return DataManager.getInstance().getUserId();
    }

    public void hitResendOTP(final RichMediatorLiveData<ForgotPasswordBean> forgotPasswordBeanRichMediatorLiveData) {
        notifyPropertyChanged(BR.timer);
        if (getTimer().equalsIgnoreCase("Resend")) {
            DataManager.getInstance().hitForgotPassword(getEmail(),DataManager.getInstance().getLanguage()).enqueue(new NetworkCallback<ForgotPasswordBean>() {
                @Override
                public void onSuccess(ForgotPasswordBean forgotPasswordBean) {
                    forgotPasswordBeanRichMediatorLiveData.setValue(forgotPasswordBean);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    forgotPasswordBeanRichMediatorLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    forgotPasswordBeanRichMediatorLiveData.setError(t);
                }
            });
        }
    }
}
