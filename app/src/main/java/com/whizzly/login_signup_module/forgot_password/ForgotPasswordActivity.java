package com.whizzly.login_signup_module.forgot_password;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.ActivityForgotPasswordBinding;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.login_signup_module.login_screen.LoginActivity;
import com.whizzly.models.forgot_password.ForgotPasswordBean;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

public class ForgotPasswordActivity extends BaseActivity implements OnClickSendListener {

    private ActivityForgotPasswordBinding mActivityForgotPasswordBinding;
    private ForgotPasswordModel mForgotPasswordModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        clearStatusBar();
    }

    private ForgotPasswordViewModel forgotPasswordViewModel;
    private void init() {
        mActivityForgotPasswordBinding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        forgotPasswordViewModel = ViewModelProviders.of(this).get(ForgotPasswordViewModel.class);
        mActivityForgotPasswordBinding.setViewModel(forgotPasswordViewModel);
        mActivityForgotPasswordBinding.llToolbar.ivBack.setOnClickListener(view -> onBackPressed());
        mForgotPasswordModel = new ForgotPasswordModel(this);
        forgotPasswordViewModel.setOnClickSendListener(this::onClickSend);
        mActivityForgotPasswordBinding.setModel(mForgotPasswordModel);
        forgotPasswordViewModel.setForgotPasswordModel(mForgotPasswordModel);
        forgotPasswordViewModel.setGenericListeners(getErrorObserver(),getFailureResponseObserver());
        forgotPasswordViewModel.getLoginResponseLiveData().observe(this, forgotPasswordBean -> {
            if(forgotPasswordBean.getCode() == 200) {
                Toast.makeText(ForgotPasswordActivity.this, R.string.txt_otp_sent_message, Toast.LENGTH_SHORT).show();
                mForgotPasswordModel.setUserId(forgotPasswordBean.getResult().getUserId());
                moveToOTPScreen(forgotPasswordBean);
            }else{
                Toast.makeText(this, forgotPasswordBean.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void moveToOTPScreen(ForgotPasswordBean forgotPasswordBean){
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClickSend(int code) {
        if(code == AppConstants.ClassConstants.ON_FORGOT_CLICKED){
            if(AppUtils.isNetworkAvailable(this)) {
                if (mForgotPasswordModel.isEmailValid() == null) {
                    mForgotPasswordModel.hitForgotPasswordApi(forgotPasswordViewModel.getLoginResponseLiveData(), mForgotPasswordModel.getEmail());
                } else {
                    Toast.makeText(this, mForgotPasswordModel.isEmailValid(), Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(this, getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
