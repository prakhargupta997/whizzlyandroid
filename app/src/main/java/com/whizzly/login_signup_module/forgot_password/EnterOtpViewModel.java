package com.whizzly.login_signup_module.forgot_password;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.forgot_password.ForgotPasswordBean;
import com.whizzly.models.forgot_password.ValidateOTPBean;

public class EnterOtpViewModel extends ViewModel {
    private RichMediatorLiveData<ValidateOTPBean> validateOTPLiveData;

    public RichMediatorLiveData<ForgotPasswordBean> getForgotPasswordBeanRichMediatorLiveData() {
        return forgotPasswordBeanRichMediatorLiveData;
    }

    private RichMediatorLiveData<ForgotPasswordBean> forgotPasswordBeanRichMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private EnterOtpModel mEnterOtpModel;

    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (validateOTPLiveData == null)
            validateOTPLiveData = new RichMediatorLiveData<ValidateOTPBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };

        if(forgotPasswordBeanRichMediatorLiveData == null){
            forgotPasswordBeanRichMediatorLiveData = new RichMediatorLiveData<ForgotPasswordBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
    }



    public void setEnterOtpModel(EnterOtpModel mEnterOtpModel) {
        this.mEnterOtpModel = mEnterOtpModel;
    }

    public RichMediatorLiveData<ValidateOTPBean> getValidateOTPLiveData() {
        return validateOTPLiveData;
    }

    public void submitOTP() {
        mEnterOtpModel.hitValidateOTP(validateOTPLiveData);
    }

    public void resetOTP() {
        mEnterOtpModel.hitResendOTP(forgotPasswordBeanRichMediatorLiveData);
    }
}
