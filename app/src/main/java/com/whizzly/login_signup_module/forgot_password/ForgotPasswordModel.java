package com.whizzly.login_signup_module.forgot_password;


import android.content.Context;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableField;

import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.forgot_password.ForgotPasswordBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

public class ForgotPasswordModel extends BaseObservable {
    public ObservableField<String> email = new ObservableField<>();
    private Context context;

    public ForgotPasswordModel(Context context) {
        this.context = context;
    }

    @Bindable
    public String getEmail() {
        return email.get();
    }

    public void setEmail(String email) {
        this.email.set(email);
        notifyPropertyChanged(BR.email);
    }


    public void hitForgotPasswordApi(final RichMediatorLiveData<ForgotPasswordBean> forgotPasswordModelRichMediatorLiveData, String email) {
        notifyPropertyChanged(BR.email);
        setEmailToPref(email);
        DataManager.getInstance().hitForgotPassword(email, DataManager.getInstance().getLanguage()).enqueue(new NetworkCallback<ForgotPasswordBean>() {
            @Override
            public void onSuccess(ForgotPasswordBean forgotPasswordBean) {
                forgotPasswordModelRichMediatorLiveData.setValue(forgotPasswordBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                forgotPasswordModelRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                forgotPasswordModelRichMediatorLiveData.setError(t);
            }
        });
    }

    public String isEmailValid() {
        if (getEmail() == null) {
            return context.getString(R.string.txt_error_empty_email);
        } else {
            if (getEmail().isEmpty()) {
                return context.getString(R.string.txt_error_empty_email);
            } else if (!AppUtils.isEmailValid(getEmail())) {
                return context.getString(R.string.txt_email_invalid);
            } else {
                return null;
            }
        }
    }

    public void setUserId(int userId) {
        DataManager.getInstance().setUserId(userId);
    }

    public void setEmailToPref(String email) {
        DataManager.getInstance().setEmail(email);
    }
}
