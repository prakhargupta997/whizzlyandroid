package com.whizzly.login_signup_module.forgot_password;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.Nullable;

import android.view.View;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.ActivityEnterOtpBinding;
import com.whizzly.login_signup_module.login_screen.LoginActivity;
import com.whizzly.login_signup_module.reset_password.ResetPasswordActivity;
import com.whizzly.models.forgot_password.ForgotPasswordBean;
import com.whizzly.models.forgot_password.ValidateOTPBean;
import com.whizzly.utils.DataManager;

public class EnterOTPActivity extends BaseActivity {

    private ActivityEnterOtpBinding mActivityEnterOtpBinding;
    private EnterOtpViewModel mEnterOtpViewModel;
    private EnterOtpModel mEnterOtpModel;
    private void addCountDownTimer(){
        new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long l) {
                mEnterOtpModel.setIsTimerFinished(false);
                mEnterOtpModel.setTimer(String.format("%s(00:%02d)",getResources().getString(R.string.txt_resend), l / 1000));
            }

            @Override
            public void onFinish() {
                mEnterOtpModel.setIsTimerFinished(true);
                mEnterOtpModel.setTimer("Resend");
            }
        }.start();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityEnterOtpBinding = DataBindingUtil.setContentView(this, R.layout.activity_enter_otp);
        clearStatusBar();
        mEnterOtpViewModel = ViewModelProviders.of(this).get(EnterOtpViewModel.class);
        mActivityEnterOtpBinding.setViewModel(mEnterOtpViewModel);
        mEnterOtpModel = new EnterOtpModel(this);
        mActivityEnterOtpBinding.setModel(mEnterOtpModel);
        mEnterOtpViewModel.setEnterOtpModel(mEnterOtpModel);
        mEnterOtpViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver());
        mActivityEnterOtpBinding.toolbar.ivBack.setOnClickListener(view -> onBackPressed());
        mEnterOtpViewModel.getValidateOTPLiveData().observe(this, new Observer<ValidateOTPBean>() {
            @Override
            public void onChanged(@Nullable ValidateOTPBean validateOTPBean) {
                if (validateOTPBean != null) {
                    if (validateOTPBean.getCode() == 200) {
                        finish();
                        Intent intent = new Intent(EnterOTPActivity.this, ResetPasswordActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else {
                        Toast.makeText(EnterOTPActivity.this, getResources().getString(R.string.txt_invalid_otp), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        mEnterOtpViewModel.getForgotPasswordBeanRichMediatorLiveData().observe(this, new Observer<ForgotPasswordBean>() {
            @Override
            public void onChanged(@Nullable ForgotPasswordBean forgotPasswordBean) {
                if(forgotPasswordBean.getCode() == 200){
                    addCountDownTimer();
                    Toast.makeText(EnterOTPActivity.this, getString(R.string.txt_otp_sent_message), Toast.LENGTH_SHORT).show();
                }
            }
        });

        addCountDownTimer();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(EnterOTPActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
