package com.whizzly.login_signup_module.forgot_password;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.forgot_password.ForgotPasswordBean;
import com.whizzly.utils.AppConstants;

public class ForgotPasswordViewModel extends ViewModel {
    private ForgotPasswordModel forgotPasswordModel;
    private RichMediatorLiveData<ForgotPasswordBean> mForgotPasswordBeanRichMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<FailureResponse> validateLiveData;

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    private OnClickSendListener onClickSendListener;


    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureObserver;
        initLiveData();
    }

    public void setForgotPasswordModel(ForgotPasswordModel forgotPasswordModel){
        this.forgotPasswordModel = forgotPasswordModel;
    }

    private void initLiveData() {
        if (mForgotPasswordBeanRichMediatorLiveData == null) {
            mForgotPasswordBeanRichMediatorLiveData = new RichMediatorLiveData<ForgotPasswordBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

        if (validateLiveData == null)
            validateLiveData = new MutableLiveData<>();
    }

    public RichMediatorLiveData<ForgotPasswordBean> getLoginResponseLiveData() {
        return mForgotPasswordBeanRichMediatorLiveData;
    }


    public void onSendOTP() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_FORGOT_CLICKED);
    }
}