package com.whizzly.login_signup_module.splash_screen;

import androidx.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.DisplayCutout;
import android.view.WindowManager;

import com.dnitinverma.amazons3library.AmazonS3;
import com.dnitinverma.amazons3library.interfaces.AmazonCallback;
import com.dnitinverma.amazons3library.model.MediaBean;
import com.google.gson.Gson;
import com.whizzly.chat.ChatActivity;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.login_signup_module.login_signup_option.LoginSignupOptionActivity;
import com.whizzly.login_signup_module.profile_setup.ProfileSetUpActivity;
import com.whizzly.models.notifications.PushNotificationResponse;
import com.whizzly.notification.NotificationActivity;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SplashActivity extends AppCompatActivity {

    private SplashViewModel mSplashViewModel;
    private BroadcastReceiver tokenReciever;

    /*private void uploadFiles(){
        AmazonS3 amazonS3 = new AmazonS3();
        amazonS3.setActivity(this);
        amazonS3.setCallback(new AmazonCallback() {
            @Override
            public void uploadSuccess(MediaBean bean) {

            }

            @Override
            public void uploadFailed(MediaBean bean) {

            }

            @Override
            public void uploadProgress(MediaBean bean) {

            }

            @Override
            public void uploadError(Exception e, MediaBean imageBean) {

            }
        });
        MediaBean mediaBean = new MediaBean();
        mediaBean.setMediaPath(getWaterMarkFile()+"moov_audioAndVideo_1560542976216.mp4");
        amazonS3.upload(mediaBean);
    }
    private String getWaterMarkFile() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"));
    }
*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mSplashViewModel = ViewModelProviders.of(this).get(SplashViewModel.class);
        if (DataManager.getInstance().getNotificationStatus() == null) {
            DataManager.getInstance().setNotificationStatus(AppConstants.ClassConstants.NOTIFICATION_ENABLE);
        }
        tokenReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String token = intent.getStringExtra(AppConstants.ClassConstants.TOKEN);
                if (token != null) {
                    DataManager.getInstance().saveDeviceToken(token);
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(tokenReciever, new IntentFilter(AppConstants.ClassConstants.TOKEN_RECIEVER));
        getKeyHash();
        getIntentData();
    }

    private void getIntentData() {
        if (getIntent() != null && getIntent().hasExtra(AppConstants.ClassConstants.NOTIFICATION_DATA) && getIntent().getStringExtra(AppConstants.ClassConstants.NOTIFICATION_DATA) != null) {
            try {
                JSONObject notificationObject = new JSONObject(getIntent().getStringExtra(AppConstants.ClassConstants.NOTIFICATION_DATA));
                PushNotificationResponse notificationResponse = new Gson().fromJson(notificationObject.toString(), PushNotificationResponse.class);
                switch (notificationObject.getInt(AppConstants.NetworkConstants.NOTIFICATION_TYPE)) {
                    case 1:
                        //chat
                        Intent chatIntent = new Intent(this, ChatActivity.class);
                        chatIntent.putExtra(AppConstants.ClassConstants.ROOM_ID, notificationResponse.getRoomId());
                        chatIntent.putExtra(AppConstants.ClassConstants.USER_ID, notificationResponse.getSenderUserId());
                        chatIntent.putExtra(AppConstants.ClassConstants.USER_NAME, notificationResponse.getUserName());
                        chatIntent.putExtra(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.FROM_SPLASH);
                        chatIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(chatIntent);
                        finish();
                        break;
                    case 2:
                    case 3:
                    case 5:
                    case 6:
                        //comment,like,reuse
                        Intent intent = new Intent(this, NotificationActivity.class);
                        intent.putExtra(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.FROM_SPLASH);
                        intent.putExtra(AppConstants.ClassConstants.NOTIFICATION_DATA, notificationResponse);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        break;
                    case 4:
                        //follow
                        Intent homeIntent = new Intent(this, HomeActivity.class);
                        homeIntent.putExtra(AppConstants.ClassConstants.NOTIFICATION_DATA, notificationResponse);
                        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(homeIntent);
                        finish();
                        break;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else
            showSplashScreen();

    }

    public void showSplashScreen() {
        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSplashViewModel.getAccessToken() != null && !mSplashViewModel.getAccessToken().isEmpty()) {
                    if (mSplashViewModel.getProfileSetUp() == 0) {
                        startActivity(new Intent(SplashActivity.this, ProfileSetUpActivity.class));
                        overridePendingTransition(0, 0);
                    } else {
                        startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                        overridePendingTransition(0, 0);
                    }
                } else {
                    startActivity(new Intent(SplashActivity.this, LoginSignupOptionActivity.class));
                    overridePendingTransition(0, 0);
                }
                finish();
            }
        }, AppConstants.ClassConstants.SPLASH_TIME);
    }

    public String getKeyHash() {
        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("com.whizzly", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
        return null;
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(tokenReciever);
    }
}
