package com.whizzly.login_signup_module.splash_screen;

import com.whizzly.utils.DataManager;

public class SplashModel {
    int isProfileSetUp(){
        return DataManager.getInstance().isProfileSetUp();
    }

    String getAccessToken(){
        return DataManager.getInstance().getAccessToken();
    }
}
