package com.whizzly.login_signup_module.splash_screen;

import androidx.lifecycle.ViewModel;

public class SplashViewModel extends ViewModel {
    private SplashModel mSplashModel = new SplashModel();

    int getProfileSetUp() {
        return mSplashModel.isProfileSetUp();
    }

    String getAccessToken(){
        return mSplashModel.getAccessToken();
    }
}

