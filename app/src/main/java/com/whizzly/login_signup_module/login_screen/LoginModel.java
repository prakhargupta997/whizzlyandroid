package com.whizzly.login_signup_module.login_screen;

import android.app.Activity;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableField;
import android.widget.Toast;

import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.login_response.LoginResponseBean;
import com.whizzly.models.social_login_response.SocialLoginResponseBean;
import com.whizzly.models.social_signup_response.SocialSignupResponseBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class LoginModel extends BaseObservable {
    public ObservableField<String> email = new ObservableField<>();
    public ObservableField<String> loginPassword = new ObservableField<>();
    public Activity context;

    public LoginModel(Activity context) {
        this.context = context;
    }


    @Bindable
    public String getEmail() {
        return email.get();
    }

    public void setEmail(String email) {
        this.email.set(email);
        notifyPropertyChanged(BR.email);
    }

    @Bindable
    public String getLoginPassword() {
        return loginPassword.get();
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword.set(loginPassword);
        notifyPropertyChanged(BR.loginPassword);
    }


    private HashMap<String, Object> getLoginDetails() {
        HashMap<String, Object> loginDetails = new HashMap<>();
        loginDetails.put(AppConstants.NetworkConstants.LOGIN_PARAM_1, getEmail());
        loginDetails.put(AppConstants.NetworkConstants.LOGIN_PARAM_2, getLoginPassword());
        loginDetails.put(AppConstants.NetworkConstants.LOGIN_PARAM_3, AppConstants.NetworkConstants.DEVICE_TYPE);
        loginDetails.put(AppConstants.NetworkConstants.LOGIN_PARAM_4, AppUtils.getDeviceId(context));
        loginDetails.put(AppConstants.NetworkConstants.LOGIN_PARAM_5, DataManager.getInstance().getDeviceToken());
        return loginDetails;
    }

    public void hitLoginApi(final RichMediatorLiveData<LoginResponseBean> loginResponseBeanRichMediatorLiveData) {
        if(AppUtils.isNetworkAvailable(context)) {
            DataManager.getInstance().hitLoginApi(getLoginDetails()).enqueue(new NetworkCallback<LoginResponseBean>() {
                @Override
                public void onSuccess(LoginResponseBean loginResponseBean) {
                    loginResponseBeanRichMediatorLiveData.setValue(loginResponseBean);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    loginResponseBeanRichMediatorLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    loginResponseBeanRichMediatorLiveData.setError(t);
                }
            });
        }else {
            Toast.makeText(context, context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }


    public void hitSocialLoginApi(final RichMediatorLiveData<SocialLoginResponseBean> loginResponseBeanRichMediatorLiveData, HashMap<String, Object> socialHashMap, int startThrough) {
        DataManager.getInstance().hitSocialLogin(socialHashMap).enqueue(new NetworkCallback<SocialLoginResponseBean>() {
            @Override
            public void onSuccess(SocialLoginResponseBean loginResponseBean) {
                DataManager.getInstance().setLoginOrSignUpType(startThrough);
                loginResponseBeanRichMediatorLiveData.setValue(loginResponseBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                loginResponseBeanRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                loginResponseBeanRichMediatorLiveData.setError(t);
            }
        });
    }

    public String isValidEmail(){
        if(getEmail() == null){
            return context.getString(R.string.txt_error_empty_email_username);
        }else {
            if(getEmail().isEmpty()){
                return context.getString(R.string.txt_error_empty_email_username);
            }else{
                return null;
            }
        }
    }

    public String isPasswordValid(){
        if(getLoginPassword() == null){
            return context.getString(R.string.txt_error_empty_password);
        }else {
            if (getLoginPassword().isEmpty()) {
                return context.getString(R.string.txt_error_empty_password);
            } else {
                return null;
            }
        }
    }

    public void setAccessToken(String accessToken) {
        DataManager.getInstance().setAccessToken(accessToken);
    }

    public void setUserId(int userId) {
        DataManager.getInstance().setUserId(userId);
    }

    public void setIsProfileSetUp(int profileSetup) {
        DataManager.getInstance().setIsProfileSetUp(profileSetup);
    }

    public void saveEmail(String email) {
        DataManager.getInstance().setEmail(email);
    }

    public void setName(String name) {
        DataManager.getInstance().setName(name);
    }

    public void setProfileImage(String profileImage) {
        DataManager.getInstance().setProfilePic(profileImage);
    }

    public void setEmailAddress(String emailAddress){
        DataManager.getInstance().setEmail(emailAddress);
    }

    public void setProfileName(String profileName){
        DataManager.getInstance().saveUserName(profileName);
    }

    public void hitSocialSignUpApi(final RichMediatorLiveData<SocialSignupResponseBean> signUpBeanRichMediatorLiveData, HashMap<String, Object> socialDetails, int signupType) {
        DataManager.getInstance().hitSocialSignUpApi(socialDetails).enqueue(new NetworkCallback<SocialSignupResponseBean>() {
            @Override
            public void onSuccess(SocialSignupResponseBean signUpBean) {

               setSignUpType(signupType);
                signUpBeanRichMediatorLiveData.setValue(signUpBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                signUpBeanRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                signUpBeanRichMediatorLiveData.setError(t);
            }
        });
    }

    private void setSignUpType(int signupType) {
        if (signupType==AppConstants.ClassConstants.SOCIAL_TYPE_FACEBOOK)
            DataManager.getInstance().setLoginOrSignUpType(2);
        else if (signupType==AppConstants.ClassConstants.SOCIAL_TYPE_GOOGLE)
            DataManager.getInstance().setLoginOrSignUpType(3);
        else if (signupType==AppConstants.ClassConstants.SOCIAL_TYPE_INSTAGRAM)
            DataManager.getInstance().setLoginOrSignUpType(4);
    }

    public void setUserName(String userName) {
        DataManager.getInstance().saveUserName(userName);
    }

    public void setLoginType(int type) {
        DataManager.getInstance().setLoginOrSignUpType(type);
    }
}
