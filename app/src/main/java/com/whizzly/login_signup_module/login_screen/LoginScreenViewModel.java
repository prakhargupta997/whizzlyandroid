package com.whizzly.login_signup_module.login_screen;

import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.jaychang.sa.AuthCallback;
import com.jaychang.sa.SocialUser;
import com.jaychang.sa.facebook.SimpleAuth;
import com.whizzly.R;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.login_response.LoginResponseBean;
import com.whizzly.models.social_login_response.SocialLoginResponseBean;
import com.whizzly.models.social_signup_response.SocialSignupResponseBean;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class LoginScreenViewModel extends ViewModel {

    private int socialType;

    public void setmSocialUser(SocialUser mSocialUser) {
        this.mSocialUser = mSocialUser;
    }

    private SocialUser mSocialUser;
    private RichMediatorLiveData<LoginResponseBean> mLoginResponseBeanRichMediatorLiveData;
    private RichMediatorLiveData<SocialLoginResponseBean> mSocialLoginResponseBeanRichMediatorLiveData;
    private RichMediatorLiveData<SocialSignupResponseBean> mSocialSignupResponseBeanRichMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<FailureResponse> validateLiveData;
    private OnClickSendListener onClickSendListener;
    private LoginModel mLoginModel;

    public SocialUser getmSocialUser() {
        return mSocialUser;
    }

    public int getSocialType() {
        return socialType;
    }

    public void setSocialType(int socialType) {
        this.socialType = socialType;
    }

    public RichMediatorLiveData<SocialSignupResponseBean> getmSocialSignupResponseBeanRichMediatorLiveData() {
        return mSocialSignupResponseBeanRichMediatorLiveData;
    }

    public RichMediatorLiveData<SocialLoginResponseBean> getmSocialLoginResponseBeanRichMediatorLiveData() {
        return mSocialLoginResponseBeanRichMediatorLiveData;
    }

    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (mLoginResponseBeanRichMediatorLiveData == null) {
            mLoginResponseBeanRichMediatorLiveData = new RichMediatorLiveData<LoginResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

        if (mSocialLoginResponseBeanRichMediatorLiveData == null) {
            mSocialLoginResponseBeanRichMediatorLiveData = new RichMediatorLiveData<SocialLoginResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

        if (mSocialSignupResponseBeanRichMediatorLiveData == null) {
            mSocialSignupResponseBeanRichMediatorLiveData = new RichMediatorLiveData<SocialSignupResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

        if (validateLiveData == null)
            validateLiveData = new MutableLiveData<>();
    }

    public RichMediatorLiveData<LoginResponseBean> getLoginResponseLiveData() {
        return mLoginResponseBeanRichMediatorLiveData;
    }

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void emailValidation() {

    }

    public void onForgotPasswordClick() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.FORGOT_PASSWORD_FROM_LOGIN);
    }

    public void onLoginClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.LOGIN_CLICKED);

    }

    public void onFbLogin() {
        if (AppUtils.isNetworkAvailable(mLoginModel.context)) {
            List<String> scopes = Arrays.asList("email");
            setSocialType(AppConstants.ClassConstants.SOCIAL_TYPE_FACEBOOK);
            SimpleAuth.connectFacebook(scopes, new AuthCallback() {
                @Override
                public void onSuccess(SocialUser socialUser) {
                    mSocialUser = socialUser;
                    firebaseFacebookLogin(socialUser.accessToken);
                    if (socialUser != null) {
                        AppConstants.ClassConstants.SOCIAL_LOGIN_TYPE = AppConstants.ClassConstants.SOCIAL_TYPE_FACEBOOK;
                        mLoginModel.hitSocialLoginApi(getmSocialLoginResponseBeanRichMediatorLiveData(), getLoginHashMap(socialUser, AppConstants.ClassConstants.SOCIAL_LOGIN_TYPE), 2);
                    }
                }

                @Override
                public void onError(Throwable error) {
                    Log.e("onError: ", error.getMessage());
                }

                @Override
                public void onCancel() {

                }
            });
        } else {
            Toast.makeText(mLoginModel.context, mLoginModel.context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }


    public void onGoogleLogin() {
        if (AppUtils.isNetworkAvailable(mLoginModel.context)) {
            List<String> scope = Arrays.asList(
                    "profile", "email"
            );
            setSocialType(AppConstants.ClassConstants.SOCIAL_TYPE_GOOGLE);
            onClickSendListener.onClickSend(AppConstants.ClassConstants.GOOGLE_SIGNUP_CLICKED);
            /*com.jaychang.sa.google.SimpleAuth.connectGoogle(scope, new AuthCallback() {
                @Override
                public void onSuccess(SocialUser socialUser) {
                    mSocialUser = socialUser;
                    firebaseGoogleLogin(socialUser.accessToken);
                    AppConstants.ClassConstants.SOCIAL_LOGIN_TYPE = AppConstants.ClassConstants.SOCIAL_TYPE_GOOGLE;
                    mLoginModel.hitSocialLoginApi(getmSocialLoginResponseBeanRichMediatorLiveData(), getLoginHashMap(socialUser, AppConstants.ClassConstants.SOCIAL_LOGIN_TYPE), 3);
                }

                @Override
                public void onError(Throwable error) {

                }

                @Override
                public void onCancel() {

                }
            });*/
        } else {
            Toast.makeText(mLoginModel.context, mLoginModel.context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    public void onInstaLogin() {
        if (AppUtils.isNetworkAvailable(mLoginModel.context)) {
            List<String> scopes = Arrays.asList("follower_list");
            setSocialType(AppConstants.ClassConstants.SOCIAL_TYPE_INSTAGRAM);
            com.jaychang.sa.instagram.SimpleAuth.connectInstagram(scopes, new AuthCallback() {
                @Override
                public void onSuccess(SocialUser socialUser) {
                    mSocialUser = socialUser;
                    AppConstants.ClassConstants.SOCIAL_LOGIN_TYPE = AppConstants.ClassConstants.SOCIAL_TYPE_INSTAGRAM;
                    mLoginModel.hitSocialLoginApi(getmSocialLoginResponseBeanRichMediatorLiveData(), getLoginHashMap(socialUser, AppConstants.ClassConstants.SOCIAL_LOGIN_TYPE), 4);
                }

                @Override
                public void onError(Throwable error) {

                }

                @Override
                public void onCancel() {

                }
            });
        } else {
            Toast.makeText(mLoginModel.context, mLoginModel.context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    public HashMap<String, Object> getLoginHashMap(SocialUser socialUser, Integer socialType) {
        HashMap<String, Object> socialLoginMap = new HashMap<>();
        socialLoginMap.put(AppConstants.NetworkConstants.SOCIAL_TYPE_PARAM_1, socialUser.accessToken);
        socialLoginMap.put(AppConstants.NetworkConstants.SOCIAL_TYPE_PARAM_2, socialUser.fullName);
        if (socialType != 3) {
            socialLoginMap.put(AppConstants.NetworkConstants.SOCIAL_TYPE_PARAM_3, socialUser.email);
        }
        socialLoginMap.put(AppConstants.NetworkConstants.SOCIAL_TYPE_PARAM_4, socialUser.profilePictureUrl);
        socialLoginMap.put(AppConstants.NetworkConstants.SOCIAL_TYPE_PARAM_5, socialUser.userId);
        socialLoginMap.put(AppConstants.NetworkConstants.SOCIAL_TYPE_PARAM_6, socialType);
        socialLoginMap.put(AppConstants.NetworkConstants.SOCIAL_TYPE_PARAM_7, AppConstants.NetworkConstants.DEVICE_TYPE);
        socialLoginMap.put(AppConstants.NetworkConstants.SOCIAL_TYPE_PARAM_8, AppUtils.getDeviceId(mLoginModel.context));
        socialLoginMap.put(AppConstants.NetworkConstants.LOGIN_PARAM_5, DataManager.getInstance().getDeviceToken());
        return socialLoginMap;
    }

    public void onSignUpClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.SIGN_UP_FROM_LOGIN);
    }

    public void setModel(LoginModel mLoginModel) {
        this.mLoginModel = mLoginModel;
    }

    public void firebaseGoogleLogin(String accessToken) {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        AuthCredential credential = GoogleAuthProvider.getCredential(accessToken, null);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(mLoginModel.context, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            String token = FirebaseInstanceId.getInstance().getToken();
                            DataManager.getInstance().saveDeviceToken(token);
                        }
                    }
                });
    }

    private void firebaseFacebookLogin(String accessToken) {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(mLoginModel.context, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            String token = FirebaseInstanceId.getInstance().getToken();
                            DataManager.getInstance().saveDeviceToken(token);
                        }
                    }
                });
    }
}
