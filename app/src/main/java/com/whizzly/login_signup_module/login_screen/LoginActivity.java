package com.whizzly.login_signup_module.login_screen;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.jaychang.sa.SocialUser;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.ActivityLoginBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.login_signup_module.forgot_password.ForgotPasswordActivity;
import com.whizzly.login_signup_module.profile_setup.ProfileSetUpActivity;
import com.whizzly.login_signup_module.signup_screen.SignupActivity;
import com.whizzly.models.login_response.LoginResponseBean;
import com.whizzly.models.social_signup_response.SocialSignupResponseBean;
import com.whizzly.staticpages.StaticPagesActivity;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

public class LoginActivity extends BaseActivity implements OnClickSendListener {

    private ActivityLoginBinding mActivityLoginBinding;
    private LoginScreenViewModel mLoginScreenViewModel;
    private LoginModel mLoginModel;
    private FirebaseAuth mFirebaseAuth;
    private BroadcastReceiver tokenReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        clearStatusBar();
        getData();
        setSpannableString();
        setBroadcastReciever();
        LocalBroadcastManager.getInstance(this).registerReceiver(tokenReceiver, new IntentFilter(AppConstants.ClassConstants.TOKEN_RECIEVER));
    }

    private void setSpannableString() {
        String privacyPolicy = getString(R.string.privacy_policy);
        String termsCondition = getString(R.string.terms_and_condition);
        String byConti = getString(R.string.by_continueing_you_are_agreeing_to_our_privacy_policy_and_terms_of_service);
        SpannableString spannableString = new SpannableString(getString(R.string.by_continueing_you_are_agreeing_to_our_privacy_policy_and_terms_of_service));

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, StaticPagesActivity.class)
                        .putExtra(AppConstants.ClassConstants.IS_FOR, AppConstants.ClassConstants.PRIVACY_POLICY));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);

                ds.bgColor = Color.TRANSPARENT;
                ds.linkColor = Color.TRANSPARENT;
            }
        };
        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, StaticPagesActivity.class)
                        .putExtra(AppConstants.ClassConstants.IS_FOR, AppConstants.ClassConstants.TERMS_AND_CONDITION));

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.bgColor = Color.TRANSPARENT;
                ds.linkColor = Color.TRANSPARENT;
            }
        };

        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.text_color)), byConti.indexOf(privacyPolicy), byConti.indexOf(privacyPolicy) + privacyPolicy.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), byConti.indexOf(privacyPolicy), byConti.indexOf(privacyPolicy) + privacyPolicy.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(clickableSpan, byConti.indexOf(privacyPolicy), byConti.indexOf(privacyPolicy) + privacyPolicy.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.text_color)), byConti.indexOf(termsCondition), byConti.indexOf(termsCondition) + termsCondition.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), byConti.indexOf(termsCondition), byConti.indexOf(termsCondition) + termsCondition.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(clickableSpan1, byConti.indexOf(termsCondition), byConti.indexOf(termsCondition) + termsCondition.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        mActivityLoginBinding.tvConditionsAndPolicy.setText(spannableString);
        mActivityLoginBinding.tvConditionsAndPolicy.setMovementMethod(LinkMovementMethod.getInstance());
        mActivityLoginBinding.tvConditionsAndPolicy.setHighlightColor(Color.TRANSPARENT);
    }

    private void getData() {
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(AppConstants.ClassConstants.ON_LOGOUT_CLICKED)) {
            intent.getStringExtra(AppConstants.ClassConstants.ON_LOGOUT_CLICKED);
            mActivityLoginBinding.llToolbar.ivBack.setVisibility(View.GONE);
        }
    }

    private void firebaseLoginEmailPass(String email, String password) {
        mFirebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser user = mFirebaseAuth.getCurrentUser();
                    }

                });
    }

    private void init() {
        mActivityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        mLoginScreenViewModel = ViewModelProviders.of(this).get(LoginScreenViewModel.class);
        mActivityLoginBinding.setViewModel(mLoginScreenViewModel);
        mActivityLoginBinding.llToolbar.ivBack.setVisibility(View.VISIBLE);
        mActivityLoginBinding.llToolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mLoginModel = new LoginModel(this);
        mActivityLoginBinding.setModel(mLoginModel);
        mLoginScreenViewModel.setModel(mLoginModel);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mLoginScreenViewModel.setOnClickSendListener(this);
        mLoginScreenViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver());
        mLoginScreenViewModel.getLoginResponseLiveData().observe(this, new Observer<LoginResponseBean>() {
            @Override
            public void onChanged(@Nullable LoginResponseBean loginResponseBean) {
                mActivityLoginBinding.flProgressBar.setVisibility(View.GONE);
                if (loginResponseBean != null && loginResponseBean.getCode() == 200) {
                    Toast.makeText(LoginActivity.this, loginResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                    mLoginModel.setAccessToken(loginResponseBean.getResult().getAccessToken());
                    mLoginModel.setUserId(loginResponseBean.getResult().getUserId());
                    mLoginModel.setIsProfileSetUp(loginResponseBean.getResult().getProfileSetup());
                    mLoginModel.setUserName(loginResponseBean.getResult().getUserName());
                    mLoginModel.setLoginType(1);
                    if (loginResponseBean.getResult().getProfileImage() != null)
                        mLoginModel.setProfileImage(loginResponseBean.getResult().getProfileImage());
                    mLoginModel.setEmailAddress(loginResponseBean.getResult().getUserEmail());
                    sendToHomeorProfile(loginResponseBean.getResult().getProfileSetup());
                    finishAffinity();
                } else {
                    Toast.makeText(LoginActivity.this, loginResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        mLoginScreenViewModel.getmSocialLoginResponseBeanRichMediatorLiveData().observe(this, socialLoginResponseBean -> {
            if (socialLoginResponseBean != null && socialLoginResponseBean.getCode() == 200) {
                mLoginModel.setAccessToken(socialLoginResponseBean.getResult().getAccessToken());
                mLoginModel.setUserId(socialLoginResponseBean.getResult().getUserId());
                mLoginModel.setIsProfileSetUp(socialLoginResponseBean.getResult().getProfileSetup());
                mLoginModel.saveEmail(socialLoginResponseBean.getResult().getUserEmail());
                mLoginModel.setName(socialLoginResponseBean.getResult().getFirstName());
                mLoginModel.setUserName(socialLoginResponseBean.getResult().getUserName());
                if (socialLoginResponseBean.getResult().getProfileImage() != null)
                    mLoginModel.setProfileImage(socialLoginResponseBean.getResult().getProfileImage());
                sendToHomeorProfile(socialLoginResponseBean.getResult().getProfileSetup());
                Toast.makeText(LoginActivity.this, socialLoginResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                finishAffinity();
            } else if (socialLoginResponseBean != null && socialLoginResponseBean.getCode() == 424) {
                Log.e("onChanged: ", "social signup");

                mLoginModel.hitSocialSignUpApi(mLoginScreenViewModel.getmSocialSignupResponseBeanRichMediatorLiveData(), mLoginScreenViewModel.getLoginHashMap(mLoginScreenViewModel.getmSocialUser(), mLoginScreenViewModel.getSocialType()), AppConstants.ClassConstants.SOCIAL_LOGIN_TYPE);
            } else if (socialLoginResponseBean != null) {
                Toast.makeText(LoginActivity.this, socialLoginResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        mLoginScreenViewModel.getmSocialSignupResponseBeanRichMediatorLiveData().observe(this, new Observer<SocialSignupResponseBean>() {
            @Override
            public void onChanged(@Nullable SocialSignupResponseBean socialSignupResponseBean) {
                if (socialSignupResponseBean != null && socialSignupResponseBean.getCode() == 200) {
                    mLoginModel.setAccessToken(socialSignupResponseBean.getResult().getAccessToken());
                    mLoginModel.setUserId(socialSignupResponseBean.getResult().getUserId());
                    mLoginModel.setIsProfileSetUp(socialSignupResponseBean.getResult().getProfileSetup());
                    mLoginModel.saveEmail(socialSignupResponseBean.getResult().getUserEmail());
                    mLoginModel.setName(socialSignupResponseBean.getResult().getFirstName());
                    if (socialSignupResponseBean.getResult().getProfileImage() != null)
                        mLoginModel.setProfileImage(socialSignupResponseBean.getResult().getProfileImage());
                    sendToHomeorProfile(socialSignupResponseBean.getResult().getProfileSetup());
                    Toast.makeText(LoginActivity.this, socialSignupResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                    finishAffinity();
                } else if (socialSignupResponseBean != null) {
                    Toast.makeText(LoginActivity.this, socialSignupResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void sendToHomeorProfile(int result) {
        if (result == 1) {
            Intent intent = new Intent(this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, ProfileSetUpActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants
                    .ClassConstants.SIGN_UP_FROM_LOGIN:
                Intent loginIntent = new Intent(this, SignupActivity.class);
                loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loginIntent);
                break;
            case AppConstants
                    .ClassConstants.FORGOT_PASSWORD_FROM_LOGIN:
                Intent forgotPasswordIntent = new Intent(this, ForgotPasswordActivity.class);
                forgotPasswordIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(forgotPasswordIntent);
                break;
            case AppConstants.ClassConstants.LOGIN_CLICKED:
                if (mLoginModel.isPasswordValid() == null && mLoginModel.isValidEmail() == null) {
                    if (AppUtils.isNetworkAvailable(this)) {
                        mLoginModel.hitLoginApi(mLoginScreenViewModel.getLoginResponseLiveData());
                        mActivityLoginBinding.flProgressBar.setVisibility(View.VISIBLE);
                        firebaseLoginEmailPass(mLoginModel.getEmail(), mLoginModel.getLoginPassword());
                    } else {
                        Toast.makeText(this, R.string.txt_no_internet, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (mLoginModel.isValidEmail() != null) {
                        showToast(mLoginModel.isValidEmail());
                    } else if (mLoginModel.isPasswordValid() != null) {
                        showToast(mLoginModel.isPasswordValid());
                    }
                }
                break;
                case AppConstants.ClassConstants.GOOGLE_SIGNUP_CLICKED:
                    GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                            .requestEmail()
                            .build();
                    GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
                    Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                    startActivityForResult(signInIntent, 121);
                    break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 121){
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            SocialUser socialUser = new SocialUser();
            socialUser.profilePictureUrl = task.getResult().getPhotoUrl().toString();
            socialUser.accessToken = task.getResult().getId();
            socialUser.email = task.getResult().getEmail();
            socialUser.fullName = task.getResult().getDisplayName();
            socialUser.userId = task.getResult().getId();
            socialUser.username = task.getResult().getDisplayName();
            mLoginScreenViewModel.setmSocialUser(socialUser);
            mLoginScreenViewModel.firebaseGoogleLogin(socialUser.accessToken);
            mLoginModel.hitSocialLoginApi(mLoginScreenViewModel.getmSocialLoginResponseBeanRichMediatorLiveData(), mLoginScreenViewModel.getLoginHashMap(socialUser, AppConstants.ClassConstants.SOCIAL_TYPE_GOOGLE), 3);
        }
    }

    private void setBroadcastReciever() {
        tokenReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String token = intent.getStringExtra(AppConstants.ClassConstants.TOKEN);
                if (token != null) {
                    DataManager.getInstance().saveDeviceToken(token);
                }

            }
        };
    }

    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(tokenReceiver);
    }
}
