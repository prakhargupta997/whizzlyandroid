package com.whizzly.login_signup_module.login_signup_option;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.WindowManager;

import com.whizzly.R;
import com.whizzly.databinding.ActivityLoginSignupOptionBinding;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.login_signup_module.login_screen.LoginActivity;
import com.whizzly.login_signup_module.signup_screen.SignupActivity;
import com.whizzly.utils.AppConstants;

public class LoginSignupOptionActivity extends AppCompatActivity implements OnClickSendListener {


    private ActivityLoginSignupOptionBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login_signup_option);
        LoginSignupOptionViewModel loginSignupOptionViewModel = ViewModelProviders.of(this).get(LoginSignupOptionViewModel.class);
        mBinding.setViewModel(loginSignupOptionViewModel);
        loginSignupOptionViewModel.setOnClickSendListener(this);
        loginSignupOptionViewModel.isAnimation.set(true);
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.LOGIN_CLICKED:
                Intent loginIntent = new Intent(this, LoginActivity.class);
                loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(loginIntent);
                break;
            case AppConstants.ClassConstants.SIGNUP_CLICKED:
                Intent signUpIntent = new Intent(this, SignupActivity.class);
                signUpIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(signUpIntent);
                break;

        }
    }
}
