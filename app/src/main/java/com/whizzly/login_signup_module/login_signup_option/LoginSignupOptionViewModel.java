package com.whizzly.login_signup_module.login_signup_option;

import androidx.lifecycle.ViewModel;
import androidx.databinding.ObservableBoolean;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class LoginSignupOptionViewModel extends ViewModel {

    public ObservableBoolean isAnimation = new ObservableBoolean(false);
    private OnClickSendListener onClickSendListener;

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void onSignUpClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.SIGNUP_CLICKED);
    }

    public void onLoginClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.LOGIN_CLICKED);
    }
}
