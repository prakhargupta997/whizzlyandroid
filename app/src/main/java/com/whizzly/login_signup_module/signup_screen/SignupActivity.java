package com.whizzly.login_signup_module.signup_screen;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.jaychang.sa.SocialUser;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.ActivitySignupBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.login_signup_module.login_screen.LoginActivity;
import com.whizzly.login_signup_module.profile_setup.ProfileSetUpActivity;
import com.whizzly.models.social_login_response.SocialLoginResponseBean;
import com.whizzly.staticpages.StaticPagesActivity;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

public class SignupActivity extends BaseActivity implements OnClickSendListener {

    private SignUpViewModel signUpViewModel;
    private SignUpModel signUpModel;

    private ActivitySignupBinding mActivitySignupBinding;
    private BroadcastReceiver tokenReciever;
    private FirebaseAuth mFirebaseAuth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivitySignupBinding = DataBindingUtil.setContentView(this, R.layout.activity_signup);
        signUpViewModel = ViewModelProviders.of(this).get(SignUpViewModel.class);
        mActivitySignupBinding.setViewModel(signUpViewModel);
        mActivitySignupBinding.included.ivBack.setOnClickListener(view -> onBackPressed());
        signUpModel = new SignUpModel(this);
        signUpViewModel.setModel(signUpModel);
        mActivitySignupBinding.setModel(signUpModel);
        signUpViewModel.setOnClickListener(this);
        clearStatusBar();
        initializeFirebase();
        setObservers();
        setSpannableString();
        tokenReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String token = intent.getStringExtra(AppConstants.ClassConstants.TOKEN);
                if (token != null) {
                    DataManager.getInstance().saveDeviceToken(token);
                }
            }
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(tokenReciever, new IntentFilter(AppConstants.ClassConstants.TOKEN_RECIEVER));
    }

    private void setSpannableString() {
        String privacyPolicy = getString(R.string.privacy_policy);
        String termsCondition = getString(R.string.terms_and_condition);
        String byConti = getString(R.string.by_signing_up_you_are_agreeing_to_our_privacy_policy_and_terms_of_service);
        SpannableString spannableString = new SpannableString(getString(R.string.by_signing_up_you_are_agreeing_to_our_privacy_policy_and_terms_of_service));

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignupActivity.this, StaticPagesActivity.class)
                        .putExtra(AppConstants.ClassConstants.IS_FOR, AppConstants.ClassConstants.PRIVACY_POLICY));


            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);

                ds.bgColor = Color.TRANSPARENT;
                ds.linkColor = Color.TRANSPARENT;
            }
        };
        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignupActivity.this, StaticPagesActivity.class)
                        .putExtra(AppConstants.ClassConstants.IS_FOR, AppConstants.ClassConstants.TERMS_AND_CONDITION));

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.bgColor = Color.TRANSPARENT;
                ds.linkColor = Color.TRANSPARENT;
            }
        };

        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorPrimaryDark)), byConti.indexOf(privacyPolicy), byConti.indexOf(privacyPolicy) + privacyPolicy.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), byConti.indexOf(privacyPolicy), byConti.indexOf(privacyPolicy) + privacyPolicy.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(clickableSpan, byConti.indexOf(privacyPolicy), byConti.indexOf(privacyPolicy) + privacyPolicy.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.text_color)), byConti.indexOf(termsCondition), byConti.indexOf(termsCondition) + termsCondition.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), byConti.indexOf(termsCondition), byConti.indexOf(termsCondition) + termsCondition.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(clickableSpan1, byConti.indexOf(termsCondition), byConti.indexOf(termsCondition) + termsCondition.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        mActivitySignupBinding.tvConditionsAndPolicy.setText(spannableString);
        mActivitySignupBinding.tvConditionsAndPolicy.setMovementMethod(LinkMovementMethod.getInstance());
        mActivitySignupBinding.tvConditionsAndPolicy.setHighlightColor(Color.TRANSPARENT);
    }

    private void initializeFirebase() {
        mFirebaseAuth = FirebaseAuth.getInstance();
    }


    private void setObservers() {
        signUpViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver());
        signUpViewModel.getValidateUserLiveData().observe(this, validateUserBean -> {

        });


        signUpViewModel.getSignUpLiveData().observe(this, signUpBean -> {
            if (signUpBean != null) {
                mActivitySignupBinding.flProgressBar.setVisibility(View.GONE);
                if (signUpBean.getCode() == 200) {
                    signUpModel.setUserId(signUpBean.getResult().getUserId());
                    signUpModel.setAccessToken(signUpBean.getResult().getAccessToken());
                    signUpModel.setProfileSetUp(signUpBean.getResult().getProfileSetup());
                    signUpModel.saveUserName(signUpBean.getResult().getUserName());
                    signUpModel.saveEmail(signUpBean.getResult().getUserEmail());
                    signUpModel.setSignUpType(1);
                    sendToProfileSetup(signUpBean.getResult().getProfileSetup());
                    Toast.makeText(SignupActivity.this, signUpBean.getMessage(), Toast.LENGTH_SHORT).show();
                    finishAffinity();
                } else
                    Toast.makeText(SignupActivity.this, signUpBean.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        signUpViewModel.getSocialLoginResponseBeanRichMediatorLiveData().observe(this, new Observer<SocialLoginResponseBean>() {
            @Override
            public void onChanged(@Nullable SocialLoginResponseBean socialLoginResponseBean) {
                if (socialLoginResponseBean != null && socialLoginResponseBean.getCode() == 200) {
                    signUpModel.setUserId(socialLoginResponseBean.getResult().getUserId());
                    signUpModel.setAccessToken(socialLoginResponseBean.getResult().getAccessToken());
                    signUpModel.setProfileSetUp(socialLoginResponseBean.getResult().getProfileSetup());
                    signUpModel.setProfileImage(socialLoginResponseBean.getResult().getProfileImage());
                    if (socialLoginResponseBean.getResult().getUserEmail() != null)
                        signUpModel.saveEmail(socialLoginResponseBean.getResult().getUserEmail());
                    setSocialLoginType(socialLoginResponseBean.getResult().getSignupType());
                    signUpModel.saveName(socialLoginResponseBean.getResult().getFirstName());
                    sendToProfileSetup(socialLoginResponseBean.getResult().getProfileSetup());
                    DataManager.getInstance().saveUserName(socialLoginResponseBean.getResult().getUserName());
                    Toast.makeText(SignupActivity.this, socialLoginResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                    finishAffinity();
                }
            }
        });

        signUpViewModel.getSocialSignupResponseBeanRichMediatorLiveData().observe(this, socialSignupResponseBean -> {
            if (socialSignupResponseBean != null && socialSignupResponseBean.getCode() == 200) {
                sendToProfileSetup(socialSignupResponseBean.getResult().getProfileSetup());
                signUpModel.setUserId(socialSignupResponseBean.getResult().getUserId());
                signUpModel.setAccessToken(socialSignupResponseBean.getResult().getAccessToken());
                signUpModel.setProfileSetUp(socialSignupResponseBean.getResult().getProfileSetup());
                if (socialSignupResponseBean.getResult().getUserEmail() != null)
                    signUpModel.saveEmail(socialSignupResponseBean.getResult().getUserEmail());
                signUpModel.saveName(socialSignupResponseBean.getResult().getFirstName());
                Toast.makeText(SignupActivity.this, socialSignupResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                finishAffinity();
            } else if (socialSignupResponseBean.getCode() == 424) {
                signUpModel.hitSocialLoginApi(signUpViewModel.getSocialLoginResponseBeanRichMediatorLiveData(), signUpViewModel.getSignUpHashMap(signUpViewModel.getmSocialUser(), signUpViewModel.getSocialType()));
            } else
                Toast.makeText(SignupActivity.this, socialSignupResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }

    private void setSocialLoginType(int signupType) {
        if (signupType == AppConstants.ClassConstants.SOCIAL_TYPE_FACEBOOK)
            DataManager.getInstance().setLoginOrSignUpType(2);
        else if (signupType == AppConstants.ClassConstants.SOCIAL_TYPE_GOOGLE)
            DataManager.getInstance().setLoginOrSignUpType(3);
        else if (signupType == AppConstants.ClassConstants.SOCIAL_TYPE_INSTAGRAM)
            DataManager.getInstance().setLoginOrSignUpType(4);

    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.LOGIN_CLICKED_FROM_SIGNUP:
                Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case AppConstants.ClassConstants.SIGNUP_CLICKED:
                if (signUpModel.isUserNameEnteredValid() == null && signUpModel.isEmailValid() == null && signUpModel.isConfirmPasswordValid() == null && signUpModel.isEmptyPasswordValid() == null) {
                    mActivitySignupBinding.flProgressBar.setVisibility(View.VISIBLE);
                    signUpModel.hitSignUpApi(signUpViewModel.getSignUpLiveData());
                    firebaseLoginEmailPass(signUpModel.getEmail(), signUpModel.getPassword());
                } else {
                    if (signUpModel.isUserNameEnteredValid() != null) {
                        showToast(signUpModel.isUserNameEnteredValid());
                    } else if (signUpModel.isEmailValid() != null) {
                        showToast(signUpModel.isEmailValid());
                    } else if (signUpModel.isEmptyPasswordValid() != null) {
                        showToast(signUpModel.isEmptyPasswordValid());
                    } else if (signUpModel.isConfirmPasswordValid() != null) {
                        showToast(signUpModel.isConfirmPasswordValid());
                    }
                }
                break;
            case AppConstants.ClassConstants.GOOGLE_SIGNUP_CLICKED:
                GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .build();
                GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, 121);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 121) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            SocialUser socialUser = new SocialUser();
            socialUser.profilePictureUrl = task.getResult().getPhotoUrl().toString();
            socialUser.accessToken = task.getResult().getId();
            socialUser.email = task.getResult().getEmail();
            socialUser.fullName = task.getResult().getDisplayName();
            socialUser.username = task.getResult().getDisplayName();
            socialUser.userId = task.getResult().getId();
            signUpViewModel.setmSocialUser(socialUser);
            signUpViewModel.firebaseGoogleLogin(socialUser.accessToken);
            signUpModel.hitSocialSignUpApi(signUpViewModel.getSocialSignupResponseBeanRichMediatorLiveData(), signUpViewModel.getSignUpHashMap(socialUser, AppConstants.ClassConstants.SOCIAL_TYPE_GOOGLE), 3);
        }
    }

    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    private void sendToProfileSetup(int profileSetup) {
        if (profileSetup == 0) {
            Intent intent = new Intent(this, ProfileSetUpActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    private void firebaseLoginEmailPass(String email, String password) {
        mFirebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser user = mFirebaseAuth.getCurrentUser();
                    }
                });
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(tokenReciever);
    }
}
