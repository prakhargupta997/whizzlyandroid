package com.whizzly.login_signup_module.signup_screen;

import android.app.Activity;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import android.widget.Toast;

import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.sign_up_beans.SignUpBean;
import com.whizzly.models.social_login_response.SocialLoginResponseBean;
import com.whizzly.models.social_signup_response.SocialSignupResponseBean;
import com.whizzly.models.validate_user_bean.ValidateUserBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class SignUpModel extends BaseObservable {
    public Activity getContext() {
        return context;
    }

    public Activity context;
    private ObservableBoolean isUserNameValid = new ObservableBoolean(false), isSearchingUserName = new ObservableBoolean(false);
    private ObservableField<String> username = new ObservableField<>();
    private ObservableField<String> email = new ObservableField<>();
    private ObservableField<String> password = new ObservableField<>();
    private ObservableField<String> confirmPassword = new ObservableField<>();

    public SignUpModel(Activity context) {
        this.context = context;
    }

    @Bindable
    public boolean isUserNameValid() {
        return isUserNameValid.get();
    }

    public void setUserNameValid(boolean userNameValid) {
        isUserNameValid.set(userNameValid);
        notifyPropertyChanged(BR.userNameValid);
    }

    @Bindable
    public boolean isSearchingUserName() {
        return isSearchingUserName.get();
    }

    public void setSearchingUserName(boolean searchingUserName) {
        isSearchingUserName.set(searchingUserName);
        notifyPropertyChanged(BR.searchingUserName);
    }

    @Bindable
    public String getUsername() {
        return username.get();
    }

    public void setUsername(String username) {
        this.username.set(username);
        notifyPropertyChanged(BR.username);

    }

    @Bindable
    public String getEmail() {
        return email.get();
    }

    public void setEmail(String email) {
        this.email.set(email);
        notifyPropertyChanged(BR.email);
    }

    @Bindable
    public String getPassword() {
        return password.get();
    }

    public void setPassword(String password) {
        this.password.set(password);
        notifyPropertyChanged(BR.password);
    }

    @Bindable
    public String getConfirmPassword() {
        return confirmPassword.get();
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword.set(confirmPassword);
        notifyPropertyChanged(BR.confirmPassword);
    }

    public void hitValidateUsername(final RichMediatorLiveData<ValidateUserBean> validateUserBeanRichMediatorLiveData) {
        setSearchingUserName(true);
        DataManager.getInstance().hitValidateUsername(getUsername(), DataManager.getInstance().getLanguage()).enqueue(new NetworkCallback<ValidateUserBean>() {
            @Override
            public void onSuccess(ValidateUserBean validateUserBean) {
                if(validateUserBean != null) {
                    if (validateUserBean.getCode() == 200) {
                        validateUserBeanRichMediatorLiveData.setValue(validateUserBean);
                        setSearchingUserName(false);
                        setUserNameValid(true);
                    } else {
                        validateUserBeanRichMediatorLiveData.setValue(validateUserBean);
                        setSearchingUserName(false);
                        setUserNameValid(false);
                        Toast.makeText(context, validateUserBean.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                validateUserBeanRichMediatorLiveData.setFailure(failureResponse);
                setSearchingUserName(false);
                setUserNameValid(false);
            }

            @Override
            public void onError(Throwable t) {
                validateUserBeanRichMediatorLiveData.setError(t);
                setSearchingUserName(false);
                setUserNameValid(false);
            }
        });
    }

    private HashMap<String, Object> setSignUpData() {
        HashMap<String, Object> signUpDetails = new HashMap<>();
        signUpDetails.put(AppConstants.NetworkConstants.SIGNUP_PARAM_1, getEmail());
        signUpDetails.put(AppConstants.NetworkConstants.SIGNUP_PARAM_2, getPassword());
        signUpDetails.put(AppConstants.NetworkConstants.SIGNUP_PARAM_3, getUsername());
        signUpDetails.put(AppConstants.NetworkConstants.SIGNUP_PARAM_4, AppConstants.NetworkConstants.DEVICE_TYPE);
        signUpDetails.put(AppConstants.NetworkConstants.SIGNUP_PARAM_5, AppUtils.getDeviceId(context));
        signUpDetails.put(AppConstants.NetworkConstants.SIGNUP_PARAM_6, AppConstants.NetworkConstants.SIGN_UP_TYPE_NORMAL);
        signUpDetails.put(AppConstants.NetworkConstants.SIGNUP_PARAM_7, DataManager.getInstance().getDeviceToken());
        return signUpDetails;
    }

    public void hitSignUpApi(final RichMediatorLiveData<SignUpBean> signUpBeanRichMediatorLiveData) {
        if(AppUtils.isNetworkAvailable(context)) {
            DataManager.getInstance().hitNormalSignUpApi(setSignUpData()).enqueue(new NetworkCallback<SignUpBean>() {
                @Override
                public void onSuccess(SignUpBean signUpBean) {
                    signUpBeanRichMediatorLiveData.setValue(signUpBean);
                }

                @Override
                public void onFailure(FailureResponse failureResponse) {
                    signUpBeanRichMediatorLiveData.setFailure(failureResponse);
                }

                @Override
                public void onError(Throwable t) {
                    signUpBeanRichMediatorLiveData.setError(t);
                }
            });
        }else {
            Toast.makeText(context, R.string.txt_no_internet, Toast.LENGTH_SHORT).show();
        }
    }

    public String isUserNameEnteredValid() {
        if (getUsername() == null) {
            return context.getString(R.string.txt_empty_username);
        } else {
            if (getUsername().length() == 0) {
                return context.getString(R.string.txt_empty_username);
            } else if (getUsername().contains(" ")) {
                return context.getString(R.string.txt_enter_valid_username);
            } else if (getUsername().length() <= 1) {
                return context.getString(R.string.txt_least_username_characters);
            } else if (getUsername().length() > 20) {
                return context.getString(R.string.txt_longest_username);
            } else {
                return null;
            }
        }
    }

    public String isEmailValid() {
        if (getEmail() == null) {
            return context.getString(R.string.txt_error_empty_email);
        } else {
            if (getEmail().isEmpty()) {
                return context.getString(R.string.txt_error_empty_email);
            } else if (!AppUtils.isEmailValid(getEmail())) {
                return context.getString(R.string.txt_email_invalid);
            } else {
                return null;
            }
        }
    }

    public String isEmptyPasswordValid() {
        if(getPassword() == null){
            return context.getString(R.string.txt_error_empty_password);
        }else {
            if (getPassword().isEmpty()) {
                return context.getString(R.string.txt_error_empty_password);
            } else if (getPassword().length() < 8) {
                return context.getString(R.string.txt_error_password_length);
            } else {
                return null;
            }
        }
    }

    public String isConfirmPasswordValid() {
        if(getConfirmPassword() == null){
            return context.getString(R.string.txt_error_empty_confirm_pass);
        }else {
            if (getConfirmPassword().isEmpty()) {
                return context.getString(R.string.txt_error_empty_confirm_pass);
            } else /*if (getConfirmPassword().length() < 8) {
                return context.getString(R.string.txt_error_password_length);
            } else */if (!getConfirmPassword().equals(getPassword())) {
                return context.getString(R.string.txt_error_pass_confirm_pass);
            } else {
                return null;
            }
        }
    }

    public void hitSocialSignUpApi(final RichMediatorLiveData<SocialSignupResponseBean> signUpBeanRichMediatorLiveData, HashMap<String, Object> socialDetails, int signupType) {
        DataManager.getInstance().hitSocialSignUpApi(socialDetails).enqueue(new NetworkCallback<SocialSignupResponseBean>() {
            @Override
            public void onSuccess(SocialSignupResponseBean signUpBean) {
                DataManager.getInstance().setLoginOrSignUpType(signupType);
                signUpBeanRichMediatorLiveData.setValue(signUpBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                signUpBeanRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                signUpBeanRichMediatorLiveData.setError(t);
            }
        });
    }

    public void setAccessToken(String accessToken) {
        DataManager.getInstance().setAccessToken(accessToken);
    }

    public void setProfileSetUp(int isProfileSetUp) {
        DataManager.getInstance().setIsProfileSetUp(isProfileSetUp);
    }

    public void setUserId(int userId) {
        DataManager.getInstance().setUserId(userId);
    }
    public void saveUserName(String username) {
        DataManager.getInstance().saveUserName(username);
    }

    public void saveEmail(String userEmail) {
        DataManager.getInstance().setEmail(userEmail);
    }

    public void saveName(String firstName) {
        DataManager.getInstance().setName(firstName);
    }

    public void saveImageUrl(String profileImage) {
        DataManager.getInstance().setProfilePic(profileImage);
    }

    public void hitSocialLoginApi(final RichMediatorLiveData<SocialLoginResponseBean> loginResponseBeanRichMediatorLiveData, HashMap<String, Object> socialHashMap) {
        DataManager.getInstance().hitSocialLogin(socialHashMap).enqueue(new NetworkCallback<SocialLoginResponseBean>() {
            @Override
            public void onSuccess(SocialLoginResponseBean loginResponseBean) {
                loginResponseBeanRichMediatorLiveData.setValue(loginResponseBean);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
                loginResponseBeanRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
                loginResponseBeanRichMediatorLiveData.setError(t);
            }
        });
    }

    public void setProfileImage(String profileImage) {
        DataManager.getInstance().setProfilePic(profileImage);
    }

    public void setSignUpType(int type) {
        DataManager.getInstance().setLoginOrSignUpType(type);
    }
}
