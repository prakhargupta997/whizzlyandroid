package com.whizzly.login_signup_module.signup_screen;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.annotation.NonNull;
import android.text.Editable;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.jaychang.sa.AuthCallback;
import com.jaychang.sa.SocialUser;
import com.jaychang.sa.facebook.SimpleAuth;
import com.whizzly.R;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.sign_up_beans.SignUpBean;
import com.whizzly.models.social_login_response.SocialLoginResponseBean;
import com.whizzly.models.social_signup_response.SocialSignupResponseBean;
import com.whizzly.models.validate_user_bean.ValidateUserBean;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class SignUpViewModel extends ViewModel {
    private RichMediatorLiveData<ValidateUserBean> mValidateUserBeanRichMediatorLiveData;
    private RichMediatorLiveData<SocialSignupResponseBean> mSocialSignupResponseBeanRichMediatorLiveData;
    private RichMediatorLiveData<SocialLoginResponseBean> mSocialLoginResponseBeanRichMediatorLiveData;
    private RichMediatorLiveData<SignUpBean> signUpLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private MutableLiveData<FailureResponse> validateLiveData;
    private SignUpModel mSignUpModel;
    private SocialUser mSocialUser;
    private int socialType;
    private OnClickSendListener onClickListener;

    public SocialUser getmSocialUser() {
        return mSocialUser;
    }

    public void setmSocialUser(SocialUser mSocialUser) {
        this.mSocialUser = mSocialUser;
    }

    public int getSocialType() {
        return socialType;
    }

    public void setSocialType(int socialType) {
        this.socialType = socialType;
    }

    public RichMediatorLiveData<SocialLoginResponseBean> getSocialLoginResponseBeanRichMediatorLiveData() {
        return mSocialLoginResponseBeanRichMediatorLiveData;
    }

    public RichMediatorLiveData<SocialSignupResponseBean> getSocialSignupResponseBeanRichMediatorLiveData() {
        return mSocialSignupResponseBeanRichMediatorLiveData;
    }

    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureObserver;
        initLiveData();
    }

    void setOnClickListener(OnClickSendListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    private void initLiveData() {
        if (signUpLiveData == null) {
            signUpLiveData = new RichMediatorLiveData<SignUpBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

        if (mSocialSignupResponseBeanRichMediatorLiveData == null) {
            mSocialSignupResponseBeanRichMediatorLiveData = new RichMediatorLiveData<SocialSignupResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

        if (mValidateUserBeanRichMediatorLiveData == null) {
            mValidateUserBeanRichMediatorLiveData = new RichMediatorLiveData<ValidateUserBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

        if (mSocialLoginResponseBeanRichMediatorLiveData == null) {
            mSocialLoginResponseBeanRichMediatorLiveData = new RichMediatorLiveData<SocialLoginResponseBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }

        if (validateLiveData == null)
            validateLiveData = new MutableLiveData<>();
    }

    public RichMediatorLiveData<SignUpBean> getSignUpLiveData() {
        return signUpLiveData;
    }

    public RichMediatorLiveData<ValidateUserBean> getValidateUserLiveData() {
        return mValidateUserBeanRichMediatorLiveData;
    }

    public void onSignUpClicked() {
        onClickListener.onClickSend(AppConstants.ClassConstants.SIGNUP_CLICKED);
    }

    public void onLoginClicked() {
        onClickListener.onClickSend(AppConstants.ClassConstants.LOGIN_CLICKED_FROM_SIGNUP);
    }

    public void onFbClicked() {
        if (AppUtils.isNetworkAvailable(mSignUpModel.getContext())) {
            List<String> scopes = Arrays.asList("email");
            setSocialType(AppConstants.ClassConstants.SOCIAL_TYPE_FACEBOOK);
            SimpleAuth.connectFacebook(scopes, new AuthCallback() {
                @Override
                public void onSuccess(SocialUser socialUser) {
                    if (socialUser != null) {
                        mSignUpModel.hitSocialSignUpApi(getSocialSignupResponseBeanRichMediatorLiveData(), getSignUpHashMap(socialUser, AppConstants.ClassConstants.SOCIAL_TYPE_FACEBOOK),2);
                        setmSocialUser(socialUser);
                        firebaseFacebookLogin(socialUser.accessToken);
                    }
                }

                @Override
                public void onError(Throwable error) {
                    Log.e("onError: ", error.getMessage());
                }

                @Override
                public void onCancel() {

                }
            });
        } else {
            Toast.makeText(mSignUpModel.getContext(), mSignUpModel.getContext().getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }


    public HashMap<String, Object> getSignUpHashMap(SocialUser socialUser, Integer socialType) {
        HashMap<String, Object> socialHashMap = new HashMap<>();
        socialHashMap.put(AppConstants.NetworkConstants.SOCIAL_TYPE_PARAM_1, socialUser.accessToken);
        socialHashMap.put(AppConstants.NetworkConstants.SOCIAL_TYPE_PARAM_2, socialUser.fullName);
        socialHashMap.put(AppConstants.NetworkConstants.SOCIAL_TYPE_PARAM_3, socialUser.email);
        socialHashMap.put(AppConstants.NetworkConstants.SOCIAL_TYPE_PARAM_4, socialUser.profilePictureUrl);
        socialHashMap.put(AppConstants.NetworkConstants.SOCIAL_TYPE_PARAM_5, socialUser.userId);
        socialHashMap.put(AppConstants.NetworkConstants.SOCIAL_TYPE_PARAM_6, socialType);
        socialHashMap.put(AppConstants.NetworkConstants.SOCIAL_TYPE_PARAM_7, AppConstants.NetworkConstants.DEVICE_TYPE);
        socialHashMap.put(AppConstants.NetworkConstants.SOCIAL_TYPE_PARAM_8, AppUtils.getDeviceId(mSignUpModel.context));
        socialHashMap.put(AppConstants.NetworkConstants.LOGIN_PARAM_5, DataManager.getInstance().getDeviceToken());

        return socialHashMap;
    }

    public void onGoogleClicked() {

        if (AppUtils.isNetworkAvailable(mSignUpModel.getContext())) {
            List<String> scope = Arrays.asList(
                    "profile", "email"
            );

            setSocialType(AppConstants.ClassConstants.SOCIAL_TYPE_GOOGLE);

            onClickListener.onClickSend(AppConstants.ClassConstants.GOOGLE_SIGNUP_CLICKED);
            /*com.jaychang.sa.google.SimpleAuth.connectGoogle(scope, new AuthCallback() {
                @Override
                public void onSuccess(SocialUser socialUser) {
                    setmSocialUser(socialUser);
                    firebaseGoogleLogin(socialUser.accessToken);
                    mSignUpModel.hitSocialSignUpApi(getSocialSignupResponseBeanRichMediatorLiveData(), getSignUpHashMap(socialUser, AppConstants.ClassConstants.SOCIAL_TYPE_GOOGLE),3);
                }

                @Override
                public void onError(Throwable error) {

                }

                @Override
                public void onCancel() {

                }
            });*/
        } else {
            Toast.makeText(mSignUpModel.getContext(), mSignUpModel.getContext().getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    public void onInstaClicked() {
        if (AppUtils.isNetworkAvailable(mSignUpModel.getContext())) {
            List<String> scopes = Arrays.asList("follower_list");
            setSocialType(AppConstants.ClassConstants.SOCIAL_TYPE_INSTAGRAM);
            com.jaychang.sa.instagram.SimpleAuth.connectInstagram(scopes, new AuthCallback() {
                @Override
                public void onSuccess(SocialUser socialUser) {
                    setmSocialUser(socialUser);
                    mSignUpModel.hitSocialSignUpApi(getSocialSignupResponseBeanRichMediatorLiveData(), getSignUpHashMap(socialUser, AppConstants.ClassConstants.SOCIAL_TYPE_INSTAGRAM),4);
                }

                @Override
                public void onError(Throwable error) {

                }

                @Override
                public void onCancel() {

                }
            });
        } else {
            Toast.makeText(mSignUpModel.getContext(), mSignUpModel.getContext().getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    public void afterTextChanged(Editable s) {
        mSignUpModel.setUsername(s.toString());
        mSignUpModel.setSearchingUserName(false);
        mSignUpModel.setUserNameValid(false);
        if (s.toString().length() >= 2) {
            if (AppUtils.isNetworkAvailable(mSignUpModel.getContext())) {
                mSignUpModel.hitValidateUsername(mValidateUserBeanRichMediatorLiveData);
            } else {
                Toast.makeText(mSignUpModel.getContext(), mSignUpModel.getContext().getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void onEmailChanged(Editable s) {
        mSignUpModel.setEmail(s.toString());
    }


    public void setModel(SignUpModel signUpModel) {
        mSignUpModel = signUpModel;
    }

    public void firebaseGoogleLogin(String accessToken) {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        AuthCredential credential = GoogleAuthProvider.getCredential(accessToken, null);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(mSignUpModel.getContext(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            String token = FirebaseInstanceId.getInstance().getToken();
                            DataManager.getInstance().saveDeviceToken(token);
                        }
                    }
                });
    }

    private void firebaseFacebookLogin(String accessToken) {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(mSignUpModel.getContext(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            String token = FirebaseInstanceId.getInstance().getToken();
                            DataManager.getInstance().saveDeviceToken(token);
                        }
                    }
                });
    }
}

