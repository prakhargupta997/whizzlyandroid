package com.whizzly.login_signup_module.reset_password;

import android.content.Context;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import android.widget.Toast;

import com.whizzly.BR;
import com.whizzly.R;
import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.forgot_password.ResetPasswordBean;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class ResetPasswordModel extends BaseObservable {

    public ObservableField<String> newPassword = new ObservableField<>("");
    public ObservableField<String> confirmNewPassword = new ObservableField<>("");
    public ObservableBoolean isShowPassword = new ObservableBoolean(false);
    public ObservableBoolean isConfirmShowPassword = new ObservableBoolean(false);
    public Context context;

    public ResetPasswordModel(Context context) {
        this.context = context;
    }

    @Bindable
    public boolean getIsShowPassword() {
        return isShowPassword.get();
    }

    public void setIsShowPassword(boolean isShowPassword) {
        this.isShowPassword.set(isShowPassword);
        notifyPropertyChanged(BR.isShowPassword);
    }

    @Bindable
    public boolean getIsConfirmShowPassword() {
        return isConfirmShowPassword.get();
    }

    public void setIsConfirmShowPassword(boolean isConfirmShowPassword) {
        this.isConfirmShowPassword.set(isConfirmShowPassword);
        notifyPropertyChanged(BR.isConfirmShowPassword);
    }

    @Bindable
    public String getNewPassword() {
        return newPassword.get();
    }


    public void setNewPassword(String newPassword) {
        this.newPassword.set(newPassword);
        notifyPropertyChanged(BR.newPassword);

    }

    @Bindable
    public String getConfirmNewPassword() {
        return confirmNewPassword.get();
    }

    public void setConfirmNewPassword(String confirmNewPassword) {
        this.confirmNewPassword.set(confirmNewPassword);
        notifyPropertyChanged(BR.confirmNewPassword);

    }

    private String isValidNewPassword() {
        if (getNewPassword().isEmpty()) {
            return (context.getString(R.string.txt_error_empty_new_password));
        } else if (getNewPassword().length() < 8) {
            return (context.getString(R.string.txt_error_password_length));
        } else {
            return null;
        }
    }

    private String isValidConfirmPassword() {
        if (getConfirmNewPassword().isEmpty()) {
            return context.getString(R.string.txt_error_empty_confirm_new_pass);
        } else if (getConfirmNewPassword().length() < 8) {
            return context.getString(R.string.txt_error_password_length);
        } else if (!getConfirmNewPassword().equals(getNewPassword())) {
            return context.getString(R.string.txt_error_pass_confirm_pass);
        } else {
            return null;
        }
    }


    public void hitResetPasswordApi(RichMediatorLiveData<ResetPasswordBean> richMediatorLiveData, HashMap<String, Object> resetPassMap) {
        if(AppUtils.isNetworkAvailable(context)) {
            if (isValidNewPassword() == null && isValidConfirmPassword() == null) {
                DataManager.getInstance().hitResetPasswordApi(resetPassMap).enqueue(new NetworkCallback<ResetPasswordBean>() {
                    @Override
                    public void onSuccess(ResetPasswordBean resetPasswordBean) {
                        richMediatorLiveData.setValue(resetPasswordBean);
                    }

                    @Override
                    public void onFailure(FailureResponse failureResponse) {
                        richMediatorLiveData.setFailure(failureResponse);
                    }

                    @Override
                    public void onError(Throwable t) {
                        richMediatorLiveData.setError(t);
                    }
                });
            } else {
                if (isValidNewPassword() != null) {
                    Toast.makeText(context, isValidNewPassword(), Toast.LENGTH_SHORT).show();
                } else if (isValidConfirmPassword() != null) {
                    Toast.makeText(context, isValidConfirmPassword(), Toast.LENGTH_SHORT).show();
                }
            }
        }else {
            Toast.makeText(context, context.getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    public int getUserId() {
        return DataManager.getInstance().getUserId();
    }
}
