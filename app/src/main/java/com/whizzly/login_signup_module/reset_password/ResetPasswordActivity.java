package com.whizzly.login_signup_module.reset_password;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;

import android.view.View;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.ActivityResetPasswordBinding;
import com.whizzly.login_signup_module.login_screen.LoginActivity;
import com.whizzly.models.forgot_password.ResetPasswordBean;
import com.whizzly.utils.DataManager;

public class ResetPasswordActivity extends BaseActivity {

    private ActivityResetPasswordBinding mActivityResetPasswordBinding;
    private ResetPasswordModel mResetPasswordModel;
    private ResetPasswordViewModel mResetPasswordViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityResetPasswordBinding = DataBindingUtil.setContentView(this, R.layout.activity_reset_password);
        clearStatusBar();
        mResetPasswordModel = new ResetPasswordModel(this);
        mResetPasswordViewModel = ViewModelProviders.of(this).get(ResetPasswordViewModel.class);
        mActivityResetPasswordBinding.setModel(mResetPasswordModel);
        mActivityResetPasswordBinding.setViewModel(mResetPasswordViewModel);
        mResetPasswordViewModel.setResetPasswordModel(mResetPasswordModel);
        mActivityResetPasswordBinding.toolbar.ivBack.setOnClickListener(view -> onBackPressed());
        mResetPasswordViewModel.setGenericListeners(getErrorObserver(), getFailureResponseObserver());
        mResetPasswordViewModel.getResetPasswordBeanRichMediatorLiveData().observe(this, new Observer<ResetPasswordBean>() {
            @Override
            public void onChanged(@Nullable ResetPasswordBean resetPasswordBean) {
                if (resetPasswordBean != null) {
                    if (resetPasswordBean.getCode() == 200) {
                        Toast.makeText(ResetPasswordActivity.this, R.string.txt_password_reset_success, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ResetPasswordActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }else{
                        Toast.makeText(ResetPasswordActivity.this, resetPasswordBean.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ResetPasswordActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
