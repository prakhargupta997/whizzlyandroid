package com.whizzly.login_signup_module.reset_password;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.forgot_password.ResetPasswordBean;
import com.whizzly.utils.AppConstants;

import java.util.HashMap;

public class ResetPasswordViewModel extends ViewModel {
    private RichMediatorLiveData<ResetPasswordBean> mResetPasswordBeanRichMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;
    private ResetPasswordModel mResetPasswordModel;

    public RichMediatorLiveData<ResetPasswordBean> getResetPasswordBeanRichMediatorLiveData() {
        return mResetPasswordBeanRichMediatorLiveData;
    }

    public void setGenericListeners(Observer<Throwable> errorObserver,
                                    Observer<FailureResponse> failureObserver) {
        this.errorObserver = errorObserver;
        this.failureResponseObserver = failureObserver;
        initLiveData();
    }

    private void initLiveData() {
        if (mResetPasswordBeanRichMediatorLiveData == null) {
            mResetPasswordBeanRichMediatorLiveData = new RichMediatorLiveData<ResetPasswordBean>() {
                @Override
                protected Observer<FailureResponse> getFailureObserver() {
                    return failureResponseObserver;
                }

                @Override
                protected Observer<Throwable> getErrorObserver() {
                    return errorObserver;
                }
            };
        }
    }

    public void setResetPasswordModel(ResetPasswordModel mResetPasswordModel) {
        this.mResetPasswordModel = mResetPasswordModel;
    }

    private HashMap<String, Object> setResetPasswordMap() {
        HashMap<String, Object> resetPasswordMap = new HashMap<>();
        resetPasswordMap.put(AppConstants.NetworkConstants.RESET_PASSWORD_PARAM_1, mResetPasswordModel.getUserId());
        resetPasswordMap.put(AppConstants.NetworkConstants.RESET_PASSWORD_PARAM_2, mResetPasswordModel.getNewPassword());
        return resetPasswordMap;
    }

    public void onSubmitClicked() {
        mResetPasswordModel.hitResetPasswordApi(mResetPasswordBeanRichMediatorLiveData, setResetPasswordMap());
    }

    public void onPasswordHideShowClick(){
        if(mResetPasswordModel.getIsShowPassword()){
            mResetPasswordModel.setIsShowPassword(false);
        }else {
            mResetPasswordModel.setIsShowPassword(true);
        }
    }

    public void onConfirmPasswordHideShowClick(){
        if(mResetPasswordModel.getIsConfirmShowPassword()){
            mResetPasswordModel.setIsConfirmShowPassword(false);
        }else {
            mResetPasswordModel.setIsConfirmShowPassword(true);
        }
    }
}
