package com.whizzly.self_collab;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.whizzly.BR;

public class SelfCollabRecordModel extends BaseObservable {
    private ObservableBoolean isRecordPressed = new ObservableBoolean(false);
    private ObservableBoolean isRemoveVisible = new ObservableBoolean(false);
    private ObservableBoolean isFlashClicked = new ObservableBoolean(false);
    private ObservableBoolean isVideoStarted = new ObservableBoolean(false);
    public ObservableField<String> time = new ObservableField<>("");
    public ObservableBoolean isStartTimer = new ObservableBoolean(false);

    @Bindable
    public boolean getIsRecordPressed() {
        return isRecordPressed.get();
    }

    public void setIsRecordPressed(boolean isRecordPressed) {
        this.isRecordPressed.set(isRecordPressed);
        notifyPropertyChanged(BR.isRecordPressed);
    }

    @Bindable
    public String getTime() {
        return time.get();
    }

    public void setTime(String time) {
        this.time.set(time);
        notifyPropertyChanged(BR.time);
    }

    @Bindable
    public boolean getIsStartTimer() {
        return isStartTimer.get();
    }

    public void setIsStartTimer(boolean isStartTimer) {
        this.isStartTimer.set(isStartTimer);
        notifyPropertyChanged(BR.isStartTimer);
    }

    @Bindable
    public boolean getIsRemoveVisible() {
        return isRemoveVisible.get();
    }

    public void setIsRemoveVisible(boolean isRemoveVisible) {
        this.isRemoveVisible.set(isRemoveVisible);
        notifyPropertyChanged(BR.isRemoveVisible);
    }

    @Bindable
    public boolean getIsFlashClicked() {
        return isFlashClicked.get();
    }

    public void setIsFlashClicked(boolean isFlashClicked) {
        this.isFlashClicked.set(isFlashClicked);
        notifyPropertyChanged(BR.isFlashClicked);
    }

    @Bindable
    public boolean getIsVideoStarted() {
        return isVideoStarted.get();
    }

    public void setIsVideoStarted(boolean isVideoStarted) {
        this.isVideoStarted.set(isVideoStarted);
        notifyPropertyChanged(BR.isVideoStarted);
    }
}
