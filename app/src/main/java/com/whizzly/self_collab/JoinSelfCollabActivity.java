package com.whizzly.self_collab;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.beans.CollabBarBean;
import com.whizzly.colorBarChanges.activity.NewVideoPreviewActivity;
import com.whizzly.databinding.ActivityJoinSelfCollabBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.FFMpegCommandListener;
import com.whizzly.utils.FFMpegCommands;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

public class JoinSelfCollabActivity extends BaseActivity implements OnClickSendListener, View.OnClickListener, View.OnLongClickListener {

    private ActivityJoinSelfCollabBinding mActivityJoinSelfCollabBinding;
    private ArrayList<SelfCollabVideoDataBean> mSelfCollabVideoDataBeanArrayList;
    private String mMusicId;
    private int totalNoOfFrames;
    private ArrayList<CollabBarBean> mCollabBarMap;
    private int templeteSize;
    private ArrayList<String> mVideoStack;
    private String mFilePath;
    private Uri mFileUri;
    private String mAudioFilePath;
    private String mVideoMuteFilePath;
    private FFMpegCommands mFFmpegCommands;
    private int click = 0;
    private boolean isOneLocked = false, isTwoLocked = false, isThreeLocked = false, isFourLocked = false, isFiveLocked = false, isSixLocked = false;
    private int numberOfViewsSelected = 0;
    private Handler handlerOne = new Handler(), handlerTwo = new Handler(), handlerThree = new Handler(), handlerFour = new Handler(), handlerFive = new Handler(), handlerSix = new Handler();
    private Runnable runnableOne = new Runnable() {
        @Override
        public void run() {
            mActivityJoinSelfCollabBinding.llRemoveOne.setVisibility(View.GONE);
        }
    }, runnableTwo = new Runnable() {
        @Override
        public void run() {
            mActivityJoinSelfCollabBinding.llRemoveTwo.setVisibility(View.GONE);
        }
    }, runnableThree = new Runnable() {
        @Override
        public void run() {
            mActivityJoinSelfCollabBinding.llRemoveThree.setVisibility(View.GONE);
        }
    }, runnableFour = new Runnable() {
        @Override
        public void run() {
            mActivityJoinSelfCollabBinding.llRemoveFour.setVisibility(View.GONE);
        }
    }, runnableFive = new Runnable() {
        @Override
        public void run() {
            mActivityJoinSelfCollabBinding.llRemoveFive.setVisibility(View.GONE);
        }
    }, runnableSix = new Runnable() {
        @Override
        public void run() {
            mActivityJoinSelfCollabBinding.llRemoveSix.setVisibility(View.GONE);
        }
    };
    private Handler mHandlerOne, mHandlerTwo, mHandlerThree, mHandlerFour, mHandlerFive, mHandlerSix;
    private Runnable mRunnableOne, mRunnableTwo, mRunnableThree, mRunnableFour, mRunnableFive, mRunnableSix;
    private String newVideoFile;
    private int isRecordingUsingHeadPhone = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityJoinSelfCollabBinding = DataBindingUtil.setContentView(this, R.layout.activity_join_self_collab);
        JoinSelfCollabViewModel mJoinSelfCollabViewModel = ViewModelProviders.of(this).get(JoinSelfCollabViewModel.class);
        mActivityJoinSelfCollabBinding.setViewModel(mJoinSelfCollabViewModel);
        mJoinSelfCollabViewModel.setOnClickSendListener(this);
        getData();
        setToolbar();
        setTemplete();
        setLockRunnables();
        setOnLongClickListeners();
        setOnClickListeners();
        mFFmpegCommands = FFMpegCommands.getInstance(this);
        try {
            mFFmpegCommands.initializeFFmpeg();
        } catch (FFmpegNotSupportedException e) {
            e.printStackTrace();
        }

        if (mVideoStack.size() > 1) {
            concatVideos(addVideosPathToTextFile(mVideoStack));
        } else {
            mFilePath = mVideoStack.get(0);
            extractAudio();
            setView();
            extractVideoLength(mFilePath);
        }

    }

    private void setOnClickListeners() {
        mActivityJoinSelfCollabBinding.fabAddAnother.setOnClickListener(this);
        mActivityJoinSelfCollabBinding.toolbar.tvDone.setOnClickListener(this);
        mActivityJoinSelfCollabBinding.toolbar.ivBack.setOnClickListener(this);
        mActivityJoinSelfCollabBinding.ivLockUnlockOne.setOnClickListener(this);
        mActivityJoinSelfCollabBinding.ivLockUnlockTwo.setOnClickListener(this);
        mActivityJoinSelfCollabBinding.ivLockUnlockThree.setOnClickListener(this);
        mActivityJoinSelfCollabBinding.ivLockUnlockFour.setOnClickListener(this);
        mActivityJoinSelfCollabBinding.ivLockUnlockFive.setOnClickListener(this);
        mActivityJoinSelfCollabBinding.ivLockUnlockSix.setOnClickListener(this);
        mActivityJoinSelfCollabBinding.ivRemoveOne.setOnClickListener(this);
        mActivityJoinSelfCollabBinding.ivRemoveTwo.setOnClickListener(this);
        mActivityJoinSelfCollabBinding.ivRemoveThree.setOnClickListener(this);
        mActivityJoinSelfCollabBinding.ivRemoveFour.setOnClickListener(this);
        mActivityJoinSelfCollabBinding.ivRemoveFive.setOnClickListener(this);
        mActivityJoinSelfCollabBinding.ivRemoveSix.setOnClickListener(this);
    }

    private void setOnLongClickListeners() {
        mActivityJoinSelfCollabBinding.ivFrameOne.setOnLongClickListener(this);
        mActivityJoinSelfCollabBinding.ivFrameTwo.setOnLongClickListener(this);
        mActivityJoinSelfCollabBinding.ivFrameThree.setOnLongClickListener(this);
        mActivityJoinSelfCollabBinding.ivFrameFour.setOnLongClickListener(this);
        mActivityJoinSelfCollabBinding.ivFrameFive.setOnLongClickListener(this);
        mActivityJoinSelfCollabBinding.ivFrameSix.setOnLongClickListener(this);
    }

    private void setToolbar() {
        mActivityJoinSelfCollabBinding.toolbar.tvToolbarText.setText(R.string.txt_choose_templete);
        mActivityJoinSelfCollabBinding.toolbar.tvDone.setVisibility(View.VISIBLE);
        mActivityJoinSelfCollabBinding.toolbar.ivBack.setVisibility(View.VISIBLE);
        mActivityJoinSelfCollabBinding.toolbar.ivBack.setOnClickListener(this);
    }

    private String getAudioFilePath(Context context) {
        final File dir = context.getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "audio_" + System.currentTimeMillis() + ".mp3";
    }

    private String getVideoFile() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "videoWithOutAudio_" + System.currentTimeMillis() + ".mp4";
    }

    public int getClick() {
        return click;
    }

    private void setClick(int code) {
        this.click = code;
        if (!mActivityJoinSelfCollabBinding.ivFrameOne.isSelected()) {
            mActivityJoinSelfCollabBinding.ivFrameOne.setVisibility(View.GONE);
            mActivityJoinSelfCollabBinding.llAddVideoOne.setVisibility(View.VISIBLE);
            mActivityJoinSelfCollabBinding.ivLockUnlockOne.setVisibility(View.GONE);
        }
        if (!mActivityJoinSelfCollabBinding.ivFrameTwo.isSelected()) {
            mActivityJoinSelfCollabBinding.ivFrameTwo.setVisibility(View.GONE);
            mActivityJoinSelfCollabBinding.llAddVideoTwo.setVisibility(View.VISIBLE);
            mActivityJoinSelfCollabBinding.ivLockUnlockTwo.setVisibility(View.GONE);
        }
        if (!mActivityJoinSelfCollabBinding.ivFrameThree.isSelected()) {
            mActivityJoinSelfCollabBinding.ivFrameThree.setVisibility(View.GONE);
            mActivityJoinSelfCollabBinding.llAddVideoThree.setVisibility(View.VISIBLE);
            mActivityJoinSelfCollabBinding.ivLockUnlockThree.setVisibility(View.GONE);
        }
        if (!mActivityJoinSelfCollabBinding.ivFrameFour.isSelected()) {
            mActivityJoinSelfCollabBinding.ivFrameFour.setVisibility(View.GONE);
            mActivityJoinSelfCollabBinding.llAddVideoFour.setVisibility(View.VISIBLE);
            mActivityJoinSelfCollabBinding.ivLockUnlockFour.setVisibility(View.GONE);
        }
        if (!mActivityJoinSelfCollabBinding.ivFrameFive.isSelected()) {
            mActivityJoinSelfCollabBinding.ivFrameFive.setVisibility(View.GONE);
            mActivityJoinSelfCollabBinding.llAddVideoFive.setVisibility(View.VISIBLE);
            mActivityJoinSelfCollabBinding.ivLockUnlockFive.setVisibility(View.GONE);
        }
        if (!mActivityJoinSelfCollabBinding.ivFrameSix.isSelected()) {
            mActivityJoinSelfCollabBinding.ivFrameSix.setVisibility(View.GONE);
            mActivityJoinSelfCollabBinding.llAddVideoSix.setVisibility(View.VISIBLE);
            mActivityJoinSelfCollabBinding.ivLockUnlockSix.setVisibility(View.GONE);
        }
        switch (code) {
            case 1:
                click = 1;
                mActivityJoinSelfCollabBinding.ivFrameOne.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llAddVideoOne.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.ivLockUnlockOne.setVisibility(View.VISIBLE);
                mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setFrameNumber("1");
                mActivityJoinSelfCollabBinding.ivFrameOne.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).getImageFrame()));
                break;
            case 2:
                click = 2;
                mActivityJoinSelfCollabBinding.ivFrameTwo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llAddVideoTwo.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.ivLockUnlockTwo.setVisibility(View.VISIBLE);
                mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setFrameNumber("2");
                mActivityJoinSelfCollabBinding.ivFrameTwo.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).getImageFrame()));
                break;
            case 3:
                click = 3;
                mActivityJoinSelfCollabBinding.ivFrameThree.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llAddVideoThree.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.ivLockUnlockThree.setVisibility(View.VISIBLE);
                mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setFrameNumber("3");
                mActivityJoinSelfCollabBinding.ivFrameThree.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).getImageFrame()));
                break;
            case 4:
                click = 4;
                mActivityJoinSelfCollabBinding.ivFrameFour.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llAddVideoFour.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.ivLockUnlockFour.setVisibility(View.VISIBLE);
                mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setFrameNumber("4");
                mActivityJoinSelfCollabBinding.ivFrameFour.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).getImageFrame()));
                break;
            case 5:
                click = 5;
                mActivityJoinSelfCollabBinding.ivFrameFive.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llAddVideoFive.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.ivLockUnlockFive.setVisibility(View.VISIBLE);
                mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setFrameNumber("5");
                mActivityJoinSelfCollabBinding.ivFrameFive.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).getImageFrame()));
                break;
            case 6:
                click = 6;
                mActivityJoinSelfCollabBinding.ivFrameSix.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llAddVideoSix.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.ivLockUnlockSix.setVisibility(View.VISIBLE);
                mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setFrameNumber("6");
                mActivityJoinSelfCollabBinding.ivFrameSix.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).getImageFrame()));
                break;
        }
    }

    private void extractAudio() {
        mAudioFilePath = getAudioFilePath(this);
        mActivityJoinSelfCollabBinding.flLoading.setVisibility(View.VISIBLE);
        String secondAudioFile = mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 2).getAudioPath();
        mVideoMuteFilePath = getVideoFile();
        try {
            mFFmpegCommands.getAudioFile(mFilePath, mAudioFilePath, new FFMpegCommandListener() {
                @Override
                public void onSuccess(String message) {
                    mFFmpegCommands.getVideoFile(mFilePath, mVideoMuteFilePath, new FFMpegCommandListener() {
                        @Override
                        public void onSuccess(String message) {
                            if (isRecordingUsingHeadPhone == 1) {
                                String audioFile = getAudioFilePath(JoinSelfCollabActivity.this);
                                mFFmpegCommands.increaseAudioVolume(mAudioFilePath, audioFile, new FFMpegCommandListener() {
                                    @Override
                                    public void onSuccess(String message) {
                                        mAudioFilePath = audioFile;
                                        String audioFile = getAudioFilePath(JoinSelfCollabActivity.this);
                                        mFFmpegCommands.mergeTwoAudioFiles(mAudioFilePath, secondAudioFile, audioFile, new FFMpegCommandListener() {
                                            @Override
                                            public void onSuccess(String message) {
                                                String videoFile = getVideoFile();
                                                mFFmpegCommands.mergeSongWithVideo(mVideoMuteFilePath, audioFile, videoFile, new FFMpegCommandListener() {
                                                    @Override
                                                    public void onSuccess(String message) {
                                                        mFilePath = videoFile;
                                                        mFileUri = Uri.fromFile(new File(mFilePath));
                                                        mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setVideoUri(mFileUri.toString());
                                                        mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setFinalVideo(mFilePath);

                                                        mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setVideoPath(mFilePath);
                                                        mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setAudioPath(audioFile);
                                                    }

                                                    @Override
                                                    public void onProgress(String message) {

                                                    }

                                                    @Override
                                                    public void onFailure(String message) {

                                                    }

                                                    @Override
                                                    public void onStart() {

                                                    }

                                                    @Override
                                                    public void onFinish() {
                                                        mActivityJoinSelfCollabBinding.flLoading.setVisibility(View.GONE);
                                                    }
                                                });
                                            }

                                            @Override
                                            public void onProgress(String message) {

                                            }

                                            @Override
                                            public void onFailure(String message) {

                                            }

                                            @Override
                                            public void onStart() {

                                            }

                                            @Override
                                            public void onFinish() {

                                            }
                                        });
                                    }

                                    @Override
                                    public void onProgress(String message) {

                                    }

                                    @Override
                                    public void onFailure(String message) {

                                    }

                                    @Override
                                    public void onStart() {

                                    }

                                    @Override
                                    public void onFinish() {

                                    }
                                });
                            } else {
                                mFileUri = Uri.fromFile(new File(mFilePath));
                                mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setVideoUri(mFileUri.toString());
                                mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setFinalVideo(mFilePath);

                                mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setVideoPath(mFilePath);
                                mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setAudioPath(mAudioFilePath);
                            }
                        }

                        @Override
                        public void onProgress(String message) {

                        }

                        @Override
                        public void onFailure(String message) {

                        }

                        @Override
                        public void onStart() {

                        }

                        @Override
                        public void onFinish() {
                            if (isRecordingUsingHeadPhone == 0)
                                mActivityJoinSelfCollabBinding.flLoading.setVisibility(View.GONE);
                        }
                    });
                }

                @Override
                public void onProgress(String message) {

                }

                @Override
                public void onFailure(String message) {

                }

                @Override
                public void onStart() {

                }

                @Override
                public void onFinish() {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getConcatFile() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "concat_" + System.currentTimeMillis() + ".mp4";
    }

    private void extractVideoLength(String filePath) {
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(filePath);
        String duration = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setVideoLength(duration);
    }

    private void concatVideos(String textFile) {
        mFilePath = getConcatFile();
        mFFmpegCommands.concatenateVideos(textFile, mFilePath, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                extractAudio();
                setView();
                extractVideoLength(mFilePath);
            }

            @Override
            public void onProgress(String message) {

            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onStart() {

            }

            @Override
            public void onFinish() {

            }
        });
    }

    private String addVideosPathToTextFile(ArrayList<String> videoPathList) {
        final File dir = getExternalFilesDir(null);
        String textFile = (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "concat_text_" + System.currentTimeMillis() + ".txt";
        File file = new File(textFile);
        try {
            FileOutputStream f = new FileOutputStream(file);
            PrintWriter pw = new PrintWriter(f);
            for (int i = 0; i < videoPathList.size(); i++) {
                pw.println("file " + videoPathList.get(i));
            }
            pw.flush();
            pw.close();
            f.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return textFile;
    }

    private void getData() {
        Intent intent = getIntent();
        mVideoStack = (ArrayList<String>) intent.getSerializableExtra(AppConstants.ClassConstants.VIDEO_FILE_PATH);
        mSelfCollabVideoDataBeanArrayList = (ArrayList<SelfCollabVideoDataBean>) intent.getSerializableExtra(AppConstants.ClassConstants.VIDEO_DATA);
        mMusicId = intent.getStringExtra(AppConstants.ClassConstants.MUSIC_ID);
        isRecordingUsingHeadPhone = intent.getIntExtra(AppConstants.ClassConstants.IS_RECORDED_WITH_HEADPHONES, 0);
        totalNoOfFrames = intent.getIntExtra(AppConstants.ClassConstants.FRAMES, 2);
        templeteSize = intent.getIntExtra(AppConstants.ClassConstants.TEMPLETE_SIZE, 2);
        mCollabBarMap = intent.getParcelableArrayListExtra(AppConstants.ClassConstants.COLOR_BAR_DATA);
        getImageFromVideo();
        numberOfViewsSelected = mSelfCollabVideoDataBeanArrayList.size();
        if (templeteSize == mSelfCollabVideoDataBeanArrayList.size()) {
            mActivityJoinSelfCollabBinding.fabAddAnother.setVisibility(View.INVISIBLE);
        }
    }

    private Uri imageFile(String imagePath) {
        File file = new File(imagePath);
        return Uri.fromFile(file);
    }

    public void setView() {
        for (int i = 0; i < mSelfCollabVideoDataBeanArrayList.size(); i++) {
            switch (mSelfCollabVideoDataBeanArrayList.get(i).getFrameNumber()) {
                case "1":
                    mActivityJoinSelfCollabBinding.ivFrameOne.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.llAddVideoOne.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockOne.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.ivFrameOne.setSelected(true);
                    mActivityJoinSelfCollabBinding.ivLockUnlockOne.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_unlocked));
                    mActivityJoinSelfCollabBinding.ivFrameOne.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(i).getImageFrame()));
                    break;
                case "2":
                    mActivityJoinSelfCollabBinding.ivFrameTwo.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.llAddVideoTwo.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockTwo.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.ivFrameTwo.setSelected(true);
                    mActivityJoinSelfCollabBinding.ivLockUnlockTwo.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_unlocked));
                    mActivityJoinSelfCollabBinding.ivFrameTwo.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(i).getImageFrame()));
                    break;
                case "3":
                    mActivityJoinSelfCollabBinding.ivFrameThree.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.llAddVideoThree.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockThree.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.ivFrameThree.setSelected(true);
                    mActivityJoinSelfCollabBinding.ivLockUnlockThree.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_unlocked));
                    mActivityJoinSelfCollabBinding.ivFrameThree.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(i).getImageFrame()));
                    break;
                case "4":
                    mActivityJoinSelfCollabBinding.ivFrameFour.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.llAddVideoFour.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockFour.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.ivFrameFour.setSelected(true);
                    mActivityJoinSelfCollabBinding.ivLockUnlockFour.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_unlocked));
                    mActivityJoinSelfCollabBinding.ivFrameFour.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(i).getImageFrame()));
                    break;
                case "5":
                    mActivityJoinSelfCollabBinding.ivFrameFive.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.llAddVideoFive.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockFive.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.ivFrameFive.setSelected(true);
                    mActivityJoinSelfCollabBinding.ivLockUnlockFive.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_unlocked));
                    mActivityJoinSelfCollabBinding.ivFrameFive.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(i).getImageFrame()));
                    break;
                case "6":
                    mActivityJoinSelfCollabBinding.ivFrameSix.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.llAddVideoSix.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockSix.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.ivFrameSix.setSelected(true);
                    mActivityJoinSelfCollabBinding.ivLockUnlockSix.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_unlocked));
                    mActivityJoinSelfCollabBinding.ivFrameSix.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(i).getImageFrame()));
                    break;
                case "":
                    if (mActivityJoinSelfCollabBinding.ivFrameOne.getVisibility() == View.GONE) {
                        click = 1;
                        mActivityJoinSelfCollabBinding.ivFrameOne.setVisibility(View.VISIBLE);
                        mActivityJoinSelfCollabBinding.llAddVideoOne.setVisibility(View.GONE);
                        mActivityJoinSelfCollabBinding.ivLockUnlockOne.setVisibility(View.VISIBLE);
                        mSelfCollabVideoDataBeanArrayList.get(i).setFrameNumber("1");
                        mActivityJoinSelfCollabBinding.ivFrameOne.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(i).getImageFrame()));
                    } else if (mActivityJoinSelfCollabBinding.ivFrameTwo.getVisibility() == View.GONE) {
                        click = 2;
                        mActivityJoinSelfCollabBinding.ivFrameTwo.setVisibility(View.VISIBLE);
                        mActivityJoinSelfCollabBinding.llAddVideoTwo.setVisibility(View.GONE);
                        mActivityJoinSelfCollabBinding.ivLockUnlockTwo.setVisibility(View.VISIBLE);
                        mSelfCollabVideoDataBeanArrayList.get(i).setFrameNumber("2");
                        mActivityJoinSelfCollabBinding.ivFrameTwo.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(i).getImageFrame()));
                    } else if (mActivityJoinSelfCollabBinding.ivFrameThree.getVisibility() == View.GONE) {
                        click = 3;
                        mActivityJoinSelfCollabBinding.ivFrameThree.setVisibility(View.VISIBLE);
                        mActivityJoinSelfCollabBinding.llAddVideoThree.setVisibility(View.GONE);
                        mActivityJoinSelfCollabBinding.ivLockUnlockThree.setVisibility(View.VISIBLE);
                        mSelfCollabVideoDataBeanArrayList.get(i).setFrameNumber("3");
                        mActivityJoinSelfCollabBinding.ivFrameThree.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(i).getImageFrame()));
                    } else if (mActivityJoinSelfCollabBinding.ivFrameFour.getVisibility() == View.GONE) {
                        click = 4;
                        mActivityJoinSelfCollabBinding.ivFrameFour.setVisibility(View.VISIBLE);
                        mActivityJoinSelfCollabBinding.llAddVideoFour.setVisibility(View.GONE);
                        mActivityJoinSelfCollabBinding.ivLockUnlockFour.setVisibility(View.VISIBLE);
                        mSelfCollabVideoDataBeanArrayList.get(i).setFrameNumber("4");
                        mActivityJoinSelfCollabBinding.ivFrameFour.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(i).getImageFrame()));
                    } else if (mActivityJoinSelfCollabBinding.ivFrameFive.getVisibility() == View.GONE) {
                        click = 5;
                        mActivityJoinSelfCollabBinding.ivFrameFive.setVisibility(View.VISIBLE);
                        mActivityJoinSelfCollabBinding.llAddVideoFive.setVisibility(View.GONE);
                        mActivityJoinSelfCollabBinding.ivLockUnlockFive.setVisibility(View.VISIBLE);
                        mSelfCollabVideoDataBeanArrayList.get(i).setFrameNumber("5");
                        mActivityJoinSelfCollabBinding.ivFrameFive.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(i).getImageFrame()));
                    } else if (mActivityJoinSelfCollabBinding.ivFrameSix.getVisibility() == View.GONE) {
                        click = 6;
                        mActivityJoinSelfCollabBinding.ivFrameSix.setVisibility(View.VISIBLE);
                        mActivityJoinSelfCollabBinding.llAddVideoSix.setVisibility(View.GONE);
                        mActivityJoinSelfCollabBinding.ivLockUnlockSix.setVisibility(View.VISIBLE);
                        mSelfCollabVideoDataBeanArrayList.get(i).setFrameNumber("6");
                        mActivityJoinSelfCollabBinding.ivFrameSix.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(i).getImageFrame()));
                    }
                    break;
            }
        }
    }

    private void setTemplete() {
        switch (templeteSize) {
            case 2:
                mActivityJoinSelfCollabBinding.llTopViews.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llMiddleViews.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llBottomView.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvOneVideo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvTwoVideo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvThreeVideo.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.cvFourVideo.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.cvFiveVideo.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.cvSixVideo.setVisibility(View.GONE);
                break;
            case 3:
                mActivityJoinSelfCollabBinding.llTopViews.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llMiddleViews.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llBottomView.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvOneVideo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvTwoVideo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvThreeVideo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvFourVideo.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.cvFiveVideo.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.cvSixVideo.setVisibility(View.GONE);
                break;
            case 4:
                mActivityJoinSelfCollabBinding.llTopViews.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llMiddleViews.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llBottomView.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvOneVideo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvTwoVideo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvThreeVideo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvFourVideo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvFiveVideo.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.cvSixVideo.setVisibility(View.GONE);
                break;
            case 5:
                mActivityJoinSelfCollabBinding.llTopViews.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llMiddleViews.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llBottomView.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvOneVideo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvTwoVideo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvThreeVideo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvFourVideo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvFiveVideo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvSixVideo.setVisibility(View.GONE);
                break;
            case 6:
                mActivityJoinSelfCollabBinding.llTopViews.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llMiddleViews.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llBottomView.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvOneVideo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvTwoVideo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvThreeVideo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvFourVideo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvFiveVideo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.cvSixVideo.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void getImageFromVideo() {
        if(mVideoStack.size()>0) {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(mVideoStack.get(0));
            Bitmap bitmap = mediaMetadataRetriever.getFrameAtTime(100, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            String imageFile = getImageFile();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            File file = new File(imageFile);
            try {
                FileOutputStream fo = new FileOutputStream(file);
                fo.write(stream.toByteArray());
                fo.flush();
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setImageFrame(imageFile);
            mediaMetadataRetriever.release();
        }
    }

    private String getImageFile() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "frame_" + System.currentTimeMillis() + ".png";
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants
                    .ClassConstants.ON_CHOOSE_TEMPLETE_ONE:
                boolean isOnePresent = false;
                for (int i = 0; i < mSelfCollabVideoDataBeanArrayList.size(); i++) {
                    if (mSelfCollabVideoDataBeanArrayList.get(i).getFrameNumber().equalsIgnoreCase("1")) {
                        isOnePresent = true;
                        break;
                    }
                }
                if (!isOnePresent) {
                    setClick(1);
                    mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setFrameNumber("1");
                }

                removeAllDeleteViews();
                break;
            case AppConstants
                    .ClassConstants.ON_CHOOSE_TEMPLETE_TWO:
                boolean isTwoPresent = false;
                for (int i = 0; i < mSelfCollabVideoDataBeanArrayList.size(); i++) {
                    if (mSelfCollabVideoDataBeanArrayList.get(i).getFrameNumber().equalsIgnoreCase("2")) {
                        isTwoPresent = true;
                        break;
                    }
                }
                if (!isTwoPresent) {
                    setClick(2);
                    mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setFrameNumber("2");
                }

                removeAllDeleteViews();
                break;
            case AppConstants
                    .ClassConstants.ON_CHOOSE_TEMPLETE_THREE:
                boolean isThreePresent = false;
                for (int i = 0; i < mSelfCollabVideoDataBeanArrayList.size(); i++) {
                    if (mSelfCollabVideoDataBeanArrayList.get(i).getFrameNumber().equalsIgnoreCase("3")) {
                        isThreePresent = true;
                        break;
                    }
                }

                if (!isThreePresent) {
                    setClick(3);
                    mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setFrameNumber("3");
                }

                removeAllDeleteViews();
                break;
            case AppConstants
                    .ClassConstants.ON_CHOOSE_TEMPLETE_FOUR:
                boolean isFourPresent = false;
                for (int i = 0; i < mSelfCollabVideoDataBeanArrayList.size(); i++) {
                    if (mSelfCollabVideoDataBeanArrayList.get(i).getFrameNumber().equalsIgnoreCase("4")) {
                        isFourPresent = true;
                        break;
                    }
                }

                if (!isFourPresent) {
                    mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setFrameNumber("4");
                    setClick(4);
                }

                removeAllDeleteViews();
                break;
            case AppConstants
                    .ClassConstants.ON_CHOOSE_TEMPLETE_FIVE:
                boolean isFivePresent = false;
                for (int i = 0; i < mSelfCollabVideoDataBeanArrayList.size(); i++) {
                    if (mSelfCollabVideoDataBeanArrayList.get(i).getFrameNumber().equalsIgnoreCase("5")) {
                        isFivePresent = true;
                        break;
                    }
                }

                if (!isFivePresent) {
                    setClick(5);
                    mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setFrameNumber("5");
                }
                removeAllDeleteViews();
                break;
            case AppConstants
                    .ClassConstants.ON_CHOOSE_TEMPLETE_SIX:
                boolean isSixPresent = false;
                for (int i = 0; i < mSelfCollabVideoDataBeanArrayList.size(); i++) {
                    if (mSelfCollabVideoDataBeanArrayList.get(i).getFrameNumber().equalsIgnoreCase("6")) {
                        isSixPresent = true;
                        break;
                    }
                }

                if (!isSixPresent) {
                    setClick(6);
                    mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setFrameNumber("6");
                }
                removeAllDeleteViews();
                break;
        }
    }

    private void setLockRunnables() {
        mHandlerOne = new Handler();
        mHandlerTwo = new Handler();
        mHandlerThree = new Handler();
        mHandlerFour = new Handler();
        mHandlerFive = new Handler();
        mHandlerSix = new Handler();

        mRunnableOne = new Runnable() {
            @Override
            public void run() {
                if (!isOneLocked) {
                    isOneLocked = true;
                    lockVideo();
                    mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setIsMandatory(1);
                    mActivityJoinSelfCollabBinding.ivLockUnlockTwo.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockThree.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockFour.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockFive.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockSix.setVisibility(View.GONE);
                } else {
                    isOneLocked = false;
                    unlockVideos();
                    mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setIsMandatory(0);
                }
            }
        };

        mRunnableTwo = new Runnable() {
            @Override
            public void run() {
                if (!isTwoLocked) {
                    isTwoLocked = true;
                    mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setIsMandatory(1);
                    lockVideo();
                    mActivityJoinSelfCollabBinding.ivLockUnlockOne.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockThree.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockFour.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockFive.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockSix.setVisibility(View.GONE);
                } else {
                    isTwoLocked = false;
                    unlockVideos();
                    mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setIsMandatory(0);
                }
            }
        };

        mRunnableThree = new Runnable() {
            @Override
            public void run() {
                if (!isThreeLocked) {
                    isThreeLocked = true;
                    mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setIsMandatory(1);
                    lockVideo();
                    mActivityJoinSelfCollabBinding.ivLockUnlockTwo.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockOne.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockFour.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockFive.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockSix.setVisibility(View.GONE);
                } else {
                    isThreeLocked = false;
                    unlockVideos();
                    mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setIsMandatory(0);
                }
            }
        };

        mRunnableFour = new Runnable() {
            @Override
            public void run() {
                if (!isFourLocked) {
                    isFourLocked = true;
                    mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setIsMandatory(1);
                    lockVideo();
                    mActivityJoinSelfCollabBinding.ivLockUnlockTwo.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockThree.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockOne.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockFive.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockSix.setVisibility(View.GONE);
                } else {
                    isFourLocked = false;
                    unlockVideos();
                    mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setIsMandatory(0);
                }
            }
        };

        mRunnableFive = new Runnable() {
            @Override
            public void run() {
                if (!isFiveLocked) {
                    isFiveLocked = true;
                    mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setIsMandatory(1);
                    lockVideo();
                    mActivityJoinSelfCollabBinding.ivLockUnlockTwo.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockThree.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockFour.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockOne.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockSix.setVisibility(View.GONE);
                } else {
                    isFiveLocked = false;
                    unlockVideos();
                    mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setIsMandatory(0);
                }
            }
        };

        mRunnableSix = new Runnable() {
            @Override
            public void run() {
                if (!isSixLocked) {
                    isSixLocked = true;
                    mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setIsMandatory(1);
                    lockVideo();
                    mActivityJoinSelfCollabBinding.ivLockUnlockTwo.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockThree.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockFour.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockFive.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockOne.setVisibility(View.GONE);
                } else {
                    isSixLocked = false;
                    unlockVideos();
                    mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setIsMandatory(0);
                }
            }
        };
    }

    private void discardRecording() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.txt_discard_collab)
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> discard())
                .setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss())
                .create().show();
    }

    private void discard(){
        finish();
    }

    @Override
    public void onBackPressed() {
        discardRecording();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                discardRecording();
                break;
            case R.id.tv_done:
                if(mSelfCollabVideoDataBeanArrayList.size() == templeteSize){
                    if (isOneLocked || isTwoLocked || isThreeLocked || isFourLocked || isFiveLocked || isSixLocked) {
                        Intent previewIntent = new Intent(JoinSelfCollabActivity.this, SelfCollabPreviewActivity.class);
                        mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setFrameNumber(String.valueOf(getClick()));
                        previewIntent.putExtra(AppConstants.ClassConstants.VIDEO_DATA, mSelfCollabVideoDataBeanArrayList);
                        previewIntent.putExtra(AppConstants.ClassConstants.TEMPLETE_SIZE, templeteSize);
                        previewIntent.putExtra(AppConstants.ClassConstants.MUSIC_ID, mMusicId);
                        previewIntent.putExtra(AppConstants.ClassConstants.COLOR_BAR_DATA, mCollabBarMap);
                        startActivity(previewIntent);
                    } else {
                        Toast.makeText(this, "Please mention the mandatory video so that other users can collab.", Toast.LENGTH_SHORT).show();

                    }
                }else{
                    Intent intent = new Intent(JoinSelfCollabActivity.this, NewVideoPreviewActivity.class);
                    mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setFrameNumber(String.valueOf(getClick()));
                    intent.putExtra(AppConstants.ClassConstants.VIDEO_DATA, mSelfCollabVideoDataBeanArrayList);
                    intent.putExtra(AppConstants.ClassConstants.TEMPLETE_SIZE, templeteSize);
                    intent.putExtra(AppConstants.ClassConstants.MUSIC_ID, Integer.parseInt(mMusicId));
                    intent.putExtra(AppConstants.ClassConstants.FRAMES, mSelfCollabVideoDataBeanArrayList.size() + 1);
                    intent.putExtra(AppConstants.ClassConstants.COLOR_BAR_DATA, mCollabBarMap);
                    startActivity(intent);
                }

                break;
            case R.id.fab_add_another:
                Intent intent = new Intent(JoinSelfCollabActivity.this, SelfCollabRecordActivity.class);
                mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).setFrameNumber(String.valueOf(getClick()));
                intent.putExtra(AppConstants.ClassConstants.VIDEO_DATA, mSelfCollabVideoDataBeanArrayList);
                intent.putExtra(AppConstants.ClassConstants.TEMPLETE_SIZE, templeteSize);
                intent.putExtra(AppConstants.ClassConstants.MUSIC_ID, Integer.parseInt(mMusicId));
                intent.putExtra(AppConstants.ClassConstants.FRAMES, mSelfCollabVideoDataBeanArrayList.size() + 1);
                intent.putExtra(AppConstants.ClassConstants.COLOR_BAR_DATA, mCollabBarMap);
                startActivity(intent);
                break;
            case R.id.iv_lock_unlock_one:
                mHandlerOne.postDelayed(mRunnableOne, 100);
                mHandlerTwo.removeCallbacks(mRunnableTwo);
                mHandlerThree.removeCallbacks(mRunnableThree);
                mHandlerFour.removeCallbacks(mRunnableFour);
                mHandlerFive.removeCallbacks(mRunnableFive);
                mHandlerSix.removeCallbacks(mRunnableSix);

                break;
            case R.id.iv_lock_unlock_two:
                mHandlerTwo.postDelayed(mRunnableTwo, 100);
                mHandlerOne.removeCallbacks(mRunnableOne);
                mHandlerThree.removeCallbacks(mRunnableThree);
                mHandlerFour.removeCallbacks(mRunnableFour);
                mHandlerFive.removeCallbacks(mRunnableFive);
                mHandlerSix.removeCallbacks(mRunnableSix);
                break;
            case R.id.iv_lock_unlock_three:
                mHandlerThree.postDelayed(mRunnableThree, 100);
                mHandlerOne.removeCallbacks(mRunnableOne);
                mHandlerTwo.removeCallbacks(mRunnableTwo);
                mHandlerFour.removeCallbacks(mRunnableFour);
                mHandlerFive.removeCallbacks(mRunnableFive);
                mHandlerSix.removeCallbacks(mRunnableSix);
                break;
            case R.id.iv_lock_unlock_four:
                mHandlerFour.postDelayed(mRunnableFour, 100);
                mHandlerOne.removeCallbacks(mRunnableOne);
                mHandlerTwo.removeCallbacks(mRunnableTwo);
                mHandlerThree.removeCallbacks(mRunnableThree);
                mHandlerFive.removeCallbacks(mRunnableFive);
                mHandlerSix.removeCallbacks(mRunnableSix);
                break;
            case R.id.iv_lock_unlock_five:
                mHandlerFive.postDelayed(mRunnableFive, 100);
                mHandlerOne.removeCallbacks(mRunnableOne);
                mHandlerTwo.removeCallbacks(mRunnableTwo);
                mHandlerThree.removeCallbacks(mRunnableThree);
                mHandlerFour.removeCallbacks(mRunnableFour);
                mHandlerSix.removeCallbacks(mRunnableSix);
                break;
            case R.id.iv_lock_unlock_six:
                mHandlerSix.postDelayed(mRunnableSix, 100);
                mHandlerOne.removeCallbacks(mRunnableOne);
                mHandlerTwo.removeCallbacks(mRunnableTwo);
                mHandlerThree.removeCallbacks(mRunnableThree);
                mHandlerFour.removeCallbacks(mRunnableFour);
                mHandlerFive.removeCallbacks(mRunnableFive);
                break;
            case R.id.iv_remove_one:
                for (int i = 0; i < mSelfCollabVideoDataBeanArrayList.size(); i++) {
                    if (mSelfCollabVideoDataBeanArrayList.get(i).getFrameNumber().equalsIgnoreCase("1")) {
                        mSelfCollabVideoDataBeanArrayList.remove(i);
                        mActivityJoinSelfCollabBinding.ivFrameOne.setVisibility(View.GONE);
                        mActivityJoinSelfCollabBinding.llAddVideoOne.setVisibility(View.VISIBLE);
                        mActivityJoinSelfCollabBinding.llRemoveOne.setVisibility(View.GONE);
                        mActivityJoinSelfCollabBinding.ivLockUnlockOne.setVisibility(View.GONE);
                        numberOfViewsSelected--;
                        if (mActivityJoinSelfCollabBinding.ivFrameOne.isSelected()) {
                            mActivityJoinSelfCollabBinding.ivFrameOne.setSelected(false);
                        }
                        mActivityJoinSelfCollabBinding.fabAddAnother.setVisibility(View.VISIBLE);
                        handlerOne.removeCallbacks(runnableOne);
                        break;
                    }
                }
                break;
            case R.id.iv_remove_two:
                for (int i = 0; i < mSelfCollabVideoDataBeanArrayList.size(); i++) {
                    if (mSelfCollabVideoDataBeanArrayList.get(i).getFrameNumber().equalsIgnoreCase("2")) {
                        mSelfCollabVideoDataBeanArrayList.remove(i);
                        mActivityJoinSelfCollabBinding.ivFrameTwo.setVisibility(View.GONE);
                        mActivityJoinSelfCollabBinding.llAddVideoTwo.setVisibility(View.VISIBLE);
                        mActivityJoinSelfCollabBinding.llRemoveTwo.setVisibility(View.GONE);
                        mActivityJoinSelfCollabBinding.ivLockUnlockTwo.setVisibility(View.GONE);
                        numberOfViewsSelected--;
                        if (mActivityJoinSelfCollabBinding.ivFrameTwo.isSelected()) {
                            mActivityJoinSelfCollabBinding.ivFrameTwo.setSelected(false);
                        }
                        mActivityJoinSelfCollabBinding.fabAddAnother.setVisibility(View.VISIBLE);
                        handlerTwo.removeCallbacks(runnableTwo);
                        break;
                    }
                }
                break;
            case R.id.iv_remove_three:
                for (int i = 0; i < mSelfCollabVideoDataBeanArrayList.size(); i++) {
                    if (mSelfCollabVideoDataBeanArrayList.get(i).getFrameNumber().equalsIgnoreCase("3")) {
                        mSelfCollabVideoDataBeanArrayList.remove(i);
                        mActivityJoinSelfCollabBinding.ivFrameThree.setVisibility(View.GONE);
                        mActivityJoinSelfCollabBinding.llAddVideoThree.setVisibility(View.VISIBLE);
                        mActivityJoinSelfCollabBinding.llRemoveThree.setVisibility(View.GONE);
                        mActivityJoinSelfCollabBinding.ivLockUnlockThree.setVisibility(View.GONE);
                        numberOfViewsSelected--;
                        if (mActivityJoinSelfCollabBinding.ivFrameThree.isSelected()) {
                            mActivityJoinSelfCollabBinding.ivFrameThree.setSelected(false);
                        }
                        mActivityJoinSelfCollabBinding.fabAddAnother.setVisibility(View.VISIBLE);
                        handlerThree.removeCallbacks(runnableThree);
                        break;
                    }
                }
                break;
            case R.id.iv_remove_four:
                for (int i = 0; i < mSelfCollabVideoDataBeanArrayList.size(); i++) {
                    if (mSelfCollabVideoDataBeanArrayList.get(i).getFrameNumber().equalsIgnoreCase("4")) {
                        mSelfCollabVideoDataBeanArrayList.remove(i);
                        mActivityJoinSelfCollabBinding.ivFrameFour.setVisibility(View.GONE);
                        mActivityJoinSelfCollabBinding.llAddVideoFour.setVisibility(View.VISIBLE);
                        mActivityJoinSelfCollabBinding.llRemoveFour.setVisibility(View.GONE);
                        mActivityJoinSelfCollabBinding.ivLockUnlockFour.setVisibility(View.GONE);
                        numberOfViewsSelected--;
                        if (mActivityJoinSelfCollabBinding.ivFrameFour.isSelected()) {
                            mActivityJoinSelfCollabBinding.ivFrameFour.setSelected(false);
                        }
                        mActivityJoinSelfCollabBinding.fabAddAnother.setVisibility(View.VISIBLE);
                        handlerFour.removeCallbacks(runnableFour);
                        break;
                    }
                }
                break;
            case R.id.iv_remove_five:
                for (int i = 0; i < mSelfCollabVideoDataBeanArrayList.size(); i++) {
                    if (mSelfCollabVideoDataBeanArrayList.get(i).getFrameNumber().equalsIgnoreCase("5")) {
                        mSelfCollabVideoDataBeanArrayList.remove(i);
                        mActivityJoinSelfCollabBinding.ivFrameFive.setVisibility(View.GONE);
                        mActivityJoinSelfCollabBinding.llAddVideoFive.setVisibility(View.VISIBLE);
                        mActivityJoinSelfCollabBinding.llRemoveFive.setVisibility(View.GONE);
                        mActivityJoinSelfCollabBinding.ivLockUnlockFive.setVisibility(View.GONE);
                        numberOfViewsSelected--;
                        if (mActivityJoinSelfCollabBinding.ivFrameFive.isSelected()) {
                            mActivityJoinSelfCollabBinding.ivFrameFive.setSelected(false);
                        }
                        mActivityJoinSelfCollabBinding.fabAddAnother.setVisibility(View.VISIBLE);
                        handlerFive.removeCallbacks(runnableFive);
                        break;
                    }
                }
                break;
            case R.id.iv_remove_six:
                for (int i = 0; i < mSelfCollabVideoDataBeanArrayList.size(); i++) {
                    if (mSelfCollabVideoDataBeanArrayList.get(i).getFrameNumber().equalsIgnoreCase("6")) {
                        mSelfCollabVideoDataBeanArrayList.remove(i);
                        mActivityJoinSelfCollabBinding.ivFrameSix.setVisibility(View.GONE);
                        mActivityJoinSelfCollabBinding.llAddVideoSix.setVisibility(View.VISIBLE);
                        mActivityJoinSelfCollabBinding.llRemoveSix.setVisibility(View.GONE);
                        mActivityJoinSelfCollabBinding.ivLockUnlockSix.setVisibility(View.GONE);
                        numberOfViewsSelected--;
                        if (mActivityJoinSelfCollabBinding.ivFrameSix.isSelected()) {
                            mActivityJoinSelfCollabBinding.ivFrameSix.setSelected(false);
                        }
                        mActivityJoinSelfCollabBinding.fabAddAnother.setVisibility(View.VISIBLE);
                        handlerSix.removeCallbacks(runnableSix);
                        break;
                    }
                }
                break;
        }
    }

    private void removeAllDeleteViews() {
        mActivityJoinSelfCollabBinding.llRemoveOne.setVisibility(View.GONE);
        mActivityJoinSelfCollabBinding.llRemoveTwo.setVisibility(View.GONE);
        mActivityJoinSelfCollabBinding.llRemoveThree.setVisibility(View.GONE);
        mActivityJoinSelfCollabBinding.llRemoveFour.setVisibility(View.GONE);
        mActivityJoinSelfCollabBinding.llRemoveFive.setVisibility(View.GONE);
        mActivityJoinSelfCollabBinding.llRemoveSix.setVisibility(View.GONE);
    }

    private void unlockVideos() {
        mActivityJoinSelfCollabBinding.ivLockUnlockOne.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_unlocked));
        mActivityJoinSelfCollabBinding.ivLockUnlockTwo.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_unlocked));
        mActivityJoinSelfCollabBinding.ivLockUnlockThree.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_unlocked));
        mActivityJoinSelfCollabBinding.ivLockUnlockFour.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_unlocked));
        mActivityJoinSelfCollabBinding.ivLockUnlockFive.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_unlocked));
        mActivityJoinSelfCollabBinding.ivLockUnlockSix.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_unlocked));
        for (int i = 0; i < mSelfCollabVideoDataBeanArrayList.size(); i++) {
            switch (mSelfCollabVideoDataBeanArrayList.get(i).getFrameNumber()) {
                case "1":
                    mActivityJoinSelfCollabBinding.ivFrameOne.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.llAddVideoOne.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockOne.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockOne.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_unlocked));
                    mActivityJoinSelfCollabBinding.ivFrameOne.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(i).getImageFrame()));
                    break;
                case "2":
                    mActivityJoinSelfCollabBinding.ivFrameTwo.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.llAddVideoTwo.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockTwo.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockTwo.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_unlocked));
                    mActivityJoinSelfCollabBinding.ivFrameTwo.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(i).getImageFrame()));
                    break;
                case "3":
                    mActivityJoinSelfCollabBinding.ivFrameThree.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.llAddVideoThree.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockThree.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockThree.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_unlocked));
                    mActivityJoinSelfCollabBinding.ivFrameThree.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(i).getImageFrame()));
                    break;
                case "4":
                    mActivityJoinSelfCollabBinding.ivFrameFour.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.llAddVideoFour.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockFour.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockFour.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_unlocked));
                    mActivityJoinSelfCollabBinding.ivFrameFour.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(i).getImageFrame()));
                    break;
                case "5":
                    mActivityJoinSelfCollabBinding.ivFrameFive.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.llAddVideoFive.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockFive.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockFive.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_unlocked));
                    mActivityJoinSelfCollabBinding.ivFrameFive.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(i).getImageFrame()));
                    break;
                case "6":
                    mActivityJoinSelfCollabBinding.ivFrameSix.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.llAddVideoSix.setVisibility(View.GONE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockSix.setVisibility(View.VISIBLE);
                    mActivityJoinSelfCollabBinding.ivLockUnlockSix.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_unlocked));
                    mActivityJoinSelfCollabBinding.ivFrameSix.setImageURI(imageFile(mSelfCollabVideoDataBeanArrayList.get(i).getImageFrame()));
                    break;
            }
        }
    }

    private void lockVideo() {
        mActivityJoinSelfCollabBinding.ivLockUnlockOne.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_closed));
        mActivityJoinSelfCollabBinding.ivLockUnlockTwo.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_closed));
        mActivityJoinSelfCollabBinding.ivLockUnlockThree.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_closed));
        mActivityJoinSelfCollabBinding.ivLockUnlockFour.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_closed));
        mActivityJoinSelfCollabBinding.ivLockUnlockFive.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_closed));
        mActivityJoinSelfCollabBinding.ivLockUnlockSix.setImageDrawable(getResources().getDrawable(R.drawable.ic_lock_closed));
    }

    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()) {
            case R.id.iv_frame_one:
                mActivityJoinSelfCollabBinding.llRemoveOne.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llRemoveTwo.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveThree.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveFour.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveFive.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveSix.setVisibility(View.GONE);
                handlerOne.postDelayed(runnableOne, 4000);
                break;
            case R.id.iv_frame_two:
                mActivityJoinSelfCollabBinding.llRemoveOne.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveTwo.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llRemoveThree.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveFour.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveFive.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveSix.setVisibility(View.GONE);
                handlerTwo.postDelayed(runnableTwo, 4000);
                break;
            case R.id.iv_frame_three:
                mActivityJoinSelfCollabBinding.llRemoveOne.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveTwo.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveThree.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llRemoveFour.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveFive.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveSix.setVisibility(View.GONE);
                handlerThree.postDelayed(runnableThree, 4000);
                break;
            case R.id.iv_frame_four:
                mActivityJoinSelfCollabBinding.llRemoveOne.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveTwo.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveThree.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveFour.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llRemoveFive.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveSix.setVisibility(View.GONE);
                handlerFour.postDelayed(runnableFour, 4000);
                break;
            case R.id.iv_frame_five:
                mActivityJoinSelfCollabBinding.llRemoveOne.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveTwo.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveThree.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveFour.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveFive.setVisibility(View.VISIBLE);
                mActivityJoinSelfCollabBinding.llRemoveSix.setVisibility(View.GONE);
                handlerFive.postDelayed(runnableFive, 4000);
                break;
            case R.id.iv_frame_six:
                mActivityJoinSelfCollabBinding.llRemoveOne.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveTwo.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveThree.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveFour.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveFive.setVisibility(View.GONE);
                mActivityJoinSelfCollabBinding.llRemoveSix.setVisibility(View.VISIBLE);
                handlerSix.postDelayed(runnableSix, 4000);
                break;
        }
        return true;
    }
}
