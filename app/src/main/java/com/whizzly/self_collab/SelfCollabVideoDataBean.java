package com.whizzly.self_collab;

import java.io.Serializable;

public class SelfCollabVideoDataBean implements Serializable {
    private String videoPath = "";
    private String videoLength = "";
    private String videoUri = "";
    private String audioPath = "";
    private String frameNumber = "";
    private String imageFrame = "";
    private String finalVideo = "";
    private int isMandatory = 0;
    private int isRecordedUsingHeadphone = 0;

    public String getFinalVideo() {
        return finalVideo;
    }

    public void setFinalVideo(String finalVideo) {
        this.finalVideo = finalVideo;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getVideoLength() {
        return videoLength;
    }

    public void setVideoLength(String videoLength) {
        this.videoLength = videoLength;
    }

    public String getVideoUri() {
        return videoUri;
    }

    public void setVideoUri(String videoUri) {
        this.videoUri = videoUri;
    }

    public String getAudioPath() {
        return audioPath;
    }

    public void setAudioPath(String audioPath) {
        this.audioPath = audioPath;
    }

    public String getFrameNumber() {
        return frameNumber;
    }

    public void setFrameNumber(String frameNumber) {
        this.frameNumber = frameNumber;
    }

    public String getImageFrame() {
        return imageFrame;
    }

    public void setImageFrame(String imageFrame) {
        this.imageFrame = imageFrame;
    }

    public int getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(int isMandatory) {
        this.isMandatory = isMandatory;
    }

    public int getIsRecordedUsingHeadphone() {
        return isRecordedUsingHeadphone;
    }

    public void setIsRecordedUsingHeadphone(int isRecordedUsingHeadphone) {
        this.isRecordedUsingHeadphone = isRecordedUsingHeadphone;
    }
}
