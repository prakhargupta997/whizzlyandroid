package com.whizzly.self_collab;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;

import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.gson.Gson;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.beans.CollabBarBean;
import com.whizzly.collab_module.beans.VideoFramesBean;
import com.whizzly.collab_module.collab_model.CollabPostModel;
import com.whizzly.collab_module.collab_model.CollabVideoArray;
import com.whizzly.collab_module.fragment.CollabBarFragment;
import com.whizzly.collab_module.fragment.PostVideoBottomSheetFragment;
import com.whizzly.collab_module.interfaces.OnSendTimeRange;
import com.whizzly.databinding.ActivitySelfCollabPreviewBinding;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;
import com.whizzly.utils.FFMpegCommandListener;
import com.whizzly.utils.FFMpegCommands;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import static com.google.android.exoplayer2.ui.AspectRatioFrameLayout.RESIZE_MODE_FIT;
import static com.google.android.exoplayer2.ui.AspectRatioFrameLayout.RESIZE_MODE_ZOOM;

public class SelfCollabPreviewActivity extends BaseActivity implements OnSendTimeRange {

    public ArrayList<VideoFramesBean> mVideoFrameBeanList = new ArrayList<>();
    private ActivitySelfCollabPreviewBinding mActivitySelfCollabPreviewBinding;
    private ArrayList<SelfCollabVideoDataBean> mSelfCollabVideoDataBeanArrayList;
    private int templeteSize;
    private String musicId;
    private ArrayList<CollabBarBean> mCollabBarBeans;
    private FFMpegCommands ffMpegCommands;
    private String mCollabedVideoFilePath;
    private String mergedVideoWithAudioFile = "";
    private String duration = "";
    private String videoThumbnail = "";
    private SimpleExoPlayer mExoPlayer;
    private String mergedAudio = "";
    private long durationInMillies;
    private int frameWidth = 60;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivitySelfCollabPreviewBinding = DataBindingUtil.setContentView(this, R.layout.activity_self_collab_preview);
        ffMpegCommands = FFMpegCommands.getInstance(this);
        initializeFFMPEG();
        getData();
        setListeners();
        mActivitySelfCollabPreviewBinding.flProgressBar.setVisibility(View.VISIBLE);
        mActivitySelfCollabPreviewBinding.ivCollabBarColor.setOnClickListener(view -> {
            mActivitySelfCollabPreviewBinding.ivCollabBarColor.setEnabled(false);
            new Handler().postDelayed(() -> mActivitySelfCollabPreviewBinding.ivCollabBarColor.setEnabled(true), 2000);
            CollabBarFragment collabBarFragment = new CollabBarFragment();
            Bundle collabBundle = new Bundle();
            collabBundle.putString(AppConstants.ClassConstants.VIDEO_FILE_PATH, mergedVideoWithAudioFile);
            collabBundle.putInt(AppConstants.ClassConstants.TEMPLETE_SIZE, templeteSize);
            collabBundle.putString(AppConstants.ClassConstants.VIDEO_FILE_URI, String.valueOf(Uri.parse(mergedVideoWithAudioFile)));
            collabBundle.putString(AppConstants.ClassConstants.VIDEO_LENGTH, duration);
            collabBundle.putInt(AppConstants.ClassConstants.FRAME_SIZE, frameWidth);
            if (mCollabBarBeans.size() > 0) {
                collabBundle.putParcelableArrayList(AppConstants.ClassConstants.COLOR_BAR_DATA, mCollabBarBeans);
            }
            collabBundle.putLong(AppConstants.ClassConstants.DURATION_IN_MILLIES, durationInMillies);
            collabBundle.putParcelableArrayList(AppConstants.ClassConstants.FRAMES_LIST, mVideoFrameBeanList);
            collabBarFragment.setArguments(collabBundle);
            collabBarFragment.setCancelable(false);
            collabBarFragment.show(getSupportFragmentManager(), CollabBarFragment.class.getCanonicalName());
        });
    }

    private void getFramesFromVideo() {
        try {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            try {
                retriever.setDataSource(this, Uri.fromFile(new File(mergedVideoWithAudioFile)));
            } catch (Exception e) {
                System.out.println("Exception= " + e);
            }
            mVideoFrameBeanList.clear();
            String duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            if (duration != null) {
                durationInMillies = Long.parseLong(duration); //duration in millisec
                int numberOfFrames = getNumberOfFramesOnScreen();
                frameWidth = getWindowManager().getDefaultDisplay().getWidth() / numberOfFrames;
                int timeInterval = (int) (durationInMillies / numberOfFrames);
                for (int i = 1; i <= numberOfFrames; i++) {
                    VideoFramesBean videoFramesBean = new VideoFramesBean();
                    videoFramesBean.setFrame(retriever.getFrameAtTime((long) (timeInterval * i * 1000000), MediaMetadataRetriever.OPTION_CLOSEST_SYNC));
                    mVideoFrameBeanList.add(videoFramesBean);
                }
            }
            retriever.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mActivitySelfCollabPreviewBinding.flProgressBar.setVisibility(View.GONE);
    }

    private int getNumberOfFramesOnScreen() {
        int number = 1;
        int screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        for (int i = 60; i < screenWidth; i++) {
            if (screenWidth % i == 0) {
                number = i;
                break;
            }
        }
        return screenWidth / (number * 2);
    }

    private void setListeners() {
        mActivitySelfCollabPreviewBinding.btnPost.setOnClickListener(v -> {
            postVideo();
        });

        mActivitySelfCollabPreviewBinding.ivCancelVideo.setOnClickListener(v -> {
            discardRecording();
        });
    }

    @Override
    public void onBackPressed() {
        discardRecording();
    }

    private void discardRecording() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.txt_discard_collab)
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> discard())
                .setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss())
                .create().show();
    }

    private void initializeFFMPEG() {
        try {
            ffMpegCommands.initializeFFmpeg(new FFMpegCommandListener() {
                @Override
                public void onSuccess(String message) {

                }

                @Override
                public void onProgress(String message) {

                }

                @Override
                public void onFailure(String message) {

                }

                @Override
                public void onStart() {

                }

                @Override
                public void onFinish() {

                }
            });
        } catch (FFmpegNotSupportedException e) {
            e.printStackTrace();
        }
    }

    private void getData() {
        Intent intent = getIntent();
        mSelfCollabVideoDataBeanArrayList = (ArrayList<SelfCollabVideoDataBean>) intent.getSerializableExtra(AppConstants.ClassConstants.VIDEO_DATA);
        mCollabBarBeans = intent.getParcelableArrayListExtra(AppConstants.ClassConstants.COLOR_BAR_DATA);
        templeteSize = intent.getIntExtra(AppConstants.ClassConstants.TEMPLETE_SIZE, 2);
        musicId = intent.getStringExtra(AppConstants.ClassConstants.MUSIC_ID);
        mCollabedVideoFilePath = getVideoFileCollabDetails();
        durationInMillies = Long.parseLong(mSelfCollabVideoDataBeanArrayList.get(0).getVideoLength());
        makeCollabs();
        setIndeteminateProgressBar(mCollabBarBeans);
    }

    private void makeCollabs() {
        Collections.sort(mSelfCollabVideoDataBeanArrayList, (selfCollabVideoDataBean, t1) -> {
            return selfCollabVideoDataBean.getFrameNumber().compareTo(t1.getFrameNumber());
        });

        switch (mSelfCollabVideoDataBeanArrayList.size()) {
            case 2:
                makeTwoVideoCollab();
                break;
            case 3:
                makeThreeVideoCollab();
                break;
            case 4:
                makeFourVideoCollab();
                break;
            case 5:
                makeFiveVideoCollab();
                break;
            case 6:
                makeSixVideoCollab();
                break;
            case 1:
                mCollabedVideoFilePath = mSelfCollabVideoDataBeanArrayList.get(0).getVideoPath();
                addAudioToVideo();
                extractThumbnail();
                break;
        }
    }

    private void mergeAudio() {
        mergedAudio = getAudioFilePath(this);
        if (checkIsRecordingHeadphones()) {
            switch (mSelfCollabVideoDataBeanArrayList.size()) {
                case 2:
                    mergeTwoAudio(mSelfCollabVideoDataBeanArrayList.get(0).getAudioPath(), mSelfCollabVideoDataBeanArrayList.get(1).getAudioPath(), mergedAudio);
                    break;
                case 3:
                    mergeThreeAudios(mSelfCollabVideoDataBeanArrayList.get(0).getAudioPath(), mSelfCollabVideoDataBeanArrayList.get(1).getAudioPath(), mSelfCollabVideoDataBeanArrayList.get(2).getAudioPath(), getAudioFilePath(this), mergedAudio);
                    break;
                case 4:
                    mergeFourAudios(mSelfCollabVideoDataBeanArrayList.get(0).getAudioPath(), mSelfCollabVideoDataBeanArrayList.get(1).getAudioPath(), mSelfCollabVideoDataBeanArrayList.get(2).getAudioPath(), mSelfCollabVideoDataBeanArrayList.get(3).getAudioPath(), getAudioFilePath(this), getAudioFilePath(this), mergedAudio);
                    break;
                case 5:
                    mergeFiveAudios(mSelfCollabVideoDataBeanArrayList.get(0).getAudioPath(), mSelfCollabVideoDataBeanArrayList.get(1).getAudioPath(), mSelfCollabVideoDataBeanArrayList.get(2).getAudioPath(), mSelfCollabVideoDataBeanArrayList.get(3).getAudioPath(), mSelfCollabVideoDataBeanArrayList.get(4).getAudioPath(), getAudioFilePath(this), getAudioFilePath(this), getAudioFilePath(this), mergedAudio);
                    break;
                case 6:
                    mergeSixAudios(mSelfCollabVideoDataBeanArrayList.get(0).getAudioPath(), mSelfCollabVideoDataBeanArrayList.get(1).getAudioPath(), mSelfCollabVideoDataBeanArrayList.get(2).getAudioPath(), mSelfCollabVideoDataBeanArrayList.get(3).getAudioPath(), mSelfCollabVideoDataBeanArrayList.get(4).getAudioPath(), mSelfCollabVideoDataBeanArrayList.get(5).getAudioPath(), getAudioFilePath(this), getAudioFilePath(this), getAudioFilePath(this), getAudioFilePath(this), mergedAudio);
                    break;
            }
        } else {
            mergedAudio = mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).getAudioPath();
            addAudioToVideo();
        }
    }

    private boolean checkIsRecordingHeadphones() {
        int count = 0;
        for (int i = 0; i < mSelfCollabVideoDataBeanArrayList.size(); i++) {
            if (mSelfCollabVideoDataBeanArrayList.get(i).getIsRecordedUsingHeadphone() == 1) {
                count++;
            }
        }
        return count != 0;
    }

    private String getVideoFileCollabDetails() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "collab_" + System.currentTimeMillis() + ".mp4";
    }

    private void makeSixVideoCollab() {
        try {
            ffMpegCommands.mergeSixVideos(mSelfCollabVideoDataBeanArrayList.get(0).getVideoPath(),
                    mSelfCollabVideoDataBeanArrayList.get(1).getVideoPath(),
                    mSelfCollabVideoDataBeanArrayList.get(2).getVideoPath(), mSelfCollabVideoDataBeanArrayList.get(3).getVideoPath(),
                    mSelfCollabVideoDataBeanArrayList.get(4).getVideoPath(), mSelfCollabVideoDataBeanArrayList.get(5).getVideoPath(),
                    mCollabedVideoFilePath, new FFMpegCommandListener() {
                        @Override
                        public void onSuccess(String message) {
                            mergeAudio();
                        }

                        @Override
                        public void onProgress(String message) {
                            Log.e("onProgress: ", message);
                        }

                        @Override
                        public void onFailure(String message) {
                            Log.e("onFailure: ", message);
                        }

                        @Override
                        public void onStart() {

                        }

                        @Override
                        public void onFinish() {

                        }
                    });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    private String getAudioFilePath(Context context) {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "audio_" + System.currentTimeMillis() + ".mp4";
    }

    private void makeFiveVideoCollab() {
        try {
            ffMpegCommands.mergeFiveVideos(mSelfCollabVideoDataBeanArrayList.get(0).getVideoPath(),
                    mSelfCollabVideoDataBeanArrayList.get(1).getVideoPath(), mSelfCollabVideoDataBeanArrayList.get(2).getVideoPath(),
                    mSelfCollabVideoDataBeanArrayList.get(3).getVideoPath(), mSelfCollabVideoDataBeanArrayList.get(4).getVideoPath(),
                    mCollabedVideoFilePath, new FFMpegCommandListener() {
                        @Override
                        public void onSuccess(String message) {
                            mergeAudio();
                        }

                        @Override
                        public void onProgress(String message) {
                            Log.e("onProgress: ", message);
                        }

                        @Override
                        public void onFailure(String message) {
                            Log.e("onFailure: ", message);
                        }

                        @Override
                        public void onStart() {

                        }

                        @Override
                        public void onFinish() {

                        }
                    });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    private void makeFourVideoCollab() {
        try {
            ffMpegCommands.mergeFourVideos(mSelfCollabVideoDataBeanArrayList.get(0).getVideoPath(),
                    mSelfCollabVideoDataBeanArrayList.get(1).getVideoPath(), mSelfCollabVideoDataBeanArrayList.get(2).getVideoPath(),
                    mSelfCollabVideoDataBeanArrayList.get(3).getVideoPath(), mCollabedVideoFilePath, new FFMpegCommandListener() {
                        @Override
                        public void onSuccess(String message) {
                            mergeAudio();
                        }

                        @Override
                        public void onProgress(String message) {
                            Log.e("onProgress: ", message);
                        }

                        @Override
                        public void onFailure(String message) {
                            Log.e("onFailure: ", message);
                        }

                        @Override
                        public void onStart() {

                        }

                        @Override
                        public void onFinish() {

                        }
                    });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    private void makeThreeVideoCollab() {
        try {
            ffMpegCommands.mergeThreeVideos(mSelfCollabVideoDataBeanArrayList.get(0).getVideoPath(),
                    mSelfCollabVideoDataBeanArrayList.get(1).getVideoPath(), mSelfCollabVideoDataBeanArrayList.get(2).getVideoPath(),
                    mCollabedVideoFilePath, new FFMpegCommandListener() {
                        @Override
                        public void onSuccess(String message) {
                            mergeAudio();
                        }

                        @Override
                        public void onProgress(String message) {
                            Log.e("onProgress: ", message);
                        }

                        @Override
                        public void onFailure(String message) {
                            Log.e("onFailure: ", message);
                        }

                        @Override
                        public void onStart() {

                        }

                        @Override
                        public void onFinish() {

                        }
                    });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    private void mergeTwoAudio(String fileOne, String fileTwo, String finalFile) {

        ffMpegCommands.mergeTwoAudioFiles(fileOne, fileTwo, finalFile, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                addAudioToVideo();
                /*Log.e("onSuccess: ", message);
                extractThumbnail();
                setVideoPlayer(finalFile);
                mActivitySelfCollabPreviewBinding.flProgressBar.setVisibility(View.GONE);*/
            }

            @Override
            public void onProgress(String message) {
            }

            @Override
            public void onFailure(String message) {
            }

            @Override
            public void onStart() {
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void mergeThreeAudios(String fileOne, String fileTwo, String fileThree, String finalFile, String finalFileTwo) {
        ffMpegCommands.mergeThreeAudioFiles(fileOne, fileTwo, fileThree, finalFile, finalFileTwo, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                addAudioToVideo();
            }

            @Override
            public void onProgress(String message) {

            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onStart() {
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void mergeFourAudios(String fileOne, String fileTwo, String fileThree, String fileFour, String finalFile, String finalFileTwo, String finalFileThree) {
        ffMpegCommands.mergeFourAudioFiles(fileOne, fileTwo, fileThree, fileFour, finalFile, finalFileTwo, finalFileThree, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                addAudioToVideo();
            }

            @Override
            public void onProgress(String message) {

            }

            @Override
            public void onFailure(String message) {
            }

            @Override
            public void onStart() {
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void mergeFiveAudios(String fileOne, String fileTwo, String fileThree, String fileFour, String fileFive, String finalFile, String finalFileTwo, String finalFileThree, String finalFileFour) {
        ffMpegCommands.mergeFiveAudioFiles(fileOne, fileTwo, fileThree, fileFour, fileFive, finalFile, finalFileTwo, finalFileThree, finalFileFour, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                addAudioToVideo();
            }

            @Override
            public void onProgress(String message) {

            }

            @Override
            public void onFailure(String message) {
            }

            @Override
            public void onStart() {
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void mergeSixAudios(String fileOne, String fileTwo, String fileThree, String fileFour, String fileFive, String fileSix, String finalFile, String finalFileTwo, String finalFileThree,
                                String finalFileFour, String finalFileFive) {
        ffMpegCommands.mergeSixAudioFiles(fileOne, fileTwo, fileThree, fileFour, fileFive, fileSix, finalFile, finalFileTwo, finalFileThree, finalFileFour, finalFileFive, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                addAudioToVideo();
            }

            @Override
            public void onProgress(String message) {

            }

            @Override
            public void onFailure(String message) {
            }

            @Override
            public void onStart() {
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void makeTwoVideoCollab() {
        try {
            ffMpegCommands.mergeTwoVideos(mSelfCollabVideoDataBeanArrayList.get(0).getVideoPath(),
                    mSelfCollabVideoDataBeanArrayList.get(1).getVideoPath(), mCollabedVideoFilePath, new FFMpegCommandListener() {
                        @Override
                        public void onSuccess(String message) {
                            mergeAudio();
                        }

                        @Override
                        public void onProgress(String message) {
                            Log.e("onProgress: ", message);
                        }

                        @Override
                        public void onFailure(String message) {
                            Log.e("onFailure: ", message);
                        }

                        @Override
                        public void onStart() {

                        }

                        @Override
                        public void onFinish() {

                        }
                    });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    private String getAudioWithVideoPath() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "mergedVideoWithSong_" + System.currentTimeMillis() + ".mp4";
    }

    private void addAudioToVideo() {
        mergedVideoWithAudioFile = getAudioWithVideoPath();
        ffMpegCommands.mergeSongWithVideo(mCollabedVideoFilePath, mergedAudio,
                mergedVideoWithAudioFile, new FFMpegCommandListener() {
                    @Override
                    public void onSuccess(String message) {
                        Log.e("onSuccess: ", message);
                        extractThumbnail();
                        getFramesFromVideo();
                        setVideoPlayer(mergedVideoWithAudioFile);
                    }

                    @Override
                    public void onProgress(String message) {

                    }

                    @Override
                    public void onFailure(String message) {

                    }

                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onFinish() {

                    }
                });
    }

    private void setVideoPlayer(String videoFile) {
        mExoPlayer = ExoPlayerFactory.newSimpleInstance(this,
                new DefaultRenderersFactory(this),
                new DefaultTrackSelector(), new DefaultLoadControl());
        mExoPlayer.setPlayWhenReady(true);
        mExoPlayer.setRepeatMode(Player.REPEAT_MODE_ONE);
        Uri uri = Uri.parse(videoFile);
        MediaSource mediaSource = buildMediaSource(uri);
        mExoPlayer.prepare(mediaSource);
        mActivitySelfCollabPreviewBinding.pvPreview.setPlayer(mExoPlayer);
        if (mSelfCollabVideoDataBeanArrayList.size() == 2) {
            mActivitySelfCollabPreviewBinding.pvPreview.setResizeMode(RESIZE_MODE_FIT);
        } else {
            mActivitySelfCollabPreviewBinding.pvPreview.setResizeMode(RESIZE_MODE_ZOOM);
        }
    }

    private MediaSource buildMediaSource(Uri uri) {
        return new ExtractorMediaSource.Factory(
                new DefaultDataSourceFactory(this, "whizzly-app")).
                createMediaSource(uri);
    }

    private void extractThumbnail() {
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(mCollabedVideoFilePath);
        duration = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        mediaMetadataRetriever.release();
        videoThumbnail = getImageFile();
        ffMpegCommands.getFrameFromVideo(mCollabedVideoFilePath, videoThumbnail, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {

            }

            @Override
            public void onProgress(String message) {

            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onStart() {

            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void discard() {
        finish();
    }

    private CollabPostModel makeBeanForPosting() {
        CollabPostModel collabPostModel = new CollabPostModel();
        collabPostModel.setCollabId(0);
        collabPostModel.setCollabDuration(Integer.parseInt(duration));
        collabPostModel.setUserId(DataManager.getInstance().getUserId());
        if (!TextUtils.isEmpty(musicId))
            collabPostModel.setCollabMusicId(Integer.valueOf(musicId));
        collabPostModel.setCollabUrl(mergedVideoWithAudioFile);
        collabPostModel.setCollabTotalFrames(templeteSize);
        collabPostModel.setCollabTitle(null);
        collabPostModel.setCollabWatermarkUrl(null);
        collabPostModel.setCollabHashtags(null);
        collabPostModel.setCollabThumbnailUrl(videoThumbnail);
        collabPostModel.setCollabMusicUrl(mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).getAudioPath());
        collabPostModel.setCollabAdditionalInfo(new Gson().toJson(mCollabBarBeans));
        collabPostModel.setCollabPrivacyTypeId(0);
        ArrayList<CollabVideoArray> videoArrays = new ArrayList<>();
        for (int i = 0; i < mSelfCollabVideoDataBeanArrayList.size(); i++) {
            CollabVideoArray videoArray = new CollabVideoArray();
            videoArray.setVideoIsMandatory(mSelfCollabVideoDataBeanArrayList.get(i).getIsMandatory());
            if (!TextUtils.isEmpty(mSelfCollabVideoDataBeanArrayList.get(i).getVideoLength()))
                videoArray.setVideoDuration(Integer.valueOf(mSelfCollabVideoDataBeanArrayList.get(i).getVideoLength()));
            videoArray.setVideoUrl(mSelfCollabVideoDataBeanArrayList.get(i).getFinalVideo());
            videoArray.setVideoThumbnailUrl(mSelfCollabVideoDataBeanArrayList.get(i).getImageFrame());
            videoArray.setVideoTitle("");
            videoArray.setVideoFrameNo(Integer.valueOf(mSelfCollabVideoDataBeanArrayList.get(i).getFrameNumber()));
            videoArrays.add(videoArray);
        }
        collabPostModel.setCollabVideoArray(videoArrays);
        return collabPostModel;
    }

    private void postVideo() {
        PostVideoBottomSheetFragment postVideoBottomSheetFragment = new PostVideoBottomSheetFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("post_data", makeBeanForPosting());
        bundle.putString("is_from", "2");
        postVideoBottomSheetFragment.setArguments(bundle);
        postVideoBottomSheetFragment.show(getSupportFragmentManager(), PostVideoBottomSheetFragment.class.getCanonicalName());
    }

    private String getImageFile() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "frame_" + System.currentTimeMillis() + ".png";
    }

    @Override
    protected void onPause() {
        super.onPause();
        mExoPlayer.setPlayWhenReady(false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mExoPlayer.stop();
        mExoPlayer.release();
    }

    @Override
    public void onSendTimeRange(ArrayList<CollabBarBean> collabBarBeanArrayList) {
        mCollabBarBeans = collabBarBeanArrayList;
        setIndeteminateProgressBar(mCollabBarBeans);
    }

    private int dpToPx(float dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    private void setIndeteminateProgressBar(ArrayList<CollabBarBean> mCollabBarBeans){
        mActivitySelfCollabPreviewBinding.llVideoColor.removeAllViews();
        int screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) mActivitySelfCollabPreviewBinding.llVideoColor.getLayoutParams();
        for (int i = 0; i < mCollabBarBeans.size(); i++) {
            View view = new View(this);
            float start = (mCollabBarBeans.get(i).getStartTime() * screenWidth) / durationInMillies;
            float end = (mCollabBarBeans.get(i).getEndTime() * screenWidth) / durationInMillies;
            float width = end - start;
            view.setBackgroundColor(Color.parseColor(mCollabBarBeans.get(i).getColorCode()));
            RelativeLayout.LayoutParams viewLayoutParams = null;
            switch (mCollabBarBeans.get(i).getCode()) {
                case "0":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templeteSize + 1)), this));
                    view.setY((float) dpToPx(20 / (templeteSize + 1), this));
                    break;
                case "1":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templeteSize + 1)), this));
                    view.setY((float) dpToPx(2 * (20 / (templeteSize + 1)), this));
                    break;
                case "2":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templeteSize + 1)), this));
                    view.setY((float) dpToPx(3 * (20 / (templeteSize + 1)), this));
                    break;
                case "3":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templeteSize + 1)), this));
                    view.setY((float) dpToPx(4 * (20 / (templeteSize + 1)), this));
                    break;
                case "4":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templeteSize + 1)), this));
                    view.setY((float) dpToPx(5 * (20 / (templeteSize + 1)), this));
                    break;
                case "5":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templeteSize + 1)), this));
                    view.setY((float) dpToPx(6 * (20 / (templeteSize + 1)), this));
                    break;
                case "6":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templeteSize + 1)), this));
                    view.setY(0);
                    break;
            }
            view.setX(start);
            view.setLayoutParams(viewLayoutParams);
            mActivitySelfCollabPreviewBinding.llVideoColor.addView(view);
        }
        mActivitySelfCollabPreviewBinding.llVideoColor.setLayoutParams(layoutParams);
    }
}
