package com.whizzly.self_collab;

import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Point;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.daasuu.camerarecorder.CameraRecordListener;
import com.daasuu.camerarecorder.CameraRecorder;
import com.daasuu.camerarecorder.CameraRecorderBuilder;
import com.daasuu.camerarecorder.LensFacing;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.camera_utils.widget.SampleGLView;
import com.whizzly.collab_module.TimerDialog;
import com.whizzly.collab_module.beans.CollabBarBean;
import com.whizzly.collab_module.gallery_trimmer.CollabGalleryTrimmerActivity;
import com.whizzly.collab_module.interfaces.OnTimerDialogTimeSelector;
import com.whizzly.databinding.ActivitySelfCollabRecordBinding;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.progress_with_divider.CountDownTimerWithPause;
import com.whizzly.utils.progress_with_divider.SegmentedProgressBar;

import java.io.File;
import java.util.ArrayList;

import static android.widget.RelativeLayout.CENTER_HORIZONTAL;

public class SelfCollabRecordActivity extends BaseActivity implements OnTimerDialogTimeSelector, OnClickSendListener {

    protected CameraRecorder cameraRecorder;
    protected LensFacing lensFacing = LensFacing.FRONT;
    private ActivitySelfCollabRecordBinding mActivitySelfCollabRecordBinding;
    private int totalNoOfFrames = 2;
    private SegmentedProgressBar mPbRecordingBar;
    private FrameLayout flCamera;
    private boolean playWhenReady = false;
    private SimpleExoPlayer mExoplayerOne, mExoplayerTwo, mExoplayerThree, mExoplayerFour, mExoplayerFive, mExoplayerSix, mExoplayerMusic;
    private boolean mIsRecordingVideo;
    private String mNextVideoAbsolutePath;
    private SampleGLView sampleGLView;
    private ArrayList<Long> mProgress = new ArrayList<>();
    private ArrayList<String> mVideoStack = new ArrayList<>();
    private String videoFromFileManager;
    private boolean isFlashActive = false;
    private SelfCollabRecordModel mSelfCollabRecordModel;
    private ArrayList<CollabBarBean> mCollabBarMap;
    private String mMusicId;
    private ArrayList<SelfCollabVideoDataBean> mSelfCollabVideoDataBeanArrayList = new ArrayList<>();
    private int templeteSize;
    private CountDownTimerWithPause mCountDownTimerWithPause;
    private int currentVolume = -1;
    private int isRecordingUsingHeadphone = 0;
    private StringBuilder stringBuilder = new StringBuilder();
    private Handler handler = new Handler();

    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivitySelfCollabRecordBinding = DataBindingUtil.setContentView(this, R.layout.activity_self_collab_record);
        SelfCollabRecordViewModel mSelfCollabRecordViewModel = ViewModelProviders.of(this).get(SelfCollabRecordViewModel.class);
        mSelfCollabRecordModel = new SelfCollabRecordModel();
        mActivitySelfCollabRecordBinding.setModel(mSelfCollabRecordModel);
        mActivitySelfCollabRecordBinding.setViewModel(mSelfCollabRecordViewModel);
        mSelfCollabRecordViewModel.setOnClickSendListener(this);
        clearStatusBar();
        getData();
        onTouchRecord();
        stopRecording();
    }

    private boolean getMicrophoneAvailable() {
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        return audioManager.isWiredHeadsetOn();
    }

    private void onTouchRecord() {
        try {
            mActivitySelfCollabRecordBinding.ivRecord.setOnTouchListener((view, motionEvent) -> {
                if (!mSelfCollabRecordModel.getIsStartTimer()) {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_UP:
                            if (mIsRecordingVideo) {
                                mIsRecordingVideo = false;
                                pauseExoplayer();
                                stopRecordingVideo();
                                mCountDownTimerWithPause.pause();
                            }
                            break;
                    }
                }
                return false;
            });

            mActivitySelfCollabRecordBinding.ivRecord.setOnLongClickListener(v -> {
                if (!mIsRecordingVideo) {
                    mIsRecordingVideo = true;
                    startRecording();
                    startExoplayer();
                    mCountDownTimerWithPause.resume();
                }
                return true;
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setProgressBar() {
        mPbRecordingBar.setCornerRadius(0f);
        mPbRecordingBar.setDividerColor(Color.WHITE);
        mPbRecordingBar.setDividerEnabled(true);
        mPbRecordingBar.setDividerWidth(2);
        mPbRecordingBar.setShader(new int[]{getResources().getColor(R.color.Green), getResources().getColor(R.color.Green), getResources().getColor(R.color.Green)});
        mPbRecordingBar.enableAutoProgressView(getMaxVideoDuration());
        mPbRecordingBar.pause();
    }

    private int getMaxVideoDuration() {
        if (mSelfCollabVideoDataBeanArrayList.size() > 0) {
            String duration = mSelfCollabVideoDataBeanArrayList.get(0).getVideoLength();
            if (duration != null)
                return Integer.parseInt(duration);
            else return 30000;
        } else {
            return 30000;
        }
    }

    private void stopRecording() {
        mCountDownTimerWithPause = new CountDownTimerWithPause(getMaxVideoDuration(), 1000, false) {
            public void onTick(long millisUntilFinished) {
                if (millisUntilFinished / 1000 == 0) {
                    stopRecordingVideo();
                }
            }

            public void onFinish() {
                stopRecordingVideo();
            }
        }.create();
    }

    public void startRecording() {
        setFlagProgress();
        mNextVideoAbsolutePath = getVideoFilePath(this);
        mSelfCollabRecordModel.setIsRecordPressed(true);
        mSelfCollabRecordModel.setIsVideoStarted(true);
        mSelfCollabRecordModel.setIsRemoveVisible(false);
        mIsRecordingVideo = true;
        cameraRecorder.start(mNextVideoAbsolutePath);
        if (mProgress.size() > 0)
            mPbRecordingBar.resume(mProgress.get(mProgress.size() - 1));
        else
            mPbRecordingBar.resume();
        mActivitySelfCollabRecordBinding.tvMessage.setVisibility(View.INVISIBLE);
    }

    private void stopRecordingVideo() {
        cameraRecorder.stop();
        mSelfCollabRecordModel.setIsRemoveVisible(true);
        mIsRecordingVideo = false;
        mSelfCollabRecordModel.setIsVideoStarted(false);
        mSelfCollabRecordModel.setIsRecordPressed(false);
        mVideoStack.add(mNextVideoAbsolutePath);
        mPbRecordingBar.pause();
        mPbRecordingBar.addDivider();
        handler.removeCallbacks(null);
        mActivitySelfCollabRecordBinding.tvMessage.setVisibility(View.VISIBLE);
    }

    private void getData() {
        Intent intent = getIntent();
        mSelfCollabVideoDataBeanArrayList = (ArrayList<SelfCollabVideoDataBean>) intent.getSerializableExtra(AppConstants.ClassConstants.VIDEO_DATA);
        mMusicId = String.valueOf(intent.getIntExtra(AppConstants.ClassConstants.MUSIC_ID, 0));
        templeteSize = intent.getIntExtra(AppConstants.ClassConstants.TEMPLETE_SIZE, 0);
        totalNoOfFrames = intent.getIntExtra(AppConstants.ClassConstants.FRAMES, 2);
        mCollabBarMap = intent.getParcelableArrayListExtra(AppConstants.ClassConstants.COLOR_BAR_DATA);
    }

    private void setTextColorDescription() {
        for (int i = 0; i < mCollabBarMap.size(); i++) {
            if (mPbRecordingBar.progressInMillies() >= (long) mCollabBarMap.get(i).getStartTime() && mPbRecordingBar.progressInMillies() < (long) mCollabBarMap.get(i).getEndTime()) {
                stringBuilder.append(mCollabBarMap.get(i).getDescription() + ",\n");
            }
        }
        if (stringBuilder.length() > 0)
            stringBuilder.deleteCharAt(stringBuilder.length() - 2);
        stringBuilder.trimToSize();
        int previousSize = 0;
        SpannableString spannableString = new SpannableString(stringBuilder);
        for (int i = 0; i < mCollabBarMap.size(); i++) {
            if (mPbRecordingBar.progressInMillies() >= (long) mCollabBarMap.get(i).getStartTime() && mPbRecordingBar.progressInMillies() < (long) mCollabBarMap.get(i).getEndTime()) {
                ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.parseColor(mCollabBarMap.get(i).getColorCode()));
                spannableString.setSpan(foregroundColorSpan, previousSize, previousSize + mCollabBarMap.get(i).getDescription().length(), SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
                previousSize = previousSize + mCollabBarMap.get(i).getDescription().length() + 2;
            }
        }
        mActivitySelfCollabRecordBinding.tvColorDescription.setText(spannableString);




        if (stringBuilder.length() == 0) {
            mActivitySelfCollabRecordBinding.tvColorDescription.setVisibility(View.INVISIBLE);
        } else {
            mActivitySelfCollabRecordBinding.tvColorDescription.setVisibility(View.VISIBLE);
        }
        stringBuilder.delete(0, stringBuilder.length());
    }

    private void setAdditionalInfoToBar() {
        mActivitySelfCollabRecordBinding.rlVideoColor.setVisibility(View.VISIBLE);
        mActivitySelfCollabRecordBinding.ivFlag.setBackgroundColor(Color.parseColor("#FFFFFF"));
        mActivitySelfCollabRecordBinding.rlVideoColor.removeAllViews();
        int screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) mActivitySelfCollabRecordBinding.rlVideoColor.getLayoutParams();
        for (int i = 0; i < mCollabBarMap.size(); i++) {
            long durationInMillies = Long.parseLong(mSelfCollabVideoDataBeanArrayList.get(0).getVideoLength());
            View view = new View(this);
            float start = (mCollabBarMap.get(i).getStartTime() * screenWidth) / durationInMillies;
            float end = (mCollabBarMap.get(i).getEndTime() * screenWidth) / durationInMillies;
            float width = end - start;
            view.setBackgroundColor(Color.parseColor(mCollabBarMap.get(i).getColorCode()));
            RelativeLayout.LayoutParams viewLayoutParams = null;
            int templateSize = templeteSize;
            switch (mCollabBarMap.get(i).getCode()) {
                case "0":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(20 / (templateSize + 1), this));
                    break;
                case "1":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(2 * (20 / (templateSize + 1)), this));
                    break;
                case "2":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(3 * (20 / (templateSize + 1)), this));
                    break;
                case "3":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(4 * (20 / (templateSize + 1)), this));
                    break;
                case "4":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(5 * (20 / (templateSize + 1)), this));
                    break;
                case "5":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(6 * (20 / (templateSize + 1)), this));
                    break;
                case "6":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY(0);
                    break;
            }
            view.setX(start);
            view.setLayoutParams(viewLayoutParams);
            mActivitySelfCollabRecordBinding.rlVideoColor.addView(view);
        }
        mActivitySelfCollabRecordBinding.rlVideoColor.setLayoutParams(layoutParams);
        mActivitySelfCollabRecordBinding.ivFlag.setX(0f);
    }

    private int dpToPx(float size, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, size, context.getResources().getDisplayMetrics());
    }

    private void manageCameraViews() {
        switch (totalNoOfFrames) {
            case 2:
                mActivitySelfCollabRecordBinding.llCameraOneTwo.setVisibility(View.INVISIBLE);
                mActivitySelfCollabRecordBinding.llVideoFiveSix.setVisibility(View.INVISIBLE);
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mActivitySelfCollabRecordBinding.llCameraThreeFour.getLayoutParams();
                layoutParams.weight = 2.0f;
                mActivitySelfCollabRecordBinding.llCameraThreeFour.setLayoutParams(layoutParams);
                mActivitySelfCollabRecordBinding.llCameraThreeFour.setVisibility(View.VISIBLE);
                flCamera = mActivitySelfCollabRecordBinding.cameraThree;
                mPbRecordingBar = mActivitySelfCollabRecordBinding.pbRecordThree;
                selectFrames(View.GONE, View.GONE, View.VISIBLE, View.VISIBLE, View.GONE, View.GONE);
                break;
            case 3:
                LinearLayout.LayoutParams layoutParams1 = (LinearLayout.LayoutParams) mActivitySelfCollabRecordBinding.llCameraThreeFour.getLayoutParams();
                layoutParams1.weight = 1.0f;
                mActivitySelfCollabRecordBinding.llCameraThreeFour.setLayoutParams(layoutParams1);
                mActivitySelfCollabRecordBinding.llVideoFiveSix.setVisibility(View.GONE);
                flCamera = mActivitySelfCollabRecordBinding.cameraThree;
                mPbRecordingBar = mActivitySelfCollabRecordBinding.pbRecordThree;
                selectFrames(View.VISIBLE, View.VISIBLE, View.VISIBLE, View.GONE, View.GONE, View.GONE);
                break;
            case 4:
                flCamera = mActivitySelfCollabRecordBinding.cameraOne;
                mPbRecordingBar = mActivitySelfCollabRecordBinding.pbRecordOne;
                mActivitySelfCollabRecordBinding.llVideoFiveSix.setVisibility(View.GONE);
                selectFrames(View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.GONE, View.GONE);
                break;
            case 5:
                flCamera = mActivitySelfCollabRecordBinding.cameraFive;
                mPbRecordingBar = mActivitySelfCollabRecordBinding.pbRecordFive;
                selectFrames(View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.GONE);
                break;
            case 6:
                LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) mActivitySelfCollabRecordBinding.llCameraOneTwo.getLayoutParams();
                layoutParams2.weight = 1.0f;
                mActivitySelfCollabRecordBinding.llCameraOneTwo.setLayoutParams(layoutParams2);

                LinearLayout.LayoutParams layoutParams3 = (LinearLayout.LayoutParams) mActivitySelfCollabRecordBinding.llCameraThreeFour.getLayoutParams();
                layoutParams3.weight = 1.0f;
                mActivitySelfCollabRecordBinding.llCameraThreeFour.setLayoutParams(layoutParams3);

                LinearLayout.LayoutParams layoutParams4 = (LinearLayout.LayoutParams) mActivitySelfCollabRecordBinding.llVideoFiveSix.getLayoutParams();
                layoutParams4.weight = 1.0f;
                mActivitySelfCollabRecordBinding.llVideoFiveSix.setLayoutParams(layoutParams4);

                flCamera = mActivitySelfCollabRecordBinding.cameraOne;
                mPbRecordingBar = mActivitySelfCollabRecordBinding.pbRecordOne;
                selectFrames(View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE);
                break;
        }
        initializeCamera();
    }

    private void selectFrames(int one, int two, int three, int four, int five, int six) {
        mActivitySelfCollabRecordBinding.flCamOne.setVisibility(one);
        mActivitySelfCollabRecordBinding.flCamTwo.setVisibility(two);
        mActivitySelfCollabRecordBinding.flCamThree.setVisibility(three);
        mActivitySelfCollabRecordBinding.flCamFour.setVisibility(four);
        mActivitySelfCollabRecordBinding.flCamFive.setVisibility(five);
        mActivitySelfCollabRecordBinding.flCamSix.setVisibility(six);
        switch (flCamera.getId()) {
            case R.id.camera_one:
                switch (totalNoOfFrames) {
                    case 2:
                        if (mSelfCollabVideoDataBeanArrayList.size() > 0) {
                            mExoplayerTwo = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(0).getFinalVideo());
                            mActivitySelfCollabRecordBinding.vvTwo.setPlayer(mExoplayerTwo);
                            mActivitySelfCollabRecordBinding.vvTwo.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                        }
                        break;
                    case 3:
                        if (mSelfCollabVideoDataBeanArrayList.size() > 1) {
                            mExoplayerTwo = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(0).getFinalVideo());
                            mExoplayerThree = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(1).getFinalVideo());
                            mActivitySelfCollabRecordBinding.vvTwo.setPlayer(mExoplayerTwo);
                            mActivitySelfCollabRecordBinding.vvThree.setPlayer(mExoplayerThree);
                            mActivitySelfCollabRecordBinding.vvTwo.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySelfCollabRecordBinding.vvThree.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                        }
                        break;
                    case 4:
                        if (mSelfCollabVideoDataBeanArrayList.size() > 2) {
                            mExoplayerTwo = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(0).getFinalVideo());
                            mExoplayerThree = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(1).getFinalVideo());
                            mExoplayerFour = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(2).getFinalVideo());
                            mActivitySelfCollabRecordBinding.vvTwo.setPlayer(mExoplayerTwo);
                            mActivitySelfCollabRecordBinding.vvThree.setPlayer(mExoplayerThree);
                            mActivitySelfCollabRecordBinding.vvFour.setPlayer(mExoplayerFour);
                            mActivitySelfCollabRecordBinding.vvTwo.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySelfCollabRecordBinding.vvThree.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySelfCollabRecordBinding.vvFour.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                        }
                        break;
                    case 5:
                        if (mSelfCollabVideoDataBeanArrayList.size() > 3) {
                            mExoplayerTwo = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(0).getFinalVideo());
                            mExoplayerThree = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(1).getFinalVideo());
                            mExoplayerFour = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(2).getFinalVideo());
                            mExoplayerFive = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(3).getFinalVideo());
                            mActivitySelfCollabRecordBinding.vvTwo.setPlayer(mExoplayerTwo);
                            mActivitySelfCollabRecordBinding.vvThree.setPlayer(mExoplayerThree);
                            mActivitySelfCollabRecordBinding.vvFour.setPlayer(mExoplayerFour);
                            mActivitySelfCollabRecordBinding.vvFive.setPlayer(mExoplayerFive);
                            mActivitySelfCollabRecordBinding.vvTwo.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySelfCollabRecordBinding.vvThree.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySelfCollabRecordBinding.vvFour.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySelfCollabRecordBinding.vvFive.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
                        }
                        break;
                    case 6:
                        if (mSelfCollabVideoDataBeanArrayList.size() > 4) {
                            mExoplayerTwo = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(0).getFinalVideo());
                            mExoplayerThree = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(1).getFinalVideo());
                            mExoplayerFour = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(2).getFinalVideo());
                            mExoplayerFive = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(3).getFinalVideo());
                            mExoplayerSix = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(4).getFinalVideo());
                            mActivitySelfCollabRecordBinding.vvTwo.setPlayer(mExoplayerTwo);
                            mActivitySelfCollabRecordBinding.vvThree.setPlayer(mExoplayerThree);
                            mActivitySelfCollabRecordBinding.vvFour.setPlayer(mExoplayerFour);
                            mActivitySelfCollabRecordBinding.vvFive.setPlayer(mExoplayerFive);
                            mActivitySelfCollabRecordBinding.vvSix.setPlayer(mExoplayerSix);
                            mActivitySelfCollabRecordBinding.vvTwo.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySelfCollabRecordBinding.vvThree.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySelfCollabRecordBinding.vvFour.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySelfCollabRecordBinding.vvFive.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySelfCollabRecordBinding.vvSix.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                        }
                        break;
                }
                setVideoViews(View.GONE, View.VISIBLE, View.VISIBLE);
                setProgressBars(View.VISIBLE, View.GONE, View.GONE);
                setViews(View.VISIBLE, View.GONE, View.GONE);
                break;
            case R.id.camera_three:
                switch (totalNoOfFrames) {
                    case 2:
                        if (mSelfCollabVideoDataBeanArrayList.size() > 0) {
                            mExoplayerOne = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(0).getFinalVideo());
                            mActivitySelfCollabRecordBinding.vvFour.setPlayer(mExoplayerOne);
                        }
                        break;
                    case 3:
                        if (mSelfCollabVideoDataBeanArrayList.size() > 1) {
                            mExoplayerOne = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(0).getFinalVideo());
                            mExoplayerTwo = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(1).getFinalVideo());
                            mActivitySelfCollabRecordBinding.vvOne.setPlayer(mExoplayerOne);
                            mActivitySelfCollabRecordBinding.vvTwo.setPlayer(mExoplayerTwo);
                            mActivitySelfCollabRecordBinding.vvOne.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySelfCollabRecordBinding.vvTwo.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            Display display = getWindowManager().getDefaultDisplay();
                            Point point = new Point();
                            display.getSize(point);
                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(point.x / 2, ViewGroup.LayoutParams.MATCH_PARENT);
                            layoutParams.addRule(CENTER_HORIZONTAL);
                            mActivitySelfCollabRecordBinding.cameraThree.setLayoutParams(layoutParams);
                        }
                        break;
                    case 4:
                        if (mSelfCollabVideoDataBeanArrayList.size() > 2) {
                            mExoplayerOne = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(0).getFinalVideo());
                            mExoplayerTwo = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(1).getFinalVideo());
                            mExoplayerFour = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(2).getFinalVideo());
                            mActivitySelfCollabRecordBinding.vvOne.setPlayer(mExoplayerOne);
                            mActivitySelfCollabRecordBinding.vvTwo.setPlayer(mExoplayerTwo);
                            mActivitySelfCollabRecordBinding.vvFour.setPlayer(mExoplayerFour);
                        }
                        break;
                    case 5:
                        if (mSelfCollabVideoDataBeanArrayList.size() > 3) {
                            mExoplayerOne = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(0).getFinalVideo());
                            mExoplayerTwo = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(1).getFinalVideo());
                            mExoplayerFour = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(2).getFinalVideo());
                            mExoplayerFive = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(3).getFinalVideo());
                            mActivitySelfCollabRecordBinding.vvOne.setPlayer(mExoplayerOne);
                            mActivitySelfCollabRecordBinding.vvTwo.setPlayer(mExoplayerTwo);
                            mActivitySelfCollabRecordBinding.vvFour.setPlayer(mExoplayerFour);
                            mActivitySelfCollabRecordBinding.vvFive.setPlayer(mExoplayerFive);
                            mActivitySelfCollabRecordBinding.vvFive.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
                        }
                        break;
                    case 6:
                        if (mSelfCollabVideoDataBeanArrayList.size() > 4) {
                            mExoplayerOne = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(0).getFinalVideo());
                            mExoplayerTwo = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(1).getFinalVideo());
                            mExoplayerFour = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(2).getFinalVideo());
                            mExoplayerFive = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(3).getFinalVideo());
                            mExoplayerSix = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(4).getFinalVideo());
                            mActivitySelfCollabRecordBinding.vvOne.setPlayer(mExoplayerOne);
                            mActivitySelfCollabRecordBinding.vvTwo.setPlayer(mExoplayerTwo);
                            mActivitySelfCollabRecordBinding.vvFour.setPlayer(mExoplayerFour);
                            mActivitySelfCollabRecordBinding.vvFive.setPlayer(mExoplayerFive);
                            mActivitySelfCollabRecordBinding.vvSix.setPlayer(mExoplayerSix);
                        }
                        break;
                }
                setVideoViews(View.VISIBLE, View.GONE, View.VISIBLE);
                setProgressBars(View.GONE, View.VISIBLE, View.GONE);
                setViews(View.GONE, View.VISIBLE, View.GONE);
                break;

            case R.id.camera_five:
                switch (totalNoOfFrames) {
                    case 5:
                        if (mSelfCollabVideoDataBeanArrayList.size() > 3) {
                            mExoplayerOne = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(0).getFinalVideo());
                            mExoplayerTwo = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(1).getFinalVideo());
                            mExoplayerFour = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(2).getFinalVideo());
                            mExoplayerThree = setExoPlayer(mSelfCollabVideoDataBeanArrayList.get(3).getFinalVideo());
                            mActivitySelfCollabRecordBinding.vvOne.setPlayer(mExoplayerOne);
                            mActivitySelfCollabRecordBinding.vvTwo.setPlayer(mExoplayerTwo);
                            mActivitySelfCollabRecordBinding.vvFour.setPlayer(mExoplayerFour);
                            mActivitySelfCollabRecordBinding.vvThree.setPlayer(mExoplayerThree);
                            mActivitySelfCollabRecordBinding.vvOne.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySelfCollabRecordBinding.vvTwo.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySelfCollabRecordBinding.vvThree.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            mActivitySelfCollabRecordBinding.vvFour.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
                            Display display = getWindowManager().getDefaultDisplay();
                            Point point = new Point();
                            display.getSize(point);
                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(point.x / 2, ViewGroup.LayoutParams.WRAP_CONTENT);
                            layoutParams.addRule(CENTER_HORIZONTAL);
                            mActivitySelfCollabRecordBinding.cameraFive.setLayoutParams(layoutParams);
                        }
                        break;
                }
                setVideoViews(View.VISIBLE, View.VISIBLE, View.GONE);
                setProgressBars(View.GONE, View.GONE, View.VISIBLE);
                setViews(View.GONE, View.GONE, View.VISIBLE);
                break;
        }
        muteExoplayers();
    }

    private void setFlagProgress() {
        mActivitySelfCollabRecordBinding.tvColorDescription.setVisibility(View.VISIBLE);
        long durationInMillies = Long.parseLong(mSelfCollabVideoDataBeanArrayList.get(0).getVideoLength());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                float valueX = ((float) getWindowManager().getDefaultDisplay().getWidth() / (float) durationInMillies) * (float) mPbRecordingBar.progressInMillies();
                if (valueX < (float) getWindowManager().getDefaultDisplay().getWidth()) {
                    mActivitySelfCollabRecordBinding.ivFlag.setX(valueX);
                    handler.postDelayed(this, 1);
                    setTextColorDescription();
                }
            }
        }, 1);
    }

    private void setVideoViews(int one, int three, int five) {
        mActivitySelfCollabRecordBinding.vvOne.setVisibility(one);
        mActivitySelfCollabRecordBinding.vvTwo.setVisibility(View.VISIBLE);
        mActivitySelfCollabRecordBinding.vvThree.setVisibility(three);
        mActivitySelfCollabRecordBinding.vvFour.setVisibility(View.VISIBLE);
        mActivitySelfCollabRecordBinding.vvFive.setVisibility(five);
        mActivitySelfCollabRecordBinding.vvSix.setVisibility(View.VISIBLE);

    }

    private void seekExoplayer(long seekTo) {
        if (mExoplayerOne != null) {
            playWhenReady = false;
            mExoplayerOne.seekTo(seekTo);
            mExoplayerOne.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerTwo != null) {
            playWhenReady = false;
            mExoplayerTwo.seekTo(seekTo);
            mExoplayerTwo.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerThree != null) {
            playWhenReady = false;
            mExoplayerThree.seekTo(seekTo);
            mExoplayerThree.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerFour != null) {
            playWhenReady = false;
            mExoplayerFour.seekTo(seekTo);
            mExoplayerFour.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerFive != null) {
            playWhenReady = false;
            mExoplayerFive.seekTo(seekTo);
            mExoplayerFive.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerSix != null) {
            playWhenReady = false;
            mExoplayerSix.seekTo(seekTo);
            mExoplayerSix.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerMusic != null) {
            playWhenReady = false;
            mExoplayerMusic.seekTo(seekTo);
            mExoplayerMusic.setPlayWhenReady(playWhenReady);
        }
    }

    private void setViews(int one, int three, int five) {
        mActivitySelfCollabRecordBinding.viewOne.setVisibility(View.INVISIBLE);
        mActivitySelfCollabRecordBinding.viewTwo.setVisibility(View.GONE);
        mActivitySelfCollabRecordBinding.viewThree.setVisibility(View.INVISIBLE);
        mActivitySelfCollabRecordBinding.viewFour.setVisibility(View.GONE);
        mActivitySelfCollabRecordBinding.viewFive.setVisibility(View.INVISIBLE);
        mActivitySelfCollabRecordBinding.viewSix.setVisibility(View.GONE);
    }

    private void setProgressBars(int one, int three, int five) {
        mActivitySelfCollabRecordBinding.pbRecordOne.setVisibility(View.INVISIBLE);
        mActivitySelfCollabRecordBinding.pbRecordTwo.setVisibility(View.GONE);
        mActivitySelfCollabRecordBinding.pbRecordThree.setVisibility(View.INVISIBLE);
        mActivitySelfCollabRecordBinding.pbRecordFour.setVisibility(View.GONE);
        mActivitySelfCollabRecordBinding.pbRecordFive.setVisibility(View.INVISIBLE);
        mActivitySelfCollabRecordBinding.pbRecordSix.setVisibility(View.GONE);
    }

    private SimpleExoPlayer setExoPlayer(String fileName) {
        SimpleExoPlayer exoPlayer = ExoPlayerFactory.newSimpleInstance(this,
                new DefaultRenderersFactory(this),
                new DefaultTrackSelector(), new DefaultLoadControl());
        exoPlayer.setPlayWhenReady(playWhenReady);
        exoPlayer.setRepeatMode(Player.REPEAT_MODE_OFF);
        Uri uri = Uri.fromFile(new File(fileName));
        MediaSource mediaSource = buildMediaSource(uri);
        exoPlayer.prepare(mediaSource);
        return exoPlayer;
    }

    private MediaSource buildMediaSource(Uri uri) {
        return new ExtractorMediaSource.Factory(
                new DefaultDataSourceFactory(this, "whizzly-app")).
                createMediaSource(uri);
    }

    private void deleteRecording() {
        if (mVideoStack.size() > 0) {
            String fileToRemove = mVideoStack.remove(mVideoStack.size() - 1);
            File file = new File(fileToRemove);
            if (file.delete()) {
                Log.e("deleteRecording: ", "Deleted File");
            }
            mActivitySelfCollabRecordBinding.ivRemoveSegment.setVisibility(View.VISIBLE);
        }
        if (mProgress.size() > 1) {
            mProgress.remove(mProgress.size() - 1);
            mPbRecordingBar.removeDivider();
            seekExoplayer(mProgress.get(mProgress.size() - 1));
            mPbRecordingBar.updateProgress(mProgress.get(mProgress.size() - 1));
        } else if (mProgress.size() == 1) {
            mProgress.clear();
            mPbRecordingBar.removeDivider();
            seekExoplayer(0);
            mPbRecordingBar.updateProgress(0);
        }
    }

    private void showRemoveSegmentDialog() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.txt_error_remove_segment)
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> deleteRecording())
                .setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss())
                .create().show();
    }

    private void discardRecording() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.txt_discard_collab)
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> discard())
                .setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss())
                .create().show();
    }

    private void discard() {
        onBackPressed();
    }

    private void chooseVideoFromGallery() {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Video"), AppConstants.ClassConstants.REQUEST_TAKE_GALLERY_VIDEO);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == AppConstants.ClassConstants.REQUEST_TAKE_GALLERY_VIDEO) {
                Uri selectedImageUri = data.getData();
                // MEDIA GALLERY
                videoFromFileManager = getPath(this, selectedImageUri);
                if (videoFromFileManager != null) {
                    Intent intent = new Intent(getApplicationContext(),
                            CollabGalleryTrimmerActivity.class);
                    intent.putExtra(AppConstants.ClassConstants.IS_FROM, 3);
                    intent.putExtra(AppConstants.ClassConstants.VIDEO_DATA, mSelfCollabVideoDataBeanArrayList);
                    intent.putExtra(AppConstants.ClassConstants.MUSIC_ID, mMusicId);
                    intent.putExtra(AppConstants.ClassConstants.TEMPLETE_SIZE, templeteSize);
                    intent.putExtra(AppConstants.ClassConstants.FRAMES, totalNoOfFrames);
                    intent.putExtra(AppConstants.ClassConstants.COLOR_BAR_DATA, mCollabBarMap);
                    intent.putExtra(AppConstants.ClassConstants.VIDEO_FILE_PATH, videoFromFileManager);
                    startActivity(intent);
                    finish();
                }
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void muteExoplayers() {
        switch (mSelfCollabVideoDataBeanArrayList.size()) {
            case 2:
                if (mExoplayerOne != null)
                    mExoplayerOne.setVolume(0F);
                break;
            case 3:
                if (mExoplayerTwo != null)
                    mExoplayerTwo.setVolume(0F);
                if (mExoplayerThree != null)
                    mExoplayerThree.setVolume(0F);
                break;
            case 4:
                if (mExoplayerOne != null)
                    mExoplayerOne.setVolume(0F);
                if (mExoplayerThree != null)
                    mExoplayerThree.setVolume(0F);
                if (mExoplayerFour != null)
                    mExoplayerFour.setVolume(0F);
                break;
            case 5:
                if (mExoplayerTwo != null)
                    mExoplayerTwo.setVolume(0F);
                if (mExoplayerThree != null)
                    mExoplayerThree.setVolume(0F);
                if (mExoplayerFour != null)
                    mExoplayerFour.setVolume(0F);
                if (mExoplayerFive != null)
                    mExoplayerFive.setVolume(0F);
                break;
        }
    }

    private void startExoplayer() {
        if (mExoplayerOne != null) {
            playWhenReady = true;
            mExoplayerOne.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerTwo != null) {
            playWhenReady = true;
            mExoplayerTwo.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerThree != null) {
            playWhenReady = true;
            mExoplayerThree.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerFour != null) {
            playWhenReady = true;
            mExoplayerFour.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerFive != null) {
            playWhenReady = true;
            mExoplayerFive.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerSix != null) {
            playWhenReady = true;
            mExoplayerSix.setPlayWhenReady(playWhenReady);
        }

        if (mExoplayerMusic != null) {
            playWhenReady = true;
            mExoplayerMusic.setPlayWhenReady(playWhenReady);
        }
    }

    private void pauseExoplayer() {
        if (null != mExoplayerMusic) {
            mExoplayerMusic.setPlayWhenReady(false);
        }

        if (null != mExoplayerOne) {
            mExoplayerOne.setPlayWhenReady(false);
        }

        if (null != mExoplayerTwo) {
            mExoplayerTwo.setPlayWhenReady(false);
        }

        if (null != mExoplayerThree) {
            mExoplayerThree.setPlayWhenReady(false);
        }

        if (null != mExoplayerFour) {
            mExoplayerFour.setPlayWhenReady(false);
        }

        if (null != mExoplayerFive) {
            mExoplayerFive.setPlayWhenReady(false);
        }

        if (null != mExoplayerSix) {
            mExoplayerSix.setPlayWhenReady(false);
        }
    }

    private void releaseExoplayer() {
        if (null != mExoplayerMusic) {
            mExoplayerMusic.setPlayWhenReady(false);
            mExoplayerMusic.release();
            mExoplayerMusic = null;
        }

        if (null != mExoplayerOne) {
            mExoplayerOne.setPlayWhenReady(false);
            mExoplayerOne.release();
            mExoplayerOne = null;
        }

        if (null != mExoplayerTwo) {
            mExoplayerTwo.setPlayWhenReady(false);
            mExoplayerTwo.release();
            mExoplayerTwo = null;
        }

        if (null != mExoplayerThree) {
            mExoplayerThree.setPlayWhenReady(false);
            mExoplayerThree.release();
            mExoplayerThree = null;
        }

        if (null != mExoplayerFour) {
            mExoplayerFour.setPlayWhenReady(false);
            mExoplayerFour.release();
            mExoplayerFour = null;
        }

        if (null != mExoplayerFive) {
            mExoplayerFive.setPlayWhenReady(false);
            mExoplayerFive.release();
            mExoplayerFive = null;
        }

        if (null != mExoplayerSix) {
            mExoplayerSix.setPlayWhenReady(false);
            mExoplayerSix.release();
            mExoplayerSix = null;
        }
    }

    private void toggleFlash() {
        if (isFlashActive) {
            if (cameraRecorder != null && cameraRecorder.isFlashSupport()) {
                cameraRecorder.switchFlashMode();
                cameraRecorder.changeAutoFocus();
            }
            mActivitySelfCollabRecordBinding.ivFlashToggle.setImageResource(R.drawable.ic_camera_flash_off);
            isFlashActive = false;
        } else {
            if (cameraRecorder != null && cameraRecorder.isFlashSupport()) {
                cameraRecorder.switchFlashMode();
                cameraRecorder.changeAutoFocus();
            }
            mActivitySelfCollabRecordBinding.ivFlashToggle.setImageResource(R.drawable.ic_camera_flash_on);
            isFlashActive = true;
        }
    }

    private void switchCamera() {
        releaseCamera();
        if (lensFacing == LensFacing.BACK) {
            lensFacing = LensFacing.FRONT;
            mActivitySelfCollabRecordBinding.ivFlashToggle.setImageResource(R.drawable.ic_camera_flash_off);
            isFlashActive = false;
        } else {
            lensFacing = LensFacing.BACK;
            mActivitySelfCollabRecordBinding.ivFlashToggle.setImageResource(R.drawable.ic_camera_flash_off);
            isFlashActive = false;
        }
    }

    private void releaseCamera() {
        if (sampleGLView != null) {
            sampleGLView.onPause();
        }

        if (cameraRecorder != null) {
            cameraRecorder.stop();
            cameraRecorder.release();
            cameraRecorder = null;
        }

        if (sampleGLView != null) {
            flCamera.removeView(sampleGLView);
            sampleGLView = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        pauseExoplayer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        pauseExoplayer();
        manageCameraViews();
        setProgressBar();
        setAdditionalInfoToBar();
        if (!getMicrophoneAvailable()) {
            AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
            currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, (int) (audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) * 0.75), AudioManager.FLAG_PLAY_SOUND);
        }
    }

    @Override
    public void onTimeSelected(int time) {
        setRecordAfterTimer(time);
        mSelfCollabRecordModel.setIsStartTimer(true);
        mSelfCollabRecordModel.setTime("" + ((time / 1000) + 1));
    }

    private void setRecordAfterTimer(int time) {
        new CountDownTimer(time, 1000) {
            @Override
            public void onTick(long l) {
                mSelfCollabRecordModel.setTime("" + l / 1000);
            }

            @Override
            public void onFinish() {
                startRecording();
                mSelfCollabRecordModel.setIsStartTimer(false);
            }
        }.start();
    }

    private void setUpCameraView() {
        FrameLayout frameLayout = flCamera;
        frameLayout.removeAllViews();
        sampleGLView = null;
        sampleGLView = new SampleGLView(getApplicationContext());
        sampleGLView.setTouchListener((event, width, height) -> {
            if (cameraRecorder == null) return;
            cameraRecorder.changeManualFocusPoint(event.getX(), event.getY(), width, height);
        });
        frameLayout.addView(sampleGLView);
    }

    private String getVideoFilePath(Context context) {
        final File dir = context.getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "video_" + System.currentTimeMillis() + ".mp4";
    }

    private void initializeCamera() {
        setUpCameraView();
        mNextVideoAbsolutePath = getVideoFilePath(this);
        cameraRecorder = new CameraRecorderBuilder(this, sampleGLView)
                .cameraRecordListener(new CameraRecordListener() {
                    @Override
                    public void onGetFlashSupport(boolean flashSupport) {

                    }

                    @Override
                    public void onRecordComplete() {
                        mIsRecordingVideo = false;
                        mProgress.add(mPbRecordingBar.progressInMillies());
                        mNextVideoAbsolutePath = null;
                    }

                    @Override
                    public void onRecordStart() {
                        if (getMicrophoneAvailable()) {
                            isRecordingUsingHeadphone = 1;
                        }
                        if (!getMicrophoneAvailable()) {
                            AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
                            currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, (int) (audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) * 0.75), AudioManager.FLAG_PLAY_SOUND);
                        }
                    }

                    @Override
                    public void onError(Exception exception) {
                        Log.e("CameraRecorder", exception.toString());
                    }

                    @Override
                    public void onCameraThreadFinish() {
                        runOnUiThread(() -> {
                            initializeCamera();
                        });
                    }
                })
                .videoSize(480, 640)
                .cameraSize(720, 1280)
                .lensFacing(lensFacing)
                .build();
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_SUGGESTION_CLOSE_CLICKED:
                discardRecording();
                break;
            case AppConstants.ClassConstants.ON_SUGGESTION_SAVE_RECORDING:
                pauseExoplayer();
                releaseExoplayer();
                releaseCamera();
                mPbRecordingBar.reset();
                Intent intent = new Intent(this, JoinSelfCollabActivity.class);
                if (!mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size()-1).getFrameNumber().isEmpty())
                    mSelfCollabVideoDataBeanArrayList.add(new SelfCollabVideoDataBean());
                intent.putExtra(AppConstants.ClassConstants.VIDEO_DATA, mSelfCollabVideoDataBeanArrayList);
                intent.putExtra(AppConstants.ClassConstants.MUSIC_ID, mMusicId);
                intent.putExtra(AppConstants.ClassConstants.TEMPLETE_SIZE, templeteSize);
                intent.putExtra(AppConstants.ClassConstants.IS_RECORDED_WITH_HEADPHONES, isRecordingUsingHeadphone);
                intent.putExtra(AppConstants.ClassConstants.VIDEO_FILE_PATH, mVideoStack);
                intent.putExtra(AppConstants.ClassConstants.FRAMES, totalNoOfFrames);
                intent.putExtra(AppConstants.ClassConstants.COLOR_BAR_DATA, mCollabBarMap);
                startActivity(intent);
                mVideoStack.clear();
                mProgress.clear();
                break;
            case AppConstants.ClassConstants.ON_SUGGESTION_TOGGLE_FLASH:
                toggleFlash();
                break;
            case AppConstants.ClassConstants.ON_SUGGESTION_CAMERA_FLIP:
                switchCamera();
                break;
            case AppConstants.ClassConstants.ON_SUGGESTION_TIMER_CLICKED:
                new TimerDialog(this).show(getSupportFragmentManager(), TimerDialog.class.getCanonicalName());
                break;
            case AppConstants.ClassConstants.ON_SUGGESTION_GALLERY_CLICKED:
                chooseVideoFromGallery();
                break;
            case AppConstants.ClassConstants.ON_SUGGESTION_REMOVE_SEGMENT:
                showRemoveSegmentDialog();
                break;
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (currentVolume != -1) {
            AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume, AudioManager.FLAG_PLAY_SOUND);
        }
    }

}
