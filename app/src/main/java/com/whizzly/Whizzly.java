package com.whizzly;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.downloader.PRDownloader;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.google.GoogleEmojiProvider;
import com.whizzly.utils.DataManager;

import java.util.Locale;

import io.fabric.sdk.android.Fabric;


public class Whizzly extends Application {
    private static Whizzly mApplicationInstance = new Whizzly();

    public static Context getInstance() {
        if (mApplicationInstance == null) {
            mApplicationInstance = new Whizzly();
        }

        return mApplicationInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        DataManager dataManager = DataManager.init(this);
        dataManager.initApiManager();
        Fabric.with(this, new Crashlytics());
        PRDownloader.initialize(getApplicationContext());
        EmojiManager.install(new GoogleEmojiProvider());
        String token = FirebaseInstanceId.getInstance().getToken();
        DataManager.getInstance().saveDeviceToken(token);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        FirebaseDatabase.getInstance().getReference().keepSynced(true);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }
}

