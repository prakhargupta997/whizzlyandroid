package com.whizzly.notification;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.fragment.SuggestedVideoFragment;
import com.whizzly.databinding.FragmentFeedsBinding;
import com.whizzly.home_feed_comments.CameraCommentActivity;
import com.whizzly.home_feed_comments.FeedsCommentPreviewActivity;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.home_screen_module.home_screen_fragments.home_feeds.BottomSheetPeopleLikeListFragment;
import com.whizzly.home_screen_module.home_screen_fragments.home_feeds.BottomSheetPeopleReuseFragment;
import com.whizzly.home_screen_module.home_screen_fragments.home_feeds.BottomSheetShareFragment;
import com.whizzly.home_screen_module.home_screen_fragments.home_feeds.CommentListAdapter;
import com.whizzly.home_screen_module.home_screen_fragments.home_feeds.VideoPreviewActivity;
import com.whizzly.home_screen_module.home_screen_fragments.profile.ProfileFragment;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.interfaces.OnSendCommentDataListener;
import com.whizzly.models.feeds_response.Result;
import com.whizzly.models.privacy_status.PrivacyStatusResponseBean;
import com.whizzly.models.reuse_video.ReuseVideoBean;
import com.whizzly.models.video_details.VideoDetailsResponse;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;
import com.whizzly.utils.EndlessRecyclerOnScrollListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import static com.google.android.exoplayer2.ui.AspectRatioFrameLayout.RESIZE_MODE_FIT;
import static com.google.android.exoplayer2.ui.AspectRatioFrameLayout.RESIZE_MODE_ZOOM;

public class FeedsFragment extends Fragment implements OnClickSendListener, OnSendCommentDataListener {
    private FragmentFeedsBinding mFragmentFeedsBinding;
    private FeedsViewModel mNotificationViewModel;
    private FeedsModel mNotificationActivityModel;
    private Result result;
    private ExoPlayer mExoPlayer;
    private int commentPage = 1;
    private CommentListAdapter commentListAdapter;
    private ArrayList<com.whizzly.models.comment_list.Result> commentResultsList;
    private Integer mVideoId;
    private String mVideoType;
    private int mIsFrom;
    private int mUserId;
    private com.whizzly.models.video_list_response.Result mResult;
    private Observer<PrivacyStatusResponseBean> mSavedVideoObserver;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentFeedsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_feeds, null, false);
        mNotificationViewModel = ViewModelProviders.of(this).get(FeedsViewModel.class);
        mFragmentFeedsBinding.setViewModel(mNotificationViewModel);
        mNotificationActivityModel = new FeedsModel();
        mFragmentFeedsBinding.setModel(mNotificationActivityModel);
        mNotificationViewModel.setOnClickSendListener(this);
        setDoubleTapListener();
        mNotificationViewModel.setGenericListeners(((BaseActivity) getActivity()).getFailureResponseObserver(), ((BaseActivity) getActivity()).getErrorObserver());
        observeSavedVideoLiveData();
        mNotificationViewModel.getmVideoDetailsRichMediatorLiveData().observe(this, new Observer<VideoDetailsResponse>() {
            @Override
            public void onChanged(@Nullable VideoDetailsResponse videoDetailsResponse) {
                if (videoDetailsResponse != null) {
                    if (videoDetailsResponse.getCode() == 200) {
                        setDataToViews(videoDetailsResponse.getResult());
                        result = videoDetailsResponse.getResult();
                        setExoplayer(result.getUrl());

                        if (mIsFrom == AppConstants.ClassConstants.FROM_HOME_COLLAB)
                            hitReuseApi();

                        if (commentPage > 0)
                            mNotificationViewModel.hitCommentListingApi(videoCommentHashMap());
                    } else if (videoDetailsResponse.getCode() == 302 || videoDetailsResponse.getCode() == 301) {
                        Toast.makeText(getActivity(), videoDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        ((BaseActivity) getActivity()).autoLogOut();
                    }
                }
            }
        });
        mNotificationViewModel.getmReuseVideoBeanRichMediatorLiveData().observe(this, new Observer<ReuseVideoBean>() {
            @Override
            public void onChanged(@Nullable ReuseVideoBean reuseVideoBean) {
                if (reuseVideoBean != null) {
                    if (reuseVideoBean.getCode() == 200) {
                        mResult = reuseVideoBean.getResult();
                    } else if (reuseVideoBean.getCode() == 301 || reuseVideoBean.getCode() == 302) {
                        Toast.makeText(getActivity(), reuseVideoBean.getMessage(), Toast.LENGTH_SHORT).show();
                        ((BaseActivity) Objects.requireNonNull(getActivity())).autoLogOut();
                    }
                }
            }
        });

        mNotificationViewModel.getLikeDislikeResponseRichMediatorLiveData().observe(this, likeDislikeResponse -> {
            if (likeDislikeResponse != null) {
                if (likeDislikeResponse.getCode() == 301 || likeDislikeResponse.getCode() == 302) {
                    Toast.makeText(getActivity(), likeDislikeResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) getActivity()).autoLogOut();
                } else if (likeDislikeResponse.getCode() == 200) {
                    if (result.getIsLiked() == 0) {
                        mFragmentFeedsBinding.lavClap.setVisibility(View.VISIBLE);
                        mFragmentFeedsBinding.lavClap.playAnimation();
                        mFragmentFeedsBinding.ivLikes.setImageDrawable(getActivity().getDrawable(R.drawable.ic_clap_filled));
                        result.setIsLiked(1);
                        mFragmentFeedsBinding.tvLikes.setText(String.valueOf(result.getTotalLikes() + 1));
                        result.setTotalLikes(result.getTotalLikes() + 1);
                    } else {
                        mFragmentFeedsBinding.ivLikes.setImageDrawable(getActivity().getDrawable(R.drawable.ic_clap));
                        mFragmentFeedsBinding.tvLikes.setText(String.valueOf(result.getTotalLikes() - 1));
                        result.setTotalLikes(result.getTotalLikes() - 1);
                        result.setIsLiked(0);
                    }
                } else
                    Toast.makeText(getActivity(), likeDislikeResponse.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

        mNotificationViewModel.getCommentListingBeanRichMediatorLiveData().observe(this, commentListingBean -> {
            if (commentListingBean != null) {
                if (commentListingBean.getCode() == 200) {
                    commentResultsList.clear();
                    for (int i = 0; i < commentListingBean.getResult().size(); i++) {
                        if (commentListingBean.getResult().get(i).getUserStatus() == 0)
                            commentResultsList.add(commentListingBean.getResult().get(i));
                    }
                    commentListAdapter.notifyDataSetChanged();
                    commentPage = commentListingBean.getNextCount();

                } else if (commentListingBean.getCode() == 302 || commentListingBean.getCode() == 301) {
                    Toast.makeText(getActivity(), commentListingBean.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) getActivity()).autoLogOut();
                }
            }

        });
        mFragmentFeedsBinding.ivBack.setOnClickListener(v -> {
            releasePlayer();
            if (mIsFrom == AppConstants.ClassConstants.FROM_SPLASH) {
                Intent homeIntent = new Intent(getActivity(), HomeActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                getActivity().finish();
            } else {
                new Handler().post(() -> getActivity().onBackPressed());
            }

        });

        mFragmentFeedsBinding.lavClap.addAnimatorListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mFragmentFeedsBinding.lavClap.setVisibility(View.GONE);
            }
        });
        commentResultsList = new ArrayList<>();
        getArgumentsData();
        setViews();
        hitVideoDetailsApi();
        return mFragmentFeedsBinding.getRoot();
    }

    private void setDoubleTapListener() {
        mFragmentFeedsBinding.getRoot().setOnTouchListener(new View.OnTouchListener() {
            private GestureDetector gestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    if (result.getIsLiked() == 0) {
                        likeDislikeVideo(1);
                    }
                    return super.onDoubleTap(e);
                }
            });

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("TEST", "Raw event: " + event.getAction() + ", (" + event.getRawX() + ", " + event.getRawY() + ")");
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });
    }

    private void setViews() {
        if (mIsFrom == AppConstants.ClassConstants.FROM_HOME_COLLAB || mIsFrom == AppConstants.ClassConstants.FROM_CATEGORY_VIDEOS) {
            mFragmentFeedsBinding.ivMusicIcon.setVisibility(View.GONE);
            mFragmentFeedsBinding.ivAddComment.setVisibility(View.GONE);
            mFragmentFeedsBinding.ivFeedOptions.setVisibility(View.INVISIBLE);
        } else {
            mFragmentFeedsBinding.ivMusicIcon.setVisibility(View.VISIBLE);
            mFragmentFeedsBinding.ivAddComment.setVisibility(View.VISIBLE);
            mFragmentFeedsBinding.ivFeedOptions.setVisibility(View.VISIBLE);
        }
    }

    private void getArgumentsData() {
        if (getArguments() != null && getArguments().containsKey(AppConstants.ClassConstants.IS_FROM)) {
            mIsFrom = getArguments().getInt(AppConstants.ClassConstants.IS_FROM);
            mVideoType = getArguments().getString(AppConstants.ClassConstants.VIDEO_TYPE);
            mVideoId = getArguments().getInt(AppConstants.ClassConstants.FEED_VIDEO_ID);
            mUserId = getArguments().getInt(AppConstants.ClassConstants.USER_ID, 0);
        }
    }

    private void hitVideoDetailsApi() {
        mFragmentFeedsBinding.flProgress.setVisibility(View.VISIBLE);
        HashMap<String, String> videoQuery = new HashMap<>();
        videoQuery.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        videoQuery.put(AppConstants.NetworkConstants.VIDEO_ID, String.valueOf(mVideoId));
        videoQuery.put(AppConstants.NetworkConstants.VIDEO_TYPE, mVideoType);
        mNotificationViewModel.hitVideoDetailsApi(videoQuery);
    }

    private void hitReuseApi() {
        HashMap<String, String> reuseQuery = new HashMap<>();
        reuseQuery.put(AppConstants.NetworkConstants.VIDEO_TYPE, "collab");
        reuseQuery.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        reuseQuery.put(AppConstants.NetworkConstants.VIDEO_ID, String.valueOf(result.getId()));
        mNotificationViewModel.hitReuseApi(reuseQuery);
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_VIDEO_COMMENT_CLICKED:
                releasePlayer();
                Intent commentIntent = new Intent(getActivity(), CameraCommentActivity.class);
                commentIntent.putExtra(AppConstants.ClassConstants.FEEDS_DATA, result);
                startActivity(commentIntent);
                break;
            case AppConstants.ClassConstants.ON_FEED_OPTION_CLICKED:
                if (result != null) {
                    releasePlayer();
                    BottomSheetShareFragment sheetShareFragment = new BottomSheetShareFragment();
                    Bundle shareSheetBundle = new Bundle();
                    shareSheetBundle.putString(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.FROM_PROFILE);
                    shareSheetBundle.putParcelable(AppConstants.ClassConstants.VIDEO_DETAILS, result);
                    sheetShareFragment.setArguments(shareSheetBundle);
                    sheetShareFragment.show(getChildFragmentManager(), BottomSheetShareFragment.class.getCanonicalName());
                }
                break;
            case AppConstants.ClassConstants.ON_VIDEO_REUSE_COUNT_CLICKED:
                if (result.getTotalReuse() > 0) {
                    BottomSheetPeopleReuseFragment bottomSheetPeopleReuseFragment = new BottomSheetPeopleReuseFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.ClassConstants.VIDEO_TYPE, result.getType());
                    bundle.putString(AppConstants.ClassConstants.FEED_VIDEO_ID, String.valueOf(result.getId()));
                    bundle.putString(AppConstants.ClassConstants.TOTAL_REUSE, String.valueOf(result.getTotalReuse()));
                    bottomSheetPeopleReuseFragment.setArguments(bundle);
                    bottomSheetPeopleReuseFragment.show(getChildFragmentManager(), BottomSheetPeopleReuseFragment.class.getCanonicalName());
                }
                break;
            case AppConstants.ClassConstants.ON_VIDEO_REUSE_CLICKED:
                if (AppUtils.isNetworkAvailable(getActivity())) {
                    Intent intent = new Intent(getActivity(), VideoPreviewActivity.class);
                    intent.putExtra(AppConstants.ClassConstants.FRAGMENT_IS_FROM, mIsFrom);
                    intent.putExtra(AppConstants.ClassConstants.FEEDS_DATA, result);
                    startActivity(intent);
                } else {
                    Toast.makeText(getActivity(), getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
                }
                break;
            case AppConstants.ClassConstants.ON_VIDEO_LIKE_COUNT_CLICKED:
                if (result.getTotalLikes() > 0) {
                    BottomSheetPeopleLikeListFragment bottomSheetPeopleLikeListFragment = new BottomSheetPeopleLikeListFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.ClassConstants.VIDEO_TYPE, result.getType());
                    bundle.putString(AppConstants.ClassConstants.FEED_VIDEO_ID, String.valueOf(result.getId()));
                    bundle.putString(AppConstants.ClassConstants.TOTAL_LIKES, String.valueOf(result.getTotalLikes()));
                    bottomSheetPeopleLikeListFragment.setArguments(bundle);
                    bottomSheetPeopleLikeListFragment.show(getChildFragmentManager(), BottomSheetPeopleLikeListFragment.class.getCanonicalName());
                }
                break;
            case AppConstants.ClassConstants.ON_VIDEO_LIKE_CLICKED:
                if (result.getIsLiked() == 0) {
                    likeDislikeVideo(1);
                } else {
                    likeDislikeVideo(0);
                }
                break;
            case AppConstants.ClassConstants.ON_USER_PROFILE_CLICKED:
            case AppConstants.ClassConstants.ON_USER_NAME_VIEW_CLICKED:
                if (!String.valueOf(mUserId).equals(String.valueOf(result.getUserId()))) {
                    releasePlayer();
                    ProfileFragment profileFragment = new ProfileFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_FEED_FRAGMENT);
                    bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.OTHER_USER_PROFILE);
                    bundle.putString(AppConstants.ClassConstants.USER_ID, result.getUserId());
                    profileFragment.setArguments(bundle);
                    Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, profileFragment).addToBackStack(ProfileFragment.class.getCanonicalName()).commit();
                }
                break;
            case AppConstants.ClassConstants.ON_MUSIC_ICON_CLICKED:
                releasePlayer();
                SuggestedVideoFragment suggestedVideoFragment = new SuggestedVideoFragment();
                Bundle musicBundle = new Bundle();
                musicBundle.putString(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_FEED);
                musicBundle.putInt(AppConstants.ClassConstants.MUSIC_ID, (result.getMusicId()));
                musicBundle.putString(AppConstants.ClassConstants.MUSIC_COLLAB_COUNT, String.valueOf((result.getMusicReuseCount())));
                musicBundle.putString(AppConstants.ClassConstants.MUSIC_NAME, (result.getMusicTitle()));
                musicBundle.putString(AppConstants.ClassConstants.MUSIC_COMPOSER, (result.getMusicDescription()));
                musicBundle.putString(AppConstants.ClassConstants.MUSIC_THUMBNAIL, (result.getMusicThumbnail()));
                musicBundle.putString(AppConstants.ClassConstants.MUSIC_LINK, (result.getMusicUrl()));
                suggestedVideoFragment.setArguments(musicBundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, suggestedVideoFragment).commit();
                break;


        }
    }


    private HashMap<String, String> videoCommentHashMap() {
        HashMap<String, String> videoCommentQuery = new HashMap<>();
        videoCommentQuery.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        videoCommentQuery.put(AppConstants.NetworkConstants.VIDEO_ID, String.valueOf(result.getId()));
        videoCommentQuery.put(AppConstants.NetworkConstants.VIDEO_TYPE, result.getType());
        videoCommentQuery.put(AppConstants.NetworkConstants.PAGE, String.valueOf(commentPage));
        return videoCommentQuery;
    }

    private void setCommentsRecyclerView(String type) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mFragmentFeedsBinding.rvCommentsList.setLayoutManager(linearLayoutManager);
        commentListAdapter = new CommentListAdapter(getActivity(), mFragmentFeedsBinding.ivAddComment.getHeight(), commentResultsList, this, type);
        mFragmentFeedsBinding.rvCommentsList.setAdapter(commentListAdapter);
        mFragmentFeedsBinding.rvCommentsList.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore() {
                if (commentPage > 1)
                    mNotificationViewModel.hitCommentListingApi(videoCommentHashMap());
            }
        });
    }


    private void setExoplayer(String url) {
        if (mExoPlayer == null) {
            TrackSelection.Factory adaptiveTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(new DefaultBandwidthMeter());

            mExoPlayer = ExoPlayerFactory.newSimpleInstance(getActivity(),
                    new DefaultRenderersFactory(getActivity()),
                    new DefaultTrackSelector(adaptiveTrackSelectionFactory),
                    new DefaultLoadControl());

            mExoPlayer.setPlayWhenReady(true);
            mExoPlayer.setRepeatMode(Player.REPEAT_MODE_ONE);
            MediaSource mediaSource = buildMediaSource(Uri.parse(url));
            mExoPlayer.addListener(new Player.DefaultEventListener() {
                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    super.onPlayerStateChanged(playWhenReady, playbackState);
                    switch (playbackState) {
                        case ExoPlayer.STATE_BUFFERING:
                            mFragmentFeedsBinding.flProgress.setVisibility(View.VISIBLE);
                            break;
                        case ExoPlayer.STATE_READY:
//                            mFragmentFeedsBinding.ivVideoThumbnail.setVisibility(View.GONE);
                            mFragmentFeedsBinding.flProgress.setVisibility(View.GONE);
                            break;
                    }
                }
            });
            mExoPlayer.prepare(mediaSource, true, false);
            mFragmentFeedsBinding.pvVideo.setPlayer(mExoPlayer);
        }
    }

    private MediaSource buildMediaSource(Uri uri) {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getActivity(),
                Util.getUserAgent(getActivity(), getString(R.string.app_name)), bandwidthMeter);
        return new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri);

    }


    @SuppressLint("CheckResult")
    private void setDataToViews(com.whizzly.models.feeds_response.Result mFeedsData) {

        if (mIsFrom != AppConstants.ClassConstants.FROM_HOME_COLLAB && mIsFrom != AppConstants.ClassConstants.FROM_CATEGORY_VIDEOS) {
            mFragmentFeedsBinding.clBottomLayout.setVisibility(View.VISIBLE);
            mFragmentFeedsBinding.tvUserName.setText(String.format("@%s", mFeedsData.getUserName()));
            mFragmentFeedsBinding.tvLikes.setText(String.valueOf(mFeedsData.getTotalLikes()));
            mFragmentFeedsBinding.tvReuseCount.setText(String.valueOf(mFeedsData.getTotalReuse()));
//            mFragmentFeedsBinding.tvVideoDescription.setText(mFeedsData.getVideoDescription());
            String hashtag = mFeedsData.getVideoHastags();
            String[] hashtags = hashtag.split(",");
            StringBuilder hashtagBuillder = new StringBuilder();
            hashtagBuillder.append(mFeedsData.getVideoDescription() + " ");
            if (!TextUtils.isEmpty(hashtag))
                for (int i = 0; i < hashtags.length; i++) {
                    if (i == 0) {
                        hashtagBuillder.append("#" + hashtags[i]);
                    } else {
                        hashtagBuillder.append(" #" + hashtags[i]);
                    }
                }


            int userCount = 0;
            if (mFeedsData.getCount() > 1) {
                hashtagBuillder.append(" with ");
                if (!TextUtils.isEmpty(mFeedsData.getUsersName())) {
                    String userName = mFeedsData.getUsersName();
                    String[] userNames = userName.split(",");
                    /*for (int i = 0; i < userNames.length; i++) {
                        if (Arrays.asList(userNames).contains(mFeedsData.getUserName())) {
                            userCount++;
                        }
                    }

                    if (userCount > 0) {
                        for (int i = 0; i < userNames.length; i++) {
                            if (mFeedsData.getUserName().equalsIgnoreCase(userNames[i])) {
                                userNames = removeTheElement(userNames, i);
                                break;
                            }
                        }
                    }*/

                    for (int i = 0; i < userNames.length; i++) {
                        if (i == 0) {
                            hashtagBuillder.append("@" + userNames[i]);
                        } else {
                            hashtagBuillder.append(" @" + userNames[i]);
                        }
                    }

                    mFragmentFeedsBinding.tvCollabWith.setVisibility(View.GONE);

                } else {
                    mFragmentFeedsBinding.tvCollabWith.setVisibility(View.GONE);
                }
            }

            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(hashtagBuillder);
            if (!TextUtils.isEmpty(mFeedsData.getVideoHastags()) && !TextUtils.isEmpty(mFeedsData.getVideoDescription())) {
                if (mFeedsData.getCount() > 1) {
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), 0, mFeedsData.getVideoDescription().length(), 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), mFeedsData.getVideoDescription().length(), mFeedsData.getVideoDescription().length() + mFeedsData.getVideoHastags().length() + hashtags.length + 1, 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), mFeedsData.getVideoDescription().length() + mFeedsData.getVideoHastags().length() + hashtags.length + 1, mFeedsData.getVideoDescription().length() + mFeedsData.getVideoHastags().length() + hashtags.length + 1 + " with ".length(), 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), mFeedsData.getVideoDescription().length() + mFeedsData.getVideoHastags().length() + hashtags.length + " with ".length(), hashtagBuillder.length(), 0);
                } else {
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), 0, mFeedsData.getVideoDescription().length(), 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), mFeedsData.getVideoDescription().length(), mFeedsData.getVideoDescription().length() + mFeedsData.getVideoHastags().length() + hashtags.length + 1, 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), mFeedsData.getVideoDescription().length() + mFeedsData.getVideoHastags().length() + hashtags.length + 1, mFeedsData.getVideoDescription().length() + mFeedsData.getVideoHastags().length() + hashtags.length + 1, 0);
                    spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), mFeedsData.getVideoDescription().length() + mFeedsData.getVideoHastags().length() + hashtags.length, hashtagBuillder.length(), 0);
                }
            } else {
                if (!TextUtils.isEmpty(mFeedsData.getVideoHastags()) && TextUtils.isEmpty(mFeedsData.getVideoDescription())) {
                    if (mFeedsData.getCount() > 1) {
                        spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), 0, mFeedsData.getVideoHastags().length() + hashtags.length + 1, 0);
                        spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), mFeedsData.getVideoHastags().length() + hashtags.length + 1, mFeedsData.getVideoHastags().length() + hashtags.length + 1 + " with ".length(), 0);
                        spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), mFeedsData.getVideoHastags().length() + hashtags.length + " with ".length(), hashtagBuillder.length(), 0);
                    } else {
                        spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), 0, mFeedsData.getVideoHastags().length() + hashtags.length + 1, 0);
                        spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), mFeedsData.getVideoHastags().length() + hashtags.length + 1, mFeedsData.getVideoHastags().length() + hashtags.length + 1, 0);
                        spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), mFeedsData.getVideoHastags().length() + hashtags.length, hashtagBuillder.length(), 0);
                    }
                }

                if (TextUtils.isEmpty(mFeedsData.getVideoHastags()) && !TextUtils.isEmpty(mFeedsData.getVideoDescription())) {
                    if (mFeedsData.getCount() > 1) {
                        spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), 0, mFeedsData.getVideoDescription().length(), 0);
                        spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), mFeedsData.getVideoDescription().length(), mFeedsData.getVideoDescription().length() + 1, 0);
                        spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), mFeedsData.getVideoDescription().length() + 1, mFeedsData.getVideoDescription().length() + 1 + " with ".length(), 0);
                        spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), mFeedsData.getVideoDescription().length() + " with ".length(), hashtagBuillder.length(), 0);
                    } else {
                        spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), 0, mFeedsData.getVideoDescription().length(), 0);
                        spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), mFeedsData.getVideoDescription().length(), mFeedsData.getVideoDescription().length() + 1, 0);
                        spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), mFeedsData.getVideoDescription().length() + 1, mFeedsData.getVideoDescription().length() + 1, 0);
                        spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), mFeedsData.getVideoDescription().length(), hashtagBuillder.length(), 0);
                    }
                }

                if (TextUtils.isEmpty(mFeedsData.getVideoHastags()) && TextUtils.isEmpty(mFeedsData.getVideoDescription())) {
                    if (mFeedsData.getCount() > 1) {
                        spannableStringBuilder.setSpan(new StyleSpan(Typeface.NORMAL), 0, " with ".length(), 0);
                        spannableStringBuilder.setSpan(new StyleSpan(Typeface.BOLD), " with ".length(), hashtagBuillder.length(), 0);
                    }
                }
            }


            if (mFeedsData.getVideoHastags() != null)
                mFragmentFeedsBinding.tvHashtags.setText(spannableStringBuilder);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.error(R.drawable.ic_feed_user_pic_placeholder);
            requestOptions.placeholder(R.drawable.ic_feed_user_pic_placeholder);
            requestOptions.circleCrop();
            Glide.with(this).load(mFeedsData.getProfileImage()).apply(requestOptions).into(mFragmentFeedsBinding.ivUserProfileIcon);
            if (mFeedsData.getIsLiked() == 0) {
                mNotificationActivityModel.setVideoFeedLiked(false);
            } else {
                mNotificationActivityModel.setVideoFeedLiked(true);
            }
            if (null == mFeedsData.getMusicId()) {
                mFragmentFeedsBinding.ivMusicIcon.setVisibility(View.GONE);
                mFragmentFeedsBinding.tvMusicName.setVisibility(View.GONE);
            } else {
                mFragmentFeedsBinding.ivMusicIcon.setVisibility(View.VISIBLE);
                mFragmentFeedsBinding.tvMusicName.setVisibility(View.VISIBLE);
                mFragmentFeedsBinding.tvMusicName.setText(String.format("%s - %s", mFeedsData.getMusicTitle(), mFeedsData.getMusicDescription()));
                mFragmentFeedsBinding.ivMusicIcon.startAnimation(rotateAnimation());
            }
            setCommentsRecyclerView(mFeedsData.getType());
//            mFragmentFeedsBinding.ivVideoThumbnail.setVisibility(View.VISIBLE);
//            Glide.with(this).load(mFeedsData.getThumbnailUrl()).into(mFragmentFeedsBinding.ivVideoThumbnail);

            if (mFeedsData.getType().equals("video")) {
                mFragmentFeedsBinding.ivReuseCount.setVisibility(View.GONE);
                mFragmentFeedsBinding.tvReuseCount.setVisibility(View.GONE);
            } else {
                mFragmentFeedsBinding.ivReuseCount.setVisibility(View.VISIBLE);
                mFragmentFeedsBinding.tvReuseCount.setVisibility(View.VISIBLE);
            }

            mFragmentFeedsBinding.tvUserFollowerCount.setText(String.valueOf(mFeedsData.getFollowerCount()));

            if (mFeedsData.getCount() == 2) {
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
//                mFragmentFeedsBinding.ivVideoThumbnail.setLayoutParams(layoutParams);
//                mFragmentFeedsBinding.ivVideoThumbnail.setScaleType(ImageView.ScaleType.FIT_START);
                mFragmentFeedsBinding.pvVideo.setResizeMode(RESIZE_MODE_FIT);
            } else {
//                mFragmentFeedsBinding.ivVideoThumbnail.setScaleType(ImageView.ScaleType.FIT_XY);
                mFragmentFeedsBinding.pvVideo.setResizeMode(RESIZE_MODE_ZOOM);
            }
            mFragmentFeedsBinding.ivFeedOptions.setVisibility(View.VISIBLE);
            mFragmentFeedsBinding.btnCollab.setVisibility(View.GONE);

        } else {
            mFragmentFeedsBinding.btnCollab.setVisibility(View.GONE);
            mFragmentFeedsBinding.ivFeedOptions.setVisibility(View.INVISIBLE);
            mFragmentFeedsBinding.clBottomLayout.setVisibility(View.INVISIBLE);
            mFragmentFeedsBinding.ivAddComment.setVisibility(View.INVISIBLE);
            mFragmentFeedsBinding.ivMusicIcon.setVisibility(View.INVISIBLE);
//            mFragmentFeedsBinding.ivVideoThumbnail.setVisibility(View.INVISIBLE);
            mFragmentFeedsBinding.ivReuseCount.setVisibility(View.INVISIBLE);
            mFragmentFeedsBinding.tvReuseCount.setVisibility(View.INVISIBLE);
            mFragmentFeedsBinding.ivLikes.setVisibility(View.INVISIBLE);
            mFragmentFeedsBinding.tvLikes.setVisibility(View.INVISIBLE);
            mFragmentFeedsBinding.tvUserFollowerCount.setVisibility(View.INVISIBLE);
            mFragmentFeedsBinding.ivUserProfileIcon.setVisibility(View.INVISIBLE);
            mFragmentFeedsBinding.rvCommentsList.setVisibility(View.INVISIBLE);

        }
        /*if(accountType == AppConstants.MY_PROFILE){
        }else if(accountType == AppConstants.OTHER_USER_PROFILE){
            mActivityFeedVideoPreviewBinding.ivFeedOptions.setVisibility(View.INVISIBLE);
        }*/

    }


    private RotateAnimation rotateAnimation() {
        RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(3000);
        rotate.setRepeatCount(Animation.INFINITE);
        rotate.setInterpolator(new LinearInterpolator());
        return rotate;
    }

    @Override
    public void onCommentClicked(com.whizzly.models.comment_list.Result result, int position) {
        releasePlayer();
        Intent intent = new Intent(getActivity(), FeedsCommentPreviewActivity.class);
        intent.putExtra(AppConstants.ClassConstants.ON_VIDEO_FEED_COMMENT_CLICKED, commentResultsList);
        intent.putExtra(AppConstants.ClassConstants.COMMENT_POSITION, position);
        startActivity(intent);
    }

    private void releasePlayer() {
        if (mExoPlayer != null) {
            mExoPlayer.setPlayWhenReady(false);
            mExoPlayer.stop();
            mExoPlayer.release();
            mExoPlayer = null;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }

    }

    private void likeDislikeVideo(int status) {
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put(AppConstants.NetworkConstants.LIKE_DISLIKE_STATUS, String.valueOf(status));
        queryMap.put(AppConstants.NetworkConstants.VIDEO_TYPE, result.getType());
        queryMap.put(AppConstants.NetworkConstants.VIDEO_ID, String.valueOf(result.getId()));
        queryMap.put(AppConstants.NetworkConstants.USER_ID, String.valueOf(DataManager.getInstance().getUserId()));
        mNotificationViewModel.hitLikeDislikeApi(queryMap);
    }

    public Observer<PrivacyStatusResponseBean> geSavedFeedsObserver() {
        return mSavedVideoObserver;
    }

    private void observeSavedVideoLiveData() {
        mSavedVideoObserver = privacyStatusResponseBean -> {
            if (privacyStatusResponseBean != null) {
                if (privacyStatusResponseBean.getCode() == 200) {
                    if (privacyStatusResponseBean.getResult().getIsLocked() != null)
                        result.setSavedVideoPrivacy(privacyStatusResponseBean.getResult().getIsLocked());
                    Toast.makeText(getActivity(), privacyStatusResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (privacyStatusResponseBean.getCode() == 301 || privacyStatusResponseBean.getCode() == 302) {
                    Toast.makeText(getActivity(), privacyStatusResponseBean.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) getActivity()).autoLogOut();
                } else
                    Toast.makeText(getActivity(), getActivity().getString(R.string.something_wrong), Toast.LENGTH_SHORT).show();

            }

        };
    }
}
