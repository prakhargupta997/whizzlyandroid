package com.whizzly.notification;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.ActivityNotificationBinding;
import com.whizzly.home_screen_module.home_screen_fragments.profile.ProfileFragment;
import com.whizzly.interfaces.OnOpenUserProfile;
import com.whizzly.interfaces.PermissionListener;
import com.whizzly.models.collab.collab_videos.CategoryVideo;
import com.whizzly.models.notifications.PushNotificationResponse;
import com.whizzly.models.notifications.Result;
import com.whizzly.models.reuse_videos_chart.VideoInfo;
import com.whizzly.models.search.videos.VideoList;
import com.whizzly.utils.AppConstants;

public class NotificationActivity extends BaseActivity  implements OnOpenUserProfile {
    private ActivityNotificationBinding mNotificationBinding;
    private Bundle mBundle;
    private int isFrom;
    private int userId;
    private PermissionListener mPermissionListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNotificationBinding = DataBindingUtil.setContentView(this, R.layout.activity_notification);
        mBundle = new Bundle();
        getIntentData();
        FeedsFragment feedsFragment = new FeedsFragment();
        mBundle.putInt(AppConstants.ClassConstants.IS_FROM, isFrom);
        mBundle.putString(AppConstants.ClassConstants.VIDEO_TYPE, "collab");
        feedsFragment.setArguments(mBundle);
        addFragment(R.id.fl_container, feedsFragment, FeedsFragment.class.getCanonicalName());
    }

    public void setOnPermissionListener(PermissionListener permissionListener) {
        this.mPermissionListener = permissionListener;
    }

    private void getIntentData() {
        if (getIntent() != null && getIntent().hasExtra(AppConstants.ClassConstants.NOTIFICATION_DATA)) {
            isFrom = getIntent().getIntExtra(AppConstants.ClassConstants.IS_FROM, 0);
            mBundle.putInt(AppConstants.ClassConstants.IS_FROM, isFrom);
            if(getIntent().hasExtra(AppConstants.ClassConstants.USER_ID)){
                userId = getIntent().getIntExtra(AppConstants.ClassConstants.USER_ID, 0);
            }
            switch (isFrom) {
                case AppConstants.ClassConstants.FROM_SPLASH:
                    PushNotificationResponse pushNotificationResponse = getIntent().getParcelableExtra(AppConstants.ClassConstants.NOTIFICATION_DATA);
                    mBundle.putInt(AppConstants.ClassConstants.FEED_VIDEO_ID, pushNotificationResponse.getVideoId());
                    mBundle.putString(AppConstants.ClassConstants.VIDEO_TYPE, pushNotificationResponse.getVideoType());
                    break;
                case AppConstants.ClassConstants.FROM_NOTIFICATION:
                    com.whizzly.models.notifications.Result result = getIntent().getParcelableExtra(AppConstants.ClassConstants.NOTIFICATION_DATA);
                    mBundle.putInt(AppConstants.ClassConstants.FEED_VIDEO_ID, getVideoId(result));
                    mBundle.putString(AppConstants.ClassConstants.VIDEO_TYPE, result.getVideoType());
                    break;
                case AppConstants.ClassConstants.FROM_CATEGORY_VIDEOS:
                    com.whizzly.models.collab.collab_videos.allvideos.Result videoResult = getIntent().getParcelableExtra(AppConstants.ClassConstants.NOTIFICATION_DATA);
                    mBundle.putInt(AppConstants.ClassConstants.FEED_VIDEO_ID, videoResult.getId());
                    mBundle.putString(AppConstants.ClassConstants.VIDEO_TYPE, "video");
                    break;
                case AppConstants.ClassConstants.FROM_HASHTAG_VIDEOS:
                    VideoList videoList = (VideoList) getIntent().getSerializableExtra(AppConstants.ClassConstants.NOTIFICATION_DATA);
                    mBundle.putInt(AppConstants.ClassConstants.FEED_VIDEO_ID, videoList.getVideoId());
                    mBundle.putString(AppConstants.ClassConstants.VIDEO_TYPE, videoList.getVideoType());

                    break;
                case AppConstants.ClassConstants.FROM_HOME_COLLAB:
                    CategoryVideo videoDatum = getIntent().getParcelableExtra(AppConstants.ClassConstants.NOTIFICATION_DATA);
                    mBundle.putInt(AppConstants.ClassConstants.FEED_VIDEO_ID, videoDatum.getId());
                    mBundle.putString(AppConstants.ClassConstants.VIDEO_TYPE, "video");
                    break;
                case AppConstants.ClassConstants.FROM_CHARTS:
                    VideoInfo videoInfo = getIntent().getParcelableExtra(AppConstants.ClassConstants.NOTIFICATION_DATA);
                    mBundle.putInt(AppConstants.ClassConstants.FEED_VIDEO_ID, videoInfo.getId());
                    mBundle.putString(AppConstants.ClassConstants.VIDEO_TYPE, "collab");
                    break;
                case AppConstants.ClassConstants.FROM_PROFILE_VIDEOS:
                    com.whizzly.models.profileVideo.VideoInfo info = (com.whizzly.models.profileVideo.VideoInfo) getIntent().getSerializableExtra(AppConstants.ClassConstants.NOTIFICATION_DATA);
                    mBundle.putInt(AppConstants.ClassConstants.FEED_VIDEO_ID, info.getId());
                    mBundle.putString(AppConstants.ClassConstants.VIDEO_TYPE, info.getType());
                    mBundle.putInt(AppConstants.ClassConstants.USER_ID, userId);
                    break;
                case AppConstants.ClassConstants.IS_FROM_FEEDS:
                    com.whizzly.models.suggested_videos_info.Result result1 = getIntent().getParcelableExtra(AppConstants.ClassConstants.NOTIFICATION_DATA);
                    mBundle.putInt(AppConstants.ClassConstants.FEED_VIDEO_ID, result1.getId());
                    mBundle.putString(AppConstants.ClassConstants.VIDEO_TYPE, result1.getType());
                    mBundle.putInt(AppConstants.ClassConstants.USER_ID, userId);
                    break;
            }
        }
    }

    private int getVideoId(Result result) {
        int videoId = 0;
        switch (result.getEventType()) {
            case AppConstants.ClassConstants.KEY_COLLAB_LIKE:
                videoId = result.getLikedCollabId();
                break;
            case AppConstants.ClassConstants.KEY_VIDEO_LIKE:
                videoId = result.getLikedVideoId();
                break;
            case AppConstants.ClassConstants.KEY_VIDEO_COMMENT:
                videoId = result.getCommentedVideoId();
                break;
            case AppConstants.ClassConstants.KEY_COLLAB_COMMENT:
                videoId = result.getCommentedCollabId();
                break;
            case AppConstants.ClassConstants.KEY_COLLAB_REUSED:
                videoId = result.getReusedCollabId();
        }
        return videoId;

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onUserProfile(String userId) {
        ProfileFragment profileFragment = new ProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(AppConstants.ClassConstants.ACCOUNT_TYPE, AppConstants.OTHER_USER_PROFILE);
        bundle.putString(AppConstants.ClassConstants.USER_ID, userId);
        bundle.putInt(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.IS_FROM_NOTIFICATION_ACTIVITY);
        profileFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, profileFragment).addToBackStack(ProfileFragment.class.getCanonicalName()).commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mPermissionListener.onPermissionGiven(requestCode, permissions, grantResults);
    }
}
