package com.whizzly.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import androidx.core.app.RemoteInput;
import androidx.core.content.ContextCompat;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.whizzly.R;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.login_signup_module.splash_screen.SplashActivity;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class FirebaseMessaging extends FirebaseMessagingService {
    private int mUniqueId;
    private String mTitle;
    private boolean foreground;
    private NotificationCompat.Builder notificationBuilder;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            foreground = new ForegroundCheckTask().execute(this).get();
            /* if (*//*!foreground&& *//*DataManager.getInstance().getNotificationStatus()!=null&&
                    DataManager.getInstance().getNotificationStatus().equalsIgnoreCase(AppConstants.ClassConstants.NOTIFICATION_ENABLE))
         */
            sendNotification(remoteMessage.getData());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public NotificationManager getNotificationManager() {
        return (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    //This method is only generating push notification
    private void sendNotification(Map<String, String> data) {
        String contentTitle = "", textMessage = "", notificationData = "", videoUrl = "", roomId = "";
        JSONObject jsonObject;
        mUniqueId = 0;
        int notificationType = 0;
        //Generating unique id
        generateUniqueId();
        String id = "channel" + mUniqueId;
        notificationBuilder = new NotificationCompat.Builder(this, id);
        try {
            notificationData = new Gson().toJson(data);
            jsonObject = new JSONObject(notificationData);
            notificationType = jsonObject.getInt(AppConstants.NetworkConstants.NOTIFICATION_TYPE);
            switch (notificationType) {
                case 1:
                    contentTitle = "Message";
                    roomId = jsonObject.optString(AppConstants.NetworkConstants.ROOM_ID);
                    textMessage = jsonObject.optString(AppConstants.NetworkConstants.MESSAGE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                        setReplyLabel(notificationData);
                    break;
                case 2:
                    contentTitle = this.getString(R.string.txt_likes);
                    textMessage = String.format("%s %s", jsonObject.optString(AppConstants.NetworkConstants.USER_NAME), this.getString(R.string.s_liked_your_video));
                    videoUrl = jsonObject.optString(AppConstants.NetworkConstants.ARGUMENT_VIDEO_URL);
                    break;
                case 3:
                    contentTitle = this.getString(R.string.txt_reused);
                    textMessage = String.format("%s %s", jsonObject.optString(AppConstants.NetworkConstants.USER_NAME), this.getString(R.string.s_reused_your_video));
                    videoUrl = jsonObject.optString(jsonObject.optString(AppConstants.NetworkConstants.ARGUMENT_VIDEO_URL));

                    break;
                case 4:
                    contentTitle = this.getString(R.string.txt_follow);
                    textMessage = String.format("%s %s", jsonObject.optString(AppConstants.NetworkConstants.USER_NAME), this.getString(R.string.s_started_following_you));
                    break;
                case 5:
                    contentTitle = this.getString(R.string.comment);
                    textMessage = String.format("%s %s", jsonObject.optString(AppConstants.NetworkConstants.USER_NAME), this.getString(R.string.s_comment_on_your_video));
                    videoUrl = jsonObject.optString(jsonObject.optString(AppConstants.NetworkConstants.ARGUMENT_VIDEO_URL));
                    break;
                case 6:
                    contentTitle = "Collab Reported";
                    textMessage = String.format("Your collab has been reported.");
                    videoUrl = jsonObject.optString(AppConstants.NetworkConstants.ARGUMENT_VIDEO_URL);
                    break;
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(this, SplashActivity.class);
        intent.putExtra(AppConstants.ClassConstants.NOTIFICATION_DATA, notificationData);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, mUniqueId, intent, PendingIntent.FLAG_ONE_SHOT);


        NotificationManager notificationManager = getNotificationManager();
        if (notificationManager != null) {
            //Check for oreo (Making notification channel
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel(id, "Message", importance);
                // mChannel.setDescription(notificationData.toString());
                mChannel.setShowBadge(true);
                mChannel.enableLights(true);
                mChannel.setLightColor(ContextCompat.getColor(this, R.color.colorPrimary));
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

                notificationManager.createNotificationChannel(mChannel);

            }


            //Set Notification for other devices
            notificationBuilder
                    .setSmallIcon(getNotificationIcon())
                    .setLargeIcon(StringToBitMap(videoUrl))
                    .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                    .setContentText(textMessage)
                    .setContentTitle(contentTitle)
                    .setAutoCancel(true).setChannelId(id)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setContentIntent(pendingIntent);

            Notification notification = notificationBuilder.build();

            notification.sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            notification.flags |=
                    Notification.FLAG_AUTO_CANCEL; //Do not clear  the notification
            notification.defaults |= Notification.DEFAULT_LIGHTS; // LED
            notification.defaults |= Notification.DEFAULT_VIBRATE;//Vibration
            if (DataManager.getInstance().getNotificationStatus()!=null && DataManager.getInstance().getNotificationStatus().equalsIgnoreCase(AppConstants.ClassConstants.NOTIFICATION_ENABLE)) {
                switch (notificationType) {
                    case 1:
                        if (foreground){
                            if (DataManager.getInstance().getRoomId() != null && !(roomId.equalsIgnoreCase(DataManager.getInstance().getRoomId())))
                                notificationManager.notify(mUniqueId, notification);
                        }
                         else
                            notificationManager.notify(mUniqueId, notification);

                        break;
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        notificationManager.notify(mUniqueId, notification);
                        break;

                }
            }

        }

    }

    private void setReplyLabel(String notificationData) {
        //reply
        String replyLabel = getResources().getString(R.string.reply_label);
        RemoteInput remoteInput = new RemoteInput.Builder(AppConstants.ClassConstants.KEY_TEXT_REPLY)
                .setLabel(replyLabel)
                .build();

        NotificationCompat.Action.Builder builder = new NotificationCompat.Action.Builder(
                0, replyLabel, getReplyPendingIntent(notificationData));
        builder.addRemoteInput(remoteInput);
        builder.setAllowGeneratedReplies(true);
        NotificationCompat.Action replyAction = builder.build();
        notificationBuilder.addAction(replyAction);


    }

    private PendingIntent getReplyPendingIntent(String notificationData) {
        Intent intent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            intent = new Intent(this, NotificationBroadcastReceiver.class);
            intent.setAction(AppConstants.ClassConstants.REPLY_ACTION);
            intent.putExtra(AppConstants.ClassConstants.KEY_NOTIFICATION_ID, mUniqueId);
            intent.putExtra(AppConstants.ClassConstants.NOTIFICATION_DATA, notificationData);
            return PendingIntent.getBroadcast(getApplicationContext(), 100, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            // start your activity for Android M and below
            intent = new Intent(this, HomeActivity.class);
            intent.setAction(AppConstants.ClassConstants.REPLY_ACTION_TO_CHAT);
            intent.putExtra(AppConstants.ClassConstants.KEY_NOTIFICATION_ID, mUniqueId);
            intent.putExtra(AppConstants.ClassConstants.NOTIFICATION_DATA, notificationData);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            return PendingIntent.getActivity(this, 100, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
        }

    }

    public Bitmap StringToBitMap(String encodedString) {
        Bitmap image = null;
        try {
            URL url = new URL(encodedString);
            image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (IOException e) {
            System.out.println(e);
        }
        return image;
    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    private void generateUniqueId() {
        mUniqueId = (int) System.currentTimeMillis();
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;
    }
}
