package com.whizzly.notification;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;

import com.whizzly.BR;

public class FeedsModel extends BaseObservable {
    public ObservableBoolean videoFeedLiked = new ObservableBoolean(false);

    @Bindable
    public boolean getVideoFeedLiked() {
        return videoFeedLiked.get();
    }

    public void setVideoFeedLiked(boolean videoFeedLiked) {
        this.videoFeedLiked.set(videoFeedLiked);
        notifyPropertyChanged(BR.videoFeedLiked);
    }



}
