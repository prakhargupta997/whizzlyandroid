package com.whizzly.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.RemoteInput;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.whizzly.chat.ChatActivity;
import com.whizzly.chat.model.Message;
import com.whizzly.models.notifications.PushNotificationResponse;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;
import com.whizzly.utils.DataManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class NotificationBroadcastReceiver extends BroadcastReceiver {
    private DatabaseReference mDatabaseReference;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            mDatabaseReference = FirebaseDatabase.getInstance().getReference();
            JSONObject notificationObject = null;
            try {
                notificationObject = new JSONObject(intent.getStringExtra(AppConstants.ClassConstants.NOTIFICATION_DATA));
                PushNotificationResponse notificationResponse = new Gson().fromJson(notificationObject.toString(), PushNotificationResponse.class);

                if (AppConstants.ClassConstants.REPLY_ACTION.equals(intent.getAction())) {
                    CharSequence testMessage = getReplyMessage(intent);
                    String id = mDatabaseReference.child(AppConstants.FirebaseConstants.MESSAGES).child(notificationResponse.getRoomId()).push().getKey();
                    Message message = new Message();
                    message.setIdReceiver(String.valueOf(notificationResponse.getSenderUserId()));
                    message.setFirebaseKey(id);
                    message.setIdSender(String.valueOf(notificationResponse.getReceiverUserId()));
                    message.setMessageType(notificationResponse.getTextType());
                    message.setText(testMessage.toString());
                    message.setTimestamp(System.currentTimeMillis());
                    if (!AppUtils.isNetworkAvailable(context)) {
                        AppConstants.ClassConstants.mChatPushQueue.add(getChatData(message, notificationResponse.getRoomId()));
                    }
                    mDatabaseReference.child(AppConstants.FirebaseConstants.MESSAGES).child(notificationResponse.getRoomId()).child(id).setValue(message).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            if (AppUtils.isNetworkAvailable(context))
                                DataManager.getInstance().hitChatMessageNotificationApi(getChatData(message, notificationResponse.getRoomId()));
                        }
                    });

                } else if (AppConstants.ClassConstants.REPLY_ACTION_TO_CHAT.equals(intent.getAction())) {
                    Intent chatIntent = new Intent(context, ChatActivity.class);
                    chatIntent.putExtra(AppConstants.ClassConstants.ROOM_ID, notificationResponse.getRoomId());
                    chatIntent.putExtra(AppConstants.ClassConstants.USER_ID, notificationResponse.getReceiverUserId());
                    chatIntent.putExtra(AppConstants.ClassConstants.USER_NAME, notificationResponse.getUserName());
                    intent.putExtra(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.FROM_NOTIFICATION);
                    chatIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(chatIntent);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            int notificationId = intent.getIntExtra(AppConstants.ClassConstants.KEY_NOTIFICATION_ID, 0);
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
            notificationManager.cancel(notificationId);

        }
    }

    private HashMap<String, String> getChatData(Message message, String roomId) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put(AppConstants.NetworkConstants.SENDER_USER_ID, message.idSender);
        hashMap.put(AppConstants.NetworkConstants.RECIEVER_USER_ID, message.idReceiver);
        hashMap.put(AppConstants.NetworkConstants.CHAT_USER_NAME, DataManager.getInstance().getUserName());
        hashMap.put(AppConstants.NetworkConstants.MESSAGE, message.text);
        hashMap.put(AppConstants.NetworkConstants.TIME, String.valueOf(message.timestamp));
        hashMap.put(AppConstants.NetworkConstants.ROOM_ID, roomId);
        hashMap.put(AppConstants.NetworkConstants.TEXT_TYPE, message.messageType);
        hashMap.put(AppConstants.NetworkConstants.PROFILE_PIC, DataManager.getInstance().getProfilePic());
        return hashMap;
    }


    private CharSequence getReplyMessage(Intent intent) {
        Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
        if (remoteInput != null) {
            return remoteInput.getCharSequence(AppConstants.ClassConstants.KEY_TEXT_REPLY);
        }
        return null;
    }
}
