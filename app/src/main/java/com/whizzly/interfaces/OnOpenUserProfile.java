package com.whizzly.interfaces;

public interface OnOpenUserProfile {
    public void onUserProfile(String userId);
}
