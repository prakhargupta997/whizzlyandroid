package com.whizzly.interfaces;

public interface OnBackPressedListener {
    public void onBackPressed();
}
