package com.whizzly.interfaces;

public interface OnRecyclerViewClickListener {
    public void onRecyclerViewClicked(Object o, int pos);
}
