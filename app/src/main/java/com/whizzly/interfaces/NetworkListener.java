package com.whizzly.interfaces;

public interface NetworkListener {
    /**
     * This method is called when response ir received with code 200
     * @param responseCode - code of response
     * @param response - response in String
     * @param requestCode
     */
    void onSuccess(int responseCode, String response, int requestCode);


    /**
     * This method is called when response ir received in the error body
     * @param response - response in th error body
     * @param requestCode
     */
    void onError(String response, int requestCode);


    /**
     * This method is called when we receive a call in failure
     */
    void onFailure(String message);

}
