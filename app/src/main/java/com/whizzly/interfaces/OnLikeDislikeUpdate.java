package com.whizzly.interfaces;

public interface OnLikeDislikeUpdate {
    public void onLikeDislikeUpdate(int position, int status);
}
