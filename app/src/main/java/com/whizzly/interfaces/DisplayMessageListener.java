package com.whizzly.interfaces;

public interface DisplayMessageListener {
    void onDisplayMessage();
}
