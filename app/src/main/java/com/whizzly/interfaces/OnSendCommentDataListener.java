package com.whizzly.interfaces;

import com.whizzly.models.comment_list.Result;

public interface OnSendCommentDataListener {
    public void onCommentClicked(Result result, int position);
}
