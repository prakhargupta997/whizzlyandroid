package com.whizzly.interfaces;

public interface OnDateSelectListener {

    public void onDateSelect(String date, String month, String year);
}