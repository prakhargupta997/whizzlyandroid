package com.whizzly.interfaces;

public interface PermissionListener {
    public void  onPermissionGiven(int requestCode, String[] permissions, int[] grantResults);
}
