package com.whizzly.interfaces;

public interface OnActionDetectListener {
    public void onActionDone(Boolean action);
}
