package com.whizzly.interfaces;

public interface OnDownloadVideoCallback {
    public void onDownloadVideo(int requestCode,int[] grantresult,String[] permissions) ;
}
