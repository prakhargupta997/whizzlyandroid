package com.whizzly.chart_module.all_time;


import android.app.Activity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.chart_module.ReuseVideosChartActivity;
import com.whizzly.chart_module.ReuseVideosChartAdapter;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.FragmentAllTimeChartBinding;
import com.whizzly.models.reuse_videos_chart.ReuseVideoChartResponse;
import com.whizzly.models.reuse_videos_chart.VideoInfo;
import com.whizzly.notification.NotificationActivity;
import com.whizzly.utils.AppConstants;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllTimeChartFragment extends Fragment implements OnClickAdapterListener {
    private FragmentAllTimeChartBinding mFagmentAllTimeChartBinding;
    private AllTimeChartViewModel mAllTimeChartViewModel;
    private ArrayList<VideoInfo> mReuseVideoList;
    private ReuseVideosChartAdapter mAdapter;
    private Activity mActivity;

    public AllTimeChartFragment() {
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mActivity=getActivity();
        mFagmentAllTimeChartBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_all_time_chart, container, false);
        mAllTimeChartViewModel = ViewModelProviders.of(this).get(AllTimeChartViewModel.class);
        mFagmentAllTimeChartBinding.setViewModel(mAllTimeChartViewModel);
        mAllTimeChartViewModel.setGenericListeners(((ReuseVideosChartActivity) Objects.requireNonNull(getActivity())).getErrorObserver(), ((ReuseVideosChartActivity) Objects.requireNonNull(getActivity())).getFailureResponseObserver());
        mReuseVideoList = new ArrayList<>();
        observeLiveData();
        setUpAdapter();
        getChartListing();
        return mFagmentAllTimeChartBinding.getRoot();
    }

    private void getChartListing() {
        mAllTimeChartViewModel.getAllTimeChartListing();
    }

    private void setUpAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mFagmentAllTimeChartBinding.rvAllTimeChart.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(),
                layoutManager.getOrientation());
        mFagmentAllTimeChartBinding.rvAllTimeChart.addItemDecoration(dividerItemDecoration);
        mAdapter = new ReuseVideosChartAdapter(this, mReuseVideoList, getActivity());
        mFagmentAllTimeChartBinding.rvAllTimeChart.setAdapter(mAdapter);


    }

    private void observeLiveData() {
        mAllTimeChartViewModel.getAllTimeReuseVideosLiveData().observe(this, new Observer<ReuseVideoChartResponse>() {
            @Override
            public void onChanged(@Nullable ReuseVideoChartResponse reuseVideoChartResponse) {
                if (reuseVideoChartResponse!=null) {
                    if (reuseVideoChartResponse.getCode()==301 ||reuseVideoChartResponse.getCode()==302){
                        Toast.makeText(mActivity, reuseVideoChartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        ((BaseActivity) Objects.requireNonNull(getActivity())).autoLogOut();
                    }
                  else if (reuseVideoChartResponse.getCode() == 200) {
                        if (reuseVideoChartResponse.getResult().getVideoInfo().size() > 0) {

                            mReuseVideoList.addAll(reuseVideoChartResponse.getResult().getVideoInfo());
                            mAdapter.notifyDataSetChanged();
                            mFagmentAllTimeChartBinding.ivNoData.setVisibility(View.GONE);
                            mFagmentAllTimeChartBinding.rvAllTimeChart.setVisibility(View.VISIBLE);

                        } else {
                            mFagmentAllTimeChartBinding.ivNoData.setVisibility(View.VISIBLE);
                            mFagmentAllTimeChartBinding.rvAllTimeChart.setVisibility(View.GONE);
                        }
                    }
                    else {
                        Toast.makeText(getActivity(), reuseVideoChartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public void onAdapterClicked(int code, Object obj, int position) {
        switch (code){
            case AppConstants.ClassConstants.ON_VIDEO_REUSE_CLICKED:
                if (obj!=null &&obj instanceof VideoInfo) {
                    VideoInfo videoInfo= (VideoInfo) obj;
                    Intent intent = new Intent(mActivity, NotificationActivity.class);
                    intent.putExtra(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.FROM_CHARTS);
                    intent.putExtra(AppConstants.ClassConstants.NOTIFICATION_DATA, videoInfo);
                    startActivity(intent);
                }
                break;

        }
    }
}
