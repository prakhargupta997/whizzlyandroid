package com.whizzly.chart_module.weekly;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;

import com.whizzly.base_classes.RichMediatorLiveData;
import com.whizzly.models.FailureResponse;
import com.whizzly.models.reuse_videos_chart.ReuseVideoChartResponse;
import com.whizzly.network.NetworkCallback;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

import java.util.HashMap;

public class WeekChartViewModel extends ViewModel {
    private RichMediatorLiveData<ReuseVideoChartResponse> mWeeklyDataRichMediatorLiveData;
    private Observer<FailureResponse> failureResponseObserver;
    private Observer<Throwable> errorObserver;

    public void setGenericListeners(Observer<Throwable> errorObserver, Observer<FailureResponse> failureResponseObserver) {
        this.failureResponseObserver = failureResponseObserver;
        this.errorObserver = errorObserver;
        initLiveData();
    }

    private void initLiveData() {
        mWeeklyDataRichMediatorLiveData = new RichMediatorLiveData<ReuseVideoChartResponse>() {
            @Override
            protected Observer<FailureResponse> getFailureObserver() {
                return failureResponseObserver;
            }

            @Override
            protected Observer<Throwable> getErrorObserver() {
                return errorObserver;
            }
        };
    }

    public RichMediatorLiveData<ReuseVideoChartResponse> getWeeklyReuseVideosLiveData() {
        return mWeeklyDataRichMediatorLiveData;
    }

    public void getWeelklyReusedVideoList() {
        DataManager.getInstance().hitReusedVideosChartApi(getData()).enqueue(new NetworkCallback<ReuseVideoChartResponse>() {
            @Override
            public void onSuccess(ReuseVideoChartResponse reuseVideoChartResponse) {
            mWeeklyDataRichMediatorLiveData.setValue(reuseVideoChartResponse);
            }

            @Override
            public void onFailure(FailureResponse failureResponse) {
            mWeeklyDataRichMediatorLiveData.setFailure(failureResponse);
            }

            @Override
            public void onError(Throwable t) {
              mWeeklyDataRichMediatorLiveData.setError(t);
            }
        });
    }

    private HashMap<String,String> getData() {
        HashMap<String,String> hashMap=new HashMap<>();
        hashMap.put(AppConstants.NetworkConstants.USER_ID,String.valueOf(DataManager.getInstance().getUserId()));
        hashMap.put(AppConstants.NetworkConstants.VIDEO_REUSED_TYPE,"2");
        return hashMap;
    }
}
