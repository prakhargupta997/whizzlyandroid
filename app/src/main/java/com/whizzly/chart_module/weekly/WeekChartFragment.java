package com.whizzly.chart_module.weekly;


import android.app.Activity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.chart_module.ReuseVideosChartActivity;
import com.whizzly.chart_module.ReuseVideosChartAdapter;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.FragmentWeekChartBinding;
import com.whizzly.models.reuse_videos_chart.VideoInfo;
import com.whizzly.notification.NotificationActivity;
import com.whizzly.utils.AppConstants;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class WeekChartFragment extends Fragment implements OnClickAdapterListener {
    private FragmentWeekChartBinding mFragmentWeekChartBinding;
    private WeekChartViewModel mWeekChartViewModel;
    private ArrayList<VideoInfo> mReuseVideoList;
    private Activity mActivity;
    private ReuseVideosChartAdapter mAdapter;

    public WeekChartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mActivity = getActivity();
        mFragmentWeekChartBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_week_chart, container, false);
        mWeekChartViewModel = ViewModelProviders.of(this).get( WeekChartViewModel.class);
        mWeekChartViewModel.setGenericListeners(((ReuseVideosChartActivity) Objects.requireNonNull(getActivity())).getErrorObserver(), ((ReuseVideosChartActivity) Objects.requireNonNull(getActivity())).getFailureResponseObserver());
        mFragmentWeekChartBinding.setViewModel(mWeekChartViewModel);
        mReuseVideoList = new ArrayList<>();
        setUpAdapter();
        observeLiveData();
        getChartsListing();
        return mFragmentWeekChartBinding.getRoot();

    }

    private void getChartsListing() {
        mWeekChartViewModel.getWeelklyReusedVideoList();
    }

    private void setUpAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        mFragmentWeekChartBinding.rvWeekChart.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(),
                layoutManager.getOrientation());
        mFragmentWeekChartBinding.rvWeekChart.addItemDecoration(dividerItemDecoration);
        mAdapter = new ReuseVideosChartAdapter(this, mReuseVideoList, mActivity);
        mFragmentWeekChartBinding.rvWeekChart.setAdapter(mAdapter);


    }


    private void observeLiveData() {
        mWeekChartViewModel.getWeeklyReuseVideosLiveData().observe(this, reuseVideoChartResponse -> {
            if (reuseVideoChartResponse != null) {
                if (reuseVideoChartResponse.getCode() == 301 || reuseVideoChartResponse.getCode() == 302) {
                    Toast.makeText(mActivity, reuseVideoChartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) Objects.requireNonNull(getActivity())).autoLogOut();
                }
                if (reuseVideoChartResponse.getCode() == 200) {
                    if (reuseVideoChartResponse.getResult().getVideoInfo().size() > 0) {
                        mFragmentWeekChartBinding.ivNoData.setVisibility(View.GONE);
                        mFragmentWeekChartBinding.rvWeekChart.setVisibility(View.VISIBLE);
                        mReuseVideoList.addAll(reuseVideoChartResponse.getResult().getVideoInfo());
                        mAdapter.notifyDataSetChanged();
                    } else {
                        mFragmentWeekChartBinding.ivNoData.setVisibility(View.VISIBLE);
                        mFragmentWeekChartBinding.rvWeekChart.setVisibility(View.GONE);

                    }
                } else {
                    Toast.makeText(getActivity(), reuseVideoChartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }


        });
    }

    @Override
    public void onAdapterClicked(int code, Object obj, int position) {
        switch (code) {
            case AppConstants.ClassConstants.ON_VIDEO_REUSE_CLICKED:
                if (obj != null && obj instanceof VideoInfo) {
                    VideoInfo videoInfo = (VideoInfo) obj;
                    Intent intent = new Intent(mActivity, NotificationActivity.class);
                    intent.putExtra(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.FROM_CHARTS);
                    intent.putExtra(AppConstants.ClassConstants.NOTIFICATION_DATA, videoInfo);
                    startActivity(intent);
                }
                break;

        }
    }
}
