package com.whizzly.chart_module;


import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.collab_module.model.ChooseTempleteSizeModel;
import com.whizzly.collab_module.viewmodel.ChooseTempleteSizeViewModel;
import com.whizzly.collab_module.fragment.VideoReplaceFragment;
import com.whizzly.databinding.FragmentChartsCollabTempleteBinding;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.models.video_list_response.Result;
import com.whizzly.notification.NotificationActivity;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChartsCollabTempleteFragment extends Fragment implements OnClickSendListener {

    private FragmentChartsCollabTempleteBinding mChartsCollabTempleteFragment;
    private ChooseTempleteSizeModel mChooseTempleteSizeModel;
    private ChooseTempleteSizeViewModel mChooseTempleteSizeViewModel;
    private int mTempleteSize = 0;
    private Result mResult;

    public ChartsCollabTempleteFragment() {
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_TEMP_SIZE_TWO:
                mTempleteSize = 2;
                setView(true, false, false, false, false);
                break;
            case AppConstants.ClassConstants.ON_TEMP_SIZE_THREE:
                mTempleteSize = 3;
                setView(false, true, false, false, false);
                break;
            case AppConstants.ClassConstants.ON_TEMP_SIZE_FOUR:
                mTempleteSize = 4;
                setView(false, false, true, false, false);
                break;
            case AppConstants.ClassConstants.ON_TEMP_SIZE_FIVE:
                mTempleteSize = 5;
                setView(false, false, false, true, false);
                break;
            case AppConstants.ClassConstants.ON_TEMP_SIZE_SIX:
                mTempleteSize = 6;
                setView(false, false, false, false, true);
                break;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getData();
    }

    private void setViews() {
        mChartsCollabTempleteFragment.toolbar.tvToolbarText.setText(getString(R.string.txt_choose_templete_theme));
        mChartsCollabTempleteFragment.toolbar.tvDone.setVisibility(View.VISIBLE);
        mChartsCollabTempleteFragment.toolbar.tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mChooseTempleteSizeModel.getIsTwoClicked() || !mChooseTempleteSizeModel.getIsThreeClicked() ||
                        !mChooseTempleteSizeModel.getIsFourClicked() && !mChooseTempleteSizeModel.getIsFiveClicked() &&
                                !mChooseTempleteSizeModel.getIsSixClicked()) {
                    mResult.setTemplateSize(mTempleteSize);
                    Bundle collabData = new Bundle();
                    collabData.putParcelable(AppConstants.ClassConstants.COLLAB_DATA, mResult);
                    VideoReplaceFragment videoReplaceFragment = new VideoReplaceFragment();
                    videoReplaceFragment.setArguments(collabData);
                    if (getActivity() instanceof NotificationActivity && AppUtils.isNetworkAvailable(getActivity())) {
                        ((NotificationActivity) getActivity()).addFragmentFromRight(R.id.fl_container, videoReplaceFragment);
                    }
                } else {
                    Toast.makeText(getActivity(), getString(R.string.txt_error_no_template_selected), Toast.LENGTH_SHORT).show();
                }
            }
        });

        mChartsCollabTempleteFragment.toolbar.ivBack.setVisibility(View.VISIBLE);
        mChartsCollabTempleteFragment.toolbar.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    private void setView(boolean twoClicked, boolean threeClicked, boolean fourClicked, boolean fiveClicked, boolean sixClicked) {
        mChooseTempleteSizeModel.setIsTwoClicked(twoClicked);
        mChooseTempleteSizeModel.setIsThreeClicked(threeClicked);
        mChooseTempleteSizeModel.setIsFourClicked(fourClicked);
        mChooseTempleteSizeModel.setIsFiveClicked(fiveClicked);
        mChooseTempleteSizeModel.setIsSixClicked(sixClicked);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mChartsCollabTempleteFragment = DataBindingUtil.inflate(inflater, R.layout.fragment_charts_collab_templete, container, false);
        mChooseTempleteSizeModel = new ChooseTempleteSizeModel();
        mChartsCollabTempleteFragment.setModel(mChooseTempleteSizeModel);
        mChooseTempleteSizeViewModel = ViewModelProviders.of(this).get(ChooseTempleteSizeViewModel.class);
        mChartsCollabTempleteFragment.setViewModel(mChooseTempleteSizeViewModel);
        mChooseTempleteSizeViewModel.setOnClickSendListener(this);
        setViews();
        return mChartsCollabTempleteFragment.getRoot();
    }

    private void getData() {
        Bundle bundle = getArguments();
        mResult = bundle.getParcelable(AppConstants.ClassConstants.COLLAB_DATA);
    }

}
