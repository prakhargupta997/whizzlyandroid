package com.whizzly.chart_module;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.chart_module.all_time.AllTimeChartFragment;
import com.whizzly.chart_module.daily.DailyChartFragment;
import com.whizzly.chart_module.weekly.WeekChartFragment;
import com.whizzly.databinding.ActivityReuseVideosChartBinding;
import com.whizzly.interfaces.OnBackPressedListener;
import com.whizzly.utils.DataManager;
import com.whizzly.utils.GenericFragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ReuseVideosChartActivity extends BaseActivity {
    private ActivityReuseVideosChartBinding mActivityReuseVideosChartBinding;
    private ArrayList<Fragment> mList;
    private GenericFragmentPagerAdapter pagerAdapter;
    private OnBackPressedListener onBackPressedListener;

    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityReuseVideosChartBinding = DataBindingUtil.setContentView(this, R.layout.activity_reuse_videos_chart);
        setUpViewPager();
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_feed_user_pic_placeholder);
        requestOptions.error(R.drawable.ic_feed_user_pic_placeholder);
        Glide.with(this).load(DataManager.getInstance().getProfilePic()).apply(requestOptions.circleCrop()).into(mActivityReuseVideosChartBinding.ivProfile);
        mActivityReuseVideosChartBinding.toolbar.ivBack.setOnClickListener(v -> onBackPressed());
    }


    private void setUpViewPager() {
        mList = new ArrayList<>();

        DailyChartFragment dailyChartFragment = new DailyChartFragment();
        WeekChartFragment weekChartFragment = new WeekChartFragment();
        AllTimeChartFragment allTimeChartFragment = new AllTimeChartFragment();


        mList.add(dailyChartFragment);
        mList.add(weekChartFragment);
        mList.add(allTimeChartFragment);

        List<String> titles = new ArrayList<>();

        titles.add(getString(R.string.daily).toUpperCase());
        titles.add(getString(R.string.weekly).toUpperCase());
        titles.add(getString(R.string.all_time).toUpperCase());

        pagerAdapter = new GenericFragmentPagerAdapter(getSupportFragmentManager(), mList, titles);

        mActivityReuseVideosChartBinding.vpChart.setAdapter(pagerAdapter);
        mActivityReuseVideosChartBinding.slidingTabs.setupWithViewPager(mActivityReuseVideosChartBinding.vpChart);

    }

    @Override
    public void onBackPressed() {
        if (onBackPressedListener != null) {
            onBackPressedListener.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        onBackPressedListener = null;
        super.onDestroy();
    }
}
