package com.whizzly.chart_module;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.whizzly.R;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.ItemRowChartBinding;
import com.whizzly.models.reuse_videos_chart.VideoInfo;
import com.whizzly.utils.AppConstants;

import java.util.ArrayList;


public class ReuseVideosChartAdapter extends RecyclerView.Adapter<ReuseVideosChartAdapter.ViewHolder> {

    private ItemRowChartBinding itemRowChartBinding;
    private ViewGroup viewGroup;
    private OnClickAdapterListener mOnClickAdapterListener;
    private Context mContext;
    private ArrayList<VideoInfo> mReuseVideoList;

    public ReuseVideosChartAdapter(OnClickAdapterListener onClickAdapterListener, ArrayList<VideoInfo> reuseVideoList, Context context) {
     this.mContext=context;
     this.mOnClickAdapterListener=onClickAdapterListener;
     this.mReuseVideoList=reuseVideoList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        itemRowChartBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_row_chart, viewGroup, false);
        this.viewGroup = viewGroup;
        return new ViewHolder(itemRowChartBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Glide.with(itemRowChartBinding.getRoot()).load(mReuseVideoList.get(i).getThumbnailUrl()).into(itemRowChartBinding.ivVideoThumbnail);
        viewHolder.itemRowChartBinding.tvUsername.setText(mReuseVideoList.get(i).getUserName());
        viewHolder.itemRowChartBinding.tvVideoTitle.setText(mReuseVideoList.get(i).getTitle());
        viewHolder.itemRowChartBinding.tvVideosCount.setText(mReuseVideoList.get(i).getReuseCount()+" "+mContext.getString(R.string.txt_reused));
        viewHolder.itemRowChartBinding.getRoot().setOnClickListener(view -> {
            mOnClickAdapterListener.onAdapterClicked(AppConstants.ClassConstants.ON_VIDEO_REUSE_CLICKED, mReuseVideoList.get(i), i);

        });
    }

    @Override
    public int getItemCount() {
        return mReuseVideoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ItemRowChartBinding itemRowChartBinding;

        public ViewHolder(@NonNull ItemRowChartBinding itemRowChartBinding) {
            super(itemRowChartBinding.getRoot());
            this.itemRowChartBinding = itemRowChartBinding;
        }


    }
}
