package com.whizzly.chart_module.daily;


import android.app.Activity;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.chart_module.ReuseVideosChartActivity;
import com.whizzly.chart_module.ReuseVideosChartAdapter;
import com.whizzly.collab_module.interfaces.OnClickAdapterListener;
import com.whizzly.databinding.FragmentDailyChartBinding;
import com.whizzly.models.reuse_videos_chart.VideoInfo;
import com.whizzly.notification.NotificationActivity;
import com.whizzly.utils.AppConstants;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class DailyChartFragment extends Fragment implements OnClickAdapterListener {

    private FragmentDailyChartBinding mFragmentDailyChartBinding;
    private DailyChartViewModel mDailyChartViewModel;
    private ArrayList<VideoInfo> mReuseVideoList;
    private ReuseVideosChartAdapter mAdapter;
    private Activity mActivity;

    public DailyChartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mActivity = getActivity();
        mFragmentDailyChartBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_daily_chart, container, false);
        mDailyChartViewModel = ViewModelProviders.of(this).get(DailyChartViewModel.class);
        mDailyChartViewModel.setGenericListeners(((ReuseVideosChartActivity) Objects.requireNonNull(getActivity())).getErrorObserver(), ((ReuseVideosChartActivity) Objects.requireNonNull(getActivity())).getFailureResponseObserver());
        mFragmentDailyChartBinding.setViewModel(mDailyChartViewModel);
        observeLiveData();
        mReuseVideoList = new ArrayList<>();
        setUpAdapter();
        getDailyReusedVideoList();
        return mFragmentDailyChartBinding.getRoot();
    }

    private void getDailyReusedVideoList() {
        mDailyChartViewModel.getDailyReusedVideoListing();
    }

    private void setUpAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(),
                layoutManager.getOrientation());
        mFragmentDailyChartBinding.rvDailyChart.addItemDecoration(dividerItemDecoration);
        mFragmentDailyChartBinding.rvDailyChart.setLayoutManager(layoutManager);
        mAdapter = new ReuseVideosChartAdapter(this, mReuseVideoList, getActivity());
        mFragmentDailyChartBinding.rvDailyChart.setAdapter(mAdapter);


    }

    private void observeLiveData() {
        mDailyChartViewModel.getDailyReuseVideosLiveData().observe(this, reuseVideoChartResponse -> {
            if (reuseVideoChartResponse.getResult() != null) {
                if (reuseVideoChartResponse.getCode() == 301 || reuseVideoChartResponse.getCode() == 302) {
                    Toast.makeText(mActivity, reuseVideoChartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    ((BaseActivity) Objects.requireNonNull(getActivity())).autoLogOut();
                } else if (reuseVideoChartResponse.getCode() == 200) {
                    if (reuseVideoChartResponse.getResult().getVideoInfo().size() > 0) {
                        mFragmentDailyChartBinding.ivNoData.setVisibility(View.GONE);
                        mFragmentDailyChartBinding.rvDailyChart.setVisibility(View.VISIBLE);

                        mReuseVideoList.addAll(reuseVideoChartResponse.getResult().getVideoInfo());
                        mAdapter.notifyDataSetChanged();
                    } else {
                        mFragmentDailyChartBinding.ivNoData.setVisibility(View.VISIBLE);
                        mFragmentDailyChartBinding.rvDailyChart.setVisibility(View.GONE);
                    }
                } else {
                    Toast.makeText(getActivity(), reuseVideoChartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    @Override
    public void onAdapterClicked(int code, Object obj, int position) {
        switch (code) {
            case AppConstants.ClassConstants.ON_VIDEO_REUSE_CLICKED:
                if (obj != null && obj instanceof VideoInfo) {
                    VideoInfo videoInfo = (VideoInfo) obj;
                    Intent intent = new Intent(mActivity, NotificationActivity.class);
                    intent.putExtra(AppConstants.ClassConstants.IS_FROM, AppConstants.ClassConstants.FROM_CHARTS);
                    intent.putExtra(AppConstants.ClassConstants.NOTIFICATION_DATA, videoInfo);
                    startActivity(intent);
                }
                break;

        }
    }
}
