package com.whizzly.staticpages;

import android.annotation.TargetApi;
import androidx.databinding.DataBindingUtil;
import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.ActivityStaticPagesBinding;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

public class StaticPagesActivity extends BaseActivity {
private ActivityStaticPagesBinding mActivityStaticPagesBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       mActivityStaticPagesBinding= DataBindingUtil.setContentView(this,R.layout.activity_static_pages);
       getData();
        if(DataManager.getInstance().isNotchDevice()){
            mActivityStaticPagesBinding.toolbar.view.setVisibility(View.VISIBLE);
        }else{
            mActivityStaticPagesBinding.toolbar.view.setVisibility(View.GONE);
        }
       mActivityStaticPagesBinding.toolbar.ivBack.setVisibility(View.VISIBLE);
       mActivityStaticPagesBinding.toolbar.ivBack.setOnClickListener(v -> onBackPressed());
    }

    private void getData() {
        if (getIntent()!=null&&getIntent().hasExtra(AppConstants.ClassConstants.IS_FOR)){
            switch (getIntent().getStringExtra(AppConstants.ClassConstants.IS_FOR)){
                case AppConstants.ClassConstants.PRIVACY_POLICY:
                    loadWebUrl(AppConstants.ClassConstants.PRIVACY_POLICY);
                    break;
                case AppConstants.ClassConstants.TERMS_AND_CONDITION:
                    loadWebUrl(AppConstants.ClassConstants.TERMS_AND_CONDITION);
                    break;
            }
        }
    }
    private void loadWebUrl(String pageTittle) {
        mActivityStaticPagesBinding.webview.setWebViewClient(new WebViewClient() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });
        switch (pageTittle) {
            case AppConstants.ClassConstants.PRIVACY_POLICY:
                mActivityStaticPagesBinding.webview.loadUrl("http://103.251.140.53:5000/privacy-policy/");
                mActivityStaticPagesBinding.toolbar.tvToolbarText.setText(R.string.s_privacy_policy);
                break;
            case AppConstants.ClassConstants.TERMS_AND_CONDITION:
                mActivityStaticPagesBinding.webview.loadUrl("http://103.251.140.53:5000/terms-conditions/");
                mActivityStaticPagesBinding.toolbar.tvToolbarText.setText(R.string.s_terms_and_conditions);
                break;
        }
    }

}
