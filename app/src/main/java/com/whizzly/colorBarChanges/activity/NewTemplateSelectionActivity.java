package com.whizzly.colorBarChanges.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.activity.VideoRecordActivity;
import com.whizzly.colorBarChanges.model.NewTemplateSelectionModel;
import com.whizzly.colorBarChanges.viewModel.NewTemplateSelectionViewModel;
import com.whizzly.databinding.ActivityNewTemplateSelectionBinding;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

public class NewTemplateSelectionActivity extends BaseActivity implements OnClickSendListener {

    private ActivityNewTemplateSelectionBinding mActivityNewTemplateSelectionBinding;
    private NewTemplateSelectionModel mNewTemplateSelectionModel;
    private int templateSize = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityNewTemplateSelectionBinding = DataBindingUtil.setContentView(this, R.layout.activity_new_template_selection);
        mNewTemplateSelectionModel = new NewTemplateSelectionModel();
        NewTemplateSelectionViewModel mNewTemplateSelectionViewModel = ViewModelProviders.of(this).get(NewTemplateSelectionViewModel.class);
        mActivityNewTemplateSelectionBinding.setModel(mNewTemplateSelectionModel);
        mNewTemplateSelectionViewModel.setOnClickSendListener(this);
        mActivityNewTemplateSelectionBinding.setViewModel(mNewTemplateSelectionViewModel);
        setView();
        if(DataManager.getInstance().isNotchDevice()){
            mActivityNewTemplateSelectionBinding.toolbar.view.setVisibility(View.VISIBLE);
        }else{
            mActivityNewTemplateSelectionBinding.toolbar.view.setVisibility(View.GONE);
        }
        if(DataManager.getInstance().isNotchDevice()){
            mActivityNewTemplateSelectionBinding.toolbar.view.setVisibility(View.VISIBLE);
        }else{
            mActivityNewTemplateSelectionBinding.toolbar.view.setVisibility(View.GONE);
        }
    }

    private void setView() {
        mActivityNewTemplateSelectionBinding.toolbar.tvToolbarText.setText(getString(R.string.txt_choose_templete_theme));
        mActivityNewTemplateSelectionBinding.toolbar.tvDone.setVisibility(View.VISIBLE);
        mActivityNewTemplateSelectionBinding.toolbar.ivBack.setVisibility(View.VISIBLE);
        mActivityNewTemplateSelectionBinding.toolbar.tvDone.setOnClickListener(view -> {
            if (templateSize != 0) {
                Intent intent = new Intent(this, VideoRecordActivity.class);
                intent.putExtra(AppConstants.TEMPLATE_SIZE, templateSize);
                startActivity(intent);
            } else {
                Toast.makeText(this, R.string.txt_choose_templete_error, Toast.LENGTH_SHORT).show();
            }
        });

        mActivityNewTemplateSelectionBinding.toolbar.ivBack.setOnClickListener(view->{
            onBackPressed();
        });
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_TEMP_SIZE_TWO:
                templateSize = 2;
                setView(true, false, false, false, false);
                break;
            case AppConstants.ClassConstants.ON_TEMP_SIZE_THREE:
                templateSize = 3;
                setView(false, true, false, false, false);
                break;
            case AppConstants.ClassConstants.ON_TEMP_SIZE_FOUR:
                templateSize = 4;
                setView(false, false, true, false, false);
                break;
            case AppConstants.ClassConstants.ON_TEMP_SIZE_FIVE:
                templateSize = 5;
                setView(false, false, false, true, false);
                break;
            case AppConstants.ClassConstants.ON_TEMP_SIZE_SIX:
                templateSize = 6;
                setView(false, false, false, false, true);
                break;
        }
    }

    private void setView(boolean twoClicked, boolean threeClicked, boolean fourClicked, boolean fiveClicked, boolean sixClicked) {
        mNewTemplateSelectionModel.setIsTwoClicked(twoClicked);
        mNewTemplateSelectionModel.setIsThreeClicked(threeClicked);
        mNewTemplateSelectionModel.setIsFourClicked(fourClicked);
        mNewTemplateSelectionModel.setIsFiveClicked(fiveClicked);
        mNewTemplateSelectionModel.setIsSixClicked(sixClicked);
    }
}
