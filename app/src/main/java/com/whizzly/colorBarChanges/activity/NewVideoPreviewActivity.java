package com.whizzly.colorBarChanges.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.whizzly.R;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.collab_module.beans.CollabBarBean;
import com.whizzly.collab_module.beans.VideoFramesBean;
import com.whizzly.collab_module.fragment.CollabBarFragment;
import com.whizzly.collab_module.interfaces.OnSendTimeRange;
import com.whizzly.databinding.ActivityNewVideoPreviewBinding;
import com.whizzly.self_collab.JoinSelfCollabActivity;
import com.whizzly.self_collab.SelfCollabRecordActivity;
import com.whizzly.self_collab.SelfCollabVideoDataBean;
import com.whizzly.utils.AppConstants;

import java.io.File;
import java.util.ArrayList;

import static android.widget.RelativeLayout.CENTER_HORIZONTAL;

public class NewVideoPreviewActivity extends BaseActivity implements View.OnClickListener, OnSendTimeRange {

    public ArrayList<VideoFramesBean> mVideoFrameBeanList = new ArrayList<>();
    private ActivityNewVideoPreviewBinding mActivityNewVideoPreviewBinding;
    private ArrayList<SelfCollabVideoDataBean> mSelfCollabVideoDataBeanArrayList;
    private SimpleExoPlayer mExoplayerOne, mExoplayerTwo, mExoplayerThree, mExoplayerFour, mExoplayerFive, mExoplayerSix;
    private int templateSize;
    private int musicId;
    private int frameSize;
    private ArrayList<CollabBarBean> mCollabBarBeans;
    private long durationInMillies;
    private int frameWidth = 60;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityNewVideoPreviewBinding = DataBindingUtil.setContentView(this, R.layout.activity_new_video_preview);
        getData();
        mActivityNewVideoPreviewBinding.flLoading.setVisibility(View.VISIBLE);
        durationInMillies = Long.parseLong(mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).getVideoLength());
        getFramesFromVideo();
        setIndeteminateProgressBar(mCollabBarBeans);
    }

    private void getData() {
        mSelfCollabVideoDataBeanArrayList = (ArrayList<SelfCollabVideoDataBean>) getIntent().getSerializableExtra(AppConstants.ClassConstants.VIDEO_DATA);
        templateSize = getIntent().getIntExtra(AppConstants.ClassConstants.TEMPLETE_SIZE, 0);
        musicId = getIntent().getIntExtra(AppConstants.ClassConstants.MUSIC_ID, 0);
        frameSize = getIntent().getIntExtra(AppConstants.ClassConstants.FRAMES, 0);
        mCollabBarBeans = getIntent().getParcelableArrayListExtra(AppConstants.ClassConstants.COLOR_BAR_DATA);
    }

    private void setView() {
        switch (mSelfCollabVideoDataBeanArrayList.size()) {
            case 2:
                setVideoViews(View.INVISIBLE, View.VISIBLE, View.INVISIBLE);
                break;
            case 3:
                setVideoViews(View.VISIBLE, View.VISIBLE, View.GONE);
                break;
            case 4:
                setVideoViews(View.VISIBLE, View.VISIBLE, View.GONE);
                break;
            case 5:
                setVideoViews(View.VISIBLE, View.VISIBLE, View.VISIBLE);
                break;
            case 6:
                setVideoViews(View.VISIBLE, View.VISIBLE, View.VISIBLE);
                break;
        }
        setExoplayers();
        mActivityNewVideoPreviewBinding.flLoading.setVisibility(View.GONE);
        mActivityNewVideoPreviewBinding.ivCollabBar.setOnClickListener(this);
        mActivityNewVideoPreviewBinding.ivSaveRecording.setOnClickListener(this);

    }

    private void setExoplayers() {
        switch (mSelfCollabVideoDataBeanArrayList.size()) {
            case 2:
                LinearLayoutCompat.LayoutParams layoutParams = (LinearLayoutCompat.LayoutParams) mActivityNewVideoPreviewBinding.llVideoThreeFour.getLayoutParams();
                layoutParams.weight = 1.0f;
                mActivityNewVideoPreviewBinding.llVideoThreeFour.setLayoutParams(layoutParams);
                mExoplayerOne = setExoplayer(mSelfCollabVideoDataBeanArrayList.get(0).getFinalVideo());
                mExoplayerTwo = setExoplayer(mSelfCollabVideoDataBeanArrayList.get(1).getFinalVideo());
                mActivityNewVideoPreviewBinding.exoplayerThree.setPlayer(mExoplayerOne);
                mActivityNewVideoPreviewBinding.exoplayerFour.setPlayer(mExoplayerTwo);
                mActivityNewVideoPreviewBinding.exoplayerThree.setVisibility(View.VISIBLE);
                mActivityNewVideoPreviewBinding.exoplayerFour.setVisibility(View.VISIBLE);
                setExoplayersVisibility(View.INVISIBLE, View.INVISIBLE, View.VISIBLE, View.VISIBLE, View.INVISIBLE, View.INVISIBLE);
                break;
            case 3:
                LinearLayoutCompat.LayoutParams layoutParams1 = (LinearLayoutCompat.LayoutParams) mActivityNewVideoPreviewBinding.llVideoThreeFour.getLayoutParams();
                layoutParams1.weight = 1.0f;

                LinearLayoutCompat.LayoutParams layoutParams8 = (LinearLayoutCompat.LayoutParams) mActivityNewVideoPreviewBinding.llVideoOneTwo.getLayoutParams();
                layoutParams8.weight = 1.0f;
                mActivityNewVideoPreviewBinding.llVideoOneTwo.setLayoutParams(layoutParams8);
                mActivityNewVideoPreviewBinding.llVideoThreeFour.setLayoutParams(layoutParams1);
                mExoplayerOne = setExoplayer(mSelfCollabVideoDataBeanArrayList.get(0).getFinalVideo());
                mExoplayerTwo = setExoplayer(mSelfCollabVideoDataBeanArrayList.get(1).getFinalVideo());
                mExoplayerThree = setExoplayer(mSelfCollabVideoDataBeanArrayList.get(2).getFinalVideo());
                mActivityNewVideoPreviewBinding.exoplayerOne.setPlayer(mExoplayerOne);
                mActivityNewVideoPreviewBinding.exoplayerTwo.setPlayer(mExoplayerTwo);
                mActivityNewVideoPreviewBinding.exoplayerThree.setPlayer(mExoplayerThree);
                setExoplayersVisibility(View.VISIBLE, View.VISIBLE, View.VISIBLE, View.GONE, View.INVISIBLE, View.INVISIBLE);
                Display display = getWindowManager().getDefaultDisplay();
                Point point = new Point();
                display.getSize(point);
                LinearLayoutCompat.LayoutParams layoutParams2 = new LinearLayoutCompat.LayoutParams(point.x / 2, ViewGroup.LayoutParams.MATCH_PARENT);
                layoutParams2.gravity = CENTER_HORIZONTAL;
                mActivityNewVideoPreviewBinding.exoplayerThree.setLayoutParams(layoutParams2);
                break;
            case 4:
                LinearLayoutCompat.LayoutParams layoutParams9 = (LinearLayoutCompat.LayoutParams) mActivityNewVideoPreviewBinding.llVideoThreeFour.getLayoutParams();
                layoutParams9.weight = 1.0f;

                LinearLayoutCompat.LayoutParams layoutParams18 = (LinearLayoutCompat.LayoutParams) mActivityNewVideoPreviewBinding.llVideoOneTwo.getLayoutParams();
                layoutParams18.weight = 1.0f;

                mActivityNewVideoPreviewBinding.llVideoOneTwo.setLayoutParams(layoutParams18);
                mActivityNewVideoPreviewBinding.llVideoThreeFour.setLayoutParams(layoutParams9);
                mExoplayerOne = setExoplayer(mSelfCollabVideoDataBeanArrayList.get(0).getFinalVideo());
                mExoplayerTwo = setExoplayer(mSelfCollabVideoDataBeanArrayList.get(1).getFinalVideo());
                mExoplayerThree = setExoplayer(mSelfCollabVideoDataBeanArrayList.get(2).getFinalVideo());
                mExoplayerFour = setExoplayer(mSelfCollabVideoDataBeanArrayList.get(3).getFinalVideo());
                mActivityNewVideoPreviewBinding.exoplayerOne.setPlayer(mExoplayerOne);
                mActivityNewVideoPreviewBinding.exoplayerTwo.setPlayer(mExoplayerTwo);
                mActivityNewVideoPreviewBinding.exoplayerThree.setPlayer(mExoplayerThree);
                mActivityNewVideoPreviewBinding.exoplayerFour.setPlayer(mExoplayerFour);
                setExoplayersVisibility(View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.INVISIBLE, View.INVISIBLE);
                break;
            case 5:

                mExoplayerOne = setExoplayer(mSelfCollabVideoDataBeanArrayList.get(0).getFinalVideo());
                mExoplayerTwo = setExoplayer(mSelfCollabVideoDataBeanArrayList.get(1).getFinalVideo());
                mExoplayerThree = setExoplayer(mSelfCollabVideoDataBeanArrayList.get(2).getFinalVideo());
                mExoplayerFour = setExoplayer(mSelfCollabVideoDataBeanArrayList.get(3).getFinalVideo());
                mExoplayerFive = setExoplayer(mSelfCollabVideoDataBeanArrayList.get(4).getFinalVideo());
                mActivityNewVideoPreviewBinding.exoplayerOne.setPlayer(mExoplayerOne);
                mActivityNewVideoPreviewBinding.exoplayerTwo.setPlayer(mExoplayerTwo);
                mActivityNewVideoPreviewBinding.exoplayerThree.setPlayer(mExoplayerThree);
                mActivityNewVideoPreviewBinding.exoplayerFour.setPlayer(mExoplayerFour);
                mActivityNewVideoPreviewBinding.exoplayerFive.setPlayer(mExoplayerFive);
                setExoplayersVisibility(View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.GONE);
                Display display1 = getWindowManager().getDefaultDisplay();
                Point point1 = new Point();
                display1.getSize(point1);
                LinearLayoutCompat.LayoutParams layoutParams3 = new LinearLayoutCompat.LayoutParams(point1.x / 2, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams3.gravity = (CENTER_HORIZONTAL);
                mActivityNewVideoPreviewBinding.exoplayerFive.setLayoutParams(layoutParams3);
                break;
            case 6:
                mExoplayerOne = setExoplayer(mSelfCollabVideoDataBeanArrayList.get(0).getFinalVideo());
                mExoplayerTwo = setExoplayer(mSelfCollabVideoDataBeanArrayList.get(1).getFinalVideo());
                mExoplayerThree = setExoplayer(mSelfCollabVideoDataBeanArrayList.get(2).getFinalVideo());
                mExoplayerFour = setExoplayer(mSelfCollabVideoDataBeanArrayList.get(3).getFinalVideo());
                mExoplayerFive = setExoplayer(mSelfCollabVideoDataBeanArrayList.get(4).getFinalVideo());
                mExoplayerSix = setExoplayer(mSelfCollabVideoDataBeanArrayList.get(5).getFinalVideo());
                mActivityNewVideoPreviewBinding.exoplayerOne.setPlayer(mExoplayerOne);
                mActivityNewVideoPreviewBinding.exoplayerTwo.setPlayer(mExoplayerTwo);
                mActivityNewVideoPreviewBinding.exoplayerThree.setPlayer(mExoplayerThree);
                mActivityNewVideoPreviewBinding.exoplayerFour.setPlayer(mExoplayerFour);
                mActivityNewVideoPreviewBinding.exoplayerFive.setPlayer(mExoplayerFive);
                mActivityNewVideoPreviewBinding.exoplayerSix.setPlayer(mExoplayerSix);
                setExoplayersVisibility(View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE, View.VISIBLE);
                LinearLayoutCompat.LayoutParams layoutParams4 = (LinearLayoutCompat.LayoutParams) mActivityNewVideoPreviewBinding.llVideoOneTwo.getLayoutParams();
                layoutParams4.weight = 1.0f;
                mActivityNewVideoPreviewBinding.llVideoOneTwo.setLayoutParams(layoutParams4);

                LinearLayoutCompat.LayoutParams layoutParams5 = (LinearLayoutCompat.LayoutParams) mActivityNewVideoPreviewBinding.llVideoThreeFour.getLayoutParams();
                layoutParams5.weight = 1.0f;
                mActivityNewVideoPreviewBinding.llVideoThreeFour.setLayoutParams(layoutParams5);

                LinearLayoutCompat.LayoutParams layoutParams6 = (LinearLayoutCompat.LayoutParams) mActivityNewVideoPreviewBinding.llVideoFiveSix.getLayoutParams();
                layoutParams6.weight = 1.0f;
                mActivityNewVideoPreviewBinding.llVideoFiveSix.setLayoutParams(layoutParams6);
                break;
        }
    }

    private void setExoplayersVisibility(int one, int two, int three, int four, int five, int six) {
        mActivityNewVideoPreviewBinding.exoplayerOne.setVisibility(one);
        mActivityNewVideoPreviewBinding.exoplayerTwo.setVisibility(two);
        mActivityNewVideoPreviewBinding.exoplayerThree.setVisibility(three);
        mActivityNewVideoPreviewBinding.exoplayerFour.setVisibility(four);
        mActivityNewVideoPreviewBinding.exoplayerFive.setVisibility(five);
        mActivityNewVideoPreviewBinding.exoplayerSix.setVisibility(six);
    }

    private SimpleExoPlayer setExoplayer(String videoFileName) {
        SimpleExoPlayer exoPlayer = ExoPlayerFactory.newSimpleInstance(this,
                new DefaultRenderersFactory(this),
                new DefaultTrackSelector(), new DefaultLoadControl());
        exoPlayer.setPlayWhenReady(true);
        exoPlayer.setRepeatMode(Player.REPEAT_MODE_ALL);
        Uri uri = Uri.fromFile(new File(videoFileName));
        MediaSource mediaSource = buildMediaSource(uri);
        exoPlayer.prepare(mediaSource);
        return exoPlayer;
    }

    private MediaSource buildMediaSource(Uri uri) {
        return new ExtractorMediaSource.Factory(
                new DefaultDataSourceFactory(this, "whizzly-app")).
                createMediaSource(uri);
    }

    private void setVideoViews(int one, int two, int three) {
        mActivityNewVideoPreviewBinding.llVideoOneTwo.setVisibility(one);
        mActivityNewVideoPreviewBinding.llVideoThreeFour.setVisibility(two);
        mActivityNewVideoPreviewBinding.llVideoFiveSix.setVisibility(three);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_collab_bar:
                mActivityNewVideoPreviewBinding.ivCollabBar.setEnabled(false);
                new Handler().postDelayed(() -> mActivityNewVideoPreviewBinding.ivCollabBar.setEnabled(true), 2000);
                CollabBarFragment collabBarFragment = new CollabBarFragment();
                Bundle collabBundle = new Bundle();
                collabBundle.putString(AppConstants.ClassConstants.VIDEO_FILE_PATH, mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).getFinalVideo());
                collabBundle.putInt(AppConstants.ClassConstants.TEMPLETE_SIZE, templateSize);
                collabBundle.putString(AppConstants.ClassConstants.VIDEO_FILE_URI, String.valueOf(Uri.parse(mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).getFinalVideo())));
                collabBundle.putString(AppConstants.ClassConstants.VIDEO_LENGTH, mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).getVideoLength());
                collabBundle.putInt(AppConstants.ClassConstants.FRAME_SIZE, frameWidth);
                if (mCollabBarBeans.size() > 0) {
                    collabBundle.putParcelableArrayList(AppConstants.ClassConstants.COLOR_BAR_DATA, mCollabBarBeans);
                }
                collabBundle.putLong(AppConstants.ClassConstants.DURATION_IN_MILLIES, Long.parseLong(mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).getVideoLength()));
                collabBundle.putParcelableArrayList(AppConstants.ClassConstants.FRAMES_LIST, mVideoFrameBeanList);
                collabBarFragment.setArguments(collabBundle);
                collabBarFragment.setCancelable(false);
                collabBarFragment.show(getSupportFragmentManager(), CollabBarFragment.class.getCanonicalName());
                break;
            case R.id.iv_save_recording:
                pauseExoplayer();
                releaseExoplayer();
                Intent intent = new Intent(NewVideoPreviewActivity.this, SelfCollabRecordActivity.class);
                intent.putExtra(AppConstants.ClassConstants.VIDEO_DATA, mSelfCollabVideoDataBeanArrayList);
                intent.putExtra(AppConstants.ClassConstants.TEMPLETE_SIZE, templateSize);
                intent.putExtra(AppConstants.ClassConstants.MUSIC_ID, musicId);
                intent.putExtra(AppConstants.ClassConstants.FRAMES, mSelfCollabVideoDataBeanArrayList.size() + 1);
                intent.putExtra(AppConstants.ClassConstants.COLOR_BAR_DATA, mCollabBarBeans);
                mVideoFrameBeanList.clear();
                startActivity(intent);
                break;
        }
    }

    private void getFramesFromVideo() {
        try {
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            try {
                retriever.setDataSource(this, Uri.fromFile(new File(mSelfCollabVideoDataBeanArrayList.get(mSelfCollabVideoDataBeanArrayList.size() - 1).getFinalVideo())));
            } catch (Exception e) {
                System.out.println("Exception= " + e);
            }
            mVideoFrameBeanList.clear();
            String duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            if (duration != null) {
                durationInMillies = Long.parseLong(duration); //duration in millisec
                int numberOfFrames = getNumberOfFramesOnScreen();
                frameWidth = getWindowManager().getDefaultDisplay().getWidth() / numberOfFrames;
                int timeInterval = (int) (durationInMillies / numberOfFrames);
                for (int i = 1; i <= numberOfFrames; i++) {
                    VideoFramesBean videoFramesBean = new VideoFramesBean();
                    videoFramesBean.setFrame(retriever.getFrameAtTime((long) (timeInterval * i * 1000000), MediaMetadataRetriever.OPTION_CLOSEST_SYNC));
                    mVideoFrameBeanList.add(videoFramesBean);
                }
            }
            retriever.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
        setView();
    }

    private int getNumberOfFramesOnScreen() {
        int number = 1;
        int screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        for (int i = 60; i < screenWidth; i++) {
            if (screenWidth % i == 0) {
                number = i;
                break;
            }
        }
        return screenWidth / (number * 2);
    }

    @Override
    public void onSendTimeRange(ArrayList<CollabBarBean> collabBarBeanArrayList) {
        mCollabBarBeans = collabBarBeanArrayList;
        setIndeteminateProgressBar(mCollabBarBeans);
    }

    private void setIndeteminateProgressBar(ArrayList<CollabBarBean> mCollabBarBeans){
        mActivityNewVideoPreviewBinding.rlColorBar.removeAllViews();
        int screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mActivityNewVideoPreviewBinding.rlColorBar.getLayoutParams();
        for (int i = 0; i < mCollabBarBeans.size(); i++) {
            View view = new View(this);
            float start = (mCollabBarBeans.get(i).getStartTime() * screenWidth) / durationInMillies;
            float end = (mCollabBarBeans.get(i).getEndTime() * screenWidth) / durationInMillies;
            float width = end - start;
            view.setBackgroundColor(Color.parseColor(mCollabBarBeans.get(i).getColorCode()));
            RelativeLayout.LayoutParams viewLayoutParams = null;
            switch (mCollabBarBeans.get(i).getCode()) {
                case "0":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(20 / (templateSize + 1), this));
                    break;
                case "1":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(2 * (20 / (templateSize + 1)), this));
                    break;
                case "2":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(3 * (20 / (templateSize + 1)), this));
                    break;
                case "3":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(4 * (20 / (templateSize + 1)), this));
                    break;
                case "4":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(5 * (20 / (templateSize + 1)), this));
                    break;
                case "5":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY((float) dpToPx(6 * (20 / (templateSize + 1)), this));
                    break;
                case "6":
                    viewLayoutParams = new RelativeLayout.LayoutParams((int) width, dpToPx((20 / (templateSize + 1)), this));
                    view.setY(0);
                    break;
            }
            view.setX(start);
            view.setLayoutParams(viewLayoutParams);
            mActivityNewVideoPreviewBinding.rlColorBar.addView(view);
        }
        mActivityNewVideoPreviewBinding.rlColorBar.setLayoutParams(layoutParams);
    }

    private int dpToPx(float dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    private void pauseExoplayer(){
        if(mExoplayerOne!=null)
            mExoplayerOne.setPlayWhenReady(false);
        if(mExoplayerTwo!=null)
            mExoplayerTwo.setPlayWhenReady(false);
        if(mExoplayerThree!=null)
            mExoplayerThree.setPlayWhenReady(false);
        if(mExoplayerFour!=null)
            mExoplayerFour.setPlayWhenReady(false);
        if(mExoplayerFive!=null)
            mExoplayerFive.setPlayWhenReady(false);
        if(mExoplayerSix!=null)
            mExoplayerSix.setPlayWhenReady(false);
    }

    private void resumeExoplayer(){
        if(mExoplayerOne!=null)
            mExoplayerOne.setPlayWhenReady(true);
        if(mExoplayerTwo!=null)
            mExoplayerTwo.setPlayWhenReady(true);
        if(mExoplayerThree!=null)
            mExoplayerThree.setPlayWhenReady(true);
        if(mExoplayerFour!=null)
            mExoplayerFour.setPlayWhenReady(true);
        if(mExoplayerFive!=null)
            mExoplayerFive.setPlayWhenReady(true);
        if(mExoplayerSix!=null)
            mExoplayerSix.setPlayWhenReady(true);
    }

    private void releaseExoplayer(){
        if(mExoplayerOne!=null) {
            mExoplayerOne.release();
            mExoplayerOne = null;
        }
        if(mExoplayerTwo!=null) {
            mExoplayerTwo.release();
            mExoplayerTwo = null;
        }
        if(mExoplayerThree!=null) {
            mExoplayerThree.release();
            mExoplayerThree = null;
        }
        if(mExoplayerFour!=null) {
            mExoplayerFour.release();
            mExoplayerFour = null;
        }
        if(mExoplayerFive!=null) {
            mExoplayerFive.release();
            mExoplayerFive = null;
        }
        if(mExoplayerSix!=null) {
            mExoplayerSix.release();
            mExoplayerSix = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        pauseExoplayer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        resumeExoplayer();
    }

    @Override
    protected void onStop() {
        super.onStop();
        releaseExoplayer();
    }

    @Override
    public void onBackPressed() {
        discardRecording();
    }

    private void discardRecording() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.txt_discard_collab)
                .setPositiveButton(android.R.string.ok, (dialogInterface, i) -> discard())
                .setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss())
                .create().show();
    }

    private void discard(){
        finish();
    }
}
