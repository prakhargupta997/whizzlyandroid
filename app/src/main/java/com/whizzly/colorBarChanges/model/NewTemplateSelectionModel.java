package com.whizzly.colorBarChanges.model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;

import com.whizzly.BR;

public class NewTemplateSelectionModel  extends BaseObservable {
    public ObservableBoolean isTwoClicked = new ObservableBoolean(false);
    public ObservableBoolean isThreeClicked = new ObservableBoolean(false);
    public ObservableBoolean isFourClicked = new ObservableBoolean(false);
    public ObservableBoolean isFiveClicked = new ObservableBoolean(false);
    public ObservableBoolean isSixClicked = new ObservableBoolean(false);

    @Bindable
    public boolean getIsTwoClicked() {
        return isTwoClicked.get();
    }

    public void setIsTwoClicked(boolean isTwoClicked) {
        this.isTwoClicked.set(isTwoClicked);
        notifyPropertyChanged(BR.isTwoClicked);
    }

    @Bindable
    public boolean getIsThreeClicked() {
        return isThreeClicked.get();
    }

    public void setIsThreeClicked(boolean isThreeClicked) {
        this.isThreeClicked.set(isThreeClicked);
        notifyPropertyChanged(BR.isThreeClicked);
    }

    @Bindable
    public boolean getIsFourClicked() {
        return isFourClicked.get();
    }

    public void setIsFourClicked(boolean isFourClicked) {
        this.isFourClicked.set(isFourClicked);
        notifyPropertyChanged(BR.isFourClicked);
    }

    @Bindable
    public boolean getIsFiveClicked() {
        return isFiveClicked.get();
    }

    public void setIsFiveClicked(boolean isFiveClicked) {
        this.isFiveClicked.set(isFiveClicked);
        notifyPropertyChanged(BR.isFiveClicked);
    }

    @Bindable
    public boolean getIsSixClicked() {
        return isSixClicked.get();
    }

    public void setIsSixClicked(boolean isSixClicked) {
        this.isSixClicked.set(isSixClicked);
        notifyPropertyChanged(BR.isSixClicked);
    }

}
