package com.whizzly.arguments.view;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.whizzly.R;
import com.whizzly.arguments.viewmodels.ArgumentPreviewViewModel;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.databinding.ActivityArgumentPreviewBinding;
import com.whizzly.interfaces.OnBackPressedListener;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.FFMpegCommandListener;
import com.whizzly.utils.FFMpegCommands;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class ArgumentPreviewActivity extends BaseActivity implements OnClickSendListener {

    private ActivityArgumentPreviewBinding mActivityArgumentPreviewBinding;
    private ArrayList<String> mArgumentList = new ArrayList<>();
    private String duration = "";
    private OnBackPressedListener onBackPressedListener;
    private ExoPlayer mExoPlayer;
    private String imageFile;
    private FFMpegCommands ffMpegCommands;
    private ArgumentPreviewViewModel argumentPreviewViewModel;
    private PostArgumentBottomSheetFragment postArgumentBottomSheetFragment;
    private String mFilePath = "";

    public void clearStatusBar() {
        Window w = getWindow();
        w.getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityArgumentPreviewBinding = DataBindingUtil.setContentView(this, R.layout.activity_argument_preview);
        argumentPreviewViewModel = ViewModelProviders.of(this).get(ArgumentPreviewViewModel.class);
        mActivityArgumentPreviewBinding.setViewModel(argumentPreviewViewModel);
        argumentPreviewViewModel.setOnClickSendListener(this);
        ffMpegCommands = FFMpegCommands.getInstance(this);
        getData();
        clearStatusBar();
        try {
            ffMpegCommands.initializeFFmpeg();
        } catch (FFmpegNotSupportedException e) {
            e.printStackTrace();
        }
        if (mArgumentList.size() <= 1) {
            setExoplayer(mArgumentList.get(0));
        }
        postArgumentBottomSheetFragment = new PostArgumentBottomSheetFragment();
    }

    @Override
    public void onBackPressed() {
        if (onBackPressedListener != null) {
            onBackPressedListener.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    private String getConcatFile() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "concat_" + System.currentTimeMillis() + ".mp4";
    }

    private String addVideosPathToTextFile(ArrayList<String> videoPathList) {
        final File dir = getExternalFilesDir(null);
        String textFile = (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "concat_text_" + System.currentTimeMillis() + ".txt";
        File file = new File(textFile);
        try {
            FileOutputStream f = new FileOutputStream(file);
            PrintWriter pw = new PrintWriter(f);
            for (int i = 0; i < videoPathList.size(); i++) {
                pw.println("file " + videoPathList.get(i));
            }
            pw.flush();
            pw.close();
            f.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return textFile;
    }

    private void concatVideos(String textFile) {
        mFilePath = getConcatFile();
        ffMpegCommands.concatenateVideos(textFile, mFilePath, new FFMpegCommandListener() {
            @Override
            public void onSuccess(String message) {
                setExoplayer(mFilePath);
            }

            @Override
            public void onProgress(String message) {

            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onStart() {

            }

            @Override
            public void onFinish() {

            }
        });
    }


    @Override
    protected void onDestroy() {
        onBackPressedListener = null;
        super.onDestroy();
    }

    private void setExoplayer(String mFilePath) {
        if (mExoPlayer == null) {
            mExoPlayer = ExoPlayerFactory.newSimpleInstance(this,
                    new DefaultRenderersFactory(this),
                    new DefaultTrackSelector(), new DefaultLoadControl());
            mExoPlayer.setPlayWhenReady(true);
            mExoPlayer.setRepeatMode(Player.REPEAT_MODE_ONE);
            Uri uri = Uri.parse(mFilePath);
            MediaSource mediaSource = buildMediaSource(uri);
            mExoPlayer.prepare(mediaSource, true, false);
            mActivityArgumentPreviewBinding.pvPreview.setPlayer(mExoPlayer);
        }
    }

    private MediaSource buildMediaSource(Uri uri) {
        return new ExtractorMediaSource.Factory(
                new DefaultDataSourceFactory(this, "whizzly-app")).
                createMediaSource(uri);
    }

    private void getData() {
        Intent intent = getIntent();
        mArgumentList = (ArrayList<String>) intent.getSerializableExtra(AppConstants.ClassConstants.ARGUMENT_FILE_PATH);
        duration = intent.getStringExtra(AppConstants.ClassConstants.ARGUMENT_DURATION);
        if (mArgumentList.size() > 1) {
            String textFile = addVideosPathToTextFile(mArgumentList);
            concatVideos(textFile);
        }
        else {
            mFilePath = mArgumentList.get(0);
        }
        extractFrame();
    }


    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_ARGUMENT_PREVIEW_CLOSE:
                finish();
                break;
            case AppConstants.ClassConstants.ON_ARGUMENT_PREVIEW_POST:
                Bundle postArgumentBundle = new Bundle();
                postArgumentBundle.putString(AppConstants.ClassConstants.FRAME, imageFile);
                postArgumentBundle.putString(AppConstants.ClassConstants.ARGUMENT_FILE_PATH, mFilePath);
                postArgumentBundle.putString(AppConstants.ClassConstants.ARGUMENT_DURATION, duration);
                postArgumentBottomSheetFragment.setArguments(postArgumentBundle);
                postArgumentBottomSheetFragment.show(getSupportFragmentManager(), PostArgumentBottomSheetFragment.class.getCanonicalName());
                break;
        }
    }

    private void extractFrame() {
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(mArgumentList.get(0));
        Bitmap bitmap = mediaMetadataRetriever.getFrameAtTime(0);
        saveBitmap(bitmap);
        mediaMetadataRetriever.release();
    }

    private String getImageDirectory() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "image_" + System.currentTimeMillis() + ".jpeg";
    }

    private void saveBitmap(Bitmap bitmap) {
        imageFile = getImageDirectory();
        try (FileOutputStream out = new FileOutputStream(imageFile)) {
            bitmap.compress(Bitmap.CompressFormat.PNG, 60, out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mExoPlayer != null) {
            mExoPlayer.setPlayWhenReady(false);
            mExoPlayer.release();
            mExoPlayer = null;
        }
    }

    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }
}
