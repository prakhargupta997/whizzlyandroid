package com.whizzly.arguments.view;

import android.app.Activity;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.whizzly.R;
import com.whizzly.arguments.models.ArgumentDataBean;
import com.whizzly.arguments.viewmodels.PostArgumentBottomSheetViewModel;
import com.whizzly.databinding.PostArgumentBottomSheetFragmentBinding;
import com.whizzly.home_screen_module.home_screen_activities.HomeActivity;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.AppUtils;

import java.io.File;
import java.util.Objects;

public class PostArgumentBottomSheetFragment extends BottomSheetDialogFragment implements OnClickSendListener {

    private PostArgumentBottomSheetViewModel mViewModel;
    private PostArgumentBottomSheetFragmentBinding mPostArgumentBottomSheetFragmentBinding;
    private String duration, thumbnailUrl, videoFileUrl;

    public static PostArgumentBottomSheetFragment newInstance() {
        return new PostArgumentBottomSheetFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mPostArgumentBottomSheetFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.post_argument_bottom_sheet_fragment, container, false);
        mViewModel = ViewModelProviders.of(this).get(PostArgumentBottomSheetViewModel.class);
        mPostArgumentBottomSheetFragmentBinding.setViewModel(mViewModel);
        mViewModel.setOnClickSendListener(this);
        getData();
        return mPostArgumentBottomSheetFragmentBinding.getRoot();
    }

    private void getData() {
        Bundle bundle = getArguments();
        duration = bundle.getString(AppConstants.ClassConstants.ARGUMENT_DURATION);
        thumbnailUrl = bundle.getString(AppConstants.ClassConstants.FRAME);
        videoFileUrl = bundle.getString(AppConstants.ClassConstants.ARGUMENT_FILE_PATH);
        if (!TextUtils.isEmpty(thumbnailUrl))
            setView();

        mPostArgumentBottomSheetFragmentBinding.etHashtags.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b)
                    hideKeyboard(view);
                else {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(mPostArgumentBottomSheetFragmentBinding.etHashtags, InputMethodManager.SHOW_IMPLICIT);
                    String text = mPostArgumentBottomSheetFragmentBinding.etHashtags.getText().toString();
                    if (text.trim().length() != 0) {
                        char last = text.trim().charAt(text.length() - 1);
                        if (last != '#')
                            mPostArgumentBottomSheetFragmentBinding.etHashtags.setText(text + " #");
                    } else
                        mPostArgumentBottomSheetFragmentBinding.etHashtags.setText("#");

                    mPostArgumentBottomSheetFragmentBinding.etHashtags.setSelection(mPostArgumentBottomSheetFragmentBinding.etHashtags.getText().toString().length());
                }
            }
        });

        mPostArgumentBottomSheetFragmentBinding.etWriteCaption.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b)
                    hideKeyboard(view);
            }
        });

        mPostArgumentBottomSheetFragmentBinding.etHashtags.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (s.length()>1) {
                    char last = s.charAt(s.length() - 1);
                    if (last == ' ' || last == '\n') {
                        mPostArgumentBottomSheetFragmentBinding.etHashtags.setText(String.format("%s#", s));
                        mPostArgumentBottomSheetFragmentBinding.etHashtags.setSelection(mPostArgumentBottomSheetFragmentBinding.etHashtags.getText().toString().length());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void setView() {
        File file = new File(thumbnailUrl);
        Glide.with(this).load(Uri.fromFile(file)).into(mPostArgumentBottomSheetFragmentBinding.ivVideoFrame);
    }
    private ArgumentDataBean argumentData  = new ArgumentDataBean();

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants
                    .ClassConstants.POST_ARGUMENT:
                String description = mPostArgumentBottomSheetFragmentBinding.etWriteCaption.getText().toString();
                String hashtags = mPostArgumentBottomSheetFragmentBinding.etHashtags.getText().toString();
                /*if (!hashtags.isEmpty()&&!(hashtags.equals("#"))) {*/
                    if(AppUtils.isNetworkAvailable(getActivity())) {
                        argumentData.setDuration(duration);
                        argumentData.setHashtag(hashtags);
                        argumentData.setThumbnailPath(thumbnailUrl);
                        argumentData.setVideoPath(videoFileUrl);
                        argumentData.setTitle(description);
                        argumentData.setUserId(String.valueOf(mViewModel.getUserId()));
                        Intent intent = new Intent(getActivity(), HomeActivity.class);
                        intent.putExtra(AppConstants.ClassConstants.ARGUMENT_DATA, argumentData);
                        startActivity(intent);
                        dismiss();
                        Objects.requireNonNull(getActivity()).finish();
                    }else{
                        Toast.makeText(getActivity(),getActivity().getString(R.string.txt_no_internet), Toast.LENGTH_SHORT).show();
                    }
              /*  } else {
                    Toast.makeText(getActivity(), R.string.txt_error_no_hashtag, Toast.LENGTH_SHORT).show();
                }*/
                break;

            case AppConstants.ClassConstants.POST_VIDEO_FACEBOOK_CLICKED:
                if (mPostArgumentBottomSheetFragmentBinding.ivShareFacebook.isSelected()) {
                    mPostArgumentBottomSheetFragmentBinding.ivShareFacebook.setSelected(false);
                    argumentData.setClickedSocailMedia(0);
                } else {
                    mPostArgumentBottomSheetFragmentBinding.ivShareFacebook.setSelected(true);
                    mPostArgumentBottomSheetFragmentBinding.ivShareInstagram.setSelected(false);
                    argumentData.setClickedSocailMedia(AppConstants.ClassConstants.SHARE_ON_FACEBOOK);
                }
                break;
            case AppConstants.ClassConstants.POST_VIDEO_INSTAGRAM_CLICKED:
                if (mPostArgumentBottomSheetFragmentBinding.ivShareInstagram.isSelected()) {
                    mPostArgumentBottomSheetFragmentBinding.ivShareInstagram.setSelected(false);
                    argumentData.setClickedSocailMedia(0);
                } else {
                    mPostArgumentBottomSheetFragmentBinding.ivShareInstagram.setSelected(true);
                    mPostArgumentBottomSheetFragmentBinding.ivShareFacebook.setSelected(false);
                    argumentData.setClickedSocailMedia(AppConstants.ClassConstants.SHARE_ON_INSTAGRAM);
                }
                break;
        }
    }
}
