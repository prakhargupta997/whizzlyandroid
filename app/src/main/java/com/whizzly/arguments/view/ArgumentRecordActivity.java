package com.whizzly.arguments.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.opengl.GLException;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.daasuu.camerarecorder.CameraRecordListener;
import com.daasuu.camerarecorder.CameraRecorder;
import com.daasuu.camerarecorder.CameraRecorderBuilder;
import com.daasuu.camerarecorder.LensFacing;
import com.whizzly.R;
import com.whizzly.arguments.models.ArgumentRecordActivityModel;
import com.whizzly.arguments.viewmodels.ArgumentRecordActivityViewModel;
import com.whizzly.base_classes.BaseActivity;
import com.whizzly.camera_utils.widget.Filters;
import com.whizzly.camera_utils.widget.SampleGLView;
import com.whizzly.databinding.ActivityArgumentRecordBinding;
import com.whizzly.dialog.ErrorDialog;
import com.whizzly.interfaces.OnBackPressedListener;
import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.opengles.GL10;

public class ArgumentRecordActivity extends BaseActivity implements OnClickSendListener {

    public static final Integer MAX_VIDEO_LENGTH = 45000;
    protected CameraRecorder cameraRecorder;
    protected LensFacing lensFacing = LensFacing.FRONT;
    protected int cameraWidth = 1280;
    protected int cameraHeight = 720;
    protected int videoWidth = 720;
    protected int videoHeight = 1280;
    private SampleGLView sampleGLView;
    private ActivityArgumentRecordBinding mActivityArgumentRecordBinding;
    private ArgumentRecordActivityModel mArgumentRecordActivityModel;
    private ArgumentRecordActivityViewModel mArgumentRecordActivityViewModel;
    //    private AutoFitTextureView mTextureView;
    private boolean mIsRecordingVideo;
    private OnBackPressedListener onBackPressedListener;
    private String mNextVideoAbsolutePath;
    private boolean isFirstTime = false;
    private Timer timer = new Timer();
    private int recordingSecs = 0;
    private boolean longClickActive;
    private long startTime;
    private ArrayList<String> mArgumentList;
    private boolean isFlashActive, isFlipCamera;
    private Handler mTimerHandler;
    private Runnable mTimerRunnable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        mActivityArgumentRecordBinding = DataBindingUtil.setContentView(this, R.layout.activity_argument_record);
        mArgumentRecordActivityModel = new ArgumentRecordActivityModel();
        mActivityArgumentRecordBinding.setModel(mArgumentRecordActivityModel);
        mArgumentRecordActivityViewModel = ViewModelProviders.of(this).get(ArgumentRecordActivityViewModel.class);
        mActivityArgumentRecordBinding.setViewModel(mArgumentRecordActivityViewModel);
        mArgumentRecordActivityViewModel.setOnClickSendListener(this);
        clearStatusBar();
        setOnTouchListener();
        mArgumentList = new ArrayList<>();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setOnTouchListener() {
        try {
            mActivityArgumentRecordBinding.ivPushToRecord.setOnTouchListener((View view, MotionEvent motionEvent) -> {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        if (!longClickActive) {
                            longClickActive = true;
                            startTime = Calendar.getInstance().getTimeInMillis();
                        }
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        if (longClickActive) {
                            long clickDuration = Calendar.getInstance().getTimeInMillis() - startTime;
                            if (clickDuration > 600) {
                                longClickActive = false;
                                if (!mActivityArgumentRecordBinding.tvRecordingSecond.getText().toString().contains("45")) {
                                    if (!mIsRecordingVideo) {
                                        setCamera();
                                        mIsRecordingVideo = true;
                                        mArgumentRecordActivityModel.setIsVideoStarted(true);
                                        mArgumentRecordActivityModel.setIsRecordPressed(true);
                                        startProgress();
                                        cameraRecorder.start(mNextVideoAbsolutePath);
                                    }
                                }
                            }
                        }
                        break;
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP:
                        if (mIsRecordingVideo && !longClickActive) {
                            mArgumentRecordActivityModel.setIsRecordPressed(false);
                            mArgumentRecordActivityModel.setIsVideoStarted(false);
                            mIsRecordingVideo = false;
                            mArgumentList.add(mNextVideoAbsolutePath);
                            cameraRecorder.stop();
                        }
                        break;
                }
                return true;
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getArgumentFile() {
        final File dir = getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + "argument_" + System.currentTimeMillis() + ".mp4";
    }

    private void startProgress() {
        mTimerHandler = new Handler();
        mTimerRunnable = new Runnable() {
            @Override
            public void run() {
                if (mIsRecordingVideo) {
                    if (recordingSecs >= 46) {
                        cameraRecorder.stop();
                        mArgumentRecordActivityModel.setIsVideoStarted(false);
                        mArgumentRecordActivityModel.setIsRecordPressed(false);
                        mTimerHandler.removeCallbacksAndMessages(null);
                        mIsRecordingVideo = false;
                    } else {
                        mActivityArgumentRecordBinding.tvRecordingSecond.setText(recordingSecs + " s");
                        recordingSecs++;
                        mTimerHandler.postDelayed(mTimerRunnable, 1000);
                    }
                }
            }
        };
        mTimerHandler.post(mTimerRunnable);
    }

    private void setCamera() {
        mNextVideoAbsolutePath = getArgumentFile();
    }

    @Override
    public void onClickSend(int code) {
        switch (code) {
            case AppConstants.ClassConstants.ON_ARGUMENT_FLASH_CLICKED:
                toggleFlash();
                break;
            case AppConstants.ClassConstants.ON_ARGUMENT_FLIP_CAMERA:
                toggleCamera();
                break;
            case AppConstants.ClassConstants.ON_CLOSE_VIDEO_RECORDING:
                this.finish();
                break;
            case AppConstants.ClassConstants.ON_ARGUMENT_SAVE_RECORDING:
                String time = mActivityArgumentRecordBinding.tvRecordingSecond.getText().toString().split(" ")[0];
                if (Integer.parseInt(time) >= 5) {
                    mArgumentRecordActivityModel.setIsVideoStarted(false);
                    mArgumentRecordActivityModel.setIsRecordPressed(false);
                    cameraRecorder.stop();
                    Intent intent = new Intent(this, ArgumentPreviewActivity.class);
                    intent.putExtra(AppConstants.ClassConstants.ARGUMENT_FILE_PATH, mArgumentList);
                    intent.putExtra(AppConstants.ClassConstants.ARGUMENT_DURATION, String.valueOf(recordingSecs));
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(this, getString(R.string.txt_error_min_length), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void toggleFlash() {
        if (mArgumentRecordActivityModel.getIsFlashClicked()) {
            if (cameraRecorder != null && cameraRecorder.isFlashSupport()) {
                cameraRecorder.switchFlashMode();
                cameraRecorder.changeAutoFocus();
            }
            mArgumentRecordActivityModel.setIsFlashClicked(false);
        } else {
            if (cameraRecorder != null && cameraRecorder.isFlashSupport()) {
                cameraRecorder.switchFlashMode();
                cameraRecorder.changeAutoFocus();
            }
            mArgumentRecordActivityModel.setIsFlashClicked(true);
        }
    }

    @Override
    public void onBackPressed() {
        if (onBackPressedListener != null) {
            onBackPressedListener.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    private void toggleCamera() {
        releaseCamera();
        if (lensFacing == LensFacing.BACK) {
            lensFacing = LensFacing.FRONT;
        } else {
            lensFacing = LensFacing.BACK;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(checkPermissionsForCamera(this)) {
            setUpCamera();
        }
    }


    public boolean checkPermissionsForCamera(Activity context) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.CAMERA)
                        || ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        || ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.RECORD_AUDIO)) {
                    ActivityCompat.requestPermissions((context), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO}, AppConstants.ClassConstants.VIDEO_INTENT_REQUEST_CODE);
                } else {
                    if (!isFirstTime) {
                        ActivityCompat.requestPermissions((context), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO}, AppConstants.ClassConstants.VIDEO_INTENT_REQUEST_CODE);
                        isFirstTime = true;
                    } else {
                        com.whizzly.dialog.ErrorDialog errorDialog = new com.whizzly.dialog.ErrorDialog();
                        if (context instanceof ArgumentRecordActivity)
                            errorDialog.show(((ArgumentRecordActivity) context).getSupportFragmentManager(), com.whizzly.dialog.ErrorDialog.class.getCanonicalName());
                    }
                }
                return false;
            } else
                return true;
        } else
            return true;
    }

        private void releaseCamera() {
            if (sampleGLView != null) {
                sampleGLView.onPause();
            }

            if (cameraRecorder != null) {
                cameraRecorder.stop();
                cameraRecorder.release();
                cameraRecorder = null;
            }

            if (sampleGLView != null) {
                mActivityArgumentRecordBinding.camera.removeView(sampleGLView);
                sampleGLView = null;
            }
        }


    private void setUpCameraView() {
        runOnUiThread(() -> {
            FrameLayout frameLayout = mActivityArgumentRecordBinding.camera;
            frameLayout.removeAllViews();
            sampleGLView = null;
            sampleGLView = new SampleGLView(getApplicationContext());
            sampleGLView.setTouchListener((event, width, height) -> {
                if (cameraRecorder == null) return;
                cameraRecorder.changeManualFocusPoint(event.getX(), event.getY(), width, height);
            });
            frameLayout.addView(sampleGLView);
        });
    }

    private void setUpCamera() {
        setUpCameraView();

        cameraRecorder = new CameraRecorderBuilder(this, sampleGLView)
                .cameraRecordListener(new CameraRecordListener() {
                    @Override
                    public void onGetFlashSupport(boolean flashSupport) {

                    }

                    @Override
                    public void onRecordComplete() {
                        mIsRecordingVideo = false;
                        mArgumentRecordActivityModel.setIsVideoStarted(false);
                        mArgumentRecordActivityModel.setIsRecordPressed(false);
                        mNextVideoAbsolutePath = null;
                    }

                    @Override
                    public void onRecordStart() {

                    }

                    @Override
                    public void onError(Exception exception) {
                        Log.e("CameraRecorder", exception.toString());
                    }

                    @Override
                    public void onCameraThreadFinish() {
                        runOnUiThread(() -> {
                            setUpCamera();
                        });
                    }
                })
                .videoSize(videoWidth, videoHeight)
                .cameraSize(cameraWidth, cameraHeight)
                .lensFacing(lensFacing)
                .build();
    }


    @Override
    protected void onPause() {
        super.onPause();
        releaseCamera();
    }

    @Override
    protected void onDestroy() {
        onBackPressedListener = null;
        super.onDestroy();
    }

    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }

}