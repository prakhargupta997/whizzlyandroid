package com.whizzly.arguments.viewmodels;

import androidx.lifecycle.ViewModel;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;
import com.whizzly.utils.DataManager;

public class PostArgumentBottomSheetViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;

    public void setOnClickSendListener(OnClickSendListener onClickSendListener){
        this.onClickSendListener = onClickSendListener;
    }

    public void onSaveAsArgument(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.POST_ARGUMENT);
    }

    public int getUserId(){
        return DataManager.getInstance().getUserId();
    }

    public void onFacebookClicked(){
        onClickSendListener.onClickSend(AppConstants
                .ClassConstants.POST_VIDEO_FACEBOOK_CLICKED);
    }

    public void onInstagramClicked(){
        onClickSendListener.onClickSend(AppConstants
                .ClassConstants.POST_VIDEO_INSTAGRAM_CLICKED);
    }}
