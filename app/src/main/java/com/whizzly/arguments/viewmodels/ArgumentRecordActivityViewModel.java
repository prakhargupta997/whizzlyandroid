package com.whizzly.arguments.viewmodels;

import androidx.lifecycle.ViewModel;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class ArgumentRecordActivityViewModel extends ViewModel {

    private OnClickSendListener onClickSendListener;

    public void onCloseClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_CLOSE_VIDEO_RECORDING);
    }

    public void onFlipClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_ARGUMENT_FLIP_CAMERA);
    }

    public void onFlashClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_ARGUMENT_FLASH_CLICKED);
    }

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void saveRecording(){
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_ARGUMENT_SAVE_RECORDING);
    }
}

