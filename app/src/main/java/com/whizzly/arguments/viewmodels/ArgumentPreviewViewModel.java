package com.whizzly.arguments.viewmodels;

import androidx.lifecycle.ViewModel;

import com.whizzly.interfaces.OnClickSendListener;
import com.whizzly.utils.AppConstants;

public class ArgumentPreviewViewModel extends ViewModel {
    private OnClickSendListener onClickSendListener;

    public void setOnClickSendListener(OnClickSendListener onClickSendListener) {
        this.onClickSendListener = onClickSendListener;
    }

    public void onCloseClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_ARGUMENT_PREVIEW_CLOSE);
    }

    public void onPostClicked() {
        onClickSendListener.onClickSend(AppConstants.ClassConstants.ON_ARGUMENT_PREVIEW_POST);
    }
}
