package com.whizzly.arguments.models;

import java.io.Serializable;

public class ArgumentDataBean implements Serializable {
    private String duration;
    private String thumbnailPath;
    private String videoPath;
    private String hashtag;
    private String title;
    private String userId;
    private int clickedSocailMedia;

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getThumbnailPath() {
        return thumbnailPath;
    }

    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getClickedSocailMedia() {
        return clickedSocailMedia;
    }

    public void setClickedSocailMedia(int clickedSocailMedia) {
        this.clickedSocailMedia = clickedSocailMedia;
    }
}
