
package com.whizzly.models.privacy_status;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("is_locked")
    @Expose
    private Integer isLocked;

    public Integer getVideoId() {
        return videoId;
    }

    public void setVideoId(Integer videoId) {
        this.videoId = videoId;
    }

    @SerializedName("video_id")
    @Expose
    private Integer videoId;

    public Integer getIsLocked() {
        return isLocked;
    }

    public void setIsLocked(Integer isLocked)
    {
        this.isLocked = isLocked;
    }

}
