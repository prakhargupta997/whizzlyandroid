package com.whizzly.models.social_login_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("access-token")
    @Expose
    private String accessToken;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("is_active")
    @Expose
    private int isActive;
    @SerializedName("is_email_verified")
    @Expose
    private int isEmailVerified;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("profile_setup")
    @Expose
    private int profileSetup;
    @SerializedName("registered_on")
    @Expose
    private String registeredOn;
    @SerializedName("signup_type")
    @Expose
    private int signupType;
    @SerializedName("social_type")
    @Expose
    private String socialType;
    @SerializedName("user_email")
    @Expose
    private String userEmail;
    @SerializedName("user_facebook_id")
    @Expose
    private String userFacebookId;
    @SerializedName("user_google_id")
    @Expose
    private String userGoogleId;
    @SerializedName("user_id")
    @Expose
    private int userId;
    @SerializedName("user_instagram_id")
    @Expose
    private String userInstagramId;
    @SerializedName("user_name")
    @Expose
    private String userName;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getIsEmailVerified() {
        return isEmailVerified;
    }

    public void setIsEmailVerified(int isEmailVerified) {
        this.isEmailVerified = isEmailVerified;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public int getProfileSetup() {
        return profileSetup;
    }

    public void setProfileSetup(int profileSetup) {
        this.profileSetup = profileSetup;
    }

    public String getRegisteredOn() {
        return registeredOn;
    }

    public void setRegisteredOn(String registeredOn) {
        this.registeredOn = registeredOn;
    }

    public int getSignupType() {
        return signupType;
    }

    public void setSignupType(int signupType) {
        this.signupType = signupType;
    }

    public String getSocialType() {
        return socialType;
    }

    public void setSocialType(String socialType) {
        this.socialType = socialType;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserFacebookId() {
        return userFacebookId;
    }

    public void setUserFacebookId(String userFacebookId) {
        this.userFacebookId = userFacebookId;
    }

    public String getUserGoogleId() {
        return userGoogleId;
    }

    public void setUserGoogleId(String userGoogleId) {
        this.userGoogleId = userGoogleId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserInstagramId() {
        return userInstagramId;
    }

    public void setUserInstagramId(String userInstagramId) {
        this.userInstagramId = userInstagramId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}
