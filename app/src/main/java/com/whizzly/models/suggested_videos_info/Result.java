
package com.whizzly.models.suggested_videos_info;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result implements Parcelable {

    @SerializedName("additional_info")
    @Expose
    private String additionalInfo;
    @SerializedName("url")
    @Expose
    private String collabUrl;
    @SerializedName("comment_count")
    @Expose
    private Integer commentCount;
    @SerializedName("composer_name")
    @Expose
    private Object composerName;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("is_follower")
    @Expose
    private Object isFollower;
    @SerializedName("is_liked")
    @Expose
    private Object isLiked;
    @SerializedName("music_id")
    @Expose
    private Object musicId;
    @SerializedName("music_reuse_count")
    @Expose
    private Integer musicReuseCount;
    @SerializedName("music_thumbnail_url")
    @Expose
    private Object musicThumbnailUrl;
    @SerializedName("music_title")
    @Expose
    private Object musicTitle;
    @SerializedName("music_url")
    @Expose
    private String musicUrl;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("ranking")
    @Expose
    private Integer ranking;
    @SerializedName("reused_by")
    @Expose
    private Object reusedBy;
    @SerializedName("thumbnail_url")
    @Expose
    private String thumbnailUrl;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("total_likes")
    @Expose
    private Integer totalLikes;
    @SerializedName("total_reuse")
    @Expose
    private Integer totalReuse;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("video_hastags")
    @Expose
    private String videoHastags;
    @SerializedName("watermark_url")
    @Expose
    private String watermarkUrl;

    protected Result(Parcel in) {
        additionalInfo = in.readString();
        collabUrl = in.readString();
        if (in.readByte() == 0) {
            commentCount = null;
        } else {
            commentCount = in.readInt();
        }
        firstName = in.readString();
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            musicReuseCount = null;
        } else {
            musicReuseCount = in.readInt();
        }
        musicUrl = in.readString();
        profileImage = in.readString();
        if (in.readByte() == 0) {
            ranking = null;
        } else {
            ranking = in.readInt();
        }
        thumbnailUrl = in.readString();
        title = in.readString();
        if (in.readByte() == 0) {
            totalLikes = null;
        } else {
            totalLikes = in.readInt();
        }
        if (in.readByte() == 0) {
            totalReuse = null;
        } else {
            totalReuse = in.readInt();
        }
        type = in.readString();
        if (in.readByte() == 0) {
            userId = null;
        } else {
            userId = in.readInt();
        }
        userName = in.readString();
        videoHastags = in.readString();
        watermarkUrl = in.readString();
    }

    public static final Creator<Result> CREATOR = new Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel in) {
            return new Result(in);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getCollabUrl() {
        return collabUrl;
    }

    public void setCollabUrl(String collabUrl) {
        this.collabUrl = collabUrl;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public Object getComposerName() {
        return composerName;
    }

    public void setComposerName(Object composerName) {
        this.composerName = composerName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getIsFollower() {
        return isFollower;
    }

    public void setIsFollower(Object isFollower) {
        this.isFollower = isFollower;
    }

    public Object getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(Object isLiked) {
        this.isLiked = isLiked;
    }

    public Object getMusicId() {
        return musicId;
    }

    public void setMusicId(Object musicId) {
        this.musicId = musicId;
    }

    public Integer getMusicReuseCount() {
        return musicReuseCount;
    }

    public void setMusicReuseCount(Integer musicReuseCount) {
        this.musicReuseCount = musicReuseCount;
    }

    public Object getMusicThumbnailUrl() {
        return musicThumbnailUrl;
    }

    public void setMusicThumbnailUrl(Object musicThumbnailUrl) {
        this.musicThumbnailUrl = musicThumbnailUrl;
    }

    public Object getMusicTitle() {
        return musicTitle;
    }

    public void setMusicTitle(Object musicTitle) {
        this.musicTitle = musicTitle;
    }

    public String getMusicUrl() {
        return musicUrl;
    }

    public void setMusicUrl(String musicUrl) {
        this.musicUrl = musicUrl;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    public Object getReusedBy() {
        return reusedBy;
    }

    public void setReusedBy(Object reusedBy) {
        this.reusedBy = reusedBy;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(Integer totalLikes) {
        this.totalLikes = totalLikes;
    }

    public Integer getTotalReuse() {
        return totalReuse;
    }

    public void setTotalReuse(Integer totalReuse) {
        this.totalReuse = totalReuse;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getVideoHastags() {
        return videoHastags;
    }

    public void setVideoHastags(String videoHastags) {
        this.videoHastags = videoHastags;
    }

    public String getWatermarkUrl() {
        return watermarkUrl;
    }

    public void setWatermarkUrl(String watermarkUrl) {
        this.watermarkUrl = watermarkUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(additionalInfo);
        dest.writeString(collabUrl);
        if (commentCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(commentCount);
        }
        dest.writeString(firstName);
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        if (musicReuseCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(musicReuseCount);
        }
        dest.writeString(musicUrl);
        dest.writeString(profileImage);
        if (ranking == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(ranking);
        }
        dest.writeString(thumbnailUrl);
        dest.writeString(title);
        if (totalLikes == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(totalLikes);
        }
        if (totalReuse == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(totalReuse);
        }
        dest.writeString(type);
        if (userId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(userId);
        }
        dest.writeString(userName);
        dest.writeString(videoHastags);
        dest.writeString(watermarkUrl);
    }
}
