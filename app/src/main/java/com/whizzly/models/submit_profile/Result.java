
package com.whizzly.models.submit_profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {
    @SerializedName("date_of_birth")
    @Expose
    private Object dateOfBirth;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("is_profile_setup")
    @Expose
    private int profileSetup;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_name")
    @Expose
    private String userName;

    public Object getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Object dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public int getProfileSetup() {
        return profileSetup;
    }

    public void setProfileSetup(int profileSetup) {
        this.profileSetup = profileSetup;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
