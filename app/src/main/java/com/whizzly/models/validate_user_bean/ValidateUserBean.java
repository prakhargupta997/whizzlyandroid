package com.whizzly.models.validate_user_bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ValidateUserBean {

    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("message")
    @Expose
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
