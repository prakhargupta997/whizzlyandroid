
package com.whizzly.models.reuse_videos_chart;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReuseVideoChartResponse implements Parcelable {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("next_count")
    @Expose
    private Integer nextCount;
    @SerializedName("next_url")
    @Expose
    private String nextUrl;
    @SerializedName("result")
    @Expose
    private Result result;

    protected ReuseVideoChartResponse(Parcel in) {
        if (in.readByte() == 0) {
            code = null;
        } else {
            code = in.readInt();
        }
        message = in.readString();
        if (in.readByte() == 0) {
            nextCount = null;
        } else {
            nextCount = in.readInt();
        }
        nextUrl = in.readString();
        result = in.readParcelable(Result.class.getClassLoader());
    }

    public static final Creator<ReuseVideoChartResponse> CREATOR = new Creator<ReuseVideoChartResponse>() {
        @Override
        public ReuseVideoChartResponse createFromParcel(Parcel in) {
            return new ReuseVideoChartResponse(in);
        }

        @Override
        public ReuseVideoChartResponse[] newArray(int size) {
            return new ReuseVideoChartResponse[size];
        }
    };

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getNextCount() {
        return nextCount;
    }

    public void setNextCount(Integer nextCount) {
        this.nextCount = nextCount;
    }

    public String getNextUrl() {
        return nextUrl;
    }

    public void setNextUrl(String nextUrl) {
        this.nextUrl = nextUrl;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (code == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(code);
        }
        dest.writeString(message);
        if (nextCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(nextCount);
        }
        dest.writeString(nextUrl);
        dest.writeParcelable(result, flags);
    }
}
