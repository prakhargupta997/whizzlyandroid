
package com.whizzly.models.reuse_videos_chart;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result implements Parcelable {

    @SerializedName("video_info")
    @Expose
    private List<VideoInfo> videoInfo = null;

    protected Result(Parcel in) {
        videoInfo = in.createTypedArrayList(VideoInfo.CREATOR);
    }

    public static final Creator<Result> CREATOR = new Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel in) {
            return new Result(in);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };

    public List<VideoInfo> getVideoInfo() {
        return videoInfo;
    }

    public void setVideoInfo(List<VideoInfo> videoInfo) {
        this.videoInfo = videoInfo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(videoInfo);
    }
}
