
package com.whizzly.models.reuse_videos_chart;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoInfo implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("parent_collab_id")
    @Expose
    private Integer parentCollabId;
    @SerializedName("reuse_count")
    @Expose
    private Integer reuseCount;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("user_name")
    @Expose
    private String userName;

    protected VideoInfo(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            parentCollabId = null;
        } else {
            parentCollabId = in.readInt();
        }
        if (in.readByte() == 0) {
            reuseCount = null;
        } else {
            reuseCount = in.readInt();
        }
        title = in.readString();
        userName = in.readString();
        thumbnailUrl = in.readString();
    }

    public static final Creator<VideoInfo> CREATOR = new Creator<VideoInfo>() {
        @Override
        public VideoInfo createFromParcel(Parcel in) {
            return new VideoInfo(in);
        }

        @Override
        public VideoInfo[] newArray(int size) {
            return new VideoInfo[size];
        }
    };

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    @SerializedName("thumbnail_url")
    @Expose
    private String thumbnailUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentCollabId() {
        return parentCollabId;
    }

    public void setParentCollabId(Integer parentCollabId) {
        this.parentCollabId = parentCollabId;
    }

    public Integer getReuseCount() {
        return reuseCount;
    }

    public void setReuseCount(Integer reuseCount) {
        this.reuseCount = reuseCount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        if (parentCollabId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(parentCollabId);
        }
        if (reuseCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(reuseCount);
        }
        dest.writeString(title);
        dest.writeString(userName);
        dest.writeString(thumbnailUrl);
    }
}
