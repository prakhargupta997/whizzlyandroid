
package com.whizzly.models.comment_list;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result implements Parcelable {

    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("comment_id")
    @Expose
    private Integer commentId;
    @SerializedName("comment_type")
    @Expose
    private String commentType;
    @SerializedName("comment_user_first_name")
    @Expose
    private String commentUserFirstName;
    @SerializedName("comment_user_profile_image")
    @Expose
    private String commentUserProfileImage;
    @SerializedName("comment_user_thumbnail_url")
    @Expose
    private Object commentUserThumbnailUrl;
    @SerializedName("is_blocked")
    @Expose
    private Integer isBlocked;
    @SerializedName("user_id")
    @Expose
    private Integer userId;

    protected Result(Parcel in) {
        comment = in.readString();
        if (in.readByte() == 0) {
            commentId = null;
        } else {
            commentId = in.readInt();
        }
        commentType = in.readString();
        commentUserFirstName = in.readString();
        commentUserProfileImage = in.readString();
        if (in.readByte() == 0) {
            isBlocked = null;
        } else {
            isBlocked = in.readInt();
        }
        if (in.readByte() == 0) {
            userId = null;
        } else {
            userId = in.readInt();
        }
        commentUserName = in.readString();
        userStatus = in.readInt();
    }

    public static final Creator<Result> CREATOR = new Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel in) {
            return new Result(in);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };

    public String getCommentUserName() {
        return commentUserName;
    }

    public void setCommentUserName(String commentUserName) {
        this.commentUserName = commentUserName;
    }

    @SerializedName("comment_user_name")
    @Expose
    private String commentUserName;


    public int getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(int userStatus) {
        this.userStatus = userStatus;
    }

    @SerializedName("user_status")
    @Expose
    private int userStatus;


    public Result() {
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public String getCommentType() {
        return commentType;
    }

    public void setCommentType(String commentType) {
        this.commentType = commentType;
    }

    public String getCommentUserFirstName() {
        return commentUserFirstName;
    }

    public void setCommentUserFirstName(String commentUserFirstName) {
        this.commentUserFirstName = commentUserFirstName;
    }

    public String getCommentUserProfileImage() {
        return commentUserProfileImage;
    }

    public void setCommentUserProfileImage(String commentUserProfileImage) {
        this.commentUserProfileImage = commentUserProfileImage;
    }

    public Object getCommentUserThumbnailUrl() {
        return commentUserThumbnailUrl;
    }

    public void setCommentUserThumbnailUrl(Object commentUserThumbnailUrl) {
        this.commentUserThumbnailUrl = commentUserThumbnailUrl;
    }

    public Integer getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(Integer isBlocked) {
        this.isBlocked = isBlocked;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(comment);
        if (commentId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(commentId);
        }
        dest.writeString(commentType);
        dest.writeString(commentUserFirstName);
        dest.writeString(commentUserProfileImage);
        if (isBlocked == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(isBlocked);
        }
        if (userId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(userId);
        }
        dest.writeString(commentUserName);
        dest.writeInt(userStatus);
    }
}
