package com.whizzly.models;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class DownloadVideoModel implements Serializable, Comparable<DownloadVideoModel> {
    private String downlaodUri;
    private int frameNumber;

    public String getAudioFile() {
        return audioFile;
    }

    public void setAudioFile(String audioFile) {
        this.audioFile = audioFile;
    }

    private String audioFile;

    public String getDownlaodUri() {
        return downlaodUri;
    }

    public void setDownlaodUri(String downlaodUri) {
        this.downlaodUri = downlaodUri;
    }

    public int getFrameNumber() {
        return frameNumber;
    }

    public void setFrameNumber(int frameNumber) {
        this.frameNumber = frameNumber;
    }


    @Override
    public int compareTo(@NonNull DownloadVideoModel downloadVideoModel) {
        if (frameNumber > downloadVideoModel.frameNumber) {
            return 1;
        }
        else if (frameNumber <  downloadVideoModel.frameNumber) {
            return -1;
        }
        else {
            return 0;
        }
    }
}
