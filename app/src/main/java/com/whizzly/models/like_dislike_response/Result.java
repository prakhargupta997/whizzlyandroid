
package com.whizzly.models.like_dislike_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("added_on")
    @Expose
    private Integer addedOn;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("updated_on")
    @Expose
    private Integer updatedOn;
    @SerializedName("user_id")
    @Expose
    private String userId;

    public Integer getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Integer addedOn) {
        this.addedOn = addedOn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Integer updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
