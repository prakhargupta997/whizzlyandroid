
package com.whizzly.models.notifications;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result implements Parcelable{

    @SerializedName("affected_user_id")
    @Expose
    private Integer affectedUserId;
    @SerializedName("affected_user_name")
    @Expose
    private String affectedUserName;
    @SerializedName("commented_collab_id")
    @Expose
    private Integer commentedCollabId;
    @SerializedName("commented_collab_title")
    @Expose
    private String commentedCollabTitle;
    @SerializedName("commented_collab_url")
    @Expose
    private String commentedCollabUrl;
    @SerializedName("commented_video_id")
    @Expose
    private Integer commentedVideoId;
    @SerializedName("commented_video_title")
    @Expose
    private String commentedVideoTitle;
    @SerializedName("commented_video_url")
    @Expose
    private String commentedVideoUrl;
    @SerializedName("event_id")
    @Expose
    private Integer eventId;
    @SerializedName("event_type")
    @Expose
    private String eventType;
    @SerializedName("liked_collab_id")
    @Expose
    private Integer likedCollabId;
    @SerializedName("liked_collab_thumbnail")
    @Expose
    private String likedCollabThumbnail;
    @SerializedName("liked_collab_title")
    @Expose
    private String likedCollabTitle;
    @SerializedName("liked_collab_url")
    @Expose
    private String likedCollabUrl;
    @SerializedName("liked_video_id")
    @Expose
    private Integer likedVideoId;
    @SerializedName("liked_video_title")
    @Expose
    private String likedVideoTitle;
    @SerializedName("liked_video_thumbnail")
    @Expose
    private String likedVideoThumbnail;
    @SerializedName("liked_video_url")
    @Expose
    private String likedVideoUrl;
    @SerializedName("reused_collab_id")
    @Expose
    private Integer reusedCollabId;
    @SerializedName("reused_collab_thumbnail")
    @Expose
    private String reusedCollabThumbnail;
    @SerializedName("reused_collab_title")
    @Expose
    private String reusedCollabTitle;
    @SerializedName("reused_collab_url")
    @Expose
    private String  reusedCollabUrl;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("is_followed")
    @Expose
    private Integer isFollowing;
    @SerializedName("sender_profile_image")
    @Expose
    private String senderProfileImage;
    @SerializedName("time")
    @Expose
    private Long time;
    @SerializedName("video_type")
    @Expose
    private String videoType;
    @SerializedName("video_id")
    @Expose
    private Integer videoId;
    @SerializedName("commented_collab_thumbnail")
    @Expose
    private String commentedCollabThumbnail;


    protected Result(Parcel in) {
        if (in.readByte() == 0) {
            affectedUserId = null;
        } else {
            affectedUserId = in.readInt();
        }
        affectedUserName = in.readString();
        if (in.readByte() == 0) {
            commentedCollabId = null;
        } else {
            commentedCollabId = in.readInt();
        }
        commentedCollabTitle = in.readString();
        commentedCollabUrl = in.readString();
        if (in.readByte() == 0) {
            commentedVideoId = null;
        } else {
            commentedVideoId = in.readInt();
        }
        commentedVideoTitle = in.readString();
        commentedVideoUrl = in.readString();
        if (in.readByte() == 0) {
            eventId = null;
        } else {
            eventId = in.readInt();
        }
        eventType = in.readString();
        if (in.readByte() == 0) {
            likedCollabId = null;
        } else {
            likedCollabId = in.readInt();
        }
        likedCollabThumbnail = in.readString();
        likedCollabTitle = in.readString();
        likedCollabUrl = in.readString();
        if (in.readByte() == 0) {
            likedVideoId = null;
        } else {
            likedVideoId = in.readInt();
        }
        likedVideoTitle = in.readString();
        likedCollabThumbnail = in.readString();
        likedVideoUrl = in.readString();
        if (in.readByte() == 0) {
            reusedCollabId = null;
        } else {
            reusedCollabId = in.readInt();
        }
        reusedCollabThumbnail = in.readString();
        reusedCollabTitle = in.readString();
        reusedCollabUrl = in.readString();
        user = in.readString();
        if (in.readByte() == 0) {
            userId = null;
        } else {
            userId = in.readInt();
        }
        if (in.readByte() == 0) {
            isFollowing = null;
        } else {
            isFollowing = in.readInt();
        }
        senderProfileImage = in.readString();
        if (in.readByte() == 0) {
            time = null;
        } else {
            time = in.readLong();
        }
        videoType = in.readString();
        if (in.readByte() == 0) {
            videoId = null;
        } else {
            videoId = in.readInt();
        }
        commentedCollabThumbnail = in.readString();
    }

    public static final Creator<Result> CREATOR = new Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel in) {
            return new Result(in);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };

    public Integer getAffectedUserId() {
        return affectedUserId;
    }

    public void setAffectedUserId(Integer affectedUserId) {
        this.affectedUserId = affectedUserId;
    }

    public String getAffectedUserName() {
        return affectedUserName;
    }

    public void setAffectedUserName(String affectedUserName) {
        this.affectedUserName = affectedUserName;
    }

    public Integer getCommentedCollabId() {
        return commentedCollabId;
    }

    public void setCommentedCollabId(Integer commentedCollabId) {
        this.commentedCollabId = commentedCollabId;
    }

    public String getCommentedCollabTitle() {
        return commentedCollabTitle;
    }

    public void setCommentedCollabTitle(String commentedCollabTitle) {
        this.commentedCollabTitle = commentedCollabTitle;
    }

    public String getCommentedCollabUrl() {
        return commentedCollabUrl;
    }

    public void setCommentedCollabUrl(String commentedCollabUrl) {
        this.commentedCollabUrl = commentedCollabUrl;
    }

    public Integer getCommentedVideoId() {
        return commentedVideoId;
    }

    public void setCommentedVideoId(Integer commentedVideoId) {
        this.commentedVideoId = commentedVideoId;
    }

    public String getCommentedVideoTitle() {
        return commentedVideoTitle;
    }

    public void setCommentedVideoTitle(String commentedVideoTitle) {
        this.commentedVideoTitle = commentedVideoTitle;
    }

    public String getCommentedVideoUrl() {
        return commentedVideoUrl;
    }

    public void setCommentedVideoUrl(String commentedVideoUrl) {
        this.commentedVideoUrl = commentedVideoUrl;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Integer getLikedCollabId() {
        return likedCollabId;
    }

    public void setLikedCollabId(Integer likedCollabId) {
        this.likedCollabId = likedCollabId;
    }

    public String getLikedCollabThumbnail() {
        return likedCollabThumbnail;
    }

    public void setLikedCollabThumbnail(String likedCollabThumbnail) {
        this.likedCollabThumbnail = likedCollabThumbnail;
    }

    public String getLikedCollabTitle() {
        return likedCollabTitle;
    }

    public void setLikedCollabTitle(String likedCollabTitle) {
        this.likedCollabTitle = likedCollabTitle;
    }

    public String getLikedCollabUrl() {
        return likedCollabUrl;
    }

    public void setLikedCollabUrl(String likedCollabUrl) {
        this.likedCollabUrl = likedCollabUrl;
    }

    public Integer getLikedVideoId() {
        return likedVideoId;
    }

    public void setLikedVideoId(Integer likedVideoId) {
        this.likedVideoId = likedVideoId;
    }

    public String getLikedVideoTitle() {
        return likedVideoTitle;
    }

    public void setLikedVideoTitle(String likedVideoTitle) {
        this.likedVideoTitle = likedVideoTitle;
    }

    public String getLikedVideoUrl() {
        return likedVideoUrl;
    }

    public void setLikedVideoUrl(String likedVideoUrl) {
        this.likedVideoUrl = likedVideoUrl;
    }

    public Integer getReusedCollabId() {
        return reusedCollabId;
    }

    public void setReusedCollabId(Integer reusedCollabId) {
        this.reusedCollabId = reusedCollabId;
    }

    public String getReusedCollabThumbnail() {
        return reusedCollabThumbnail;
    }

    public void setReusedCollabThumbnail(String reusedCollabThumbnail) {
        this.reusedCollabThumbnail = reusedCollabThumbnail;
    }

    public String getReusedCollabTitle() {
        return reusedCollabTitle;
    }

    public void setReusedCollabTitle(String reusedCollabTitle) {
        this.reusedCollabTitle = reusedCollabTitle;
    }

    public String getReusedCollabUrl() {
        return reusedCollabUrl;
    }

    public void setReusedCollabUrl(String reusedCollabUrl) {
        this.reusedCollabUrl = reusedCollabUrl;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getIsFollowed() {
        return isFollowing;
    }

    public void setIsFollowed(Integer isFollowing) {
        this.isFollowing = isFollowing;
    }

    public String getSenderProfileImage() {
        return senderProfileImage;
    }

    public void setSenderProfileImage(String senderProfileImage) {
        this.senderProfileImage = senderProfileImage;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getVideoType() {
        return videoType;
    }

    public void setVideoType(String videoType) {
        this.videoType = videoType;
    }

    public Integer getVideoId() {
        return videoId;
    }

    public void setVideoId(Integer videoId) {
        this.videoId = videoId;
    }


    public String getCommentedCollabThumbnail() {
        return commentedCollabThumbnail;
    }

    public void setCommentedCollabThumbnail(String commentedCollabThumbnail) {
        this.commentedCollabThumbnail = commentedCollabThumbnail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (affectedUserId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(affectedUserId);
        }
        dest.writeString(affectedUserName);
        if (commentedCollabId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(commentedCollabId);
        }
        dest.writeString(commentedCollabTitle);
        dest.writeString(commentedCollabUrl);
        if (commentedVideoId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(commentedVideoId);
        }
        dest.writeString(commentedVideoTitle);
        dest.writeString(commentedVideoUrl);
        if (eventId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(eventId);
        }
        dest.writeString(eventType);
        if (likedCollabId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(likedCollabId);
        }
        dest.writeString(likedCollabThumbnail);
        dest.writeString(likedCollabTitle);
        dest.writeString(likedCollabUrl);
        if (likedVideoId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(likedVideoId);
        }
        dest.writeString(likedVideoThumbnail);
        dest.writeString(likedVideoTitle);
        dest.writeString(likedVideoUrl);
        if (reusedCollabId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(reusedCollabId);
        }
        dest.writeString(reusedCollabThumbnail);
        dest.writeString(reusedCollabTitle);
        dest.writeString(reusedCollabUrl);
        dest.writeString(user);
        if (userId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(userId);
        }
        if (isFollowing == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(isFollowing);
        }
        dest.writeString(senderProfileImage);
        if (time == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(time);
        }
        dest.writeString(videoType);
        if (videoId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(videoId);
        }
        dest.writeString(commentedCollabThumbnail);
    }

    public String getLikedVideoThumbnail() {
        return likedVideoThumbnail;
    }

    public void setLikedVideoThumbnail(String likedVideoThumbnail) {
        this.likedVideoThumbnail = likedVideoThumbnail;
    }
}
