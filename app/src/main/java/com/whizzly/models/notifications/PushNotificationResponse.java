package com.whizzly.models.notifications;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PushNotificationResponse implements Parcelable {
    @SerializedName("notification_type")
    @Expose
    private Integer notificationType;
    @SerializedName("text_type")
    @Expose
    private String TextType;
    @SerializedName("username")
    @Expose
    private String userName;
    @SerializedName("sender_user_id")
    @Expose
    private Integer senderUserId;
    @SerializedName("time")
    @Expose
    private Long time;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("receiver_user_id")
    @Expose
    private  Integer receiverUserId;
    @SerializedName("message")
    @Expose
    private  String message;
    @SerializedName("room_id")
    @Expose
    private  String roomId;
    @SerializedName("video_id")
    @Expose
    private  Integer videoId;
    @SerializedName("video_type")
    @Expose
    private  String videoType;


    protected PushNotificationResponse(Parcel in) {
        if (in.readByte() == 0) {
            notificationType = null;
        } else {
            notificationType = in.readInt();
        }
        TextType = in.readString();
        userName = in.readString();
        if (in.readByte() == 0) {
            senderUserId = null;
        } else {
            senderUserId = in.readInt();
        }
        if (in.readByte() == 0) {
            time = null;
        } else {
            time = in.readLong();
        }
        profilePic = in.readString();
        if (in.readByte() == 0) {
            receiverUserId = null;
        } else {
            receiverUserId = in.readInt();
        }
        if (in.readByte() == 0) {
            message = null;
        } else {
            message = in.readString();
        }
        if (in.readByte() == 0) {
            roomId = null;
        } else {
            roomId = in.readString();
        }
        if (in.readByte() == 0) {
            videoId = null;
        } else {
            videoId = in.readInt();
        }
        videoType = in.readString();
    }

    public static final Creator<PushNotificationResponse> CREATOR = new Creator<PushNotificationResponse>() {
        @Override
        public PushNotificationResponse createFromParcel(Parcel in) {
            return new PushNotificationResponse(in);
        }

        @Override
        public PushNotificationResponse[] newArray(int size) {
            return new PushNotificationResponse[size];
        }
    };

    public Integer getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(Integer notificationType) {
        this.notificationType = notificationType;
    }

    public String getTextType() {
        return TextType;
    }

    public void setTextType(String textType) {
        TextType = textType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getSenderUserId() {
        return senderUserId;
    }

    public void setSenderUserId(Integer senderUserId) {
        this.senderUserId = senderUserId;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public Integer getReceiverUserId() {
        return receiverUserId;
    }

    public void setReceiverUserId(Integer receiverUserId) {
        this.receiverUserId = receiverUserId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }


    public Integer getVideoId() {
        return videoId;
    }

    public void setVideoId(Integer videoId) {
        this.videoId = videoId;
    }

    public String getVideoType() {
        return videoType;
    }

    public void setVideoType(String videoType) {
        this.videoType = videoType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (notificationType == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(notificationType);
        }
        dest.writeString(TextType);
        dest.writeString(userName);
        if (senderUserId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(senderUserId);
        }
        if (time == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(time);
        }
        dest.writeString(profilePic);
        if (receiverUserId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(receiverUserId);
        }
        if (message == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeString(message);
        }
        if (roomId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeString(roomId);
        }
        if (videoId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(videoId);
        }
        dest.writeString(videoType);
    }
}
