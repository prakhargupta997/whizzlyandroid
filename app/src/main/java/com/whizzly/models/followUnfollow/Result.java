
package com.whizzly.models.followUnfollow;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("added_on")
    @Expose
    private int addedOn;
    @SerializedName("follower_user_id")
    @Expose
    private String followerUserId;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("updated_on")
    @Expose
    private int updatedOn;
    @SerializedName("user_id")
    @Expose
    private String userId;

    public int getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(int addedOn) {
        this.addedOn = addedOn;
    }

    public String getFollowerUserId() {
        return followerUserId;
    }

    public void setFollowerUserId(String followerUserId) {
        this.followerUserId = followerUserId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(int updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
