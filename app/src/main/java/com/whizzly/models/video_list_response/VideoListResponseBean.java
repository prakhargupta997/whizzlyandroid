
package com.whizzly.models.video_list_response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoListResponseBean implements Parcelable {

    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.code);
        dest.writeString(this.message);
        dest.writeTypedList(this.result);
    }


    protected VideoListResponseBean(Parcel in) {
        this.code = in.readInt();
        this.message = in.readString();
        this.result = in.createTypedArrayList(Result.CREATOR);
    }

    public static final Creator<VideoListResponseBean> CREATOR = new Creator<VideoListResponseBean>() {
        @Override
        public VideoListResponseBean createFromParcel(Parcel source) {
            return new VideoListResponseBean(source);
        }

        @Override
        public VideoListResponseBean[] newArray(int size) {
            return new VideoListResponseBean[size];
        }
    };
}
