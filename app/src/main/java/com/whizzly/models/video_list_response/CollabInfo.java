
package com.whizzly.models.video_list_response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CollabInfo implements Parcelable {

    @SerializedName("frame_no")
    @Expose
    private int frameNo;
    @SerializedName("hashtag")
    @Expose
    private String hashtag;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("is_replaceable")
    @Expose
    private int isReplaceable;
    @SerializedName("privacy_type")
    @Expose
    private int privacyType;
    @SerializedName("thumbnail_url")
    @Expose
    private String thumbnailUrl;
    @SerializedName("user_id")
    @Expose
    private int userId;
    @SerializedName("video_duration")
    @Expose
    private String videoDuration;
    @SerializedName("video_title")
    @Expose
    private String videoTitle;
    @SerializedName("video_url")
    @Expose
    private String videoUrl;

    public int getFrameNo() {
        return frameNo;
    }

    public void setFrameNo(int frameNo) {
        this.frameNo = frameNo;
    }

    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIsReplaceable() {
        return isReplaceable;
    }

    public void setIsReplaceable(int isReplaceable) {
        this.isReplaceable = isReplaceable;
    }

    public int getPrivacyType() {
        return privacyType;
    }

    public void setPrivacyType(int privacyType) {
        this.privacyType = privacyType;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(String videoDuration) {
        this.videoDuration = videoDuration;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.frameNo);
        dest.writeString(this.hashtag);
        dest.writeInt(this.id);
        dest.writeInt(this.isReplaceable);
        dest.writeInt(this.privacyType);
        dest.writeString(this.thumbnailUrl);
        dest.writeInt(this.userId);
        dest.writeString(this.videoDuration);
        dest.writeString(this.videoTitle);
        dest.writeString(this.videoUrl);
    }

    public CollabInfo() {
    }

    protected CollabInfo(Parcel in) {
        this.frameNo = in.readInt();
        this.hashtag = in.readString();
        this.id = in.readInt();
        this.isReplaceable = in.readInt();
        this.privacyType = in.readInt();
        this.thumbnailUrl = in.readString();
        this.userId = in.readInt();
        this.videoDuration = in.readString();
        this.videoTitle = in.readString();
        this.videoUrl = in.readString();
    }

    public static final Creator<CollabInfo> CREATOR = new Creator<CollabInfo>() {
        @Override
        public CollabInfo createFromParcel(Parcel source) {
            return new CollabInfo(source);
        }

        @Override
        public CollabInfo[] newArray(int size) {
            return new CollabInfo[size];
        }
    };
}
