
package com.whizzly.models.video_list_response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result implements Parcelable {

    @SerializedName("additional_info")
    @Expose
    private String additinalInfo = null;
    @SerializedName("collab_id")
    @Expose
    private int collabId;
    @SerializedName("collab_info")
    @Expose
    private List<CollabInfo> collabInfo = null;
    @SerializedName("collab_thumbnail")
    @Expose
    private String collabThumbnail;
    @SerializedName("music_id")
    @Expose
    private int musicId;
    @SerializedName("music_url")
    @Expose
    private String musicUrl;
    @SerializedName("template_size")
    @Expose
    private int templateSize;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("user_id")
    @Expose
    private int userId;

    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

    @SerializedName("hashtags")
    @Expose
    private String hashtag;

    public String  getAdditinalInfo() {
        return additinalInfo;
    }

    public void setAdditinalInfo(String  additinalInfo) {
        this.additinalInfo = additinalInfo;
    }

    public int getCollabId() {
        return collabId;
    }

    public void setCollabId(int collabId) {
        this.collabId = collabId;
    }

    public List<CollabInfo> getCollabInfo() {
        return collabInfo;
    }

    public void setCollabInfo(List<CollabInfo> collabInfo) {
        this.collabInfo = collabInfo;
    }

    public String getCollabThumbnail() {
        return collabThumbnail;
    }

    public void setCollabThumbnail(String collabThumbnail) {
        this.collabThumbnail = collabThumbnail;
    }

    public int getMusicId() {
        return musicId;
    }

    public void setMusicId(int musicId) {
        this.musicId = musicId;
    }

    public String getMusicUrl() {
        return musicUrl;
    }

    public void setMusicUrl(String musicUrl) {
        this.musicUrl = musicUrl;
    }

    public int getTemplateSize() {
        return templateSize;
    }

    public void setTemplateSize(int templateSize) {
        this.templateSize = templateSize;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.additinalInfo);
        dest.writeInt(this.collabId);
        dest.writeTypedList(this.collabInfo);
        dest.writeString(this.collabThumbnail);
        dest.writeInt(this.musicId);
        dest.writeString(this.musicUrl);
        dest.writeInt(this.templateSize);
        dest.writeString(this.title);
        dest.writeString(this.url);
        dest.writeInt(this.userId);
        dest.writeString(this.hashtag);
    }

    public Result() {
    }

    protected Result(Parcel in) {
        this.additinalInfo = in.readString();
        this.collabId = in.readInt();
        this.collabInfo = in.createTypedArrayList(CollabInfo.CREATOR);
        this.collabThumbnail = in.readString();
        this.musicId = in.readInt();
        this.musicUrl = in.readString();
        this.templateSize = in.readInt();
        this.title = in.readString();
        this.url = in.readString();
        this.userId = in.readInt();
        this.hashtag = in.readString();
    }

    public static final Creator<Result> CREATOR = new Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel source) {
            return new Result(source);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };
}
