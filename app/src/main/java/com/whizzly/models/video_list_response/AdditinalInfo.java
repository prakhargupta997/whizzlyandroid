
package com.whizzly.models.video_list_response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AdditinalInfo implements Parcelable {

    @SerializedName("color_code")
    @Expose
    private String colorCode;
    @SerializedName("detail")
    @Expose
    private List<Detail> detail = null;
    @SerializedName("frame_no")
    @Expose
    private int frameNo;

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public List<Detail> getDetail() {
        return detail;
    }

    public void setDetail(List<Detail> detail) {
        this.detail = detail;
    }

    public int getFrameNo() {
        return frameNo;
    }

    public void setFrameNo(int frameNo) {
        this.frameNo = frameNo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.colorCode);
        dest.writeTypedList(this.detail);
        dest.writeInt(this.frameNo);
    }

    public AdditinalInfo() {
    }

    protected AdditinalInfo(Parcel in) {
        this.colorCode = in.readString();
        this.detail = in.createTypedArrayList(Detail.CREATOR);
        this.frameNo = in.readInt();
    }

    public static final Creator<AdditinalInfo> CREATOR = new Creator<AdditinalInfo>() {
        @Override
        public AdditinalInfo createFromParcel(Parcel source) {
            return new AdditinalInfo(source);
        }

        @Override
        public AdditinalInfo[] newArray(int size) {
            return new AdditinalInfo[size];
        }
    };
}
