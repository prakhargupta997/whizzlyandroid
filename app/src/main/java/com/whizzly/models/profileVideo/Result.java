
package com.whizzly.models.profileVideo;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result implements Serializable {

    @SerializedName("next")
    @Expose
    private int next;
    @SerializedName("next_url")
    @Expose
    private String nextUrl;
    @SerializedName("video_info")
    @Expose
    private List<VideoInfo> videoInfo = null;

    public int getNext() {
        return next;
    }

    public void setNext(int next) {
        this.next = next;
    }

    public String getNextUrl() {
        return nextUrl;
    }

    public void setNextUrl(String nextUrl) {
        this.nextUrl = nextUrl;
    }

    public List<VideoInfo> getVideoInfo() {
        return videoInfo;
    }

    public void setVideoInfo(List<VideoInfo> videoInfo) {
        this.videoInfo = videoInfo;
    }

}
