package com.whizzly.models;

import android.os.Parcel;
import android.os.Parcelable;

public class AddVideoTempleteModel implements Parcelable {
    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    private boolean isSelected;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.isSelected ? (byte) 1 : (byte) 0);
    }

    public AddVideoTempleteModel() {
    }

    protected AddVideoTempleteModel(Parcel in) {
        this.isSelected = in.readByte() != 0;
    }

    public static final Creator<AddVideoTempleteModel> CREATOR = new Creator<AddVideoTempleteModel>() {
        @Override
        public AddVideoTempleteModel createFromParcel(Parcel source) {
            return new AddVideoTempleteModel(source);
        }

        @Override
        public AddVideoTempleteModel[] newArray(int size) {
            return new AddVideoTempleteModel[size];
        }
    };
}
