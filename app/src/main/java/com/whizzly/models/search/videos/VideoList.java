
package com.whizzly.models.search.videos;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoList implements Serializable {

    @SerializedName("blocked_users")
    @Expose
    private List<Integer> blockedUsers = null;
    @SerializedName("detail")
    @Expose
    private String detail;
    @SerializedName("hashtag")
    @Expose
    private String hashtag;
    @SerializedName("thumbnail_url")
    @Expose
    private String thumbnailUrl;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("video_id")
    @Expose
    private Integer videoId;
    @SerializedName("video_title")
    @Expose
    private String videoTitle;
    @SerializedName("video_type")
    @Expose
    private String videoType;
    @SerializedName("doc_count")
    @Expose
    private Integer docCount;

    public List<Integer> getBlockedUsers() {
        return blockedUsers;
    }

    public void setBlockedUsers(List<Integer> blockedUsers) {
        this.blockedUsers = blockedUsers;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getVideoId() {
        return videoId;
    }

    public void setVideoId(Integer videoId) {
        this.videoId = videoId;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getVideoType() {
        return videoType;
    }

    public void setVideoType(String videoType) {
        this.videoType = videoType;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getDocCount() {
        return docCount;
    }

    public void setDocCount(Integer docCount) {
        this.docCount = docCount;
    }
}
