
package com.whizzly.models.search.users;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserList {

    @SerializedName("blocked_users")
    @Expose
    private List<Object> blockedUsers = new ArrayList<>();
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;


    public List<Object> getBlockedUsers() {
        return blockedUsers;
    }

    public void setBlockedUsers(List<Object> blockedUsers) {
        this.blockedUsers = blockedUsers;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }
}
