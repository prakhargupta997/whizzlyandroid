package com.whizzly.models.post_video;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostVideoBean {

    @SerializedName("collab_id")
    @Expose
    private int collabId;
    @SerializedName("user_id")
    @Expose
    private int userId;
    @SerializedName("collab_url")
    @Expose
    private String collabUrl;
    @SerializedName("video_url")
    @Expose
    private String videoUrl;
    @SerializedName("video_duration")
    @Expose
    private String videoDuration;
    @SerializedName("frame_no")
    @Expose
    private int frameNo;
    @SerializedName("is_mandatory")
    @Expose
    private int isMandatory;
    @SerializedName("collab_title")
    @Expose
    private String title;
    @SerializedName("hashtags")
    @Expose
    private String hashtags;
    @SerializedName("privacy_type_id")
    @Expose
    private int privacyTypeId;
    @SerializedName("total_frames")
    @Expose
    private int totalFrames;
    @SerializedName("additional_info")
    @Expose
    private List<AdditionalInfo> additionalInfo = null;
    @SerializedName("music_id")
    @Expose
    private int musicId;
    @SerializedName("music_url")
    @Expose
    private String musicUrl;
    @SerializedName("thumbnail_url")
    @Expose
    private String thumbnailUrl;
    @SerializedName("video_thumbnail_url")
    @Expose
    private String videoThumbnailUrl;
    @SerializedName("watermark_url")
    @Expose
    private String waterMarkUrl;

    /**
     * No args constructor for use in serialization
     */
    public PostVideoBean() {
    }

    public PostVideoBean(int collabId, int userId, String collabUrl, String videoUrl, String videoDuration, int frameNo, int isMandatory, String title, String hashtags, int privacyTypeId, int totalFrames, List<AdditionalInfo> additionalInfo, int musicId, String musicUrl, String thumbnailUrl, String videoThumbnailUrl, String waterMarkUrl) {
        super();
        this.collabId = collabId;
        this.userId = userId;
        this.collabUrl = collabUrl;
        this.videoUrl = videoUrl;
        this.videoDuration = videoDuration;
        this.frameNo = frameNo;
        this.isMandatory = isMandatory;
        this.title = title;
        this.hashtags = hashtags;
        this.privacyTypeId = privacyTypeId;
        this.totalFrames = totalFrames;
        this.additionalInfo = additionalInfo;
        this.musicId = musicId;
        this.musicUrl = musicUrl;
        this.thumbnailUrl = thumbnailUrl;
        this.videoThumbnailUrl = videoThumbnailUrl;
        this.waterMarkUrl = waterMarkUrl;
    }

    public String getWaterMarkUrl() {
        return waterMarkUrl;
    }

    public void setWaterMarkUrl(String waterMarkUrl) {
        this.waterMarkUrl = waterMarkUrl;
    }

    public String getVideoThumbnailUrl() {
        return videoThumbnailUrl;
    }

    public void setVideoThumbnailUrl(String videoThumbnailUrl) {
        this.videoThumbnailUrl = videoThumbnailUrl;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public int getCollabId() {
        return collabId;
    }

    public void setCollabId(int collabId) {
        this.collabId = collabId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCollabUrl() {
        return collabUrl;
    }

    public void setCollabUrl(String collabUrl) {
        this.collabUrl = collabUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(String videoDuration) {
        this.videoDuration = videoDuration;
    }

    public int getFrameNo() {
        return frameNo;
    }

    public void setFrameNo(int frameNo) {
        this.frameNo = frameNo;
    }

    public int getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(int isMandatory) {
        this.isMandatory = isMandatory;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHashtags() {
        return hashtags;
    }

    public void setHashtags(String hashtags) {
        this.hashtags = hashtags;
    }

    public int getPrivacyTypeId() {
        return privacyTypeId;
    }

    public void setPrivacyTypeId(int privacyTypeId) {
        this.privacyTypeId = privacyTypeId;
    }

    public int getTotalFrames() {
        return totalFrames;
    }

    public void setTotalFrames(int totalFrames) {
        this.totalFrames = totalFrames;
    }

    public List<AdditionalInfo> getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(List<AdditionalInfo> additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public int getMusicId() {
        return musicId;
    }

    public void setMusicId(int musicId) {
        this.musicId = musicId;
    }

    public String getMusicUrl() {
        return musicUrl;
    }

    public void setMusicUrl(String musicUrl) {
        this.musicUrl = musicUrl;
    }

}
