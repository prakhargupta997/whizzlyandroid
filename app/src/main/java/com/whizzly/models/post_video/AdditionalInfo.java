
package com.whizzly.models.post_video;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AdditionalInfo {

    @SerializedName("frame_no")
    @Expose
    private int frameNo;
    @SerializedName("color_code")
    @Expose
    private String colorCode;
    @SerializedName("detail")
    @Expose
    private List<Detail> detail = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public AdditionalInfo() {
    }

    /**
     * 
     * @param detail
     * @param colorCode
     * @param frameNo
     */
    public AdditionalInfo(int frameNo, String colorCode, List<Detail> detail) {
        super();
        this.frameNo = frameNo;
        this.colorCode = colorCode;
        this.detail = detail;
    }

    public int getFrameNo() {
        return frameNo;
    }

    public void setFrameNo(int frameNo) {
        this.frameNo = frameNo;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public List<Detail> getDetail() {
        return detail;
    }

    public void setDetail(List<Detail> detail) {
        this.detail = detail;
    }

}
