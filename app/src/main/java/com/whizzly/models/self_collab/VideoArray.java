
package com.whizzly.models.self_collab;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoArray {

    @SerializedName("video_id")
    @Expose
    private Integer videoId;
    @SerializedName("video_file_without_audio")
    @Expose
    private String videoFileWithoutAudio;
    @SerializedName("video_duration")
    @Expose
    private Integer videoDuration;
    @SerializedName("video_path")
    @Expose
    private String videoPath;
    @SerializedName("audio_file")
    @Expose
    private String audioFile;
    @SerializedName("is_mandatory")
    @Expose
    private Integer isMandatory;
    @SerializedName("image_frame")
    @Expose
    private String imageFrame;
    @SerializedName("video_frame_number")
    @Expose
    private Integer videoFrameNumber;

    public Integer getVideoId() {
        return videoId;
    }

    public void setVideoId(Integer videoId) {
        this.videoId = videoId;
    }

    public String getVideoFileWithoutAudio() {
        return videoFileWithoutAudio;
    }

    public void setVideoFileWithoutAudio(String videoFileWithoutAudio) {
        this.videoFileWithoutAudio = videoFileWithoutAudio;
    }

    public Integer getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(Integer videoDuration) {
        this.videoDuration = videoDuration;
    }

    public String getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }

    public String getAudioFile() {
        return audioFile;
    }

    public void setAudioFile(String audioFile) {
        this.audioFile = audioFile;
    }

    public Integer getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(Integer isMandatory) {
        this.isMandatory = isMandatory;
    }

    public String getImageFrame() {
        return imageFrame;
    }

    public void setImageFrame(String imageFrame) {
        this.imageFrame = imageFrame;
    }

    public Integer getVideoFrameNumber() {
        return videoFrameNumber;
    }

    public void setVideoFrameNumber(Integer videoFrameNumber) {
        this.videoFrameNumber = videoFrameNumber;
    }

}
