
package com.whizzly.models.self_collab;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SelfCollabDataBean {

    @SerializedName("video_array")
    @Expose
    private List<VideoArray> videoArray = null;
    @SerializedName("collab_bar_info")
    @Expose
    private String collabBarInfo;
    @SerializedName("video_template_size")
    @Expose
    private Integer videoTemplateSize;
    @SerializedName("collab_video")
    @Expose
    private String collabVideo;
    @SerializedName("collab_video_thumb")
    @Expose
    private String collabVideoThumb;
    @SerializedName("collab_video_duration")
    @Expose
    private Integer collabVideoDuration;
    @SerializedName("collab_video_title")
    @Expose
    private String collabVideoTitle;
    @SerializedName("collab_video_hashtag")
    @Expose
    private String collabVideoHashtag;
    @SerializedName("privacy_type_id")
    @Expose
    private Integer privacyTypeId;
    @SerializedName("music_id")
    @Expose
    private Integer musicId;
    @SerializedName("music_url")
    @Expose
    private String musicUrl;
    @SerializedName("watermark_url")
    @Expose
    private String watermarkUrl;
    @SerializedName("collab_id")
    @Expose
    private Integer collabId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;

    public List<VideoArray> getVideoArray() {
        return videoArray;
    }

    public void setVideoArray(List<VideoArray> videoArray) {
        this.videoArray = videoArray;
    }

    public String getCollabBarInfo() {
        return collabBarInfo;
    }

    public void setCollabBarInfo(String collabBarInfo) {
        this.collabBarInfo = collabBarInfo;
    }

    public Integer getVideoTemplateSize() {
        return videoTemplateSize;
    }

    public void setVideoTemplateSize(Integer videoTemplateSize) {
        this.videoTemplateSize = videoTemplateSize;
    }

    public String getCollabVideo() {
        return collabVideo;
    }

    public void setCollabVideo(String collabVideo) {
        this.collabVideo = collabVideo;
    }

    public String getCollabVideoThumb() {
        return collabVideoThumb;
    }

    public void setCollabVideoThumb(String collabVideoThumb) {
        this.collabVideoThumb = collabVideoThumb;
    }

    public Integer getCollabVideoDuration() {
        return collabVideoDuration;
    }

    public void setCollabVideoDuration(Integer collabVideoDuration) {
        this.collabVideoDuration = collabVideoDuration;
    }

    public String getCollabVideoTitle() {
        return collabVideoTitle;
    }

    public void setCollabVideoTitle(String collabVideoTitle) {
        this.collabVideoTitle = collabVideoTitle;
    }

    public String getCollabVideoHashtag() {
        return collabVideoHashtag;
    }

    public void setCollabVideoHashtag(String collabVideoHashtag) {
        this.collabVideoHashtag = collabVideoHashtag;
    }

    public Integer getPrivacyTypeId() {
        return privacyTypeId;
    }

    public void setPrivacyTypeId(Integer privacyTypeId) {
        this.privacyTypeId = privacyTypeId;
    }

    public Integer getMusicId() {
        return musicId;
    }

    public void setMusicId(Integer musicId) {
        this.musicId = musicId;
    }

    public String getMusicUrl() {
        return musicUrl;
    }

    public void setMusicUrl(String musicUrl) {
        this.musicUrl = musicUrl;
    }

    public String getWatermarkUrl() {
        return watermarkUrl;
    }

    public void setWatermarkUrl(String watermarkUrl) {
        this.watermarkUrl = watermarkUrl;
    }

    public Integer getCollabId() {
        return collabId;
    }

    public void setCollabId(Integer collabId) {
        this.collabId = collabId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}
