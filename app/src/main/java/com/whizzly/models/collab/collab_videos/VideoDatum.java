
package com.whizzly.models.collab.collab_videos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VideoDatum  implements Parcelable {

    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("category_video")
    @Expose
    private List<CategoryVideo> categoryVideo = null;

    protected VideoDatum(Parcel in) {
        if (in.readByte() == 0) {
            categoryId = null;
        } else {
            categoryId = in.readInt();
        }
        categoryName = in.readString();
        categoryVideo = in.createTypedArrayList(CategoryVideo.CREATOR);
    }

    public static final Creator<VideoDatum> CREATOR = new Creator<VideoDatum>() {
        @Override
        public VideoDatum createFromParcel(Parcel in) {
            return new VideoDatum(in);
        }

        @Override
        public VideoDatum[] newArray(int size) {
            return new VideoDatum[size];
        }
    };

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<CategoryVideo> getCategoryVideo() {
        return categoryVideo;
    }

    public void setCategoryVideo(List<CategoryVideo> categoryVideo) {
        this.categoryVideo = categoryVideo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (categoryId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(categoryId);
        }
        dest.writeString(categoryName);
        dest.writeTypedList(categoryVideo);
    }
}
