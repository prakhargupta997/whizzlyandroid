
package com.whizzly.models.collab.collab_videos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryVideo implements Parcelable {

    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("hastags")
    @Expose
    private Object hastags;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("thumbnail_url")
    @Expose
    private String thumbnailUrl;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("total_reuse")
    @Expose
    private Integer totalReuse;

    protected CategoryVideo(Parcel in) {
        duration = in.readString();
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        thumbnailUrl = in.readString();
        title = in.readString();
        if (in.readByte() == 0) {
            totalReuse = null;
        } else {
            totalReuse = in.readInt();
        }
    }

    public static final Creator<CategoryVideo> CREATOR = new Creator<CategoryVideo>() {
        @Override
        public CategoryVideo createFromParcel(Parcel in) {
            return new CategoryVideo(in);
        }

        @Override
        public CategoryVideo[] newArray(int size) {
            return new CategoryVideo[size];
        }
    };

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Object getHastags() {
        return hastags;
    }

    public void setHastags(Object hastags) {
        this.hastags = hastags;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getTotalReuse() {
        return totalReuse;
    }

    public void setTotalReuse(Integer totalReuse) {
        this.totalReuse = totalReuse;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(duration);
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(thumbnailUrl);
        dest.writeString(title);
        if (totalReuse == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(totalReuse);
        }
    }
}
