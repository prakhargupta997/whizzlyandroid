
package com.whizzly.models.collab.collab_videos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result {

    @SerializedName("admin_video")
    @Expose
    private List<AdminVideo> adminVideo = null;
    @SerializedName("video_data")
    @Expose
    private List<VideoDatum> videoData = null;

    public List<AdminVideo> getAdminVideo() {
        return adminVideo;
    }

    public void setAdminVideo(List<AdminVideo> adminVideo) {
        this.adminVideo = adminVideo;
    }

    public List<VideoDatum> getVideoData() {
        return videoData;
    }

    public void setVideoData(List<VideoDatum> videoData) {
        this.videoData = videoData;
    }

}
