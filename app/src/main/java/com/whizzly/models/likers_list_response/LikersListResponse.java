
package com.whizzly.models.likers_list_response;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LikersListResponse implements Parcelable
{

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("next_count")
    @Expose
    private Integer nextCount;
    @SerializedName("next_url")
    @Expose
    private String nextUrl;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;
    public final static Parcelable.Creator<LikersListResponse> CREATOR = new Creator<LikersListResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public LikersListResponse createFromParcel(Parcel in) {
            return new LikersListResponse(in);
        }

        public LikersListResponse[] newArray(int size) {
            return (new LikersListResponse[size]);
        }

    }
    ;

    protected LikersListResponse(Parcel in) {
        this.code = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.nextCount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.nextUrl = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.result, (com.whizzly.models.likers_list_response.Result.class.getClassLoader()));
    }

    public LikersListResponse() {
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getNextCount() {
        return nextCount;
    }

    public void setNextCount(Integer nextCount) {
        this.nextCount = nextCount;
    }

    public String getNextUrl() {
        return nextUrl;
    }

    public void setNextUrl(String nextUrl) {
        this.nextUrl = nextUrl;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(code);
        dest.writeValue(message);
        dest.writeValue(nextCount);
        dest.writeValue(nextUrl);
        dest.writeList(result);
    }

    public int describeContents() {
        return  0;
    }

}
