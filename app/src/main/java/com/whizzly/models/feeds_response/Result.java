package com.whizzly.models.feeds_response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result implements Parcelable {

    public final static Parcelable.Creator<Result> CREATOR = new Creator<Result>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Result createFromParcel(Parcel in) {
            return new Result(in);
        }

        public Result[] newArray(int size) {
            return (new Result[size]);
        }

    };
    @SerializedName("comment_count")
    @Expose
    private Integer commentCount;
    @SerializedName("deep_link")
    private String deepLink;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("is_follower")
    @Expose
    private String isFollower;
    @SerializedName("music_id")
    @Expose
    private Integer musicId;
    @SerializedName("music_url")
    @Expose
    private String musicUrl;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("profile_thumbnail")
    @Expose
    private String profileThumbnail;
    @SerializedName("ranking")
    @Expose
    private Integer ranking;
    @SerializedName("reused_by")
    @Expose
    private Object reusedBy;
    @SerializedName("thumbnail_url")
    @Expose
    private String thumbnailUrl;
    @SerializedName("total_likes")
    @Expose
    private Integer totalLikes;
    @SerializedName("total_reuse")
    @Expose
    private Integer totalReuse;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("video_description")
    @Expose
    private String videoDescription;
    @SerializedName("video_hastags")
    @Expose
    private String videoHastags;
    @SerializedName("is_liked")
    @Expose
    private Integer isLiked;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("music_description")
    @Expose
    private String musicDescription;
    @SerializedName("music_title")
    @Expose
    private String musicTitle;
    @SerializedName("music_reuse_count")
    @Expose
    private Integer musicReuseCount;
    @SerializedName("music_thumbnail")
    @Expose
    private String musicThumbnail;
    @SerializedName("follower_count")
    @Expose
    private Integer followerCount;
    @SerializedName("following_count")
    @Expose
    private Integer followingCount;

    @SerializedName("saved_status")
    @Expose
    private Integer savedStatus;
    @SerializedName("saved_video_privacy")
    @Expose
    private Integer savedVideoPrivacy;

    @SerializedName("watermark_url")
    @Expose
    private String waterMarkUrl;

    @SerializedName("users_name")
    @Expose
    private String usersName;

    protected Result(Parcel in) {
        this.commentCount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.firstName = ((String) in.readValue((String.class.getClassLoader())));
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.isFollower = ((String) in.readValue((String.class.getClassLoader())));
        this.musicId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.musicUrl = ((String) in.readValue((String.class.getClassLoader())));
        this.profileImage = ((String) in.readValue((String.class.getClassLoader())));
        this.profileThumbnail = ((String) in.readValue((String.class.getClassLoader())));
        this.ranking = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.reusedBy = in.readValue((Object.class.getClassLoader()));
        this.thumbnailUrl = ((String) in.readValue((String.class.getClassLoader())));
        this.totalLikes = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.totalReuse = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.type = ((String) in.readValue((String.class.getClassLoader())));
        this.url = ((String) in.readValue((String.class.getClassLoader())));
        this.userId = ((String) in.readValue((String.class.getClassLoader())));
        this.userName = ((String) in.readValue((String.class.getClassLoader())));
        this.videoDescription = ((String) in.readValue((String.class.getClassLoader())));
        this.videoHastags = ((String) in.readValue((String.class.getClassLoader())));
        this.isLiked = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.count = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.musicTitle = ((String) in.readValue((String.class.getClassLoader())));
        this.musicDescription = ((String) in.readValue((String.class.getClassLoader())));
        this.musicReuseCount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.musicThumbnail = ((String) in.readValue((String.class.getClassLoader())));
        this.followerCount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.followingCount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.deepLink = ((String) in.readValue(String.class.getClassLoader()));
        this.savedStatus = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.savedVideoPrivacy = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.waterMarkUrl = ((String) in.readValue(String.class.getClassLoader()));
        this.usersName = ((String) in.readValue(String.class.getClassLoader()));
    }

    public Result() {
    }

    public Integer getFollowerCount() {
        return followerCount;
    }

    public void setFollowerCount(Integer followerCount) {
        this.followerCount = followerCount;
    }

    public Integer getFollowingCount() {
        return followingCount;
    }

    public void setFollowingCount(Integer followingCount) {
        this.followingCount = followingCount;
    }

    public Integer getMusicReuseCount() {
        return musicReuseCount;
    }

    public void setMusicReuseCount(Integer musicReuseCount) {
        this.musicReuseCount = musicReuseCount;
    }

    public String getMusicThumbnail() {
        return musicThumbnail;
    }

    public void setMusicThumbnail(String musicThumbnail) {
        this.musicThumbnail = musicThumbnail;
    }

    public String getMusicDescription() {
        return musicDescription;
    }

    public void setMusicDescription(String musicDescription) {
        this.musicDescription = musicDescription;
    }

    public String getMusicTitle() {
        return musicTitle;
    }

    public void setMusicTitle(String musicTitle) {
        this.musicTitle = musicTitle;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(Integer isLiked) {
        this.isLiked = isLiked;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIsFollower() {
        return isFollower;
    }

    public void setIsFollower(String isFollower) {
        this.isFollower = isFollower;
    }

    public Integer getMusicId() {
        return musicId;
    }

    public void setMusicId(Integer musicId) {
        this.musicId = musicId;
    }

    public String getMusicUrl() {
        return musicUrl;
    }

    public void setMusicUrl(String musicUrl) {
        this.musicUrl = musicUrl;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getProfileThumbnail() {
        return profileThumbnail;
    }

    public void setProfileThumbnail(String profileThumbnail) {
        this.profileThumbnail = profileThumbnail;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    public Object getReusedBy() {
        return reusedBy;
    }

    public void setReusedBy(Object reusedBy) {
        this.reusedBy = reusedBy;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public Integer getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(Integer totalLikes) {
        this.totalLikes = totalLikes;
    }

    public Integer getTotalReuse() {
        return totalReuse;
    }

    public void setTotalReuse(Integer totalReuse) {
        this.totalReuse = totalReuse;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getVideoDescription() {
        return videoDescription;
    }

    public void setVideoDescription(String videoDescription) {
        this.videoDescription = videoDescription;
    }

    public String getVideoHastags() {
        return videoHastags;
    }

    public void setVideoHastags(String videoHastags) {
        this.videoHastags = videoHastags;
    }

    public String getDeepLink() {
        return deepLink;
    }

    public void setDeepLink(String deepLink) {
        this.deepLink = deepLink;
    }


    public Integer getSavedStatus() {
        return savedStatus;
    }

    public void setSavedStatus(Integer savedStatus) {
        this.savedStatus = savedStatus;
    }

    public Integer getSavedVideoPrivacy() {
        return savedVideoPrivacy;
    }

    public void setSavedVideoPrivacy(Integer savedVideoPrivacy) {
        this.savedVideoPrivacy = savedVideoPrivacy;
    }


    public String getWaterMarkUrl() {
        return waterMarkUrl;
    }

    public void setWaterMarkUrl(String waterMarkUrl) {
        this.waterMarkUrl = waterMarkUrl;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(commentCount);
        dest.writeValue(firstName);
        dest.writeValue(id);
        dest.writeValue(isFollower);
        dest.writeValue(musicId);
        dest.writeValue(musicUrl);
        dest.writeValue(profileImage);
        dest.writeValue(profileThumbnail);
        dest.writeValue(ranking);
        dest.writeValue(reusedBy);
        dest.writeValue(thumbnailUrl);
        dest.writeValue(totalLikes);
        dest.writeValue(totalReuse);
        dest.writeValue(type);
        dest.writeValue(url);
        dest.writeValue(userId);
        dest.writeValue(userName);
        dest.writeValue(videoDescription);
        dest.writeValue(videoHastags);
        dest.writeValue(isLiked);
        dest.writeValue(count);
        dest.writeValue(musicTitle);
        dest.writeValue(musicDescription);
        dest.writeValue(musicReuseCount);
        dest.writeValue(musicThumbnail);
        dest.writeValue(followerCount);
        dest.writeValue(followingCount);
        dest.writeValue(deepLink);
        dest.writeValue(savedStatus);
        dest.writeValue(savedVideoPrivacy);
        dest.writeValue(waterMarkUrl);
        dest.writeValue(usersName);
    }

    public int describeContents() {
        return 0;
    }

    public String getUsersName() {
        return usersName;
    }

    public void setUsersName(String usersName) {
        this.usersName = usersName;
    }
}
