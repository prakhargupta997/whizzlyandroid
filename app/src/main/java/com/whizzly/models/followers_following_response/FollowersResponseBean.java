
package com.whizzly.models.followers_following_response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FollowersResponseBean implements Parcelable {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("next_count")
    @Expose
    private Integer nextCount;
    @SerializedName("next_url")
    @Expose
    private String nextUrl;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;
    private String type;
    private Integer count;
    private Integer accountType;
    private String OtherUserId;

    protected FollowersResponseBean(Parcel in) {
        if (in.readByte() == 0) {
            code = null;
        } else {
            code = in.readInt();
        }
        message = in.readString();
        if (in.readByte() == 0) {
            nextCount = null;
        } else {
            nextCount = in.readInt();
        }
        nextUrl = in.readString();
        result = in.createTypedArrayList(Result.CREATOR);
        type = in.readString();
        if (in.readByte() == 0) {
            count = null;
        } else {
            count = in.readInt();
        }
        if (in.readByte() == 0) {
            accountType = null;
        } else {
            accountType = in.readInt();
        }
        OtherUserId = in.readString();
    }

    public static final Creator<FollowersResponseBean> CREATOR = new Creator<FollowersResponseBean>() {
        @Override
        public FollowersResponseBean createFromParcel(Parcel in) {
            return new FollowersResponseBean(in);
        }

        @Override
        public FollowersResponseBean[] newArray(int size) {
            return new FollowersResponseBean[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getNextCount() {
        return nextCount;
    }

    public void setNextCount(Integer nextCount) {
        this.nextCount = nextCount;
    }

    public String getNextUrl() {
        return nextUrl;
    }

    public void setNextUrl(String nextUrl) {
        this.nextUrl = nextUrl;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }



    public Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }

    public String getOtherUserId() {
        return OtherUserId;
    }

    public void setOtherUserId(String otherUserId) {
        OtherUserId = otherUserId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (code == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(code);
        }
        dest.writeString(message);
        if (nextCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(nextCount);
        }
        dest.writeString(nextUrl);
        dest.writeTypedList(result);
        dest.writeString(type);
        if (count == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(count);
        }
        if (accountType == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(accountType);
        }
        dest.writeString(OtherUserId);
    }
}
