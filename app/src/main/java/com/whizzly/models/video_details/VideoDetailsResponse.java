
package com.whizzly.models.video_details;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoDetailsResponse implements Parcelable {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private com.whizzly.models.feeds_response.Result result;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public com.whizzly.models.feeds_response.Result getResult() {
        return result;
    }

    public void setResult(com.whizzly.models.feeds_response.Result result) {
        this.result = result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.code);
        dest.writeString(this.message);
        dest.writeParcelable(this.result, flags);
    }

    protected VideoDetailsResponse(Parcel in) {
        this.code = (Integer) in.readValue(Integer.class.getClassLoader());
        this.message = in.readString();
        this.result = in.readParcelable(Result.class.getClassLoader());
    }

    public static final Creator<VideoDetailsResponse> CREATOR = new Creator<VideoDetailsResponse>() {
        @Override
        public VideoDetailsResponse createFromParcel(Parcel source) {
            return new VideoDetailsResponse(source);
        }

        @Override
        public VideoDetailsResponse[] newArray(int size) {
            return new VideoDetailsResponse[size];
        }
    };
}
