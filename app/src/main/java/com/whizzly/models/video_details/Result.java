
package com.whizzly.models.video_details;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result implements Parcelable {

    @SerializedName("comment_count")
    @Expose
    private Integer commentCount;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("follower_count")
    @Expose
    private Integer followerCount;
    @SerializedName("following_count")
    @Expose
    private Integer followingCount;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("is_follower")
    @Expose
    private Integer isFollower;
    @SerializedName("is_liked")
    @Expose
    private Integer isLiked;
    @SerializedName("music_description")
    @Expose
    private String musicDescription;
    @SerializedName("music_id")
    @Expose
    private Integer musicId;
    @SerializedName("music_reuse_count")
    @Expose
    private Integer musicReuseCount;
    @SerializedName("music_thumbnail")
    @Expose
    private String musicThumbnail;
    @SerializedName("music_title")
    @Expose
    private String musicTitle;
    @SerializedName("music_url")
    @Expose
    private String musicUrl;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("ranking")
    @Expose
    private Integer ranking;
    @SerializedName("reused_by")
    @Expose
    private String reusedBy;
    @SerializedName("thumbnail_url")
    @Expose
    private String thumbnailUrl;
    @SerializedName("total_likes")
    @Expose
    private Integer totalLikes;
    @SerializedName("total_reuse")
    @Expose
    private Integer totalReuse;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("video_description")
    @Expose
    private String videoDescription;
    @SerializedName("video_hastags")
    @Expose
    private String videoHastags;

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getFollowerCount() {
        return followerCount;
    }

    public void setFollowerCount(Integer followerCount) {
        this.followerCount = followerCount;
    }

    public Integer getFollowingCount() {
        return followingCount;
    }

    public void setFollowingCount(Integer followingCount) {
        this.followingCount = followingCount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIsFollower() {
        return isFollower;
    }

    public void setIsFollower(Integer isFollower) {
        this.isFollower = isFollower;
    }

    public Integer getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(Integer isLiked) {
        this.isLiked = isLiked;
    }

    public String getMusicDescription() {
        return musicDescription;
    }

    public void setMusicDescription(String musicDescription) {
        this.musicDescription = musicDescription;
    }

    public Integer getMusicId() {
        return musicId;
    }

    public void setMusicId(Integer musicId) {
        this.musicId = musicId;
    }

    public Integer getMusicReuseCount() {
        return musicReuseCount;
    }

    public void setMusicReuseCount(Integer musicReuseCount) {
        this.musicReuseCount = musicReuseCount;
    }

    public String getMusicThumbnail() {
        return musicThumbnail;
    }

    public void setMusicThumbnail(String musicThumbnail) {
        this.musicThumbnail = musicThumbnail;
    }

    public String getMusicTitle() {
        return musicTitle;
    }

    public void setMusicTitle(String musicTitle) {
        this.musicTitle = musicTitle;
    }

    public String getMusicUrl() {
        return musicUrl;
    }

    public void setMusicUrl(String musicUrl) {
        this.musicUrl = musicUrl;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    public String getReusedBy() {
        return reusedBy;
    }

    public void setReusedBy(String reusedBy) {
        this.reusedBy = reusedBy;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public Integer getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(Integer totalLikes) {
        this.totalLikes = totalLikes;
    }

    public Integer getTotalReuse() {
        return totalReuse;
    }

    public void setTotalReuse(Integer totalReuse) {
        this.totalReuse = totalReuse;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getVideoDescription() {
        return videoDescription;
    }

    public void setVideoDescription(String videoDescription) {
        this.videoDescription = videoDescription;
    }

    public String getVideoHastags() {
        return videoHastags;
    }

    public void setVideoHastags(String videoHastags) {
        this.videoHastags = videoHastags;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.commentCount);
        dest.writeValue(this.count);
        dest.writeString(this.firstName);
        dest.writeValue(this.followerCount);
        dest.writeValue(this.followingCount);
        dest.writeValue(this.id);
        dest.writeValue(this.isFollower);
        dest.writeValue(this.isLiked);
        dest.writeString(this.musicDescription);
        dest.writeValue(this.musicId);
        dest.writeValue(this.musicReuseCount);
        dest.writeString(this.musicThumbnail);
        dest.writeString(this.musicTitle);
        dest.writeString(this.musicUrl);
        dest.writeString(this.profileImage);
        dest.writeValue(this.ranking);
        dest.writeString(this.reusedBy);
        dest.writeString(this.thumbnailUrl);
        dest.writeValue(this.totalLikes);
        dest.writeValue(this.totalReuse);
        dest.writeString(this.type);
        dest.writeString(this.url);
        dest.writeValue(this.userId);
        dest.writeString(this.userName);
        dest.writeString(this.videoDescription);
        dest.writeString(this.videoHastags);
    }

    public Result() {
    }

    protected Result(Parcel in) {
        this.commentCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.count = (Integer) in.readValue(Integer.class.getClassLoader());
        this.firstName = in.readString();
        this.followerCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.followingCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.isFollower = (Integer) in.readValue(Integer.class.getClassLoader());
        this.isLiked = (Integer) in.readValue(Integer.class.getClassLoader());
        this.musicDescription = in.readString();
        this.musicId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.musicReuseCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.musicThumbnail = in.readString();
        this.musicTitle = in.readString();
        this.musicUrl = in.readString();
        this.profileImage = in.readString();
        this.ranking = (Integer) in.readValue(Integer.class.getClassLoader());
        this.reusedBy = in.readString();
        this.thumbnailUrl = in.readString();
        this.totalLikes = (Integer) in.readValue(Integer.class.getClassLoader());
        this.totalReuse = (Integer) in.readValue(Integer.class.getClassLoader());
        this.type = in.readString();
        this.url = in.readString();
        this.userId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.userName = in.readString();
        this.videoDescription = in.readString();
        this.videoHastags = in.readString();
    }

    public static final Creator<Result> CREATOR = new Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel source) {
            return new Result(source);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };
}
