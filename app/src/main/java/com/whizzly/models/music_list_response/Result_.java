package com.whizzly.models.music_list_response;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableInt;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.whizzly.BR;

public class Result_ extends BaseObservable {

    @SerializedName("composer_name")
    @Expose
    private String composerName;
    @SerializedName("music_url")
    @Expose
    private String musicFile;
    @SerializedName("id")
    @Expose
    private int musicId;
    @SerializedName("music_title")
    @Expose
    private String musicName;
    @SerializedName("thumbnail_url")
    @Expose
    private String thumbnail;
    @SerializedName("usage_count")
    @Expose
    private String musicCollabCount;
    @SerializedName("duration")
    @Expose
    private String musicTime;

    private ObservableBoolean isCardClicked = new ObservableBoolean(false);
    private ObservableBoolean isPlayPause = new ObservableBoolean(false);

    public String getMusicTime() {
        return musicTime + " s";
    }

    public void setMusicTime(String musicTime) {
        this.musicTime = musicTime;
    }

    public String getMusicCollabCount() {
        return musicCollabCount;
    }

    public void setMusicCollabCount(String musicCollabCount) {
        this.musicCollabCount = musicCollabCount;
    }

    public String getComposerName() {
        return composerName;
    }

    public void setComposerName(String composerName) {
        this.composerName = composerName;
    }

    public String getMusicFile() {
        return musicFile;
    }

    public void setMusicFile(String musicFile) {
        this.musicFile = musicFile;
    }

    public int getMusicId() {
        return musicId;
    }

    public void setMusicId(int musicId) {
        this.musicId = musicId;
    }

    public String getMusicName() {
        return musicName;
    }

    public void setMusicName(String musicName) {
        this.musicName = musicName;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Bindable
    public boolean getIsCardClicked() {
        return isCardClicked.get();
    }

    public void setIsCardClicked(boolean isCardClicked) {
        this.isCardClicked.set(isCardClicked);
        notifyPropertyChanged(BR.isCardClicked);
    }

    @Bindable
    public boolean getIsPlayPause() {
        return isPlayPause.get();
    }

    public void setIsPlayPause(boolean isPlayPause) {
        this.isPlayPause.set(isPlayPause);
        notifyPropertyChanged(BR.isPlayPause);
    }
}
