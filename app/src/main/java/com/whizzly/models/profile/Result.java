
package com.whizzly.models.profile;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {


    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("follower_count")
    @Expose
    private int followerCount;
    @SerializedName("following_count")
    @Expose
    private int followingCount;
    @SerializedName("is_following")
    @Expose
    private int isFollowing;
    @SerializedName("likes_count")
    @Expose
    private int likesCount;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("reused_count")
    @Expose
    private int reusedCount;
    @SerializedName("thumbnail_url")
    @Expose
    private Object thumbnailUrl;
    @SerializedName("user_email")
    @Expose
    private String userEmail;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getFollowerCount() {
        return followerCount;
    }

    public void setFollowerCount(int followerCount) {
        this.followerCount = followerCount;
    }

    public int getFollowingCount() {
        return followingCount;
    }

    public void setFollowingCount(int followingCount) {
        this.followingCount = followingCount;
    }

    public int getIsFollowing() {
        return isFollowing;
    }

    public void setIsFollowing(int isFollowing) {
        this.isFollowing = isFollowing;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public int getReusedCount() {
        return reusedCount;
    }

    public void setReusedCount(int reusedCount) {
        this.reusedCount = reusedCount;
    }

    public Object getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(Object thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

}
