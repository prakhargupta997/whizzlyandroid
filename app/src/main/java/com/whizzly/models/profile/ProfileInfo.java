package com.whizzly.models.profile;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileInfo implements Parcelable {


    public int getReusesByOthers() {
        return reusesByOthers;
    }

    public void setReusesByOthers(int reusesByOthers) {
        this.reusesByOthers = reusesByOthers;
    }
    public int getReusesByUser() {
        return reusesByUser;
    }

    public void setReusesByUser(int reusesByUser) {
        this.reusesByUser = reusesByUser;
    }

    @SerializedName("reuses_by_others")
    @Expose
    private int reusesByOthers;

    @SerializedName("reuses_by_user")
    @Expose
    private int reusesByUser;

    @SerializedName("Rating")
    @Expose
    private String rating;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("follower_count")
    @Expose
    private int followerCount;
    @SerializedName("following_count")
    @Expose
    private int followingCount;
    @SerializedName("likes_count")
    @Expose
    private int likesCount;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("reused_count")
    @Expose
    private int reusedCount;
    @SerializedName("thumbnail_url")
    @Expose
    private String thumbnailUrl;
    @SerializedName("user_email")
    @Expose
    private String userEmail;
    @SerializedName("is_following")
    @Expose
    private int isFollowing;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("user_bio")
    @Expose
    private String userBio;
    @SerializedName("is_blocked")
    @Expose
    private int isBlocked;
    @SerializedName("gender")
    @Expose
    private int gender;

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getUserBio() {
        return userBio;
    }

    public void setUserBio(String userBio) {
        this.userBio = userBio;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getFollowerCount() {
        return followerCount;
    }

    public void setFollowerCount(int followerCount) {
        this.followerCount = followerCount;
    }

    public int getFollowingCount() {
        return followingCount;
    }

    public void setFollowingCount(int followingCount) {
        this.followingCount = followingCount;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public int getReusedCount() {
        return reusedCount;
    }

    public void setReusedCount(int reusedCount) {
        this.reusedCount = reusedCount;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public int getIsFollowing() {
        return isFollowing;
    }

    public void setIsFollowing(int isFollowing) {
        this.isFollowing = isFollowing;
    }


    public int getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(int isBlocked) {
        this.isBlocked = isBlocked;
    }

    public ProfileInfo() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.reusesByOthers);
        dest.writeInt(this.reusesByUser);
        dest.writeString(this.rating);
        dest.writeString(this.firstName);
        dest.writeInt(this.followerCount);
        dest.writeInt(this.followingCount);
        dest.writeInt(this.likesCount);
        dest.writeString(this.profileImage);
        dest.writeInt(this.reusedCount);
        dest.writeString(this.thumbnailUrl);
        dest.writeString(this.userEmail);
        dest.writeInt(this.isFollowing);
        dest.writeString(this.userName);
        dest.writeString(this.userBio);
        dest.writeInt(this.isBlocked);
        dest.writeInt(this.gender);
    }

    protected ProfileInfo(Parcel in) {
        this.reusesByOthers = in.readInt();
        this.reusesByUser = in.readInt();
        this.rating = in.readString();
        this.firstName = in.readString();
        this.followerCount = in.readInt();
        this.followingCount = in.readInt();
        this.likesCount = in.readInt();
        this.profileImage = in.readString();
        this.reusedCount = in.readInt();
        this.thumbnailUrl = in.readString();
        this.userEmail = in.readString();
        this.isFollowing = in.readInt();
        this.userName = in.readString();
        this.userBio = in.readString();
        this.isBlocked = in.readInt();
        this.gender = in.readInt();
    }

    public static final Creator<ProfileInfo> CREATOR = new Creator<ProfileInfo>() {
        @Override
        public ProfileInfo createFromParcel(Parcel source) {
            return new ProfileInfo(source);
        }

        @Override
        public ProfileInfo[] newArray(int size) {
            return new ProfileInfo[size];
        }
    };
}
